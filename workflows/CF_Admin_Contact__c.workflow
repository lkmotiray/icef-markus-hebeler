<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Active_CF_Admin_s_Account_has_no_CF_schools</fullName>
        <description>Active CF Admin&apos;s Account has no CF schools</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>hschoenleber@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Coursefinder_Communication/Active_CF_Admin_s_Account_has_no_CF_Schools</template>
    </alerts>
    <alerts>
        <fullName>Reminder_to_SalesRep_7days_after_Contact_has_been_marked_as_CF_Admin</fullName>
        <ccEmails>rholmes@icef.com</ccEmails>
        <description>Reminder to SalesRep 7days after Contact has been marked as CF Admin</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/Check_profile_completeness_of_CF_school_new</template>
    </alerts>
    <alerts>
        <fullName>send_CF_welcome_Email_with_review_link</fullName>
        <ccEmails>internal@coursefinders.com</ccEmails>
        <description>send CF welcome Email with review link</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rholmes@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Coursefinder_Communication/Welcome_Email_with_link_to_review</template>
    </alerts>
    <rules>
        <fullName>New CF Admin reminder after 7 days</fullName>
        <active>true</active>
        <description>send a reminder to the account owner if a Contact has been added as CF Admin to check CF Schools&apos; profile completeness</description>
        <formula>AND(
  ISPICKVAL(Status__c,&quot;Active&quot;),
  Welcome_Email_sent_date__c &gt;= NOW()-0.01
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_to_SalesRep_7days_after_Contact_has_been_marked_as_CF_Admin</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>CF_Admin_Contact__c.Welcome_Email_sent_date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
