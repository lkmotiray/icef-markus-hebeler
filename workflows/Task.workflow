<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>check_for_Date_acted_upon_tick</fullName>
        <description>tick box for checking Date acted upon</description>
        <field>Check_for_Date_acted_upon__c</field>
        <literalValue>1</literalValue>
        <name>check for Date acted upon - tick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Contact Date</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>Email:</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>notContain</operation>
            <value>Act-On</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Contact field &apos;Date last acted upon&apos; is updated when an email is logged to &apos;Activity History&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>check for Date acted upon</fullName>
        <actions>
            <name>check_for_Date_acted_upon_tick</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.TaskSubtype</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <description>When a task is created, check tick box to activate process (Task is created or edited)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
