<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CP_field_No_eSchedule_has_been_unticked</fullName>
        <description>CP field &apos;No eSchedule&apos; has been unticked</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Providers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_communication_Workshop/CP_field_No_eSchedule_has_been_unticked</template>
    </alerts>
    <alerts>
        <fullName>CP_has_been_ticked_with_No_eSchedule</fullName>
        <description>CP has been ticked with &apos;No eSchedule&apos;</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Providers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/CP_has_been_ticked_with_No_eSchedule</template>
    </alerts>
    <alerts>
        <fullName>CP_of_an_agency_has_been_set_to_status_Guest</fullName>
        <description>CP of an agency has been set to status &apos;Guest&apos;</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ARMManager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/CP_for_a_Guest_has_been_created_for_an_agency</template>
    </alerts>
    <alerts>
        <fullName>Client_has_not_logged_in_3_weeks_before_start_date</fullName>
        <description>Client has not logged in 3 weeks before start date</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Rep_communication/Client_has_not_yet_logged_in_to_ESP_alert</template>
    </alerts>
    <alerts>
        <fullName>Client_has_too_little_number_of_Appointments_28_days_prior_to_the_WS</fullName>
        <description>Client has too little number of Appointments 28 days prior to the WS</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Rep_communication/Client_has_less_than_50_of_possible_appointments</template>
    </alerts>
    <alerts>
        <fullName>Client_hasn_t_logged_in_to_ESp_10_days_after_it_s_open</fullName>
        <description>Client hasn&apos;t logged in to ESp 10 days after it&apos;s open</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Rep_communication/Client_has_not_yet_logged_in_to_ESP_alert</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_Letter_for_agents_alert</fullName>
        <description>Confirmation Letter for agents alert</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Confirmation_Letter_Alert_CP</template>
    </alerts>
    <alerts>
        <fullName>Contact_Participation_Agency_has_been_canceled_onsite</fullName>
        <description>Contact Participation Agency has been canceled onsite</description>
        <protected>false</protected>
        <recipients>
            <field>ARM_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Contact_Participation_Agency_canceled_onsite</template>
    </alerts>
    <alerts>
        <fullName>Contact_Participation_Agency_has_been_cancelled_or_replaced</fullName>
        <description>Contact Participation Agency has been cancelled or replaced</description>
        <protected>false</protected>
        <recipients>
            <field>ARM_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Contact_Participation_Agency_cancelled</template>
    </alerts>
    <alerts>
        <fullName>Worst_Offender_has_booked_another_workshop</fullName>
        <description>Worst Offender has booked another workshop</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Worst_Offender_is_booked_for_another_Workshop</template>
    </alerts>
    <fieldUpdates>
        <fullName>AP_Record_Type_Text_fill</fullName>
        <field>Account_Participation_Record_Type_Text__c</field>
        <formula>Account_Participation__r.RecordType.Name</formula>
        <name>AP Record Type Text fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AP_Status_text_fill</fullName>
        <field>Account_Participation_Status_Text__c</field>
        <formula>TEXT(Account_Participation__r.Participation_Status__c)</formula>
        <name>AP Status text fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ARM_email_update</fullName>
        <field>ARM_emailaddress__c</field>
        <formula>Contact__r.Account.ARM__r.Email</formula>
        <name>ARM email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Owner_Email_update</fullName>
        <field>Account_Owner_Email__c</field>
        <formula>Account_Participation__r.Account__r.Owner.Email</formula>
        <name>Account Owner Email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CP_Title</fullName>
        <description>Fills field Title with the value of field Title in object contacts.</description>
        <field>Contact_Title__c</field>
        <formula>Contact__r.Title</formula>
        <name>CP Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CP_status_update</fullName>
        <description>Change the field entry in &apos;participation status&apos; in Contact Participation to &apos;cancelled&apos; when Account Participaton is cancelled.</description>
        <field>Participation_Status__c</field>
        <literalValue>cancelled</literalValue>
        <name>CP status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Manager_Agents_email_update</fullName>
        <field>Event_Managers_Agents_Email__c</field>
        <formula>Account_Participation__r.ICEF_Event__r.Event_Manager_Agents__r.Email</formula>
        <name>Event Manager (Agents) email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Manager_Educators_email_update</fullName>
        <field>Event_Managers_Providers_Email__c</field>
        <formula>Account_Participation__r.ICEF_Event__r.Event_Manager_Educators__r.Email</formula>
        <name>Event Manager (Educators) email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Preferred_WS_email</fullName>
        <description>Field Preferred WS Email shall be updated with the value of field &apos;Workshop Email&apos; if it is empty.</description>
        <field>Preferred_WS_Email__c</field>
        <formula>Workshop_email__c</formula>
        <name>Preferred WS email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Udate_Event_Start_Date</fullName>
        <field>Start_Date_of_Event__c</field>
        <formula>ICEF_Event__r.Start_date__c</formula>
        <name>Udate Event Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ARM Email Autofill</fullName>
        <actions>
            <name>ARM_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ARM_emailaddress__c &lt;&gt; Contact__r.Account.ARM__r.Email</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Owner Email Autofill</fullName>
        <actions>
            <name>Account_Owner_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Account_Owner_Email__c &lt;&gt;  Account_Participation__r.Account__r.Owner.Email</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Alert for Confirmation Letters to Agents</fullName>
        <actions>
            <name>Confirmation_Letter_for_agents_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Record_Type__c</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Status__c</field>
            <operation>equals</operation>
            <value>accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>registered</value>
        </criteriaItems>
        <description>When an agency gets Status &apos;Accepted&apos; then an email alert will be sent to the agents&apos; event manager to remind him to send out the confirmation letter</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CP Title</fullName>
        <actions>
            <name>CP_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Title</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Contact_Title__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field &apos;Title&apos; is filled with the value of field &apos;Title&apos; in Contact when CP is created.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CP created or edited</fullName>
        <actions>
            <name>Event_Manager_Agents_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Event_Manager_Educators_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Udate_Event_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CP field %27No eSchedule%27 has been unticked</fullName>
        <actions>
            <name>CP_field_No_eSchedule_has_been_unticked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND ( 
  AND ( 
    ICEF_Event__r.ESP_go_live__c, 
    Start_Date_of_Event__c &gt; TODAY() 
  ),
  ISNEW() = FALSE,
  PRIORVALUE ( No_eSchedule__c ) = TRUE,
  No_eSchedule__c = FALSE,
  Participation_Status_Text__c = &quot;registered&quot;,
  OR (
    Account_Participation_Status__c = &quot;registered&quot;,
    Account_Participation_Status__c = &quot;pending payment&quot;,
    Account_Participation_Status__c = &quot;accepted&quot;
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CP has been ticked with %27No eSchedule%27</fullName>
        <actions>
            <name>CP_has_been_ticked_with_No_eSchedule</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND ( 
  AND ( 
    ICEF_Event__r.ESP_go_live__c, 
    Start_Date_of_Event__c &gt; TODAY() 
  ),
  ISNEW() = FALSE,
  PRIORVALUE ( No_eSchedule__c ) = FALSE,
  No_eSchedule__c = TRUE,
  Participation_Status_Text__c = &quot;registered&quot;,
  OR (
    Account_Participation_Status__c = &quot;registered&quot;,
    Account_Participation_Status__c = &quot;pending payment&quot;,
    Account_Participation_Status__c = &quot;accepted&quot;
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CP is created</fullName>
        <actions>
            <name>AP_Record_Type_Text_fill</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AP_Status_text_fill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CP of Agency has Status %27Guest%27</fullName>
        <actions>
            <name>CP_of_an_agency_has_been_set_to_status_Guest</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Status__c</field>
            <operation>equals</operation>
            <value>accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>Guest</value>
        </criteriaItems>
        <description>A CP for an AP that belongs to an agency has been set to status &apos;Guest&apos;. The appropriate Event Manager (Agents) is informed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cancellation of Agency CP</fullName>
        <actions>
            <name>Contact_Participation_Agency_has_been_cancelled_or_replaced</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>cancelled,replaced</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Record_Type__c</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Status__c</field>
            <operation>equals</operation>
            <value>accepted</value>
        </criteriaItems>
        <description>The ARM, Tiffany and the Agencies&apos; event manager are informed when an agency&apos;s CP is cancelled or replaced.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer hasn%27t logged in to ESP yet</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Record_Type__c</field>
            <operation>equals</operation>
            <value>Agent,Educator,Exhibitor,Work and Travel</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.espstats_last_login__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.esp_login_transfer__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Status_Text__c</field>
            <operation>equals</operation>
            <value>accepted,registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>ICEF_Event__c.Start_date__c</field>
            <operation>greaterThan</operation>
            <value>NEXT 10 DAYS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.No_eSchedule__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Customer hasn&apos;t logged in to ESP after 10 days  and Account Owner is informed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Client_has_not_logged_in_3_weeks_before_start_date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contact_Participation__c.ICEF_event_Start_Date__c</offsetFromField>
            <timeLength>-21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Client_hasn_t_logged_in_to_ESp_10_days_after_it_s_open</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contact_Participation__c.ESP_access_possible_day__c</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Onsite Cancelation of Agency CP</fullName>
        <actions>
            <name>Contact_Participation_Agency_has_been_canceled_onsite</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>canceled onsite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Record_Type__c</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <description>The ARM, Tiffany and the Agencies&apos; event manager are informed when an agency&apos;s CP is canceled onsite.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Preferred WS email update</fullName>
        <actions>
            <name>Preferred_WS_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact_Participation__c.Preferred_WS_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field &apos;Preferred WS Email&apos; is filled with the emailaddress from &apos;Workshop Email&apos; IF it is empty.
Reason: &apos;Workshop Email&apos; is a formular field and therefore does not appear in the search. &apos;Preferred WS Email&apos; will be searchable now.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Too little appointments prior to WS</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9</booleanFilter>
        <criteriaItems>
            <field>Contact_Participation__c.esp_login_transfer__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Meeting_number_ratio__c</field>
            <operation>lessThan</operation>
            <value>0.5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.ESP_access_possible_day__c</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.espstats_last_login__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>ICEF_Event__c.Start_date__c</field>
            <operation>greaterThan</operation>
            <value>NEXT 27 DAYS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.No_eSchedule__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Status__c</field>
            <operation>equals</operation>
            <value>accepted,registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Record_Type__c</field>
            <operation>equals</operation>
            <value>Agent,Educator,Exhibitor,Work and Travel</value>
        </criteriaItems>
        <description>The CP has less than 50% of maximum appointments 4 weeks prior to the event.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Client_has_too_little_number_of_Appointments_28_days_prior_to_the_WS</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contact_Participation__c.ICEF_event_Start_Date__c</offsetFromField>
            <timeLength>-28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Worst Offender person is registered for a workshop</fullName>
        <actions>
            <name>Worst_Offender_has_booked_another_workshop</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact_Participation__c.Account_Participation_Record_Type__c</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Worst_Offender_entries__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
