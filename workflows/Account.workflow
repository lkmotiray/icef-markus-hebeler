<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ARM_please_check_owner</fullName>
        <description>ARM please check owner</description>
        <protected>false</protected>
        <recipients>
            <field>ARM_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Agency_has_unexpected_Owner</template>
    </alerts>
    <alerts>
        <fullName>Account_Record_Type_has_been_changed</fullName>
        <description>Account Record Type has been changed</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>james.love@pobox.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mhebeler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Rep_communication/Account_record_type_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Account_created_by_FF_support_has_been_edited</fullName>
        <description>Account created by FF support has been edited</description>
        <protected>false</protected>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Account_created_by_FF_has_been_edited</template>
    </alerts>
    <alerts>
        <fullName>Account_has_no_PDM_contact</fullName>
        <ccEmails>hschoenleber@icef.com</ccEmails>
        <description>Account has no PDM contact</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Account_has_no_PDM_alert</template>
    </alerts>
    <alerts>
        <fullName>Account_with_CF_school_has_been_deactivated</fullName>
        <description>Account with CF school has been deactivated</description>
        <protected>false</protected>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pschmitz@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/DB_update_Account_with_CF_School_has_been_deactivated</template>
    </alerts>
    <alerts>
        <fullName>Agency_has_booked_more_than_3_WSs_in_this_year</fullName>
        <description>Agency has booked more than 3 WSs in this year</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Agency_has_more_than_3_bookings_in_this_year</template>
    </alerts>
    <alerts>
        <fullName>Association_Type_has_been_changed_and_Sys_Admins_are_alerted</fullName>
        <description>Association Type has been changed and Sys Admins are alerted</description>
        <protected>false</protected>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Association_type_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>CF_Free_Trial_has_run_out</fullName>
        <ccEmails>internal@coursefinders.com</ccEmails>
        <description>CF Free Trial has run out</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/CF_Free_Trial_is_running_out</template>
    </alerts>
    <alerts>
        <fullName>New_Account_has_been_entered_and_Territory_Manager_is_informed</fullName>
        <description>New Account has been entered and Territory Manager is informed</description>
        <protected>false</protected>
        <recipients>
            <field>Territory_Manager_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/New_unassigned_Account_has_been_entered</template>
    </alerts>
    <alerts>
        <fullName>New_Association_with_Short_name_has_been_created</fullName>
        <ccEmails>shoresh@marcom-connect.com</ccEmails>
        <description>New Association with Short name has been created</description>
        <protected>false</protected>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Association_new_alert</template>
    </alerts>
    <alerts>
        <fullName>OW_account_is_about_to_expire</fullName>
        <description>OW account is about to expire</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Providers_OW_account_is_about_to_expire</template>
    </alerts>
    <alerts>
        <fullName>Organisation_Description_has_been_filled_out_via_webform</fullName>
        <description>Organisation Description has been filled out via webform</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Organisation_Description_Webform_has_been_filled_out</template>
    </alerts>
    <alerts>
        <fullName>Short_name_of_an_Association_has_been_changed</fullName>
        <ccEmails>shoresh@marcom-connect.com</ccEmails>
        <description>Short name of an Association has been changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Association_short_name_alert</template>
    </alerts>
    <alerts>
        <fullName>Students_sent_number_fields_have_been_filled_out</fullName>
        <description>Students sent number fields have been filled out</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>mhebeler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ARM_Alerts/Students_sent_Online_form_has_been_filled_out</template>
    </alerts>
    <alerts>
        <fullName>Sunhee_has_made_changed_and_Tiff_is_alerted</fullName>
        <description>Sunhee has made changed and Tiff is alerted</description>
        <protected>false</protected>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Sunhee_has_made_changes_and_Tiff_is_informed</template>
    </alerts>
    <alerts>
        <fullName>Test_OW_free_access_is_running_out_in_1_month</fullName>
        <description>Test - OW free access is running out in 1 month</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Alerts_provisioning_for_Online_Product_sale</template>
    </alerts>
    <alerts>
        <fullName>Website_of_an_Association_has_been_changed</fullName>
        <ccEmails>shoresh@marcom-connect.com</ccEmails>
        <description>Website of an Association has been changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Association_website_alert</template>
    </alerts>
    <alerts>
        <fullName>alert_to_create_IAS_membership</fullName>
        <ccEmails>hschoenleber@icef.com</ccEmails>
        <ccEmails>mhebeler@icef.com</ccEmails>
        <description>alert to create IAS membership</description>
        <protected>false</protected>
        <senderAddress>tegler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ARM_Alerts/IAS_Membership_to_create</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_update_checker</fullName>
        <field>Name</field>
        <name>Account update checker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_CF_credit_to_set_CF_balance_to_0</fullName>
        <description>when an account is set to free, set balance to 0 to avoid negative balance email alerts. 
Take negative balance and add its positive value in Added Credit</description>
        <field>Added_Credit__c</field>
        <formula>IF (
  Coursefinder_Account_Balance__c &lt; 0,
  Coursefinder_Account_Balance__c * -1,
  NULL
)</formula>
        <name>Add CF credit to set CF balance to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clean_CF_Account_Balance</fullName>
        <field>Coursefinder_Account_Balance__c</field>
        <name>Clean CF Account Balance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clean_CF_Trial_end</fullName>
        <field>CF_Free_Trial_end__c</field>
        <name>Clean CF Trial end</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clean_CF_Trial_start</fullName>
        <field>CF_Free_Trial__c</field>
        <name>Clean CF Trial start</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clean_CF_used_up_date</fullName>
        <field>CF_Account_Balance_Used_up_Date__c</field>
        <name>Clean CF used up date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_MyICEF_regform_date_field</fullName>
        <field>MyICEF_regform_date__c</field>
        <name>Clear MyICEF regform date field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Facebook_starts_with_http</fullName>
        <description>Facebook starts with http://and will be cut.</description>
        <field>Facebook__c</field>
        <formula>RIGHT (Facebook__c , (LEN (Facebook__c) -7))</formula>
        <name>Facebook starts with http</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Facebook_starts_with_https</fullName>
        <description>Facebook starts with https:// and is cut.</description>
        <field>Facebook__c</field>
        <formula>RIGHT (Facebook__c , (LEN (Facebook__c) -8))</formula>
        <name>Facebook starts with https://</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Mapping_Status_set_to_Not_Located</fullName>
        <field>Mapping_Status__c</field>
        <literalValue>Not Located Yet</literalValue>
        <name>Field Mapping Status set to Not Located</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Building_Country_by_Lookup</fullName>
        <field>ShippingCountry</field>
        <formula>Building_Country__r.Name</formula>
        <name>Fill Building Country by Lookup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Invoice_Country_by_Lookup</fullName>
        <field>BillingCountry</field>
        <formula>Invoice_Country__r.Name</formula>
        <name>Fill Invoice Country by Lookup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Mailing_Country_by_Lookup</fullName>
        <field>MailingCountry__c</field>
        <formula>Mailing_Country__r.Name</formula>
        <name>Fill Mailing Country by Lookup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IAS_Logo_publish_info_date_is_updated</fullName>
        <description>The IAS publish info date is updated to NOW.</description>
        <field>IAS_Logo_published_info_Date__c</field>
        <formula>NOW()</formula>
        <name>IAS Logo publish info date is updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IAS_Start_year_update</fullName>
        <description>Field &apos;IAS Start year&apos; is updated with current year if the account has been provided with an IAS number (and didn&apos;t have one before)</description>
        <field>IAS_Start_Year__c</field>
        <formula>TEXT(YEAR(TODAY()))</formula>
        <name>IAS Start year update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IAS_renewal_date_update</fullName>
        <description>Field &apos;IAS renewal date&apos; is updated with the date exactly one year in the future if the account has been provided with an IAS number (and didn&apos;t have one before)</description>
        <field>IAS_renewal_date__c</field>
        <formula>TODAY() + 365</formula>
        <name>IAS renewal date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IAS_sent_update_value</fullName>
        <description>The value of field &apos;IAS sent&apos; is changed to TODAY()</description>
        <field>IAS_sent__c</field>
        <formula>TODAY()</formula>
        <name>IAS sent - update value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IAS_year_update</fullName>
        <description>Field &apos;IAS year&apos; is updated with current year if the account has been provided with an IAS number (and didn&apos;t have one before)</description>
        <field>IAS_Year__c</field>
        <formula>TEXT(YEAR(TODAY()))</formula>
        <name>IAS year update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Invoice_address_Zip</fullName>
        <description>To update invoice address fields with the values from Mailing address (if Invoice address is empty) - Zip</description>
        <field>BillingPostalCode</field>
        <formula>Mailing_Post_Code__c</formula>
        <name>Invoice address Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Invoice_address_city</fullName>
        <description>To update invoice address fields with the values from Mailing address (if Invoice address is empty) - City</description>
        <field>BillingCity</field>
        <formula>Mailing_City__c</formula>
        <name>Invoice address_city</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Invoice_address_country</fullName>
        <description>To update invoice address fields with the values from Mailing address (if Invoice address is empty) - Country</description>
        <field>BillingCountry</field>
        <formula>MailingCountry__c</formula>
        <name>Invoice address country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Invoice_address_state</fullName>
        <description>To update invoice address fields with the values from Mailing address (if Invoice address is empty) - State</description>
        <field>BillingState</field>
        <formula>Mailing_State_Province__c</formula>
        <name>Invoice address state</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Invoice_address_street</fullName>
        <description>To update invoice address fields with the values from Mailing address (if Invoice address is empty) - Street</description>
        <field>BillingStreet</field>
        <formula>Mailing_street__c</formula>
        <name>Invoice address_street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Level_of_interest_update</fullName>
        <description>Connects field &apos;level of interest&apos; with the correct entry of field &apos;Status details&apos;</description>
        <field>Level_of_Interest_ICEF_Workshops__c</field>
        <literalValue>Company doesn&apos;t exist any more</literalValue>
        <name>Level of interest update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CF_Account_Type_to_Free</fullName>
        <field>CF_Account_Type__c</field>
        <literalValue>Free account</literalValue>
        <name>Set &apos;CF Account Type&apos; to Free</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CF_Account_Type_to_Paid</fullName>
        <field>CF_Account_Type__c</field>
        <literalValue>Paid account</literalValue>
        <name>Set &apos;CF Account Type&apos; to Paid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CF_Free_Trial_end_date</fullName>
        <field>CF_Free_Trial_end__c</field>
        <formula>CF_Free_Trial__c + 90</formula>
        <name>Set CF Free Trial end date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CF_account_type_to_Child</fullName>
        <field>CF_Account_Type__c</field>
        <literalValue>Child account</literalValue>
        <name>Set CF account type to Child</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_details_update</fullName>
        <description>When field &apos;level of interest&apos; shows &apos;Not interested in agents&apos; then the entry in field &apos;Status details&apos; is updated accordingly.</description>
        <field>Status_details__c</field>
        <literalValue>Not interested in agents</literalValue>
        <name>Status details update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_details_update_ceased_business</fullName>
        <field>Status_details__c</field>
        <literalValue>Ceased Business</literalValue>
        <name>Status details update ceased business</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_update</fullName>
        <description>When field &apos;level of interest&apos; shows &apos;Not interested in agents&apos; then the entry in field &apos;Status&apos; is updated accordingly.</description>
        <field>Status__c</field>
        <literalValue>Inactive</literalValue>
        <name>Status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_update_oos</fullName>
        <description>When field &apos;level of interest&apos; shows &apos;Company doesn&apos;t exist any more&apos; then the entry in field &apos;Status&apos; is updated accordingly</description>
        <field>Status__c</field>
        <literalValue>Out of service</literalValue>
        <name>Status update oos</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Students_sent_info_update</fullName>
        <description>Field Students sent info last updated is set to current time.</description>
        <field>Students_sent_info_last_updated__c</field>
        <formula>NOW()</formula>
        <name>Students sent info update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Students_sent_update_AUS_NZ</fullName>
        <description>Field &apos;Students sent info last updated AUS/NZ&apos; is filled with the value of now.</description>
        <field>Students_sent_info_last_updated_AUS_NZ__c</field>
        <formula>NOW()</formula>
        <name>Students sent update AUS/NZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Students_sent_update_USA_CAN</fullName>
        <description>Field &apos;Students sent info last updated USA/CAN&apos; is updated with value of now.</description>
        <field>Students_sent_info_last_updated_USA_CAN__c</field>
        <formula>NOW()</formula>
        <name>Students sent update USA/CAN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Territory_Manager_emailaddress_update</fullName>
        <field>Territory_Manager_emailaddress__c</field>
        <formula>Mailing_Country__r.Sales_Territory_Managers_Email__c</formula>
        <name>Territory Manager emailaddress update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Twitter_starts_with_http</fullName>
        <description>Twitter starts with http:// and is cut.</description>
        <field>Twitter__c</field>
        <formula>RIGHT (Twitter__c , (LEN (Twitter__c) -7))</formula>
        <name>Twitter starts with http://</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Twitter_starts_with_https</fullName>
        <description>Twitter starts with https:// and is cut</description>
        <field>Twitter__c</field>
        <formula>RIGHT (Twitter__c , (LEN (Twitter__c) -8))</formula>
        <name>Twitter starts with https://</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Invoice_Email</fullName>
        <description>Updates it from the email address of the Finance Contact</description>
        <field>c2g__CODAInvoiceEmail__c</field>
        <formula>c2g__CODAFinanceContact__r.Email</formula>
        <name>Update Invoice Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Website_1_account_update</fullName>
        <description>A URL may look like this only:
www. etc
http:// (but then no www.)
https:// followed by www.or not.</description>
        <field>Website</field>
        <formula>IF (
  BEGINS (LOWER (Website), &quot;http://www.&quot;),
  RIGHT (LOWER (Website), (LEN (Website) - 7)),
  &quot;http://&quot; + Substitute (LOWER (Website), &quot; &quot;, &quot;&amp;20&quot;)
)</formula>
        <name>Website 1 account update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Website_2_account_update</fullName>
        <description>A URL may look like this only:
www. etc
http:// (but then no www.)
https:// followed by www.or not.</description>
        <field>Website_2__c</field>
        <formula>IF (
  BEGINS (LOWER (Website_2__c) , &quot;http://www.&quot;),
  RIGHT (LOWER (Website_2__c), (LEN (Website_2__c) - 7)),
  &quot;http://&quot; + Substitute (LOWER(Website_2__c), &quot; &quot;, &quot;&amp;20&quot;)
)</formula>
        <name>Website 2 account update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>change_Account_Owner_to_Markus_Hebeler</fullName>
        <description>Owner is changed to Markus Hebeler</description>
        <field>OwnerId</field>
        <lookupValue>mhebeler@icef.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>change Account Owner to Markus Hebeler</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>clear_Latitude</fullName>
        <description>sets field Latitude to null</description>
        <field>Lat__c</field>
        <name>clear Latitude</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>clear_Longitude</fullName>
        <description>sets field Longitude to null</description>
        <field>Lon__c</field>
        <name>clear Longitude</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ARM please check owner</fullName>
        <actions>
            <name>ARM_please_check_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An agency has an owner that is not the person who is generally responsible for this country.</description>
        <formula>AND ( 
RecordTypeId   = &quot;012200000005D7g&quot;,
OwnerId  &lt;&gt;  Mailing_Country__r.ARM_User_ID__c 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Record Type has been changed</fullName>
        <actions>
            <name>Account_Record_Type_has_been_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The Record Type of an Account has been changed and the Account owner is informed about this.</description>
        <formula>AND (
  PRIORVALUE( RecordTypeId ) &lt;&gt;  RecordTypeId,
  $User.Id &lt;&gt; OwnerId ,
  $User.LastName &lt;&gt; &quot;Hebeler&quot;,
  $User.LastName &lt;&gt; &quot;Love&quot;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account created by FF has been edited</fullName>
        <actions>
            <name>Account_created_by_FF_support_has_been_edited</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>An account that has been created by FinancialForce support for technical reasons has been created.</description>
        <formula>AND (
  CreatedBy.Username  = &quot;ffonboarding@icef.com&quot;,
  CreatedDate  &lt;&gt;  LastModifiedDate 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account has been entered and must be assigned</fullName>
        <actions>
            <name>New_Account_has_been_entered_and_Territory_Manager_is_informed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An Account has been entered and the appropriate Sales Territory Manager is informed about that.</description>
        <formula>AND (
 OwnerId &lt;&gt;&quot;005200000013vSO&quot;,
 OwnerId &lt;&gt;&quot;005200000015Z3C&quot;,
 OwnerId &lt;&gt;&quot;005200000015Z37&quot;,
 OwnerId &lt;&gt;&quot;00520000001owvc&quot;,
OR (
RecordTypeId =&quot;012200000005EEc&quot;,
RecordTypeId =&quot;012200000005D7R&quot;,
RecordTypeId =&quot;012200000005Eis&quot;,
RecordTypeId =&quot;012200000001Um2&quot;
)
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account with CF school is deactivated</fullName>
        <actions>
            <name>Account_with_CF_school_has_been_deactivated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>equals</operation>
            <value>Out of service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Number_of_CF_Schools__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>OBSOLETE An Account with at least one CF school has been set to &apos;Out of service&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Agency has booked more than 3 WSs in this year</fullName>
        <actions>
            <name>Agency_has_booked_more_than_3_WSs_in_this_year</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ICEF_events_visited_2012__c</field>
            <operation>greaterThan</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Association Type has been changed</fullName>
        <actions>
            <name>Association_Type_has_been_changed_and_Sys_Admins_are_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Assoc. Type has been changed and an alert email is sent to Markus.</description>
        <formula>ISCHANGED ( Association_Type__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autofill Invoice Email</fullName>
        <actions>
            <name>Update_Invoice_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.c2g__CODAInvoiceEmail__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Autofills invoice Email when the Finance Contact has been specified</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CF Free Trial start has been set</fullName>
        <actions>
            <name>Add_CF_credit_to_set_CF_balance_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_CF_Free_Trial_end_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Datefield &apos;CF Free Trial&apos; has been filled out</description>
        <formula>AND(
NOT ( ISBLANK ( CF_Free_Trial__c )),
ISBLANK( CF_payment_Account__c )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_CF_Account_Type_to_Free</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.CF_Free_Trial__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CF Payment Account has been set</fullName>
        <actions>
            <name>Clean_CF_Account_Balance</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clean_CF_Trial_end</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clean_CF_Trial_start</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clean_CF_used_up_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_CF_account_type_to_Child</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>A CF payment account has been set and CF account type is set to Child and Free Trial date fields are emptied</description>
        <formula>NOT(ISBLANK( CF_payment_Account__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CF Trial dates have been deleted</fullName>
        <actions>
            <name>Set_CF_Account_Type_to_Paid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Datefields &apos;CF Trial end&apos; and &apos;CF Trial start&apos; have been deleted and CF Account Type is set to Paid Account</description>
        <formula>AND (
ISBLANK ( CF_Free_Trial_end__c ),
ISBLANK ( CF_Free_Trial__c ),
ISBLANK ( CF_payment_Account__c )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CF Trial end date has been reached</fullName>
        <active>true</active>
        <description>Date of Datefield &apos;CF Trial end&apos; has been reached: Datefieldds are cleaned, Owner is alerted and CF Account Type is set to Paid Account</description>
        <formula>NOT ( ISBLANK ( CF_Free_Trial_end__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CF_Free_Trial_has_run_out</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Clean_CF_Trial_end</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Clean_CF_Trial_start</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_CF_Account_Type_to_Paid</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.CF_Free_Trial_end__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Facebook starts with http%3A%2F%2F</fullName>
        <actions>
            <name>Facebook_starts_with_http</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Facebook__c</field>
            <operation>startsWith</operation>
            <value>http://</value>
        </criteriaItems>
        <description>Facebook address starts with http:// and is cut.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Facebook starts with https%3A%2F%2F</fullName>
        <actions>
            <name>Facebook_starts_with_https</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Facebook__c</field>
            <operation>startsWith</operation>
            <value>https://</value>
        </criteriaItems>
        <description>Facebook address starts with https:// and is cut.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Geo Code address has changed</fullName>
        <actions>
            <name>Field_Mapping_Status_set_to_Not_Located</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>clear_Latitude</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>clear_Longitude</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When address details for geo coding have changed, update mapping status to &quot;Not Located Yet&quot;</description>
        <formula>OR(
  AND(
     ISPICKVAL(Which_Address__c, &quot;Mailing&quot;),
     OR(
        ISCHANGED( Mailing_street__c ),
        ISCHANGED( Mailing_Post_Code__c ) ,
        ISCHANGED( Mailing_City__c ) ,
        ISCHANGED( Mailing_State_Province__c ),
        ISCHANGED( Mailing_Country__c )   
     )
  ),

  AND(
     ISPICKVAL(Which_Address__c, &quot;Billing&quot;),
     OR(
        ISCHANGED(  BillingStreet  ),
        ISCHANGED(  BillingPostalCode  ) ,
        ISCHANGED(  BillingCity  ) ,
        ISCHANGED(  BillingState  ),
        ISCHANGED(  BillingCountry  )   
     )
  )

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IAS Number has been filled out</fullName>
        <actions>
            <name>IAS_Start_year_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IAS_renewal_date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IAS_sent_update_value</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IAS_year_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.IAS_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This Account has been provided with an IAS number.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IAS publishing info has been changed</fullName>
        <actions>
            <name>IAS_Logo_publish_info_date_is_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The info about publishing of the IAS Logo on the client&apos;s side has been changed in SF.</description>
        <formula>OR (
ISCHANGED (  IAS_Logo_published_Media__c  ),
ISCHANGED (  IAS_logo_published_Comments__c  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Invoice address update</fullName>
        <actions>
            <name>Invoice_address_Zip</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Invoice_address_city</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Invoice_address_country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Invoice_address_state</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Invoice_address_street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCity</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingStreet</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingPostalCode</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If Invoice address is empty, then it is filled with the values of the Mailing address.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Level of Interest update</fullName>
        <actions>
            <name>Level_of_interest_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>equals</operation>
            <value>Out of service</value>
        </criteriaItems>
        <description>When Status of an Account is &apos;Out of service&apos; the level of interest is amended accordingly.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OW account is about to expire</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Association,Work and Travel,Service Provider,Educator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>equals</operation>
            <value>Active,Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Most_Recent_ICEF_Event__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>The most recent ICEf event lies 10 months in the past. In two months the access to the OW will expire.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Account.Most_Recent_ICEF_Event__c</offsetFromField>
            <timeLength>303</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Organisation description has been filled out</fullName>
        <actions>
            <name>Organisation_Description_has_been_filled_out_via_webform</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An Agency has filled out the webform &apos;Organisation description&apos; and the appropriate ARM is alerted.</description>
        <formula>ISCHANGED (  Description_webform_filled_out__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PDM has become zero</fullName>
        <actions>
            <name>Account_has_no_PDM_contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Number_of_PMD_Contacts__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Work and Travel,Educator,Agent,Association,Service Provider</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>notEqual</operation>
            <value>Out of service,Created by FF due to technical reasons</value>
        </criteriaItems>
        <description>&apos;Number of PDM contacts&apos; in Account has become 0 and an alert to the owner is sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Address Countries by Lookups</fullName>
        <actions>
            <name>Fill_Building_Country_by_Lookup</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fill_Invoice_Country_by_Lookup</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fill_Mailing_Country_by_Lookup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR (
MailingCountry__c  &lt;&gt;  Mailing_Country__r.Name,
Building_Country__r.Name &lt;&gt;  Building_Country__c,
Invoice_Country__r.Name &lt;&gt;  Invoice_Country__c 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status update</fullName>
        <actions>
            <name>Status_details_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Level_of_Interest_ICEF_Workshops__c</field>
            <operation>equals</operation>
            <value>Not interested in agents</value>
        </criteriaItems>
        <description>When field &apos;Level of Interest&apos; is set to &apos;Not interested in agents&apos;, then fields &apos;Status&apos; and &apos;Status details&apos; are amended accordingly.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status update oos</fullName>
        <actions>
            <name>Status_details_update_ceased_business</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_update_oos</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Level_of_Interest_ICEF_Workshops__c</field>
            <operation>equals</operation>
            <value>Company doesn&apos;t exist any more</value>
        </criteriaItems>
        <description>When field &apos;Level of Interest&apos; is set to &apos;Company doesn&apos;t exist any more&apos;, then fields &apos;Status&apos; and &apos;Status details&apos; are amended accordingly.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Students sent figures have been filled out</fullName>
        <actions>
            <name>Students_sent_number_fields_have_been_filled_out</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An Agency has filled out the webform &apos;Students sent&apos; and the appropriate ARM is alerted.</description>
        <formula>ISCHANGED (  Students_sent_webform_filled_out__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Students sent update AUS%2FNZ</fullName>
        <actions>
            <name>Students_sent_update_AUS_NZ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>A change in one of the Fields in Section &quot;Student sent in the last 12 months (ANZA and NAW)&quot; is made for AUS/NZ figures.</description>
        <formula>OR (
  ISCHANGED ( Students_to_language_schools_AUS__c ),
  ISCHANGED ( Students_to_language_schools_NZ__c ),
  ISCHANGED ( Students_to_secondary_AUS__c ),
  ISCHANGED ( Students_to_secondary_NZ__c ),
  ISCHANGED ( Students_to_vocational_AUS__c ),
  ISCHANGED ( Students_to_vocational_NZ__c ),
  ISCHANGED ( Students_to_undergrad_AUS__c ),
  ISCHANGED ( Students_to_undergrad_NZ__c ),
  ISCHANGED ( Students_to_grad_certificates_AUS__c ),
  ISCHANGED ( Students_to_grad_certificates_NZ__c ),
  ISCHANGED ( Students_to_grad_Masters_AUS__c ),
  ISCHANGED ( Students_to_grad_Masters_NZ__c ),
  ISCHANGED ( Students_to_grad_MBA_AUS__c ),
  ISCHANGED ( Students_to_grad_MBA_NZ__c ),
  ISCHANGED ( Students_to_grad_Doc_AUS__c ),
  ISCHANGED ( Students_to_grad_Doc_NZ__c ),
  ISCHANGED ( Students_to_other_schools_AUS__c ),
  ISCHANGED ( Students_to_other_schools_NZ__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Students sent update International</fullName>
        <actions>
            <name>Students_sent_info_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>A change in one of the Fields in Section &quot;Student sent in the last 12 months (international and regional events)&quot; is made.</description>
        <formula>OR (
  ISCHANGED ( Students_to_language_schools__c ),
  ISCHANGED ( Students_to_secondary__c ),
  ISCHANGED ( Students_to_vocational__c ),
  ISCHANGED ( Students_to_undergrad__c ),
  ISCHANGED ( Students_to_grad_certificates__c ),
  ISCHANGED ( Students_to_grad_Masters__c ),
  ISCHANGED ( Students_to_grad_MBA__c ),
  ISCHANGED ( Students_to_grad_Doc__c ),
  ISCHANGED ( Students_to_other_schools__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Students sent update USA%2FCAN</fullName>
        <actions>
            <name>Students_sent_update_USA_CAN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>A change in one of the Fields in Section &quot;Student sent in the last 12 months (ANZA and NAW)&quot; is made for USA/CAN figures.</description>
        <formula>OR (
  ISCHANGED ( Students_to_language_schools_USA__c ),
  ISCHANGED ( Students_to_language_schools_CAN__c ),
  ISCHANGED ( Students_to_secondary_USA__c ),
  ISCHANGED ( Students_to_secondary_CAN__c ),
  ISCHANGED ( Students_to_vocational_USA__c ),
  ISCHANGED ( Students_to_vocational_CAN__c ),
  ISCHANGED ( Students_to_undergrad_USA__c ),
  ISCHANGED ( Students_to_undergrad_CAN__c ),
  ISCHANGED ( Students_to_grad_certificates_USA__c ),
  ISCHANGED ( Students_to_grad_certificates_CAN__c ),
  ISCHANGED ( Students_to_grad_Masters_USA__c ),
  ISCHANGED ( Students_to_grad_Masters_CAN__c ),
  ISCHANGED ( Students_to_grad_MBA_USA__c ),
  ISCHANGED ( Students_to_grad_MBA_CAN__c ),
  ISCHANGED ( Students_to_grad_Doc_USA__c ),
  ISCHANGED ( Students_to_grad_Doc_CAN__c ),
  ISCHANGED ( Students_to_other_schools_USA__c ),
  ISCHANGED ( Students_to_other_schools_CAN__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sunhee has made changes to her account</fullName>
        <actions>
            <name>Sunhee_has_made_changed_and_Tiff_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>equals</operation>
            <value>Kang</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>Sunhee Kang</value>
        </criteriaItems>
        <description>Sunhee has made changes to one of her accounts and Tiff is informed.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Territory Managers email update</fullName>
        <actions>
            <name>Territory_Manager_emailaddress_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The emailadress of the Sales Territory Manager is copied to Account if different to the Sales Territory manager&apos;s emailaddress.</description>
        <formula>Territory_Manager_emailaddress__c &lt;&gt;  Mailing_Country__r.Sales_Territory_Manager__r.Email</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Twitter starts with http%3A%2F%2F</fullName>
        <actions>
            <name>Twitter_starts_with_http</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Twitter__c</field>
            <operation>startsWith</operation>
            <value>http://</value>
        </criteriaItems>
        <description>Twitter address starts with http:// and is cut.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Twitter starts with https%3A%2F%2F</fullName>
        <actions>
            <name>Twitter_starts_with_https</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Twitter__c</field>
            <operation>startsWith</operation>
            <value>https://</value>
        </criteriaItems>
        <description>Twitter address starts with https:// and is cut.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Website 1 account update</fullName>
        <actions>
            <name>Website_1_account_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The URL starts with http://www and starts neither with www. nor with https:// nor with http://</description>
        <formula>AND (
OR (
  NOT (
    OR (
      BEGINS (LOWER (Website) , &quot;www.&quot; ),
      BEGINS (LOWER (Website), &quot;http://&quot;),
      BEGINS (LOWER (Website), &quot;https://&quot;)
    )
  ),
  BEGINS (LOWER (Website), &quot;http://www.&quot;)
),
NOT (ISBLANK (Website))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Website 2 account update</fullName>
        <actions>
            <name>Website_2_account_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The URL starts with http://www and starts neither with www. nor with https:// nor with http://</description>
        <formula>AND (
OR (
  NOT (
    OR (
      BEGINS (LOWER (Website_2__c) , &quot;www.&quot; ),
      BEGINS (LOWER (Website_2__c) , &quot;http://&quot;),
      BEGINS (LOWER (Website_2__c) , &quot;https://&quot;)
    )
  ),
  BEGINS (LOWER (Website_2__c) , &quot;http://www.&quot;)
),
NOT (ISBLANK (Website_2__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>create IAS Membership</fullName>
        <actions>
            <name>alert_to_create_IAS_membership</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>OBSOLETE email alert to account owner for creating a new membership when account receives an IAS number</description>
        <formula>AND(
NOT (ISBLANK( IAS_Number__c ) ),
ISBLANK(PRIORVALUE(IAS_Number__c) ),
$User.LastName &lt;&gt; &quot;Hebeler&quot;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>reallocate Audreys inactive account to Markus Hebeler</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>Audrey Ruchet Bach</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <description>When an account with account owner Audrey Ruchet-Bach is recieves status inactive it is allocated to Markus Hebeler an hour later.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>change_Account_Owner_to_Markus_Hebeler</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>First_Timer</fullName>
        <assignedToType>owner</assignedToType>
        <description>A First Timer has booked. Please check that they have booked the appropriate workshop for their needs. Consider using this opportunity to call them and discuss their needs if these are not clear.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Account.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>First Timer</subject>
    </tasks>
</Workflow>
