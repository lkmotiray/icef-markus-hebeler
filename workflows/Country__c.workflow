<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Country_Name_has_been_changed_Alert_to_Markus</fullName>
        <description>Country Name has been changed - Alert to Markus</description>
        <protected>false</protected>
        <recipients>
            <recipient>ManagerDatabaseDepartment</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Country_Name_has_been_changed</template>
    </alerts>
    <rules>
        <fullName>Country name has been changed</fullName>
        <actions>
            <name>Country_Name_has_been_changed_Alert_to_Markus</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Name )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
