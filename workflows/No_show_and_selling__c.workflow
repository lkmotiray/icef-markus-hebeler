<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Fill_Uniqueness_Verifier</fullName>
        <description>When a no-show is created then a unique field is filled with info about reporting/reported AP and time. Thus creation of duplicates will be prevented.</description>
        <field>Uniqueness_verifier__c</field>
        <formula>Reported_AP__r.Id &amp;  
Reporting_AP__r.Id &amp;  
TEXT(Appointment_started__c )</formula>
        <name>Fill Uniqueness Verifier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Prevent dupliacte no-shows</fullName>
        <actions>
            <name>Fill_Uniqueness_Verifier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a no-show is created then a unique field is filled with info about reporting/reported AP and time. Thus creation of duplicates will be prevented.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
