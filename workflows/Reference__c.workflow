<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Reference_entered</fullName>
        <description>New Reference entered</description>
        <protected>false</protected>
        <recipients>
            <field>Receiver_Account_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>mhebeler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Reference_Emails/New_Reference_has_been_entered_via_webform</template>
    </alerts>
    <alerts>
        <fullName>Ref_checks_not_welcome</fullName>
        <description>Ref checks not welcome</description>
        <protected>false</protected>
        <recipients>
            <field>Receiver_Account_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>mhebeler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Reference_Emails/Ref_checks_not_welcome</template>
    </alerts>
    <alerts>
        <fullName>Reference_Request_to_Educator</fullName>
        <ccEmails>references@icef.com</ccEmails>
        <description>Reference Request to Educator</description>
        <protected>false</protected>
        <recipients>
            <field>Reference_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>references@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Reference_Emails/Reference_request_to_Educator</template>
    </alerts>
    <alerts>
        <fullName>Reference_has_been_closed_without_success_and_Account_Owner_is_alerted</fullName>
        <description>Reference has been closed without success and Account Owner is alerted</description>
        <protected>false</protected>
        <recipients>
            <field>Receiver_Account_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>mhebeler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ARM_Alerts/Reference_has_been_closed_withour_or_with_negative_result</template>
    </alerts>
    <alerts>
        <fullName>Reference_has_been_successfully_closed_and_Account_Owner_is_alerted</fullName>
        <description>Reference has been successfully closed and Account Owner is alerted</description>
        <protected>false</protected>
        <recipients>
            <field>Receiver_Account_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>mhebeler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Reference_Emails/Reference_has_been_completed_and_accepted</template>
    </alerts>
    <fieldUpdates>
        <fullName>No_reply_from_Giver_close_text</fullName>
        <description>When a Reference is closed without result due to no reply from client, then an appropriate text is written.</description>
        <field>Recommendation_text__c</field>
        <formula>Recommendation_text__c &amp; &quot;No reply from Giver Contact: Closed without result&quot;&amp; BR() &amp; 
TEXT(DAY (TODAY()))&amp;&quot;.&quot;&amp;TEXT (MONTH (TODAY()))&amp;&quot;.&quot;&amp;TEXT(YEAR (TODAY()))</formula>
        <name>No reply from Giver close text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rec_text_update_no_ref_checks_welcome</fullName>
        <description>When the Giver Account is not willing to give references then there is a remark in Recommendation text.</description>
        <field>Recommendation_text__c</field>
        <formula>&quot;No ref check request wanted. Closed without result&quot; &amp; BR() &amp;
TEXT(DAY (TODAY()))&amp;&quot;.&quot;&amp;TEXT (MONTH (TODAY()))&amp;&quot;.&quot;&amp;TEXT(YEAR (TODAY()))</formula>
        <name>Rec text update no ref checks welcome</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Receiver_Account_Owner_Email_update</fullName>
        <description>! Updates with ARM emailaddress, not Owner email!</description>
        <field>Receiver_Account_Owner_Email__c</field>
        <formula>Receiving_Account__r.ARM__r.Email</formula>
        <name>Receiver Account Owner Email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recommendation_text_checkbox</fullName>
        <description>Marks the field &apos;recommendation text filled out?&apos; when there is any entry in the textfield &apos;Recommendation text&apos;.</description>
        <field>Recommendation_text_filled_out__c</field>
        <literalValue>1</literalValue>
        <name>Recommendation text checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recommendation_text_checkbox_uncheck</fullName>
        <field>Recommendation_text_filled_out__c</field>
        <literalValue>0</literalValue>
        <name>Recommendation text checkbox uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reference_Email_update</fullName>
        <field>Reference_Email__c</field>
        <formula>Ref_Check_Email__c</formula>
        <name>Reference Email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Date_update</fullName>
        <field>Request_Date__c</field>
        <formula>Today ()</formula>
        <name>Request Date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Number_update_FIRST</fullName>
        <field>Request_Number__c</field>
        <literalValue>First</literalValue>
        <name>Request Number update - FIRST</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_update_closed_without_result</fullName>
        <field>Status__c</field>
        <literalValue>closed without result</literalValue>
        <name>Status update closed without result</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_update_verification</fullName>
        <field>Status__c</field>
        <literalValue>in verification</literalValue>
        <name>Status update verification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Every time a reference is edited</fullName>
        <actions>
            <name>Reference_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Reference Alert to Account Owner</fullName>
        <actions>
            <name>New_Reference_entered</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2</booleanFilter>
        <criteriaItems>
            <field>Reference__c.Giver_Account_Survey_text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Reference__c.Giver_Contact_Survey_text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>The Owner of the recieving account is informed when a reference has been created via web form.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Receiving Account Owner Email update</fullName>
        <actions>
            <name>Receiver_Account_Owner_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Recommendation text checkbox</fullName>
        <actions>
            <name>Recommendation_text_checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Reference__c.Recommendation_text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>The checkbox &apos;Recommendation Text filled out?&apos; is checked when there is any entry in field &apos;Recommendation text&apos;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Recommendation text checkbox unchecked</fullName>
        <actions>
            <name>Recommendation_text_checkbox_uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Reference__c.Recommendation_text__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>The checkbox &apos;Recommendation Text filled out?&apos; is unchecked when there is no entry in field &apos;Recommendation text&apos;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ref checks not welcome - Owner is alerted</fullName>
        <actions>
            <name>Ref_checks_not_welcome</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Rec_text_update_no_ref_checks_welcome</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_update_closed_without_result</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Emailalert to ARM if for a newly entered Reference the educator has indicated that Ref checks are not welcome.</description>
        <formula>AND (
ref_checks_welcome__c  = &quot;No&quot;,
 Exceptionally_accept_Ref_request__c = FALSE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reference closed without result - Rec text update</fullName>
        <actions>
            <name>No_reply_from_Giver_close_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Giver contact did not reply four times and the reference is closed without result, then the Recommendation text is updated accordingly.</description>
        <formula>AND ( 
ISPICKVAL (Status__c , &quot;closed without result&quot;),
ISPICKVAL (Request_Number__c, &quot;Fourth&quot;), 
ISBLANK (Recommendation_text__c), 
OR (
  ref_checks_welcome__c = &quot;Yes&quot;,
   Exceptionally_accept_Ref_request__c = TRUE
  ) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reference has been closed without success</fullName>
        <actions>
            <name>Reference_has_been_closed_without_success_and_Account_Owner_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>2 or (1 and 3)</booleanFilter>
        <criteriaItems>
            <field>Reference__c.Rating__c</field>
            <operation>equals</operation>
            <value>non committal,negative</value>
        </criteriaItems>
        <criteriaItems>
            <field>Reference__c.Status__c</field>
            <operation>equals</operation>
            <value>closed without result</value>
        </criteriaItems>
        <criteriaItems>
            <field>Reference__c.Status__c</field>
            <operation>equals</operation>
            <value>closed with result</value>
        </criteriaItems>
        <description>A Reference has been closed without result or with a negative/non-committal rating.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reference has been successfully closed</fullName>
        <actions>
            <name>Reference_has_been_successfully_closed_and_Account_Owner_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Reference__c.Status__c</field>
            <operation>equals</operation>
            <value>closed with result</value>
        </criteriaItems>
        <criteriaItems>
            <field>Reference__c.Rating__c</field>
            <operation>equals</operation>
            <value>positive</value>
        </criteriaItems>
        <description>The reference has been accepted and successfiully closed with positive result.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reference request to Educator - First</fullName>
        <active>true</active>
        <description>Autoemailer to Educator, asking for reference confirmation for this particular agency.</description>
        <formula>AND (
OR (ISPICKVAL (Status__c , &quot;open&quot;), ISPICKVAL ( Status__c , &quot;in verification&quot;)),
ISPICKVAL (Request_Number__c, &quot;&quot;),
NOT (ISBLANK ( Reference_Email__c )),
NOT (ISBLANK (  Giver_Contact__c )),
ISBLANK (Recommendation_text__c),
OR (ref_checks_welcome__c  = &quot;Yes&quot;, Exceptionally_accept_Ref_request__c = true)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reference_Request_to_Educator</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Request_Date_update</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Request_Number_update_FIRST</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Status_update_verification</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Reference request to Educator - Fourth</fullName>
        <actions>
            <name>Reference_Request_to_Educator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Request_Date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Autoemailer to Educator, asking for reference confirmation for this particular agency.</description>
        <formula>AND (
OR (ISPICKVAL (Status__c , &quot;open&quot;), ISPICKVAL ( Status__c , &quot;in verification&quot;)),
ISPICKVAL (Request_Number__c, &quot;Fourth&quot;),
ISBLANK (Recommendation_text__c),
OR (ref_checks_welcome__c  = &quot;Yes&quot;, Exceptionally_accept_Ref_request__c = true)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reference request to Educator - Second</fullName>
        <actions>
            <name>Reference_Request_to_Educator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Request_Date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Autoemailer to Educator, asking for reference confirmation for this particular agency.</description>
        <formula>AND (
OR (ISPICKVAL (Status__c , &quot;open&quot;), ISPICKVAL ( Status__c , &quot;in verification&quot;)),
ISPICKVAL (Request_Number__c, &quot;Second&quot;),
ISBLANK (Recommendation_text__c),
OR (ref_checks_welcome__c  = &quot;Yes&quot;, Exceptionally_accept_Ref_request__c = true)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reference request to Educator - Third</fullName>
        <actions>
            <name>Reference_Request_to_Educator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Request_Date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Autoemailer to Educator, asking for reference confirmation for this particular agency.</description>
        <formula>AND (
OR (ISPICKVAL (Status__c , &quot;open&quot;), ISPICKVAL ( Status__c , &quot;in verification&quot;)),
ISPICKVAL (Request_Number__c, &quot;Third&quot;),
ISBLANK (Recommendation_text__c),
OR (ref_checks_welcome__c  = &quot;Yes&quot;, Exceptionally_accept_Ref_request__c = true)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
