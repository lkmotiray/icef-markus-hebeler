<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_to_new_case_assignee</fullName>
        <description>Alert to new case assignee</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cases/New_Case_Alert</template>
    </alerts>
    <alerts>
        <fullName>Case_open_for_48_hourse_alert</fullName>
        <description>Case open for 48 hourse alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cases/Case_open_for_48_hours_Alert</template>
    </alerts>
    <alerts>
        <fullName>New_CF_feedback_account_owner_is_alerted</fullName>
        <description>New CF feedback - account owner is alerted</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>rholmes@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Coursefinder_Communication/CF_feedback_alert_to_Account_Owner</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Alert</fullName>
        <description>New Case Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cases/New_Case_Alert</template>
    </alerts>
    <alerts>
        <fullName>New_Case_for_Internal_Support</fullName>
        <description>New Case for Internal Support</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cases/New_Case_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Recursive_field_update</fullName>
        <field>Recursive_Field__c</field>
        <literalValue>0</literalValue>
        <name>Recursive field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Case open for 48 hours - alert owner</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_open_for_48_hourse_alert</name>
                <type>Alert</type>
            </actions>
            <timeLength>48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Fill Contact Email with web Email</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If we have a web emailaddress, but no Contact Email, then the value is copied from webemail to Contact email</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New CF feedback alert to Account owner</fullName>
        <actions>
            <name>New_CF_feedback_account_owner_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CF contact us,CF Feedback</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>A CF client has given feedback or contacted us and the account owner is alerted about this.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Case Alert to Internal Support</fullName>
        <actions>
            <name>New_Case_for_Internal_Support</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Internal support</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Recursive field rule</fullName>
        <actions>
            <name>Recursive_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Recursive_Field__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow rule is used to update the field so that a trigger will get fired to update the Case Owner field.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Case_Open_for_3_days</fullName>
        <assignedToType>owner</assignedToType>
        <description>You have a Case that is more than 3 days old - please go to My Open Cases in the Case tab and check that this Case is in progress i.e. that you have contacted the person concerned and that you are following.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Case Open for &gt; 3 days</subject>
    </tasks>
</Workflow>
