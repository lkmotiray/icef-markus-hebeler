<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ACT_has_been_changed_and_ARM_is_alerted</fullName>
        <description>ACT has been changed and ARM is alerted</description>
        <protected>false</protected>
        <recipients>
            <field>ARM_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/ACT_number_has_been_changed</template>
    </alerts>
    <fieldUpdates>
        <fullName>ARM_emailaddress_update</fullName>
        <description>The emailaddress of the Country&apos;s ARM is updated in the ACT</description>
        <field>ARM_emailaddress__c</field>
        <formula>Country__r.Agent_Relationship_Manager__r.Email</formula>
        <name>ARM emailaddress update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>ACT is changed</fullName>
        <actions>
            <name>ACT_has_been_changed_and_ARM_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When an ACT is created or changed then the ARM must be informed about that.</description>
        <formula>NOT (ISBLANK ( Agent_Target__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACT is created or edited</fullName>
        <actions>
            <name>ARM_emailaddress_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
