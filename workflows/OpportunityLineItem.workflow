<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_ARM_when_product_of_closed_won_Opp_has_been_changed_or_added</fullName>
        <description>Alert ARM when product of closed won Opp has been changed or added</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Closed_won_Opp_Product_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Alert_EMT_when_a_table_booth_for_a_very_close_WS_has_been_booked</fullName>
        <ccEmails>accounts@icef.com</ccEmails>
        <description>Alert EMT when a table/booth for a very close WS has been booked</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Manager_Provider_Emails__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Product_of_close_WS_has_been_booked</template>
    </alerts>
    <alerts>
        <fullName>Alert_EMT_when_a_table_booth_for_an_Event_with_open_has_been_booked</fullName>
        <ccEmails>accounts@icef.com</ccEmails>
        <description>Alert EMT when a table/booth for an Event with open ESP has been booked</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Manager_Provider_Emails__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMT_alerts/Product_of_event_with_ESP_open_has_been_booked</template>
    </alerts>
    <alerts>
        <fullName>Alert_EMT_when_product_of_closed_won_Opp_has_been_changed_or_added</fullName>
        <description>Alert EMT when product of closed won Opp has been changed or added</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Manager_Provider_Emails__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Closed_won_Opp_Product_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Alert_to_provisioning_re_ICF</fullName>
        <description>Alert to provisioning re ICF</description>
        <protected>false</protected>
        <recipients>
            <recipient>hkreiner@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>james.love@pobox.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lstevens@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Alerts_provisioning_for_Online_Product_sale</template>
    </alerts>
    <alerts>
        <fullName>Alerts_ICF_team_for_provisioning</fullName>
        <description>Alerts ICF team for provisioning</description>
        <protected>false</protected>
        <recipients>
            <recipient>hkreiner@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>james.love@pobox.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lstevens@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Alerts_provisioning_for_Online_Product_sale</template>
    </alerts>
    <alerts>
        <fullName>Alerts_Rod_when_there_is_an_Enquiry_Genaration_sale_for_Hyperstudy</fullName>
        <description>Alerts Rod when there is an Enquiry Genaration sale for Hyperstudy</description>
        <protected>false</protected>
        <recipients>
            <recipient>james.love@pobox.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rhearps@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Alerts_provisioning_for_Online_Product_sale</template>
    </alerts>
    <alerts>
        <fullName>Alerts_Sunhee_and_Rod_for_IOW_provisioning</fullName>
        <description>Alerts Sunhee and Rod for IOW provisioning</description>
        <protected>false</protected>
        <recipients>
            <recipient>james.love@pobox.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rhearps@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>skang@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Alerts_provisioning_for_Online_Product_sale</template>
    </alerts>
    <alerts>
        <fullName>EMT_is_alerted_when_a_product_of_a_closed_won_Opp_is_changed_or_added</fullName>
        <description>EMT is alerted when a product of a closed won Opp is changed or added</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Manager_Provider_Emails__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Closed_won_Opportunity_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Event_overbooked_alert</fullName>
        <description>Event overbooked alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Event_tables_overbooked_alert</template>
    </alerts>
    <alerts>
        <fullName>ICEF_insights_ad_sold</fullName>
        <description>ICEF insights ad sold</description>
        <protected>false</protected>
        <recipients>
            <recipient>VPSalesandMarketing</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/ICEF_Insights_Opp_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>Opp_for_Monitor_ad_weekly_digest_has_been_sold</fullName>
        <description>Opp for Monitor ad (weekly digest) has been sold</description>
        <protected>false</protected>
        <recipients>
            <recipient>lstevens@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Opp_for_Monitor_Ads_closed_won</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_for_Coursefinder_Product_closed_won</fullName>
        <ccEmails>internal@coursefinders.com</ccEmails>
        <description>Opportunity for Coursefinder Product closed won</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/Opp_for_Coursefinder_closed_won</template>
    </alerts>
    <fieldUpdates>
        <fullName>Product_Name_update</fullName>
        <description>Field &apos;Product Name&apos; is willed with the name of the appropriate product.</description>
        <field>Product_Name__c</field>
        <formula>Product2.Name</formula>
        <name>Product Name update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_rate_by_logic</fullName>
        <field>Given_Rate__c</field>
        <formula>IF(
  ISPICKVAL(Rate__c, &quot;Standard&quot;),
  IF(
    ICEF_Event__r.Returnee_Rate_ends__c &gt;= Today(),
    &quot;Returnee&quot;,
    IF(
      ICEF_Event__r.Early_Bird_Rate_ends__c &gt;= Today(),
      &quot;Early Bird&quot;,
      IF(
        ICEF_Event__r.Mid_Rate_ends__c &gt;= Today(),
        &quot;Mid&quot;,
        &quot;Regular&quot;
      )
    )
  ),
  TEXT(Rate__c)
)</formula>
        <name>Set rate by logic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_salesprice_by_logic</fullName>
        <field>UnitPrice</field>
        <formula>CASE(IF(
AND(ISPICKVAL(Rate__c, &quot;Multi Booking&quot;),
OR(NOT OR(ISNULL(Product2.Multi_Booking__c),
OR(CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(NOT ISNULL(Product2.Multi_Booking_for_Medium__c),CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),
AND(NOT ISNULL(Product2.Multi_Booking_for_High__c),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Multi Booking&quot;,
IF(AND(ISPICKVAL(Rate__c, &quot;Returnee&quot;),OR(NOT OR(ISNULL(Product2.Returnee__c),OR(
CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(NOT ISNULL(Product2.Returnee_for_Medium__c),CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),
AND(NOT ISNULL(Product2.Returnee_for_High__c),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Returnee&quot;,
IF(
AND(ISPICKVAL(Rate__c, &quot;Early Bird&quot;),OR(NOT OR(ISNULL(Product2.Early_Bird__c),OR(CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(NOT ISNULL(Product2.Early_Bird_for_Medium__c),CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),AND(NOT ISNULL(Product2.Early_Bird_for_High__c),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Early Bird&quot;,
IF(
AND(ISPICKVAL(Rate__c, &quot;Mid&quot;),OR(NOT OR(ISNULL(Product2.Mid__c),OR(CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(NOT ISNULL(Product2.Mid_for_Medium__c),CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),AND(NOT ISNULL(Product2.Mid_for_High__c),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Mid&quot;,
IF(
AND(ISPICKVAL(Rate__c, &quot;Regular&quot;),OR(NOT OR(ISNULL(Product2.Regular__c),OR(CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(NOT ISNULL(Product2.Regular_for_Medium__c),CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),
AND(NOT ISNULL(Product2.Regular_for_High__c),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Regular&quot;,
IF(
ISPICKVAL(Rate__c, &quot;Standard&quot;),
IF(
AND(ICEF_Event__r.Early_Bird_Rate_ends__c &gt;= Today(),OR(NOT OR(ISNULL(Product2.Early_Bird__c),
OR(CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(
NOT ISNULL(Product2.Early_Bird_for_Medium__c),
CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),
AND(
NOT ISNULL(Product2.Early_Bird_for_High__c),
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Early Bird&quot;,
IF(
AND(
ICEF_Event__r.Mid_Rate_ends__c &gt;= Today(),
OR(
NOT OR(
ISNULL(Product2.Mid__c),
OR(
CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(
NOT ISNULL(Product2.Mid_for_Medium__c),
CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),
AND(
NOT ISNULL(Product2.Mid_for_High__c),
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Mid&quot;,
&quot;Regular&quot;)),
&quot;Individual&quot;))))))
&amp;
IF(
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;),
&quot; High&quot;,
IF(
CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),
&quot; Medium&quot;,
&quot;&quot;)),
&quot;Multi Booking High&quot;,
Product2.Multi_Booking_for_High__c,
&quot;Multi Booking Medium&quot;,
Product2.Multi_Booking_for_Medium__c,
&quot;Multi Booking&quot;,
Product2.Multi_Booking__c,
&quot;Returnee Medium&quot;,
Product2.Returnee_for_Medium__c,
&quot;Early Bird High&quot;,
Product2.Early_Bird_for_High__c,
&quot;Early Bird Medium&quot;,
Product2.Early_Bird_for_Medium__c,
&quot;Early Bird&quot;,
Product2.Early_Bird__c,
&quot;Mid High&quot;,
Product2.Mid_for_High__c,
&quot;Mid Medium&quot;,
Product2.Mid_for_Medium__c,
&quot;Mid&quot;,
Product2.Mid__c,
&quot;Regular High&quot;,
Product2.Regular_for_High__c,
&quot;Regular Medium&quot;,
Product2.Regular_for_Medium__c,
&quot;Regular&quot;,
Product2.Regular__c,
UnitPrice
)</formula>
        <name>Set salesprice by logic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Renewal_Date</fullName>
        <field>Renewal_Date__c</field>
        <formula>Opportunity.CloseDate+375</formula>
        <name>Update Renewal Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>salesforce_bug_01</fullName>
        <field>UnitPrice</field>
        <formula>CASE(IF(
AND(ISPICKVAL(Rate__c, &quot;Multi Booking&quot;),
OR(NOT OR(ISNULL(Product2.Multi_Booking__c),
OR(CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(NOT ISNULL(Product2.Multi_Booking_for_Medium__c),CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),
AND(NOT ISNULL(Product2.Multi_Booking_for_High__c),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Multi Booking&quot;,
IF(AND(ISPICKVAL(Rate__c, &quot;Returnee&quot;),OR(NOT OR(ISNULL(Product2.Returnee__c),OR(
CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(NOT ISNULL(Product2.Returnee_for_Medium__c),CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),
AND(NOT ISNULL(Product2.Returnee_for_High__c),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Returnee&quot;,
IF(
AND(ISPICKVAL(Rate__c, &quot;Early Bird&quot;),OR(NOT OR(ISNULL(Product2.Early_Bird__c),OR(CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(NOT ISNULL(Product2.Early_Bird_for_Medium__c),CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),AND(NOT ISNULL(Product2.Early_Bird_for_High__c),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Early Bird&quot;,
IF(
AND(ISPICKVAL(Rate__c, &quot;Mid&quot;),OR(NOT OR(ISNULL(Product2.Mid__c),OR(CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(NOT ISNULL(Product2.Mid_for_Medium__c),CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),AND(NOT ISNULL(Product2.Mid_for_High__c),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Mid&quot;,
IF(
AND(ISPICKVAL(Rate__c, &quot;Regular&quot;),OR(NOT OR(ISNULL(Product2.Regular__c),OR(CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(NOT ISNULL(Product2.Regular_for_Medium__c),CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),
AND(NOT ISNULL(Product2.Regular_for_High__c),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Regular&quot;,
IF(
ISPICKVAL(Rate__c, &quot;Standard&quot;),
IF(
AND(ICEF_Event__r.Early_Bird_Rate_ends__c &gt;= Today(),OR(NOT OR(ISNULL(Product2.Early_Bird__c),
OR(CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(
NOT ISNULL(Product2.Early_Bird_for_Medium__c),
CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),
AND(
NOT ISNULL(Product2.Early_Bird_for_High__c),
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Early Bird&quot;,
IF(
AND(
ICEF_Event__r.Mid_Rate_ends__c &gt;= Today(),
OR(
NOT OR(
ISNULL(Product2.Mid__c),
OR(
CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;))),
AND(
NOT ISNULL(Product2.Mid_for_Medium__c),
CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;)),
AND(
NOT ISNULL(Product2.Mid_for_High__c),
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;)))),
&quot;Mid&quot;,
&quot;Regular&quot;)),
&quot;Individual&quot;))))))
&amp;
IF(
CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;),
&quot; High&quot;,
IF(
CONTAINS(ICEF_Event__r.Name, &quot;ANZA&quot;),
&quot; Medium&quot;,
&quot;&quot;)),
&quot;Multi Booking High&quot;,
Product2.Multi_Booking_for_High__c,
&quot;Multi Booking Medium&quot;,
Product2.Multi_Booking_for_Medium__c,
&quot;Multi Booking&quot;,
Product2.Multi_Booking__c,
&quot;Returnee High&quot;,
Product2.Returnee_for_High__c,
&quot;Returnee Medium&quot;,
Product2.Returnee_for_Medium__c,
&quot;Returnee&quot;,
Product2.Returnee__c,
&quot;Early Bird High&quot;,
Product2.Early_Bird_for_High__c,
&quot;Early Bird Medium&quot;,
Product2.Early_Bird_for_Medium__c,
&quot;Early Bird&quot;,
Product2.Early_Bird__c,
&quot;Mid High&quot;,
Product2.Mid_for_High__c,
&quot;Mid Medium&quot;,
Product2.Mid_for_Medium__c,
&quot;Mid&quot;,
Product2.Mid__c,
&quot;Regular High&quot;,
Product2.Regular_for_High__c,
&quot;Regular Medium&quot;,
Product2.Regular_for_Medium__c,
&quot;Regular&quot;,
{!ID:Product2.00N200</formula>
        <name>salesforce bug 01</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alerts provisioning for HS sale</fullName>
        <actions>
            <name>Alerts_Rod_when_there_is_an_Enquiry_Genaration_sale_for_Hyperstudy</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Family__c</field>
            <operation>contains</operation>
            <value>Enquiry Generation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Notifies Rod when there is a sale of Hyperstudy</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Alerts provisioning for ICF sale</fullName>
        <actions>
            <name>Alerts_ICF_team_for_provisioning</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Renewal_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Family__c</field>
            <operation>equals</operation>
            <value>Course Finder</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Notifies Harald and Lydia when an Opp Closed Won contains a Course Finder product</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Alerts provisioning for IOW sale</fullName>
        <actions>
            <name>Alerts_Sunhee_and_Rod_for_IOW_provisioning</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Family__c</field>
            <operation>equals</operation>
            <value>Online Workshop,Enquiry Generation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Notifies Sunhee and Rod when there is a sale of IOW</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event fully booked alert</fullName>
        <actions>
            <name>Event_overbooked_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>The tables of an event have been fully booked and a closed won has been created for this event)</description>
        <formula>AND (
  Event_Tables_fully_booked__c ,
  CONTAINS ( Product2.Name , &quot;1st Educator&quot; ),
  ISPICKVAL ( Opportunity.StageName , &quot;Closed Won&quot; )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event overbooked alert</fullName>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ICEF Insights ad sold</fullName>
        <actions>
            <name>ICEF_insights_ad_sold</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.Name</field>
            <operation>contains</operation>
            <value>Insights</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp for Coursefinder product closed won</fullName>
        <actions>
            <name>Opportunity_for_Coursefinder_Product_closed_won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Family__c</field>
            <operation>contains</operation>
            <value>Course Finder,CourseFinders</value>
        </criteriaItems>
        <description>An Opportunity for a Coursefinder Product is closed won. internal@coursefinders.com is informed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp for Monitor Ads closed won</fullName>
        <actions>
            <name>Opp_for_Monitor_ad_weekly_digest_has_been_sold</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Family__c</field>
            <operation>equals</operation>
            <value>ICEF Monitor Advertising - Weekly Digest</value>
        </criteriaItems>
        <description>An Opportunity for a Monitor Advertisement Product is closed won. Lidia is informed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Product Name update</fullName>
        <actions>
            <name>Product_Name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The name of the Opportunity Product&apos;s appropriate product is updated within the Opp prod. object.</description>
        <formula>Product_Name__c &lt;&gt; Product2.Name</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set rates by logic</fullName>
        <actions>
            <name>Set_rate_by_logic</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_salesprice_by_logic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
