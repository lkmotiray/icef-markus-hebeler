<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Student_Enquiry_CF_School_with_balance_less_than_10_and_no_CF_admin</fullName>
        <description>Student Enquiry: CF School with balance less than -10 and no CF admin</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rholmes@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Coursefinder_Communication/Student_Enquiry_No_CF_Admin_and_balance_below_10</template>
    </alerts>
    <alerts>
        <fullName>Student_Enquiry_CF_School_with_balance_less_than_10_but_CF_admin_receives_an_enq</fullName>
        <description>Student Enquiry: CF School with balance less than -10 but CF admin receives an enquiry</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rholmes@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Coursefinder_Communication/Student_Enquiry_CF_Admin_but_balance_below_10</template>
    </alerts>
    <alerts>
        <fullName>Student_Enquiry_CF_School_with_negative_balance_and_has_CF_admin</fullName>
        <description>Student Enquiry: CF School with negative balance and has CF admin</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rholmes@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Coursefinder_Communication/Student_Enquiry_CF_Admin_but_negative_balance</template>
    </alerts>
    <alerts>
        <fullName>Student_Enquiry_No_CF_Admin</fullName>
        <description>Student Enquiry: No CF Admin</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rholmes@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Coursefinder_Communication/Student_Enquiry_no_CF_Admin</template>
    </alerts>
    <fieldUpdates>
        <fullName>Untick_New_Enquiry</fullName>
        <field>New_Enquiry__c</field>
        <literalValue>0</literalValue>
        <name>Untick New Enquiry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Untick New Enquiry after 7 days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.New_Enquiry__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>sevent days after enquiry date, untick</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Untick_New_Enquiry</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Quote__c.Date_of_Request__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
