<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_contact_has_been_re_activated_and_had_been_Monitor_subscriber_before</fullName>
        <description>A contact has been re-activated and had been Monitor subscriber before</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahaytova@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eleyendecker@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_unsubscriber_has_been_reactivated</template>
    </alerts>
    <alerts>
        <fullName>Act_On_Agents_form_has_been_filled_and_owner_is_informed</fullName>
        <description>Act-On Agents form has been filled and owner is informed</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Act_On_form_for_Agents_has_been_filled_out</template>
    </alerts>
    <alerts>
        <fullName>CF_Administrator_tickbox_has_been_removed</fullName>
        <description>CF Administrator tickbox has been removed</description>
        <protected>false</protected>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/DB_update_Contact_is_no_longer_CF_admin</template>
    </alerts>
    <alerts>
        <fullName>CF_Administrator_tickbox_has_been_ticked</fullName>
        <ccEmails>internal@coursefinders.com</ccEmails>
        <description>CF Administrator tickbox has been ticked</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rholmes@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Coursefinder_Communication/Welcome_Email_R</template>
    </alerts>
    <alerts>
        <fullName>CF_Sales_ticked_automatically</fullName>
        <ccEmails>info@coursefinders.com</ccEmails>
        <description>CF Sales has been ticked automatically and Account owner is informed</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>rholmes@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Coursefinder_Communication/Contact_has_been_marked_as_CF_Sales</template>
    </alerts>
    <alerts>
        <fullName>CF_admin_account_has_chenaged_and_Ross_is_alerted</fullName>
        <ccEmails>internal@coursefinders.com</ccEmails>
        <description>CF admin account has chenaged and Ross is alerted</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/Account_of_CF_admin_has_changed</template>
    </alerts>
    <alerts>
        <fullName>CF_admin_belongs_to_contact_that_is_not_a_Beta_candidate</fullName>
        <description>CF admin belongs to contact that is not a Beta candidate</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/Contact_is_CF_admin_but_account_is_not_in_Beta</template>
    </alerts>
    <alerts>
        <fullName>Contact_name_has_been_changed_and_Laura_is_informed</fullName>
        <ccEmails>mhebeler@icef.com</ccEmails>
        <description>Contact name has been changed and Laura is informed</description>
        <protected>false</protected>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lott@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Name_of_a_registered_Contact_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Contact_of_an_upcoming_Workshop_has_left_the_company</fullName>
        <description>Contact of an upcoming Workshop has left the company</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Contact_of_an_upcoming_WS_has_been_set_to_Inactive</template>
    </alerts>
    <alerts>
        <fullName>Contact_owner_is_informed_about_MyICEF_account_request</fullName>
        <description>Contact owner is informed about MyICEF account request</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>tegler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ARM_Alerts/Client_has_requested_a_MyICEF_Account</template>
    </alerts>
    <alerts>
        <fullName>Emailaddress_of_a_Monitor_subscriber_has_been_changed_and_ICEF_Monitor_team_is_a</fullName>
        <ccEmails>monitoralerts@icef.com</ccEmails>
        <description>Emailaddress of a Monitor subscriber has been changed and ICEF Monitor team is alerted</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Emailaddress_of_a_Monitor_subscriber_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Emailaddress_of_a_contact_with_upcoming_workshops_has_been_changed</fullName>
        <description>Emailaddress of a contact with upcoming workshops has been changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Contact_of_an_upcoming_WS_has_new_emailaddress</template>
    </alerts>
    <alerts>
        <fullName>Monitor_Checkbox_has_been_set</fullName>
        <description>One of the Monitor Checkboxes has been set and none had been set before</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahaytova@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eleyendecker@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_Check_box_is_checked</template>
    </alerts>
    <alerts>
        <fullName>Monitor_Daily_has_been_unchecked</fullName>
        <description>Monitor Daily has been unchecked</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahaytova@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eleyendecker@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_daily_has_been_removed</template>
    </alerts>
    <alerts>
        <fullName>Monitor_Email_address_2_has_been_changed_in_Contact_and_Team_is_alerted</fullName>
        <description>Monitor Email address 2 has been changed in Contact and Team is alerted</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahaytova@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eleyendecker@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_Emailaddress_2_of_a_Contact_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Monitor_Email_address_has_been_changed_in_Contact_and_Team_is_alerted</fullName>
        <description>Monitor Email address has been changed in Contact and Team is alerted</description>
        <protected>false</protected>
        <recipients>
            <recipient>Monitor_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_Emailaddress_of_a_Contact_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Monitor_Weekly_has_been_unchecked</fullName>
        <description>Monitor Weekly has been unchecked</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahaytova@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eleyendecker@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_weekly_has_been_removed</template>
    </alerts>
    <alerts>
        <fullName>Monitor_subscriber_contact_has_been_deactivated</fullName>
        <ccEmails>monitoralerts@icef.com</ccEmails>
        <description>Monitor subscriber contact has been deactivated</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_subscriber_contact_has_been_deactivated</template>
    </alerts>
    <alerts>
        <fullName>Monitor_subscriber_date_has_been_set_alert</fullName>
        <description>Monitor subscriber date has been set alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>eleyendecker@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_subscriber_date_is_set</template>
    </alerts>
    <alerts>
        <fullName>Monitor_subscription_has_been_removed_alert_to_team</fullName>
        <description>Monitor subscription has been removed - alert to team</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahaytova@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eleyendecker@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_subscription_has_been_removed</template>
    </alerts>
    <alerts>
        <fullName>Monitor_unsubscriber_date_has_been_set_alert</fullName>
        <description>Monitor unsubscriber date has been set alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>eleyendecker@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_unsubscriber_date_is_set</template>
    </alerts>
    <alerts>
        <fullName>New_CF_admin_Reminder_for_SR_after_one_week</fullName>
        <ccEmails>rholmes@icef.com</ccEmails>
        <description>New CF admin: Reminder for SR after one week</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/Check_profile_completeness_of_CF_school</template>
    </alerts>
    <alerts>
        <fullName>One_of_the_Monitor_Checkboxes_has_been_unchecked</fullName>
        <description>One of the Monitor Checkboxes has been unchecked</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahaytova@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eleyendecker@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ICEF_Monitor/Monitor_Check_box_is_unchecked</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clean_NeverBounce_Email_Check</fullName>
        <description>NeverBounce Email check is cleaned when a new email address is added (NOT if the Contact Email is blank now)</description>
        <field>Webbula_Email_Check__c</field>
        <name>Clean &apos;NeverBounce Email Check&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_MyICEF_regform_date_field</fullName>
        <description>Clear field in order to be able to refill with data from lead</description>
        <field>MyICEF_regform_date__c</field>
        <name>Clear MyICEF regform date field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Email_Last_User_Fill</fullName>
        <field>Contact_Email_Last_User_new__c</field>
        <formula>$User.Full_Name__c</formula>
        <name>Contact Email Last User Fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_email_prior_value_fill</fullName>
        <description>When the contact emailaddress is changed then field &apos;Contact EMail - prior value&apos; is filled with the prior value. Used for ICEF Monitor email alert.</description>
        <field>Contact_email_prior_value__c</field>
        <formula>PRIORVALUE(Contact_Email__c)</formula>
        <name>Contact email prior value fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_has_left_company_1</fullName>
        <description>Updates Role to &apos;Former contact - inactive&apos;</description>
        <field>Role__c</field>
        <literalValue>Former Contact - INACTIVE</literalValue>
        <name>Contact has left company 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_has_left_company_2</fullName>
        <description>Updates Contact Status to &apos;Left company&apos;</description>
        <field>Contact_Status__c</field>
        <literalValue>Left company</literalValue>
        <name>Contact has left company 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_MyICEF_registration_reason</fullName>
        <description>Clear field to be able to refill from new lead</description>
        <field>MyICEF_registration_reason__c</field>
        <name>Delete MyICEF registration reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Bounce_Reason_Date_updated</fullName>
        <description>Field &apos;Email Bounce Reason last updated&apos; is set t Today.</description>
        <field>Email_Bounce_Reason_last_updated__c</field>
        <formula>TODAY ()</formula>
        <name>Email Bounce Reason Date updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Bounced_Reason_clean</fullName>
        <description>When Contact Email is changed then field &apos;Email Bounce Reason&apos; is emptied.</description>
        <field>Email_Bounce_Reason__c</field>
        <name>Email Bounced Reason clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Emailaddress_update</fullName>
        <description>The field &apos;Email&apos; is updated with the value of &apos;Contact Email&apos;, so that we have always the same values in these two fields.</description>
        <field>Email</field>
        <formula>Contact_Email__c</formula>
        <name>Emailaddress update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Last_Changed_Email</fullName>
        <field>Last_changed_Email__c</field>
        <formula>TODAY()</formula>
        <name>Fill Last Changed: Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ICEF_Monitor_subscriber_date_clean</fullName>
        <field>ICEF_Monitor_subscriber_date__c</field>
        <name>ICEF Monitor subscriber date clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ICEF_Monitor_subscriber_date_update</fullName>
        <field>ICEF_Monitor_subscriber_date__c</field>
        <formula>TODAY()</formula>
        <name>ICEF Monitor subscriber date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ICEF_Monitor_subscriber_fill</fullName>
        <description>Field &apos;ICEF Monitor subscriber&apos; is filled with value &apos;Monitor subscriber&apos;</description>
        <field>ICEF_Monitor_subscriber1__c</field>
        <literalValue>Monitor subscriber</literalValue>
        <name>ICEF Monitor subscriber fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ICEF_Monitor_subscriber_remove</fullName>
        <description>change Monitor subscriber to None</description>
        <field>ICEF_Monitor_subscriber1__c</field>
        <name>ICEF Monitor subscriber remove</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Insights_mag_remove</fullName>
        <description>Deselect the checkbox &apos;Insights mag&apos;</description>
        <field>X20_year_mag__c</field>
        <literalValue>0</literalValue>
        <name>Insights mag remove</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Login_update</fullName>
        <description>Update Username with Email</description>
        <field>Username__c</field>
        <formula>Contact_Email__c</formula>
        <name>Login update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MON_daily_2_unsub_date_update</fullName>
        <field>Unsubscribed_Date_Daily_2__c</field>
        <formula>TODAY()</formula>
        <name>MON daily 2 unsub date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MON_daily_date_update</fullName>
        <description>fills field Monitor daily with today()</description>
        <field>ICEF_Monitor_subscriber_Date_Daily__c</field>
        <formula>TODAY()</formula>
        <name>MON daily date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MON_daily_unsub_date_update</fullName>
        <description>fill Daily unsub date with today()</description>
        <field>Unsubscribed_Date_Daily__c</field>
        <formula>TODAY()</formula>
        <name>MON daily unsub date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MON_weekly_2_unsub_date_update</fullName>
        <description>set to today</description>
        <field>Unsubscribed_Date_Weekly_2__c</field>
        <formula>TODAY()</formula>
        <name>MON weekly 2 unsub date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MON_weekly_unsub_update</fullName>
        <description>weekly unsub date is set to today()</description>
        <field>Unsubscribed_Date__c</field>
        <formula>today()</formula>
        <name>MON weekly unsub update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mobile_Phone_Builder</fullName>
        <description>Mobile Number is built using &apos;Mobile Country Code&apos; and &apos;Mobile Number&apos;</description>
        <field>MobilePhone</field>
        <formula>MID(TEXT (Mobile_Country_Code__c), (FIND(&quot;+&quot;, TEXT (Mobile_Country_Code__c))),LEN ( TEXT (Mobile_Country_Code__c))-(FIND(&quot;+&quot;,TEXT (Mobile_Country_Code__c))))
&amp; &quot;-&quot;
&amp;  Mobile_Number__c</formula>
        <name>Mobile Phone Builder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Monitor_email_2_prior_value_fill</fullName>
        <field>Monitor_Email_2_prior_value__c</field>
        <formula>PRIORVALUE( Monitor_Email_2__c )</formula>
        <name>Monitor email 2 prior value fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Monitor_email_prior_value_fill</fullName>
        <field>Monitor_Email_prior_value__c</field>
        <formula>PRIORVALUE( Monitor_Email__c )</formula>
        <name>Monitor email prior value fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NeverBounce_emailcheck_prior_value_fill</fullName>
        <field>NeverBounce_Email_Check_prior_value__c</field>
        <formula>Priorvalue ( Webbula_Email_Check__c )</formula>
        <name>NeverBounce emailcheck prior value fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_Checkbox_CF_Administrator</fullName>
        <field>CF_Administrator__c</field>
        <literalValue>0</literalValue>
        <name>Tick Checkbox &apos;CF Administrator&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Welcome_Email_sent</fullName>
        <description>sets Date in Welcome Email sent field</description>
        <field>Welcome_Email_sent__c</field>
        <formula>NOW()</formula>
        <name>set Welcome Email sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account of a CF admin Contact has been changed</fullName>
        <actions>
            <name>CF_admin_account_has_chenaged_and_Ross_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>A Contact that is marked as CF admin contact has been allocated to a different account and Ross is alerted.</description>
        <formula>AND 
(
  CF_Administrator__c ,
  ISCHANGED ( Account_Name__c ),
  NOT (ISCHANGED( Contact_Email__c ))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Acount of CF Admin is not Beta</fullName>
        <actions>
            <name>CF_admin_belongs_to_contact_that_is_not_a_Beta_candidate</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>The tickbox &apos;CF Administrator&apos; has been ticked, but the approp. account is not marked with &apos;Coursefinder Beta candidate&apos;.</description>
        <formula>CF_Administrator__c  = TRUE  &amp;&amp; 
 Account.LLN_Beta_candidate__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Act-On form for Agents has been filled out</fullName>
        <actions>
            <name>Act_On_Agents_form_has_been_filled_and_owner_is_informed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An Act-On form has been filled out and an alert is sent t the owner. Fields &apos;Lead Source&apos; is used as trigger</description>
        <formula>AND (
  ISCHANGED ( Act_on_Form__c ),
  ISPICKVAL (LeadSource , &quot;Act-on Form Agents&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CF Admin tickbox has been ticked</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.CF_Administrator__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>The tickbox &apos;CF Administrator&apos; has been ticked and one week later a reminder is sent to the account owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>New_CF_admin_Reminder_for_SR_after_one_week</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CF Admin tickbox has been ticked - backup</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contact.CF_Administrator__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Welcome_Email_sent__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>The tickbox &apos;CF Administrator&apos; has been ticked, and 2h later a welcome email is sent to the contact. Only if email with review link has not been sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CF_Administrator_tickbox_has_been_ticked</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>set_Welcome_Email_sent</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CF Sales tickbox has been ticked</fullName>
        <actions>
            <name>CF_Sales_ticked_automatically</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The tickbox &apos;CF Sales&apos; has been ticked automatically, alert to Account owner</description>
        <formula>LastModifiedBy.Email  = &quot;robot@icef.com&quot;  &amp;&amp; CF_Sales__c = true &amp;&amp; PRIORVALUE(CF_Sales__c) = false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CF admin contact has been deactivated</fullName>
        <actions>
            <name>Tick_Checkbox_CF_Administrator</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Contact_Status__c</field>
            <operation>equals</operation>
            <value>Left company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.CF_Administrator__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>A Contact that is marked as CF admin has been deactivated and therefor the CF admin tick is removed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact Email has been added</fullName>
        <actions>
            <name>Contact_Email_Last_User_Fill</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fill_Last_Changed_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Contact has been added and has an email address</description>
        <formula>NOT ( ISBLANK ( Contact_Email__c ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact Email has been changed</fullName>
        <actions>
            <name>Clean_NeverBounce_Email_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Email_Last_User_Fill</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_email_prior_value_fill</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email_Bounced_Reason_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fill_Last_Changed_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NeverBounce_emailcheck_prior_value_fill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Field &apos;Contact Email&apos; has been changed</description>
        <formula>ISCHANGED ( Contact_Email__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact Name has changed</fullName>
        <actions>
            <name>Contact_name_has_been_changed_and_Laura_is_informed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The name of a contact who is registered for an upcoming workshop has been changed</description>
        <formula>AND (
 AND (
 Most_recent_ICEF_event__c &lt;  (TODAY() + 60),
 Most_recent_ICEF_event__c &gt; TODAY()
 ),
  OR (
   PRIORVALUE( LastName ) &lt;&gt; LastName,
   PRIORVALUE( FirstName ) &lt;&gt; FirstName
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact create or change</fullName>
        <actions>
            <name>Emailaddress_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Login_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact has left company 1</fullName>
        <actions>
            <name>Contact_has_left_company_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Contact_Status__c</field>
            <operation>equals</operation>
            <value>Left company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Role__c</field>
            <operation>notEqual</operation>
            <value>Former Contact - INACTIVE</value>
        </criteriaItems>
        <description>A contact has left the company and the role is set to &apos;Former contact - inactive&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact has left company 2</fullName>
        <actions>
            <name>Contact_has_left_company_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Contact_Status__c</field>
            <operation>notEqual</operation>
            <value>Left company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Role__c</field>
            <operation>equals</operation>
            <value>Former Contact - INACTIVE</value>
        </criteriaItems>
        <description>A contact&apos;s role has been set to  &apos;Former contact - inactive&apos; and Status is changed to &apos;Left company&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact has left company 3</fullName>
        <actions>
            <name>Insights_mag_remove</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Contact_Status__c</field>
            <operation>equals</operation>
            <value>Left company</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact has requested a MyICEF account</fullName>
        <actions>
            <name>Contact_owner_is_informed_about_MyICEF_account_request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MyICEF_registration_reason__c</field>
            <operation>equals</operation>
            <value>I am a new client,Other reason</value>
        </criteriaItems>
        <description>coming from Lead: An agent has filled in the MyICEF registration form as new client or for other reason</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Clear_MyICEF_regform_date_field</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Delete_MyICEF_registration_reason</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contact of an upcoming WS has been set to Inactive</fullName>
        <actions>
            <name>Contact_of_an_upcoming_Workshop_has_left_the_company</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>A contact who is registered for an upcoming workshop has been set to status &apos;Left company&apos;</description>
        <formula>AND (
  Most_recent_ICEF_event__c &gt; TODAY(),
  ISPICKVAL (Contact_Status__c , &quot;Left company&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Country Survey has been filled out</fullName>
        <actions>
            <name>Country_Survey_has_been_filled_out</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>A Contact has filled out the Country Survey.</description>
        <formula>AND (
ISCHANGED (Survey_Date__c),
 Country_Survey_filled_out_by_ARM__c = FALSE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Bounce Date update</fullName>
        <actions>
            <name>Email_Bounce_Reason_Date_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Field &apos;Email Bounce Reason&apos; is updated.</description>
        <formula>ISCHANGED ( Email_Bounce_Reason__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email change of Monitor subscriber</fullName>
        <actions>
            <name>Emailaddress_of_a_Monitor_subscriber_has_been_changed_and_ICEF_Monitor_team_is_a</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The emailaddress of a Monitor subscriber has changed.</description>
        <formula>AND (
ISCHANGED( Contact_Email__c ),
ISPICKVAL ( ICEF_Monitor_subscriber1__c , &quot;Monitor subscriber&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email of a contact with upcoming WSs has changed</fullName>
        <actions>
            <name>Emailaddress_of_a_contact_with_upcoming_workshops_has_been_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The emailaddress of a contact who is registered for an upcoming ICEF event has changed. An alert is sent to Frauke.</description>
        <formula>AND (
ISCHANGED ( Contact_Email__c ),
 Most_recent_ICEF_event__c &gt; TODAY()
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MobilePhone builder</fullName>
        <actions>
            <name>Mobile_Phone_Builder</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Field MobilePhone is filled with values from &apos;Mobile Country Code&apos; and &apos;Mobile Number&apos;.</description>
        <formula>OR (
  ISCHANGED ( Mobile_Country_Code__c ),
  ISCHANGED ( Mobile_Number__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor Email 2 change</fullName>
        <actions>
            <name>Monitor_Email_address_2_has_been_changed_in_Contact_and_Team_is_alerted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Monitor_email_2_prior_value_fill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>The Monitor Email 2 of a Contact has changed.</description>
        <formula>AND (
  ISCHANGED(Monitor_Email_2__c),
  OR (
    NOT(ISBLANK(PRIORVALUE(Monitor_Email_2__c))),
    ISBLANK(Monitor_Email_2__c)
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor Email change</fullName>
        <actions>
            <name>Monitor_Email_address_has_been_changed_in_Contact_and_Team_is_alerted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Monitor_email_prior_value_fill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>The Monitor Email of a Contact has changed.</description>
        <formula>AND (
  ISCHANGED(Monitor_Email__c),
  OR (
    NOT(ISBLANK(PRIORVALUE(Monitor_Email__c))),
    ISBLANK(Monitor_Email__c)
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor Subscriber empty</fullName>
        <actions>
            <name>ICEF_Monitor_subscriber_remove</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Monitor_Daily__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Monitor_Daily_2__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Monitor_Weekly__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Monitor_Weekly_2__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.ICEF_Monitor_subscriber1__c</field>
            <operation>equals</operation>
            <value>Monitor subscriber</value>
        </criteriaItems>
        <description>if all checkboxes for Monitor Subscription are unticked remove Monitor Subscriber from ICEF Monitor subscriber</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Monitor checkbox has been set</fullName>
        <actions>
            <name>Monitor_Checkbox_has_been_set</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>One of 4 Monitor checkboxes has been ticked. Eva is alerted.</description>
        <formula>OR (
    Monitor_Daily__c &amp;&amp; (NOT (PRIORVALUE ( Monitor_Daily__c )) || ISNEW()),
    Monitor_Daily_2__c &amp;&amp; (NOT (PRIORVALUE ( Monitor_Daily_2__c )) || ISNEW()),
    Monitor_Weekly__c &amp;&amp; (NOT (PRIORVALUE ( Monitor_Weekly__c )) || ISNEW()),
    Monitor_Weekly_2__c &amp;&amp; (NOT (PRIORVALUE ( Monitor_Weekly_2__c )) || ISNEW())
  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor checkbox has been unchecked</fullName>
        <actions>
            <name>One_of_the_Monitor_Checkboxes_has_been_unchecked</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>One of 4 Monitor checkboxes has been unticked. Team is alerted.</description>
        <formula>OR (
    NOT(Monitor_Daily__c) &amp;&amp; PRIORVALUE ( Monitor_Daily__c ),
    NOT(Monitor_Daily_2__c) &amp;&amp; PRIORVALUE ( Monitor_Daily_2__c ),
    NOT(Monitor_Weekly__c) &amp;&amp; PRIORVALUE ( Monitor_Weekly__c ),
    NOT(Monitor_Weekly_2__c) &amp;&amp; PRIORVALUE ( Monitor_Weekly_2__c )
  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor daily 2 has been removed</fullName>
        <actions>
            <name>MON_daily_2_unsub_date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>update daily 2 unsubscriber date to today()</description>
        <formula>AND (
ISCHANGED ( Monitor_Daily_2__c ),
PRIORVALUE ( Monitor_Daily_2__c ) = TRUE,
ISBLANK ( Unsubscribed_Date_Daily_2__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor daily has been removed</fullName>
        <actions>
            <name>MON_daily_unsub_date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND (
ISCHANGED ( Monitor_Daily__c ),
PRIORVALUE ( Monitor_Daily__c ) = TRUE,
ISBLANK ( Unsubscribed_Date_Daily__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor subscribe date update</fullName>
        <actions>
            <name>ICEF_Monitor_subscriber_date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND (
  OR (
    ISCHANGED ( ICEF_Monitor_subscriber1__c ),
    ISNEW()
    ),
  NOT (ISPICKVAL (ICEF_Monitor_subscriber1__c, &quot;&quot; )
  ),
  ISBLANK ( ICEF_Monitor_subscriber_date__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor subscriber contact has been deactivated</fullName>
        <actions>
            <name>Monitor_subscriber_contact_has_been_deactivated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>A contact who is subscribed to the Monitor is set to status &apos;INACTIVE&apos;</description>
        <formula>AND (
 TEXT(Contact_Status__c) = &apos;Left company&apos;,
 TEXT(ICEF_Monitor_subscriber1__c) = &apos;Monitor subscriber&apos;,
 TEXT(PRIORVALUE(Contact_Status__c)) &lt;&gt; &apos;Left company&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor subscriber date clean</fullName>
        <actions>
            <name>ICEF_Monitor_subscriber_date_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.ICEF_Monitor_subscriber1__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When the entry in &apos;ICEF Monitor subscriber&apos; is deleted then the date must be removed from the system.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Monitor subscription has been removed</fullName>
        <actions>
            <name>Monitor_subscription_has_been_removed_alert_to_team</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Entry &apos;Monitor subscriber&apos; in field &apos;ICEF Monitor subscriber&apos; has been removed.</description>
        <formula>AND (
  ISPICKVAL ( ICEF_Monitor_subscriber1__c , &quot;&quot;),
  ISPICKVAL ((PRIORVALUE (ICEF_Monitor_subscriber1__c)), &quot;Monitor subscriber&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor weekly 2 has been removed</fullName>
        <actions>
            <name>MON_weekly_2_unsub_date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>unsubscribe date is set to today if it is empty</description>
        <formula>AND (
ISCHANGED ( Monitor_Weekly_2__c ),
PRIORVALUE ( Monitor_Weekly_2__c ) = TRUE,
ISBLANK(  Unsubscribed_Date_Weekly_2__c  ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor weekly has been removed</fullName>
        <actions>
            <name>MON_weekly_unsub_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>unsubscribe date is set to today if it is empty</description>
        <formula>AND (
ISCHANGED ( Monitor_Weekly__c ),
PRIORVALUE ( Monitor_Weekly__c ) = TRUE,
ISBLANK( Unsubscribed_Date__c ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Monitor weekly%2Fdaily fills subscriber field</fullName>
        <actions>
            <name>ICEF_Monitor_subscriber_fill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 4 OR 5) AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Monitor_Daily__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Monitor_Weekly__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.ICEF_Monitor_subscriber1__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Monitor_Daily_2__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Monitor_Weekly_2__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If one of the checkboxes weekly&apos; or &apos;daily&apos; is ticked but the field&apos;ICEF Monitor subscriber&apos; is empty then this filed will be updated to &apos;Monitor subscriber&apos; automatically.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tickbox %27CF Administrator%27 has been removed</fullName>
        <actions>
            <name>CF_Administrator_tickbox_has_been_removed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The tickbox &apos;CF Administrator&apos; has been removed</description>
        <formula>AND (
  CF_Administrator__c  = FALSE,
  PRIORVALUE (CF_Administrator__c) = TRUE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Finance Contact</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Role__c</field>
            <operation>equals</operation>
            <value>Finance Contact</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>a monitor subscriber date has been set</fullName>
        <actions>
            <name>Monitor_subscriber_date_has_been_set_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>one of the monitor dates has been filled and Eva is alerted</description>
        <formula>OR(
  NOT(ISBLANK(ICEF_Monitor_subscriber_Date_Daily__c)) &amp;&amp; ISBLANK(PRIORVALUE(ICEF_Monitor_subscriber_Date_Daily__c)),
  NOT(ISBLANK(ICEF_Monitor_subscriber_Date_Daily_2__c)) &amp;&amp; ISBLANK(PRIORVALUE(ICEF_Monitor_subscriber_Date_Daily_2__c)),
  NOT(ISBLANK(ICEF_Monitor_subscriber_date__c)) &amp;&amp; ISBLANK(PRIORVALUE(ICEF_Monitor_subscriber_date__c)),
  NOT(ISBLANK(ICEF_Monitor_subscription_Date_Weekly_2__c)) &amp;&amp; ISBLANK(PRIORVALUE(ICEF_Monitor_subscription_Date_Weekly_2__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>a monitor unsubscriber date has been set</fullName>
        <actions>
            <name>Monitor_unsubscriber_date_has_been_set_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>one of the monitor unsubscriber dates has been filled and Eva is alerted</description>
        <formula>OR(
  NOT(ISBLANK(Unsubscribed_Date_Daily__c)) &amp;&amp; ISBLANK(PRIORVALUE(Unsubscribed_Date_Daily__c)),
  NOT(ISBLANK(Unsubscribed_Date_Daily_2__c)) &amp;&amp; ISBLANK(PRIORVALUE(Unsubscribed_Date_Daily_2__c)),
  NOT(ISBLANK(Unsubscribed_Date__c)) &amp;&amp; ISBLANK(PRIORVALUE(Unsubscribed_Date__c)),
  NOT(ISBLANK(ICEF_Monitor_subscription_Date_Weekly_2__c)) &amp;&amp; ISBLANK(PRIORVALUE(Unsubscribed_Date_Weekly_2__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Country_Survey_has_been_filled_out</fullName>
        <assignedToType>accountOwner</assignedToType>
        <description>A Contact has just completed the Country Survey.
Please follow up with a sales call.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Contact.Survey_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Country Survey has been filled out</subject>
    </tasks>
</Workflow>
