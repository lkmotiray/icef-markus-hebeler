<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AP_for_agency_with_negative_reference_has_been_created_and_Tiff_is_alerted</fullName>
        <description>AP for agency with negative reference has been created and Tiff is alerted</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Agency_with_negative_reference_is_booked_for_a_Workshop</template>
    </alerts>
    <alerts>
        <fullName>AP_has_been_created_for_Agency_with_Status_Inactive_or_Blacklisted</fullName>
        <description>AP has been created for Agency with Status Inactive or Blacklisted(potential client)</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Agency_with_bad_status_is_booked_for_a_Workshop</template>
    </alerts>
    <alerts>
        <fullName>AP_of_agency_has_lost_status_accepted</fullName>
        <ccEmails>tegler@icef.com</ccEmails>
        <description>AP of agency has lost status &apos;accepted&apos;</description>
        <protected>false</protected>
        <recipients>
            <field>ARM_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Agency_AP_has_lost_Status_Accepted</template>
    </alerts>
    <alerts>
        <fullName>AP_status_has_changed_from_pending_payment_to_registered</fullName>
        <description>AP status has changed from pending payment to registered</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Payment_for_AP_has_been_entered</template>
    </alerts>
    <alerts>
        <fullName>AP_with_active_Hotel_Rooms_is_cancelled_and_EMT_is_alerted</fullName>
        <description>AP with active Hotel Rooms is cancelled and EMT is alerted</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/AP_cancelled_but_has_Accommodations</template>
    </alerts>
    <alerts>
        <fullName>A_Booking_has_been_cancelled_and_Sales_Rep_is_informed_Americas</fullName>
        <description>A Booking has been cancelled and Sales Rep is informed - Americas</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Account_cancelled_or_transferred_an_event_and_Sales_Manager_is_informed</template>
    </alerts>
    <alerts>
        <fullName>A_Booking_has_been_cancelled_and_Sales_Rep_is_informed_Asia_Pac</fullName>
        <description>A Booking has been cancelled and Sales Rep is informed - Asia/Pac</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>rhearps@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Account_cancelled_or_transferred_an_event_and_Sales_Manager_is_informed</template>
    </alerts>
    <alerts>
        <fullName>A_Booking_has_been_cancelled_and_Sales_Rep_is_informed_EMEA</fullName>
        <description>A Booking has been cancelled and Sales Rep is informed - EMEA</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>hkreiner@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Account_cancelled_or_transferred_an_event_and_Sales_Manager_is_informed</template>
    </alerts>
    <alerts>
        <fullName>Agency_has_been_marked_as_Newcomer</fullName>
        <description>Agency has been marked as Newcomer</description>
        <protected>false</protected>
        <recipients>
            <field>ARM_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/AP_has_been_marked_as_Newcomer</template>
    </alerts>
    <alerts>
        <fullName>Agency_with_less_than_4_pos_references_has_been_accepted_for_WS</fullName>
        <description>Agency with less than 4 pos. references has been accepted for WS</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Agency_with_less_than_4_refs_accepted</template>
    </alerts>
    <alerts>
        <fullName>Agency_with_negative_behaviour_at_a_previous_WS_has_a_new_AP</fullName>
        <description>Agency with negative behaviour at a previous WS has a new AP</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Agency_with_negative_behaviour_is_booked_for_a_Workshop</template>
    </alerts>
    <alerts>
        <fullName>Agent_AP_accepted_but_essential_info_is_missing</fullName>
        <description>Agent AP accepted but essential info is missing</description>
        <protected>false</protected>
        <recipients>
            <field>ARM_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>tegler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ARM_Alerts/Agency_with_missing_info_AP_acepted</template>
    </alerts>
    <alerts>
        <fullName>Agent_AP_accepted_but_student_sent_info_is_missing</fullName>
        <description>Agent AP accepted but student sent info is missing</description>
        <protected>false</protected>
        <recipients>
            <field>ARM_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>tegler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ARM_Alerts/Agency_with_missing_Students_sent_info_is_accepted</template>
    </alerts>
    <alerts>
        <fullName>Agent_AP_accepted_but_web_check_info_is_missing</fullName>
        <description>Agent AP accepted but web check info is missing</description>
        <protected>false</protected>
        <recipients>
            <field>ARM_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>tegler@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ARM_Alerts/Agency_with_missing_web_check_info_AP_accepted</template>
    </alerts>
    <alerts>
        <fullName>Alert_Finance_when_an_account_cancels</fullName>
        <ccEmails>accounts@icef.com</ccEmails>
        <description>Alert Finance when an account cancels</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMT_alerts/Alert_Finance_and_EMT_about_AP_cancellation</template>
    </alerts>
    <alerts>
        <fullName>Alert_Finance_when_an_account_cancels_Asia_Pac</fullName>
        <ccEmails>accounts@icef.com</ccEmails>
        <description>Alert Finance when an account cancels Asia/Pac</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>cathy@edmedia.info</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMT_alerts/Alert_Finance_and_EMT_about_AP_cancellation</template>
    </alerts>
    <alerts>
        <fullName>Alert_Finance_when_an_account_cancels_or_transfers</fullName>
        <description>Alert Finance when an account cancels or transfers</description>
        <protected>false</protected>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>james.love@pobox.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jsarfraz@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pruehseler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Transfer_or_cancellation</template>
    </alerts>
    <alerts>
        <fullName>Alert_Finance_when_an_account_cancels_or_transfers_Asia_Pacific</fullName>
        <description>Alert Finance when an account cancels or transfers Asia-Pacific</description>
        <protected>false</protected>
        <recipients>
            <recipient>cathy@edmedia.info</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pruehseler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Transfer_or_cancellation</template>
    </alerts>
    <alerts>
        <fullName>Alert_Finance_when_an_account_transfers</fullName>
        <ccEmails>accounts@icef.com</ccEmails>
        <description>Alert Finance when an account transfers</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMT_alerts/Alert_Finance_and_EMT_about_AP_transfer</template>
    </alerts>
    <alerts>
        <fullName>Alert_Finance_when_an_account_transfers_Asia_Pac</fullName>
        <ccEmails>accounts@icef.com</ccEmails>
        <description>Alert Finance when an account transfers Asia/Pac</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>cathy@edmedia.info</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ffestersen@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMT_alerts/Alert_Finance_and_EMT_about_AP_transfer</template>
    </alerts>
    <alerts>
        <fullName>An_ANZ_Account_has_either_cancelled_or_transferred</fullName>
        <description>An ANZ Account has either cancelled or transferred</description>
        <protected>false</protected>
        <recipients>
            <recipient>cathy@edmedia.info</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>james.love@pobox.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pruehseler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rhearps@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Transfer_or_cancellation</template>
    </alerts>
    <alerts>
        <fullName>An_AP_has_been_created_for_your_client</fullName>
        <description>An AP has been created for your client</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/AP_has_been_created_for_your_client</template>
    </alerts>
    <alerts>
        <fullName>An_AP_is_registered_pending_payment_and_Territory_Manager_is_informed</fullName>
        <description>An Account Participation is registered or pending payment and Territory Manager is informed</description>
        <protected>false</protected>
        <recipients>
            <field>Territory_Manager_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Account_booked_an_event</template>
    </alerts>
    <alerts>
        <fullName>An_Account_Participation_has_been_created_and_Sales_Manager_is_informed_All_terr</fullName>
        <description>An Account Participation has been created and Sales Manager is informed /  All territories</description>
        <protected>false</protected>
        <recipients>
            <field>Territory_Manager_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Account_booked_an_event</template>
    </alerts>
    <alerts>
        <fullName>An_Account_Participation_has_been_created_and_Sales_Manager_is_informed_CAN</fullName>
        <description>An Account Participation has been created and Sales Manager is informed / CAN</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Manager_Canada</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Account_booked_an_event</template>
    </alerts>
    <alerts>
        <fullName>An_Account_Participation_has_been_created_and_Sales_Manager_is_informed_Harald</fullName>
        <description>An Account Participation has been created and Sales Manager is informed / EMEA</description>
        <protected>false</protected>
        <recipients>
            <recipient>hkreiner@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Account_booked_an_event</template>
    </alerts>
    <alerts>
        <fullName>An_Account_Participation_has_been_created_and_Sales_Manager_is_informed_Rod</fullName>
        <description>An Account Participation has been created and Sales Manager is informed / Asia-Pacific</description>
        <protected>false</protected>
        <recipients>
            <recipient>rhearps@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Account_booked_an_event</template>
    </alerts>
    <alerts>
        <fullName>An_Account_Participation_has_been_created_and_Sales_Manager_is_informed_USA</fullName>
        <description>An Account Participation has been created and Sales Manager is informed / USA</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesManagerUSA</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Account_booked_an_event</template>
    </alerts>
    <alerts>
        <fullName>An_agency_without_website_has_been_accepted_for_a_workshop</fullName>
        <description>An agency without website has been accepted for a workshop</description>
        <protected>false</protected>
        <recipients>
            <field>ARM_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ivogt@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>james.love@pobox.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/AP_for_Agency_without_Website_has_been_created</template>
    </alerts>
    <alerts>
        <fullName>Cancellation_of_Agency_AP</fullName>
        <description>Cancellation of Agency AP</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Account_Participation_Agency_cancelled</template>
    </alerts>
    <alerts>
        <fullName>Catalogue_City_of_an_agency_has_changed</fullName>
        <description>Catalogue City of an agency has changed</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMT_alerts/Catalogue_City_of_an_AP_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_Letter_for_agencies_alert</fullName>
        <description>Confirmation Letter for agencies alert</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Confirmation_Letter_Alert_AP</template>
    </alerts>
    <alerts>
        <fullName>Country_Section_incorrect_Alert</fullName>
        <description>Country Section incorrect Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Country_Section_needs_to_be_checked</template>
    </alerts>
    <alerts>
        <fullName>Event_Booths_fully_booked_alert</fullName>
        <description>Event Booths fully booked alert</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Event_booths_fully_booked_alert</template>
    </alerts>
    <alerts>
        <fullName>Event_Tables_fully_booked_alert</fullName>
        <description>Event Tables fully booked alert</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Event_tables_fully_booked_alert</template>
    </alerts>
    <alerts>
        <fullName>Event_exhibit_tables_fully_booked_alert</fullName>
        <description>Event exhibit tables fully booked alert</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Event_exhibit_tables_fully_booked_alert</template>
    </alerts>
    <alerts>
        <fullName>Event_exhibit_tables_overbooked_alert</fullName>
        <description>Event Exhibit Tables overbooked alert</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Event_exhibit_tables_overbooked_alert</template>
    </alerts>
    <alerts>
        <fullName>Event_tables_overbooked_alert</fullName>
        <description>Event Tables overbooked alert</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Event_tables_overbooked_alert</template>
    </alerts>
    <alerts>
        <fullName>Exhibitor_Booth_overbooked_alert</fullName>
        <description>Exhibitor Booth overbooked alert</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Event_booths_overbooked_alert</template>
    </alerts>
    <alerts>
        <fullName>Notify_Finance_if_transfer_or_cancellation</fullName>
        <description>Notify Finance if transfer or cancellation</description>
        <protected>false</protected>
        <recipients>
            <recipient>cathy@edmedia.info</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>james.love@pobox.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jsarfraz@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pruehseler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Transfer_or_cancellation</template>
    </alerts>
    <alerts>
        <fullName>Primary_Rec_Country_Check_Alert</fullName>
        <description>Primary Rec. Country Check Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Primary_Rec_Country_needs_to_be_checked</template>
    </alerts>
    <alerts>
        <fullName>Sponsorship_package_has_been_booked</fullName>
        <description>Sponsorship package has been booked</description>
        <protected>false</protected>
        <recipients>
            <recipient>sbobrov@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_general_templates/Sponsorship_package_has_been_booked</template>
    </alerts>
    <alerts>
        <fullName>Toronto_2018_Exhibitor_Catalogue_Name_change_alert</fullName>
        <description>Toronto 2018 Exhibitor Catalogue Name change alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>mbauz@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Toronto_2018_Catalogue_Name_change</template>
    </alerts>
    <alerts>
        <fullName>Unassigned_Account_booked_an_event</fullName>
        <description>Unassigned Account booked an event</description>
        <protected>false</protected>
        <recipients>
            <field>Territory_Manager_emailaddress__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Unassigned_Account_booked_an_event</template>
    </alerts>
    <alerts>
        <fullName>Workshop_participants_have_been_designated_via_online_form</fullName>
        <description>Workshop participants have been designated via online form</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Workshop_participant_Webform_has_been_filled_out</template>
    </alerts>
    <fieldUpdates>
        <fullName>ARM_Hotel_Comments_Clean</fullName>
        <field>ARM_Hotel_Comments__c</field>
        <name>ARM Hotel Comments Clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Analogue_telephone_clean</fullName>
        <field>Analogue_telephone__c</field>
        <name>Analogue telephone clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Beamer_clean</fullName>
        <field>Beamer__c</field>
        <name>Beamer clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Booth_Signage_left_clean</fullName>
        <field>signage_left__c</field>
        <name>Booth Signage left clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Booth_Signage_right_clean</fullName>
        <field>signage_right__c</field>
        <name>Booth Signage right clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Booth_comments_clean</fullName>
        <field>Booth_comments__c</field>
        <name>Booth comments clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Booth_size_clean</fullName>
        <field>Booth_size__c</field>
        <name>Booth size clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Brochure_display_rack_clean</fullName>
        <field>Brochure_display_rack__c</field>
        <name>Brochure display rack clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Carpet_colour_clean</fullName>
        <field>Carpet_colour__c</field>
        <name>Carpet colour clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Carpet_sqm_clean</fullName>
        <field>Carpet_sqm__c</field>
        <name>Carpet (sqm) clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_City</fullName>
        <field>Catalogue_City__c</field>
        <formula>Account__r.Mailing_City__c</formula>
        <name>Catalogue City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_Department</fullName>
        <field>Catalogue_Department__c</field>
        <formula>Account__r.Department__c</formula>
        <name>Catalogue Department</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_Email1</fullName>
        <field>Catalogue_Email1__c</field>
        <formula>Account__r.Email__c</formula>
        <name>Catalogue Email1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_Fax</fullName>
        <field>Catalogue_Fax__c</field>
        <formula>Account__r.Fax</formula>
        <name>Catalogue Fax</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_Name</fullName>
        <description>Fills field &apos;Catalogue Name&apos; with value of Account Name</description>
        <field>Catalogue_Name__c</field>
        <formula>Account__r.Name</formula>
        <name>Catalogue Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_Post_Code</fullName>
        <field>Catalogue_Post_Code__c</field>
        <formula>Account__r.Mailing_Post_Code__c</formula>
        <name>Catalogue Post Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_State_Province</fullName>
        <field>Catalogue_State_Province__c</field>
        <formula>Account__r.Mailing_State_Province__c</formula>
        <name>Catalogue State/Province</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_Street</fullName>
        <field>Catalogue_Street__c</field>
        <formula>Account__r.Mailing_street__c</formula>
        <name>Catalogue Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_Tel</fullName>
        <field>Catalogue_Tel__c</field>
        <formula>Account__r.Phone</formula>
        <name>Catalogue Tel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_Text_checked_clean</fullName>
        <description>Field &apos;catalogue text checked by native speaker&apos; is set to 0 when an AP is cloned.</description>
        <field>Catalogue_text_checked_by_native_speaker__c</field>
        <name>Catalogue Text checked clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_Website</fullName>
        <field>Catalogue_Website__c</field>
        <formula>Account__r.Website</formula>
        <name>Catalogue Website</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Catalogue_Website2</fullName>
        <field>Catalogue_Website2__c</field>
        <formula>Account__r.Website_2__c</formula>
        <name>Catalogue Website2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Chair_clean</fullName>
        <field>Chair__c</field>
        <name>Chair clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Primary_Rec_Country_Solved</fullName>
        <field>Primary_Re_Country_Solved__c</field>
        <literalValue>0</literalValue>
        <name>Clear Primary Rec. Country Solved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Counter_clean</fullName>
        <field>Lockable_counter__c</field>
        <name>Counter clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DVD_Player_clean</fullName>
        <field>DVD_Player__c</field>
        <name>DVD Player clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Direct_print_on_booth_wall_panels_clean</fullName>
        <field>Direct_print_on_booth_wall_panels__c</field>
        <literalValue>0</literalValue>
        <name>Direct print on booth wall panels clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dustbins_clean</fullName>
        <field>dustbins__c</field>
        <name>Dustbins clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Electricity_at_table_clean</fullName>
        <field>Electricity_at_table__c</field>
        <name>Electricity at table clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Manager_Agents_email_update</fullName>
        <field>Event_Managers_Agents_Email__c</field>
        <formula>ICEF_Event__r.Event_Manager_Agents__r.Email</formula>
        <name>Event Manager (Agents) email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Manager_Educators_email_update</fullName>
        <field>Event_Managers_Email__c</field>
        <formula>ICEF_Event__r.Event_Manager_Educators__r.Email</formula>
        <name>Event Manager (Educators) email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>External_Booth_Comments_clean</fullName>
        <field>External_Booth_Comments__c</field>
        <name>External Booth Comments clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Facebook_copy_value_form_Account</fullName>
        <field>Facebook__c</field>
        <formula>Account__r.Facebook__c</formula>
        <name>Facebook - copy value form Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Addendum_is_emptied</fullName>
        <description>When an AP is created then the field &apos;Addendum&apos; will updated with a blank entry. This is to avoid wrong entry when an AP is cloned.</description>
        <field>Addendum__c</field>
        <name>Field Addendum is emptied</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_Agreements_in_progress_clean</fullName>
        <field>Agreements_in_process__c</field>
        <name>Follow up Agreements in progress clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_Agreements_progress_no_clean</fullName>
        <field>Number_of_agreements_processing__c</field>
        <name>Follow up Agreements progress no clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_Considerations_Clean</fullName>
        <field>Considerations_for_future_participations__c</field>
        <name>Follow up Considerations Clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_Fam_tour_feedback_clean</fullName>
        <field>Fam_Tour_Feedback__c</field>
        <name>Follow up Fam tour feedback clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_Fam_tour_part_clean</fullName>
        <field>Fam_Tour_participation__c</field>
        <name>Follow up Fam tour part. clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_Negative_WS_behaviour_clean</fullName>
        <field>Negative_Workshop_Behaviour__c</field>
        <name>Follow up Negative WS behaviour clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_New_or_existing_partners_clean</fullName>
        <field>New_or_existing_partners_met__c</field>
        <name>Follow up New or existing partners clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_Students_sent_clean</fullName>
        <field>Students_sent_already__c</field>
        <name>Follow up Students sent clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_agreement_number_clean</fullName>
        <field>Number_of_agreements_signed__c</field>
        <name>Follow up agreement number clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_agreement_remarks_clean</fullName>
        <field>Agreement_remarks__c</field>
        <name>Follow up agreement remarks clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_agreements_signed_clean</fullName>
        <field>Agreements_signed__c</field>
        <name>Follow up agreements signed clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_appointment_number_clean</fullName>
        <field>Number_of_Appointments__c</field>
        <name>Follow up appointment number clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_appointment_remarks_clean</fullName>
        <field>Number_of_Appointments_Remarks__c</field>
        <name>Follow up appointment remarks clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_business_needs_clean</fullName>
        <field>Business_needs_met__c</field>
        <name>Follow up business needs clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_business_needs_remarks_clean</fullName>
        <field>Business_needs_remarks__c</field>
        <name>Follow up business needs remarks clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_date_clean</fullName>
        <field>Follow_up_completed_date__c</field>
        <name>Follow up date clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Follow_up_students_sent_to_who_clean</fullName>
        <field>Students_sent_to_who__c</field>
        <name>Follow up students sent to who clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Free_Airport_Transfer_clean</fullName>
        <field>Free_airport_transfer_Number_of_persons__c</field>
        <name>Free Airport Transfer clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>High_Table_clean</fullName>
        <field>High_Table__c</field>
        <name>High Table clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>High_speed_internet_5MB_clean</fullName>
        <field>High_speed_internet_5MB__c</field>
        <name>High speed internet 5MB clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>High_stool_clean</fullName>
        <field>High_stool__c</field>
        <name>High stool clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_booked_clean</fullName>
        <field>Hotel_booked__c</field>
        <literalValue>0</literalValue>
        <name>Hotel booked clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_confirmation_clean</fullName>
        <field>Hotel_Confirmation__c</field>
        <name>Hotel confirmation clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_confirmation_date_clean</fullName>
        <field>Hotel_Confirmation_Date__c</field>
        <name>Hotel confirmation date clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_date_arrival_clean</fullName>
        <field>Hotel_Date_Arrival__c</field>
        <name>Hotel date arrival clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_date_departure_clean</fullName>
        <field>Hotel_Date_Departure__c</field>
        <name>Hotel date departure clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_extras_clean</fullName>
        <field>Hotel_Extras__c</field>
        <name>Hotel extras clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_list_date_clean</fullName>
        <field>Hotel_List_Date__c</field>
        <name>Hotel list date clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_list_new_change_clean</fullName>
        <field>Hotel_List_New_Change__c</field>
        <name>Hotel list new/change clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_number_clean</fullName>
        <field>Hotel_Number__c</field>
        <name>Hotel number clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_remarks_clean</fullName>
        <field>Hotel_Remarks__c</field>
        <name>Hotel remarks clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hotel_time_arrival_clean</fullName>
        <field>Hotel_Time_Arrival__c</field>
        <name>Hotel time arrival clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ICEF_Event_name_autofill</fullName>
        <field>ICEF_event_name_text__c</field>
        <formula>ICEF_Event__r.Name</formula>
        <name>ICEF Event name autofill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ICEF_event_Start_date_autofill</fullName>
        <field>Start_date_of_event1__c</field>
        <formula>ICEF_Event__r.Start_date__c</formula>
        <name>ICEF event Start date autofill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Internal_hotel_remarks_clean</fullName>
        <field>Internal_Hotel_Remarks__c</field>
        <name>Internal hotel remarks clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lockable_counter_clean</fullName>
        <field>Lockable_counter__c</field>
        <name>Lockable counter clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Logo_and_Slogan_on_Booth_ID_sign_clean</fullName>
        <field>Logo_and_Slogan_on_Booth_ID_sign__c</field>
        <literalValue>0</literalValue>
        <name>Logo and Slogan on Booth ID sign clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Logo_on_Booth_ID_sign_clean</fullName>
        <field>Logo_on_Booth_ID_sign__c</field>
        <literalValue>0</literalValue>
        <name>Logo on Booth ID sign clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Multimedia_Laptop_clean</fullName>
        <field>Multimedia_Laptop__c</field>
        <name>Multimedia Laptop clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Number_of_Persons_to_Dinner_clean</fullName>
        <field>Number_of_Persons_to_Dinner__c</field>
        <name>Number of Persons to Dinner clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Other_audiovisual_clean</fullName>
        <field>Other_audiovisual__c</field>
        <name>Other audiovisual clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Other_furniture_clean</fullName>
        <field>Other_furniture__c</field>
        <name>Other furniture clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Other_graphics_clean</fullName>
        <field>Other_graphics__c</field>
        <name>Other graphics clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Other_web_phone_equipment</fullName>
        <field>Other_web_phone_equipment__c</field>
        <name>Other web/phone equipment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Own_design_on_front_of_the_counter_clean</fullName>
        <field>Own_design_on_front_of_the_counter__c</field>
        <literalValue>0</literalValue>
        <name>Own design on front of the counter clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Parcels_clean</fullName>
        <field>Parcels__c</field>
        <name>Parcels clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Persons_attending_clean</fullName>
        <field>Contacts__c</field>
        <name>Persons attending clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Plasma_Display_32_clean</fullName>
        <field>Plasma_Display_32__c</field>
        <name>Plasma Display 32&apos;&apos; clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Plasma_Display_42_clean</fullName>
        <field>Plasma_Display_42__c</field>
        <name>Plasma Display 42&apos;&apos; clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Power_Strips_clean</fullName>
        <field>power_strips__c</field>
        <name>Power Strips clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pre_evenet_survey_clean</fullName>
        <field>Pre_event_survey_filled_out__c</field>
        <name>Pre evenet survey clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Refusal_sent_clean</fullName>
        <field>Refusal_sent__c</field>
        <name>Refusal sent clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Room_type_clean</fullName>
        <description>Field &apos;Room type&apos; is cleaned when a new AP is created.</description>
        <field>Room_Type__c</field>
        <name>Room type clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sponsorship_Package_clean</fullName>
        <description>Entry in Field &apos;Sponsorship Package&apos; is deleted when an AP is created.</description>
        <field>Sponsorship_Package__c</field>
        <name>Sponsorship Package clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Spotlights_clean</fullName>
        <field>spotlights__c</field>
        <name>Spotlights clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Date_autofill</fullName>
        <description>Field &apos;Status Date&apos; is filled with todays date.</description>
        <field>Status_Date__c</field>
        <formula>TODAY()</formula>
        <name>Status Date autofill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Date_clean</fullName>
        <field>Status_Date__c</field>
        <name>Status Date clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>T_Sign_printed_clean</fullName>
        <description>value of field &apos;T-Sign Printed&apos; is deleted when a new Ap is created.</description>
        <field>T_Sign_printed__c</field>
        <name>T-Sign printed clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Table_80x120_clean</fullName>
        <field>Table_80x120__c</field>
        <name>Table (80x120) clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Table_80x80_clean</fullName>
        <field>Table_80x80__c</field>
        <name>Table (80x80) clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Twitter_copy_value_from_Account</fullName>
        <field>Twitter__c</field>
        <formula>Account__r.Twitter__c</formula>
        <name>Twitter - copy value from Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Workshop_excursion_of_persons_clean</fullName>
        <field>Workshop_excursion_Number_of_persons__c</field>
        <name>Workshop excursion # of persons clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Youtube_copy_value_from_Account</fullName>
        <field>YouTube__c</field>
        <formula>Account__r.YouTube__c</formula>
        <name>Youtube - copy value from Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iPad_Holder_clean</fullName>
        <description>Field &apos;iPad Holder&apos; is emptied when am AP is created. Necessary for cloned APs</description>
        <field>iPad_Holder__c</field>
        <name>iPad Holder clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>no_own_booth_clean</fullName>
        <field>no_own_booth__c</field>
        <literalValue>0</literalValue>
        <name>no own booth clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_area_colour</fullName>
        <description>updates area colour depending on table/booth number</description>
        <field>Area_Colour_text__c</field>
        <formula>IF (
  ISNUMBER(Table_Booth_Number__c),
  IF(
   VALUE(Table_Booth_Number__c) &gt;=  ICEF_Event__r.First_Booth_Number_in_Area_Colour_Green__c ,
   &quot;green&quot;,
   IF (
    AND(
      VALUE(Table_Booth_Number__c) &gt;= ICEF_Event__r.First_Booth_Number_in_Area_Colour_Red__c,
      VALUE(Table_Booth_Number__c) &lt; ICEF_Event__r.First_Booth_Number_in_Area_Colour_Green__c
    ),
    &quot;red&quot;,
    IF(
      AND(
        VALUE(Table_Booth_Number__c) &gt;= ICEF_Event__r.First_Booth_Number_in_Area_Colour_Grey__c,
        VALUE(Table_Booth_Number__c) &lt; ICEF_Event__r.First_Booth_Number_in_Area_Colour_Red__c
      ),
      &quot;grey&quot;,
      &quot;blue&quot;
    )
   )
  ),
  IF(
    OR (
      BEGINS (Table_Booth_Number__c, &quot;KP&quot;),
      BEGINS (Table_Booth_Number__c, &quot;CH&quot;),
      BEGINS (Table_Booth_Number__c, &quot;TG&quot;),
      BEGINS (Table_Booth_Number__c, &quot;SB&quot;)
      ),
    &quot;green&quot;,
    IF (
      OR (
        BEGINS (Table_Booth_Number__c, &quot;TX&quot;),
        BEGINS (Table_Booth_Number__c, &quot;BV&quot;),
        BEGINS (Table_Booth_Number__c, &quot;GL&quot;),
        BEGINS (Table_Booth_Number__c, &quot;DA&quot;)
      ),
      &quot;red&quot;,
      IF(
        OR(
          AND(
            BEGINS (Table_Booth_Number__c, &quot;P&quot;),
            NOT (
              OR (
                BEGINS (Table_Booth_Number__c, &quot;P &quot;),
                BEGINS (Table_Booth_Number__c, &quot;P1&quot;),
                BEGINS (Table_Booth_Number__c, &quot;P2&quot;)
              )
            )
          ),
          BEGINS (Table_Booth_Number__c, &quot;KA&quot;)
        ),
        &quot;grey&quot;,
        &quot;blue&quot;
      )
    )
  )
)</formula>
        <name>update area colour</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AP created or edited</fullName>
        <actions>
            <name>Event_Manager_Agents_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Event_Manager_Educators_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ICEF_Event_name_autofill</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ICEF_event_Start_date_autofill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AP creation - no facebook info</fullName>
        <actions>
            <name>Facebook_copy_value_form_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.Facebook__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>An AP is created but field &apos;facebook&apos; in the new AP is blank.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP creation - no twitter info</fullName>
        <actions>
            <name>Twitter_copy_value_from_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.Twitter__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>An AP is created but field &apos;twitter&apos; in the new AP is blank.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP creation - no youtube info</fullName>
        <actions>
            <name>Youtube_copy_value_from_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.YouTube__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>An AP is created but field &apos;youtube&apos; in the new AP is blank.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP for agency with bad Account Status created</fullName>
        <actions>
            <name>AP_has_been_created_for_Agency_with_Status_Inactive_or_Blacklisted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>expression of interest,accepted,more info,wait listed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>equals</operation>
            <value>Inactive,Blacklisted (potential client),Blacklisted</value>
        </criteriaItems>
        <description>An AP has been created for an agency that has Account Status &apos;Inactive&apos; or &apos;Blacklisted&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP for agency with less than 4 references created</fullName>
        <actions>
            <name>Agency_with_less_than_4_pos_references_has_been_accepted_for_WS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Positive_References_Total__c</field>
            <operation>lessThan</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Start_date_of_event1__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>An AP has been set to accepted for an agency that has received less than 4 positive references</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP for agency with negative reference created</fullName>
        <actions>
            <name>AP_for_agency_with_negative_reference_has_been_created_and_Tiff_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>expression of interest,accepted,more info,wait listed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Negative_References__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>An AP has been cretaed for an agency that has received a negative reference in the past.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP for agent without website created</fullName>
        <actions>
            <name>An_agency_without_website_has_been_accepted_for_a_workshop</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Website</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Website_2__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>accepted</value>
        </criteriaItems>
        <description>An AP for agency without a website in its account info has been created and the ARM is informed about that.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP is created</fullName>
        <actions>
            <name>Field_Addendum_is_emptied</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_agreement_number_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_agreement_remarks_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_agreements_signed_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_appointment_number_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_appointment_remarks_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_business_needs_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_business_needs_remarks_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_date_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sponsorship_Package_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP is created %282%29</fullName>
        <actions>
            <name>Hotel_booked_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Hotel_date_arrival_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Hotel_date_departure_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Hotel_extras_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Hotel_list_new_change_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Hotel_remarks_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Internal_hotel_remarks_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Persons_attending_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_Date_autofill</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>T_Sign_printed_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This new rule is necessary as the other &apos;AP created&apos; rule has reached its limit of 10 field updates.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP is created %283%29</fullName>
        <actions>
            <name>Catalogue_Text_checked_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Hotel_confirmation_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Hotel_confirmation_date_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Hotel_list_date_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Hotel_number_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Hotel_time_arrival_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Parcels_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Refusal_sent_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Room_type_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>iPad_Holder_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This new rule is necessary as the other &apos;AP created&apos; rule has reached its limit of 10 field updates.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP is created %284%29</fullName>
        <actions>
            <name>Follow_up_Agreements_in_progress_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_Agreements_progress_no_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_Considerations_Clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_Fam_tour_feedback_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_Fam_tour_part_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_Negative_WS_behaviour_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_New_or_existing_partners_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_Students_sent_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_up_students_sent_to_who_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Workshop_excursion_of_persons_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP is created %285%29</fullName>
        <actions>
            <name>ARM_Hotel_Comments_Clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Booth_comments_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Primary_Rec_Country_Solved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Direct_print_on_booth_wall_panels_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Free_Airport_Transfer_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Logo_and_Slogan_on_Booth_ID_sign_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Logo_on_Booth_ID_sign_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Number_of_Persons_to_Dinner_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Other_graphics_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Own_design_on_front_of_the_counter_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will clean all entries in AP sections &apos;Booth details&apos; and &apos;Graphics&apos;</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP is created %286%29</fullName>
        <actions>
            <name>Brochure_display_rack_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Carpet_colour_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Carpet_sqm_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Chair_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>High_Table_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>High_stool_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lockable_counter_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Other_furniture_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Table_80x120_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Table_80x80_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will clean all entries in AP section &apos;Furniture&apos;</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP is created %287%29</fullName>
        <actions>
            <name>Booth_Signage_left_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Booth_Signage_right_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Counter_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Electricity_at_table_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>External_Booth_Comments_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Power_Strips_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Pre_evenet_survey_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Spotlights_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>no_own_booth_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP is created %288%29</fullName>
        <actions>
            <name>Analogue_telephone_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Beamer_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>DVD_Player_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Dustbins_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>High_speed_internet_5MB_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Multimedia_Laptop_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Other_audiovisual_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Other_web_phone_equipment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Plasma_Display_32_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Plasma_Display_42_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AP with active HRs is cancelled</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>canceled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Number_Hotel_Rooms_for_Accommodations__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>An AP that has active Hotel Rooms is cancelled and the Event Managers are alerted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>AP_with_active_Hotel_Rooms_is_cancelled_and_EMT_is_alerted</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Agent AP accepted but missing Students sent info</fullName>
        <actions>
            <name>Agent_AP_accepted_but_student_sent_info_is_missing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An AP for an agency has been accepted although there is missing info about students sent figures. The ARM is informed about that.</description>
        <formula>AND (
  ISPICKVAL ( Participation_Status__c , &quot;accepted&quot; ),
  OR (
    AND (
      OR (
        Event_Country__c = &quot;a1N200000008SIy&quot;,
        Event_Country__c = &quot;a1N200000008SLD&quot;
      ),
      Students_to_AUS_Total_Account__c = 0,
      Students_to_NZ_Total_Account__c = 0
    ),
    AND (
      OR (
        Event_Country__c = &quot;a1N200000008SFo&quot;,
        Event_Country__c = &quot;a1N200000008SJN&quot;
      ),
      Students_to_USA_Total_Account__c = 0,
      Students_to_CAN_Total_Account__c = 0
    ),
    AND (
      Event_Country__c &lt;&gt; &quot;a1N200000008SIy&quot;,
      Event_Country__c &lt;&gt; &quot;a1N200000008SLD&quot;,
      Event_Country__c &lt;&gt; &quot;a1N200000008SFo&quot;,
      Event_Country__c &lt;&gt; &quot;a1N200000008SJN&quot;, 
      Students_sent_Total_Account__c = 0
    )
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Agent AP accepted but missing info</fullName>
        <actions>
            <name>Agent_AP_accepted_but_essential_info_is_missing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>AN AP for an agency has been accepted although there is missing essential info. The ARM is informed about that.</description>
        <formula>AND (
  ISPICKVAL ( Participation_Status__c , &quot;accepted&quot; ),
  OR (
    ISBLANK ( Account__r.Year_of_Foundation__c ),
    ISBLANK ( Account__r.Staff_full_time__c ),
    ISBLANK ( Account__r.Main_customers__c ),
    ISBLANK ( Account__r.Promotional_Strategies__c ),
    ISBLANK ( Priority_Countries__c )
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Agent AP accepted but missing info about web check</fullName>
        <actions>
            <name>Agent_AP_accepted_but_web_check_info_is_missing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An AP for an agency has been accepted although there is missing info about web check. The ARM is informed about that.</description>
        <formula>AND (
  ISPICKVAL ( Participation_Status__c , &quot;accepted&quot; ),
  OR (
    ISBLANK ( Account__r.Web_check__c ),
    ISBLANK (  Account__r.Web_check_date__c ),
    Account__r.Web_check_date__c  &lt; TODAY() - 365
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cancellation of Agency AP</fullName>
        <actions>
            <name>Cancellation_of_Agency_AP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>canceled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cancellation or Transfer of Booking - Americas</fullName>
        <actions>
            <name>A_Booking_has_been_cancelled_and_Sales_Rep_is_informed_Americas</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>canceled,transferred</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Agent,Guest Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Territory__c</field>
            <operation>equals</operation>
            <value>Americas</value>
        </criteriaItems>
        <description>Notifies Sales Mangers and Finance except Cathy  when an Account cancels or transfers.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cancellation or Transfer of Booking - Asia%2FPac</fullName>
        <actions>
            <name>A_Booking_has_been_cancelled_and_Sales_Rep_is_informed_Asia_Pac</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Alert_Finance_when_an_account_cancels_or_transfers_Asia_Pacific</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>canceled,transferred</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Agent,Guest Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Territory__c</field>
            <operation>equals</operation>
            <value>Asia/Pac</value>
        </criteriaItems>
        <description>Notifies Sales Mangers and Finance  when an Account cancels or transfers.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cancellation or Transfer of Booking - EMEA</fullName>
        <actions>
            <name>A_Booking_has_been_cancelled_and_Sales_Rep_is_informed_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>canceled,transferred</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Agent,Guest Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Territory__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <description>Notifies Sales Mangers and Finance except Cathy  when an Account cancels or transfers.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Catalogue Address Autofill</fullName>
        <actions>
            <name>Catalogue_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Catalogue_Department</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Catalogue_Email1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Catalogue_Fax</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Catalogue_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Catalogue_Post_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Catalogue_State_Province</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Catalogue_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Catalogue_Tel</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Catalogue_Website</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.Catalogue_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This rule fills the Catalogue address fields with the mailing address fileds from Account when a new AP is created. If Catalogue Name is filled out, the rule doesn&apos;t work (to avoid overwriting when cloning APs).</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Country Section incorrect</fullName>
        <actions>
            <name>Country_Section_incorrect_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.Country_section_correct__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Start_date_of_event1__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>accepted</value>
        </criteriaItems>
        <description>Country Section is incorrect and Tiffany is alerted about that.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event booths fully booked</fullName>
        <actions>
            <name>Event_Booths_fully_booked_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An AP for a WS booth has been created and now all WS booths are sold</description>
        <formula>AND (
  ICEF_Event__r.Exhibitor_Booths_still_available__c =1,
  ISPICKVAL (Table_Booth_type__c , &quot;Booth&quot;),
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;01220000000Q8OV&quot;,
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;0120J000000YfcG&quot;,
  OR (
    RecordType.Name = &quot;Educator&quot;,
    RecordType.Name = &quot;Exhibitor&quot;,
    RecordType.Name = &quot;Work and Travel&quot;
  ),
  OR (
    ISPICKVAL ( Participation_Status__c , &quot;registered&quot;),
    ISPICKVAL ( Participation_Status__c , &quot;pending payment&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event booths overbooked</fullName>
        <actions>
            <name>Exhibitor_Booth_overbooked_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An AP for a WS booth has been created although all WS booths are sold</description>
        <formula>AND (
  ICEF_Event__r.Exhibitor_Booths_still_available__c  &lt;=0,
  ISPICKVAL (Table_Booth_type__c , &quot;Booth&quot;),
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;01220000000Q8OV&quot;, 
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;0120J000000YfcG&quot;, 
  OR (
    RecordType.Name = &quot;Educator&quot;,
    RecordType.Name = &quot;Exhibitor&quot;,
    RecordType.Name = &quot;Work and Travel&quot;
  ),
  OR (
    ISPICKVAL ( Participation_Status__c , &quot;registered&quot;),
    ISPICKVAL ( Participation_Status__c , &quot;pending payment&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event exhibit tables fully booked</fullName>
        <actions>
            <name>Event_exhibit_tables_fully_booked_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An AP for an exhibit table has been created and now all exhibit tables are sold for this event</description>
        <formula>AND (
  ICEF_Event__r.Exhibitor_Tables_still_available__c =1,
  ISPICKVAL (Table_Booth_type__c , &quot;Exhibit Table&quot;),
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;01220000000Q8OV&quot;, 
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;0120J000000YfcG&quot;, 
  OR (
    RecordType.Name = &quot;Educator&quot;,
    RecordType.Name = &quot;Exhibitor&quot;,
    RecordType.Name = &quot;Work and Travel&quot;
  ),
  OR (
    ISPICKVAL ( Participation_Status__c , &quot;registered&quot;),
    ISPICKVAL ( Participation_Status__c , &quot;pending payment&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event exhibit tables overbooked</fullName>
        <actions>
            <name>Event_exhibit_tables_overbooked_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An AP for an exhibit table has been created although all exhibit tables are sold for this event</description>
        <formula>AND (
  ICEF_Event__r.Exhibitor_Tables_still_available__c &lt;=0,
  ISPICKVAL (Table_Booth_type__c , &quot;Exhibit Table&quot;),
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;01220000000Q8OV&quot;, 
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;0120J000000YfcG&quot;, 
  OR (
    RecordType.Name = &quot;Educator&quot;,
    RecordType.Name = &quot;Exhibitor&quot;,
    RecordType.Name = &quot;Work and Travel&quot;
  ),
  OR (
    ISPICKVAL ( Participation_Status__c , &quot;registered&quot;),
    ISPICKVAL ( Participation_Status__c , &quot;pending payment&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event tables fully booked</fullName>
        <actions>
            <name>Event_Tables_fully_booked_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An AP for a WS table has been created and now all WS tables are sold</description>
        <formula>AND (
  ICEF_Event__r.Tables_still_available__c =1,
  ISPICKVAL (Table_Booth_type__c , &quot;Table&quot;),
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;01220000000Q8OV&quot;, 
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;0120J000000YfcG&quot;, 
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;01220000000Qw0O&quot;,
  OR (
    RecordType.Name = &quot;Educator&quot;,
    RecordType.Name = &quot;Exhibitor&quot;,
    RecordType.Name = &quot;Work and Travel&quot;
  ),
  OR (
    ISPICKVAL ( Participation_Status__c , &quot;registered&quot;),
    ISPICKVAL ( Participation_Status__c , &quot;pending payment&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event tables overbooked</fullName>
        <actions>
            <name>Event_tables_overbooked_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An AP for a WS table has been created although all WS tables are sold</description>
        <formula>AND (
  ICEF_Event__r.Tables_still_available__c &lt;=0,
  ISPICKVAL (Table_Booth_type__c , &quot;Table&quot;),
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;01220000000Q8OV&quot;, 
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;0120J000000YfcG&quot;, 
  ICEF_Event__r.RecordTypeId &lt;&gt; &quot;01220000000Qw0O&quot;,
  OR (
    RecordType.Name = &quot;Educator&quot;,
    RecordType.Name = &quot;Exhibitor&quot;,
    RecordType.Name = &quot;Work and Travel&quot;
  ),
  OR (
    ISPICKVAL ( Participation_Status__c , &quot;registered&quot;),
    ISPICKVAL ( Participation_Status__c , &quot;pending payment&quot;)
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Exhibitor Miami 2018 has changed Catalogue Name</fullName>
        <actions>
            <name>Toronto_2018_Exhibitor_Catalogue_Name_change_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An Exhibitor participating in Miami2018 has changed its Catalogue Name and Marlene is informed to make sure that booth signage and Catalogue Name are the same</description>
        <formula>AND (
  ICEF_Event__r.Name  = &quot;NA Miami 2018&quot;,
  OR (
    ISPICKVAL (Participation_Status__c, &quot;registered&quot;),
    ISPICKVAL (Participation_Status__c, &quot;pending payment&quot;)
  ),
  OR (
    ISPICKVAL (Table_Booth_type__c , &quot;Booth&quot;),
    ISPICKVAL (Table_Booth_type__c , &quot;Exhibit Table&quot;),
    ISPICKVAL (Table_Booth_type__c , &quot;Table in Foyer&quot;),
    ISPICKVAL (Table_Booth_type__c , &quot;Agent Lounge&quot;)
  ),
  ISCHANGED (Catalogue_Name__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Fill Area Colour on change of Table%2FBooth Number</fullName>
        <actions>
            <name>update_area_colour</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Table/Booth number changed, fill Area colour with correct value</description>
        <formula>AND(
  OR(
    ISCHANGED(Table_Booth_Number__c),
    ISCHANGED(Area_Colour_recalculate__c)
  ),
  Agent_or_Provider__c = &quot;Provider&quot;,
  OR (
    CONTAINS(ICEF_Event__r.Name, &quot;Berlin&quot;),
    CONTAINS(ICEF_Event__r.Name, &quot;Miami&quot;)
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inform Sales Manager when booking %2F CAN</fullName>
        <actions>
            <name>An_Account_Participation_has_been_created_and_Sales_Manager_is_informed_CAN</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>registered,pending payment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.MailingCountry__c</field>
            <operation>equals</operation>
            <value>Canada</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>Sarah Mines</value>
        </criteriaItems>
        <description>The appropriate Sales Manager (Sarah) is informed, when the Acount participation of an Account has been created with the status &apos;Registered&apos;. Canada only.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Inform Sales Manager when booking %2F Harald</fullName>
        <actions>
            <name>An_Account_Participation_has_been_created_and_Sales_Manager_is_informed_Harald</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>registered,pending payment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.MailingCountry__c</field>
            <operation>notEqual</operation>
            <value>USA,Canada</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>Liz Adams,Cathy Hearps,Gavin Hopper,Myriam Marchand,Ashley Wood,Harald Kreiner</value>
        </criteriaItems>
        <description>The Sales Manager of the Account Owner (Harald) is informed, when the Acount participation of an Account has been created with the status &apos;Registered&apos; or &quot;Pending Payment&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Inform Sales Manager when booking %2F Rod</fullName>
        <actions>
            <name>An_Account_Participation_has_been_created_and_Sales_Manager_is_informed_Rod</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>registered,pending payment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>Liz Adams,Cathy Hearps,Gavin Hopper,Myriam Marchand,Ashley Wood</value>
        </criteriaItems>
        <description>The appropriate Sales Manager (Rod) is informed, when the Acount participation of an Account has been created with the status &apos;Registered&apos; or &quot;Pending Payment&quot;. Rod&apos;s Team.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Inform Sales Manager when booking %2F USA</fullName>
        <actions>
            <name>An_Account_Participation_has_been_created_and_Sales_Manager_is_informed_USA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>registered,pending payment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.MailingCountry__c</field>
            <operation>equals</operation>
            <value>USA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>Ian Cann</value>
        </criteriaItems>
        <description>The appropriate Sales Manager (Ian) is informed, when the Acount participation of an Account has been created with the status &apos;Registered&apos;. USA only.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Negative behavious agency has a new AP</fullName>
        <actions>
            <name>Agency_with_negative_behaviour_at_a_previous_WS_has_a_new_AP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>expression of interest,accepted,more info,wait listed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Negative_workshop_behaviour_on_WSs__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>An agency that has an entry for misbehaviour at a previous workshops has a new AP with optimistic participation status.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Territory Manager when Unassigned Account books an event</fullName>
        <actions>
            <name>Unassigned_Account_booked_an_event</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>registered,pending payment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>Markus Hebeler</value>
        </criteriaItems>
        <description>Notify Territory Manager when Unassigned Account books an event.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Participant and Catalogue have been filled out text request</fullName>
        <actions>
            <name>Workshop_participants_have_been_designated_via_online_form</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The online form &apos;Workshop Participants&apos; has been filled out and the Account Owner is informed about that.</description>
        <formula>ISCHANGED ( Participants_announced_webform_date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sponsor pack has been sold</fullName>
        <actions>
            <name>Sponsorship_package_has_been_booked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Participation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Exhibitor,Educator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Participation_Status__c</field>
            <operation>equals</operation>
            <value>registered,pending payment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Participation__c.Sponsorship_Package__c</field>
            <operation>equals</operation>
            <value>Platinum,Gold,Silver</value>
        </criteriaItems>
        <description>When an AP contains the info that a sponsorship package has been sold then an alert is sent out to Korinne.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status Change of Agency%3A not accepted any longer</fullName>
        <actions>
            <name>AP_of_agency_has_lost_status_accepted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When the status of an agency changes from &apos;accepted&apos; to any other status then the Event Manager is informed about this.</description>
        <formula>AND (
ISPICKVAL (PRIORVALUE ( Participation_Status__c ), &quot;accepted&quot;),
NOT (ISPICKVAL (Participation_Status__c , &quot;accepted&quot;)),
NOT (ISPICKVAL (Participation_Status__c , &quot;canceled&quot;)),
RecordTypeId =&quot;012200000005Dz8&quot;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status change from Pending payment to registered</fullName>
        <actions>
            <name>AP_status_has_changed_from_pending_payment_to_registered</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When the Status of an AP is changed from pending payment to registered, then the apropriate event manager needs to be informed about that.</description>
        <formula>AND (
ISPICKVAL( Participation_Status__c, &quot;registered&quot;), 
 OR (
 ISPICKVAL (PRIORVALUE(  Participation_Status__c  ), &quot;pending payment&quot;),
 ISPICKVAL (PRIORVALUE(  Participation_Status__c  ), &quot;transferred&quot;),
 ISPICKVAL (PRIORVALUE(  Participation_Status__c  ), &quot;canceled&quot;)
),
(ICEF_Event__r.Start_date__c - 120) &lt; TODAY ()
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>An_Account_has_cancelled_or_tranferred</fullName>
        <assignedTo>cathy@edmedia.info</assignedTo>
        <assignedToType>user</assignedToType>
        <description>An account has cancelled or transferred - please either issue a credit note or issue a credit note and a new invoice if the Account has transferred.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>An Account has cancelled or transferred</subject>
    </tasks>
    <tasks>
        <fullName>An_Account_has_cancelled_or_transferred</fullName>
        <assignedTo>pruehseler@icef.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>One Account has cancelled. Please issue a credit note and if they have transfered a new invoice - you will need to find out which event they have transferred to.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>An Account has cancelled or transferred</subject>
    </tasks>
    <tasks>
        <fullName>Enter_into_IOW</fullName>
        <assignedTo>lstevens@icef.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Someone has registered for an upcoming event. Please set-up them up in IOW if they are not already registered and update their workshop attendance even if already registered for IOW.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Enter into IOW</subject>
    </tasks>
    <tasks>
        <fullName>New_order_please_ensure_Opportunity_closed_won_and_ready_for_billing</fullName>
        <assignedToType>owner</assignedToType>
        <description>An Account Participation has been created for your client as a result of an order being placed. Should you require a copy of the order then please ask your Territory manager. Please close the Opportunity ready for billing.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Account_Participation__c.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New order - please ensure Opportunity closed won and ready for billing</subject>
    </tasks>
    <tasks>
        <fullName>Reminder_to_check_customers_eSchedulePro_account_to_see_if_they_need_some_help_s</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please check this customer&apos;s eSchedulePro account to see that the customer is using it and has set up enough meetings. If not then please contact the customer and offer your assistance.</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Account_Participation__c.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Reminder to check customers&apos; eSchedulePro account to see if they need some help setting up meetings</subject>
    </tasks>
</Workflow>
