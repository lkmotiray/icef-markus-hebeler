<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Exhibitor_Product_has_been_booked_Isabel_is_alerted</fullName>
        <description>Exhibitor Product has been booked - Isabel is alerted</description>
        <protected>false</protected>
        <recipients>
            <recipient>ivogt@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Exhibitor_Product_has_been_booked</template>
    </alerts>
    <alerts>
        <fullName>PP_Alert_to_Event_Manager_Agents_for_specific_products</fullName>
        <description>PP Alert to Event Manager Agents for specific products</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMT_alerts/PP_Alert_for_Agents_Event_Manager</template>
    </alerts>
    <alerts>
        <fullName>PP_status_has_changed_from_pending_payment_to_registered</fullName>
        <description>PP status has changed from pending payment to registered</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Payment_for_PP_has_been_entered</template>
    </alerts>
    <fieldUpdates>
        <fullName>Event_Manager_Agent_Email_update</fullName>
        <field>Event_Managers_Agents_Email__c</field>
        <formula>ICEF_Event__r.Event_Manager_Agents__r.Email</formula>
        <name>Event Manager Agent Email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Managers_Email_filled</fullName>
        <field>Event_Managers_Email__c</field>
        <formula>ICEF_Event__r.Event_Manager_Educators__r.Email</formula>
        <name>Event Managers Email filled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Exhibitor Product has been booked</fullName>
        <actions>
            <name>Exhibitor_Product_has_been_booked_Isabel_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product_Participation__c.Product_status__c</field>
            <operation>notEqual</operation>
            <value>cancelled,transferred,available</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Participation__c.Product_Name__c</field>
            <operation>notContain</operation>
            <value>Bag insert,Bag tags,advertisement,Logo on eSchedulePRO,front cover,back cover</value>
        </criteriaItems>
        <description>An Exhibitor Product has been booked for an upcoming WS. Isabel is alerted.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify by Task Sales Owner when Account orders a product for an event</fullName>
        <actions>
            <name>New_product_order_please_ensure_Opportunity_closed_won_and_ready_for_billing</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Product_Participation__c.Product_status__c</field>
            <operation>equals</operation>
            <value>registered,pending payment</value>
        </criteriaItems>
        <description>Notifies sales people when a Product Participation is created.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PP Status change from Pending payment to registered</fullName>
        <actions>
            <name>PP_status_has_changed_from_pending_payment_to_registered</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When the Status of a PP is changed from pending payment to registered, then the apropriate event manager needs to be informed about that.</description>
        <formula>AND ( 
ISPICKVAL( Product_status__c, &quot;registered&quot;), 
OR ( 
ISPICKVAL (PRIORVALUE( Product_status__c ), &quot;pending payment&quot;), 
ISPICKVAL (PRIORVALUE( Product_status__c ), &quot;transferred&quot;), 
ISPICKVAL (PRIORVALUE( Product_status__c ), &quot;cancelled&quot;) 
), 
(ICEF_Event__r.Start_date__c - 120) &lt; TODAY () 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PP has been registered - EM Agent Alert</fullName>
        <actions>
            <name>PP_Alert_to_Event_Manager_Agents_for_specific_products</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 and (2 or 3 or 4 or 5 or 6)</booleanFilter>
        <criteriaItems>
            <field>Product_Participation__c.Product_status__c</field>
            <operation>equals</operation>
            <value>pending payment,registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Participation__c.Product_Name__c</field>
            <operation>equals</operation>
            <value>Agent lounge in dedicated room,ANZ Client Lunch Sponsorship,Bag tags (agent bags),Bag tags (both bags),Bag tags (provider bags),Branded water bottles (agents only),Branding of Marlene bar,First-timer Lunch</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Participation__c.Product_Name__c</field>
            <operation>equals</operation>
            <value>Gold Sponsorship - Recognition as event sponsor,Hotel Key cards/sleeves,Info-point sponsorship,Internet Hub,Logo on catalogue USB sticks,Neck cords,Note pads,Onsite registration - Agents packs,Onsite registration - Both packs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Participation__c.Product_Name__c</field>
            <operation>equals</operation>
            <value>Onsite registration - Providers packs,Platinum Sponsorship - Recognition as event sponsor,Pens,Pens and note pads,Refreshment break sponsorship - Day 1,Refreshment break sponsorship - Day 2,Refreshment break sponsorship - Registration Day</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Participation__c.Product_Name__c</field>
            <operation>equals</operation>
            <value>Room Drops to Agents - Day 1,Room Drops to Agents - Day 2,Room Drops to Agents - Registration Day,Silver Sponsorship - Recognition as event sponsor,Welcome reception sponsorship,Workshop Dinner Reception sponsorship</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Participation__c.Product_Name__c</field>
            <operation>equals</operation>
            <value>Workshop lunch sponsorship - Day 1,Workshop lunch sponsorship - Day 2,Workshop lunch sponsorship - Registration Day</value>
        </criteriaItems>
        <description>A PP has been registered and the appropriate EVent Manager Agents is informed. Relevant for specific products only.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PP is created or edited</fullName>
        <actions>
            <name>Event_Manager_Agent_Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Event_Managers_Email_filled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule triggers every time a change to the PP is made</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>New_product_order_please_ensure_Opportunity_closed_won_and_ready_for_billing</fullName>
        <assignedToType>owner</assignedToType>
        <description>A Product Participation has been set up for one of your customers. 

If the Opportunity has not been set up already or is not Closed Won then please set up a Closed Won Opportunity with the appropriate Product details.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New product order - please ensure Opportunity closed won and ready for billing</subject>
    </tasks>
</Workflow>
