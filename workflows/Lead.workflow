<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_client_who_filled_Pricer_Quote_Request_Form_NOT_personalised</fullName>
        <description>Email to client who filled Pricer Quote Request Form (NOT personalised)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Emails/Pricer_Quote_Request_Email_NOT_personalised</template>
    </alerts>
    <alerts>
        <fullName>Email_to_client_who_filled_Pricer_Quote_Request_Form_Personalised</fullName>
        <description>Email to client who filled Pricer Quote Request Form (personalised)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Emails/Pricer_Quote_Request_Email_personalised</template>
    </alerts>
    <alerts>
        <fullName>Email_to_client_who_filled_Pricer_Registration_form_from_Lead_Owner</fullName>
        <description>Email to client who filled Pricer Registration form (from Lead Owner)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Emails/Pricer_Register_Email_from_Lead_Owner</template>
    </alerts>
    <alerts>
        <fullName>Email_to_client_who_filled_Pricer_Registration_form_from_Territory_Manager</fullName>
        <description>Email to client who filled Pricer Registration form (from Territory Manager)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Emails/Pricer_Register_Email_from_Territory_Manager</template>
    </alerts>
    <alerts>
        <fullName>New_Lead_from_Price_Quote_Alert_to_Owner</fullName>
        <description>New Lead from Price Quote - Alert to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Emails/New_Lead_from_Price_Quote_Request</template>
    </alerts>
    <alerts>
        <fullName>Very_Hot_Lead_Alert_to_Lead_Owner</fullName>
        <description>Very Hot Lead Alert to Lead Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Emails/New_Very_Hot_Lead</template>
    </alerts>
    <fieldUpdates>
        <fullName>Email_update</fullName>
        <description>Field &apos;Email&apos; is filled with value of field &apos;Contact Email&apos;</description>
        <field>Email</field>
        <formula>Contact_Email__c</formula>
        <name>Email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ICEF_Monitor_subscriber_date_clean</fullName>
        <field>ICEF_Monitor_subscriber_date__c</field>
        <name>ICEF Monitor subscriber date clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Fill Email field</fullName>
        <actions>
            <name>Email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Email</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field &apos;Email&apos; is filled with value from &apos;Contact Email&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ICEF Monitor subscriber date update</fullName>
        <actions>
            <name>ICEF_Monitor_subscriber_date_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.ICEF_Monitor_subscriber__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
