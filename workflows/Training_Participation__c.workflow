<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>TP_changes_for_AIRC_member_Alert_Tiffany</fullName>
        <description>TP changes for AIRC member: Alert Tiffany</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/TP_for_AIRC_member_has_been_created_or_edited</template>
    </alerts>
    <alerts>
        <fullName>Test_Registration_has_been_paid_and_Owner_is_alerted</fullName>
        <description>Test Registration has been paid and Owner is alerted</description>
        <protected>false</protected>
        <recipients>
            <field>TP_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Test_Registration_has_been_paid</template>
    </alerts>
    <fieldUpdates>
        <fullName>No_of_Max_Points_as_in_Training_Type</fullName>
        <description>The field &apos;Maximum Points&apos; in the TP object is filled with the appropriate value in Training type. We don&apos;t use a formula field in case the number of max points is changed somewhen in the Training Type.</description>
        <field>Maximum_Points__c</field>
        <formula>Training_Type__r.Maximum_points__c</formula>
        <name>No of Max. Points as in Training Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TP_Owner_email_address_update</fullName>
        <description>Email address of TP owner is updated in TP</description>
        <field>TP_Owner_Email__c</field>
        <formula>TP_Owner__r.Email</formula>
        <name>TP Owner email address update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AIRC member TP changes</fullName>
        <actions>
            <name>TP_changes_for_AIRC_member_Alert_Tiffany</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Participation__c.AIRC_membership_of_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Training_Type__c.Certificate_Name__c</field>
            <operation>equals</operation>
            <value>USCG</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TP is created</fullName>
        <actions>
            <name>No_of_Max_Points_as_in_Training_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule is triggered whenever a new Training Participation instance is created.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TP is created or edited</fullName>
        <actions>
            <name>TP_Owner_email_address_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>OBSOLETE</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Test Registration has been paid</fullName>
        <actions>
            <name>Test_Registration_has_been_paid_and_Owner_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Training_Participation__c.Payment_Status__c</field>
            <operation>equals</operation>
            <value>PAID</value>
        </criteriaItems>
        <description>OBSOLETE (replaced with Flow) The TP is marked that the registration fee has been paid and Account owner is alerted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
