<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Evaluation_for_TP_has_been_created_and_Tiffany_is_alerted</fullName>
        <description>Evaluation for TP has been created and Tiffany is alerted</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Communication_Training_Feedback/Agent_training_feedback_has_been_given</template>
    </alerts>
    <alerts>
        <fullName>Negative_evaluation_comment</fullName>
        <description>Negative evaluation comment</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Negative_Workshop_Comment</template>
    </alerts>
    <alerts>
        <fullName>New_evaluation_form_has_been_entered</fullName>
        <description>New evaluation form has been entered</description>
        <protected>false</protected>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/New_evaluation_form_has_been_filled_out</template>
    </alerts>
    <rules>
        <fullName>Negative evaluation comment</fullName>
        <actions>
            <name>Negative_evaluation_comment</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Evaluation__c.Rating_of_Comment__c</field>
            <operation>equals</operation>
            <value>Negative</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New evaluation has been entered</fullName>
        <actions>
            <name>New_evaluation_form_has_been_entered</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Evaluation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>WS Evaluation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Evaluation__c.Rating_of_Comment__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Evaluation__c.Evaluation_Comments__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>A new evaluation has been entered and must be checked for comments.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TP evaluation has been created</fullName>
        <actions>
            <name>Evaluation_for_TP_has_been_created_and_Tiffany_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Evaluation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>TP Evaluation</value>
        </criteriaItems>
        <description>An evaluation for a Training participation has been created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
