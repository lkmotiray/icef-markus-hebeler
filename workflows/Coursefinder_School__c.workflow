<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_CF_school_has_been_allocated_to_a_different_account_CF_team_is_alerted</fullName>
        <description>A CF school has been allocated to a different account. CF team is alerted</description>
        <protected>false</protected>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/DB_update_CF_School_has_other_Account_now</template>
    </alerts>
    <alerts>
        <fullName>Alert_when_a_CoursePricer_ID_is_changed_on_a_CF_school_profile</fullName>
        <description>Alert when a CoursePricer ID is changed on a CF school profile</description>
        <protected>false</protected>
        <recipients>
            <recipient>pruffing@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rholmes@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Coursefinder_Communication/Alert_if_CP_ID_updated</template>
    </alerts>
    <alerts>
        <fullName>New_CF_school_has_been_created_alert_to_CF_team</fullName>
        <description>New CF school has been created - alert to CF team</description>
        <protected>false</protected>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pschmitz@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/DB_update_New_CF_school_has_been_created</template>
    </alerts>
    <alerts>
        <fullName>New_CF_school_is_not_marked_as_to_publish</fullName>
        <description>New CF school is not marked as &apos;to publish&apos;</description>
        <protected>false</protected>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/New_CF_school_is_not_marked_to_publish</template>
    </alerts>
    <fieldUpdates>
        <fullName>auto_tick_To_Publish</fullName>
        <field>To_Publish__c</field>
        <literalValue>1</literalValue>
        <name>auto-tick To Publish</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>remove_Delete_date</fullName>
        <field>Delete__c</field>
        <name>remove Delete date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>remove_Deletion_Approved</fullName>
        <field>Deletion_Approved__c</field>
        <literalValue>0</literalValue>
        <name>remove Deletion Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>remove_deleting_reason</fullName>
        <field>Deleting_Reason__c</field>
        <name>remove deleting reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert if CP ID changed</fullName>
        <actions>
            <name>Alert_when_a_CoursePricer_ID_is_changed_on_a_CF_school_profile</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Coursefinder_School__c.CoursePricer_School_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Alerts Ross and Patrick if the CP ID is changed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CF school is allocated to different account</fullName>
        <actions>
            <name>A_CF_school_has_been_allocated_to_a_different_account_CF_team_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An existing CF school in SF has been allocated to a different account.</description>
        <formula>ISCHANGED( Account__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New CF school has been created</fullName>
        <actions>
            <name>New_CF_school_has_been_created_alert_to_CF_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>A new CF school has been created.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New CF school is not marked to publish</fullName>
        <actions>
            <name>New_CF_school_is_not_marked_as_to_publish</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Coursefinder_School__c.To_Publish__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>A Coursefinder School has been created, but field &apos;To publish&apos; has not been ticked.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>unhide hidden school has been filled</fullName>
        <actions>
            <name>auto_tick_To_Publish</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>remove_Delete_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>remove_Deletion_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>remove_deleting_reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Coursefinder_School__c.Unhide_hidden_school__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>if there is a timestamp in field &quot;unhide hidden school&quot;, fields Delete and Deleting reason are emptied to prevent school from being re-hidden by sync</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
