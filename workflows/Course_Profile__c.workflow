<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Course_profile_webform_filled_out_Alert_to_Owner</fullName>
        <description>Course profile webform filled out - Alert to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Course_Profile_Online_Form_has_been_filled_out1</template>
    </alerts>
    <fieldUpdates>
        <fullName>Course_Profile_AG_HE</fullName>
        <description>To mark the checkbox of field  &apos;Agents HE&apos; when the agency is interested in any undergraduate, graduate or  postgraduate courses in this profile.</description>
        <field>Agents_HE__c</field>
        <literalValue>1</literalValue>
        <name>Course Profile AG HE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Profile_AG_additional</fullName>
        <description>To mark the checkbox of field  &apos;Agents additional&apos; when the agency is interested in any additional course or service in this profile.</description>
        <field>Agents_additional__c</field>
        <literalValue>1</literalValue>
        <name>Course Profile AG additional</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Profile_AG_career</fullName>
        <description>To mark the checkbox of field  &apos;Agents career&apos; when the agency is interested in any career/vocational course in this profile.</description>
        <field>Agents_career__c</field>
        <literalValue>1</literalValue>
        <name>Course Profile AG career</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Profile_AG_doctorate</fullName>
        <description>To mark the checkbox of field  &apos;Agents doctorate&apos; when the agency is interested in any doctoral course in this profile.</description>
        <field>Agents_doctorate__c</field>
        <literalValue>1</literalValue>
        <name>Course Profile AG doctorate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Profile_AG_language</fullName>
        <description>To mark the checkbox of field  &apos;Agents language&apos; when the agency is interested in any language course in this profile.</description>
        <field>Agents_language__c</field>
        <literalValue>1</literalValue>
        <name>Course Profile AG language</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Profile_AG_masters</fullName>
        <description>To mark the checkbox of field  &apos;Agents graduate&apos; when the agency is interested in any master course in this profile.</description>
        <field>Agents_masters__c</field>
        <literalValue>1</literalValue>
        <name>Course Profile AG masters</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Profile_AG_secondary</fullName>
        <description>To mark the checkbox of field  &apos;Agents secondary&apos; when the agency is interested in any secondary or high school programme in this profile.</description>
        <field>Agents_secondary__c</field>
        <literalValue>1</literalValue>
        <name>Course Profile AG secondary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Profile_AG_undergraduate</fullName>
        <description>To mark the checkbox of field  &apos;Agents undergraduate&apos; when the agency is interested in any undergraduate course in this profile.</description>
        <field>Agents_undergraduate__c</field>
        <literalValue>1</literalValue>
        <name>Course Profile AG undergraduate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Profile_Educators_HE</fullName>
        <description>To check the checkbox &apos;Educators HE&apos; in object &apos;Course profile&apos; when the account offers any HE course.</description>
        <field>Educators_HE__c</field>
        <literalValue>1</literalValue>
        <name>Course Profile Educators HE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_profile_AG_graduate_certificate</fullName>
        <description>To mark the checkbox of field &apos;Agents graduate certificate&apos; when the agency is interested in any graduate/postgraduate certificate/diploma course in this profile.</description>
        <field>Agents_graduate_certificate__c</field>
        <literalValue>1</literalValue>
        <name>Course profile AG graduate certificate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tickbox_Blended_Learning_Clean</fullName>
        <field>Blended_Learning__c</field>
        <literalValue>0</literalValue>
        <name>Tickbox Blended Learning Clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tickbox_Blended_Learning_Tick</fullName>
        <field>Blended_Learning__c</field>
        <literalValue>1</literalValue>
        <name>Tickbox Blended Learning Tick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tickbox_Online_Learning_Clean</fullName>
        <field>Online_Learning__c</field>
        <literalValue>0</literalValue>
        <name>Tickbox Online Learning Clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tickbox_Online_Learning_Tick</fullName>
        <field>Online_Learning__c</field>
        <literalValue>1</literalValue>
        <name>Tickbox Online Learning Tick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tickbox_Traditional_Learning_Clean</fullName>
        <field>Traditional_Learning__c</field>
        <literalValue>0</literalValue>
        <name>Tickbox Traditional Learning Clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tickbox_Traditional_Learning_Tick</fullName>
        <field>Traditional_Learning__c</field>
        <literalValue>1</literalValue>
        <name>Tickbox Traditional Learning Tick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Course Profile AG HE</fullName>
        <actions>
            <name>Course_Profile_AG_HE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5 OR 6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Undergraduate_Degree_Bachelor__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Undergraduate_Degree_Bachelor__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Graduate_Postgraduate_Masters__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Graduate_Postgraduate_Masters__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Graduate_Postgraduate_Doctorate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Graduate_Postgraduate_Doctorate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To mark the checkbox of field  &apos;Agents HE&apos; when the agency is interested in any higher education course in this profile.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Profile AG additional</fullName>
        <actions>
            <name>Course_Profile_AG_additional</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Additional_programmes_and_services__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_additional_programmes_and_services__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To mark the checkbox of field  &apos;Agents additional&apos; when the agency is interested in any additional course or servicein this profile.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Profile AG career</fullName>
        <actions>
            <name>Course_Profile_AG_career</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Career_Vocational_Certificate_Diploma__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Career_VocationalCertificate_Dipl__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To mark the checkbox of field  &apos;Agents career&apos; when the agency is interested in any career/vocational course in this profile.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Profile AG doctorate</fullName>
        <actions>
            <name>Course_Profile_AG_doctorate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Graduate_Postgraduate_Doctorate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Graduate_Postgraduate_Doctorate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To mark the checkbox of field  &apos;Agents doctorate&apos; when the agency is interested in any postgraduate course in this profile.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Profile AG graduate certificate</fullName>
        <actions>
            <name>Course_profile_AG_graduate_certificate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Graduate_PostgraduateCertificate_Diploma__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Graduate_PostgradCertificate_Dipl__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To mark the checkbox of field  &apos;Agents graduate certificate&apos; when the agency is interested in any graduate/postgraduate certificate/diploma course in this profile.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Profile AG language</fullName>
        <actions>
            <name>Course_Profile_AG_language</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 or 9 or 10 or 11 or 12 or 13 or 14)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_Arabic_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_Chinese_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_English_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_French_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_German_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_Italian_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_Japanese_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_Korean_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_Portuguese_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_Russian_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_Spanish_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Language_Other_in__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_types_of_Language_Course__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To mark the checkbox of field  &apos;Agents language&apos; when the agency is interested in any language course in this profile.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Profile AG masters</fullName>
        <actions>
            <name>Course_Profile_AG_masters</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Graduate_Postgraduate_Masters__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Graduate_Postgraduate_Masters__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To mark the checkbox of field  &apos;Agents masters&apos; when the agency is interested in any master course in this profile.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Profile AG secondary</fullName>
        <actions>
            <name>Course_Profile_AG_secondary</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Secondary_and_high_school_programmes__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_secondary_and_high_school_programm__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To mark the checkbox of field  &apos;Agents secondary&apos; when the agency is interested in any secondary or high school programme in this profile.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Profile AG undergraduate</fullName>
        <actions>
            <name>Course_Profile_AG_undergraduate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Undergraduate_Degree_Bachelor__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Undergraduate_Degree_Bachelor__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To mark the checkbox of field  &apos;Agents undergraduate&apos; when the agency is interested in any undergraduate course in this profile.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Profile Educator HE</fullName>
        <actions>
            <name>Course_Profile_Educators_HE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5 OR 6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Educator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Graduate_Postgraduate_Masters__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Graduate_Postgraduate_Masters__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Graduate_Postgraduate_Doctorate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Graduate_Postgraduate_Doctorate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Undergraduate_Degree_Bachelor__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Profile__c.Other_Undergraduate_Degree_Bachelor__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To mark the checkbox of field  &apos;Educators HE&apos; when the account is interested in any undergraduate, graduate or postgraduate course in this profile.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Profile has been filled out - Alert to Owner</fullName>
        <actions>
            <name>Course_profile_webform_filled_out_Alert_to_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The Course Profile Online Form has been filled out and the Account Owner is alerted.</description>
        <formula>AND (
NOT (ISBLANK (Online_form_filled_out__c )),
 Account__r.RecordTypeId = &quot;012200000005D7g&quot;
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Take Main Sector from Account</fullName>
        <active>false</active>
        <description>Field &apos;Main Sector&apos; is filled with value from Account field &apos;Educational Sector&apos; when a Course Profile is newly created.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Tickbox Blended Learning Clean</fullName>
        <actions>
            <name>Tickbox_Blended_Learning_Clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Tcikbox is cleaned when not a single course of this section has been chosen.</description>
        <formula>AND (
ISBLANK ( Career_Voc_Cert_Diploma_Blended__c ),
ISBLANK ( Other_Career_Voc_Cert_Dipl_Blended__c ),
ISBLANK ( Undergraduate_Deg_Bachelor_Blended__c ),
ISBLANK ( Other_Undergrad_Deg_Bach_Blended__c ),
ISBLANK ( Grad_Postgrad_Cert_Diploma_Blended__c ),
ISBLANK ( Other_Grad_PostgradCert_Dipl_Blended__c ),
ISBLANK ( Graduate_Postgrad_Masters_Blended__c ),
ISBLANK ( Other_Grad_Postgrad_Masters_Blended__c ),
ISBLANK ( Graduate_Postgrad_Doctorate_Blended__c ),
ISBLANK ( Other_Grad_Postgrad_Doc_Blended__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tickbox Blended Learning Tick</fullName>
        <actions>
            <name>Tickbox_Blended_Learning_Tick</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Is ticked when any course of this section has been chosen.</description>
        <formula>OR (
NOT (ISBLANK ( Career_Voc_Cert_Diploma_Blended__c )),
NOT (ISBLANK ( Other_Career_Voc_Cert_Dipl_Blended__c )),
NOT (ISBLANK ( Undergraduate_Deg_Bachelor_Blended__c )),
NOT (ISBLANK ( Other_Undergrad_Deg_Bach_Blended__c )),
NOT (ISBLANK ( Grad_Postgrad_Cert_Diploma_Blended__c )),
NOT (ISBLANK ( Other_Grad_PostgradCert_Dipl_Blended__c )),
NOT (ISBLANK ( Graduate_Postgrad_Masters_Blended__c )),
NOT (ISBLANK ( Other_Grad_Postgrad_Masters_Blended__c )),
NOT (ISBLANK ( Graduate_Postgrad_Doctorate_Blended__c )),
NOT (ISBLANK ( Other_Grad_Postgrad_Doc_Blended__c ))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tickbox Online Learning Clean</fullName>
        <actions>
            <name>Tickbox_Online_Learning_Clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Tcikbox is cleaned when not a single course of this section has been chosen.</description>
        <formula>AND (
ISBLANK ( Career_Voc_Cert_Diploma_Online__c ),
ISBLANK ( Other_Career_Voc_Cert_Dipl_Online__c ),
ISBLANK ( Undergraduate_Deg_Bachelor_Online__c ),
ISBLANK ( Other_Undergrad_Deg_Bach_Online__c ),
ISBLANK ( Grad_Postgrad_Cert_Diploma_Online__c ),
ISBLANK ( Other_Grad_PostgradCert_Dipl_Online__c ),
ISBLANK ( Graduate_Postgrad_Masters_Online__c ),
ISBLANK ( Other_Grad_Postgrad_Masters_Online__c ),
ISBLANK ( Graduate_Postgrad_Doctorate_Online__c ),
ISBLANK ( Other_Grad_Postgrad_Doc_Online__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tickbox Online Learning Tick</fullName>
        <actions>
            <name>Tickbox_Online_Learning_Tick</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Is ticked when any course of this section has been chosen.</description>
        <formula>OR (
NOT (ISBLANK ( Career_Voc_Cert_Diploma_Online__c )),
NOT (ISBLANK ( Other_Career_Voc_Cert_Dipl_Online__c )),
NOT (ISBLANK ( Undergraduate_Deg_Bachelor_Online__c )),
NOT (ISBLANK ( Other_Undergrad_Deg_Bach_Online__c )),
NOT (ISBLANK ( Grad_Postgrad_Cert_Diploma_Online__c )),
NOT (ISBLANK ( Other_Grad_PostgradCert_Dipl_Online__c )),
NOT (ISBLANK ( Graduate_Postgrad_Masters_Online__c )),
NOT (ISBLANK ( Other_Grad_Postgrad_Masters_Online__c )),
NOT (ISBLANK ( Graduate_Postgrad_Doctorate_Online__c )),
NOT (ISBLANK ( Other_Grad_Postgrad_Doc_Online__c ))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tickbox Traditional Learning Clean</fullName>
        <actions>
            <name>Tickbox_Traditional_Learning_Clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>That tickbox is cleaned when not a single course of this section has been chosen.</description>
        <formula>AND (
ISBLANK ( Career_Vocational_Certificate_Diploma__c ),
ISBLANK ( Other_Career_VocationalCertificate_Dipl__c ),
ISBLANK ( Undergraduate_Degree_Bachelor__c ),
ISBLANK ( Other_Undergraduate_Degree_Bachelor__c ),
ISBLANK ( Graduate_PostgraduateCertificate_Diploma__c ),
ISBLANK ( Other_Graduate_PostgradCertificate_Dipl__c ),
ISBLANK ( Graduate_Postgraduate_Masters__c ),
ISBLANK ( Other_Graduate_Postgraduate_Masters__c ),
ISBLANK ( Graduate_Postgraduate_Doctorate__c ),
ISBLANK ( Other_Graduate_Postgraduate_Doctorate__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tickbox Traditional Learning Tick</fullName>
        <actions>
            <name>Tickbox_Traditional_Learning_Tick</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Is ticked when any course of this section has been chosen.</description>
        <formula>OR (
NOT (ISBLANK ( Career_Vocational_Certificate_Diploma__c )),
NOT (ISBLANK ( Other_Career_VocationalCertificate_Dipl__c )),
NOT (ISBLANK ( Undergraduate_Degree_Bachelor__c )),
NOT (ISBLANK ( Other_Undergraduate_Degree_Bachelor__c )),
NOT (ISBLANK ( Graduate_PostgraduateCertificate_Diploma__c )),
NOT (ISBLANK ( Other_Graduate_PostgradCertificate_Dipl__c )),
NOT (ISBLANK ( Graduate_Postgraduate_Masters__c )),
NOT (ISBLANK ( Other_Graduate_Postgraduate_Masters__c )),
NOT (ISBLANK ( Graduate_Postgraduate_Doctorate__c )),
NOT (ISBLANK ( Other_Graduate_Postgraduate_Doctorate__c ))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
