<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Hotel_Room_has_irregular_number_of_Accommodations</fullName>
        <description>Hotel Room has irregular number of Accommodations</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Manager_Agents_Email_HR__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Manager_Providers_Email_HR__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/A_Hotel_Room_has_an_irregular_number_of_Accommodations</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Room_Type</fullName>
        <field>Room_Type__c</field>
        <name>Clear Room Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_HCT_with_Hotel_remarks</fullName>
        <description>Field Hotel Communication Text&apos; is filled with value of field &apos;Hotel remarks&apos;</description>
        <field>Hotel_Communication_Text__c</field>
        <formula>Hotel_Remarks__c</formula>
        <name>Fill HCT with Hotel remarks</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Hotel_Communication_Cache</fullName>
        <field>Hotel_Communication_Cache__c</field>
        <formula>IF (
  NOT (Any_relevant_change__c ||  Room_Type_changed__c || Early_Check_in_changed__c ) ,
  &quot;&quot;,
  &quot;WAS:&quot; &amp; BR() &amp;
  IF ( (NOT (ISBLANK ( WAS_Values_part_1__c))),  WAS_Values_part_1__c, &quot;&quot;) &amp;
  IF ( (NOT (ISBLANK ( WAS_Values_part_2__c))),  WAS_Values_part_2__c, &quot;&quot;) &amp;
  BR() &amp; &quot;NOW:&quot; &amp; BR() &amp;
  IF ( (NOT (ISBLANK ( NOW_Values_part_1__c))),  NOW_Values_part_1__c, &quot;&quot;) &amp;
  IF ( (NOT (ISBLANK ( NOW_Values_part_2__c))),  NOW_Values_part_2__c, &quot;&quot;) &amp;
  IF (NOT (ISBLANK ( Hotel_Remarks__c )), Hotel_Remarks__c, &quot;&quot;)
)</formula>
        <name>Fill Hotel Communication Cache</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Hotel_Communication_Text</fullName>
        <field>Hotel_Communication_Text__c</field>
        <formula>IF (
  NOT (ISBLANK (Hotel_Communication_Override__c)),
  Hotel_Communication_Override__c,
  IF (
    NOT (Any_relevant_change__c ||  Room_Type_changed__c || Early_Check_in_changed__c) ,
    &quot;&quot;,
    &quot;WAS:&quot; &amp; BR() &amp;
    IF ( (NOT (ISBLANK ( WAS_Values_part_1__c))),  WAS_Values_part_1__c , &quot;&quot;) &amp;
    IF ( (NOT (ISBLANK ( WAS_Values_part_2__c))),  WAS_Values_part_2__c , &quot;&quot;) &amp;
    BR() &amp; &quot;NOW:&quot; &amp; BR() &amp;
    IF ( (NOT (ISBLANK ( NOW_Values_part_1__c))),  NOW_Values_part_1__c, &quot;&quot;) &amp;
    IF ( (NOT (ISBLANK ( NOW_Values_part_2__c))),  NOW_Values_part_2__c, &quot;&quot;) &amp;
    IF (NOT (ISBLANK ( Hotel_Remarks__c )), Hotel_Remarks__c, &quot;&quot;)
  )
)</formula>
        <name>Fill Hotel Communication Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Hotel Comunication text Fill</fullName>
        <active>false</active>
        <description>Fills field Hotel Communication Text with content in &apos;Hotel Communication Formula&apos; only if field &apos;Hotel Communication Override&apos; is empty.</description>
        <formula>Hotel_List_sent__c &amp;&amp; 
(Any_relevant_change__c  ||
ISCHANGED ( Any_relevant_change__c ) ||
ISCHANGED ( Hotel_Communication_Override__c ) ||
ISCHANGED ( Booking_Status_changed__c ) ||
ISCHANGED ( Early_Check_in_changed__c ) ||
ISCHANGED ( Desired_Check_in_Time__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Hotel Comunication text Fill with Hotel Remarks</fullName>
        <actions>
            <name>Fill_HCT_with_Hotel_remarks</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Fills field Hotel Communication Text with content in &apos;Hotel Remarks&apos; only if Hotel List sent is not set.</description>
        <formula>NOT (Hotel_List_sent__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Irregular number of Accommodations</fullName>
        <actions>
            <name>Clear_Room_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Hotel_Room__c.Booking_Status__c</field>
            <operation>equals</operation>
            <value>booked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Hotel_Room__c.Number_of_Accommodations__c</field>
            <operation>greaterThan</operation>
            <value>3</value>
        </criteriaItems>
        <description>A Hotel Room has more than 3 appropriate accommodations. An Alert is sent and Room Type is cleared.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Hotel_Room_has_irregular_number_of_Accommodations</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Irregular number of Accommodations 2</fullName>
        <actions>
            <name>Clear_Room_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Hotel_Room__c.Booking_Status__c</field>
            <operation>equals</operation>
            <value>booked</value>
        </criteriaItems>
        <criteriaItems>
            <field>Hotel_Room__c.Number_of_Accommodations__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <description>A Hotel Room has less than 1 ppropriate accommodation. Room Type is cleared.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
