<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_new_User_is_created_Make_sure_their_FF_User_companies_are_created</fullName>
        <description>A new User is created. Make sure their FF User companies are created</description>
        <protected>false</protected>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/User_has_been_created</template>
    </alerts>
    <alerts>
        <fullName>User_has_been_deactivated_alert_Database_managers</fullName>
        <description>User has been deactivated - alert Database managers</description>
        <protected>false</protected>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/User_has_been_inactivated</template>
    </alerts>
    <rules>
        <fullName>User has been deactivated</fullName>
        <actions>
            <name>User_has_been_deactivated_alert_Database_managers</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>User has been deactivated and Data Managers are alerted to take care of user&apos;s Account Ownership and ARM entries.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
