<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Application_Alert_to_Account_owner</fullName>
        <description>New Application - Alert to Account owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/New_Application</template>
    </alerts>
    <rules>
        <fullName>New Application</fullName>
        <actions>
            <name>New_Application_Alert_to_Account_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
