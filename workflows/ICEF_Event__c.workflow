<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alerting_Sales_persons_10_days_prior_to_event_to_check_payment_status_of_their_c</fullName>
        <ccEmails>jlove@icef.com, rlaub@icef.com, mhebeler@icef.com</ccEmails>
        <description>Alerting Sales persons 10 days prior to event to check payment status of their clients</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesAsiaANZA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesEMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerAsiaANZA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerEMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerUSA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesUSA</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Rep_communication/Non_paid_clients_alert_10_days_prior_to_ESP_opens</template>
    </alerts>
    <alerts>
        <fullName>Alerting_Sales_persons_4_weeks_prior_to_event_to_check_payment_status_of_their_c</fullName>
        <ccEmails>jlove@icef.com, rlaub@icef.com, mhebeler@icef.com</ccEmails>
        <description>Alerting Sales persons 4 weeks prior to event to check payment status of their clients</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesAsiaANZA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesEMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerAsiaANZA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerEMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerUSA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesUSA</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Rep_communication/Non_paid_clients_alert_4_weeks_prior_to_ESP_opens</template>
    </alerts>
    <alerts>
        <fullName>Bags_deadline_alert</fullName>
        <ccEmails>salesteam@icef.com</ccEmails>
        <ccEmails>emt@icef.com</ccEmails>
        <description>Bags deadline alert</description>
        <protected>false</protected>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Event_Management_general/Bags_Deadline_two_weeks_left</template>
    </alerts>
    <alerts>
        <fullName>Branded_cups_deadline_alert</fullName>
        <ccEmails>salesteam@icef.com</ccEmails>
        <ccEmails>emt@icef.com</ccEmails>
        <description>Branded cups deadline alert</description>
        <protected>false</protected>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Event_Management_general/Branded_Cups_Deadline_two_weeks_left</template>
    </alerts>
    <alerts>
        <fullName>Catalogue_cut_off_date_alert</fullName>
        <ccEmails>salesteam@icef.com</ccEmails>
        <ccEmails>arm@icef.com</ccEmails>
        <ccEmails>emt@icef.com</ccEmails>
        <description>Catalogue cut off date alert</description>
        <protected>false</protected>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Event_Management_general/Catalogue_Deadline_two_weeks_left</template>
    </alerts>
    <alerts>
        <fullName>ESP_opens_email_need_to_check_their_first_timers</fullName>
        <ccEmails>mhebeler@icef.com, rlaub@icef.com, jlove@icef.com</ccEmails>
        <description>ESP opens - email need to check their first timers</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesAsiaANZA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesEMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerAsiaANZA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerEMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerUSA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesUSA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Sales_Canada</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Sales_Manager_Canada</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Rep_communication/ESP_opens_check_First_Timers</template>
    </alerts>
    <alerts>
        <fullName>Event_Table_target_changed</fullName>
        <description>Event Table target changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Event_Manager_Agents__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Table_Target_change_notification</template>
    </alerts>
    <alerts>
        <fullName>Event_starts_in_3_months_TBA_check_alert</fullName>
        <ccEmails>mhebeler@icef.com</ccEmails>
        <description>Event starts in 3 months - TBA check alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesAsiaANZA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesEMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerAsiaANZA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerEMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerUSA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesUSA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Sales_Canada</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Sales_Manager_Canada</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Event_starts_in_3_months_Check_your_TBAs</template>
    </alerts>
    <alerts>
        <fullName>Event_starts_in_3_months_booth_allocation_alert</fullName>
        <ccEmails>tm@icef.com</ccEmails>
        <description>Event starts in 3 months - booth allocation alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>VPSalesandMarketing</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Event_starts_in_3_months_booth_allocation_alert</template>
    </alerts>
    <alerts>
        <fullName>Expected_Bookings_have_been_changed_and_Isabel_is_informed_about_that</fullName>
        <description>Expected Bookings have been changed and Isabel is informed about that</description>
        <protected>false</protected>
        <recipients>
            <recipient>ivogt@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Expected_booking_field_in_event_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>Key_card_deadline_alert</fullName>
        <ccEmails>salesteam@icef.com</ccEmails>
        <ccEmails>emt@icef.com</ccEmails>
        <description>Key card deadline alert</description>
        <protected>false</protected>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Event_Management_general/Key_Card_Deadline_two_weeks_left</template>
    </alerts>
    <alerts>
        <fullName>Max_Educator_Tables_changed</fullName>
        <description>Max Educator Tables changed</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Manager_Agents__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Event_Manager_Educators__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMT_alerts/ICEF_Event_Educators_Table_Max_changed</template>
    </alerts>
    <alerts>
        <fullName>Max_Exhib_W_T_numbers_changed</fullName>
        <description>Max Exhib/W&amp;T numbers changed</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Manager_Agents__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Event_Manager_Educators__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EMT_alerts/ICEF_Event_Exhibitors_W_T_Max_changed</template>
    </alerts>
    <alerts>
        <fullName>NECK_CORD_deadline_alert</fullName>
        <ccEmails>salesteam@icef.com</ccEmails>
        <ccEmails>emt@icef.com</ccEmails>
        <description>Neck cord deadline alert</description>
        <protected>false</protected>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Event_Management_general/Neck_cord_Deadline_two_weeks_left</template>
    </alerts>
    <alerts>
        <fullName>New_event_has_been_created</fullName>
        <description>New event has been created</description>
        <protected>false</protected>
        <recipients>
            <recipient>ivogt@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/New_ICEF_event_created</template>
    </alerts>
    <alerts>
        <fullName>Provider_Participant_List_changed_Alert</fullName>
        <description>&apos;Provider Participant List&apos; changed Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ARM_Alerts/Provider_Participant_List_has_been_changed</template>
    </alerts>
    <alerts>
        <fullName>USB_Cards_deadline_alert</fullName>
        <ccEmails>salesteam@icef.com</ccEmails>
        <ccEmails>emt@icef.com</ccEmails>
        <description>USB Cards deadline alert</description>
        <protected>false</protected>
        <senderAddress>ffestersen@icef.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Event_Management_general/USB_Card_Deadline_two_weeks_left</template>
    </alerts>
    <alerts>
        <fullName>eSP_opens_in_one_week_TBA_check_alert</fullName>
        <ccEmails>mhebeler@icef.com</ccEmails>
        <description>eSP opens in one week - TBA check alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>SalesAsiaANZA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesEMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerAsiaANZA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerEMEA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesManagerUSA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SalesUSA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Sales_Canada</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Sales_Manager_Canada</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/eSP_opens_in_one_week_Check_your_TBAs</template>
    </alerts>
    <fieldUpdates>
        <fullName>Provider_Participant_lists_clean</fullName>
        <field>Provider_Participant_list__c</field>
        <name>Provider Participant lists clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>USB_Card_Cut_off_Date_clean</fullName>
        <description>clear field</description>
        <field>USB_Card_Cut_off_Date__c</field>
        <name>USB Card Cut-off Date clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Expected_Educator_Table_Target</fullName>
        <field>Expected_Educator_Table_Bookings__c</field>
        <formula>Educator_table_target__c</formula>
        <name>Update Expected Educator Table Target</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Expected_Exhibitor_Booth_Bookings</fullName>
        <field>Expected_Exhibitor_Booth_Bookings__c</field>
        <formula>Exhibitor_booths_target__c</formula>
        <name>Update Expected Exhibitor Booth Bookings</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Expected_Exhibitor_Table_Bookings</fullName>
        <field>Expected_Exhibitor_Table_Bookings__c</field>
        <formula>Exhibitor_tables_target__c</formula>
        <name>Update Expected Exhibitor Table Bookings</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Expected_W_T_Bookings</fullName>
        <field>Expected_W_T_Table_Bookings__c</field>
        <formula>Work_Travel_table_target__c</formula>
        <name>Update Expected W&amp;T Bookings</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_1st_Day_with_Business_Meetings_date</fullName>
        <field>First_Day_with_Business_Meetings__c</field>
        <formula>IF(
  Start_date__c = End_date__c,
  Start_date__c,
  Start_date__c + 1
 
)</formula>
        <name>set 1st Day with Business Meetings date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_2nd_Day_with_Business_Meetings_date</fullName>
        <description>sets default day for 2nd business day if there is any. Else leave empty.</description>
        <field>Second_Day_with_Business_Meetings__c</field>
        <formula>IF(
 End_date__c - Start_date__c &gt; 1,
 End_date__c,
 DATEVALUE(&apos;&apos;) 
)</formula>
        <name>set 2nd Day with Business Meetings date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Registration_Day_date</fullName>
        <description>sets registration day to start date by default. Leave empty for events of 1 day length</description>
        <field>Registration_Day__c</field>
        <formula>IF(
  Start_date__c = End_date__c,
  DATEVALUE(&apos;&apos;) ,
  Start_date__c
)</formula>
        <name>set Registration Day date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert to be sent before esp opens</fullName>
        <active>true</active>
        <criteriaItems>
            <field>ICEF_Event__c.eSchedulePRO_live__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule will trigger an email alert 10 days/4 weeks prior the opening of ESP to remind Sales Reps of contacting their clients who haven&apos;t paid yet.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Alerting_Sales_persons_10_days_prior_to_event_to_check_payment_status_of_their_c</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.eSchedulePRO_live__c</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Alerting_Sales_persons_4_weeks_prior_to_event_to_check_payment_status_of_their_c</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.eSchedulePRO_live__c</offsetFromField>
            <timeLength>-28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Bags Deadline reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>ICEF_Event__c.Bags_deadline__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Two weeks prior to the production deadline for the bags a reminder is sent to the salesteam.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Bags_deadline_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.Bags_deadline__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Branded cups Deadline reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>ICEF_Event__c.Branded_Cups_cut_off_date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Two weeks prior to the production deadline for Branded cups a reminder is sent to the salesteam.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Branded_cups_deadline_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.Branded_Cups_cut_off_date__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Catalogue Deadline reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>ICEF_Event__c.Catalogue_cut_off_date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Two weeks prior to thecatalogue cut off date a reminder is sent to the salesteam and to ARMs.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Catalogue_cut_off_date_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.Catalogue_cut_off_date__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ESP opens</fullName>
        <actions>
            <name>ESP_opens_email_need_to_check_their_first_timers</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>ESP opens. An alert to the sales team regarding their first timers is sent.</description>
        <formula>AND (
 NOT ( ISBLANK (eSchedulePRO_live__c )),
 eSchedulePRO_live__c  &gt; TODAY()
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event starts in 3 months - TBA check alert</fullName>
        <active>true</active>
        <description>Event starts in 3 months. Sales Reps are asked to check their TBAs</description>
        <formula>AND (
 NOT ( ISBLANK (Start_date__c )),
 Start_date__c  &gt; TODAY()
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Event_starts_in_3_months_TBA_check_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.Start_date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Event starts in 3 months - booth allocation alert</fullName>
        <active>false</active>
        <description>Event starts in 3 months. TMs and MM are asked to allocate booths.</description>
        <formula>AND (
 NOT ( ISBLANK (Start_date__c )),
 Start_date__c  &gt; TODAY()
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Event_starts_in_3_months_booth_allocation_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.Start_date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Expected Bookings have been changed</fullName>
        <actions>
            <name>Expected_Bookings_have_been_changed_and_Isabel_is_informed_about_that</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>One of the fields &apos;Expected xx Bookings&apos; has been updated and Isabel needs to be informed about that.</description>
        <formula>OR (
 ISCHANGED( Expected_Educator_Table_Bookings__c ),
 ISCHANGED( Expected_Exhibitor_Booth_Bookings__c ),
 ISCHANGED( Expected_Exhibitor_Table_Bookings__c ),
 ISCHANGED( Expected_W_T_Table_Bookings__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Key Card Deadline reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>ICEF_Event__c.Key_Card_Deadline__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Two weeks prior to the production deadlinefor Key cards a reminder is sent to the salesteam.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Key_card_deadline_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.Key_Card_Deadline__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Max Educator Number has been changed</fullName>
        <actions>
            <name>Max_Educator_Tables_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>&apos;Educator tables Max.&apos; in ICEF Event has been changed and Event Managers are alerted.</description>
        <formula>AND (
ISCHANGED ( Tables_max__c ),
NOT ( ISBLANK (PRIORVALUE ( Tables_max__c )))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Max Exhib%2FW%26T Number has been changed</fullName>
        <actions>
            <name>Max_Exhib_W_T_numbers_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Max numbers of Exhibitor Booths, Tables or W&amp;T Tables in ICEF Event has been changed and Event Managers are alerted.</description>
        <formula>OR (
  AND (
    ISCHANGED ( Exhibitor_Booths_max__c ),
    NOT ( ISBLANK (PRIORVALUE ( Exhibitor_Booths_max__c )))
  ),
  AND (
    ISCHANGED ( Exhibitor_Tables_max__c ),
    NOT ( ISBLANK (PRIORVALUE ( Exhibitor_Tables_max__c )))
  ),
  AND (
    ISCHANGED ( Work_and_Travel_Tables_max__c ),
    NOT ( ISBLANK (PRIORVALUE ( Work_and_Travel_Tables_max__c )))
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Neck cord Deadline reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>ICEF_Event__c.Neck_cord_deadline__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Two weeks prior to the production deadline for neck cords a reminder is sent to the salesteam.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>NECK_CORD_deadline_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.Neck_cord_deadline__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>New ICEF event created</fullName>
        <actions>
            <name>Provider_Participant_lists_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>USB_Card_Cut_off_Date_clean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>some fields are cleaned upon creation</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New event created</fullName>
        <actions>
            <name>New_event_has_been_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ICEF_Event__c.CreatedById</field>
            <operation>notContain</operation>
            <value>Isabel Vogt</value>
        </criteriaItems>
        <description>email alert if created by someone else than Isabel</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Table Target change</fullName>
        <actions>
            <name>Event_Table_target_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When the target of tables for Educators is changed, an emai, is sent to the responsible agent manager to notify him about the change.</description>
        <formula>AND (
ISCHANGED ( Educator_table_target__c ), 
PRIORVALUE (Educator_table_target__c) &gt;1
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>USB Cards Deadline reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>ICEF_Event__c.USB_Card_Cut_off_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Two weeks prior to the production deadline for the USB Cards a reminder is sent to the salesteam.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>USB_Cards_deadline_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.USB_Card_Cut_off_Date__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Expected Educator Table Targets</fullName>
        <actions>
            <name>Update_Expected_Educator_Table_Target</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the expected targets when the targets are first set or changed</description>
        <formula>ISCHANGED(Educator_table_target__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Expected Exhibitor Booth Targets</fullName>
        <actions>
            <name>Update_Expected_Exhibitor_Booth_Bookings</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the expected targets when the targets are first set or changed</description>
        <formula>ISCHANGED(Exhibitor_booths_target__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Expected Exhibitor Table Targets</fullName>
        <actions>
            <name>Update_Expected_Exhibitor_Table_Bookings</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the expected targets when the targets are first set or changed</description>
        <formula>ISCHANGED( Exhibitor_tables_target__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Expected W%26T Table Targets</fullName>
        <actions>
            <name>Update_Expected_W_T_Bookings</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the expected targets when the targets are first set or changed</description>
        <formula>ISCHANGED(Work_Travel_table_target__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update dates of Registration Day and Business Days</fullName>
        <actions>
            <name>set_1st_Day_with_Business_Meetings_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_2nd_Day_with_Business_Meetings_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_Registration_Day_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>when the event is created or start / end dates are changed, three date fields are updated</description>
        <formula>OR(
 ISNEW() ,
 ISCHANGED( Start_date__c ) ,
 ISCHANGED( End_date__c ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>eSP opens in one week - TBA check alert</fullName>
        <active>true</active>
        <description>eSP opens in one week. Sales Reps are asked to check their TBAs</description>
        <formula>AND (
 NOT ( ISBLANK (eSchedulePRO_live__c )),
 eSchedulePRO_live__c  &gt; TODAY()
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>eSP_opens_in_one_week_TBA_check_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ICEF_Event__c.eSchedulePRO_live__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Workshop_Statistics_to_be_created</fullName>
        <assignedTo>mhebeler@icef.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Two days prior to the workshop the statistics must be ready for use (tomorrow).</description>
        <dueDateOffset>-3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>ICEF_Event__c.Start_date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Workshop Statistics to be created</subject>
    </tasks>
</Workflow>
