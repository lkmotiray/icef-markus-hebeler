<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Untick_Review_after_90_days</fullName>
        <description>Untick Review after 90 days</description>
        <field>Review_of_last_3_months__c</field>
        <literalValue>0</literalValue>
        <name>Untick Review after 90 days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>untick_new_review</fullName>
        <description>New Review is unticked 7 days after Review Date</description>
        <field>New_Review__c</field>
        <literalValue>0</literalValue>
        <name>untick new review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>untick %27Review 3 months%27 after 90 days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>CF_Review__c.Review_of_last_3_months__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>untick &apos;Review of last 3 months&apos; after 90 days for recent review count</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Untick_Review_after_90_days</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>CF_Review__c.created__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>untick New Review after 7 days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>CF_Review__c.New_Review__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>untick New Review after 7days for recent review count</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>untick_new_review</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>CF_Review__c.created__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
