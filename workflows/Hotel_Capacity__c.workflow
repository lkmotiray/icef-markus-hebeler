<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Hotel_Contingent_is_overbooked</fullName>
        <description>Hotel Contingent is overbooked</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Manager_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Manager_Provider_Emails__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ivogt@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Contingent_has_been_overbooked</template>
    </alerts>
    <alerts>
        <fullName>Storno_Option_1_is_running_out</fullName>
        <description>Storno Option 1 is running out</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Manager_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Manager_Provider_Emails__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ivogt@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Storno_option_1_running_out</template>
    </alerts>
    <alerts>
        <fullName>Storno_Option_2_is_running_out</fullName>
        <description>Storno Option 2 is running out</description>
        <protected>false</protected>
        <recipients>
            <field>Event_Manager_Agents_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Event_Manager_Provider_Emails__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ivogt@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Event_Management_general/Storno_option_2_running_out</template>
    </alerts>
    <rules>
        <fullName>Storno option 1 running out</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Hotel_Capacity__c.Storno_Option_1_Expiry_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Storno option 1 will expire in 7 days and EMT is alerted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Storno_Option_1_is_running_out</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Hotel_Capacity__c.Storno_Option_1_Expiry_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Storno option 2 running out</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Hotel_Capacity__c.Storno_Option_2_Expiry_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Storno option 2 will expire in 7 days and EMT is alerted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Storno_Option_2_is_running_out</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Hotel_Capacity__c.Storno_Option_2_Expiry_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
