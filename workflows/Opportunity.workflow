<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Autocreated_Opp_Alert_Owner</fullName>
        <description>Autocreated Opp - Alert Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Autocreated_Opp_from_Lead</template>
    </alerts>
    <alerts>
        <fullName>CP_renewal_12_months_alert</fullName>
        <description>CP renewal 12 months alert</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/CoursePricer_12_months_renewal_alert</template>
    </alerts>
    <alerts>
        <fullName>CP_renewal_1_month_alert</fullName>
        <description>CP renewal 1 month alert</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/CoursePricer_1_month_renewal_alert</template>
    </alerts>
    <alerts>
        <fullName>Check_if_credit_is_really_added</fullName>
        <description>Check if credit is really added</description>
        <protected>false</protected>
        <recipients>
            <recipient>hschoenleber@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/opp_closed_for_CF_credit</template>
    </alerts>
    <alerts>
        <fullName>Close_won_Opp_with_CF_products_has_been_changed_Alert_to_Ross</fullName>
        <description>Close won Opp with CF products has been changed --&gt; Alert to Ross</description>
        <protected>false</protected>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Opportunity_closed_won_with_CF_products_has_been_edited</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_EMT_when_a_closed_won_opp_has_been_edited</fullName>
        <description>Email alert to EMT when a closed won opp has been edited</description>
        <protected>false</protected>
        <recipients>
            <recipient>cwojcik@icef.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mhebeler@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Opportunity_closed_won_has_been_edited</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Rod_notifying_of_new_Opp_Cosed_Won_in_Asia_Pac</fullName>
        <description>Email to Rod notifying of new Opp Cosed Won in Asia/Pac</description>
        <protected>false</protected>
        <recipients>
            <recipient>rhearps@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/New_Opportunity_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>First_Timer_s_Opp_Created_without_an_ICEF_Coach</fullName>
        <description>First Timer&apos;s Opp Created without an ICEF Coach</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>VPSalesandMarketing</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/First_Timer_with_no_ICEF_Coach</template>
    </alerts>
    <alerts>
        <fullName>Opp_is_closed_without_CF_product</fullName>
        <description>Opp is closed without CF product</description>
        <protected>false</protected>
        <recipients>
            <recipient>rholmes@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coursefinder_Communication/Opp_has_been_closed_w_o_CF_Product</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_closed_won_OZ_NZ</fullName>
        <description>When an opportunity is a closed won in OZ/NZ, Rod is informed.</description>
        <protected>false</protected>
        <recipients>
            <recipient>rhearps@icef.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Rep_communication/Opportunity_closed_won_Rod</template>
    </alerts>
    <fieldUpdates>
        <fullName>Updates_Close_Date</fullName>
        <description>Updates Close Date to day Closed Won or Closed Lost</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Updates Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Closed Won Opp has been edited</fullName>
        <actions>
            <name>Email_alert_to_EMT_when_a_closed_won_opp_has_been_edited</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>An Opportunity with Status &apos;Closed won&apos; has been edited. EMT is alerted to take the necessary steps.</description>
        <formula>AND (
  ISPICKVAL (StageName, &quot;Closed Won&quot;),
  ISPICKVAL(PRIORVALUE(StageName), &quot;Closed Won&quot;),
  $User.Id &lt;&gt; &quot;005200000015MIZ&quot;,
  PRIORVALUE (Number_of_Sales_Invoices__c)  = Number_of_Sales_Invoices__c
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>First Timer alert</fullName>
        <actions>
            <name>First_Timer_s_Opp_Created_without_an_ICEF_Coach</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Number_of_Coach_Products__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Number_of_ICEF_events_visited__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Work and Travel,Supplier,Agent,Hotel,Association,Other,Staff</value>
        </criteriaItems>
        <description>Alerts TMs when there is a First Timer booked that does not have a coach product booked</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Cathy and Rod when an Opportunity is Closed Won - Only Aus%2FNZ</fullName>
        <actions>
            <name>Opportunity_closed_won_OZ_NZ</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>An_Oz_or_NZ_Opportunity_has_closed_ready_for_billing</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CurrencyIsoCode</field>
            <operation>equals</operation>
            <value>AUD</value>
        </criteriaItems>
        <description>Notifies Cathy of all Closed Won Opportunities so that she may bill them. Only Australia/New Zealand Opportunities. Excludes Agents.
Works combined with workflow: &quot;Notify Rod when an Opportunity is Closed Won in Asia/Pac&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Rod when an Opportunity is Closed Won in Asia%2FPac</fullName>
        <actions>
            <name>Opportunity_closed_won_OZ_NZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Territory__c</field>
            <operation>equals</operation>
            <value>Asia/Pac</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Number_of_products__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CurrencyIsoCode</field>
            <operation>notEqual</operation>
            <value>AUD</value>
        </criteriaItems>
        <description>Notifies Rod of all Closed Won Opportunities in Asia/Pac. Excludes Agents.
Works combined with workflow: &quot;Workflow Rule
Notify Cathy and Rod when an Opportunity is Closed Won - Only Aus/NZ&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp closed w%2Fo CF product</fullName>
        <actions>
            <name>Opp_is_closed_without_CF_product</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Number_of_1st_Educators__c</field>
            <operation>greaterOrEqual</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Coursefinder_Products__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Educational_Sector__c</field>
            <operation>equals</operation>
            <value>1 LANGUAGE COURSES</value>
        </criteriaItems>
        <description>An Opp with at least two 1st educator products has been closed, but there is no CF product attached.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity is closed won and adding CF credit</fullName>
        <actions>
            <name>Check_if_credit_is_really_added</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Added_Credit__c</field>
            <operation>greaterThan</operation>
            <value>EUR 1</value>
        </criteriaItems>
        <description>when an opportunity is closed won and should add CF credit automatically, alert Henriette to compare with sync added credit notifications</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Finance Contact and Email in Account</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Updates the fields Finance Contact and Invoice Email in Account when an opportunity is closed won.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updates Close Date to day Closed Won</fullName>
        <actions>
            <name>Updates_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>,Closed Lost,Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>An_Oz_or_NZ_Opportunity_has_closed_ready_for_billing</fullName>
        <assignedTo>cathy@edmedia.info</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A Sales Owner has set an Opportunity to Closed Won and the Opportunity is now ready for billing</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>An Oz or NZ Opportunity has closed ready for billing</subject>
    </tasks>
</Workflow>
