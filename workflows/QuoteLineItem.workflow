<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Discount_Percentage_calculation</fullName>
        <description>The discount percentage is calculated automatically.</description>
        <field>Discount</field>
        <formula>IF (
  Offered_Price__c &gt; 0,
  1- (Offered_Price__c / UnitPrice),
  0
)</formula>
        <name>Discount Percentage calculation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Discount Percentage calculation</fullName>
        <actions>
            <name>Discount_Percentage_calculation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>An &apos;offered Price&apos; has been entered by the Sales Rep and the discount percentage is calculated.</description>
        <formula>ISCHANGED( Offered_Price__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
