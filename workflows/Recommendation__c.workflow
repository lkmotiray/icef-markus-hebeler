<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Recommendation_is_created_and_Account_Owner_is_alerted</fullName>
        <description>Recommendation is created and Account Owner is alerted</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All/Recommendation_created_alert_to_owner</template>
    </alerts>
    <rules>
        <fullName>Recommendation has been created</fullName>
        <actions>
            <name>Recommendation_is_created_and_Account_Owner_is_alerted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Recommendation has been created and Account Owner of recommended Account is alerted.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
