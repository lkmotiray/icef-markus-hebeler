var createProductApp = angular.module("CreateProductApp",["ngTable"]);

function RemotingFactory($q){
    	DataFactoryObj = {};
    	try{
	    		DataFactoryObj.invokeRemoteAction = function(options){
	    		var defer = $q.defer();
	    		options.callback = function(result,event){
	    			if(event.status){
	    				defer.resolve(result);
	    			}else{
	    				defer.reject(event.message);
	    			}
	    		};
	    		
	    		options.vfConfig = {escape:false};
	    		var arguments = [];
	    		
	    		arguments.push(options.url);
	    		
	    		for(var i=0,len=options.params.length;i<len;i++){
	    			arguments.push(options.params[i]);
	    		}
	    		
	    		arguments.push(options.callback);
	    		arguments.push(options.vfConfig);
	    		Visualforce.remoting.Manager.invokeAction.apply(Visualforce.remoting.Manager,arguments);
	    		return defer.promise;
    		}
    	}catch(e){
    		console.error(e.message);
    	}
    	
    	return DataFactoryObj;
}
createProductApp.factory('RemotingFactory',RemotingFactory);

function numericType(){
    return {
        restrict:'A',
        require:'ngModel',
        scope:{
          showError:'&'  
        },
        link:function(scope,elem,attr,ngModel){
            
            function inputValue(val){
                if(val){
                    var input = val.replace(/[^0-9]/g,'');
                    if(input!==val){
                        ngModel.$setViewValue(input);
                        ngModel.$render();
                    }
                    
                    return input;
                }
                return "";
            }
            ngModel.$parsers.push( inputValue );
        }
    };
}

createProductApp.directive('numericType',numericType);

function CreateProductAppController($scope,RemotingFactory,NgTableParams,$filter,$window,$timeout){
    $scope.tableParams = null;
    $scope.search = { term: '' };
    $scope.hasError = false;
    $scope.setLoading = false;
    $scope.loadingImg = window.loadingImg;
    $scope.noDataFound = false;
    $scope.recordIds = {
        eventId:window.eventId,
        accountId:window.accountId
    };
    toastr.options={
       positionClass:'toast-top-center',
       preventDuplicates:true,
       closeDuration:1000  
    };
    $scope.createProductList = [];
    
    $scope.getProducts = function(){
        $scope.setLoading = true;

        var options = {
			url:'CreateProductsController.getProducts',
			params:[]
		};
			
		RemotingFactory.invokeRemoteAction(options).then(
			function(result){
				$scope.setLoading = false;
				$scope.hasError = false;
				$scope.tableParams = new NgTableParams({
					page: 1,
					count: result.length,
					filter: $scope.search
				}, { 
					dataset: result,
					counts: [],
					total: result.length,
					getData: function(params) {
						
						var orderedData;

						if (params.filter().term) {
							orderedData = params.filter() ? $filter('filter')(result, params.filter().term) : result;
						} else {
							orderedData = params.sorting() ? $filter('orderBy')(result, params.orderBy()) : result;
						}
						//searchText(result);
						params.total(orderedData.length);
						if(!orderedData.length){
							$scope.noDataFound = true;
						}else{
							$scope.noDataFound = false;
						}
						return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
						//$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
						
					}
				});
				$scope.createProductList = result;
				console.log(result);
				
			},
			function(error){
				toastr.error(error);
				$scope.hasError = true;
				$scope.setLoading = false;  
				
				
			}
		);

    }
    
    $scope.updateCreateRecord = function(productRecord){
        productRecord.isModified = true;
    }
    
    $scope.sendSelectedProducts = function(){
        $scope.setLoading = true;
        var modifiedRecList = $filter('filter')($scope.createProductList,{isModified:true},true);
        console.log(modifiedRecList);
        var modifiedProductArray = [];
        var isZero = false;
        angular.forEach(modifiedRecList,function(product,index){
            //console.log(auction);
            var dataObj = {};
            dataObj={};
            dataObj.Id = product.Id;
            dataObj.Name = product.Name;
            dataObj.Family = product.Family;
            
            //dataObj.isModified = auction.isModified;
            if(product.count != '' && parseInt(product.count) > 0){
                dataObj.count = parseInt(product.count);
                modifiedProductArray.push(dataObj);
            }else if(parseInt(product.count) == 0){
                isZero = true;
            }
            
        });
        
        if(isZero){
            $scope.setLoading = false;
            $scope.noDataFound = false;
            toastr.warning('Please enter number greater than zero in input box to create records.');
            return false;
        }
        
        if(modifiedProductArray.length){
            $scope.noDataFound = true;
            var options = {
                url:'CreateProductsController.createPPRecords',
                params:[ JSON.stringify(modifiedProductArray), $scope.recordIds.eventId, $scope.recordIds.accountId ]
    		};
    		
            RemotingFactory.invokeRemoteAction(options).then(
                function(result){
                    console.log(result);
                    
                    if(result){
                        toastr.success('Record created successfully');
                        $scope.hasError = false;
                       // window.location = "/"+$scope.recordIds.eventId;
                        if(window.opener){
                            if (UITheme.getUITheme() === 'Theme4d' || UITheme.getUITheme() === 'Theme4u'){
                                sforce.one.navigateToURL("/"+$scope.recordIds.eventId);
                            } else {
                                window.opener.location.href="/"+$scope.recordIds.eventId;
                            }
        					window.top.close();
                        }else{
                            if (UITheme.getUITheme() === 'Theme4d' || UITheme.getUITheme() === 'Theme4u'){
                                sforce.one.navigateToURL("/"+$scope.recordIds.eventId);
                            } else {
                                window.opener.location.href="/"+$scope.recordIds.eventId;
                            }
                        }
                       
                    }else{
                        toastr.error('Error occured while creating records, Please contact System Admin');
                        $scope.hasError = true;
                    }
                    
                    /*toastr.success('Updated Auction Details Successfully.');
                    $window.location.href='/'+$scope.auctionId;
                    console.log(result);*/
                    $scope.setLoading = false;
                    
                },
                function(error){
                    $scope.setLoading = false;     
                    console.log(error);
                    $scope.hasError = true;
                    toastr.error('Error occured while creating records, Please contact System Admin');
                }
            );
        }else{
            $scope.setLoading = false;
            toastr.warning('Please enter number in input box to create records.');
        }
    }
    
    $scope.resetRecords = function(){
        $scope.setLoading = true;
        var modifiedRecList = $filter('filter')($scope.createProductList,{isModified:true},true);
        angular.forEach(modifiedRecList,function(product,index){
            product.count = '';
        });
        //console.log(window.parent.location);
        if(window.opener){
            if (UITheme.getUITheme() === 'Theme4d' || UITheme.getUITheme() === 'Theme4u'){
                sforce.one.navigateToURL("/"+$scope.recordIds.eventId);
            } else {
                window.opener.location.href="/"+$scope.recordIds.eventId;
            }
            window.top.close();
        }else{
            if (UITheme.getUITheme() === 'Theme4d' || UITheme.getUITheme() === 'Theme4u'){
                sforce.one.navigateToURL("/"+$scope.recordIds.eventId);
            } else {
                window.opener.location.href="/"+$scope.recordIds.eventId;
            }
        }
        //window.location = "/"+$scope.recordIds.eventId;
    }
    
    function GetQueryStringByParameter(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

createProductApp.controller('CreateProductAppController',CreateProductAppController);