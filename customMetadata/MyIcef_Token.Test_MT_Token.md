<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Test MT Token</label>
    <protected>false</protected>
    <values>
        <field>access_token__c</field>
        <value xsi:type="xsd:string">looooooong access token</value>
    </values>
    <values>
        <field>refresh_token__c</field>
        <value xsi:type="xsd:string">loooooooooooooooooooong refresh token</value>
    </values>
</CustomMetadata>
