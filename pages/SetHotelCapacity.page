<!------------------------------------------------------------------
    Purpose      : Holds functionality to assing hotel capacity lookup
                   to Accout participant at mass quantity.
    Page Name    : setHotelCapacity
    Created Date : 01-july-2016  
------------------------------------------------------------------>
<apex:page sidebar="false" showHeader="false" controller="SetHotelCapacityController" lightningStylesheets="true">
<head>
    <title>Select Hotel Capacity</title>
    <apex:includeScript value="{!URLFOR($Resource.setHotelCapacityResource, 'js/jquery.js')}" />
    <apex:stylesheet value="{!URLFOR($Resource.setHotelCapacityResource,'css/cdnDatatableCss.css')}" />
    <apex:includeScript value="{!URLFOR($Resource.setHotelCapacityResource, 'js/cdnDatatable.js')}" />
    <apex:includeScript value="/soap/ajax/37.0/connection.js" />
    <apex:includeScript value="/soap/ajax/24.0/apex.js" />
    
    <!---------- CSS ---------->    
    <style>
        .gridContainer tbody tr:nth-child(5n+1) {
               background: #fafafa;
            }
        .newButton{
            height: 25px;
            width: 67px;
            font-size: 12px;
        }
        tbody tr.odd {
            background-color: rgba(158, 158, 158, 0.16);
        }
        
        .background-block {display:none; position: fixed;width: 100%;height: 100%;top: 0;left: 0;background: rgba(255,255,255,0.44);z-index: 999;}
        .spinner{position: fixed;top: 48%;left: 48%;} 
        .hc_container{
            margin-top:20px;
        }
        body {
            margin-left: 8px;
            margin-right: 8px;
        }
        
        .processIcon {
                
                border: 10px solid #f3f3f3;
                border-radius: 50%;
                border-top: 10px solid blue;
                border-bottom: 10px solid blue;
                width: 50px;
                height: 50px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
                top: 45%;
                position: absolute;
                left: 47%;
                box-sizing: border-box;
            }
            
            .loader{
                    background: rgba(0, 0, 0, 0.1);
                    position: absolute;
                    top: 0;
                    width: 100%;
                    height: 100%;
                    bottom: 0;
                    right: 0;
                    z-index: 3;
                    
            }
            
            @-webkit-keyframes spin {
              0% { -webkit-transform: rotate(0deg); }
              100% { -webkit-transform: rotate(360deg); }
            }
            
            @keyframes spin {
              0% { transform: rotate(0deg); }
              100% { transform: rotate(360deg); }
            }
            
            .loadingWrap { position: absolute; z-index: 999; top: 30%; left: 50%; margin-left: -35px; }
            .loadingWrap img{width:70px;}
            .loadingBg{ background: rgba(0, 0, 0, 0.47); position: fixed; top: 0; width: 100%; bottom: 0; left: 0; z-index: 99;}
            .message { background-color: #ffc; border-style: solid; border-width: 1px; color: #000; padding: 6px 8px 6px 6px; margin: 4px 20px; display: inline-block; }
            .msgIcon { display: inline-block; vertical-align: middle; padding-right: 9px; background-repeat: no-repeat; }
            #msgInfo_custom{ display: block; padding-left: 33px;}
            .message .messageText { margin-left: 0px;}
    </style>
    
    <!---------- JAVASCRIPT METHODS ---------->
    <script type="text/javascript">
    
    function getAccountPartRecords() {
        var accountPartIds = '{!$CurrentPage.parameters.ids}';
        var recordsH = new Array();
        if(accountPartIds) {
            Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.SetHotelCapacityController.getHotelCapacityRecords}',
                '{!$CurrentPage.parameters.ids}', 
                function(result, event){
                    if (event.status) {
                       createHotelCapacityTable(createDataSet(result));
                       displayDatatable();
                    } else if (event.type === 'exception') {
                        console.log('exception');
                        $('#errorMsgs').show();
                        document.getElementById("msgInfo_custom").textContent = 'Exception occurred : ' + event.message;
                    } else {
                        $('#errorMsgs').show();
                        document.getElementById("msgInfo_custom").textContent = 'Unexpected exception occurred : ' + event.message;
                    }
                }, 
                {escape: true}
            );
        } else {
            document.getElementById("errorMsgs").innerHTML = 'Id\'s not found in URL!';
        }
        $('.loader').hide();
    }
    
    function convertDateFormat(dateInput) {
        var processedDate = new Date(dateInput); 
        processedDate = new Date(processedDate - processedDate.getTimezoneOffset() * 60000); var d = processedDate.getDate(); var m = processedDate.getMonth()+1; var y = processedDate.getFullYear(); return ('' + (m <= 9 ? '0' + m: m ) + '-' + (d <=9 ? '0' + d : d) + '-' + y); 
        return processedDate;
    }
    
    function displayDatatable() {
        $('#hotelCapacityTable_filter').hide();
        
        var table = $('#hotelCapacityTable').DataTable();
 
        $('#hotelCapacityTable tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );
        
        
        $('#button').click( function () {
               var hotelCapacityId = '0';
               hotelCapacityId = getSelectedHotelCapacityId();
               if(hotelCapacityId == '0'){
                   alert('Please Select Hotel Capacity ');
               }else{
                   //spinner();
                   $('.loader').show();
                   updateAccountParticipants(hotelCapacityId);             
               }
        });
        table.$('tr').click(function(){
            var setHc = $(this).find('a').attr('id');
            $('#rdo_'+setHc).prop('checked',true);
        });
    }
    
    /* Method to Create data set used to populate table rows */
    function createDataSet(records){
        var dataSet = new Array();
        if(records != null){
            for (var i = 0; i < records.length; i++) {
                //var processedDate = ;
                var dataRow = new Array();
                
                dataRow =[
                         "<input type='radio' name='rdo_hc' value='"+records[i].Id+"' Id='rdo_"+records[i].Id+"' >",
                         (records[i].Name != undefined?"<a target='_blank' class='trid' id='"+records[i].Id+"'  href='../../"+records[i].Id+"'>"+records[i].Name+"</a>" :'- none -'),
                         records[i].RecordType != undefined?records[i].RecordType.Name:'- none -',
                         records[i].Hotel_City__c || '- none -',
                         records[i].First_event_day_in_this_Hotel__c != undefined?convertDateFormat(records[i].First_event_day_in_this_Hotel__c):'- none -'
                    ];
              
                dataSet.push(dataRow);
            }
        return dataSet;
        }
        else{
            return null;
        }
    }
    
    /* Method to create hotel capacity datatable */
    function createHotelCapacityTable(dataSet) {
        console.log('dataSet :' + dataSet);
        $('#hotelCapacityTable').DataTable({
            paging: true,
            destroy: true,
            bStateSave: true,
            data: dataSet,
            oLanguage: {"sEmptyTable": "<strong>No Hotel Capacity records are available for associated ICEF Event.</strong>"},
            columns: [
                        { title: "Select" },
                        { title: "Hotel Capacity" },
                        { title: "Record Type" },
                        { title: "Hotel City" },
                        { title: "First event day in this Hotel" }
                     ]
        });
    }
            
    /* Used to initiate methods on page load*/      
    $(document).ready(function(){
        $('.loader').show();
        if('{!$CurrentPage.parameters.ids}') {
            
            getAccountPartRecords();
        }
    });      
    
    function getSelectedHotelCapacityId(){
        var hcId = '0';
        $('[name="rdo_hc"]').each(function(){ 
            if($(this).is(':checked')) {
                console.log('hotel capacity '+ $(this).val());
                hcId = $(this).val(); 
            }
        });
        return hcId;  
    }
    
    /* Method to get ids from url */
    function getIds(){
        var paramdecoded = '{!$CurrentPage.parameters.ids}';
        
        paramdecoded = paramdecoded.replace('[','');
        paramdecoded = paramdecoded.replace(']','');
        var params = new Array();
        params = paramdecoded.split(",");
        
        return params;
    }
    
    function spinner(){       
        $('.loader').show();        
    }
    
    function checkStatusAndRedirect(status, updatedCount) {
        if(status == 'Completed') {
            /*setTimeout(function(){
                //do what you need here
            }, 1000);*/
            $('.loader').hide();
            var msg = 'Successfully updated '+ updatedCount +' records out of ' + getIds().length;
            alert(msg);
            window.close();
        } else if( status == 'Aborted' || status == 'Failed') {
            if(confirm("Batch execution is " + status + '. Do you want to close the window?')) {
                window.close();
            }else {
                $('.loader').hide();
            }
        }
    }
    
    /* Method to get ids from url */
    function getIds(){
       
        var paramdecoded = '{!$CurrentPage.parameters.ids}';
        
        paramdecoded = paramdecoded.replace('[','');
        paramdecoded = paramdecoded.replace(']','');
        var params = new Array();
        params = paramdecoded.split(",");
        
        return params;
    }
    
    </script>
</head>
<body>
    <div style="padding-top:20px;">
        <apex:pagemessages id="msgPanel" ></apex:pagemessages>
    </div>
    <div id="errorMsgs" class="message errorM3" style="display:none">
        <span class="msgIcon"></span><h4 style="color:#cc0000;">Error:</h4>
        <span id="msgInfo_custom"> </span>
    </div>
    <apex:form rendered="{!!ISBLANK($CurrentPage.parameters.ids)}">
    <div class="loader" style="display:none;" ><div class="processIcon" ></div></div>
    <apex:actionStatus id="showProcessing" stopText="" styleClass="showProcessingCls">
        <apex:facet name="start">
            <div class="loader" style="display:none;" ><div class="processIcon" ></div></div>
            <!--<div class="loadingBg"></div>
            <div class="loadingWrap">
                <img src="{!$Resource.spinner}" class="loadingImg"/>
            </div>-->
        </apex:facet>
    </apex:actionStatus>
    <apex:actionFunction name="updateAccountParticipants" reRender="msgPanel,actionPoller" action="{!updateAccountParticipants}">
        <apex:param name="hotelCapacityId" value=""/>
    </apex:actionFunction>
    
    <apex:actionPoller action="{!checkAccountPartUpdateStatus}" reRender="msgPanel" interval="5" enabled="{!isPollerActive}" id="actionPoller" oncomplete="checkStatusAndRedirect('{!batchExecutionStatus}','{!updatedCount}');" />
    
    <div class="hc_container"  />
    <!------ Title ------>
    <div align="center" style="font-size: large;">Select Hotel Capacity</div>
    
    <hr width="100%"/>
    <!------ Buttons  ------>
    <div align="center" style="margin-top:20px;margin-bottom:20px">
        <input type="button" class="button newButton" id="button" value="Save" />
        <input type="button" class="button newButton" onClick="window.close();" value="Cancel" />
    </div>
    <!------ Hotel Capacity Table ------>
    <table id="hotelCapacityTable" class="table-responsive table stripe" width="100%" >
    </table>
    </apex:form>
</body>

</apex:page>