/**
 * This trigger on is written to auto-populate the Country lookup field
 * on Lead with the Country picklist field on Lead.
 *
 * @modified by     Jasmine J 
 * @modified date   25/09/2015 
 */
trigger setCountryLookupOnLead on Lead (before insert, before update) {
    List<Lead> led_list = new List<Lead>();
    List<String> listCountry = new List<String>();
    Map<String, Id> mapCountries = new Map<String, Id>();
    List<Country__c> listLookupCountry = new List<Country__c>();
    
    if (Trigger.isInsert){
        // Create the list of required Country through the loop
        for (Lead led : Trigger.new){            
            if(led.Country__c != ''){
                listCountry.add(led.Country__c);                          
            }
        }            
    }
    
    if(Trigger.isUpdate) {
        // Create the list of required Country through the loop
        for (Lead led : Trigger.new){ 
            if(led.Country__c != ''){
                listCountry.add(led.Country__c);                
            }
        }        
    }
    
    System.debug('listCountry ::'+listCountry);
    
    // Query the Countries outside the loop
    listLookupCountry = [SELECT Id, Name FROM Country__c WHERE Name IN :listCountry];
    System.debug('listLookupCountry ::'+listLookupCountry);
    
    // Create Map of Country and simply assign to the lookup.
    if(!listLookupCountry.isEmpty()) {
        for(Country__c country : listLookupCountry){
            mapCountries.put(country.Name, country.Id);     
        }
        System.debug('mapCountries ::' +mapCountries);
    }
    
    
    // Run loop through the records again and assign the lookup id to the records.
    for (Lead led : Trigger.new) {            
        if(led.Country__c != '') {
            led.Country_Lookup__c = mapCountries.get(led.Country__c);
            System.debug('mapCountries.get(led.Country__c) ::'+mapCountries.get(led.Country__c));
        }
        else 
            led.Country_Lookup__c = null;        
    } 
}