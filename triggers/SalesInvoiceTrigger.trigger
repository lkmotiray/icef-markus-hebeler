/****************************************************************
** Trigger to roll-up Total number of Sales Invocies related to opportunity.
*****************************************************************/
trigger SalesInvoiceTrigger on c2g__codaInvoice__c (after insert, after delete, after undelete, after update) 
{

    RollupSalesInvoiceHandler handler = new RollupSalesInvoiceHandler();
    //--check if Trigger is called after DML operation.
    if(Trigger.isAfter)
    {
        //--Check if Trigger is called after insert operation
        if(Trigger.isInsert)
        {
            handler.insertInvoice(Trigger.new);            
        }
        
        if(Trigger.isUpdate){
            System.debug('Is in update ');
            List<c2g__codaInvoice__c> listOfSaleInvoices = new List<c2g__codaInvoice__c>();
            for(c2g__codaInvoice__c salesInv : Trigger.new){
                c2g__codaInvoice__c oldInv = Trigger.oldMap.get(salesInv.Id);
                if( oldInv.c2g__InvoiceStatus__c != salesInv.c2g__InvoiceStatus__c && 
                    salesInv.c2g__InvoiceStatus__c.equalsIgnoreCase('Complete') ){
                    System.debug('Is in update Complete');
                    listOfSaleInvoices.add(salesInv);
                }
            }
            if(listOfSaleInvoices != null && !listOfSaleInvoices.isEmpty()){
                System.debug('Is in listOfSaleInvoices '+listOfSaleInvoices);
                handler.insertInvoice(listOfSaleInvoices);
            }   
        }
       /* 
        //--Check if Trigger is called after delete operation
        if(Trigger.isDelete)
        {
            handler.deleteInvoice(Trigger.old);
        }    
        //--Check if Trigger is called after undelete operation
        if(Trigger.isUndelete)
        {
            handler.undeleteInvoice(Trigger.new);
        }*/
    }  
}