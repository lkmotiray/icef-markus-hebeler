/*--------------------------------------
    @Developer:     Henriette Schönleber
    @Purpose:       trigger to update fields in Member account of Membership
    @Created Date:  2018/02/07
    @Modified Date: 2018/02/09
 ----------------------------------------*/


trigger MembershipTrigger on Membership__c (after insert, after update, after delete) {
    if (Trigger.IsInsert){
        if(Trigger.IsAfter){
            //update accounts' fields: Memberships, Industry Qualification, Industry Recognition
            MembershipsHandler.updateMembershipsInAccount(Trigger.newMap);
        }
    } else if (Trigger.IsUpdate){
        if(Trigger.isAfter){
            //update accounts' fields if necessary: Memberships, Industry Qualification, Industry Recognition
            MembershipsHandler.updateMembershipsInAccount(Trigger.newMap);
        }
    } else if (Trigger.IsDelete){
        if(Trigger.isAfter){
            //update accounts' fields if necessary: Memberships, Industry Qualification, Industry Recognition
            MembershipsHandler.updateMembershipsInAccount(Trigger.oldMap);
        }
    }
}