/*****************************************************************

Purpose of trigger:

Each time the contact is created or updated the field ‘magic_link’ in Contacts. The value of this field should be as follows:
 
1 = contact.email
2 = unix time stamp
3 = random value with 10 letters
4 = secret is a given string
5 = SHA1 hash out of [1, 2, 4, 3].join('')

******************************************************************/
trigger setMagicLinkValue on Contact (before insert, before update) 
{
    Long timeStamp;
    String randomValue, secret = 'ejesheitohn8eeV7yu7j', openid, combinedString, sha1Coded;
    //List<String> values;
    
    for(Contact cont : Trigger.new)
    {
        //values = new List<String>();
        
        timeStamp = System.Now().getTime()/1000;
        randomValue = EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(0,10);
        openid = cont.email;
        /*values.add(openid);
        values.add(String.valueof(timeStamp));
        values.add(secret);
        values.add(randomValue);*/
        
        combinedString = openid + timeStamp + secret + randomValue;
        
        sha1Coded = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', Blob.valueOf(combinedString)));
        
        cont.magic_link__c = '/login/magic?openid=' + cont.email +'&time_stamp='+ String.ValueOf(timeStamp) +'&salt='+ randomValue +'&hash='+ sha1Coded;                              
    }
}