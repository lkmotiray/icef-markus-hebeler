/*
@ Name : onChangedOfICEFEventTrigger (Trigger)
@ Description : 1) When ICEF Event of AP is changed and their exist PP related to AP 
@                  through Account and having same ICEF Event as old ICEF Event of AP. 
@                  Then send email alert to this ICEF Event manager.
@               2) If ICEF Event of AP is changed, then update the ICEF Event of its related CP.
@ Notation : AP - Account Particiaption
@            CP - Contact Particiaption
@            PP - Product Particiaption
@ Developer : Shaikh Saquib
@ Date : 09-Dec-2014
*/

trigger onChangedOfICEFEventTrigger on Account_Participation__c (before update, after update) {

    if(Trigger.isBefore && Trigger.isUpdate) {           
            
        ICEFEventChanged handler = new ICEFEventChanged();
        
        System.debug('email Trigger.new ::: ' + Trigger.new);
        System.debug('email Trigger.old ::: ' + Trigger.old);
        
        if(checkRecursive.runOnce()) {
           
            handler.icefEventChangedOnAP(Trigger.new,Trigger.oldMap); 
            
            System.debug('\n LIST OF Account_Participation__c RECORDs (after UPDATE) \n' +  Trigger.new );
        }        
    }
    
    if(Trigger.isAfter && Trigger.isUpdate) { 
    
        ICEFEventChanged handler = new ICEFEventChanged();
        
        System.debug('email Trigger.new ::: ' + Trigger.new);
        System.debug('email Trigger.old ::: ' + Trigger.old);
        
        handler.changeICEFEventOnCP(Trigger.newMap, Trigger.oldMap);        
    }

}