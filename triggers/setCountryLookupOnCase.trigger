trigger setCountryLookupOnCase on Case (before insert, before update) 
{
    List<Lead> led_list = new List<Lead>();
    
    if (Trigger.isInsert || Trigger.isUpdate)
    {
        for (Case cse : Trigger.new)
        {
            if(cse.Country_of_Business_picklist__c != '')
            {
                List<Country__c> ctry = [Select Id from Country__c where Name =: cse.Country_of_Business_picklist__c];
                if(ctry.size() != 0)
                {
                     cse.Country__c = ctry[0].Id;             
                }
                else
                {
                    cse.Country__c = null;
                }
            }
        }               
    }    
}