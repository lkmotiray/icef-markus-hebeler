trigger updateContactPassword on Contact (before insert, before update) {

    Contact[] cs = Trigger.new;
    for (Contact c:cs)
    {
        if (c.Password__c==null)
        {
            Datetime saltDate = Datetime.now();
            String apiString = String.valueOfGmt(saltDate) + String.valueOf(c.id);
            Blob apiBlob = Crypto.generateDigest('SHA-256', Blob.valueOf(apiString));
        
            c.Password__c = EncodingUtil.convertToHex(apiBlob).substring(0, 8);
            c.password_update__c = saltDate;
        }
    }

}