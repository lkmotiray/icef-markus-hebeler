/*
    @ Created By : Shaikh Saquib
    @ Date       : 21-10-2014
    @ Description: Trigger for updating 'Countries_Recruiting_From_text__c' text field related to 'Countries_recruiting_from__c'
    @			   multipicklist field type.
    */
    
    trigger updateCountriesRecruitingFrom on Account_Participation__c (before insert, before update) {
        
        // List of new or updated records
        List<Account_Participation__c> accntPartList = new List<Account_Participation__c>(Trigger.new);
        String strMultiPicklistValue, strCountriesRecrutingFrom;
        
        if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate))
        {	
            try
            {
                for(Account_Participation__c accntPartRecord : accntPartList) 
                {
                    // Get the selected multiple picklist values.
                    strMultiPicklistValue = accntPartRecord.Countries_recruiting_from__c;
                   
                    // Assign selected values to text field separated by ','
                    if(strMultiPicklistValue != null) 
                    {                        
                        strCountriesRecrutingFrom = strMultiPicklistValue.replaceAll(';', ', ');
                        if(strCountriesRecrutingFrom.length() > 255) 
                        {
                            accntPartRecord.Countries_Recruiting_From_text__c = strCountriesRecrutingFrom.substring(0, 255); 							
                        }
                        else 
                        {
                            accntPartRecord.Countries_Recruiting_From_text__c = strCountriesRecrutingFrom;    
                        }       
                    }  
                    else
                    {
                        accntPartRecord.Countries_Recruiting_From_text__c = null;
                    }
                }
            }
            catch(Exception e)
            {
                System.debug('ERROR : ' + e.getMessage());
            }
        }
        
    }