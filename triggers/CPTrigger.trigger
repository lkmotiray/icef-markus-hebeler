trigger CPTrigger on Contact_Participation__c (after delete, after insert, after update, after undelete) 
{
    system.debug(' Trigger called');
    if(Trigger.isAfter) {
        system.debug(' isAfter');
        if(Trigger.isDelete ){
            system.debug('Trigger.isDelete');
            CPTriggerHandler.updateAPfromCP(Trigger.Old);
        }
        if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUndelete){
            system.debug('Trigger.isInsert || Trigger.isUpdate');
            CPTriggerHandler.updateAPfromCP(Trigger.New);
        }
    }
}