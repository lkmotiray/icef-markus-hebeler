/********************************************************************************************
Developer   - Dreamwares (Amol)
Description - Populate Catalogoue Entry word count
Date        - 08 Apr. 2015
********************************************************************************************/

trigger AccountParticipationTrigger on Account_Participation__c (before insert, before update,after update) 
{
    if(Trigger.isBefore) {
        //create handler class instance object
        AccParticipationTriggerHandler triggerHandler = new AccParticipationTriggerHandler();
        
        //call handler class method on insert operation
        if(Trigger.isInsert)
        {
            System.debug('AccountParticipationTrigger is called: before insert');
            triggerHandler.updateCatlogEntryWordCount(Trigger.new);
//            triggerHandler.setNumberOfRequiredAccommodations(Trigger.newMap); es wird IMMER erst die AP erstellt, danach die CPs. 
        }
        
        //call handler class method on update operation
        if(Trigger.isUpdate)
        {
            System.debug('AccountParticipationTrigger is called: Before update');
            List<Account_Participation__c> lstUpdatedAccPart = new List<Account_Participation__c>();
            List<Account_Participation__c> listUpdateAPAccom = new List<Account_Participation__c>();
            Account_Participation__c oldAccPart;
            
            //create list of Account Participation record whose Catalogoue Entry value is updated
            for(Account_Participation__c newAccPart :Trigger.new)
            {
                oldAccPart = new Account_Participation__c();
                
                oldAccPart = Trigger.oldMap.get(newAccPart.Id);
                
                if(newAccPart.Catalogue_entry__c != oldAccPart.Catalogue_entry__c)
                {
                    lstUpdatedAccPart.add(newAccPart);
                }
                if(newAccPart.No_Hotel_Room_needed__c != oldAccPart.No_Hotel_Room_needed__c){
                    listUpdateAPAccom.add(newAccPart);
                }
            }
            System.debug('AccountParticipationTrigger - AP list to update word count: Number' + lstUpdatedAccPart.size()+ ': '+lstUpdatedAccPart);
            System.debug('AccountParticipationTrigger - AP list to update required accommodations: Number' + listUpdateAPAccom.size()+ ': '+listUpdateAPAccom);
            if(!lstUpdatedAccPart.isEmpty())
            {
                triggerHandler.updateCatlogEntryWordCount(lstUpdatedAccPart);
            }
            if(listUpdateAPAccom.size() > 0){
                Map<Id, Account_Participation__c> ApMap = new Map<Id, Account_Participation__c>(listUpdateAPAccom);
                triggerHandler.setNumberOfRequiredAccommodations(ApMap);
            }
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            system.debug('AccPartTriggerHndlrForHRAccommodation  is called');
            if(!AccPartTriggerHndlrForHRAccommodation.isExecuted){
                AccPartTriggerHndlrForHRAccommodation.createHotelRoomAndAccommodation(Trigger.new,Trigger.oldMap);
            }
        }
    }
}