/***********************************************************************
This trigger is used to calculate the amount of tax for each line item 
and write the sum into field ‘Tax’ in the appropriate quote, when quote 
line item is edited/created/deleted.
***********************************************************************/

trigger populateTaxInQuote on QuoteLineItem (after delete, after insert, after update) 
{
    Decimal totalTaxAmount;
    String taxAmount;
    Integer taxAmt;
    
    Set<id> QuoteIds = new Set<id>();
    Set<Quote> QuotesToUpdate = new Set<Quote>();
    List<Quote> QuotesToUpdate_list = new List<Quote>();
    
    //Put all quote id's in SET
    if (Trigger.isUpdate || Trigger.isDelete) 
    {
        for (QuoteLineItem item : Trigger.old)
            QuoteIds.add(item.QuoteId);
    }
    else
    {
        for (QuoteLineItem item : Trigger.new)
            QuoteIds.add(item.QuoteId);
    }
    
    // get a map of the Quote
    Map<id,Quote> QuoteMap = new Map<id,Quote>([select id, Tax from Quote where id IN :QuoteIds]);
 
    Map<Id, QuoteLineItem> qliMap = new Map<Id, QuoteLineItem>();
    
    Map<Id, List<QuoteLineItem>> quoteLineItemWithQuote = new Map<Id, List<QuoteLineItem>>();
    
    qliMap.putAll([select Id, Tax__c, QuoteId, TotalPrice from QuoteLineItem where QuoteId IN: QuoteIds]);
        
    //Fill quoteLineItemWithQuote Map with quote and their quote line items
    for(Id qliId: qliMap.keySet())
    {
         Id key = qliMap.get(qliId).QuoteId;
         List<QuoteLineItem> groupedQLIs = quoteLineItemWithQuote.get(key);
         if (null == groupedQLIs)
         {
             groupedQLIs = new List<QuoteLineItem>();
             quoteLineItemWithQuote.put(key, groupedQLIs);
         }
       
         groupedQLIs.add(qliMap.get(qliId));                          
    }    
     
    //Access all quote line items to calculate tax field value in quote
    for(ID qId: quoteLineItemWithQuote.KeySet())
    {
        totalTaxAmount = 0; 
        taxAmt = 0;    
        
        List<QuoteLineItem> quotelineitems = new List<QuoteLineItem>();
        
        if(quotelineitems != null)
        {
            quotelineitems = quoteLineItemWithQuote.get(qId);
                        
            for(QuoteLineItem qli : quotelineitems)
            {
                taxAmount = qli.Tax__c;
                if(taxAmount != null)
                {
                    taxAmount = taxAmount.Replace('%', '');
                    taxAmt = Integer.valueOf(taxAmount);                
                    totalTaxAmount += ((qli.TotalPrice * taxAmt) / 100); 
                }
            }
            QuoteMap.get(qId).Tax = totalTaxAmount;
            // add the value in the map to a list so we can update it
            QuotesToUpdate.add(QuoteMap.get(qId));
        }
    }
    QuotesToUpdate_list.clear();
    QuotesToUpdate_list.addAll(QuotesToUpdate);
    if(QuotesToUpdate_list.size() != null)
    {
        update QuotesToUpdate_list;
    }
}