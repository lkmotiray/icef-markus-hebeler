trigger assignAccountId on Account (after insert) 
{    
    assignNewlyCreatedAccountID ancaId = new assignNewlyCreatedAccountID();
    for(integer i = 0; i < Trigger.new.size(); i++)
    {
        ancaId.assignAccountId(Trigger.new[i].Id);
    }
}