/***********************************************
Purpose of trigger:
This trigger is used to set contact owner same as 
Account Owner on insertion of Contact. 

************************************************/

trigger setOwnerAsAccount on Contact (before insert, before update) 
{
    Set<id> AccountIds = new Set<id>();
    
    for (Contact cont : Trigger.new)
        AccountIds.add(cont.AccountId);
        
    // get a map of the Account 
    Map<id,Account> AccountMap = new Map<id,Account>([select id, OwnerId from Account where id IN :AccountIds]);    
    
    
       
    for(Contact cont : Trigger.new)
    {
        if(AccountMap.containsKey(cont.AccountId))
            cont.OwnerId =  AccountMap.get(cont.AccountId).OwnerId;                         
    }
}