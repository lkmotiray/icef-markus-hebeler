/*
 *  @purpose Syncing quoteLineItem to opportunityLineItem
 *  @recreatedby Mamta Kukreja
 *  @recreateddate 30/6/15
 */
trigger QuoteLineSyncTrigger on QuoteLineItem (before insert, after insert, after update, after delete) {

    QuoteLineSyncTriggerHandler handler = new QuoteLineSyncTriggerHandler();
    
    if(trigger.isInsert && trigger.isBefore){
        handler.beforeInsert(trigger.new);
    }
    if (TriggerStopper.stopQuoteLine) return;
    
    if(trigger.isInsert && trigger.isAfter){
        handler.afterInsert(trigger.new);
    }
    else if(trigger.isUpdate) {
        handler.afterUpdate(trigger.new, trigger.old, trigger.oldMap);
    }
    else if(trigger.isDelete) {
        handler.afterDelete(trigger.old);
    }
}