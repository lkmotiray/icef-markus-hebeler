/*********
 * Purpose: on change of CP_links_on_external_sites__c (Roll-up summary for active CoursePricer Agencies)
 * 			- update Names of Agencies if Number of links is bigger than 0
 * Date: 27.08.2018
 * Developer: Henriette Schönleber
 *********/

trigger coursefinderSchoolTrigger on Coursefinder_School__c (before update) {
    if (Trigger.isBefore && Trigger.isUpdate){
        System.debug('Trigger isBefore && Trigger isUpdate');
        //update names of CoursePricer Agencies before update
        List<Coursefinder_School__c> fieldToUpdate = new List<Coursefinder_School__c>();
            
        for(Coursefinder_School__c newRecord : Trigger.new){
            Coursefinder_School__c oldRecord = Trigger.oldMap.get(newRecord.Id);
            if(newRecord.CP_links_on_external_sites__c <> oldRecord.CP_links_on_external_sites__c){
                fieldToUpdate.add(newRecord);
            }
        }
        //update field if fieldToUpddate > 0
        if(fieldToUpdate.size() > 0){
            Map<Id,Coursefinder_School__c> mapSchoolsFieldToUpdate = new Map<Id, Coursefinder_School__c>(fieldToUpdate);
            System.debug('Agency Name field to update in: ' + mapSchoolsFieldToUpdate.keySet());
            coursefinderSchoolTriggerHelper.updateNamesOfAgencies(mapSchoolsFieldToUpdate);
        }
    }
}