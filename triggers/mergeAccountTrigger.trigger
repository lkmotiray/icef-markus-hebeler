/*--------------------------------------
    @Developer:     Henriette Schönleber
    @Purpose:       trigger to make sure triggers on child records are fired after account has been merged (as loser)
    @Created Date:  2018/02/07
    @Modified Date: 
 ----------------------------------------*/
trigger mergeAccountTrigger on Account (after delete) {
    //update Nudge field in one membership of each account to fire triggers on newly allocated Memberships (MembershipTrigger)
    mergeAccountHandler.fireMembershipTrigger(Trigger.OldMap);
}