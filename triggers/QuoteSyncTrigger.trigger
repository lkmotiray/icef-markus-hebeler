trigger QuoteSyncTrigger on Quote (after insert, after update) 
{
    if(trigger.isInsert)
        TriggerStopper.newQuote = true;
    else
        TriggerStopper.newQuote = false;
    
    if (TriggerStopper.stopQuote) return;
        
    TriggerStopper.stopQuote = true;    

    Set<String> quoteFields = QuoteSyncUtil.getQuoteFields();
    List<String> oppFields = QuoteSyncUtil.getOppFields();
    
    String quote_fields = QuoteSyncUtil.getQuoteFieldsString();
    
    String opp_fields = QuoteSyncUtil.getOppFieldsString();

    Map<Id, Id> startSyncQuoteMap = new Map<Id, Id>();
    String quoteIds = '';
    for (Quote quote : trigger.new) {
        if (quote.isSyncing && !trigger.oldMap.get(quote.Id).isSyncing) 
        {
            startSyncQuoteMap.put(quote.Id, quote.OpportunityId);
        }
        if (quoteIds != '') quoteIds += ', ';
        quoteIds += '\'' + quote.Id + '\'';
    }

    String quoteQuery = 'select Id, OpportunityId, isSyncing' + quote_fields + ' from Quote where Id in (' + quoteIds + ')';
    //System.debug(quoteQuery);     

    List<Quote> quotes = Database.query(quoteQuery);
    
    String oppIds = '';    
    Map<Id, Quote> quoteMap = new Map<Id, Quote>();
    
    for (Quote quote : quotes) {
        if (trigger.isInsert || (trigger.isUpdate && quote.isSyncing)) {
            quoteMap.put(quote.OpportunityId, quote);
            if (oppIds != '') oppIds += ', ';
            oppIds += '\'' + quote.opportunityId + '\'';            
        }
    }
    
    if (oppIds != '') {
        String oppQuery = 'select Id' + opp_fields + ' from Opportunity where Id in (' + oppIds + ')';
        //System.debug(oppQuery);     
    
        List<Opportunity> opps = Database.query(oppQuery);
        List<Opportunity> updateOpps = new List<Opportunity>();
        List<Quote> updateQuotes = new List<Quote>();        
        
        for (Opportunity opp : opps) 
        {
            Quote quote = quoteMap.get(opp.Id);
            boolean hasChange = false;
            for (String quoteField : quoteFields) 
            {
                String oppField = QuoteSyncUtil.getQuoteFieldMapTo(quoteField);
                Object oppValue = opp.get(oppField);
                Object quoteValue = quote.get(quoteField);
                if (oppValue != quoteValue) 
                {                   
                    if (trigger.isInsert && quoteValue == null) 
                    {
                        quote.put(quoteField, oppValue);
                        hasChange = true;                          
                    } else if (trigger.isUpdate) 
                    {
                        if (quoteValue == null) opp.put(oppField, null);
                        else opp.put(oppField, quoteValue);
                        hasChange = true;                          
                    }                    
                }                     
            }    
            if (hasChange) {
                if (trigger.isInsert) { 
                    updateQuotes.add(quote);
                } else if (trigger.isUpdate) {
                    updateOpps.add(opp);                
                }               
            }                                  
        } 
   
        if (trigger.isInsert) {
            Database.update(updateQuotes);
        } else if (trigger.isUpdate) {
            TriggerStopper.stopOpp = true;            
            Database.update(updateOpps);
            TriggerStopper.stopOpp = false;              
        }    
    }
    
    // Check start sync quote with matching opp lines and quote lines
    /*
    if (!startSyncQuoteMap.isEmpty()) {
    
        String syncQuoteIds = '';
        String syncOppIds = '';
        
        for (Id quoteId : startSyncQuoteMap.keySet()) {
            if (syncQuoteIds != '') syncQuoteIds += ', ';
            syncQuoteIds += '\'' + quoteId + '\'';
                           
            if (syncOppIds != '') syncOppIds += ', ';
            syncOppIds += '\'' + startSyncQuoteMap.get(quoteId) + '\'';
        }
        
          
        String qliFields = QuoteSyncUtil.getQuoteLineFieldsString();    
        String oliFields = QuoteSyncUtil.getOppLineFieldsString(); 
                
        String qliQuery = 'select Id, QuoteId, PricebookEntryId, UnitPrice, Quantity, Discount' + qliFields + ' from QuoteLineItem where QuoteId in (' + syncQuoteIds + ')';           
        String oliQuery = 'select Id, OpportunityId, PricebookEntryId, UnitPrice, Quantity, Discount, SortOrder' + oliFields + ' from OpportunityLineItem where OpportunityId in (' + syncOppIds + ')';   
    
        List<QuoteLineItem> qlis = Database.query(qliQuery);   
        List<OpportunityLineItem> olis = Database.query(oliQuery);
        
        Map<Id, List<OpportunityLineItem>> oppToOliMap = new Map<Id, List<OpportunityLineItem>>();
        Map<Id, List<QuoteLineItem>> quoteToQliMap = new Map<Id, List<QuoteLineItem>>();        
        
        for (QuoteLineItem qli : qlis) {
            List<QuoteLineItem> qliList = quoteToQliMap.get(qli.QuoteId);
            if (qliList == null) {
                qliList = new List<QuoteLineItem>();
            } 
            qliList.add(qli);  
            quoteToQliMap.put(qli.QuoteId, qliList);        
        }
        
        for (OpportunityLineItem oli : olis) {
            List<OpportunityLineItem> oliList = oppToOliMap.get(oli.OpportunityId);
            if (oliList == null) {
                oliList = new List<OpportunityLineItem>();
            } 
            oliList.add(oli);  
            oppToOliMap.put(oli.OpportunityId, oliList);       
        }        
          
        Set<OpportunityLineItem> updateOlis = new Set<OpportunityLineItem>();  
        Set<String> quoteLineFields = QuoteSyncUtil.getQuoteLineFields();
          
        for (Id quoteId : startSyncQuoteMap.keySet()) {
            Id oppId = startSyncQuoteMap.get(quoteId);
            List<QuoteLineItem> quotelines = quoteToQliMap.get(quoteId);
            List<OpportunityLineItem> opplines = oppToOliMap.get(oppId);
            
            if (quotelines != null && opplines != null && !quotelines.isEmpty() && !opplines.isEmpty()) {
            
                for (QuoteLineItem qli : quotelines) {
                    boolean hasChange = false;
                                                  
                    for (OpportunityLineItem oli : opplines) {
                        if (oli.pricebookentryid == qli.pricebookentryId  
                            && oli.UnitPrice == qli.UnitPrice
                            && oli.Quantity == qli.Quantity
                            && oli.Discount == qli.Discount
                            //&& oli.SortOrder == qli.SortOrder
                           ) {
                           
                            if (updateOlis.contains(oli)) continue; 
                            
                            //System.debug('########## qliId: ' + qli.Id + '     oliId: ' + oli.Id);
                              
                            for (String qliField : quoteLineFields) {
                                String oliField = QuoteSyncUtil.getQuoteLineFieldMapTo(qliField);
                                Object oliValue = oli.get(oliField);
                                Object qliValue = qli.get(qliField);
                                if (oliValue != qliValue) {
                                    hasChange = true;
                                    if (qliValue == null) oli.put(oliField, null);
                                    else oli.put(oliField, qliValue);                                                                
                                }    
                            }
                            
                            if (hasChange) {
                                updateOlis.add(oli);
                            }
                                
                            break;        
                        }                        
                    }
                }
            }
         }
         
         if (!updateOlis.isEmpty()) {
             List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
             oliList.addAll(updateOlis);
             
             TriggerStopper.stopOpp = true;              
             TriggerStopper.stopOppLine = true;
             TriggerStopper.stopQuoteLine = true;  
                        
             Database.update(oliList);
             
             TriggerStopper.stopOpp = false;              
             TriggerStopper.stopOppLine = false;
             TriggerStopper.stopQuoteLine = false;                               
         }                
    }
    */
    
    TriggerStopper.stopQuote = false; 
    System.debug(' TriggerStopper.newQuote  in QuoteSync::: '+TriggerStopper.newQuote);
}