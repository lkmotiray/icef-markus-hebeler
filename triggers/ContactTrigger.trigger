/*
 *  @purpose - in Contact: set Password__c and password_update__c if empty
 *           - in Contact: set API token if empty
 *           - in Contact: update magic_link__c
 *  @createdby Mamta Kukreja, Amol Kulthe
 *  @createddate 15/6/15
 *  @modifiedby Henriette Schönleber 
 *  @mergedwith AccountRelatedContactTrigger 26.11.2018
 *  @modified 22.11.2018
 */


trigger ContactTrigger on Contact (before insert, before update) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            ContactTriggerHandler.isBeforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate){
            ContactTriggerHandler.isBeforeUpdate(Trigger.new);
        }
    }
}