/**
 *  @Purpose:  Notification for Limit of CP Agencies reached    
 *  @Author : Dreamwares
 *  @Date 12-09-2018
 */
trigger Coursefinder_School_Trigger on Coursefinder_School__c (after update) {
    
    if(trigger.isAfter){       
        if(trigger.isUpdate){
            system.debug('update Coursefinder_School_Trigger'); 
            Coursefinder_School_TriggerHandler handler = new Coursefinder_School_TriggerHandler();
            handler.handleUpdatedCoursefinder_Schools(trigger.new, trigger.oldMap);
        }
    }
    
}