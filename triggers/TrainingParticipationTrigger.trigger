/******************************************************************************************
Developer   = Dreamwares (India)
Description = Updates the value of TP Owner field
Events      = After Insert of Training Participation
Date        = 16-01-2015 
******************************************************************************************/

trigger TrainingParticipationTrigger on Training_Participation__c (after insert) 
{
    //Call afterInsert method of TrainingParticipationTriggerHandler
    TrainingParticipationTriggerHandler triggerHandler = new TrainingParticipationTriggerHandler();
    triggerHandler.afterInsert(Trigger.newMap);
}