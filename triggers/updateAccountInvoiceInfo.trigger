trigger updateAccountInvoiceInfo on Opportunity (after insert, after update)
{  
    if(Trigger.isInsert && Trigger.isAfter){
        IcefUpdate.accountInvoiceInfo(Trigger.new);
    
    }
    if(Trigger.isUpdate && Trigger.isAfter){
        IcefUpdate.accountInvoiceInfo(Trigger.new); 
    }
}