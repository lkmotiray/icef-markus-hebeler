trigger updateAPRecordTypeAndAPStatus on Account_Participation__c (after update) 
{
    //The map allows us to keep track of the accounts
    //that actually have new record type
    
    if(trigger.isAfter && trigger.isUpdate){
        
        Map<Id, Account_Participation__c> accountsWithNewRecordTypePartStatus = new Map<Id, Account_Participation__c>();
        
        List<Contact_Participation__c> updatedContactPart = new List<Contact_Participation__c>();
        
        if (trigger.new != null || !trigger.new.isEmpty()){
            
            for(Account_Participation__c accountParticipation : trigger.new){
                if(accountParticipation.RecordTypeId != null 
                   && (accountParticipation.RecordTypeId != trigger.oldMap.get(accountParticipation.Id).RecordTypeId 
                       || accountParticipation.Participation_Status__c !=  trigger.oldMap.get(accountParticipation.Id).Participation_Status__c))
                {
                    
                    accountsWithNewRecordTypePartStatus.put(accountParticipation.id,accountParticipation);                    
                }
            }
        }
        
        for (Contact_Participation__c contactParticipation  : [SELECT Id, Account_Participation_Record_Type_Text__c, 
                                                               Account_Participation_Status_Text__c, Account_Participation__c 
                                                               FROM Contact_Participation__c 
                                                               WHERE Account_Participation__c in :accountsWithNewRecordTypePartStatus.keySet()])
        {
            if(accountsWithNewRecordTypePartStatus.containsKey(contactParticipation.Account_Participation__c)){
                Account_Participation__c accountparticipation = accountsWithNewRecordTypePartStatus.get(contactParticipation.Account_Participation__c);
                String recordtypename = Schema.SObjectType.Account_Participation__c.getRecordTypeInfosById().get(accountparticipation.recordTypeId).getname(); 
                if(recordtypename != null){
                    contactParticipation.Account_Participation_Record_Type_Text__c = recordtypename;
                }                    
                contactParticipation.Account_Participation_Status_Text__c = accountparticipation.Participation_Status__c;
                updatedContactPart.add(contactParticipation);
            }
        } 
        update updatedContactPart;
    }
    
}