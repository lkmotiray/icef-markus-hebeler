/********************************************************************
Purpose:  
a.) A Dimension1 is created when a new ICEF event is created 
b.) the appropriate Dimension1 is renamed whenever the appropriate 
ICEF event is renamed.
********************************************************************/

trigger Dim4FromIcefEvent on ICEF_Event__c (after insert, after update) 
{
    List<c2g__codaDimension1__c> dimension1_new = new List<c2g__codaDimension1__c>();
    List<c2g__codaDimension1__c> dimension1_update = new List<c2g__codaDimension1__c>();
    
    //The map allows us to keep track of the ICEF events that actually have new Name
    Map<Id, ICEF_Event__c> event_map = new Map<Id, ICEF_Event__c>();    
    
    //Create new Dimension 1 records on insert of ICEF event
    if (Trigger.isInsert)
    {
        for (ICEF_Event__c event : Trigger.new)
        {
            c2g__codaDimension1__c dim1 = new c2g__codaDimension1__c();
            dim1.ICEF_Event__c = event.Id;
            dim1.Name = event.Name;
            dim1.c2g__ReportingCode__c = event.Name;
            dimension1_new.add(dim1);            
        }
        insert dimension1_new;
    }
    
    //Update existing Dimension 1 records on update of ICEF Event Name
    if(Trigger.isUpdate)
    {
        for(integer i = 0; i < Trigger.new.size(); i++)
        {
             if(Trigger.old[i].Name != Trigger.new[i].Name)
             {
                 event_map.put(Trigger.old[i].id, Trigger.new[i]);                                 
             }      
        }
        
        for (c2g__codaDimension1__c cd : [Select Id, ICEF_Event__c, Name, c2g__ReportingCode__c from c2g__codaDimension1__c where ICEF_Event__c IN: event_map.keySet()])
        {
            ICEF_Event__c cd_related_event = event_map.get(cd.ICEF_Event__c);
            cd.Name = cd_related_event.Name;
            cd.c2g__ReportingCode__c = cd_related_event.Name;
            dimension1_update.add(cd);
        }
        
        update dimension1_update;
    }
    
}