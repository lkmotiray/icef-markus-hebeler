trigger updateAccountApiToken on Account (before insert, before update)
{
	Account[] ass = Trigger.new; // as is reserved key word
	for (Account a:ass)
    {
        if (a.Api_Token__c==null)
        {
			Datetime saltDate = Datetime.now();
			String apiString = String.valueOfGmt(saltDate) + String.valueOf(a.id);
			Blob apiBlob = Crypto.generateDigest('SHA-256', Blob.valueOf(apiString));
		
			a.Api_Token__c = EncodingUtil.convertToHex(apiBlob);	
		}
    }
}