/**
 * This trigger on is written to change the Contact  
 * recordType on change of Account recordType.
 *
 * @modified by     Jasmine J 
 * @modified date   25/09/2015 
 */

trigger updateContactRecordType on Account (before insert) {
    //The map allows us to keep track of the accounts
    //that actually have new record type

   /* Map<Id, Account> accountsWithNewRecordType = new Map<Id, Account>();  
    Map<Id, String> mapAccountIdWithRecTypeName = new Map<Id, String>();
    Map<String, Id> mapAccNameWithContRecTypeId = new Map<String, Id>();
    List<Contact> updatedContacts = new List<Contact>();     
    Set<Id> setAccRecordTypes = new Set<Id>();
    
    for(Account sampleAccount : Trigger.New){
        if(sampleAccount.RecordTypeId != null){     // checks recordtype of the Accounts
            if(Trigger.oldMap.get(sampleAccount.Id).RecordTypeId != sampleAccount.RecordTypeId){
                accountsWithNewRecordType.put(Trigger.oldMap.get(sampleAccount.Id).id, sampleAccount);
                setAccRecordTypes.add(sampleAccount.RecordTypeId);
            }
        }
    }
    System.debug('accountsWithNewRecordType ::'+accountsWithNewRecordType);
    System.debug('setAccRecordTypes ::'+setAccRecordTypes);
    
    for(RecordType recType : [SELECT Id, Name FROM RecordType WHERE Id IN :setAccRecordTypes]){
        mapAccountIdWithRecTypeName.put(recType.Id, recType.Name);
    }
    System.debug('mapAccountIdWithRecTypeName ::'+mapAccountIdWithRecTypeName);
    
    for(RecordType recType : [SELECT Id, Name 
                              FROM RecordType 
                              WHERE Name IN :mapAccountIdWithRecTypeName.values()
                              AND SobjectType='Contact']) {
        mapAccNameWithContRecTypeId.put(recType.Name, recType.Id);
    }
    System.debug('mapAccNameWithContRecTypeId ::'+mapAccNameWithContRecTypeId);
    
    System.debug('Trigger.New[0] ::' +Trigger.New[0]);   
    
    for(Contact sampleContact : [SELECT Id, RecordTypeId, AccountId 
                     FROM contact 
                     WHERE accountId in :accountsWithNewRecordType.keySet()]) {
        Account parentAccount = accountsWithNewRecordType.get(sampleContact.accountId);
        if(parentAccount != null) {
            String recordTypeName = mapAccountIdWithRecTypeName.get(parentAccount.RecordTypeId);
            if(String.isNotBlank(recordTypeName)) {
                Id contactRecTypeId = mapAccNameWithContRecTypeId.get(recordTypeName);
                             
                if(contactRecTypeId != null) 
                    sampleContact.RecordTypeId = contactRecTypeId;
                else
                    sampleContact.RecordTypeId = null;
            
                updatedContacts.add(sampleContact);
            }                 
        }
    } 
    System.debug('updatedContacts ::'+updatedContacts);
    
    if(!updatedContacts.isEmpty())    
        update updatedContacts;   */
        if(true){}
}