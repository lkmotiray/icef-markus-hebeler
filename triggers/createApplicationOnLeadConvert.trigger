trigger createApplicationOnLeadConvert on Lead (after update) 
{
    //trigger to create Application object record on Lead convert and assign record type of Lead to the converted Account
    List<Application__c> app = new List<Application__c>();
    List<Account> acct = new List<Account>();
    String account_recordTypeName = '';
    System.debug('Trigger.new.size() 1 : '+Trigger.new.size());
    
    Map<String, Account>  idNAccountMap = new Map<String, Account>();
    Map<String, RecordType>  recordTypeMap = new Map<String, RecordType>();
    Map<String, List<Country__c>>  idNCountryListMap = new Map<String, List<Country__c>>();
    
    Set<Id> accountIdSet = new Set<Id>();
    Set<Id> RecordTypeId = new Set<Id>();
    Set<Id> countryIdSet = new Set<Id>();
    
    for(Lead leadRec : [SELECT Id, RecordTypeId, ConvertedAccountId FROM Lead WHERE Id IN: Trigger.new] ){
        accountIdSet.add(leadRec.ConvertedAccountId);
        RecordTypeId.add(leadRec.RecordTypeId);
    }
    
    for(Account accountRec :  [SELECT Id, Designated_Owner_Id__c, RecordTypeId, OwnerId, CreatedDate 
                               FROM Account 
                               WHERE Id =: accountIdSet]) {
                                  idNAccountMap.put(accountRec.Id, accountRec);
                               }
    for(RecordType rt : [SELECT Id, Name FROM RecordType WHERE Id IN: RecordTypeId] ){
        recordTypeMap.put(rt.Id, rt);
    }
    
    if (Trigger.new.size() != 0) 
    { 
        System.debug('Trigger.new.size() 2 : '+Trigger.new.size());

        //System.debug('Trigger.new[0].Events_interested_in__c::'+Trigger.new[0].Events_interested_in__c);
        for(integer i = 0; i < Trigger.new.size(); i++)
        {
            System.debug('Trigger.old[i].isConverted : '+Trigger.old[i].isConverted);
            System.debug('Trigger.new[i].isConverted : '+Trigger.new[i].isConverted);
            System.debug('Trigger.new[i].Events_interested_in__c :'+ Trigger.new[i].Events_interested_in__c);

            if ((Trigger.old[i].isConverted == false 
                && Trigger.new[i].isConverted == true 
                && Trigger.new[i].Events_interested_in__c != null)) 
            {
                System.debug('Converted Lead:'+Trigger.new[i]);
                Account at = idNAccountMap.get(Trigger.new[i].ConvertedAccountId);                
                System.debug('at at: '+at);
                
                System.debug('at1.CreatedDate.format(MM/dd/yyyy HH:mm) == System.now().format(MM/dd/yyyy HH:mm)): '+ at.CreatedDate.format('MM/dd/yyyy HH:mm') == System.now().format('MM/dd/yyyy HH:mm'));
                System.debug('(60 - Integer.valueOf(at1.CreatedDate.format(ss)) < 5) : '+(60 - Integer.valueOf(at.CreatedDate.format('ss')) < 5));
                System.debug('Integer.valueOf(at1.CreatedDate.format(mm))) == 1) : '+(Integer.valueOf(System.now().format('mm')) - Integer.valueOf(at.CreatedDate.format('mm'))));
                
                if((at.CreatedDate.format('MM/dd/yyyy HH:mm') == System.now().format('MM/dd/yyyy HH:mm')) 
                   || ((60 - Integer.valueOf(at.CreatedDate.format('ss')) < 5) 
                   && ((Integer.valueOf(System.now().format('mm')) - Integer.valueOf(at.CreatedDate.format('mm'))) == 1))) {           
                    
                    //Assign record type of Lead to the converted account
                    RecordType rt = recordTypeMap.get(Trigger.new[i].RecordTypeId);   
                    System.debug('record type name:'+rt.Name);  
                    //Account at = [Select Id, RecordTypeId from Account where Id =: Trigger.new[i].ConvertedAccountId];
                    
                    List<RecordType> rt_acct = [SELECT Id,Name FROM RecordType WHERE Name =:  rt.Name and SobjectType='Account'];   
                    System.debug('rt_acct:'+rt_acct);
                       
                    if(rt_acct.size() != 0)
                    {
                        at.RecordTypeId =  rt_acct[0].Id;
                        account_recordTypeName = rt_acct[0].Name;
                    }
                    else
                    {
                        at.RecordTypeId =  null;
                        account_recordTypeName = '';
                    }
                    
                    //set mailing country of converted Account same as Lead
                    at.Mailing_Country__c = Trigger.new[i].Country_Lookup__c;
                
                    //if(Trigger.isBefore)
                   // {
                        System.debug('assignNewlyCreatedAccountID.accountId::'+assignNewlyCreatedAccountID.accountId);
                        System.debug('Trigger.new[i].ConvertedAccountId::'+Trigger.new[i].ConvertedAccountId);
                        System.debug('Trigger.old[i].ConvertedAccountId::'+Trigger.old[i].ConvertedAccountId);
                        //List<Account> acct_exist = [Select Id, Name from Account where Id =: Trigger.new[i].ConvertedAccountId];
                        if(assignNewlyCreatedAccountID.accountId == Trigger.new[i].ConvertedAccountId)
                        {
                            if(Trigger.new[i].Country_Lookup__c != null)
                            {
                                List<Country__c> ctry = [Select Id, Agent_Relationship_Manager__c, Sales_Territory_Manager__c 
                                                         from Country__c 
                                                         where Id =: Trigger.new[i].Country_Lookup__c];                                
                                if(ctry.size() != 0)
                                {
                                    if(Trigger.new[i].Owner_is_Sales__c == true)
                                    {
                                        at.OwnerId = Trigger.new[i].OwnerId;                         
                                    }
                                    else if(at.Designated_Owner_Id__c == NULL || String.isBlank(at.Designated_Owner_Id__c ))
                                    {
                                        at.OwnerId = Trigger.new[i].OwnerId;
                                    }
                                    else if(at.Designated_Owner_Id__c != NULL || String.isNotBlank(at.Designated_Owner_Id__c ))
                                    {
                                        at.OwnerId = at.Designated_Owner_Id__c;
                                    }
                                }
                            }
                        }
                   // }
                    //set Account Owner of Converted Leads as per mailing country
                    
                    acct.add(at);    
                }
                
                //create Application record on lead conversion
                Application__c a = new Application__c();
                a.First_Name__c = Trigger.new[i].FirstName;
                a.Last_Name__c = Trigger.new[i].LastName;
                a.Account__c = Trigger.new[i].ConvertedAccountId;
                a.Application_Date_Account__c = Trigger.new[i].Application_Date_Account__c;
                a.Country__c = Trigger.new[i].Country__c;
                a.Country_Lookup__c = Trigger.new[i].Country_Lookup__c;
                a.Events_interested_in__c = Trigger.new[i].Events_interested_in__c;
                a.Application_Owner__c = at.OwnerId;
                a.Contact__c = Trigger.new[i].ConvertedContactId;
                a.Looking_for_Partners_in__c = Trigger.new[i].Looking_for_Partners_in__c;
                a.Application_Comments__c = Trigger.new[i].Application_Comments__c;
                a.Additional_Comments_by_Client__c = Trigger.new[i].Additional_Comments_by_Client__c;
                app.add(a);   
                
            }
            else if(Trigger.old[i].isConverted == false && Trigger.new[i].isConverted == true)
            {
                RecordType rt1 = recordTypeMap.get(Trigger.new[i].RecordTypeId);   
                
                Account at1 =  idNAccountMap.get(Trigger.new[i].ConvertedAccountId);
                
                System.debug('at1.CreatedDate.format(MM/dd/yyyy HH:mm) == System.now().format(MM/dd/yyyy HH:mm)): '+ at1.CreatedDate.format('MM/dd/yyyy HH:mm') == System.now().format('MM/dd/yyyy HH:mm'));
                System.debug('(60 - Integer.valueOf(at1.CreatedDate.format(ss)) < 5) : '+(60 - Integer.valueOf(at1.CreatedDate.format('ss')) < 5));
                System.debug('Integer.valueOf(at1.CreatedDate.format(mm))) == 1) : '+(Integer.valueOf(System.now().format('mm')) - Integer.valueOf(at1.CreatedDate.format('mm'))));
                
                if(((at1.CreatedDate.format('MM/dd/yyyy HH:mm') == System.now().format('MM/dd/yyyy HH:mm')) 
                   || ((60 - Integer.valueOf(at1.CreatedDate.format('ss')) < 5) 
                   && ((Integer.valueOf(System.now().format('mm')) - Integer.valueOf(at1.CreatedDate.format('mm'))) == 1))) 
                   || test.isrunningtest())
                {
                    List<RecordType> rt_acct1 = [SELECT Id,Name FROM RecordType WHERE Name =:  rt1.Name and SobjectType='Account'];   
                    if(rt_acct1.size() != 0)
                    {
                        at1.RecordTypeId =  rt_acct1[0].Id;
                        account_recordTypeName = rt_acct1[0].Name;
                    }
                    else
                    {
                        at1.RecordTypeId =  null;
                        account_recordTypeName = '';
                    }
                    
                    System.debug('account_recordTypeName : '+ account_recordTypeName);

                    //set mailing country of converted Account same as Lead
                    at1.Mailing_Country__c = Trigger.new[i].Country_Lookup__c;
                    
                    //set Account Owner of Converted Leads as per mailing country
                    if(Trigger.new[i].Country_Lookup__c != null)
                    {
                        List<Country__c> ctry = [Select Id, Agent_Relationship_Manager__c, Sales_Territory_Manager__c from Country__c where Id =: Trigger.new[i].Country_Lookup__c];
                        if(ctry.size() != 0)
                        {
                            if(Trigger.new[i].Owner_is_Sales__c == true)
                            {
                                at1.OwnerId = Trigger.new[i].OwnerId;                         
                            }
                            else if(String.isBlank(at1.Designated_Owner_Id__c ))
                            {
                                at1.OwnerId = Trigger.new[i].OwnerId;
                            }
                            else if(at1.Designated_Owner_Id__c != NULL && String.isNotBlank(at1.Designated_Owner_Id__c ))
                            {
                                at1.OwnerId = at1.Designated_Owner_Id__c;
                            }
                        }
                    }                   
                    acct.add(at1);
                }
            }
        }
        
        if(acct.size() != 0)
        {
            update acct;
        }
        System.debug('acct 2'+ acct);

        if(app.size() != 0)
        {
            insert app;
        }
        System.debug('app 2'+ app);
    }
}