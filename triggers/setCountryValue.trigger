trigger setCountryValue on Application__c (before insert, before update) 
{
    List<Application__c> led_list = new List<Application__c>();
    
    if (Trigger.isInsert || Trigger.isUpdate)
    {
        for (Application__c appl : Trigger.new)
        {
            if(appl.Account__c != null)
            {
                List<Account> acc = [Select Id, Mailing_Country__c from Account where Id =: appl.Account__c];
                if(acc.size() != 0)
                {
                    if(acc[0].Mailing_Country__c != null)
                    {
                        appl.Country_Lookup__c = acc[0].Mailing_Country__c;                        
                        Country__c ctry = [Select Name from Country__c where Id =: acc[0].Mailing_Country__c];
                        appl.Country__c = ctry.Name;
                    }      
                    else
                    {
                        appl.Country_Lookup__c = null;
                        appl.Country__c = '';
                    }                                 
                }
                else
                {
                    appl.Country_Lookup__c = null;
                    appl.Country__c = '';
                }
            }
        }                
    } 
}