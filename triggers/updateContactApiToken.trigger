trigger updateContactApiToken on Contact (before insert, before update)
{
    Contact[] cs = Trigger.new;
    for (Contact c:cs)
    {
        if (c.Api_Token__c==null)
        {
            Datetime saltDate = Datetime.now();
            String apiString = String.valueOfGmt(saltDate) + String.valueOf(c.id);
            Blob apiBlob = Crypto.generateDigest('SHA-256', Blob.valueOf(apiString));
        
            c.Api_Token__c = EncodingUtil.convertToHex(apiBlob);    
        }
    }

}