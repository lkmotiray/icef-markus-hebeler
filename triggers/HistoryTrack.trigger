Trigger HistoryTrack on CampaignMember (after insert, before update, before delete) {          
    List<CampaignHistory__c> ch= new List<CampaignHistory__c>(); 
    List<CampaignMember> cmOld= Trigger.old;  
    List<String> changes  = new List<String>(); 
    List<String> CampHisId  = new List<String>(); 
    List<String> ChangeText = new List<String>();
    List<String> FromValue = new List<String>();
    List<String> ToValue = new List<String>();
    List<String> MemberEmail = new List<String>();
    integer i=0;     
    if(Trigger.isDelete){
        for(CampaignMember cm: Trigger.old ){     
            CampHisId.add(cm.campaignId);
            ChangeText.add('Campaign Member removed');
            FromValue.add(cm.Id);
            ToValue.add(null);
            MemberEmail.add(cm.Email);
            CampaignHistory__c c= new CampaignHistory__c();               
            c.Name='History '+DateTime.now();  
            System.debug('CName:'+c.Name);
            c.CampaignId__c=CampHisId[i];
            System.debug('CampaignId:'+c.CampaignId__c);                                    
            c.Change__c=ChangeText[i];
            System.debug('ChangeText: '+c.Change__c);         
            c.From_Value__c=FromValue[i];
            System.debug('FromValue: '+c.From_Value__c);              
            c.To_Value__c=ToValue[i];
            System.debug('ToValue: '+c.To_Value__c);              
            c.Members_Email__c=MemberEmail[i];
            System.debug('MemberEmail: '+c.Members_Email__c);                      
            c.Members_Id__c = cm.Id;
            c.Contact_Id__c = cm.ContactId;
            c.Lead_Id__c = cm.LeadId;
            ch.add(c);                              
            i++;           
        } 
    }else {      
        for(CampaignMember cm: Trigger.new ){  
            if((Trigger.isUpdate)){
                if(cmOld[i].status!=cm.status){    
                    CampHisId.add(cm.campaignId);   
                    ChangeText.add('Campaign Member Status changed');
                    FromValue.add(cmOld[i].Status);
                    ToValue.add(cm.status);
                    MemberEmail.add(cm.Email);                    
                    CampaignHistory__c c= new CampaignHistory__c();                          
                    c.Name='History '+DateTime.now();
                    System.debug('cm: ' + cm.Account_Owner__c);
                    System.debug('CName:'+c.Name);
                    c.CampaignId__c=CampHisId[i];                        
                    System.debug('CampaignId:'+c.CampaignId__c);                                      
                    c.Change__c=ChangeText[i];
                    System.debug('ChangeText: '+c.Change__c);         
                    c.From_Value__c=FromValue[i];
                    System.debug('FromValue: '+c.From_Value__c);              
                    c.To_Value__c=ToValue[i];
                    System.debug('ToValue: '+c.To_Value__c);              
                    c.Members_Email__c=MemberEmail[i];
                    System.debug('MemberEmail: '+c.Members_Email__c);  
                    c.Members_Id__c = cm.Id;
                    c.Contact_Id__c = cm.ContactId;
                    c.Lead_Id__c = cm.LeadId;                    
                    ch.add(c); 
                }     
            }else if(Trigger.isInsert){                                
                CampHisId.add(cm.campaignId);
                ChangeText.add('Campaign Member added');
                FromValue.add(null);
                ToValue.add(cm.Id);
                MemberEmail.add(cm.Email);                                                                                    
                CampaignHistory__c c= new CampaignHistory__c();
                c.Name='History '+DateTime.now();                   
                System.debug('CName:'+c.Name);
                c.CampaignId__c=CampHisId[i];
                System.debug('CampaignId:'+c.CampaignId__c);                                          
                c.Change__c=ChangeText[i];
                System.debug('ChangeText: '+c.Change__c);         
                c.From_Value__c=FromValue[i];
                System.debug('FromValue: '+c.From_Value__c);              
                c.To_Value__c=ToValue[i];
                System.debug('ToValue: '+c.To_Value__c);              
                c.Members_Email__c=MemberEmail[i];
                System.debug('MemberEmail: '+c.Members_Email__c);                 
                c.Members_Id__c = cm.Id;
                c.Contact_Id__c = cm.ContactId;
                c.Lead_Id__c = cm.LeadId;                
                ch.add(c);
            } 
            i++;     
        }         
    }    
    insert ch;
}