/********************************************************************
This trigger is fired after update of Account.
Purpose:
This trigger is used to update the value of the Contact Owner to the value of the Account Owner each time the Account Owner is changed
Created: Rupali, 26th Nov 2012
********************************************************************/
trigger updateContactOwner on Account (before insert)
{
    //The map allows us to keep track of the accounts
   //that actually have new Owner

   /* Map<Id, Account> accountsWithNewOwner = new Map<Id, Account>();
    
    List<Contact> updatedContacts = new List<Contact>();
    
    if (Trigger.new.size() != 0)
    {
        for(integer i = 0; i < Trigger.new.size(); i++)
        {
            if(Trigger.new[i].OwnerId != null)
            {
                if(Trigger.old[i].OwnerId != Trigger.new[i].OwnerId)
                {
                    accountsWithNewOwner.put(Trigger.old[i].id,Trigger.new[i]);
                }
            }
         }
      }
      
      for (Contact c : [select Id, OwnerId, AccountId from contact where accountId in :accountsWithNewOwner.keySet()])
      {
          Account parentAccount = accountsWithNewOwner.get(c.accountId);
          c.OwnerId =  parentAccount.OwnerId;  
          updatedContacts.add(c);
      }
      update updatedContacts;         
      */
      if(true){}
}