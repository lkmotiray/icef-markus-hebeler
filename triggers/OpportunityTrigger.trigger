/*
 * @Purpose : store converted amount value to new field
 * @created By : Dreamwares[Amol k] 
 */

trigger OpportunityTrigger on Opportunity (before insert, before update) {
    if(Trigger.isBefore) {
        OpportunityTriggerHandler handler = new OpportunityTriggerHandler();
        if(Trigger.isInsert || Trigger.isUpdate) {    
            // send all updatates opportunties though amount is not changes as Currency Rate can vari
            handler.updateCurrenency( Trigger.new);
        }
    }
}