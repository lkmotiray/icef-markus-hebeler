/****************************************************
Case Owner change to Account Owner
The account owner should become the case owner right from the creation of the case.
*****************************************************/

trigger updateCaseOwner on Case (before insert, after insert, after update) 
{       
   /* Fetch Account Ids in SET    
   Set<Id> accountID = new Set<Id>();
   if (Trigger.new.size() != 0) 
    {
        for(integer i = 0; i < Trigger.new.size(); i++)
        {
            if(Trigger.new[i].AccountId != null)
            {
                accountID.add(Trigger.new[i].AccountId);
            }
        }
    }*/

    /* Map Account Id's with respective OwnerId's 
    Map<Id, Id> AccountOwnerMap = new Map<Id, Id>();
    List<Account> acc = [Select Id, OwnerId from Account where Id IN: accountID];
    for(Account a: acc)
    {
        AccountOwnerMap.put(a.id, a.OwnerId);
    }    
   
    if (Trigger.new.size() != 0) 
    {
        for(integer i = 0; i < Trigger.new.size(); i++)
        {
            if(Trigger.new[i].AccountId != null)
            {
                Trigger.new[i].OwnerId = AccountOwnerMap.get(Trigger.new[i].AccountId);
            }
        }
    }*/
    try {
        if(Trigger.isAfter && Trigger.isUpdate && CaseOwnerHelper.isExecuted == false) {        
            CaseOwnerHelper.isExecuted = true;
            CaseOwnerHelper.ChangeCaseOwner(Trigger.new,Trigger.oldMap);                        
        }
    }
    catch(Exception e) {
        System.debug('Exception Caught:::'+e.getMessage());
    }
}