/*
 *  @purpose - in Account: tick X20_year_mag__c if any related contact's X20_year_mag__c is ticked
 *           - in Account: Count number of account related contacts whose role is Primary Decision Maker
 *           - in Account: tick Has_Contact_with_emailaddress__c in account if any of related contacts has email address
 *  @createdby Mamta Kukreja, Amol Kulthe
 *  @createddate 15/6/15
 *  @modified by: Henriette Schönleber
 *  @modified date: 28.11.2018
 */
trigger AccountRelatedContactsTrigger on Contact (after insert, after update, after delete, after undelete) {

    ContactHandler handler = new ContactHandler();
    
    if(Trigger.isAfter){       
        //Check if Trigger is called after insert or undelete operation
        if(Trigger.isInsert || Trigger.isUndelete){
            handler.checkAccountFields(Trigger.new);            
        }
        //Check if Trigger is called after update operation
        //(modifications Vivek.p)->  if(Trigger.isUpdate && AccountRelatedContactsTriggerHandler.IsRunFirstTime){ 
        if(Trigger.isUpdate ){        
            handler.checkUpdate(Trigger.new);
        }
        //Check if Trigger is called after delete operation
        if(Trigger.isDelete){
            handler.checkAccountFields(Trigger.old); 
        }    
    }
}