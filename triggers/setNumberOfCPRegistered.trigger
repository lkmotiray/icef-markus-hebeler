trigger setNumberOfCPRegistered on Contact_Participation__c (after delete, after insert, after update) 
{
    Decimal numberOfAppointments {get; set;}
    Decimal espstatsNumberOfAppointment;
    numberOfAppointments = 0;
    Set<id> APIds = new Set<id>();
    Set<Account_Participation__c> APsToUpdate = new Set<Account_Participation__c>();
    List<Account_Participation__c> APsToUpdate_list = new List<Account_Participation__c>();
 
    if (Trigger.isUpdate)
    {
        Map<Id, Contact_Participation__c> contactPartMapOld = trigger.oldmap;
        for (Contact_Participation__c item : Trigger.new){
            if(item.Participation_Status__c != contactPartMapOld.get(item.Id).Participation_Status__c 
               || item.espstats_number_of_appointment__c != contactPartMapOld.get(item.Id).espstats_number_of_appointment__c){
                   
                   APIds.add(item.Account_Participation__c);
               }            
        }        
    }
    if(Trigger.isDelete)
    {
        System.debug('Trigger.old'+Trigger.old);
        for (Contact_Participation__c item : Trigger.old)
            APIds.add(item.Account_Participation__c);
    }
    if(Trigger.isInsert)
    {
        for (Contact_Participation__c item : Trigger.new)
            APIds.add(item.Account_Participation__c);
    }
    
    // get a map of the Account Participation with the number of items
    Map<id,Account_Participation__c> APMap = new Map<id,Account_Participation__c>([select id, Number_of_CPs_registered__c, Number_of_Appointments__c 
                                                                                   from Account_Participation__c 
                                                                                   where id IN :APIds]);
 
    Map<Id, Contact_Participation__c> cpMap = new Map<Id, Contact_Participation__c>();
    
    Map<Id, List<Contact_Participation__c>> CPWithAP = new Map<Id, List<Contact_Participation__c>>();
    
    cpMap.putAll([select Id, Account_Participation__c, Participation_Status__c, espstats_number_of_appointment__c 
                  from Contact_Participation__c 
                  where Account_Participation__c IN: APIds and Participation_Status__c = 'registered']);
      
    for(ID accountPartId :APMap.keySet()){
        if(!CPWithAP.containsKey(accountPartId)){
            CPWithAP.put(accountPartId, new List<Contact_Participation__c>());
        }
    }    
    for(Contact_Participation__c  contactPart: cpMap.values())
    {
        Id accountPartId = contactPart.Account_Participation__c;
        if(!CPWithAP.containsKey(accountPartId)){
            CPWithAP.put(accountPartId, new List<Contact_Participation__c>());
        }
        CPWithAP.get(accountPartId).add(contactPart);                                 
    }    
     
    
    for(ID apId: CPWithAP.KeySet())
    {
        numberOfAppointments = 0;        
        List<Contact_Participation__c> cps = new List<Contact_Participation__c>();
        System.debug('apId::'+apId);
        if(apId != null)
        {
            cps = CPWithAP.get(apId);
            APMap.get(apId).Number_of_CPs_registered__c = cps.size();
            
            for(Contact_Participation__c contPart : cps)
            {
               espstatsNumberOfAppointment = contPart.espstats_number_of_appointment__c;
               if(espstatsNumberOfAppointment != null)
                   numberOfAppointments += contPart.espstats_number_of_appointment__c; 
            }
            APMap.get(apId).Number_of_Appointments__c = numberOfAppointments;
            // add the value in the map to a list so we can update it
            APsToUpdate.add(APMap.get(apId));
        }
    }
    APsToUpdate_list.clear();
    APsToUpdate_list.addAll(APsToUpdate);
    if(APsToUpdate_list.size() > 0)
    {
        update APsToUpdate_list;
    }
}