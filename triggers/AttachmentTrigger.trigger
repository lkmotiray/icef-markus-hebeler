/*  
    @Purpose      : Trigger on attachment object to populate latest attachment Id on the parent Advert record 
    @Author       : Poonam 
    @Created Date : 07/08/2017 
*/

trigger AttachmentTrigger on Attachment (after insert, after delete) {
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            AttachmentTriggerHandler.populateLatestAttachmentId(Trigger.new);
        }
        
        if(Trigger.isDelete){
            AttachmentTriggerHandler.populateLatestAttachmentId(Trigger.old);
        }
    }
}