/*---------------------------------------------------------------------------------------------------------
    @ Developer          : Dreamwares IT Solutions [Sagar].
    @ Purpose            : Trigger to Update 'Persons attending' on Hotel Room
    @ Created Date       : 20 May 16
    @ Last Modified By   : 
    @ Last Modified Date : 
-----------------------------------------------------------------------------------------------------------*/
trigger AccommodationTrigger on Accommodation__c (after insert, after update , after delete) {    
    if(Trigger.isAfter){
        if(Trigger.isUpdate){ 
            AccommodationHandler.afterUpdate(Trigger.newmap,Trigger.oldmap);        
        }
        if(Trigger.isInsert){
            AccommodationHandler.afertInsert(trigger.new);        
        }
        
        if(Trigger.isdelete){
            //System.debug('delete Trigger on Accommodation');
            AccommodationHandler.afterDelete(trigger.Old);        
        }
            
    }
}