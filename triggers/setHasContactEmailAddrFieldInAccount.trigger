/***********************************************
Purpose of trigger:
In Account we have a field called ‚Has Contact with emailaddress‘ (a checkbox). This box should be ticked if the account has 
any contact that has an entry in field ‘Contact Email’.
Whenever a contact is changed the appropriate account must be checked whether any of its contacts has an entry in field 
‘Contact Email’. If yes: Box must be ticked, if no: the tick must be removed from that field.

************************************************/
trigger setHasContactEmailAddrFieldInAccount on Contact (after delete, after insert, after update) 
{
    Set<id> AccountIds = new Set<id>();
    Set<Account> AccountsToUpdate = new Set<Account>();
    //List<Account> AccountsToUpdate_list = new List<Account>();
    Map<Id, Boolean> mapAccountIdToHasContactEmail = new Map<Id, Boolean>();
 
    if (Trigger.isUpdate || Trigger.isDelete) 
    {
        for (Contact item : Trigger.old)
            AccountIds.add(item.AccountId);
    }
    else
    {
        for (Contact item : Trigger.new)
            AccountIds.add(item.AccountId);
    }
    AccountIds.remove(null);
    // get a map of the Account Participation with the number of items
    Map<id,Account> AccountMap = new Map<id,Account>([select id, Has_Contact_with_emailaddress__c from Account where id IN :AccountIds]);
 
    Map<Id, Contact> ContactMap = new Map<Id, Contact>();
    
    Map<Id, List<Contact>> ContactWithAccount = new Map<Id, List<Contact>>();
    
    ContactMap.putAll([select Id, Contact_Email__c, AccountId from Contact where AccountId IN: AccountIds]);
    System.debug('ContactMap::'+ContactMap);    
    for(Id contactId: ContactMap.keySet())
     {
         Id key = ContactMap.get(contactId).AccountId;
         List<Contact> groupedContacts = ContactWithAccount.get(key);
          if (null==groupedContacts)
          {
           groupedContacts = new List<Contact>();
           ContactWithAccount.put(key, groupedContacts);
          }
       
         groupedContacts.add(ContactMap.get(contactId));                          
     }    
     
    
    for(ID accountId: ContactWithAccount.KeySet())
    {
        List<Contact> contacts = new List<Contact>();
        System.debug('contacts::'+contacts);
        if(accountId != null)
        {
            contacts = ContactWithAccount.get(accountId);
            
            for(Contact cont : contacts)
            {
                if(cont.Contact_Email__c != null)               
                {
                    //AccountMap.get(accountId).Has_Contact_with_emailaddress__c = true;
                    mapAccountIdToHasContactEmail.put(accountId, true);
                    break;
                }
                else
                {
                    //AccountMap.get(accountId).Has_Contact_with_emailaddress__c = false;
                    mapAccountIdToHasContactEmail.put(accountId, false);
                }
             }
            // add the value in the map to a list so we can update it
            //AccountsToUpdate.add(AccountMap.get(accountId));
        }
    }
    /*AccountsToUpdate_list.clear();
    AccountsToUpdate_list.addAll(AccountsToUpdate);
    if(AccountsToUpdate_list.size() != null)
    {
        update AccountsToUpdate_list;
    }*/
    if(!mapAccountIdToHasContactEmail.isEmpty()) {
//        ContactTriggerHandler.updateAccountContactHasEmailField(mapAccountIdToHasContactEmail);    
    }
}