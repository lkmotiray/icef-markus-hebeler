/**
@ Created By  : Yogesh Mahajan.
@ Date        : 11-06-2014.   
@ Description : Add three multipicklist value into one multipicklist
                Example : Undergraduate_Deg_Bachelor_Blended__c
                          Undergraduate_Deg_Bachelor_Online__c
                          Undergraduate_Degree_Bachelor__c 
                Above all the field value store into Undergraduate_Deg_Bachelor_ALL__c field.
*/

trigger AssignSummaryField on Course_Profile__c ( before insert, before update) {
    Map<Id, Course_Profile__c> mapNewCourseProfile = new Map<Id, Course_Profile__c>();
    if(Trigger.New != null)
        {
        for (Course_Profile__c courseProfileRecord  : Trigger.new) {
            mapNewCourseProfile.put(courseProfileRecord.Id, courseProfileRecord);
        }
        
        AssignSummaryFieldHandler summary = new AssignSummaryFieldHandler();
        if (Trigger.isInsert || Trigger.isUpdate ) {
            summary.updateSummaryField(mapNewCourseProfile);  
        }
    }
}