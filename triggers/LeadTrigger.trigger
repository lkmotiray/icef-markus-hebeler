/**
 * This trigger on is written to create Recommendation records for the 
 * Leads which are updated to be Converted Leads. 
 *
 * @author          Jasmine J 
 * @created date    31/08/2015
 */
trigger LeadTrigger on Lead (after update) {
    
    if(!LeadTriggerHandler.hasExecuted) {   // Execute only if the trigger has not already 
                                            // been executed in the current execution instance
        
        // to store the converted Leads and create Recommendations for
        List<Lead> listConvertedLeads = new List<Lead>();   
        
        // list of converted Leads to update Account Owner of the Converted Accounts
        List<Lead> listLeadsForAccOwnerUpdate = new List<Lead>();   
        
        // check if the Lead that is updated is updated as Converted
        for(Lead updatedLead: Trigger.New){
            if(updatedLead.IsConverted && 
               (updatedLead.IsConverted != Trigger.oldMap.get(updatedLead.Id).IsConverted)) {
                   if(updatedLead.LeadSource == 'Refer a partner functionality')
                        listConvertedLeads.add(updatedLead);
                   listLeadsForAccOwnerUpdate.add(updatedLead);
            }        
        }   
        System.debug('listConvertedLeads ::' +listConvertedLeads);    
        System.debug('listLeadsForAccOwnerUpdate ::' +listLeadsForAccOwnerUpdate);   
        
        // pass the list of converted Leads on which Recommendations are to be created
        // only if the list contains records of leads converted else stop execution
        if(!listConvertedLeads.isEmpty()) {             
            LeadTriggerHandler.createRecommendation(listConvertedLeads);
            LeadTriggerHandler.updateAccounts(listConvertedLeads, listLeadsForAccOwnerUpdate);
        }
    }
    System.debug('LeadTriggerHandler.hasExecuted ::'+LeadTriggerHandler.hasExecuted);
}