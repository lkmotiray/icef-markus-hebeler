<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Junction Object between Account/Account to create relationship between an Association and its members.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Association_Type__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT ( Association__r.Association_Type__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Association Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Association__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Association</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Assoc_memberships1</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>External_ID__c</fullName>
        <description>Membership ID as used by the Association.</description>
        <externalId>false</externalId>
        <inlineHelpText>Membership ID as used by the Association.</inlineHelpText>
        <label>External ID</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Last_updated__c</fullName>
        <defaultValue>TODAY()</defaultValue>
        <externalId>false</externalId>
        <label>Last updated</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Member_Account_Owner__c</fullName>
        <externalId>false</externalId>
        <formula>Member__r.Account_Owner_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Member Account Owner</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Member_Account_Status__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT ( Member__r.Status__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Member Account Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Member_City_Post_Code_State__c</fullName>
        <externalId>false</externalId>
        <formula>Member__r.Mailing_City_Post_Code_State__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Member City Post Code State</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Member_Country__c</fullName>
        <externalId>false</externalId>
        <formula>Member__r.Mailing_Country__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Member Country</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Member_Record_Type__c</fullName>
        <externalId>false</externalId>
        <formula>CASE (Member__r.RecordTypeId,
&quot;012200000005D7g&quot;, &quot;Agent&quot;,
&quot;012200000005EEc&quot;, &quot;Association&quot;,
&quot;012200000005D7R&quot;, &quot;Educator&quot;,
&quot;012200000005DcZ&quot;, &quot;Hotel&quot;,
&quot;012200000005Eis&quot;, &quot;Service Provider&quot;,
&quot;012200000001YtP&quot;, &quot;Supplier&quot;,
&quot;012200000001Um2&quot;, &quot;Work and Travel&quot;,
&quot;Other&quot;
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Member Record Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Member__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Member</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Memberships (Member)</relationshipLabel>
        <relationshipName>Assoc_memberships</relationshipName>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Membership_comments__c</fullName>
        <externalId>false</externalId>
        <label>Membership comments</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Name_short__c</fullName>
        <externalId>false</externalId>
        <formula>Association__r.Name_short__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Name (short)</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Nudge__c</fullName>
        <description>This field is to foces a data record change.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is to foces a data record change.</inlineHelpText>
        <label>Nudge</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Source__c</fullName>
        <externalId>false</externalId>
        <label>Source</label>
        <length>200</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Special_Membership__c</fullName>
        <externalId>false</externalId>
        <label>Special Membership</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>AIRC Pathway</fullName>
                    <default>false</default>
                    <label>AIRC Pathway</label>
                </value>
                <value>
                    <fullName>AACSB Accredited</fullName>
                    <default>false</default>
                    <label>AACSB Accredited</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active</fullName>
                    <default>true</default>
                    <label>Active</label>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                    <label>Inactive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Status_details__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please specify, why that membership is inactivated.</inlineHelpText>
        <label>Status details</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Status__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Association inactive</fullName>
                    <default>false</default>
                    <label>Association inactive</label>
                </value>
                <value>
                    <fullName>Skipped membership</fullName>
                    <default>false</default>
                    <label>Skipped membership</label>
                </value>
                <value>
                    <fullName>Wrong entry</fullName>
                    <default>false</default>
                    <label>Wrong entry</label>
                </value>
                <value>
                    <fullName>Member ceased business</fullName>
                    <default>false</default>
                    <label>Member ceased business</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Inactive</controllingFieldValue>
                <valueName>Association inactive</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Inactive</controllingFieldValue>
                <valueName>Skipped membership</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Inactive</controllingFieldValue>
                <valueName>Wrong entry</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <label>Membership</label>
    <listViews>
        <fullName>Test_Queue_Membership</fullName>
        <filterScope>Queue</filterScope>
        <label>Test Queue</label>
        <queue>Test_Queue</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <nameField>
        <displayFormat>M{0000000000}</displayFormat>
        <label>Membership Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Memberships</pluralLabel>
    <searchLayouts>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <lookupDialogsAdditionalFields>Association__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Member__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>LAST_UPDATE</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Association__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Member__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>LAST_UPDATE</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>Association__c</searchFilterFields>
        <searchFilterFields>Member__c</searchFilterFields>
        <searchFilterFields>LAST_UPDATE</searchFilterFields>
        <searchFilterFields>CURRENCY_ISO_CODE</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
