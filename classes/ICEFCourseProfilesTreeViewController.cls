/**
* @ Description : Controller Class to generate Course Profile Details for ICEF Event record
* @ Created Date : 10 November 2017
*/
public with sharing class ICEFCourseProfilesTreeViewController {
    
    static Map<Id, Course_Profile__c> courseProfilesMap;
    
    static Integer languageCourses_total;
    static Integer secHighSchoolPrograms_total;
    static Integer studyPrograms_total;
    static Integer addProgramsAndServices_total;
    
    static Integer languageCourseInArabic_total;
    static Integer languageCourseInChinese_total;
    static Integer languageCourseInEnglish_total;
    static Integer languageCourseInFrench_total;
    static Integer languageCourseInGerman_total;
    static Integer languageCourseInItalian_total;
    static Integer languageCourseInJapanese_total;
    static Integer languageCourseInKorean_total;
    static Integer languageCourseInPortuguese_total;
    static Integer languageCourseInRussian_total;
    static Integer languageCourseInSpanish_total;
    static Integer furtherLanguagesIn_total;
    static Integer typeOfLanguageCourse_total;
    static Integer otherTypeOfLanguageCourse_total;
    static Integer careerVocationalCertificateDiploma_total;
    static Integer otherCareerVocationalCertificateDiploma_total;
    static Integer undergraduateDegreeBachelor_total;
    static Integer otherUndergraduateDegreeBachelor_total;
    static Integer graduatePostGraduateCertificateDiploma_total;
    static Integer otherGraduatePostGraduateCertificateDiploma_total;
    static Integer graduatePostGraduateMasters_total;
    static Integer otherGraduatePostGraduateMasters_total;
    static Integer graduatePostGraduateDoctorate_total;
    static Integer otherGraduatePostGraduateDoctorate_total;
    
    static Map<String, Integer> langCoursesMap;
    static Map<String, Integer> arabicCountryCountMap;
    static Map<String, Integer> chineseCountryCountMap;
    static Map<String, Integer> englishCountryCountMap;
    static Map<String, Integer> frenchCountryCountMap;
    static Map<String, Integer> germanCountryCountMap;
    static Map<String, Integer> italianCountryCountMap;
    static Map<String, Integer> japaneseCountryCountMap;
    static Map<String, Integer> koreanCountryCountMap;
    static Map<String, Integer> portugueseCountryCountMap;
    static Map<String, Integer> russianCountryCountMap;
    static Map<String, Integer> spanishCountryCountMap;
    static Map<String, Integer> typeOfCourseCountMap ;
    static Map<String, Integer> furtherLanguageCountMap;
    static Map<String, Integer> otherTypeOfLanguageCountMap;
    static Map<String, Integer> careerVocationalCertificateDiplomaCountMap;
    static Map<String, Integer> otherCareerVocationalCertificateDiplomaCountMap;
    static Map<String, Integer> undergraduateDegreeBachelorCountMap;
    static Map<String, Integer> otherUndergraduateDegreeBachelorCountMap;
    static Map<String, Integer> graduatePostgraduateCertificateDiplomaCountMap;
    static Map<String, Integer> otherGraduatePostgradCertificateDiplomaCountMap;
    static Map<String, Integer> graduatePostgraduateMastersCountMap;
    static Map<String, Integer> otherGraduatePostgraduateMastersCountMap;
    static Map<String, Integer> graduatePostgraduateDoctorateCountMap;
    static Map<String, Integer> otherGraduatePostgraduateDoctorateCountMap;
    static Map<String, Integer> secondaryHighSchoolProgMap;
    static Map<String, Integer> additionalProgMap;
    
    //constructors
    public ICEFCourseProfilesTreeViewController(ApexPages.StandardController controller){
        
    }
    
    /*
    * @ Description : Method to initialize the Map and Integer variables
    */
    private static void initializeMapAndVariables(){        
        languageCourses_total = 0;
        secHighSchoolPrograms_total = 0;
        studyPrograms_total = 0;
        addProgramsAndServices_total = 0;
        languageCourseInArabic_total = 0;
        languageCourseInChinese_total = 0;
        languageCourseInEnglish_total = 0;
        languageCourseInFrench_total = 0;
        languageCourseInGerman_total = 0;
        languageCourseInItalian_total = 0;
        languageCourseInJapanese_total = 0;
        languageCourseInKorean_total = 0;
        languageCourseInPortuguese_total = 0;
        languageCourseInRussian_total = 0;
        languageCourseInSpanish_total = 0;
        furtherLanguagesIn_total = 0;
        typeOfLanguageCourse_total = 0;
        otherTypeOfLanguageCourse_total = 0;
        careerVocationalCertificateDiploma_total = 0;
        otherCareerVocationalCertificateDiploma_total = 0;
        undergraduateDegreeBachelor_total = 0;
        otherUndergraduateDegreeBachelor_total = 0;
        graduatePostGraduateCertificateDiploma_total = 0;
        otherGraduatePostGraduateCertificateDiploma_total = 0;
        graduatePostGraduateMasters_total = 0;
        otherGraduatePostGraduateMasters_total = 0;
        graduatePostGraduateDoctorate_total = 0;
        otherGraduatePostGraduateDoctorate_total = 0;   

        arabicCountryCountMap = new Map<String, Integer>();
        chineseCountryCountMap = new Map<String, Integer>();
        englishCountryCountMap = new Map<String, Integer>();
        frenchCountryCountMap = new Map<String, Integer>();
        germanCountryCountMap = new Map<String, Integer>();
        italianCountryCountMap = new Map<String, Integer>();
        japaneseCountryCountMap = new Map<String, Integer>();
        koreanCountryCountMap = new Map<String, Integer>();
        portugueseCountryCountMap = new Map<String, Integer>();
        russianCountryCountMap = new Map<String, Integer>();
        spanishCountryCountMap = new Map<String, Integer>();
        typeOfCourseCountMap = new Map<String, Integer>();
        furtherLanguageCountMap = new Map<String, Integer>();
        otherTypeOfLanguageCountMap = new Map<String, Integer>();
        careerVocationalCertificateDiplomaCountMap = new Map<String, Integer>();
        otherCareerVocationalCertificateDiplomaCountMap = new Map<String, Integer>();
        undergraduateDegreeBachelorCountMap = new Map<String, Integer>();
        otherUndergraduateDegreeBachelorCountMap = new Map<String, Integer>();
        graduatePostgraduateCertificateDiplomaCountMap = new Map<String, Integer>();
        otherGraduatePostgradCertificateDiplomaCountMap = new Map<String, Integer>();
        graduatePostgraduateMastersCountMap = new Map<String, Integer>();
        otherGraduatePostgraduateMastersCountMap = new Map<String, Integer>();
        graduatePostgraduateDoctorateCountMap = new Map<String, Integer>();
        otherGraduatePostgraduateDoctorateCountMap = new Map<String, Integer>();
        secondaryHighSchoolProgMap = new Map<String, Integer>();
        additionalProgMap = new Map<String, Integer>();         
    }
    
    /*
    * @ Description : Remote method to return Course Profile final string
    * @ Parameter : Id of ICEF Event
    * @ Return : resultant JSON String
    */
    @RemoteAction
    public static String showCourseProfileData( String ICEFEventId){
        
        String wrapperJSON;
        
        //initialize the variables and Maps
        initializeMapAndVariables();
        
        // fetch the Id for Account Participation Agent recordtype
        Id agentRecordTypeId = Schema.SObjectType.Account_Participation__c.getRecordTypeInfosByName().get('Agent').getRecordTypeId();               
                
        courseProfilesMap  = new Map<Id, Course_Profile__c>();        
        
        // fetch account Participation Map where status is accepted and recordtype is Agent
        Map<Id, Account_Participation__c> accountParticipationMap = 
        new Map<Id, Account_Participation__c>([ Select Id, Name 
                                                FROM Account_Participation__c 
                                                WHERE ICEF_Event__c =: ICEFEventId 
                                                    AND Participation_Status__c =: 'accepted' 
                                                    AND RecordTypeId =: agentRecordTypeId ]);       
        if( !accountParticipationMap.isEmpty() ){
            try{
                courseProfilesMap.putAll([ SELECT Id, Name, Language_Arabic_in__c, 
                                                  Language_Chinese_in__c, Language_English_in__c, 
                                                  Language_French_in__c, Language_German_in__c, 
                                                  Language_Italian_in__c, Language_Japanese_in__c, 
                                                  Language_Korean_in__c, Language_Portuguese_in__c, 
                                                  Language_Russian_in__c, Language_Spanish_in__c, 
                                                  Language_Other_in__c, Type_of_Language_Course__c, 
                                                  Other_types_of_Language_Course__c, 
                                                  Secondary_and_high_school_programmes__c, 
                                                  Other_secondary_and_high_school_programm__c, 
                                                  Career_Vocational_Certificate_Diploma__c, 
                                                  Other_Career_VocationalCertificate_Dipl__c, 
                                                  Undergraduate_Degree_Bachelor__c, 
                                                  Other_Undergraduate_Degree_Bachelor__c, 
                                                  Graduate_PostgraduateCertificate_Diploma__c, 
                                                  Other_Graduate_PostgradCertificate_Dipl__c, 
                                                  Graduate_Postgraduate_Masters__c, 
                                                  Other_Graduate_Postgraduate_Masters__c, 
                                                  Graduate_Postgraduate_Doctorate__c, 
                                                  Other_Graduate_Postgraduate_Doctorate__c, 
                                                  Additional_programmes_and_services__c, 
                                                  Other_additional_programmes_and_services__c  
                                           FROM Course_Profile__c 
                                           WHERE Account_Participation__c in :accountParticipationMap.keySet() ]);
            }catch(Exception e){
                System.debug('Exception occured while fetching Course Profile map.' + e.getStackTraceString());
            }
            
            if( !courseProfilesMap.isEmpty() ){
                wrapperJSON = itrateMapToGenerateJSON();
            }
        }        
        return wrapperJSON;
    }
    
    /*
    * @ Description : method to iterate over records and generate JSON string representation of course Profile
    * @ Return : resultant JSON String
    */
    public static String itrateMapToGenerateJSON(){
        
        String finalWrapperString;
        
        if( !courseProfilesMap.isEmpty() ){
            
            for( Course_Profile__c courseProfile : courseProfilesMap.values() ){
                
                if( courseProfile.Language_Arabic_in__c != null || courseProfile.Language_Chinese_in__c!= null || 
                    courseProfile.Language_English_in__c!= null || courseProfile.Language_French_in__c!= null || 
                    courseProfile.Language_Italian_in__c!= null || courseProfile.Language_Japanese_in__c!= null || 
                    courseProfile.Language_Korean_in__c!= null || courseProfile.Language_Portuguese_in__c!= null || 
                    courseProfile.Language_Russian_in__c!= null || courseProfile.Language_Spanish_in__c!= null || 
                    courseProfile.Language_Other_in__c!= null || courseProfile.Type_of_Language_Course__c!= null || 
                    courseProfile.Other_types_of_Language_Course__c != null ){
                        
                    languageCourses_total++;
                }
                
                if( courseProfile.Secondary_and_high_school_programmes__c != null || 
                    courseProfile.Other_secondary_and_high_school_programm__c != null ){
                        
                    secHighSchoolPrograms_total++;
                }
                
                if( courseProfile.Career_Vocational_Certificate_Diploma__c != null || 
                    courseProfile.Other_Career_VocationalCertificate_Dipl__c != null ||  
                    courseProfile.Undergraduate_Degree_Bachelor__c!= null ||  
                    courseProfile.Other_Undergraduate_Degree_Bachelor__c!= null ||  
                    courseProfile.Graduate_PostgraduateCertificate_Diploma__c!= null ||  
                    courseProfile.Other_Graduate_PostgradCertificate_Dipl__c!= null ||  
                    courseProfile.Graduate_Postgraduate_Masters__c!= null ||  
                    courseProfile.Other_Graduate_Postgraduate_Masters__c!= null ||  
                    courseProfile.Graduate_Postgraduate_Doctorate__c!= null ||  
                    courseProfile.Other_Graduate_Postgraduate_Doctorate__c!= null ){
                        
                    studyPrograms_total++;
                }
                
                if( courseProfile.Additional_programmes_and_services__c!= null || 
                    courseProfile.Other_additional_programmes_and_services__c != null ){
                        
                    addProgramsAndServices_total++;
                }
                
                if( courseProfile.Language_Arabic_in__c != null ){
                    languageCourseInArabic_total++;
                    getCountAndSetCountInMap(courseProfile.Language_Arabic_in__c, arabicCountryCountMap);                      
                }
                
                if( courseProfile.Language_Chinese_in__c != null ){
                    languageCourseInChinese_total++;
                    getCountAndSetCountInMap(courseProfile.Language_Chinese_in__c, chineseCountryCountMap);                    
                }
                
                if( courseProfile.Language_English_in__c != null ){
                    languageCourseInEnglish_total++;
                    getCountAndSetCountInMap(courseProfile.Language_English_in__c, englishCountryCountMap);                    
                }
                
                if( courseProfile.Language_French_in__c != null ){
                    languageCourseInFrench_total++;
                    getCountAndSetCountInMap(courseProfile.Language_French_in__c, frenchCountryCountMap);                     
                }
                
                if( courseProfile.Language_German_in__c != null ){
                    languageCourseInGerman_total++;
                    getCountAndSetCountInMap(courseProfile.Language_German_in__c, germanCountryCountMap);                    
                }
                
                if( courseProfile.Language_Italian_in__c != null ){
                    languageCourseInItalian_total++;
                    getCountAndSetCountInMap(courseProfile.Language_Italian_in__c, italianCountryCountMap);              
                }
                
                if( courseProfile.Language_Japanese_in__c != null ){
                    languageCourseInJapanese_total++;
                    getCountAndSetCountInMap(courseProfile.Language_Japanese_in__c, japaneseCountryCountMap);                
                }
                
                if( courseProfile.Language_Korean_in__c != null ){
                    languageCourseInKorean_total++;
                    getCountAndSetCountInMap(courseProfile.Language_Korean_in__c, koreanCountryCountMap);                
                }
                
                if( courseProfile.Language_Portuguese_in__c != null ){
                    languageCourseInPortuguese_total++;
                    getCountAndSetCountInMap(courseProfile.Language_Portuguese_in__c, portugueseCountryCountMap);            
                }
                
                if( courseProfile.Language_Russian_in__c != null ){
                    languageCourseInRussian_total++;
                    getCountAndSetCountInMap(courseProfile.Language_Russian_in__c, russianCountryCountMap);              
                }   
                
                if( courseProfile.Language_Spanish_in__c != null ){
                    languageCourseInSpanish_total++;
                    getCountAndSetCountInMap(courseProfile.Language_Spanish_in__c, spanishCountryCountMap);              
                }
                
                if( courseProfile.Type_of_Language_Course__c != null ){
                    typeOfLanguageCourse_total++;
                    getCountAndSetCountInMap(courseProfile.Type_of_Language_Course__c, typeOfCourseCountMap);                
                }
                
                if( courseProfile.Language_Other_in__c != null ){
                    furtherLanguagesIn_total++;
                    getCountAndSetCountInMap(courseProfile.Language_Other_in__c, furtherLanguageCountMap);               
                }
                
                if( courseProfile.Other_types_of_Language_Course__c != null ){
                    getCountAndSetCountInMap(courseProfile.Other_types_of_Language_Course__c, typeOfCourseCountMap);
                }

                if( courseProfile.Secondary_and_high_school_programmes__c != null ){
                    getCountAndSetCountInMap(courseProfile.Secondary_and_high_school_programmes__c , secondaryHighSchoolProgMap);
                }
                                
                if( courseProfile.Other_secondary_and_high_school_programm__c != null ){
                    getCountAndSetCountInMap(courseProfile.Other_secondary_and_high_school_programm__c , secondaryHighSchoolProgMap);
                }
                
                if( courseProfile.Career_Vocational_Certificate_Diploma__c != null ){
                    careerVocationalCertificateDiploma_total++;
                    getCountAndSetCountInMap(courseProfile.Career_Vocational_Certificate_Diploma__c, careerVocationalCertificateDiplomaCountMap);                
                }
                
                if( courseProfile.Other_Career_VocationalCertificate_Dipl__c != null ){
                    getCountAndSetCountInMap(courseProfile.Other_Career_VocationalCertificate_Dipl__c, careerVocationalCertificateDiplomaCountMap);
                }
                
                if( courseProfile.Undergraduate_Degree_Bachelor__c != null ){
                     undergraduateDegreeBachelor_total++;
                     getCountAndSetCountInMap(courseProfile.Undergraduate_Degree_Bachelor__c,undergraduateDegreeBachelorCountMap);
                }
                
                if( courseProfile.Other_Undergraduate_Degree_Bachelor__c != null ){              
                    getCountAndSetCountInMap(courseProfile.Other_Undergraduate_Degree_Bachelor__c,undergraduateDegreeBachelorCountMap);
                }
                
                if( courseProfile.Graduate_PostgraduateCertificate_Diploma__c != null ){
                    graduatePostGraduateCertificateDiploma_total++;
                    getCountAndSetCountInMap(courseProfile.Graduate_PostgraduateCertificate_Diploma__c,graduatePostgraduateCertificateDiplomaCountMap);
                }
                
                if( courseProfile.Other_Graduate_PostgradCertificate_Dipl__c != null ){
                    getCountAndSetCountInMap(courseProfile.Other_Graduate_PostgradCertificate_Dipl__c, graduatePostgraduateCertificateDiplomaCountMap);
                }
                
                if( courseProfile.Graduate_Postgraduate_Masters__c != null ){
                    graduatePostGraduateMasters_total++;
                    getCountAndSetCountInMap(courseProfile.Graduate_Postgraduate_Masters__c, graduatePostgraduateMastersCountMap);
                }
                
                if( courseProfile.Other_Graduate_Postgraduate_Masters__c != null ){
                    getCountAndSetCountInMap(courseProfile.Other_Graduate_Postgraduate_Masters__c, graduatePostgraduateMastersCountMap);
                }
                
                if( courseProfile.Graduate_Postgraduate_Doctorate__c != null ){
                    graduatePostGraduateDoctorate_total++;
                    getCountAndSetCountInMap(courseProfile.Graduate_Postgraduate_Doctorate__c, graduatePostgraduateDoctorateCountMap);
                }
                
                if( courseProfile.Other_Graduate_Postgraduate_Doctorate__c != null ){
                    getCountAndSetCountInMap(courseProfile.Other_Graduate_Postgraduate_Doctorate__c, graduatePostgraduateDoctorateCountMap);
                }  
                
                if( courseProfile.Additional_programmes_and_services__c != null ){
                    getCountAndSetCountInMap(courseProfile.Additional_programmes_and_services__c, additionalProgMap);
                }                 

                if( courseProfile.Other_additional_programmes_and_services__c !=null ){
                    getCountAndSetCountInMap(courseProfile.Other_additional_programmes_and_services__c, additionalProgMap);
                }
            }
            
            List<SubSectionWrapperClass> languageSectionList = new List<SubSectionWrapperClass>();
            
            List<SubSectionWrapperClass> studyProgSectionList = new List<SubSectionWrapperClass>();
            
            //Language section
            SubSectionWrapperClass langArabic = formSubSectionWrapperData( 'Language Arabic In' , languageCourseInArabic_total, arabicCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langArabic, languageCourseInArabic_total);
            
            SubSectionWrapperClass langChinese = formSubSectionWrapperData( 'Language Chinese In' , languageCourseInChinese_total, chineseCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langChinese, languageCourseInChinese_total);
            
            SubSectionWrapperClass langEnglish = formSubSectionWrapperData( 'Language English In' , languageCourseInEnglish_total, englishCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langEnglish, languageCourseInEnglish_total);
            
            SubSectionWrapperClass langFrench = formSubSectionWrapperData( 'Language French In' , languageCourseInFrench_total, frenchCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langFrench, languageCourseInFrench_total);
            
            SubSectionWrapperClass langGerman = formSubSectionWrapperData( 'Language German In' , languageCourseInGerman_total, germanCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langFrench, languageCourseInFrench_total);
            
            SubSectionWrapperClass langItalian = formSubSectionWrapperData( 'Language Italian In' , languageCourseInItalian_total, italianCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langItalian, languageCourseInItalian_total);
            
            SubSectionWrapperClass langJapanese = formSubSectionWrapperData( 'Language Japanese In' , languageCourseInJapanese_total, japaneseCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langJapanese, languageCourseInJapanese_total);
            
            SubSectionWrapperClass langKorean = formSubSectionWrapperData( 'Language Korean In' , languageCourseInKorean_total, koreanCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langKorean, languageCourseInKorean_total);
            
            SubSectionWrapperClass langPortuguese = formSubSectionWrapperData( 'Language Portuguese In' , languageCourseInPortuguese_total, portugueseCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langPortuguese, languageCourseInPortuguese_total);
            
            SubSectionWrapperClass langRussian = formSubSectionWrapperData( 'Language Russian In' , languageCourseInRussian_total, russianCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langRussian, languageCourseInRussian_total);
            
            SubSectionWrapperClass langSpanish = formSubSectionWrapperData( 'Language Spanish In' , languageCourseInSpanish_total, spanishCountryCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langSpanish, languageCourseInSpanish_total);
            
            SubSectionWrapperClass langFurther = formSubSectionWrapperData( 'Further language(s) in' , furtherLanguagesIn_total, furtherLanguageCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langFurther, furtherLanguagesIn_total);
            
            SubSectionWrapperClass langTypeOf = formSubSectionWrapperData( 'Type of Language Course' , typeOfLanguageCourse_total, typeOfCourseCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(languageSectionList, langTypeOf, typeOfLanguageCourse_total);
            
            //form language course full section 
            FullSectionWrapperClass languageCourse = formFullSectionWrapperData('Language Courses', languageCourses_total, languageSectionList );
            
            //Higher School Section
            SubSectionWrapperClass highSchoolProg = formSubSectionWrapperData( 'Secondary and High School Programmes' , secHighSchoolPrograms_total, secondaryHighSchoolProgMap);
            
            //Study Programme Section
            SubSectionWrapperClass careerVocationalCertificateDiploma = formSubSectionWrapperData( 'Career / Vocational Certificate /Diploma' , careerVocationalCertificateDiploma_total, careerVocationalCertificateDiplomaCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(studyProgSectionList, careerVocationalCertificateDiploma, careerVocationalCertificateDiploma_total);
            
            SubSectionWrapperClass undergraduateDegreeBachelor = formSubSectionWrapperData( 'Undergraduate Degree / Bachelor' , undergraduateDegreeBachelor_total, undergraduateDegreeBachelorCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(studyProgSectionList, undergraduateDegreeBachelor, undergraduateDegreeBachelor_total);
            
            SubSectionWrapperClass graduatePostGraduateCertificateDiploma = formSubSectionWrapperData( 'Graduate/PostgraduateCertificate/Diploma' , graduatePostGraduateCertificateDiploma_total, graduatePostgraduateCertificateDiplomaCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(studyProgSectionList, graduatePostGraduateCertificateDiploma, graduatePostGraduateCertificateDiploma_total);
            
            SubSectionWrapperClass graduatePostGraduateMasters = formSubSectionWrapperData( 'Graduate / Postgraduate Masters' , graduatePostGraduateMasters_total, graduatePostgraduateMastersCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(studyProgSectionList, graduatePostGraduateMasters, graduatePostGraduateMasters_total);
            
            SubSectionWrapperClass graduatePostGraduateDoctorate = formSubSectionWrapperData( 'Graduate / Postgraduate Doctorate' , graduatePostGraduateDoctorate_total, graduatePostgraduateDoctorateCountMap);
            //add element to list if count is gretaer than 0
            addElementsToList(studyProgSectionList, graduatePostGraduateDoctorate, graduatePostGraduateDoctorate_total);

            //form study program full section
            FullSectionWrapperClass studyProg = formFullSectionWrapperData('Study Programmes', studyPrograms_total, studyProgSectionList );
                    
            // additional program section
            SubSectionWrapperClass addPrograms = formSubSectionWrapperData( 'Additional Programmes and Services' , addProgramsAndServices_total, additionalProgMap);                        
            
            FullContentWrapperClass finalWrapper = formFullContentWrapperData(languageCourse, studyProg, addPrograms, highSchoolProg);  
                        
            finalWrapperString = JSON.serialize(finalWrapper);          
            
        }
        return finalWrapperString;
    }   
    
    /*
    * @ Description : Method to add elements to specified List
    * @ Parameters : i) List of SubSectionWrapperClass to which elements are to be added
                     ii) SubSectionWrapperClass instance to be added to list
                     iii) count to check the condition
    */
    private static void addElementsToList( List<SubSectionWrapperClass> subSectionList, SubSectionWrapperClass sectionElement, Integer count ){
        if( count > 0){
            subSectionList.add(sectionElement);
        }
    }
    
    /*
    * @ Description : Method to generate Map of Topic to its respective count
    * @ Parameters : i) String of topics(semicoln seperated string)
                     ii) Map to which elements are to be added
    */
    private static void getCountAndSetCountInMap(String stringToBeCounted, Map<String, Integer> valueMap){
        String[] arrayOfSplitValues = stringToBeCounted.split(';');
        
        for(Integer i = 0; i < arrayOfSplitValues.size(); i++){
             Integer count = valueMap.get(arrayOfSplitValues[i]);

             if(count == null)
                   count = 1;                                     
             else
                   count++;                                             
             valueMap.put(arrayOfSplitValues[i], count);
        }
    }
    
    /*
    * @ Description : method to form combine all blocks and form a single wrapper with all data
    * @ Return : Instance of FullContentWrapperClass
    */
    public static FullContentWrapperClass formFullContentWrapperData(FullSectionWrapperClass languageCourse, FullSectionWrapperClass studyProg, SubSectionWrapperClass additionalProg, SubSectionWrapperClass higherStudies){
        
        FullContentWrapperClass fullContentWrapper = new FullContentWrapperClass();
                                                        
        fullContentWrapper.languageCourses = languageCourse;     
        fullContentWrapper.secondaryHighSchoolProg = higherStudies;                     
        fullContentWrapper.studyProgrammes = studyProg;
        fullContentWrapper.addProgrammesServices = additionalProg;
        return fullContentWrapper;
    }
    
    /*
    * @ Description : method to combine sub sections and form complete one section
    * @ Return : Instance of FullSectionWrapperClass
    */ 
    public static FullSectionWrapperClass formFullSectionWrapperData(String mainTopicName, Integer count, List<SubSectionWrapperClass> subSectionList ){
        
        FullSectionWrapperClass fullSectionWrapper = new FullSectionWrapperClass();
        
        if( String.isNotBlank(mainTopicName) && count != 0 ){
                               
            fullSectionWrapper.topicName = mainTopicName;
            fullSectionWrapper.count = count;
            fullSectionWrapper.childrenList = subSectionList;
        }
        return fullSectionWrapper;
    }
    
    /*
    * @ Description : method to combine core section and form subsection
    * @ Return : Instance of SubSectionWrapperClass
    */
    public static SubSectionWrapperClass formSubSectionWrapperData(String mainTopicName, Integer count, Map<String, Integer> topicNameToCountMap){
        
        SubSectionWrapperClass subSectionWrapper = new SubSectionWrapperClass();
        
        if( String.isNotBlank(mainTopicName) && count != 0 ){
            
            List<CoreWrapperClassForTopicAndCount> coreWrapperList = createCoreWrapperClassForTopicAndCount(topicNameToCountMap);                   
            
            subSectionWrapper.topicName = mainTopicName;
            subSectionWrapper.count = count;
            subSectionWrapper.childrenList = coreWrapperList;
        }
        return subSectionWrapper;
    }       
    
    /*
    * @ Description : method to form core wrapper class by forming list of children
    * @ Return : List of CoreWrapperClassForTopicAndCount
    */
    public static List<CoreWrapperClassForTopicAndCount> createCoreWrapperClassForTopicAndCount( Map<String, Integer> topicNameToCountMap ){
        
        List<CoreWrapperClassForTopicAndCount> childrenList = new List<CoreWrapperClassForTopicAndCount>();
        
        for( String topic : topicNameToCountMap.keySet() ){         
            childrenList.add(new CoreWrapperClassForTopicAndCount( topic, topicNameToCountMap.get(topic)));
        }
        return childrenList;
    }
        
    
    public class CoreWrapperClassForTopicAndCount {
        public String topicName;
        public Integer count = 0;
        
        public CoreWrapperClassForTopicAndCount(String topic, Integer value) {
            topicName = topic;
            count = value;
        }
    }
    
    public class SubSectionWrapperClass {
        public String topicName;
        public Integer count = 0;
        public List<CoreWrapperClassForTopicAndCount> childrenList;               
    }
    
    public class FullSectionWrapperClass {
        public String topicName;
        public Integer count = 0;
        public List<SubSectionWrapperClass> childrenList;
    }
    
    public class FullContentWrapperClass {
        public FullSectionWrapperClass languageCourses;
        public SubSectionWrapperClass secondaryHighSchoolProg;
        public FullSectionWrapperClass studyProgrammes;
        public SubSectionWrapperClass addProgrammesServices;        
    }   
}