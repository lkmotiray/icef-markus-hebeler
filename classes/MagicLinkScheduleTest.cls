@isTest
private class MagicLinkScheduleTest {

    // Dummy CRON expression: midnight on March 15.
    // Because this is a test, job executes
    // immediately after Test.stopTest().
    public static String CRON_EXP = '0 0 0 15 3 ? 2030';

    static testmethod void testMagicLinkScheduler(){
        DateTime currentDate = System.now();
        Integer numCons = 3;
        Integer expectedUpdates = numCons*2;
        Account acct = DataFactory.createTestEducators(1)[0];
        insert acct;
        List<Contact> contacts = new List<Contact>();
  		List<contact> newContacts = new List<Contact>();
        List<Contact> conList1 = new List<Contact>();
        List<Contact> conList2 = new List<Contact>();
        List<Contact> conList3 = new List<Contact>();
        List<Contact> conList4 = new List<Contact>();
        contacts = DataFactory.createTestContacts(acct, numCons*3);
        for (Integer i=0;i<contacts.size();i++){
            if(i<numCons){
                conList1.add(contacts[i]);
            }
            else if (i<numCons*2){
                conList2.add(contacts[i]);
            }
            else if (i<numCons*3){
                conList3.add(contacts[i]);
            }
            else {
                ConList4.add(contacts[i]);
            }
        }
        
        //Create numCons Contacts with outdated Magic link date
        //should be updated
        contacts = conList1;
        for(Contact con : contacts){
            con.magic_link_updated__c = currentDate - 9;
            newContacts.add(con);
        }
        
        //create numCons contacts with Magic link date of two days ago
        //should NOT be updated
        contacts.clear();
        contacts = conList2;
        for(Contact con : contacts){
            con.magic_link_updated__c = currentDate -2;
            newContacts.add(con);
        }
        
        //create numCons contacts with empty Magic link date
        //should be updated
        contacts.clear();
        contacts = ConList3;
        newContacts.addAll(contacts);
        
        //create numCons contact without Email address
        //should NOT be updated
//        contacts.clear();
//        contacts = conList4;
//        for (Contact con : contacts){
//            con.Email = null;
//            newContacts.add(con);
//        }
        
        //insert Contacts
        insert newContacts;
        
        //Get ids of newly created contacts
        Map<Id, Contact> contactMap = new Map<Id,Contact>(newContacts);
        List<Id> contactIds = new List<Id>(contactMap.keySet());
        
        Test.StartTest();
        
        //schedule scheduler
        String jobId = System.schedule('Scheduled Apex Test', CRON_EXP, new MagicLinkSchedule());
        
        //verify it has not run yet
        List<Contact> conList = [SELECT Id 
                                 FROM Contact 
                                 WHERE Id IN :contactIds 
                                 AND magic_link_updated__c > :currentDate-1];
        System.assertEquals(0, conList.size());
        
        //StopTest to run scheduler
        Test.StopTest();
        
		//check if batches have been created
		CronTrigger testJob = [SELECT id, State FROM CronTrigger WHERE Id = :jobId][0];
        System.assertEquals('WAITING', testJob.State);
    }    
}