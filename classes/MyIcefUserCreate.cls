//create My Icef User for newly created Contacts
//use MyIcefUserUpdate instead!!!
public class MyIcefUserCreate {
	@invocableMethod(label='create MyIcef User' description = 'calls future Method to create MyIcefUser')
    public static void createUsersForNewContacts(List<Id> contactIds){
        for(Id cId : contactIds){
            //check contact Status first, only create if active? bei Create notwendig?
        	AuthUser.createorUpdateUser(cId);
        }
    }
}