/**
 * @className  CaseOwnerHelper
 * @purpose  Update case owner and send mail to owner when case is created
 * @lastModifiedOn  18 Nov 2015
 * @ModifiedBy Amol
 */ 
public class CaseOwnerHelper {
    public static Boolean isExecuted = false;
    public static void ChangeCaseOwner(List<Case> newCases, Map<Id,Case>mapOldCases) {
        /* Fetch Account Ids in SET  */  
        Set<Id> accountID = new Set<Id>();
        List<Case> caseList = getNewCases(newCases);        
        if (caseList.size() > 0) {
            for(Case caseRec : caseList) {
                accountID.add(caseRec.AccountId);
            }
            
            /* Map Account Id's with respective OwnerId's */
            Map<Id, Id> AccountOwnerMap = new Map<Id, Id>();
            List<Account> acc = [Select Id, OwnerId from Account where Id IN: accountID];
            for(Account a: acc) {
                AccountOwnerMap.put(a.id, a.OwnerId);
            }                
            
            for(Case caseRec : caseList) {                
                if(String.isNotBlank(caseRec.AccountId) && AccountOwnerMap.containsKey(caseRec.AccountId)) {                
                    caseRec.OwnerId = AccountOwnerMap.get(caseRec.AccountId);      
                }
                //  set recurssive field
                caseRec.Recursive_Field__c = TRUE;    
                System.debug('caseRec.Recursive_Field__c::' + caseRec.Recursive_Field__c);                

            }            
            
            List<Id> recordsUpdates = new List<Id>();            
            try {
                Database.SaveResult[] updateResult = Database.update(caseList, false);
                for (Database.SaveResult sr : updateResult) {
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        recordsUpdates.add(sr.getId());
                    }
                }
            }
            catch(Exception e) {
                System.debug('Exception caught:::'+e.getMessage());
            }
            
            /*
            if(!recordsUpdates.isEmpty()) {
                // Send Email To Oner of cases record which is saved successfully
                System.debug('recordsUpdates::' + recordsUpdates);
                sendEmail(recordsUpdates, mapOldCases);
            }
            */           
        }
        // Send email If case owner is updaetd 
//         sendEmail(newCases,mapOldCases); //don't send emails - 13.03.2019

    }
    
    private static List<Case> getNewCases(List<Case> newCases) {
        List<Case> caseList = new List<Case>();
        
        try {
            caseList = [SELECT Id, AccountId, ContactId, OwnerId, Recursive_Field__c                         
                        FROM Case 
                        WHERE Id IN: newCases 
                        AND Recursive_Field__c = FALSE
                        AND AccountId != NULL];
        }
        catch(Exception e) {
            System.debug('Exception caught:::'+e.getMessage());
        }
        
        return caseList;
    }
    
//    public static void sendEmail(List<Case> listNewCasesUpdated, Map<Id,Case>mapOldCasesToCheckOwner) {
//        
//        List<Case> listNewCases;
//        try {
 //           listNewCases = [SELECT Id, AccountId, ContactId, OwnerId, 
//                            Owner.Name, Owner.Email, SuppliedName, SuppliedEmail, Account.Name, 
//                            Account_Country__c, SuppliedCompany, Country_of_business__c, Country_of_Business_picklist__c,
//                            Main_Activity__c, Subject, Description 
//                            FROM Case 
 //                           WHERE Id IN: listNewCasesUpdated
//                           ];
//        }
//        catch(exception ex){
//            System.debug('Exception caught:::'+ex.getMessage());
//        }        
//        if(listNewCases != null) {
//            System.debug('listNewCases:::' + listNewCases);
//            List<Messaging.SingleEmailMessage>listEmailToSend = new List<Messaging.SingleEmailMessage>();
//            
//            Messaging.SingleEmailMessage mail;
//            List<String> listStringArguments;
            
            // create mail for each case
//            for(Case caseRec: listNewCases) { 
//                if((mapOldCasesToCheckOwner.containsKey(caseRec.Id))){
//                    if(mapOldCasesToCheckOwner.get(caseRec.Id).OwnerId == caseRec.OwnerId) {
//                        continue;
//                    }                
//                }  
//                listStringArguments = new List<String>(); 
                
                // create Email template body arguments sequentially 
 //               if(caseRec.Owner.Name != null) {
//                    listStringArguments.add(caseRec.Owner.Name);
//                }
//                else {
//                    listStringArguments.add(' ');
//                }
//                
//                if(caseRec.SuppliedName != null) {
//                    listStringArguments.add(caseRec.SuppliedName);
//                }
//                else {
//                    listStringArguments.add(' ');
//                }
//                
//                if(caseRec.SuppliedEmail != null) {
//                    listStringArguments.add(caseRec.SuppliedEmail);
//                }
//                else {
//                    listStringArguments.add(' ');
//                }
                
//                if(caseRec.Account.Name != null) {
//                    listStringArguments.add(caseRec.Account.Name);
//                }
//                else {
//                    listStringArguments.add(' ');
//                }
//                if(caseRec.Account_Country__c != null) {
//                    listStringArguments.add(caseRec.Account_Country__c);
//                }
//                else {
//                    listStringArguments.add(' ');
//                }
//                if(caseRec.SuppliedCompany != null) {
//                    listStringArguments.add(caseRec.SuppliedCompany);
//                }
//                else {
//                    listStringArguments.add(' ');
//                }
//                if(caseRec.Country_of_Business_picklist__c != null) {
//                    listStringArguments.add(caseRec.Country_of_Business_picklist__c);
//                }
//                else {
//                    listStringArguments.add(' ');
//                }
//                if(caseRec.Main_Activity__c != null) {
//                    listStringArguments.add(caseRec.Main_Activity__c);
//                }
//                else {
//                    listStringArguments.add(' ');
//                }
//                if(caseRec.Subject != null) {
//                    listStringArguments.add(caseRec.Subject);
//                }
//                else {
//                    listStringArguments.add(' ');
//                }
//                if(caseRec.Description != null) {
//                    string strDescrition = caseRec.Description.replace('\n','<br/>');
 //                   listStringArguments.add(strDescrition.replace(' ','&nbsp;'));
//                    System.debug('caseRec.Description::'+caseRec.Description);
//                }
//                else {
//                    listStringArguments.add(' ');
//                }
                //
//                system.debug('caseRec::' + caseRec);
//                //New instance of a single email message
//                mail = new Messaging.SingleEmailMessage();            
//                mail.setHtmlBody('');
                //String strHtmlBody = '<p style="color: rgb(0, 0, 0); font-family: "Times New Roman"; font-size: medium; '
                    //+ ' font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; '
                    //+ ' line-height: normal; orphans: auto; text-align: start; text-indent: 0px;'
                    //+ ' text-transform: none; white-space: normal; widows: 1; word-spacing: 0px;'
                    //+ ' -webkit-text-stroke-width: 0px;">Dear {0},</p> <p style="color: rgb(0, 0, 0); '
                    //+ 'font-family: "Times New Roman"; font-size: medium; font-style: normal;' 
                    //+ 'font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal;'
                    //+ ' orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; '
                    //+ 'widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;">You have a new case:<br /> <br /> '
                    //+ 'Name: <b>{1}</b><br />Email address: <b>{2}</b><br /> Account: <b>{3} </b><br />Account Country: '
                    //+ '<b>{4}</b><br /><br />Additional information if the case came from the web:<br />Company: <b>{5} '
                    //+ '</b><br />Country of business: <b>{6}</b><br />Main activity: <b>{7}</b></p>'
                    //+ '<p style="color: rgb(0, 0, 0); font-family: "Times New Roman"; font-size: medium; font-style: normal; '
                    //+ 'font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; '
                    //+ ' text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; '
                    //+ 'word-spacing: 0px; -webkit-text-stroke-width: 0px;"><b>Subject: </b><i>{8}</i><br /> <b>Description: </b><i>{9} '                   
                    //+ '</i></p><p style="color: rgb(0, 0, 0); font-family: "Times New Roman"; font-size: medium; font-style: normal; '
                    //+ 'font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto;'
                    //+ ' text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; '
//                    + ' -webkit-text-stroke-width: 0px;"><a href="'
                    //+ String.ValueOf(URL.getSalesforceBaseUrl()).substringBetween('Url:[delegate=',']')
//                    + '/' 
//                    + caseRec.Id
                    //+ '">Click here </a>to go to the case</p><p style="color: rgb(0, 0, 0); font-family: "Times New Roman"; '
                    //+ ' font-size: medium; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; '
                    //+ 'line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;' 
//                    + ' widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px;"> '
                    //+ '<i>NOTE: Please respond to the enquiry or problem as soon as possible and then close the case.</i></p>';
                //
//                strHtmlBody = String.format(strHtmlBody, listStringArguments);
//                system.debug(strHtmlBody);
                //
//                mail.setHtmlBody(strHtmlBody);              
                //
//                mail.setSubject('You have a new case');
//                String[] toAddresses = new String[] {caseRec.Owner.Email};                
//                mail.setToAddresses(toAddresses);
//                mail.setHtmlBody(strHtmlBody);
//                mail.setBccSender(false);
//                mail.setUseSignature(false);            
//                mail.setReplyTo(userinfo.getUserEmail());
                //mail.setSenderDisplayName(userinfo.getUserName() + ' ' + caseRec.Account.Name);
//                mail.setSaveAsActivity(false);                        
//                listEmailToSend.add(mail);
                //
//            } 
//            try {
//                Messaging.sendEmail(listEmailToSend);
//            } catch(Exception ex) {
                //System.debug('Error Line' + ex.getLineNumber() + ' Message::' + ex.getMessage());
//            }
//        }
//    }
}