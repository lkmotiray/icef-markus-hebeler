public class UserInformation {
	public String id, username, firstName, lastName, email;
    public String createdTimestamp;
    public UserInfoAttribute attributes;
    
    String sfAccountId, recTypeId, recType,sfContactId;
    
    public void getAttributes(){
		sfAccountId = this.attributes.account_id[0];     
        recType = this.attributes.account_record_type[0];
        recTypeId = this.attributes.record_type_id[0];
        sfContactId = this.attributes.contact_id[0];
    }
        
}