@isTest(SeeAllData = true)
public class salesInvoicePDFComponentControllerTest {
    
    private static void setupTestData() {
        
        List<Account> accountList  = [SELECT ID FROM ACCOUNT LIMIT 1];
        List<c2g__codaAccountingCurrency__c> accountCurrencyList = [SELECT ID FROM c2g__codaAccountingCurrency__c LIMIT 1];
        
        c2g__codaInvoice__c invoiceToInsert = new c2g__codaInvoice__c(); 
        invoiceToInsert.c2g__InvoiceDate__c = Date.newInstance(2017,2,17);
        invoiceToInsert.c2g__DueDate__c = Date.newInstance(2017,3,17);
        invoiceToInsert.c2g__Account__c = accountList[0].Id;
        invoiceToInsert.c2g__InvoiceCurrency__c = accountCurrencyList[0].id;
        
        INSERT invoiceToInsert;
        
        c2g__codaInvoiceInstallmentLineItem__c installMentLineItem = new c2g__codaInvoiceInstallmentLineItem__c();
        installMentLineItem.c2g__Amount__c = 0;
        installMentLineItem.c2g__DueDate__c = system.today();
        installMentLineItem.c2g__Invoice__c = invoiceToInsert.Id;
    
        INSERT installMentLineItem;
        //InvoiceEmailSend.createMapInvoceineItems(new set<Id>{invoiceToInsert.id});
    } 
    
    static testMethod void testsalesInvoicePDF() {  
        
        List<Account> accountList  = [SELECT ID FROM ACCOUNT LIMIT 1];
        List<c2g__codaAccountingCurrency__c> accountCurrencyList = [SELECT ID FROM c2g__codaAccountingCurrency__c LIMIT 1];
        
        c2g__codaInvoice__c invoiceToInsert = new c2g__codaInvoice__c(); 
        invoiceToInsert.c2g__InvoiceDate__c = Date.newInstance(2017,2,17);
        invoiceToInsert.c2g__DueDate__c = Date.newInstance(2017,3,17);
        invoiceToInsert.c2g__Account__c = accountList[0].Id;
        invoiceToInsert.c2g__InvoiceCurrency__c = accountCurrencyList[0].id;
        
        INSERT invoiceToInsert;
        
        c2g__codaInvoiceInstallmentLineItem__c installMentLineItem = new c2g__codaInvoiceInstallmentLineItem__c();
        installMentLineItem.c2g__Amount__c = 0;
        installMentLineItem.c2g__DueDate__c = system.today();
        installMentLineItem.c2g__Invoice__c = invoiceToInsert.Id;
    
        INSERT installMentLineItem;
        
        //salesInvoicePDFComponentControllerTest.setupTestData();
        /*c2g__codaInvoice__c salesInvoice = [ SELECT Id 
                                             FROM c2g__codaInvoice__c 
                                             WHERE Id IN (SELECT c2g__Invoice__c 
                                                 FROM c2g__codaInvoiceInstallmentLineItem__c)
                                             ORDER BY c2g__Amount__c ASC 
                                             LIMIT 10 ];*/
        Test.startTest();
            List<c2g__codaInvoice__c>  listInvoice = [SELECT Id,c2g__InvoiceTotal__c,c2g__OutstandingValue__c
                                                     FROM c2g__codaInvoice__c 
                                                     WHERE Id IN (SELECT c2g__Invoice__c 
                                                         FROM c2g__codaInvoiceInstallmentLineItem__c)
                                                     Order by c2g__Account__c ASC
                                                     LIMIT 100];
        
        	/*for(c2g__codaInvoice__c salesInvoice: [  SELECT Id,c2g__InvoiceTotal__c,c2g__OutstandingValue__c
                                                     FROM c2g__codaInvoice__c 
                                                     WHERE Id IN (SELECT c2g__Invoice__c 
                                                         FROM c2g__codaInvoiceInstallmentLineItem__c)
                                                     Order by c2g__Account__c ASC
                                                     LIMIT 100]){*/
        	Set<Id> setId = new Set<Id>();	
        	for(c2g__codaInvoice__c salesInvoice : listInvoice){   
                setId.add(salesInvoice.id);
            }
        	
        	InvoiceEmailSend.mapInvoiceLines = InvoiceEmailSend.createMapInvoceineItems(setId);
            InvoiceEmailSend.mapInvoices = InvoiceEmailSend.createMapInvoces(setId);
        
        	for(c2g__codaInvoice__c salesInvoice : listInvoice){   
                salesInvoicePDFComponentController controller = new salesInvoicePDFComponentController();
                controller.invoice = salesInvoice;                               
            }
            
        Test.stopTest();
    }  
    
}