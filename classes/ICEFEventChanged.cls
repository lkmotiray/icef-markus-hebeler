/*
@ Name : ICEFEventChanged (Handler class for 'onChangedOfICEFEventTrigger ' trigger)
@ Description : 1) When ICEF Event of AP is changed and their exist PP related to AP 
@                  through Account and having same ICEF Event as old ICEF Event of AP. 
@                  Then send email alert to this ICEF Event manager.
@               2) If ICEF Event of AP is changed, then update the ICEF Event of its related CP.
@ Notation : AP - Account Particiaption
@            CP - Contact Particiaption
@            PP - Product Particiaption
@ Developer : Shaikh Saquib
@ Date : 09-Dec-2014
*/

public class ICEFEventChanged {

    public void icefEventChangedOnAP(List<Account_Participation__c> listOfNewAPs, 
                                     Map<Id, Account_Participation__c> mapOfOldAps) {
        
        // Map of Account_Participation__c related Account ids and ICEF Event ids whose 
        // Account_Participation__c.ICEF_Event__c  changed.
        Map<Id,Set<Id>> mapOfAccountAndICEFIds = new Map<Id,Set<Id>>();
        
        Id oldICEFEventId;
        Id accountID;
        
        String oldParticipationStatus;
        String newParticipationStatus;
        
        Set<Id> setOfICEFEvent;
        
        try {
            
            // Make map of changed Account_Participation__c.ICEF_Event__c
            for(Account_Participation__c recOfAp : listOfNewAPs) {
                
                // Id of old ICEF Event of AP
                oldICEFEventId = mapOfOldAps.get(recOfAp.id).ICEF_Event__c;
                // Old Participation Status of AP
                oldParticipationStatus = mapOfOldAps.get(recOfAp.Id).Participation_Status__c;
                // New Participation Status of AP
                newParticipationStatus = recOfAp.Participation_Status__c;
                
                System.debug('new ICEF_Event__c ::: ' + recOfAp.ICEF_Event__c);
                System.debug('old ICEF_Event__c ::: ' + oldICEFEventId);
                System.debug('oldParticipationStatus ::: ' + oldParticipationStatus);
                System.debug('newParticipationStatus ::: ' + newParticipationStatus);
                System.debug('condition ::: ' + (recOfAp.ICEF_Event__c != oldICEFEventId || 
                                                 (!newParticipationStatus.equals(oldParticipationStatus) &&
                                                  newParticipationStatus.equals('transferred'))));  
                
                System.debug('condtion 2 ::: ' + (!newParticipationStatus.equals(oldParticipationStatus) &&
                                                  newParticipationStatus.equals('transferred')));                
                
                // Check if ICEF Event or Participation Status is changed or not and 
                // value of new Participation status id 'trasfered' or not. 
                if(recOfAp.ICEF_Event__c != oldICEFEventId || 
                   (!newParticipationStatus.equals(oldParticipationStatus) &&
                    newParticipationStatus.equals('transferred') || newParticipationStatus.equals('canceled'))) {                                                                    
                        system.debug('newParticipationStatus=='+newParticipationStatus);
                        // Account Id of AP    
                        accountID = recOfAp.Account__c;
                        
                        // Create map of Account Id and set ICEF Event Id. 
                        // This ICEF Event manager will be getting email alert. 
                        if( mapOfAccountAndICEFIds.containsKey(accountID) ) {                        
                            
                            setOfICEFEvent = mapOfAccountAndICEFIds.get(accountID);
                            setOfICEFEvent.add(oldICEFEventId);
                            mapOfAccountAndICEFIds.put(accountID,setOfICEFEvent);
                        }   
                        else
                            mapOfAccountAndICEFIds.put(accountID,new Set<Id>{oldICEFEventId});
                    }
            }
            
            system.debug('mapOfAccountAndICEFIds=='+mapOfAccountAndICEFIds);
            
            // Set of ICEF Event Id which are getting email alert to their manager
            Set<Id> setOfChangedICEFEventIds = new Set<Id>();
            if(!mapOfAccountAndICEFIds.isEmpty()) {                
                setOfChangedICEFEventIds = new Set<Id>(getICEFEventManager(mapOfAccountAndICEFIds));
            }
            
            system.debug('setOfChangedICEFEventIds=='+setOfChangedICEFEventIds);
            
            // Send email alert to ICEF Event manager. 
            if(setOfChangedICEFEventIds.size() > 0) {
                sendEmailToICEFEventManager(setOfChangedICEFEventIds, mapOfOldAps);
                System.debug('\n Emails sent successfully');
            }
            
        } catch(Exception e) {
            System.debug('Exception : Line Number - ' + e.getLineNumber() + '\tError Message - ' + e.getMessage());
        }
    }
    
    /**
     @ Description : Get the set of ICEF Event Id's which are going to be notified through email.
     */
    private Set<Id> getICEFEventManager(Map<Id,Set<Id>> mapOfAccountAndICEFIds) {
    
        // Find list of account with product participation.
        List<Account> listOfAccount = [SELECT id, 
                                       (SELECT id,ICEF_Event__c from Product_Participations__r)
                                       FROM Account 
                                       WHERE 
                                        ID IN:mapOfAccountAndICEFIds.keySet()];
        
        Set<Id> setOfICEFEventIds;
        Set<Id> setOfChangedICEFEventIds = new Set<Id>();
        
        // Find list of ICEF event.
        for(Account recOfAccount : listOfAccount) {
                setOfICEFEventIds = mapOfAccountAndICEFIds.get(recOfAccount.id);
                if(setOfICEFEventIds !=null) {
                    System.debug('recOfAccount.Product_Participations__r ::: ' + recOfAccount.Product_Participations__r);
                    for(Product_Participation__c recOfProdParti : recOfAccount.Product_Participations__r) {
                        if(setOfICEFEventIds.contains(recOfProdParti.ICEF_Event__c))
                            setOfChangedICEFEventIds.add(recOfProdParti.ICEF_Event__c);
                    }
                }
        }
        
        return setOfChangedICEFEventIds;    
    }

    /**
     @ Description : Send email alert to ICEF Event managers.
     */
    private void sendEmailToICEFEventManager(Set<Id> setOfChangedICEFEventIds, Map<Id, Account_Participation__c> mapOldAccntParticipation) {
        
        // Map of ICEF Event records, which are going to be notified
        Map<Id, ICEF_Event__c> mapICEFEvent = new Map<Id, ICEF_Event__c>([SELECT Id, Name
                                                                         FROM ICEF_Event__c
                                                                         WHERE 
                                                                            Id IN :setOfChangedICEFEventIds]);
        
        Set<Id> setAccountId = new Set<Id>();
        
        // Create set of Account Id's related AP whose ICEF Event is changed
        for(Account_Participation__c recordAP : mapOldAccntParticipation.values() ) {
         
            if(setOfChangedICEFEventIds.contains(recordAP.ICEF_Event__c)) {                
                setAccountId.add(recordAP.Account__c);
            }
        }
        
        // Map of Account records related to AP
        Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Name, Owner.Name
                                                           FROM Account
                                                           WHERE
                                                           Id IN :setAccountId]);

        List<String> listEventManagerEmail;       
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        for(Account_Participation__c recordAP : mapOldAccntParticipation.values() ) {
            
            if(setOfChangedICEFEventIds.contains(recordAP.ICEF_Event__c)) {
                
                // Create a new Email
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                // Set list of people who should get the email
                List<String> sendTo = new List<String>();
                sendTo.add(recordAP.Event_Managers_Email__c);
                mail.setToAddresses(sendTo);
                
                // Set email contents - you can use variables!
                mail.setSubject('AP for ' + mapAccount.get(recordAP.Account__c).Name + ' has been transferred to other Workshop');
                
                
                String htmlBody = '<p>Hello,</p>' +
                    '<p>an AP has been transferred to a different Workshop. The appropriate Account has at least one ' + 
                    'Product Participation for that Workshop. Please check whether the PP needs to be transferred to ' + 
                    'the new Workshop as well.</p>' +
                    '<p>Account Name: <a href="https://emea.salesforce.com/' + recordAP.Account__c + '">' + mapAccount.get(recordAP.Account__c).Name + '</a><br/>' +
                    'Old Workshop: <b>' + mapICEFEvent.get(recordAP.ICEF_Event__c).Name + '</b><br/>' +
                    'Account Owner: <b>' + mapAccount.get(recordAP.Account__c).Owner.Name + '</b><br/>' +
                    'Account Participation: <a href="https://emea.salesforce.com/' + recordAP.Id + '">' +
                    recordAP.Name + '</a><p>Best regards,<br/>ICEF GmbH</p>';                
               
                // Set HTML body of mail
                mail.setHtmlBody(htmlBody);   
                
                String plainTextBody = 'Hello,\n\n' + 
                    'an AP has been transferred to a different Workshop. The apropriate Account has at least one Product Participation for hat Workshop. Please check whether the PP needs to be transferred to the new Workshop as well.\n\n' +
                    'Account Name: {!Account.Name}' + 
                    'Old Workshop: {!Account_Participation__c.ICEF_Event__c}' +
                    'Account Owner: {!Account.OwnerFullName}' +
                    'Account Participation: {!Account_Participation__c.Name}\n\n' +
                    'Best regards,\nICEF GmbH';
                
                // Set Plain Text body of mail
                mail.setPlainTextBody(plainTextBody);
                
                // Add your email to the master list
                mails.add(mail);
            }
        }
        
        // Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
    } 
    
    /**
     @ Description : Change ICEF Event on CP when their is change of ICEF Event on AP
     */
    public void changeICEFEventOnCP(Map<Id, Account_Participation__c> mapNewAccountParticipation, 
                                   Map<Id, Account_Participation__c> mapOldAccountParticipation) {
                                       
        List<Contact_Participation__c> listContactParticipation = new List<Contact_Participation__c>();
        Set<Id> setAccntParticipationId = new Set<Id>();
        
        try {
            
            // Get set of Account Participation records Id, whose ICEF Event is changed
            for(Account_Participation__c recordAP : mapNewAccountParticipation.values()) {
                
                if(recordAP.ICEF_Event__c != mapOldAccountParticipation.get(recordAP.Id).ICEF_Event__c) {
                    
                    setAccntParticipationId.add(recordAP.Id);                
                }
            } 
            System.debug('setAccntParticipationId :::  ' + setAccntParticipationId);
            
            if(!setAccntParticipationId.isEmpty()) {
                
                // Get list of Contact Participation related set of Account Participation records Id.
                listContactParticipation = new List<Contact_Participation__c>([SELECT ICEF_Event__c, Account_Participation__c
                                                                               FROM Contact_Participation__c
                                                                               WHERE
                                                                               Account_Participation__c IN :setAccntParticipationId]);    
                
                System.debug('listContactParticipation :::  ' + listContactParticipation);
                
                // Change/update the ICEF Event of CP related to AP's ICEF Event
                for(Contact_Participation__c recordCP : listContactParticipation) {
                    
                    recordCP.ICEF_Event__c = mapNewAccountParticipation.get(recordCP.Account_Participation__c).ICEF_Event__c;
                }                  
            }
            
        } catch(Exception e) {
            System.debug('Exception : Line Number - ' + e.getLineNumber() + '\tError Message - ' + e.getMessage());
        }
        
        // Update list of Contact Participation
        if(!listContactParticipation.isEmpty()) {
            update listContactParticipation; 
            System.debug('after update listContactParticipation :::  ' + listContactParticipation);
        }
    }
    
   
}