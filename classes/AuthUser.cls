/* invobable future methods for MyIcef users. 
 * Process to call these methods: 
 * - on create of new Contact: create new MyIcef User
 * - on change of email address: update MyIcef User (find by contact_email_prior_value__c) or delete User if Email is empty
 * - on change of other relevant fields: update User (find by Contact_Email__c) Make sure to only call if Email address is present!!
 * (- on deletion of Contact: delete User(?))
 */

public class AuthUser{ 
    
    public static String className (){
        return AuthUser.class.getName();
    }
    
    //email of a contact in Salesforce has changed. Find user and update username and email.
    @future(callout=true)
    public static void updateUserEmail(Id ContactId){
        System.debug('User Email changed. Find user to update by prior email value.');
        Contact con = getSfContact(contactId);
        String token = Authorization.getAccessToken();
        //as email (-> username) has changed, find Contact by prior email
        UserInformation user = getUserByUsername(con.Contact_email_prior_value__c, token);
        if (con.Contact_Email__c == null){
            //Email has been deleted. Delete User.
            deleteUser(user, token);
        } else {
            //Update user
            updateUser(contactId, user, token);
        }
    }
    
    //when this method is called, My Icef relevant info has changed, but not email address. Email address must not be null! 
    @future(callout=true)
    public static void createOrUpdateUser(Id contactId){      
        Contact con = getSfContact(contactId);
        if (con != null){
            if (con.Contact_Email__c == null){
                String subject = className() + ' - createOrUpdateUser';
                String body = 'no email address for contact with id ' + contactId + ' found. Aborting MyIcef User creation.';
                EmailUtil.sendErrorEmail(subject, body);
            } else {
                String token = Authorization.getAccessToken();    
                UserInformation user = getUserByUsername(con.Contact_Email__c, token);    
                // If there is no user for contact email: create new.
                if(user.id == null){
                    System.debug('No User found. Creating new one.');
                    createUser(contactId);
                } else {
                    System.debug('Userinfo changed in Contact. Updating user.');
                    updateUser(contactId, user, token);
                }
            }
        } else {
            String subject = className() + ' - createOrUpdateUser';
            String body = 'no active contact with id ' + contactId + ' found. Aborting myIcef user creation.';
            EmailUtil.sendErrorEmail(subject, body);
        }
    }

    //called from @future method so can't be @future
    public static void createUser(Id contactId){
		UserInformation usr = createUserInfo(contactId);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String token = Authorization.getAccessToken();
        String endpoint = 'https://login.icef.com:8443/auth/admin/realms/ICEF/users';
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setHeader('Authorization', 'Bearer '+ token);
        String postString = JSON.serialize(usr, true);
        System.debug('postString: ' + postString);  
        request.setBody(postString);
        HttpResponse response = http.send(request);
        if (response.getStatusCode() == 409){
            System.debug('User is already existing: '+ usr);
            String subject = 'Error in Class: ' + className();
            String body = 'User is already exiting: ' + JSON.serialize(usr, true);
            EmailUtil.sendErrorEmail(subject, body);
        }else if(response.getStatusCode() != 201){
            System.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus()); //email an mich?
        } else {
            System.debug(response.getBody());
        }
    }
    
    public static void deleteUser(UserInformation user, String token){
        //double check if there is no active contact with the user's email address
        List<Contact> contacts = new List<Contact>();
        contacts = [SELECT Id FROM Contact 
                    WHERE Contact_Email__c = :user.email 
                    AND Contact_Status__c = 'Active'];
        if (contacts.size() > 0 ){
        	System.debug('Will not delete user as active Contact is existing for username ' + user.username + '.SF Contact: '+contact.Id);   
        } else {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            String endpoint = 'https://login.icef.com:8443/auth/admin/realms/ICEF/users/'+user.id;
            request.setEndpoint(endpoint);
            request.setMethod('DELETE');
            request.setHeader('Authorization', token);
            System.debug('deletion request: ' + request);
            
            HttpResponse response = http.send(request);
            System.debug('Deletion Response: '+response.getStatusCode()+' '+response.getStatus());
        }
    }

    //called from @future method so can't be @future
    public static void updateUser(Id contactId, UserInformation user, String token){
        UserInformation userNew = createUserInfo(contactId);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String endpoint = 'https://login.icef.com:8443/auth/admin/realms/ICEF/users/'+user.id;        
        request.setEndpoint(endpoint);
        request.setMethod('PUT');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setHeader('Authorization', 'Bearer '+token);
        String bodyString = JSON.serialize(userNew, true);
        request.setBody(bodyString);
        
        HttpResponse response = http.send(request);
        if(response.getStatusCode() == 204){
            System.debug('update successful');
        } else {
            System.debug('update request not successful: '+ response.getStatusCode()+' '+response.getStatus());    
        }
    }

    
    public static UserInformation createUserInfo (Id contactId){
    	Contact con = getSfContact(contactId);
        UserInformation usr = new UserInformation();
        usr.firstName = con.FirstName;
        usr.lastName = con.LastName;
        usr.email = con.Contact_Email__c;
        usr.username = con.Username__c;
        
        //create attributes
        UserInfoAttribute attrs = new UserInfoAttribute();
        
        List<String> acctRecType = new List<String>();
        List<String> acctId = new List<String>();
        List<String> conId = new List<String>();
        List<String> recTypeId = new List<String>();
        
        acctRecType.add(con.Account_Record_Type__c);
        acctId.add(con.AccountId);
        conId.add(con.Id);
        recTypeId.add(con.RecordTypeId);
        
        attrs.account_record_type = acctRecType;
        attrs.account_id = acctId;
        attrs.contact_id = conId;
        attrs.record_type_id = recTypeId;
        
        usr.attributes = attrs;
        
        //System.debug('usr: ' + usr);
        //System.debug('userinfo to JSON: ' + JSON.serialize(usr));
        
        return usr;        
    }
    
    private static Contact getSfContact(Id cId){
        Contact con = [SELECT Id,FirstName,Username__c,LastName,Contact_Email__c,Account_Record_Type__c, RecordTypeId, Contact_email_prior_value__c, AccountId 
                       FROM Contact 
                       WHERE Id = :cId
                       AND Contact_Status__c = 'Active'];
        System.debug('found Contact in getSfContact: ' + con);
        return con;
    }

    
    public static UserInformation getUserByUsername(String username, String token){
        UserInformation usrInfo = new UserInformation();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String endpoint = 'https://login.icef.com:8443/auth/admin/realms/ICEF/users/?username='+username;
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        request.setHeader('Authorization', 'Bearer '+token);
        HttpResponse response = http.send(request);
        System.debug('response: ' + response.getBody());
        System.debug('Status User: '+response.getStatusCode());
        if(response.getStatusCode() == 200){
            List<UserInformation> usrInfos = (List<UserInformation>) JSON.deserialize(response.getBody(), List<UserInformation>.class);
            System.debug('usrInfos: ' + usrInfos);
            System.debug('usrInfos, size: ' + usrInfos.size());
            if (usrInfos.size() == 1){
            	usrInfo = usrInfos[0];
            	usrInfo.getAttributes();                
            }
          
        }  
        System.debug('usrInfo: ' + usrInfo);
        return usrInfo;
      
    }
    
    //wird die gebraucht?
    public static UserInformation getUserByUserId(String userId, String token){
        UserInformation usrInfo = new UserInformation();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String endpoint = 'https://login.icef.com:8443/auth/admin/realms/ICEF/users/'+userId;
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        request.setHeader('Authorization', 'Bearer '+token);
        HttpResponse response = http.send(request);
        System.debug('response User By Id: ' + response.getBody());
        System.debug('Status User by Id: '+response.getStatusCode());
        if(response.getStatusCode() == 200){
            usrInfo = (UserInformation) JSON.deserialize(response.getBody(), UserInformation.class);
            System.debug('usrInfo by Id: ' + usrInfo);
           	usrInfo.getAttributes();                
        }  
        System.debug('usrInfo: ' + usrInfo);
        return usrInfo;
      
    }
    
}