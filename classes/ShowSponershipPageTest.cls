/**
*   @Description : This class is used to Check the functionality of "ShowSponershipCon" classes.
*	@Date        : 19-1-2018
*/
@isTest(SeeAllData=True)
public class ShowSponershipPageTest {
    /**
    *   @Description : Test the functionality of "ShowSponershipPageCon" , "ShowSponershipCon" and "SponershipDetailsCon" classes.
    */
    public static testmethod void testShowSponershipPage(){ 
        
        // Get ICEF_Event__c record
        ICEF_Event__c ICEFEventRec = [SELECT Id 
                                      FROM ICEF_Event__c LIMIT 1];
        
        Test.startTest();
            
            PageReference pageReference = Page.ShowSponershipPage;
            Test.setCurrentPage(pageReference);
            
            // Put "ICEF_EventId" records 'Id' as a parameter
            ApexPages.currentPage().getParameters().put('Id',ICEFEventRec.Id);
            
            ApexPages.StandardController standerdControllerObj = new ApexPages.StandardController(ICEFEventRec);
        	
            // Test ShowSponershipPageCon class
            ShowSponershipPageCon ShowSponershipPageObj = new ShowSponershipPageCon(standerdControllerObj);
			ICEFEventRec = ShowSponershipPageObj.ICEF_EventRecord;
        
        	// Test ShowSponershipCon class
            ShowSponershipCon ShowSponershipObj = new ShowSponershipCon();
       	    ShowSponershipObj.ICEF_EventId = ICEFEventRec.Id;
        	ShowSponershipObj.showTopBorder = False;
        	String hideTopBorder = ShowSponershipObj.topBorder;
            ShowSponershipObj.showTopBorder = True;
        	String showTopBorder = ShowSponershipObj.topBorder;
            Map<String,List<ICEF_Event_Sponsorship__c>> ICEF_EventSponsorshipMap = ShowSponershipObj.ICEF_EventSponsorshipMap;
        	
            // Get Accommodation record
            Id accommodationId = [SELECT Id
                                  FROM Accommodation__c 
                                  LIMIT 1].Id;
        
            // Test SponershipDetailsCon class
            SponershipDetailsCon SponershipDetailsObj = new SponershipDetailsCon();
            SponershipDetailsObj.accommodationId = accommodationId;
            Accommodation__c accommodationRec = SponershipDetailsObj.accommodationRec;
        
        Test.stopTest();
   		
    }
}