/*  
    @Purpose      : Test class of Attachment trigger
    @Author       : Poonam 
    @Created Date : 16/08/2017 
*/

@isTest 
private class AttachmentTriggerHandlerTest {
    static testMethod void testPositiveCase() {
        Advert__c advert = new Advert__c(Name = 'Test advert', Advert_location__c = '1 | Top', Text_Advert__c = true);
        
        // Insert advert 
        insert advert;
        
        Attachment attachment = new Attachment();     
        attachment.Name = 'Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attachment.Body = bodyBlob;
        attachment.ParentId = advert.Id;
        insert attachment;
        
        delete attachment;
    }
}