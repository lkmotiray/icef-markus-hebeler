/*
*    @ Description: Webservice calling onclick of Sort Event on related list of opportunityline Item
*    Sorting order : 1. ICEF_Event__r.Start_date__c
*                    2. Product with name Display Table
*/

global class customSortOpportunityItem {
    
    webservice static String MRsort(Id oppID)
    {
        
        Map<Date,List<OpportunityLineItem>> mapOfProduct = new Map<Date,List<OpportunityLineItem>>();
        
        for(OpportunityLineItem product :  [ SELECT Id, 
                                                 Name,
                                                 ICEF_Event__r.Start_date__c
                                          FROM OpportunityLineItem 
                                          WHERE OpportunityId = :oppId
                                          ORDER BY ICEF_Event__r.Start_date__c, TotalPrice desc
                                         ]){
                                         
            List<OpportunityLineItem> listLineItem = new List<OpportunityLineItem>();
            
            if(mapOfProduct.containsKey(product.ICEF_Event__r.Start_date__c)){
                listLineItem = mapOfProduct.get(product.ICEF_Event__r.Start_date__c);
                listLineItem.add(product);
                mapOfProduct.put(product.ICEF_Event__r.Start_date__c,listLineItem);
            }else{
                listLineItem.add(product);
                mapOfProduct.put(product.ICEF_Event__r.Start_date__c,listLineItem);
            }
                
        }
        System.debug('mapOfProduct===='+mapOfProduct);
        String sortedIds = '';
        //build the comma separated 15 character OLI Id string to send back
        for(Date sDate : mapOfProduct.keySet()){
        
            List<OpportunityLineItem> ListProduct = new List<OpportunityLineItem>();
            List<OpportunityLineItem> ListProduct1 = new List<OpportunityLineItem>();
            List<OpportunityLineItem> ListProduct2 = new List<OpportunityLineItem>();
            
            ListProduct = mapOfProduct.get(sDate);
            
            for(OpportunityLineItem lineItem : ListProduct ){
                if(lineItem.Name.contains('Display Table')){
                    ListProduct1.add(lineItem);
                    
                }else{
                    ListProduct2.add(lineItem);
                }
            }
            ListProduct1.addAll(ListProduct2);
            ListProduct2.clear();
            mapOfProduct.put(sDate,ListProduct1);
            
        }
        
        for(LIST<OpportunityLineItem> listLineItemSort : mapOfProduct.values()){            
            for(OpportunityLineItem lineItem : listLineItemSort){
                sortedIds += String.valueOf(lineItem.Id).substring(0,15) + ',';
            }
        }
        
        sortedIds = sortedIds.substring(0,sortedIds.length() - 1);
        System.debug('sortedIds ===='+sortedIds );
        return sortedIds;
    }
}