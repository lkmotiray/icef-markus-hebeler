public class EmailAttachmentPDFController
{
    public string emailTemplateHtml{get;set;}
    Public Account accountRecord {get;set;}
    public EmailAttachmentPDFController()
    {
        String emailTemplateId = ApexPages.currentPage().getParameters().get('etid');
        String contactPartId = ApexPages.currentPage().getParameters().get('cpid');
        String accommodationId = ApexPages.currentPage().getParameters().get('id');
        
        if(!String.isBlank(emailTemplateId) && !String.isBlank(contactPartId))
        {
            List<EmailTemplate> lstEmailTemplates = [SELECT Id,Name,Body
                                                     FROM EmailTemplate
                                                     WHERE id = :emailTemplateId LIMIT 1];
                                                     
            if(!lstEmailTemplates.isEmpty())
            {
                accountRecord = new Account();
                String templateBody = lstEmailTemplates[0].Body;
                System.debug('templateBody::'+templateBody);
                if(accommodationId != null && accommodationId != '' && accommodationId != 'null'){
                   sendAccommodationConfirmationController emailController = new sendAccommodationConfirmationController(accommodationId,contactPartId,emailTemplateId);
                    //emailController.showEmailBodyOnPageLoad(emailTemplateId);
                    if(String.isNotBlank(templateBody)){
                        templateBody = emailController.replaceAccommodationMergeFields(templateBody);                   
                        templateBody = emailController.replaceCPMergeFields(templateBody);                  
                        templateBody = templateBody.replaceAll('\r\n','<br/>');
                        templateBody = '<p>' + templateBody + '</p>';
                        emailTemplateHtml = templateBody;
                        //emailTemplateHtml = emailController.body.replaceAll('<!\\[CDATA\\[','').replaceAll(']]>','');
                    }
                }
                else {
                    sendemailtocontactparticipation emailController = new sendemailtocontactparticipation(emailTemplateId,contactPartId);
                    templateBody = emailController.replaceMergeFields(templateBody);
                    templateBody = templateBody.replaceAll('\r\n','<br/>');
                    templateBody = '<p>' + templateBody + '</p>';
                    emailTemplateHtml = templateBody;
                }
            }
        }
    }
}