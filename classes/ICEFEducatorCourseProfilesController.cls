public with sharing class ICEFEducatorCourseProfilesController {

    public string ICEFEventId;
    public string icefEventWithIdForTestMethod;
    
    /*
    *    PageBlock Section values
    *
    */
    public integer languageCourses_total {get; set;}
    public integer secHighSchoolPrograms_total {get; set;}
    public integer studyPrograms_total {get; set;}
    public integer addProgramsAndServices_total {get; set;}
    
    public string eventName{get; set;}
    public string langCourseTitle {get; set;}
    public string secHighSchoolPrograms {get; set;}
    public string studyPrograms {get; set;}
    public string addProgramsAndServices {get; set;}
     
    /*
    *
    * Counts and Getter-Setter for Language Courses
    */
    public string languageCourseInArabic {get; set;}
    public String languageCourseInChinese  {get; set;}
    public String languageCourseInEnglish  {get; set;}
    public String languageCourseInFrench  {get; set;}
    public String languageCourseInGerman  {get; set;}
    public String languageCourseInItalian  {get; set;}
    public String languageCourseInJapanese  {get; set;}
    public String languageCourseInKorean  {get; set;}
    public String languageCourseInPortuguese  {get; set;}
    public String languageCourseInRussian  {get; set;}
    public String languageCourseInSpanish  {get; set;}
    public String furtherLanguagesIn {get; set;}
    public String typeOfLanguageCourse {get; set;}
    public String otherTypeOfLanguageCourse {get; set;}
   
    public integer languageCourseInArabic_total {get; set;}
    public integer languageCourseInChinese_total {get; set;}
    public integer languageCourseInEnglish_total {get; set;}
    public integer languageCourseInFrench_total {get; set;}
    public integer languageCourseInGerman_total {get; set;}
    public integer languageCourseInItalian_total {get; set;}
    public integer languageCourseInJapanese_total {get; set;}
    public integer languageCourseInKorean_total {get; set;}
    public integer languageCourseInPortuguese_total {get; set;}
    public integer languageCourseInRussian_total {get; set;}
    public integer languageCourseInSpanish_total {get; set;}
    public integer furtherLanguagesIn_total {get; set;}
    public integer typeOfLanguageCourse_total {get; set;}
    public integer otherTypeOfLanguageCourse_total {get; set;}
    
    /*
    * Counts and Getter-Setters for Study Programmes 
    *
    */
    public String careerVocationalCertificateDiploma {get; set;}
    public String otherCareerVocationalCertificateDiploma {get; set;}
    public String undergraduateDegreeBachelor {get; set;}
    public String otherUndergraduateDegreeBachelor {get; set;}
    public String graduatePostGraduateCertificateDiploma {get; set;}
    public String otherGraduatePostGraduateCertificateDiploma {get; set;}
    public String graduatePostGraduateMasters {get; set;}
    public String otherGraduatePostGraduateMasters {get; set;}
    public String graduatePostGraduateDoctorate {get; set;}
    public String otherGraduatePostGraduateDoctorate {get; set;}
    
    public Integer careerVocationalCertificateDiploma_total {get; set;}
    public Integer otherCareerVocationalCertificateDiploma_total {get; set;}
    public Integer undergraduateDegreeBachelor_total {get; set;}
    public Integer otherUndergraduateDegreeBachelor_total {get; set;}
    public Integer graduatePostGraduateCertificateDiploma_total {get; set;}
    public Integer otherGraduatePostGraduateCertificateDiploma_total {get; set;}
    public Integer graduatePostGraduateMasters_total {get; set;}
    public Integer otherGraduatePostGraduateMasters_total {get; set;}
    public Integer graduatePostGraduateDoctorate_total {get; set;}
    public Integer otherGraduatePostGraduateDoctorate_total {get; set;}
    

    /*
    * Maps for Language Courses
    */
    public Map<String, Integer> langCoursesMap {get; set;}
    public Map<String, Integer> ArabicCountryCountMap {get; set;}
    public Map<String, Integer> ChineseCountryCountMap {get; set;}
    public Map<String, Integer> EnglishCountryCountMap {get; set;}
    public Map<String, Integer> FrenchCountryCountMap {get; set;}
    public Map<String, Integer> GermanCountryCountMap {get; set;}
    public Map<String, Integer> ItalianCountryCountMap {get; set;}
    public Map<String, Integer> JapaneseCountryCountMap {get; set;}
    public Map<String, Integer> KoreanCountryCountMap {get; set;}
    public Map<String, Integer> PortugueseCountryCountMap {get; set;}
    public Map<String, Integer> RussianCountryCountMap {get; set;}
    public Map<String, Integer> SpanishCountryCountMap {get; set;}
    public Map<String, Integer> TypeOfCourseCountMap {get; set;}
    public Map<String, Integer> FurtherLanguageCountMap {get; set;}
    public Map<String, Integer> OtherTypeOfLanguageCountMap {get; set;}
    
    /*
    * Maps for Study Programmes
    *
    */
    
    public Map<String, Integer> CareerVocationalCertificateDiplomaCountMap {get; set;}
    public Map<String, Integer> OtherCareerVocationalCertificateDiplomaCountMap {get; set;}
    public Map<String, Integer> UndergraduateDegreeBachelorCountMap {get; set;}
    public Map<String, Integer> OtherUndergraduateDegreeBachelorCountMap {get; set;}
    public Map<String, Integer> GraduatePostgraduateCertificateDiplomaCountMap {get; set;}
    public Map<String, Integer> OtherGraduatePostgradCertificateDiplomaCountMap {get; set;}
    public Map<String, Integer> GraduatePostgraduateMastersCountMap {get; set;}
    public Map<String, Integer> OtherGraduatePostgraduateMastersCountMap {get; set;}
    public Map<String, Integer> GraduatePostgraduateDoctorateCountMap {get; set;}
    public Map<String, Integer> OtherGraduatePostgraduateDoctorateCountMap {get; set;}
    
    /**
    *
    * Map for Secondary and High School Programmes
    */

    public Map<String, Integer> SecondaryHighSchoolProgMap {get; set;}
    
    /*
    * Map for Additional Programmes and Services
    *
    */
    
    public Map<String, Integer> AdditionalProgMap {get; set;}
    //public Map<String, Integer> newAdditionalProgMap {get; set;}
    //public Map<String, Integer> testMap {get; set;}
    
     public Map<Id, Course_Profile__c> courseProfilesMap {get; set;}
     
    /*
    *
    * Lists for storing the names of countries/courses to enable sorting
    */
    public List<String> testCountryList {get; set;}
    public List<String> listOfLangCourses {get; set;}
    public List<String> ArabicCountryList {get; set;}
    public List<String> ChineseCountryList {get; set;}
    public List<String> EnglishCountryList {get; set;}
    public List<String> FrenchCountryList {get; set;}
    public List<String> GermanCountryList {get; set;}
    public List<String> ItalianCountryList {get; set;}
    public List<String> JapaneseCountryList {get; set;}
    public List<String> KoreanCountryList {get; set;}
    public List<String> PortugueseCountryList {get; set;}
    public List<String> RussianCountryList {get; set;}
    public List<String> SpanishCountryList {get; set;}
    public List<String> TypeOfCourseList {get; set;}
    public List<String> FurtherLanguageList {get; set;}
    public List<String> OtherTypeOfLanguageList {get; set;}
    
    public List<String> SecondaryHighSchoolList {get; set;}
    
    public List<String> CareerVocationalCertificateDiplomaList {get; set;}
    public List<String> OtherCareerVocationalCertificateDiplomaList {get; set;}
    public List<String> UndergraduateDegreeBachelorList {get; set;}
    public List<String> OtherUndergraduateDegreeBachelorList {get; set;}
    public List<String> GraduatePostgraduateCertificateDiplomaList {get; set;}
    public List<String> OtherGraduatePostgradCertificateDiplomaList {get; set;}
    public List<String> GraduatePostgraduateMastersList {get; set;}
    public List<String> OtherGraduatePostgraduateMastersList {get; set;}
    public List<String> GraduatePostgraduateDoctorateList {get; set;}
    public List<String> OtherGraduatePostgraduateDoctorateList {get; set;}
    
    public List<String> AdditionalProgList {get; set;}
    
        
    /*
    * End of declarations
    *
    */
     
     
     //public List<ICEFAgentCourseProfilesControllerWrapper> testListWrapper {get; set;}
    public ICEFEducatorCourseProfilesController(){}   
    
    public ICEFEducatorCourseProfilesController(ApexPages.StandardController controller) 
    {
        ICEFEventId = ApexPages.currentPage().getParameters().get('id');      
          
    }
        
    public PageReference ViewData(){
        langCoursesMap = new Map<String, Integer>();
        langCoursesMap.put('str1', 1);
        langCoursesMap.put('str2', 2);        
    
        if(ICEFEventId == null){
            ICEFEventId = icefEventWithIdForTestMethod;
        }
        
        ICEF_Event__c event = [Select Name from ICEF_Event__c where Id =: ICEFEventId];
        eventName = event.Name;
        
        /**
        * Initialization of Language Courses Maps
        *
        */
        ArabicCountryCountMap = new Map<String, Integer>();
        ChineseCountryCountMap = new Map<String, Integer>();
        EnglishCountryCountMap = new Map<String, Integer>();
        FrenchCountryCountMap = new Map<String, Integer>();
        GermanCountryCountMap = new Map<String, Integer>();
        ItalianCountryCountMap = new Map<String, Integer>();
        JapaneseCountryCountMap = new Map<String, Integer>();
        KoreanCountryCountMap = new Map<String, Integer>();
        PortugueseCountryCountMap = new Map<String, Integer>();
        RussianCountryCountMap = new Map<String, Integer>();
        SpanishCountryCountMap = new Map<String, Integer>();
        TypeOfCourseCountMap = new Map<String, Integer>();
        FurtherLanguageCountMap = new Map<String, Integer>();
        OtherTypeOfLanguageCountMap = new Map<String, Integer>();
        
        /**
        *
        * Initialization of Study Programmes Maps
        */
        
        CareerVocationalCertificateDiplomaCountMap = new Map<String, Integer>();
        OtherCareerVocationalCertificateDiplomaCountMap = new Map<String, Integer>();
        UndergraduateDegreeBachelorCountMap = new Map<String, Integer>();
        OtherUndergraduateDegreeBachelorCountMap = new Map<String, Integer>();
        GraduatePostgraduateCertificateDiplomaCountMap = new Map<String, Integer>();
        OtherGraduatePostgradCertificateDiplomaCountMap = new Map<String, Integer>();
        GraduatePostgraduateMastersCountMap = new Map<String, Integer>();
        OtherGraduatePostgraduateMastersCountMap = new Map<String, Integer>();
        GraduatePostgraduateDoctorateCountMap = new Map<String, Integer>();
        OtherGraduatePostgraduateDoctorateCountMap = new Map<String, Integer>();
        
        /*
        *
        * Initialization of Secondary School Programmes Map
        */
        SecondaryHighSchoolProgMap = new Map<String, Integer>();
        
        /*
        *
        * Initialization of Additional Programmes Map
        */
        
        AdditionalProgMap = new Map<String, Integer>();
        
        
        courseProfilesMap  = new Map<Id, Course_Profile__c>();
        
        //newAdditionalProgMap = new Map<String, Integer>();
        //testMap = new Map<String, Integer>();
        //testListWrapper = new List<ICEFAgentCourseProfilesControllerWrapper>();
        
        /**
        *
        * Initialization of countries List
        */
        testCountryList = new List<String>();
        listOfLangCourses = new List<String>();
        
        ArabicCountryList = new List<String>();
        ChineseCountryList = new List<String>();
        EnglishCountryList = new List<String>();
        FrenchCountryList = new List<String>();
        GermanCountryList = new List<String>();
        ItalianCountryList = new List<String>();
        JapaneseCountryList = new List<String>();
        KoreanCountryList = new List<String>();
        PortugueseCountryList = new List<String>();
        RussianCountryList = new List<String>();
        SpanishCountryList = new List<String>();
        TypeOfCourseList = new List<String>();
        FurtherLanguageList = new List<String>();
        OtherTypeOfLanguageList = new List<String>();
         
        SecondaryHighSchoolList = new List<String>();
         
        CareerVocationalCertificateDiplomaList = new List<String>();
        OtherCareerVocationalCertificateDiplomaList = new List<String>();
        UndergraduateDegreeBachelorList = new List<String>();
        OtherUndergraduateDegreeBachelorList = new List<String>();
        GraduatePostgraduateCertificateDiplomaList = new List<String>();
        OtherGraduatePostgradCertificateDiplomaList = new List<String>();
        GraduatePostgraduateMastersList = new List<String>();
        OtherGraduatePostgraduateMastersList = new List<String>();
        GraduatePostgraduateDoctorateList = new List<String>();
        OtherGraduatePostgraduateDoctorateList = new List<String>();
         
        AdditionalProgList = new List<String>();
        
        
        /*
        *
        *  End Initializations
        */
        Id rtid = null;
        
        List<RecordType> rt = [SELECT Id,Name FROM RecordType WHERE sobjecttype =: 'Account_Participation__c' and Name =: 'Educator'];  
        if(rt.size() != null)
        {
            rtid = rt[0].Id;
        }
        List<Account_Participation__c> accountParticipants = [Select Id, Name FROM Account_Participation__c WHERE ICEF_Event__c =: ICEFEventId AND (Participation_Status__c =: 'registered' OR Participation_Status__c =: 'pending payment') AND RecordTypeId =: rtid];
        Set<Id> acctPartId = new Set<Id>();
        
        for(Account_Participation__c ap: accountParticipants)
        {
            acctPartId.add(ap.Id);
        }
        
        //System.debug('AP: ' + accountParticipants);
                  
        languageCourses_total = 0;
        secHighSchoolPrograms_total = 0;
        studyPrograms_total = 0;
        addProgramsAndServices_total = 0;
        languageCourseInArabic_total = 0;
        languageCourseInChinese_total =  0;
        languageCourseInEnglish_total =  0;
        languageCourseInFrench_total =  0;
        languageCourseInGerman_total =  0;
        languageCourseInItalian_total =  0;
        languageCourseInJapanese_total =  0;
        languageCourseInKorean_total =  0;
        languageCourseInPortuguese_total =  0;
        languageCourseInRussian_total =  0;
        languageCourseInSpanish_total =  0;
        furtherLanguagesIn_total = 0;
        typeOfLanguageCourse_total = 0;
        otherTypeOfLanguageCourse_total = 0;

        
        //Study Programmes variable initializations
        careerVocationalCertificateDiploma_total = 0;
        otherCareerVocationalCertificateDiploma_total = 0;
        undergraduateDegreeBachelor_total = 0;
        otherUndergraduateDegreeBachelor_total = 0;
        graduatePostGraduateCertificateDiploma_total = 0;
        otherGraduatePostGraduateCertificateDiploma_total = 0;
        graduatePostGraduateMasters_total = 0;
        otherGraduatePostGraduateMasters_total = 0;
        graduatePostGraduateDoctorate_total = 0;
        otherGraduatePostGraduateDoctorate_total = 0;

        courseProfilesMap.putAll([Select Id, Name, Language_Arabic_in__c, Language_Chinese_in__c, Language_English_in__c, Language_French_in__c, Language_German_in__c, Language_Italian_in__c, Language_Japanese_in__c, Language_Korean_in__c, Language_Portuguese_in__c, Language_Russian_in__c, Language_Spanish_in__c, Language_Other_in__c, Type_of_Language_Course__c, Other_types_of_Language_Course__c, Secondary_and_high_school_programmes__c, Other_secondary_and_high_school_programm__c, Career_Vocational_Certificate_Diploma__c, Other_Career_VocationalCertificate_Dipl__c, Undergraduate_Degree_Bachelor__c, Other_Undergraduate_Degree_Bachelor__c, Graduate_PostgraduateCertificate_Diploma__c, Other_Graduate_PostgradCertificate_Dipl__c, Graduate_Postgraduate_Masters__c, Other_Graduate_Postgraduate_Masters__c, Graduate_Postgraduate_Doctorate__c, Other_Graduate_Postgraduate_Doctorate__c, Additional_programmes_and_services__c, Other_additional_programmes_and_services__c  from Course_Profile__c where Account_Participation__c in :acctPartId]);    
        //List<Course_Profile__c> courseprofiles = new List<Course_Profile__c>();
        //System.debug('courseProfilesMap::'+courseProfilesMap);
        for(Id cId: courseProfilesMap.keySet())
               {
             //List<Course_Profile__c> courseProfiles = [Select Id, Name, Language_Arabic_in__c, Language_Chinese_in__c, Language_English_in__c, Language_French_in__c, Language_German_in__c, Language_Italian_in__c, Language_Japanese_in__c, Language_Korean_in__c, Language_Portuguese_in__c, Language_Russian_in__c, Language_Spanish_in__c, Language_Other_in__c, Type_of_Language_Course__c, Other_types_of_Language_Course__c, Secondary_and_high_school_programmes__c, Other_secondary_and_high_school_programm__c, Career_Vocational_Certificate_Diploma__c, Other_Career_VocationalCertificate_Dipl__c, Undergraduate_Degree_Bachelor__c, Other_Undergraduate_Degree_Bachelor__c, Graduate_PostgraduateCertificate_Diploma__c, Other_Graduate_PostgradCertificate_Dipl__c, Graduate_Postgraduate_Masters__c, Other_Graduate_Postgraduate_Masters__c, Graduate_Postgraduate_Doctorate__c, Other_Graduate_Postgraduate_Doctorate__c, Additional_programmes_and_services__c, Other_additional_programmes_and_services__c  from Course_Profile__c where Account_Participation__c =: acctPart.Id];    
             
             //Need to add course profiles in Map before looping them
             
            Course_Profile__c  cp = courseProfilesMap.get(cId);
             System.debug('cp::::'+cp.Career_Vocational_Certificate_Diploma__c +', '+ cp.Other_Career_VocationalCertificate_Dipl__c +', '+cp.Undergraduate_Degree_Bachelor__c +', '+ cp.Other_Undergraduate_Degree_Bachelor__c +', '+ cp.Graduate_PostgraduateCertificate_Diploma__c +', '+  cp.Other_Graduate_PostgradCertificate_Dipl__c +', '+ cp.Graduate_Postgraduate_Masters__c +', '+ cp.Other_Graduate_Postgraduate_Masters__c +', '+ cp.Graduate_Postgraduate_Doctorate__c +', '+ cp.Other_Graduate_Postgraduate_Doctorate__c);
            // for(Course_Profile__c cp: courseprofiles)
             //{
                 //System.debug('cp::'+cp);
                 //System.debug('cp.Language_Arabic_in__c::'+cp.Language_Arabic_in__c);
                 //System.debug('cp.Other_types_of_Language_Course__c::'+cp.Other_types_of_Language_Course__c);
                 if(cp.Language_Arabic_in__c != null || cp.Language_Chinese_in__c!= null || cp.Language_English_in__c!= null || cp.Language_French_in__c!= null || cp.Language_Italian_in__c!= null || cp.Language_Japanese_in__c!= null || cp.Language_Korean_in__c!= null || cp.Language_Portuguese_in__c!= null || cp.Language_Russian_in__c!= null || cp.Language_Spanish_in__c!= null || cp.Language_Other_in__c!= null || cp.Type_of_Language_Course__c!= null || cp.Other_types_of_Language_Course__c != null)
                 {
                     languageCourses_total++;
                     //System.debug('languageCourses_total::'+languageCourses_total);
                 }
                 if(cp.Secondary_and_high_school_programmes__c != null || cp.Other_secondary_and_high_school_programm__c != null)
                 {
                     secHighSchoolPrograms_total++;
                     //System.debug('secHighSchoolPrograms_total:'+secHighSchoolPrograms_total+' cp.Other_secondary_and_high_school_programm__c:'+cp.Other_secondary_and_high_school_programm__c);
                 }
                 if(cp.Career_Vocational_Certificate_Diploma__c != null || cp.Other_Career_VocationalCertificate_Dipl__c != null ||  cp.Undergraduate_Degree_Bachelor__c!= null ||  cp.Other_Undergraduate_Degree_Bachelor__c!= null ||  cp.Graduate_PostgraduateCertificate_Diploma__c!= null ||  cp.Other_Graduate_PostgradCertificate_Dipl__c!= null ||  cp.Graduate_Postgraduate_Masters__c!= null ||  cp.Other_Graduate_Postgraduate_Masters__c!= null ||  cp.Graduate_Postgraduate_Doctorate__c!= null ||  cp.Other_Graduate_Postgraduate_Doctorate__c!= null)
                 {
                     studyPrograms_total++;
                 }
                 if(cp.Additional_programmes_and_services__c!= null || cp.Other_additional_programmes_and_services__c != null)
                 {
                     addProgramsAndServices_total++;
                 }
                 
                /*
                 * Language Courses values being set here.
                 *
                 */
                 
                 if(cp.Language_Arabic_in__c != null)
                 {
                     languageCourseInArabic_total++;
                     getCountAndSetCountInMap(cp.Language_Arabic_in__c, ArabicCountryCountMap);
                     
                     
                 }
                 if(cp.Language_Chinese_in__c != null){
                     languageCourseInChinese_total++;
                     getCountAndSetCountInMap(cp.Language_Chinese_in__c, ChineseCountryCountMap);
                    
                 }
                 if(cp.Language_English_in__c != null){
                     languageCourseInEnglish_total++;
                     getCountAndSetCountInMap(cp.Language_English_in__c, EnglishCountryCountMap);
                     
                 }
                 if(cp.Language_French_in__c != null){
                     languageCourseInFrench_total++;
                     getCountAndSetCountInMap(cp.Language_French_in__c, FrenchCountryCountMap);
                     
                 }
                 if(cp.Language_German_in__c != null){
                     languageCourseInGerman_total++;
                     getCountAndSetCountInMap(cp.Language_German_in__c, GermanCountryCountMap);
                     
                 }
                 if(cp.Language_Italian_in__c != null){
                     languageCourseInItalian_total++;
                     getCountAndSetCountInMap(cp.Language_Italian_in__c, ItalianCountryCountMap);
                     
                 }
                 if(cp.Language_Japanese_in__c != null){
                     languageCourseInJapanese_total++;
                     getCountAndSetCountInMap(cp.Language_Japanese_in__c, JapaneseCountryCountMap);
                     
                 }
                 if(cp.Language_Korean_in__c != null){
                     languageCourseInKorean_total++;
                     getCountAndSetCountInMap(cp.Language_Korean_in__c, KoreanCountryCountMap);
                     
                 }
                 if(cp.Language_Portuguese_in__c != null){
                     languageCourseInPortuguese_total++;
                     getCountAndSetCountInMap(cp.Language_Portuguese_in__c, PortugueseCountryCountMap);
                     
                 }
                 if(cp.Language_Russian_in__c != null){
                     languageCourseInRussian_total++;
                     getCountAndSetCountInMap(cp.Language_Russian_in__c, RussianCountryCountMap);
                     
                 }                 
                 if(cp.Language_Spanish_in__c != null){
                     languageCourseInSpanish_total++;
                     getCountAndSetCountInMap(cp.Language_Spanish_in__c, SpanishCountryCountMap);
                     
                 }
                 if(cp.Type_of_Language_Course__c != null){
                     typeOfLanguageCourse_total++;
                     getCountAndSetCountInMap(cp.Type_of_Language_Course__c, TypeOfCourseCountMap);
                     
                 }
                 if(cp.Language_Other_in__c != null){
                     furtherLanguagesIn_total++;
                     getCountAndSetCountInMap(cp.Language_Other_in__c, FurtherLanguageCountMap);
                     
                 }
                 if(cp.Other_types_of_Language_Course__c != null){
                     //otherTypeOfLanguageCourse_total++;
                     //typeOfLanguageCourse_total++;
                     getCountAndSetCountInMap(cp.Other_types_of_Language_Course__c, TypeOfCourseCountMap);
                     
                 }
                 
                 /**
                 * Secondary and High School Programmes
                 *
                 */
                 
                 if(cp.Secondary_and_high_school_programmes__c != null){
                     getCountAndSetCountInMap(cp.Secondary_and_high_school_programmes__c , SecondaryHighSchoolProgMap);
                  }
                                    
                  if(cp.Other_secondary_and_high_school_programm__c != null){
                      getCountAndSetCountInMap(cp.Other_secondary_and_high_school_programm__c , SecondaryHighSchoolProgMap);
                  }
                  
                  /*
                 *
                 *Study Programmes variable initializations
                 */                                
                 

                 if(cp.Career_Vocational_Certificate_Diploma__c != null || cp.Other_Career_VocationalCertificate_Dipl__c != null){
                     careerVocationalCertificateDiploma_total++;
                     if(cp.Career_Vocational_Certificate_Diploma__c != null)
                     {
                         getCountAndSetCountInMap(cp.Career_Vocational_Certificate_Diploma__c, CareerVocationalCertificateDiplomaCountMap);
                     }
                     if(cp.Other_Career_VocationalCertificate_Dipl__c != null)
                     {
                         getCountAndSetCountInMap(cp.Other_Career_VocationalCertificate_Dipl__c, CareerVocationalCertificateDiplomaCountMap);
                     }
                     
                 }
                 
                 if(cp.Undergraduate_Degree_Bachelor__c != null || cp.Other_Undergraduate_Degree_Bachelor__c != null)
                 {
                     undergraduateDegreeBachelor_total++;
                     if(cp.Undergraduate_Degree_Bachelor__c != null)
                     {
                         getCountAndSetCountInMap(cp.Undergraduate_Degree_Bachelor__c,UndergraduateDegreeBachelorCountMap);
                     }
                     if(cp.Other_Undergraduate_Degree_Bachelor__c != null)
                     {                     
                         getCountAndSetCountInMap(cp.Other_Undergraduate_Degree_Bachelor__c,UndergraduateDegreeBachelorCountMap);
                     }
                 }
                 
                 if(cp.Graduate_PostgraduateCertificate_Diploma__c != null || cp.Other_Graduate_PostgradCertificate_Dipl__c != null){
                     graduatePostGraduateCertificateDiploma_total++;
                     if(cp.Graduate_PostgraduateCertificate_Diploma__c != null){
                         getCountAndSetCountInMap(cp.Graduate_PostgraduateCertificate_Diploma__c,GraduatePostgraduateCertificateDiplomaCountMap);
                     }
                     if(cp.Other_Graduate_PostgradCertificate_Dipl__c != null){                     
                         getCountAndSetCountInMap(cp.Other_Graduate_PostgradCertificate_Dipl__c, GraduatePostgraduateCertificateDiplomaCountMap);
                     }
                 }
                 
                 if(cp.Graduate_Postgraduate_Masters__c != null || cp.Other_Graduate_Postgraduate_Masters__c != null){
                     graduatePostGraduateMasters_total++;
                     if(cp.Graduate_Postgraduate_Masters__c != null)
                     {
                         getCountAndSetCountInMap(cp.Graduate_Postgraduate_Masters__c, GraduatePostgraduateMastersCountMap);
                     }
                     if(cp.Other_Graduate_Postgraduate_Masters__c != null){                     
                         getCountAndSetCountInMap(cp.Other_Graduate_Postgraduate_Masters__c, GraduatePostgraduateMastersCountMap);
                     }
                 }
                 
                 if(cp.Graduate_Postgraduate_Doctorate__c != null || cp.Other_Graduate_Postgraduate_Doctorate__c != null){
                     graduatePostGraduateDoctorate_total++;
                     if(cp.Graduate_Postgraduate_Doctorate__c != null){
                         getCountAndSetCountInMap(cp.Graduate_Postgraduate_Doctorate__c, GraduatePostgraduateDoctorateCountMap);
                     }
                     if(cp.Other_Graduate_Postgraduate_Doctorate__c != null){
                         getCountAndSetCountInMap(cp.Other_Graduate_Postgraduate_Doctorate__c, GraduatePostgraduateDoctorateCountMap);
                     }
                 }
                 
                 
                  /*
                 *
                 * Additional Programs and Services
                 */
                
                 
                 if(cp.Additional_programmes_and_services__c != null){
                       getCountAndSetCountInMap(cp.Additional_programmes_and_services__c, AdditionalProgMap);
                 }                 
                 
                 if(cp.Other_additional_programmes_and_services__c !=null){
                        getCountAndSetCountInMap(cp.Other_additional_programmes_and_services__c, AdditionalProgMap);
                 }
             //}
         }
         
         //testCountryList.add('argentina#2');
         //testCountryList.add('armenia#4');
         
        createSortList(ArabicCountryCountMap, ArabicCountryList);
        createSortList(ArabicCountryCountMap, testCountryList);
        createSortList(ChineseCountryCountMap, ChineseCountryList);
        createSortList(EnglishCountryCountMap, EnglishCountryList);
        createSortList(FrenchCountryCountMap, FrenchCountryList);
        createSortList(GermanCountryCountMap, GermanCountryList);
        createSortList(ItalianCountryCountMap, ItalianCountryList);
        createSortList(JapaneseCountryCountMap, JapaneseCountryList);
        createSortList(PortugueseCountryCountMap, PortugueseCountryList);
        createSortList(KoreanCountryCountMap, KoreanCountryList);
        createSortList(RussianCountryCountMap, RussianCountryList);
        createSortList(SpanishCountryCountMap, SpanishCountryList);
        createSortList(FurtherLanguageCountMap, FurtherLanguageList);
        createSortList(TypeOfCourseCountMap, TypeOfCourseList);
        
        createSortList(SecondaryHighSchoolProgMap, SecondaryHighSchoolList);
        
        createSortList(CareerVocationalCertificateDiplomaCountMap, CareerVocationalCertificateDiplomaList);
        createSortList(OtherCareerVocationalCertificateDiplomaCountMap, OtherCareerVocationalCertificateDiplomaList);
        createSortList(UndergraduateDegreeBachelorCountMap, UndergraduateDegreeBachelorList);
        createSortList(OtherUndergraduateDegreeBachelorCountMap,  OtherUndergraduateDegreeBachelorList);
        createSortList(GraduatePostgraduateCertificateDiplomaCountMap, GraduatePostgraduateCertificateDiplomaList);
        createSortList(OtherGraduatePostgradCertificateDiplomaCountMap, OtherGraduatePostgradCertificateDiplomaList);
        createSortList(GraduatePostgraduateMastersCountMap, GraduatePostgraduateMastersList);
        createSortList(OtherGraduatePostgraduateMastersCountMap, OtherGraduatePostgraduateMastersList);
        createSortList(GraduatePostgraduateDoctorateCountMap, GraduatePostgraduateDoctorateList);
        createSortList(OtherGraduatePostgraduateDoctorateCountMap, OtherGraduatePostgraduateDoctorateList);
        
        createSortList(AdditionalProgMap, AdditionalProgList);
        
        langCourseTitle = 'Language Courses';
        secHighSchoolPrograms = 'Secondary and High School Programmes';
        studyPrograms = 'Study Programmes';
        addProgramsAndServices = 'Additional Programmes and Services'; 
        
        languageCourseInArabic = Course_Profile__c.Language_Arabic_in__c.getDescribe().getLabel();
        languageCourseInChinese = Course_Profile__c.Language_Chinese_in__c.getDescribe().getLabel();
        languageCourseInEnglish = Course_Profile__c.Language_English_in__c.getDescribe().getLabel();
        languageCourseInFrench = Course_Profile__c.Language_French_in__c.getDescribe().getLabel();
        languageCourseInGerman = Course_Profile__c.Language_German_in__c.getDescribe().getLabel();
        languageCourseInItalian = Course_Profile__c.Language_Italian_in__c.getDescribe().getLabel();
        languageCourseInJapanese = Course_Profile__c.Language_Japanese_in__c.getDescribe().getLabel();
        languageCourseInKorean = Course_Profile__c.Language_Korean_in__c.getDescribe().getLabel();
        languageCourseInPortuguese = Course_Profile__c.Language_Portuguese_in__c.getDescribe().getLabel();
        languageCourseInRussian = Course_Profile__c.Language_Russian_in__c.getDescribe().getLabel();
        languageCourseInSpanish = Course_Profile__c.Language_Spanish_in__c.getDescribe().getLabel();
        furtherLanguagesIn = Course_Profile__c.Language_Other_in__c.getDescribe().getLabel();
        typeOfLanguageCourse = Course_Profile__c.Type_of_Language_Course__c.getDescribe().getLabel();
        otherTypeOfLanguageCourse = Course_Profile__c.Other_types_of_Language_Course__c.getDescribe().getLabel();

        careerVocationalCertificateDiploma = Course_Profile__c.Career_Vocational_Certificate_Diploma__c.getDescribe().getLabel();
        undergraduateDegreeBachelor = Course_Profile__c.Undergraduate_Degree_Bachelor__c.getDescribe().getLabel();
        graduatePostGraduateCertificateDiploma = Course_Profile__c.Graduate_PostgraduateCertificate_Diploma__c.getDescribe().getLabel();
        graduatePostGraduateMasters = Course_Profile__c.Graduate_Postgraduate_Masters__c.getDescribe().getLabel();
        graduatePostGraduateDoctorate = Course_Profile__c.Graduate_Postgraduate_Doctorate__c.getDescribe().getLabel();

        
        return null;
    }
    
    public Integer getCount(String value){
         List<string> listOfTest = value.split(';');
         return listOfTest.size();
    }
    
    public void getCountAndSetCountInMap(String stringToBeCounted, Map<String, Integer> valueMap){
            String[] arrayOfSplitValues = stringToBeCounted.split(';');
                for(integer i = 0; i < arrayOfSplitValues.size(); i++){
                     Integer count = valueMap.get(arrayOfSplitValues[i]);

                     if(count == null)
                           count = 1;                                     
                     else
                           count++;                                          
                     valueMap.put(arrayOfSplitValues[i], count);
                }
                //valueMap = sortMap(valueMap);
    } 
    
    public void createSortList(Map<String, Integer> valueMap, List<String> listToCreate){
         
         List<String> tempList = new List<String>();
         tempList.addAll(valueMap.keySet()); 
         tempList.sort();
         
         for(String fieldName : tempList){
             listToCreate.add(fieldName + '#'+ valueMap.get(fieldName));
            
         }

    }    
        
    public Map<String, Integer> sortMap(Map <String, Integer> someMap){
        
        Map<String, Integer> newMap = new Map<String, Integer>();
         
        List<String> aList = new List<String>();
        aList.addAll(someMap.keySet());
        aList.sort();
        //System.debug('aList::'+aList);

        for(String a: aList){
             Integer value = someMap.get(a);
             newMap.put(a, value);
        }
        return newMap;
    }
    
    @IsTest(SeeAllData=true) 
    public static void testICEFAgentCourseProfilesController(){
    
    Country__c ctry = new Country__c(Name = 'China', Region__c = 'Asia');
    insert ctry;
    
    Account acc = new Account(Name = 'Test Account11', Status__c = 'Active');
    insert acc;
    
    date dueDate = date.newInstance(2014, 1, 30);
    ICEF_Event__c icefEvent = new ICEF_Event__c(Name='Test IcefEvent11', Event_City__c = 'City', technical_event_name__c = 'TestingEvent', Event_Manager_Educators__c = UserInfo.getUserId(), Event_Manager_Agents__c = UserInfo.getUserId(), Start_date__c=dueDate);
    insert icefEvent;
    
    ID rt_id1 = null;
    List<RecordType> rt1 = [SELECT Id,Name FROM RecordType WHERE Name = 'Educator' and SobjectType='Account_Participation__c'];
    if(rt1.size() != null)
             rt_id1 = rt1[0].Id;         
             
    Account_Participation__c ap = new Account_Participation__c(Account__c = acc.Id, ICEF_Event__c = icefEvent.Id, Participation_Status__c = 'pending payment', Country_Section__c = ctry.id, Catalogue_Country__c = ctry.id, RecordTypeID = rt_id1);  
    insert ap;
    
    Course_Profile__c cp = new Course_Profile__c(Account__c = acc.id, ICEF_Event__c = icefEvent.id, Main_sector__c = '0 u-n-k-n-o-w-n', Language_Arabic_in__c = 'Armenia');
    insert cp;
    
    
    ICEFEducatorCourseProfilesController icefPC = new ICEFEducatorCourseProfilesController(new ApexPages.StandardController(icefEvent));
    icefPC.icefEventWithIdForTestMethod = icefEvent.Id;
    
    update ap; 
    Test.startTest();
    //System.debug('Test Started');
    
    icefPC.ViewData();
    
    string testString = 'India;Armenia';
    Map<String, Integer> testMap= new Map<String, Integer>();
    List<String> testList = new List<String>();
    
    icefPc.getCount(testString);
    icefPc.getCountAndSetCountInMap(testString, testMap);
    icefPc.sortMap(testMap);
    icefPc.createSortList(testMap, testList);
    
    //icefPC.ViewData();
    
    Test.stopTest();
    
    }
}