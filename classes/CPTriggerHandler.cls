/**
 *  @Purpose Handler for CPTrigger
 *       update AP on change of CP
 *         - update number of valid CPs per AP
 *         - update number of Accommodations needed
 *         - update number of ESP appointments 
 *         - update field Persons attending (Contacts__c)(removed 30.10.18 due to fail of Marcom Salesforce sync)
 *  @Date 12 Sept 2017
 *   @Modified by Henriette to add update of Persons Attending - 23 Feb 2018
 */
public class CPTriggerHandler {

    /*
     *   @Purpose   : update AP from CP
     *   @Param   : listOfCP
     */ 
    public static void updateAPfromCP(List<Contact_Participation__c> listOfCP){
        
        system.debug('handler called '+listOfCP.size());
        Set<Id> setOfAPIds = new Set<Id>();
        
        //collect parent APs
        for (Contact_Participation__c recordOfCP : listOfCP) {
            setOfAPIds.add(recordOfCP.Account_Participation__c);
        }
        
        Map<Id,Contact_Participation__c> mapOfCP = new Map<Id,Contact_Participation__c>();
        Map<Id,List<Contact_Participation__c>> mapOfAPIdToCPList = new Map<Id,List<Contact_Participation__c>>();
    
        // get all CPs related to ap
        mapOfCP.putAll(fetchValidCPs(setOfAPIds));
        
        // populate Map Of AP Id To CP List
        mapOfAPIdToCPList = populateMapOfAPIdToCPList(mapOfCP,setOfAPIds);
        //system.debug('mapOfAPIdToCPList  '+mapOfAPIdToCPList );
        // populate mapOfAP
        List<Account_Participation__c> ListOfAPToUpdate = createListOfAPToUpdate(mapOfAPIdToCPList);
        if(ListOfAPToUpdate.size() > 0){
            update ListOfAPToUpdate;
            //system.debug('AP updated ');
        }
    }
    
    /*
     *   @Purpose   : fetch CP records
     *   @Param   : setOfAPIds
     *   @return   : mapOfCP
     */
    private static Map<Id, Contact_Participation__c> fetchValidCPs(Set<Id> setOfAPIds){
        
        set<String> setOfStatus = new set<String>{'registered','Guest','participating','Speaker'};
        
        return new Map<Id, Contact_Participation__c>([SELECT Id, Account_Participation__c, Participation_Status__c, 
                                                             espstats_number_of_appointment__c, No_Hotel_Room_needed_for_this_Contact__c
                                                      FROM Contact_Participation__c 
                                                      WHERE Account_Participation__c IN :setOfAPIds 
                                                      AND Participation_Status__c IN :setOfStatus
                                                      ]);
    }
    
    /*
     *   @Purpose   : Build map APId To CPList
     *   @Param   : mapOfCP, setOfAPIds
     *   @return   : mapOfAPIdToCPList
     */
    private static Map<Id,List<Contact_Participation__c>> populateMapOfAPIdToCPList(Map<Id,Contact_Participation__c> mapOfCP,
                                                                                    set<Id> setOfAPIds)
    {         
        Map<Id,List<Contact_Participation__c>> mapOfAPIdToCPList = new Map<Id,List<Contact_Participation__c>>();
        
        for(ID accountPartId :setOfAPIds){
            if(!mapOfAPIdToCPList.containsKey(accountPartId)){
                mapOfAPIdToCPList.put(accountPartId, new List<Contact_Participation__c>());
            }
        }        
        for(Contact_Participation__c  contactPart : mapOfCP.values()){
            
            Id accountPartId = contactPart.Account_Participation__c;
            mapOfAPIdToCPList.get(accountPartId).add(contactPart);                                 
        }
        return mapOfAPIdToCPList;
    }
    
    /*
     *   @Purpose   : Build AP Record with updated values
     *   @Param   : mapOfAPIdToCPList
     *   @return   : listOfAP
     */
    private static  List<Account_Participation__c> createListOfAPToUpdate(Map<Id,List<Contact_Participation__c>> mapOfAPIdToCPList){
        List<Account_Participation__c> listOfAP = new List<Account_Participation__c>();
        Account_Participation__c recordOfAP;
        Decimal numberOfAppointments;
        integer numberOfCPsHavingNoHotelRoomSelected;
//        List<String> personsAttendingList;
//        String personsAttending;
        for(ID idOfAPRecord: mapOfAPIdToCPList.KeySet())
        {
            recordOfAP = new Account_Participation__c(Id = idOfAPRecord);
            numberOfAppointments = 0;
            numberOfCPsHavingNoHotelRoomSelected = 0;
//            personsAttendingList = new List<String>();
//            personsAttending = '';
            List<Contact_Participation__c> ListOfCPs = mapOfAPIdToCPList.get(idOfAPRecord);
            
            for(Contact_Participation__c CPrecord : ListOfCPs){
                if( ! CPrecord.No_Hotel_Room_needed_for_this_Contact__c){
                    numberOfCPsHavingNoHotelRoomSelected += 1;
                }
                if(CPrecord.espstats_number_of_appointment__c != null) {
                   numberOfAppointments += CPrecord.espstats_number_of_appointment__c; 
                }
//                personsAttendingList.add(CPRecord.Contact__r.Salutation + ' ' + CPRecord.Contact__r.Name);
            }
//            personsAttending = String.join(personsAttendingList, '\n'); // with newline: \n');
//            if(personsAttending.length() > 255){
//                personsAttending = PersonsAttending.left(255);
//            }
            // set values for AP record
            recordOfAP.Number_of_CPs_registered__c = ListOfCPs.size();
            recordOfAP.Number_of_required_Accommodations__c = numberOfCPsHavingNoHotelRoomSelected;            
            recordOfAP.Number_of_Appointments__c = numberOfAppointments;
//            recordOfAP.Contacts__c = personsAttending; 
            // add the value in the map to a list so we can update it
            listOfAP.add(recordOfAP);
        }
        return listOfAP;
    }
}