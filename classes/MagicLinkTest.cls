@isTest
public class MagicLinkTest {
	
    private static testMethod void testMagicLinknewContacts(){

        Integer numConts = 30;
        
	    List<Account> accts = DataFactory.createTestAgents(1);
        insert accts;
        
        Account acct = [SELECT Id, Name, RecordTypeId FROM Account WHERE Name LIKE 'Test%'][0];
	    List<Contact> contacts = DataFactory.createTestContacts(acct, numConts);
		
        insert contacts;        
        
    	Map<Id, Contact> contactMap = new Map<Id, Contact>(contacts);
    
    	Set<Id> contactIds = contactMap.keySet();
    	List<Id> conIdList = new List<Id>(contactIds);

        System.assertEquals(numConts, [SELECT Id FROM Contact WHERE magic_link__c = null].size());
        
        Test.startTest();
        MagicLinkAction.generateMagicLink(conIdList);
        Test.stopTest();
        
        //all newly created Contacts should contain a magic link.
        System.assertEquals(0, [SELECT Id FROM Contact WHERE magic_link__c = null].size());  
        
    }
        //test if fired if magic_link_updated__c is older than 2 secs
    private static testMethod void testMagicLinkOldLink(){
        integer numConts = 1;
	    List<Account> accts = DataFactory.createTestAgents(1);
        insert accts;
        
        Account acct = [SELECT Id, Name, RecordTypeId FROM Account WHERE Name LIKE 'Test%'][0];
	    List<Contact> contacts = DataFactory.createTestContacts(acct, numConts);
		
        insert contacts;

        Contact con = [SELECT Id, magic_link_updated__c FROM Contact][0];
        DateTime testTime = DateTime.now()-2;
        con.magic_link_updated__c = testTime;
        List<id> conList = new List<id>();
        conList.add(con.Id);
        
        Test.startTest();
        MagicLinkAction.generateMagicLink(conList);
        Test.stopTest();
        
        Contact testContact = [SELECT Id, magic_link_updated__c FROM Contact WHERE Id = :con.Id][0];
        
        //magic link should not be updated with testTime
        System.assertNotEquals(testTime, testContact.magic_link_updated__c);
    }

    
}