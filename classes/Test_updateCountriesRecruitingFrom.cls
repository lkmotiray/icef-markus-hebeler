/*
    @ Created By : Shaikh Saquib
    @ Date       : 21-10-2014
    @ Description: Test class for 'updateCountriesRecruitingFrom' trigger on Account Participation.
    */

    @isTest
    class Test_updateCountriesRecruitingFrom 
    {
        static testmethod void test_Method()
        {
            // List of Account Participation
            List<Account_Participation__c> accntPartList = new List<Account_Participation__c>();
            try
            {
                // Create new Account record
                Account testAccount = new Account(Name='TestAccount');
                insert testAccount;
                
                // Create new ICEF Event record
                ICEF_Event__c testEvent = new ICEF_Event__c( Name='testICEF',
                                                            Event_City__c = 'testEventCity',
                                                            technical_event_name__c = 'testEventTechinal', 
                                                            Event_Full_Name__c='TestFullEvent' );
                insert testEvent;
                
                // Create new Country record
                Country__c testCountry = new Country__c( Name = 'testCountry');
                insert testCountry;
                
                // Create  or insert Account Participation records
                for(integer count = 0 ; count < 210; count++) 
                {
                    accntPartList.add(new Account_Participation__c ( Account__c = testAccount.Id,
                                                                    ICEF_Event__c = testEvent.Id,
                                                                    Participation_Status__c = 'reconfirmed',
                                                                    Country_Section__c = testCountry.Id,
                                                                    Catalogue_Country__c = testCountry.Id,
                                                                    Countries_Recruiting_From_text__c = '',
                                                                    //Countries_recruiting_from__c = 'test1;test2;test3;test4')
                                                                    Countries_recruiting_from__c = 'test1;test2;test3;test4;test11;test12;test13;test14;test21;test22;test23;test24;test31;test32;test33;test34;test41;test42;test43;test44;test51;test52;test53;test54;test61;test62;test63;test64;test71;test72;test73;test74;test81;test82;test83;test84;test91;test92;test93;test94')
                                     );
                }
                
                Test.startTest();
                
                insert accntPartList;
                
                // Get all records of Account Participation
                List<Account_Participation__c> testAccntPartList;
                testAccntPartList = new List<Account_Participation__c>([SELECT Countries_Recruiting_From_text__c 
                                                                        FROM Account_Participation__c]);
                String strCountriesrecruitingFrom;
                // Check 'Countries Recruiting From' text field generated correctly for Insertion
                for(Account_Participation__c accntPartRecord : testAccntPartList) 
                {
                    strCountriesrecruitingFrom = accntPartRecord.Countries_Recruiting_From_text__c;
                    //System.assertEquals('test1, test2, test3, test4', strCountriesrecruitingFrom);
                    System.assertEquals('test1, test2, test3, test4, test11, test12, test13, test14, test21, test22, test23, test24, test31, test32, test33, test34, test41, test42, test43, test44, test51, test52, test53, test54, test61, test62, test63, test64, test71, test72, test73, test74, tes', strCountriesrecruitingFrom);
                }  
                
                // Change value of 'Countries Recruiting From' multi picklist value.
                for(Account_Participation__c accntPartRecord : accntPartList) 
                {
                    accntPartRecord.Countries_recruiting_from__c = 'test5;test6;test7;test8';
                }
                
                update accntPartList;
                
                // Get all records of Account Participation
                testAccntPartList = new List<Account_Participation__c>([SELECT Countries_Recruiting_From_text__c 
                                                                        FROM Account_Participation__c]);
                
                // Check 'Countries Recruiting From' text field generated correctly for Updation
                for(Account_Participation__c accntPartRecord : testAccntPartList) 
                {
                    strCountriesrecruitingFrom = accntPartRecord.Countries_Recruiting_From_text__c;
                    System.assertEquals('test5, test6, test7, test8', strCountriesrecruitingFrom);
                }  
                
                Test.stopTest();
            }
            catch(Exception e)
            {
                System.debug('ERROR : ' + e.getMessage());
            }
        }
    }