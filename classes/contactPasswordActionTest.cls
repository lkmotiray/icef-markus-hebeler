@isTest
// inactivate process that calls the Action before running the test!
public class contactPasswordActionTest {
    
    public static testMethod void testPasswordGeneration(){
 		
        Integer numConts = 30;
        
	    List<Account> accts = DataFactory.createTestAgents(1);
        insert accts;
        
        Account acct = [SELECT Id, Name, RecordTypeId FROM Account WHERE Name LIKE 'Test%'][0];
	    List<Contact> contacts = DataFactory.createTestContacts(acct, numConts);
		
        insert contacts;        
        
    	Map<Id, Contact> contactMap = new Map<Id, Contact>(contacts);
    
    	Set<Id> contactIds = contactMap.keySet();
    	List<Id> conIdList = new List<Id>(contactIds);

        System.assertEquals(numConts, [SELECT Id FROM Contact WHERE Password__c = null].size());
        
        Test.startTest();
        contactPasswordAction.generatePassword(conIdList);
        Test.stopTest();
        
        System.assertEquals(0, [SELECT Id FROM Contact WHERE Password__c = null].size());
    }

}