/**
    @ Developer Name : Sagar[Dws]
    @ Purpose        : To Create Activity History of Sales Invocie records as well as Account and Contact
    @ Created Date   : 16 April, 2016       
*/
global without sharing class ActivityManager {
    
    /**
        @Purpose : To Create Activity history of Sales Invocie records as well as Account and Contact
    */
    webservice static void createActivityHistory(List<String> listOfSalesInvoceIDs){
    
        System.debug('listOfSalesInvoceIDs::'+ listOfSalesInvoceIDs);
        
        if(!listOfSalesInvoceIDs.isEmpty()){
            // List of codaInvoice records
            List<c2g__codaInvoice__c> listOfcodaInvoice = new List<c2g__codaInvoice__c>();
            
            // Map of Email template and Sales Invoice Transaction Owner  
            Map<String,Id> mapOfEmailTemplate = new Map<String,Id>();
            
            try{
                listOfcodaInvoice = [ SELECT Id, Name, c2g__Transaction__r.owner.name,c2g__Account__r.Name,
                                      c2g__OwnerCompany__c,c2g__Account__r.c2g__CODAFinanceContact__c,
                                      c2g__Account__r.c2g__CODAFinanceContact__r.Contact_Email__c,
                                      c2g__Opportunity__r.Invoicing_contact__c, 
                                      c2g__Opportunity__r.Invoicing_contact__r.Email
                                      FROM c2g__codaInvoice__c 
                                      Where 
                                      c2g__Transaction__r.owner.Name IN ('FF ICEF GmbH','FF ICEF Asia Pacific') 
                                          AND
                                      Id IN: listOfSalesInvoceIDs 
                                    ];
                                         
                System.debug('listOfcodaInvoice::'+listOfcodaInvoice);                                         
            }catch( Exception e){
                exceptionHandling(e);
            }
            
            try{
                // List of Email Templates 
                List<EmailTemplate> listOfEmailTemplates = [SELECT Id, DeveloperName 
                                                            FROM EmailTemplate 
                                                            Where DeveloperName IN ('codaSalesInvoice','AsiaPaccodaSalesInvoice')];
                
                    if(!listOfEmailTemplates.isEmpty()){
                        for( EmailTemplate  emailtemp: listOfEmailTemplates) {
                            if(emailtemp.DeveloperName.equalsIgnoreCase('codaSalesInvoice')){
                                mapOfEmailTemplate.put('FF ICEF GmbH',emailtemp.Id);
                            }else if(emailtemp.DeveloperName.equalsIgnoreCase('AsiaPaccodaSalesInvoice')){
                                mapOfEmailTemplate.put('FF ICEF Asia Pacific',emailtemp.Id);
                            }
                        }
                    }
            }catch( Exception e){
                exceptionHandling(e);
            }
            
            
            if(!listOfcodaInvoice.isEmpty()){
                Map<Id,Attachment> mapOfAttachments = createMapOfAttachments(listOfcodaInvoice);
                Map<String,String> mapofSubject = createMapSubject(listOfcodaInvoice);
                Map<Id, Task> mapInvoiceIdToTask = createMapInvoiceIdToTask(listOfcodaInvoice);
                List<Attachment> listOfTaskAttachments = new List<Attachment>();
                List<Task> listOfTask = new List<Task>();
                Task newTask;
                
                for( c2g__codaInvoice__c  salesInv : listOfcodaInvoice ){
                    newTask = new Task();
                    
                    if(mapInvoiceIdToTask.containsKey(salesInv.Id)){
                        newTask.Id = mapInvoiceIdToTask.get(salesInv.Id).Id;
                        
                        if(mapInvoiceIdToTask.get(salesInv.Id).Description != null){
                            String strDescription = String.valueOf( mapInvoiceIdToTask.get(salesInv.Id).Description);
                            if(String.isNotBlank(strDescription)){
                                strDescription = strDescription.replaceAll('[\n ]{2,}','\n\n');
                            }
                            newTask.Description = strDescription;
                        }
                    }
                    
                    newTask.WhatId =  salesInv.Id;
                    newTask.ActivityDate = Date.today();
                    newTask.Status = 'Completed';
                    newTask.Priority = 'Normal';
                    
                    if(mapofSubject.containsKey(salesInv.Name)){
                        newTask.Subject = 'Email: '+mapofSubject.get(salesInv.Name);
                    }
                    
                    String strTemplateId;
                    // get Email template Id 
                    if(!mapOfEmailTemplate.isEmpty() && mapOfEmailTemplate.containsKey(salesInv.c2g__Transaction__r.owner.Name)){
                        strTemplateId = mapOfEmailTemplate.get(salesInv.c2g__Transaction__r.owner.Name);
                    }
                    
                    newTask.WhoId = salesInv.c2g__Opportunity__r.Invoicing_contact__c;    
                    
                    listOfTask.add(newTask);
                }
                /*
                Map<Id, c2g__codaInvoice__c> salesInvoiceMap = new Map<Id, c2g__codaInvoice__c>(listOfcodaInvoice);
                Map<Id, String> salesInvoice2EmailBodyMap = generateEmailtemplateBodies(listOfcodaInvoice, mapOfEmailTemplate);
                
                //Update description for the task
                for(Task task : listOfTask) {
                    c2g__codaInvoice__c parentSalesInvoice = salesInvoiceMap.get(task.WhatId);
                    
                    //System.debug('Task What ID : ' + task.WhatId + '\nsalesInvoice2EmailBodyMap : ' + JSON.serialize(salesInvoice2EmailBodyMap));
                    
                    if(salesInvoice2EmailBodyMap.containsKey(parentSalesInvoice.Id)) {
                        String strToEmail = parentSalesInvoice.c2g__Opportunity__r.Invoicing_contact__r.Email;
                        String strEmailBody = salesInvoice2EmailBodyMap.get(parentSalesInvoice.Id);
                        String strAttachmentName = parentSalesInvoice.c2g__Account__r.Name +' ICEF Invoice ' + parentSalesInvoice.Name + '.pdf';
                        //task.Description = 'To: '+strToEmail+'\n Subject: '+ mapofSubject.get(parentSalesInvoice.Name)+',\n Attachment:'+strAttachmentName+',\n Body:\n '+strEmailBody;
                    }
                }*/
                
                try{ 
                    if(!listOfTask.isEmpty()){
                         upsert listOfTask;
                    }
                }catch(Exception e){
                    exceptionHandling(e);
                }
                //  Insert Attachments
                if(!listOfTask.isEmpty()){
                    for(Task task : listOfTask ){
                        if( task.WhatId != null && mapOfAttachments.containsKey(task.WhatId)){
                            Attachment eAttach  = mapOfAttachments.get(task.WhatId);
                            eAttach.ParentId = task.Id;
                            listOfTaskAttachments.add( eAttach);
                        }
                    }
                }
                try{                    
                    if(!listOfTaskAttachments.isEmpty()){
                         Insert listOfTaskAttachments;
                    }
                }catch(Exception e){
                    exceptionHandling(e);
                }
            }           
        }
    }
    
    private static Map<Id, Task> createMapInvoiceIdToTask(List<c2g__codaInvoice__c> listOfcodaInvoice){
        Map<Id, Task> mapInvoiceIdToTask = new Map<Id, Task>();
        
        Map<Id, c2g__codaInvoice__c> mapSalesInvoice = new Map<Id, c2g__codaInvoice__c>(listOfcodaInvoice);
         
        for(Task task : [SELECT Id, Subject, WhatId, Description
                         FROM Task 
                         WHERE CreatedDate >=: Date.today()
                         AND WhatId IN :mapSalesInvoice.keySet()
                         ORDER BY CreatedDate Desc]){
             if(!mapInvoiceIdToTask.containsKey(task.WhatId)){
                 mapInvoiceIdToTask.put(task.WhatId, task);
             }
        }
        
        return mapInvoiceIdToTask;
    }
    
    /*private static Map<Id, String> generateEmailtemplateBodies(List<c2g__codaInvoice__c> salesInvList, Map<String,Id> mapTransationOwner2TemplateId) {
        List<Messaging.SingleEmailMessage> listOfEmailsToHold = new List<Messaging.SingleEmailMessage>();
        String strTemplateId;
        for(c2g__codaInvoice__c salesInv : salesInvList) {
            String transationOwner = salesInv.c2g__Transaction__r.owner.Name;
            
            
            if(mapTransationOwner2TemplateId.containsKey(transationOwner)) {
                strTemplateId = mapTransationOwner2TemplateId.get(transationOwner);
            }
            
            if(String.isNotBlank(salesInv.c2g__Account__r.c2g__CODAFinanceContact__c)
              && String.isNotBlank(salesInv.c2g__Account__r.c2g__CODAFinanceContact__r.Contact_Email__c)) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(strTemplateId);
                mail.setSaveAsActivity(false);
                mail.setWhatId(salesInv.Id);
                mail.setTargetObjectId(salesInv.c2g__Account__r.c2g__CODAFinanceContact__c);
                listOfEmailsToHold.add(mail);
            }
        }
        
        SavePoint savePointValue = Database.setSavePoint();
        
        try {
            Messaging.sendEmail(listOfEmailsToHold);
        }
        catch (exception e) {
            System.debug('Error occured while sending dummy mail::' + e);
        }
        
        Database.rollback(savePointValue);
        Map<Id, String> salesInvoice2EmailBodyMap = new Map<Id, String>();
        
        for(Messaging.SingleEmailMessage emailMessage : listOfEmailsToHold) {
            if(String.isNotBlank(emailMessage.getHtmlBody())) {
                
                //PageReference pageRef = new PageReference('/' + emailMessage.WhatId); 
                //pageRef.setRedirect(true);              
                //Blob b = pageRef.getContent();
                //String strEmailBody = b.toString();
                //String strEmailBody = emailMessage.getPlainTextBody();
                emailMessage = Messaging.renderStoredEmailTemplate(emailMessage.getTemplateId(),emailMessage.getTargetObjectId(),emailMessage.getWhatId());
                
                String strEmailBody = emailMessage.getHtmlBody();
                
                //update new line characters with temporary char
                strEmailBody = strEmailBody.replaceAll('\\n','xNewLinex');
                
                //remove all unnecessary blank spaces
                strEmailBody = strEmailBody.replaceAll('\\s\\s+','');
                
                //remove all content that is not being displayed
                strEmailBody = strEmailBody.replaceAll('<p style=\"display:none\">(.*?)<\\/p>','');
                
                //remove all remaining html tags
                strEmailBody = strEmailBody.replaceAll('\\<.*?\\>','');
                
                //remove multiple ocurences of new line character with single character
                strEmailBody = strEmailBody.replaceAll('(xNewLinex)+','xNewLinex');
                
                //modify temparary new line character with actual new line character
                strEmailBody = strEmailBody.replaceAll('xNewLinex','\\\n\\\n');

                System.debug('strEmailBody::'+strEmailBody);
                salesInvoice2EmailBodyMap.put(emailMessage.WhatId, strEmailBody);
            }
        }
        
        return salesInvoice2EmailBodyMap;
    }
    */
    /**
        @Purpose : Function to map the VF Pages rendered as PDF that is to be used.
     */
    private static Map <Id,Attachment> createMapOfAttachments( List<c2g__codaInvoice__c> listOfcodaInvoice) {
        Map <Id,Attachment> mapOfSaleInvAttachments = new Map <Id,Attachment>();
        try{        
            if(!listOfcodaInvoice.isEmpty()){
            Attachment eAttach = new Attachment();    
            // get Map of related template name it's email templates  
                Map<String, PageReference> mapPDFToUse = createMapOfVFPdfs();
                for( c2g__codaInvoice__c salesInv : listOfcodaInvoice){
                    if(!mapPDFToUse.isEmpty() && 
                        mapPDFToUse.containsKey(salesInv.c2g__Transaction__r.owner.Name)){
                             eAttach = new Attachment();
                             PageReference pdfAttachment = mapPDFToUse.get(salesInv.c2g__Transaction__r.owner.Name);
                             pdfAttachment.getParameters().put('id', salesInv.Id);
                             pdfAttachment.setRedirect(true);
                             eAttach.Name = salesInv.c2g__Account__r.Name +' ICEF Invoice ' + salesInv.Name + '.pdf';
                             eAttach.contentType = 'application/pdf';
                             if(Test.isRunningTest()) {
                                 eAttach.Body = Blob.valueof('test content');
                             } else {
                                 eAttach.Body = pdfAttachment.getContentAsPDF();
                             }
                             mapOfSaleInvAttachments.put(salesInv.Id,eAttach);
                    }
                }               
            }
        }catch(Exception e){
            exceptionHandling(e);
        }
        return mapOfSaleInvAttachments;
    }
    
    /**
        @Purpose : Function to map the VF Pages rendered as PDF that is to be used.
     */
    private static Map<String, PageReference> createMapOfVFPdfs() {
        
        Map<String, PageReference> mapPDFToUse = new Map<String, PageReference>();     
        mapPDFToUse.put('FF ICEF Asia Pacific', Page.ICEF_Asia_Pac_Sales_Invoice_PDF);
        mapPDFToUse.put('FF ICEF GmbH', Page.ICEF_GmbH_Sales_Invoice_PDF);        
        return mapPDFToUse;
    }
    
    /**
    *   @Purpose: To create a map of Subject
    */
    private static Map<String,String> createMapSubject(List<c2g__codaInvoice__c> listOfcodaInvoice){
        Set<Id> setOfOwnerCompanyId = new Set<Id>();
        Map<String, String> mapofSubject = new Map<String, String>();
        
        if(!listOfcodaInvoice.isEmpty()){
            for(c2g__codaInvoice__c saleinv :  listOfcodaInvoice){
                setOfOwnerCompanyId.add(saleinv.c2g__OwnerCompany__c);
            }
            if(!setOfOwnerCompanyId.isEmpty()){
                List<c2g__codaCompany__c> listOfcompany;
                try{
                    listOfcompany = [SELECT Id, Name 
                                        FROM c2g__codaCompany__c 
                                        WHERE Id IN :setOfOwnerCompanyId];
                }
                catch(Exception e){
                    exceptionHandling(e);
                } 
                if(listOfcompany != null && !listOfcompany.isEmpty()){
                    for(c2g__codaInvoice__c saleinv :  listOfcodaInvoice){
                        for(c2g__codaCompany__c company : listOfcompany){
                            if(String.valueOf(company.Id).equalsIgnoreCase(saleinv.c2g__OwnerCompany__c)){                      
                                mapofSubject.put(saleinv.Name,'Your invoice from ' + company.Name +' - invoice number ' + saleinv.Name);
                            }
                        }
                    }
                }                                               
            }
        }
        System.debug('mapofSubject::'+mapofSubject);
        return mapofSubject;
    }
    
    /**
        @Purpose : Function to get email template plain text body 
    */
    private static String getEmailtemplateBody( String strTargetObjId, String strWhatId,String strTemplateId ) {
        String strEmailBody;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        /*mail.setTemplateId(strTemplateId);
        mail.setSaveAsActivity(false);
        mail.setWhatId(strWhatId);
        mail.setTargetObjectId(strTargetObjId);
        List<Messaging.SingleEmailMessage> listOfEmailsToHold = new List<Messaging.SingleEmailMessage>{mail};
        
        SavePoint savePointValue = Database.setSavePoint();
        try {
            Messaging.sendEmail(listOfEmailsToHold);
        }
        catch (exception e) {
            System.debug('Error occured while sending dummy mail::' + e);
        }
        Database.rollback(savePointValue);
        if(!listOfEmailsToHold.isEmpty()){
            System.debug('listOfEmailsToHold::'+listOfEmailsToHold);
            strEmailBody = listOfEmailsToHold[0].getHtmlBody().replaceAll('\\<.*?\\>', '');
        }*/
        mail = Messaging.renderStoredEmailTemplate(strTemplateId,strTargetObjId,strWhatId);
        strEmailBody = mail.getHtmlBody();
        System.debug('strEmailBody::'+strEmailBody);
        return strEmailBody;
    }
    
    /**
        @ Purpose : To Print the Exception  
    */
    global static void exceptionHandling(Exception e){
        
         System.debug('Error ::' + e.getMessage() + ' \n Stack Trace:' + e.getStackTraceString());
    }
}