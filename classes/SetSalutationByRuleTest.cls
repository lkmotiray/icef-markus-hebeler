@isTest
private class SetSalutationByRuleTest 
{
    static testMethod void testSetSalutationByRule() 
    {
        Country__c con = new Country__c(Name='USA',Agent_Relationship_Manager__c = Userinfo.getUserId());
        insert con;
        Account a = new Account(ARM__c = Userinfo.getUserId(),Name='Testing acc', Status__c='Active', Mailing_State_Province__c='CA', Mailing_Country__c=con.Id, account_owner_email__c='aaa.bbb@gmail.com', Email__c='aaa.bbb@gmail.com');
        insert a;
        
        Contact c = new Contact();
        c.AccountId = a.Id;
        c.Gender__c = 'male';
        c.FirstName = 'First';
        c.LastName = 'Last';
        c.Salutation = 'Mr';
        
        insert c;
        
        Contact c2 = new Contact();
        c2.Gender__c = 'male';
        c2.FirstName = 'Firstname';
        c2.LastName = 'Lastname';
        c2.Salutation = 'Mr';
        c2.AccountId = a.Id;
        insert c2;
    }
}