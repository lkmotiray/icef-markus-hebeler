/**
  * @Purpose - TextDefinitionViewer component controller
  * @Created Date - 2 August 2017
 */
public with sharing class TextDefinitionViewerController {
    public c2g__codaInvoice__c salesInvoiceRecord {
        get;
        set;
    }
    /**
      * @Purpose - get Text Definition records
     */
    public List<c2g__codaTextDefinition__c> getTextDefinitionRecords() {
        return [ SELECT Id, c2g__Heading__c, c2g__Text__c 
                 FROM c2g__codaTextDefinition__c 
                 WHERE c2g__OwnerCompany__c =: salesInvoiceRecord.c2g__OwnerCompany__c 
                       AND c2g__TextPosition__c IN ('1','2','3')
                       AND c2g__Invoice__c != 'Not Used'
                 ORDER BY c2g__TextPosition__c ];
    }
}