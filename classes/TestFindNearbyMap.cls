@isTest
private class TestFindNearbyMap {

    static testMethod void tGetSets() {
        Country__c usa = new Country__c(Name = 'USA');
        usa.Agent_Relationship_Manager__c = UserInfo.getUserId();
        insert usa;
        
        FindNearbyMap f = new FindNearbyMap();
        PageReference pageRef = Page.FindNearbyMap;
        //Test.setCurrentPage(pageRef);
        Account acct1 = new Account(Name = 'Test Account1', Industry = 'Agriculture', BillingStreet = '2 Market St');
        acct1.Mailing_Country__c = usa.Id;
        acct1.Mailing_State_Province__c = 'CA';
        insert acct1;
        
        Account acct = new Account(Name = 'Test Account', Industry = 'Agriculture');
        acct.Mailing_Country__c = usa.Id;
        acct.Mailing_State_Province__c = 'CA';
        insert acct;
        
        Account a = [select id from Account where Id =: acct.Id limit 1];
        
        Contact cont = new Contact(Salutation = 'Dr.', FirstName = 'Test', LastName = 'Test', Gender__c = 'male', Role__c = 'Assistant', Contact_Status__c = 'Active', AccountId = a.Id);
        insert cont;
        
        Contact c = [select id from Contact where Id =: cont.Id limit 1];
        
        Country__c ctry = new Country__c(Name = 'China', Region__c = 'Asia');
        insert ctry;
         
        Lead led = new Lead(LastName='Test Lead', Company = 'Test Company', Country__c = 'China');
        insert led; 
           
        Lead l = [select id from Lead where isConverted=false and Id =: led.Id limit 1];
        
        a.Mapping_Status__c = 'Located';
        a.Which_Address__c  = 'Billing';   
        a.BillingStreet = '2 Market St'; 
        l.Street = '2 market st';
        c.MailingStreet = '2 market st';
        update a;         
        update l;
        System.assert(l != null);        
        
        f.getAccountIndustryOptions(); 
        f.setAccountIndustryOptions(); 
        
        f.getAccountTypeOptions();
        f.setAccountTypeOptions();
        
        f.getDistances();
        f.getLeadStatusOptions();	

        f.searchAccounts = true;
        f.searchContacts = true;
        f.searchLeads = true; 
        
        //first If 
        f.needFilters = false;
        f.setStartingPoint();
        
        //List View Locations
        f.locs.add(new MapItem(a));
        f.setStartingPoint();
        
        //Contacts
        f.needFilters = true;
        f.locs.add(new MapItem(a));
        ApexPages.currentPage().getParameters().put('cid', c.Id);
        f.init();
        f.setStartingPoint();
        
        f.filterLocs();
        f.AccountIndustries.clear();
        f.AccountTypes.clear();
        f.LeadStatuses.clear();
        f.filterLocs();
        //Accounts
        f.needFilters = true;
        f.locs.add(new MapItem(a));
        ApexPages.currentPage().getParameters().put('aid', a.Id);
        f.setStartingPoint();
        
        //Leads
        f.needFilters = true;
        f.locs.add(new MapItem(a));
        ApexPages.currentPage().getParameters().put('lid', l.Id);
        f.setStartingPoint();
        
        
        f.locs = null;
        f.getCount();               
        System.Assert(f.getCount() != null);
        
        f.getDistance();
        System.Assert(f.getDistance() != null);
        
        f.getDestinations();
        
        List<String> s = new List<String>();
        s.add('All');
        
        f.setDistance('200');
        f.setAccountTypes(s);
        f.getAccountTypes();
        f.setAccountIndustries(s);
        f.getAccountIndustries();
        f.setLeadStatuses(s);
        f.getLeadStatuses();
        f.search();
        f.getL();
        f.getGKey();
        f.goBackToRecord(); 
        f.searchAccounts = true;
        f.searchContacts = true;
        f.searchLeads = true;
        f.getLocations();      
        
        f.needFilters = false;
        f.ListAccounts = true;
        f.getLocations();
        f.goBackToRecord(); 
        f.ListAccounts = false;
        f.ListContacts = true;        
        f.goBackToRecord();
        f.ListContacts = false; 
        f.ListLeads = true;        
        f.goBackToRecord();
        
        f.setDistance('250');
        MapUtility.getLeadsToPlot(); 
        MapUtility.getObjectLimit();
        MapUtility.getAccuracy();       
        
        MapItem startingPoint = new MapItem(a);
        MapUtility.getNearbyAccounts(startingPoint, 20);   
        
        MapUtility.getBoundingCondition(startingPoint, 20); 
        MapUtility.DetermineStatus('200', '5');
        MapUtility.DetermineStatus('610', '5');
        MapUtility.DetermineStatus('620', '5');
        MapUtility.DetermineStatus('660', '5');
        MapItem startingPoint1 = new MapItem(c);
        MapUtility.getNearbyContacts(startingPoint1, 20); 
        
        MapItem startingPoint2 = new MapItem(l);
        MapUtility.getNearbyLeads(startingPoint2, 20);     
        
        a.Lat__c = 19.997453;
        a.Lon__c = 73.789802;
        update a;
        MapItem startingPoint3 = new MapItem(a);
        MapUtility.calcMaxLon(startingPoint3, 20);
        MapUtility.calcMinLon(startingPoint3, 20);
    }   
    
     static testMethod void tlocations() {
        Country__c usa = new Country__c(Name = 'USA');
        usa.Agent_Relationship_Manager__c = UserInfo.getUserId();
        insert usa;
         
        FindNearbyMap f = new FindNearbyMap();
        PageReference pageRef = Page.FindNearbyMap;
        //Test.setCurrentPage(pageRef);
       
        Account acct = new Account(Name = 'Test Account');
        acct.Mailing_Country__c = usa.Id;
        acct.Mailing_State_Province__c = 'CA';
        insert acct;
        
        Account a = [select id from Account where Id =: acct.Id limit 1];
        
        Contact cont = new Contact(Salutation = 'Dr.', FirstName = 'Test', LastName = 'Test', Gender__c = 'male', Role__c = 'Assistant', Contact_Status__c = 'Active', AccountId = a.Id);
        insert cont;
        
        Contact c = [select id from Contact where Id =: cont.Id limit 1];
        
        Country__c ctry = new Country__c(Name = 'China', Region__c = 'Asia');
        insert ctry;
         
        Lead led = new Lead(LastName='Test Lead', Company = 'Test Company', Country__c = 'China');
        insert led; 
           
        Lead l = [select id from Lead where isConverted=false and Id =: led.Id limit 1];
        
        a.Mapping_Status__c = 'Located';
        a.Which_Address__c  = 'Billing';   
        update a; 
        
        f.init();
        
        ApexPages.currentPage().getParameters().put('llids', l.Id);
        ApexPages.currentPage().getParameters().put('clids', c.Id);
        ApexPages.currentPage().getParameters().put('alids', a.Id);
        f.init();
        f.handleListViews();
        
        
        System.assert(a != null);
        
     }
    
}