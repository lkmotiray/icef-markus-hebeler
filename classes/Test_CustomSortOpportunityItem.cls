@isTest(seeAllData = true) 
private class Test_CustomSortOpportunityItem {

    static testMethod void test_Method(){
        //-- Set up local variables
        String opportunityName = 'My Opportunity';
        String standardPriceBookId = '';

        //--Fetch Pricebook
        PriceBook2 pb2Standard = [SELECT Id FROM Pricebook2 WHERE isStandard=true];
        standardPriceBookId = pb2Standard.Id;
        
        //--insert Account
              
        User userRecord = [SELECT Id FROM User LIMIT 1];
        
        Country__c countryRecord = new Country__c ( Name='TestC' ,
                                                    Region__c = 'South & Central America',
                                                    Sales_Territory__c = 'Americas',
                                                    Agent_Relationship_Manager__c = userRecord.Id,
                                                    countries_from_to_in__c = True,
                                                    Sales_Territory_Manager__c = userRecord.Id,
                                                    City_Post_Code_State_Order__c = 'City Post Code',
                                                    Sales_Priority__c = '1',
                                                    Sales_Priority_Agents__c = '1',
                                                    Phone_Country_Code__c = '001',
                                                    Languages_spoken__c = 'Dutch, English, Papiamento',
                                                    ISO_Number__c = 999,
                                                    ISO2__c = 'SX',
                                                    ISO3__c = 'SXM' );
        insert countryRecord;
        
        
        RecordType recordType = [SELECT Id FROM RecordType WHERE Name = 'Service Provider' AND SobjectType = 'Account' ];
        
        Account testAcc = new Account( RecordType = recordType,
                                            Name = 'Test',
                                            CurrencyIsoCode = 'USD',
                                            Business_Sectors__c = 'Software & Multimedia',
                                            Status__c = 'Active',
                                            Mailing_Country__c = countryRecord.Id,
                                            ARM__c = userRecord.Id,
                                            BillingCity = 'Melbourne',
                                            BillingState = 'Victoria',
                                            BillingCountry = 'Australia'
                                           );
        
        insert testAcc;
        
        
        //--set up opp and Verify that the results are as expected.
        Opportunity testOpp = new Opportunity(AccountId=testAcc.Id, Name=opportunityName,
        StageName='Prospecting', CloseDate=Date.today());
        insert testOpp;
        Opportunity opp = [SELECT Name FROM Opportunity WHERE Id = :testOpp.Id];
        System.assertEquals(opportunityName, opp.Name);

        //--set up product2 and Verify that the results are as expected.
        Product2 p2 = new Product2(Name='Display Table',isActive=true);
        insert p2;
        Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];
        System.assertEquals('Display Table', p2ex.Name);
        Product2 p3 = new Product2(Name='product',isActive=true);
        insert p3;    

        //--set up PricebookEntry and Verify that the results are as expected.
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p2.Id, UnitPrice=99, isActive=true);
        insert pbe;
        PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];
        System.assertEquals(standardPriceBookId, pbeex.Pricebook2Id);

        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p3.Id, UnitPrice=99, isActive=true);
        insert pbe1;

        
        //--Insert ICEF Event        
        ICEF_Event__c testEvent1 = new ICEF_Event__c();
        testEvent1.Name = 'TestEvent1';
        testEvent1.Event_Manager_Agents__c = UserInfo.getUserId();
        testEvent1.Event_Manager_Educators__c = UserInfo.getUserId();
        testEvent1.Event_City__c = 'Test';
        testEvent1.technical_event_name__c = 'TestEvent1-Test-Test';
        testEvent1.Start_date__c = System.today();
        insert testEvent1;

        //--set up OpportunityLineItem and Verify that the results are as expected.
        OpportunityLineItem oli = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=testOpp.Id, Quantity=1, TotalPrice=99,ICEF_Event__c = testEvent1.Id);
        insert oli;

        OpportunityLineItem oliex = [SELECT PriceBookEntryId FROM OpportunityLineItem WHERE Id = :oli.Id];
        System.assertEquals(pbe.Id, oliex.PriceBookEntryId);

        OpportunityLineItem oli1 = new OpportunityLineItem(PriceBookEntryId=pbe1.Id, OpportunityId=testOpp.Id, Quantity=1, TotalPrice=99,ICEF_Event__c = testEvent1.Id);
        insert oli1;
        //--Call web-service
        String sortedIds = customSortOpportunityItem.MRsort(testOpp.Id);
        System.debug('sortedIds =='+sortedIds );
    }
}