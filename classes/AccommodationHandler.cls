/*---------------------------------------------------------------------------------------------------------
    @ Developer          : Dreamwares IT Solutions [Sagar].
    @ Purpose            : On change of Accommodation:
							Update in Hotel Room:
								- Persons attending
							Update in AP:
               					- Number of Hotel Rooms
							Update in CP:
								- Number of Accommodations
    @ Created Date       : 2016/01/25   
    @ Modified Date      : 2016/06/29
							2018/04/16 (Henriette)
-----------------------------------------------------------------------------------------------------------*/
public class AccommodationHandler {

    /**
        @Purpose   : Update Hotel Room and AP after insert of Accommodation
        @Parameter : Trigger.new        
    */
    public static void afertInsert(List<Accommodation__c> newAccommodationList) {
       
       //Trigger to Update 'Persons attending' on Hotel Room
       afertInsertUpdateHotelRoom(newAccommodationList);
        
        //Trigger to Update 'Number Hotel Rooms for Accommodations' on AP(Account Participation)
        afertInsertUpdateAP(newAccommodationList);
    }
    
    /**
      @Purpose   : Update Hotel Room and AP after update of Accommodation
      @Parameter : Trigger.newmap,Trigger.oldmap.     
     */
    public static void afterUpdate(Map<Id,Accommodation__c> newAccommodationMap, Map<Id,Accommodation__c> oldAccommodationMap) {
        //Trigger to Update 'Persons attending' on Hotel Room
        afterUpdateHotelRoom(newAccommodationMap,oldAccommodationMap);
        
        //Trigger to Update 'Number Hotel Rooms for Accommodations' on AP(Account Participation)
        afertUpdate_UpdateAP(newAccommodationMap,oldAccommodationMap);
    }
    
    /**
        @Purpose   : Trigger to delet 'Persons attending' on Hotel Room
        @Parameter : Trigger.Old        
    */
    public static void afterDelete(List<Accommodation__c> oldAccommodationList) {
        // Trigger to delete 'Persons attending' on Hotel Room records
        afterDeleteUpdateHotelRoom( oldAccommodationList );
        
        //Trigger to Update 'Number Hotel Rooms for Accommodations' on AP(Account Participation)
        afertDeleteUpdateAP(oldAccommodationList );
    }
    
    /**
        @Purpose   : Trigger to Update 'Number Hotel Rooms for Accommodations' on AP(Account Participation)
                     **Modified to update booked accommodation count on CP
        @Parameter : Trigger.new
        @Return:
    */
    public static void afertInsertUpdateAP(List<Accommodation__c> newAccommodationList) {
        // Set of filter AP Ids 
        set<ID> setOfAPIds = new set<ID>();  
        
        // Set of filter CP Ids 
        set<ID> setOfCPIds = new set<ID>(); 
            
        try{
            for(Accommodation__c newAccommodation : newAccommodationList){          
                if(newAccommodation.Status__c != null &&                               
                   newAccommodation.Status__c.equalsIgnoreCase('booked')  
                   ){ 
                      if(newAccommodation.Account_Part__c != null)                     
                          setOfAPIds.add(newAccommodation.Account_Part__c);
                      if(newAccommodation.Contact_Participation__c != null)
                          setOfCPIds.add(newAccommodation.Contact_Participation__c);                        
                }
            }
            if(!setOfAPIds.isEmpty()){ 
                updateNumberHotelRooms(setOfAPIds);
            }
        }
        catch(Exception e){
            handleException(e);
        }
        system.debug(' After insert '+setOfCPIds); 
        updateContactParticiaption(setOfCPIds);
    }
    
    /**
        @Purpose   : Trigger to Update 'Number Hotel Rooms for Accommodations' on AP(Account Participation)
                     **Modified to update booked accommodation count on CP
        @Parameter : Trigger.Old
        @Return:
    */
    public static void afertDeleteUpdateAP(List<Accommodation__c> oldAccommodationList) {       
       
        // Set of filter AP Ids 
        set<ID> setOfAPIds = new set<ID>();
        
        // Set of filter CP Ids 
        set<ID> setOfCPIds = new set<ID>(); 
        
        try{
            for(Accommodation__c oldAccommodation : oldAccommodationList){          
                if(oldAccommodation.Status__c != null &&                               
                   oldAccommodation.Status__c.equalsIgnoreCase('booked') 
                    ){
                      if(oldAccommodation.Account_Part__c != null)                     
                          setOfAPIds.add(oldAccommodation.Account_Part__c);
                      if(oldAccommodation.Contact_Participation__c != null)
                          setOfCPIds.add(oldAccommodation.Contact_Participation__c);           
                }
            }
            if(!setOfAPIds.isEmpty()){ 
               updateNumberHotelRooms(setOfAPIds);
            }
            
        }
        catch(Exception e){
            handleException(e);
        }
        system.debug(' After delete '+setOfCPIds); 
        updateContactParticiaption(setOfCPIds);    
    }
    
    /**
      @Purpose   : Trigger to Update 'Number Hotel Rooms for Accommodations' on AP(Account Participation)
                   **Modified to update booked accommodation count on CP
      @Parameter : Trigger.newmap,Trigger.oldmap.
      @Return:
     */
    public static void afertUpdate_UpdateAP(Map<Id,Accommodation__c> newAccommodationMap, Map<Id,Accommodation__c> oldAccommodationMap) {
        
        // List of filter Accommodation records 
        set<ID> setOfAPIds = new set<ID>(); 
        
        // Set of filter CP Ids 
        set<ID> setOfCPIds = new set<ID>(); 
       
       Accommodation__c oldAccommodation ;
        try{
            for(Accommodation__c newAccommodation : newAccommodationMap.values()){
               oldAccommodation = oldAccommodationMap.get(newAccommodation.id);
               
               if(newAccommodation.Hotel_Room__c != oldAccommodation.Hotel_Room__c){
                   if(newAccommodation.Account_Part__c != null)                     
                          setOfAPIds.add(newAccommodation.Account_Part__c);
               } 
               if( newAccommodation.Status__c != oldAccommodation.Status__c ){    
                   
                   if(newAccommodation.Account_Part__c != null)                     
                          setOfAPIds.add(newAccommodation.Account_Part__c);
                   
                   if(newAccommodation.Contact_Participation__c != null)
                       setOfCPIds.add(newAccommodation.Contact_Participation__c);
               }
               if( newAccommodation.Account_Part__c != oldAccommodation.Account_Part__c){ 
                    if(oldAccommodation.Account_Part__c != null)                     
                          setOfAPIds.add(oldAccommodation.Account_Part__c);
                    if(newAccommodation.Account_Part__c != null)                     
                          setOfAPIds.add(newAccommodation.Account_Part__c);
               }
               if( newAccommodation.Contact_Participation__c != oldAccommodation.Contact_Participation__c ){    
                
                   if(oldAccommodation.Contact_Participation__c != null)                     
                        setOfCPIds.add(oldAccommodation.Contact_Participation__c);
                   if(newAccommodation.Contact_Participation__c != null)
                       setOfCPIds.add(newAccommodation.Contact_Participation__c);
               }
            }
            if(!setOfAPIds.isEmpty()){               
               updateNumberHotelRooms(setOfAPIds);
            }
            
        }
        catch(Exception e){
            handleException(e);
        }
        system.debug(' After update '+setOfCPIds); 
        updateContactParticiaption(setOfCPIds);   
    }
    
    /**
        @Purpose   : Trigger to Update 'Number Hotel Rooms for Accommodations' on AP(Account Participation)
        @Parameter : Trigger.new
        @Return:
    */
    public static void updateNumberHotelRooms(Set<Id> setOfAPIds) {
        
        List<AggregateResult> listOfAP = new List<AggregateResult>();
        Map<String,Integer> mapOfParentIDandCounter = new Map<String,Integer>();
        List<Account_Participation__c> listOfaccoutPart;
        
        System.debug('setOfAPIds::'+setOfAPIds);
        
        try{
            listOfAP = [SELECT COUNT(ID) counter, Hotel_Room__c, Account_Part__c 
                                              FROM Accommodation__c 
                                              WHERE Account_Part__c IN :setOfAPIds AND Status__c = 'booked'  
                                              GROUP BY Hotel_Room__c, Account_Part__c];
         
         System.debug('listOfAP ::'+listOfAP );  
                                            
           // if(!listOfAP.isEmpty()){
            
                listOfaccoutPart = new List<Account_Participation__c>();
                
                for(AggregateResult accommodation : listOfAP){
                    
                    String strMapKey = String.valueOf( accommodation.get('Account_Part__c') );
                    
                    if( !mapOfParentIDandCounter.containsKey( strMapKey ) ) {
                        mapOfParentIDandCounter.put( strMapKey, 0 );
                    }
                    mapOfParentIDandCounter.put(strMapKey, mapOfParentIDandCounter.get( strMapKey ) + 1 );                 
                }
                
                // correction for lost AP id after deletion and upadation
                //set<String> setOfMappedAPId = mapOfParentIDandCounter.keyset();
                System.debug('before for setOfAPIds::'+setOfAPIds);
                for(Id apId : setOfAPIds){
                    System.debug('apId::'+apId );
                    system.debug('mapkey ::'+mapOfParentIDandCounter.get(apId));
                    if(!mapOfParentIDandCounter.containskey(apId))
                        mapOfParentIDandCounter.put(apId,0);
                }
                
                for(Id accommodationId : mapOfParentIDandCounter.keyset()) {
                
                    Account_Participation__c accoutPart = new Account_Participation__c();
                    accoutPart.Id = accommodationId;
                    accoutPart.Number_Hotel_Rooms_for_Accommodations__c = mapOfParentIDandCounter.get( accommodationId );
                    listOfaccoutPart.add(accoutPart);
                }
                Update listOfaccoutPart;    
           // }                                 
        }catch(Exception e){
            handleException(e);
        }
        
        
    }
    
    
    /**
        @Purpose   : Trigger to Update 'Persons attending' on Hotel Room
        @Parameter : Trigger.new
        @Return:
    */
    public static void afertInsertUpdateHotelRoom(List<Accommodation__c> newAccommodationList) {
        // List of filter Accommodation records 
        List<Accommodation__c> listOfAccommodation = new List<Accommodation__c>();      
        try{
            for(Accommodation__c newAccommodation : newAccommodationList){          
                if(newAccommodation.Status__c != null &&                               
                   newAccommodation.Status__c.equalsIgnoreCase('booked') && 
                   newAccommodation.Hotel_Room__c != null /*&& 
                   newAccommodation.Salutation__c != null && 
                   newAccommodation.First_Name__c != null && 
                   newAccommodation.Last_Name__c != null*/){
                      System.debug('newAccommodation::'+newAccommodation); 
                      listOfAccommodation.add(newAccommodation);                        
                }
            }
            if(!listOfAccommodation.isEmpty()){ 
                newAccommodationList = new List<Accommodation__c>();
                newAccommodationList = getHotelRoomList(listOfAccommodation);
                System.debug('afterInsertUpdateAccommodation::'+newAccommodationList); 
                afterInsertUpdateAccommodation(newAccommodationList);
            }
        }
        catch(Exception e){
            handleException(e);
        }   
    }
    
    /**
      @Purpose   : Trigger to Update 'Persons attending' on Hotel Room
      @Parameter : Trigger.newmap,Trigger.oldmap.
      @Return:
     */
    public static void afterUpdateHotelRoom(Map<Id,Accommodation__c> newAccommodationMap, Map<Id,Accommodation__c> oldAccommodationMap) {
        // List of filter Accommodation records 
        Set<Id> listOfHotelRoomId = new Set<Id>();
        Accommodation__c oldAccommodation ;
        try{
            for(Accommodation__c newAccommodation : newAccommodationMap.values()){
                oldAccommodation = oldAccommodationMap.get(newAccommodation.id);
                if(newAccommodation.Status__c != null && 
                   newAccommodation.Hotel_Room__c != null){
                       
                    // Check if any modification in ‘First Name’, ‘Last Name’, ‘Salutation’
                    if( ( newAccommodation.Salutation__c != oldAccommodation.Salutation__c) || 
                        ( newAccommodation.First_Name__c != oldAccommodation.First_Name__c)||
                        ( newAccommodation.Last_Name__c != oldAccommodation.Last_Name__c) ||
                        ( newAccommodation.Status__c != oldAccommodation.Status__c) ||
                         //Henriette - start
                        ( newAccommodation.Hotel_Room__c != oldAccommodation.Hotel_Room__c )){
                         //Henriette - end
                           system.debug('listOfHotelRoomId::'+newAccommodation.Hotel_Room__c);
                           listOfHotelRoomId.add(newAccommodation.Hotel_Room__c);
                            //Henriette - start
                           listOfHotelRoomId.add(oldAccommodation.Hotel_Room__c);
                            //Henriette - end
                       } 
                }
            }
            if(!listOfHotelRoomId.isEmpty()){
                //Update 'Persons attending' on Hotel Room
                system.debug('listOfHotelRoomId2::'+listOfHotelRoomId);
                updateHotelRoom(listOfHotelRoomId);
            }
        }
        catch(Exception e){
            handleException(e);
        }   
    } 
    
    /**
        @Purpose   : Trigger to delet 'Persons attending' on Hotel Room
        @Parameter : Trigger.Old
        @Return:
    */
    public static void afterDeleteUpdateHotelRoom(List<Accommodation__c> oldAccommodationList) {
        // List of filter Accommodation records 
       Set<Id> listOfHotelRoomId = new Set<Id>();    
        try{
            for(Accommodation__c oldAccommodation : oldAccommodationList){          
                if(oldAccommodation.Status__c != null &&                               
                   oldAccommodation.Status__c.equalsIgnoreCase('booked') && 
                   oldAccommodation.Hotel_Room__c != null /*&& 
                   oldAccommodation.Salutation__c != null && 
                   oldAccommodation.First_Name__c != null && 
                   oldAccommodation.Last_Name__c != null*/){
                      System.debug('oldAccommodation::'+oldAccommodation); 
                      listOfHotelRoomId.add(oldAccommodation.Hotel_Room__c);                      
                }
            }
            if(!listOfHotelRoomId.isEmpty()){ 
                //Update 'Persons attending' on Hotel Room
                system.debug('listOfHotelRoomId2::'+listOfHotelRoomId);
                updateHotelRoom(listOfHotelRoomId);
            }
        }
        catch(Exception e){
            handleException(e);
        }     
    }
    
    /**
        @Purpose : get Hotel room records and update 'Persons attending' custom field 
        @Parameter : set of Hotel Id 
    */
    public static void updateHotelRoom( Set <Id> listOfHotelRoomId){
        List<Hotel_Room__c> hotelRoomList = new List<Hotel_Room__c>();
        try{
            hotelRoomList = getHotelist(listOfHotelRoomId);
            if(!hotelRoomList.isEmpty()){
                for(Hotel_Room__c hotelRoom : hotelRoomList){
                    
                    if(hotelRoom.Accommodations__r.size()>0 ){
                        String attendingList;
                        
                        for( Accommodation__c accommodation : hotelRoom.Accommodations__r){  
                            if(String.isNotBlank(accommodation.Status__c) && 
                               accommodation.Status__c.equalsIgnoreCase('booked')){
                                if(String.isBlank(attendingList)){
                                    attendingList = getattendingNameList( 
                                    accommodation ) ;
                                }else{
                                    attendingList += getattendingNameList( accommodation );
                                } 
                            } else if(String.isNotBlank(accommodation.Status__c) && 
                               accommodation.Status__c.equalsIgnoreCase('cancelled') ){
                                if(String.isBlank(attendingList)){
                                    attendingList = ' ';
                                }else{
                                    attendingList += ' ';
                                }
                            }
                        }
                        
                        if(attendingList.length() > 255 ){
                            attendingList = attendingList.substring(0,255);
                        }
                        hotelRoom.Persons_attending__c = attendingList;
                    }
                }
                if(!hotelRoomList.isEmpty()){
                    Update hotelRoomList;
                }
            }   
        }
        catch(Exception e){
            handleException(e);
        }       
    }
    
    /**
        @Purpose : get attending Name List
        @Parameter : accommodation
     */
    public static String getattendingNameList( Accommodation__c accommodation ){
        String strFullName = '';
        if( String.isNotBlank(accommodation.First_Name__c) || String.isNotBlank(accommodation.Last_Name__c) ) {
            if( String.isNotBlank(accommodation.Salutation__c) ) {
                strFullName += accommodation.Salutation__c;
            }
            if( String.isNotBlank(accommodation.First_Name__c) ) { 
                strFullName +=' ' + accommodation.First_Name__c;
            }
            if( String.isNotBlank(accommodation.Last_Name__c) ) { 
                strFullName += ' ' + accommodation.Last_Name__c;
            }
            strFullName+='\n';
        }  
        return strFullName ;      
    }
    
    /**
        Purpose: To get Hotel room records 
        Parameter : list to Records 
        
    */
    public static List<Hotel_Room__c> getHotelist( Set <Id> listOfHotelRoomId ){
        List<Hotel_Room__c> newHotelRoomList = new List<Hotel_Room__c>();
        try{
            newHotelRoomList = [ SELECT Id, Persons_attending__c, 
                                        ( SELECT Id, Name, Hotel_Room__r.Persons_attending__c, 
                                          First_Name__c, Last_Name__c, Salutation__c, Status__c 
                                          FROM Accommodations__r 
                                          /*Where Status__c = 'booked'*/) 
                                    FROM Hotel_Room__c 
                                    WHERE Id IN : listOfHotelRoomId];
        }catch(Exception e){
            handleException(e);
        }
        return newHotelRoomList;
    }
    
    
     /**
        @Purpose : To Update Hotel Persons_attending__c custom field 
        @Parameter : List Of Accommodation records 
    */ 

    public static void afterInsertUpdateAccommodation(List<Accommodation__c> newAccommodationList) {
        try{
            map<Id,List<Accommodation__c>> mapofIdAccommodation = new map<Id,List<Accommodation__c>>();
            List<Accommodation__c> listOfAccommodation = new List<Accommodation__c>();
            List<Hotel_Room__c> hotelList = new List<Hotel_Room__c>();
           
           if(!newAccommodationList.isEmpty()){
                for(Accommodation__c accommodation : newAccommodationList){                
                    if( mapofIdAccommodation.isEmpty() || !mapofIdAccommodation.containsKey(accommodation.Hotel_Room__c)) {
                        mapofIdAccommodation.put(accommodation.Hotel_Room__c,new List<Accommodation__c>());
                    }
                    mapofIdAccommodation.get(accommodation.Hotel_Room__c).add(accommodation);                   
                }
                
                if( !mapofIdAccommodation.isEmpty()){
                    Hotel_Room__c hotel;
                    for( Id hotelId : mapofIdAccommodation.keyset()){
                        List<Accommodation__c> listOfAccommodationRec = mapofIdAccommodation.get(hotelId);
                        
                        for( Accommodation__c accommodationRec : listOfAccommodationRec ){
                            String attendingList;
                            
                            if(String.isBlank(attendingList)){
                                attendingList = getattendingNameList(accommodationRec );
                            }else{
                                attendingList += getattendingNameList(accommodationRec );
                            }
                            
                            if(String.isNotBlank(attendingList)){
                                if(attendingList.length() > 255 ){    
                                    attendingList = attendingList.substring(0,255);
                                }
                                
                                hotel = new Hotel_Room__c();
                                hotel.id = accommodationRec.Hotel_Room__c;
                                System.debug('attendingList::'+attendingList);
                               
                               if( accommodationRec.Hotel_Room__r.Persons_attending__c != null ){
                                    hotel.Persons_attending__c = accommodationRec.Hotel_Room__r.Persons_attending__c+'\n '+attendingList;
                                }else{
                                    hotel.Persons_attending__c = attendingList;
                                }                                
                                hotelList.add(hotel);
                            }
                                                      
                        }
                    }
                }
            }
            if(!hotelList.isEmpty()){
                Update hotelList;
            }
        }catch(Exception e){
            handleException(e);
        }   
    }
    
    
    public static void updateContactParticiaption(Set<Id> setOfCPIds){
        if(setOfCPIds != null && !setOfCPIds.isEmpty()){
            List<Accommodation__c> accommodationList = new List<Accommodation__c>();
            
            accommodationList = [SELECT Contact_Participation__c 
                                 FROM Accommodation__c
                                 WHERE Contact_Participation__c IN :setOfCPIds
                                     AND Status__c = 'booked'];
                                     
            Map<Id,Integer> mapOfCPtoAccommodationCount = new Map<Id,Integer>();
            for(Accommodation__c accommodationRecord : accommodationList){
                if(!mapOfCPtoAccommodationCount.containskey(accommodationRecord.Contact_Participation__c))
                    mapOfCPtoAccommodationCount.put(accommodationRecord.Contact_Participation__c,0);
                    
                mapOfCPtoAccommodationCount.put(accommodationRecord.Contact_Participation__c,
                                                mapOfCPtoAccommodationCount.get(accommodationRecord.Contact_Participation__c)+1);
            }
            
            // correction of count for deleted or status changed scenario
            Set<Id> setOfAccommodationCPId = mapOfCPtoAccommodationCount.keySet();
            for(Id cpId :setOfCPIds){
                if(!setOfAccommodationCPId.contains(cpId))
                mapOfCPtoAccommodationCount.put(cpId,0);
            }
            
            List<Contact_Participation__c> contactParticipationList = new List<Contact_Participation__c>();
            for(Id cpId : mapOfCPtoAccommodationCount.keySet()){
                contactParticipationList.add(new Contact_Participation__c( Id = cpId,
                                                                           Number_of_Accommodations__c = mapOfCPtoAccommodationCount.get(cpId) ));
            } 
            
            
            
      System.debug('contactParticipationList==>'+contactParticipationList);
            update contactParticipationList;
        }
    }
    
    /**
        Purpose: To get Hotel room records 
        Parameter : list to Records 
        
    */
    public static List<Accommodation__c> getHotelRoomList( List<Accommodation__c> listOfAccommodation ){
        List<Accommodation__c> newAccommodationList = new List<Accommodation__c>();
        try{
            newAccommodationList = [ SELECT Id, Name, Hotel_Room__r.Persons_attending__c, 
                                     First_Name__c, Last_Name__c, Salutation__c, Status__c 
                                     FROM Accommodation__c Where Id IN :listOfAccommodation ];
        }catch(Exception e){
            handleException(e);
        }
        return newAccommodationList;
    }
    
    /**
        @Purpose : To handle Exception 
        @Parameter : Exception 
    */
    public static void handleException(Exception e) {
        System.debug('The following exception has occurred  ' + e.getMessage() +
                      'At line number :' + e.getLineNumber() + ' Error ' + e.getStackTraceString());
    }
}