/*  Test Class
    @purpose-This test class is developed to test the PicklistDescriber class
    @created_date-28-02-14
    @last_modified-[28-02-14]    
*/


@isTest
public class TestPicklistDescriber{
	/*  Test Method
        @purpose-This test method tests the functionality of PicklistDescriber class           
	*/
    public static testMethod void test() {
    	PicklistDescriber.describe('Account_Participation__c', 'Agent', 'Participation_Status__c');        
    }
}