public class assignNewlyCreatedAccountID
{
    public static ID accountId = null;
    
    public void assignAccountId(ID acctId)
    {
        accountId = acctID;
    }
    
    public static testMethod void testassignNewlyCreatedAccountID()
    {
        assignNewlyCreatedAccountID ancaid = new assignNewlyCreatedAccountID();
        Country__c ctry = new Country__c(Name = 'Afghanistan', Region__c = 'Middle East');
        insert ctry;
        
        Account acc = new Account(Name = 'Test Account', Status__c = 'Active', Mailing_Country__c = ctry.id);
        insert acc; 
        ancaid.assignAccountId(acc.Id); 
    }
    
}