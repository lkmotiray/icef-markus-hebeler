/*
    @Purpose : Get Id of current "ICEF_Event" record.
    @Created Date : 19/07/2017
*/
public class ShowSponershipPageCon {
    public Id ICEF_EventId {get; set;}
    
    public ShowSponershipPageCon(ApexPages.StandardController controller) {
        ICEF_EventId = ApexPages.currentPage().getParameters().get('Id');
    }  
    
    public ICEF_Event__c ICEF_EventRecord{
        get{
            ICEF_EventRecord = [SELECT Id,Event_Full_Name__c
                                FROM ICEF_Event__c 
                                WHERE Id =: ICEF_EventId];
            return ICEF_EventRecord;
        }
        set;
    }
}