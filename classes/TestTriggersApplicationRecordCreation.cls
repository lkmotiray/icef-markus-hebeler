public class TestTriggersApplicationRecordCreation
{    
    public static testMethod void test()
    {
         Country__c ctry = new Country__c(Name = 'China', Region__c = 'Asia');
         insert ctry;
         
         Lead l = new Lead(LastName='Test Lead', Company = 'Test Company', Country__c = 'China');
         insert l;    
         
         l.Events_interested_in__c = 'China';
         update l;
    }
    
    //test method for contactrecordtype trigger
    public static testMethod void test_contactrecordtype()
    {
         Country__c ctry = new Country__c(Name = 'Afghanistan', Region__c = 'Middle East');
         insert ctry;
         
         ID rt_id = null, rt_id1 = null; 
         List<RecordType> rt = [SELECT Id,Name FROM RecordType WHERE Name = 'Agent' and SobjectType='Account'];
         if(rt.size() != null)
             rt_id = rt[0].Id;
         Account acc = new Account(Name = 'Test Account', RecordTypeId = rt_id, Status__c = 'Active', Mailing_Country__c = ctry.id);
         insert acc;
         
         Application__c appl = new Application__c(First_Name__c = 'Test1', Last_Name__c = 'Test1', Account__c = acc.Id);
         insert appl;
         
         contact cont = new Contact(Salutation = 'Dr.', FirstName = 'Test', LastName = 'Test', Gender__c = 'male', Role__c = 'Assistant', Contact_Status__c = 'Active', AccountId = acc.Id);
         insert cont;
         
         List<RecordType> rt1 = [SELECT Id,Name FROM RecordType WHERE Name = 'Educator' and SobjectType='Account'];
         if(rt1.size() != null)
             rt_id1 = rt1[0].Id;
             
         acc.Educational_Sector__c = '0 u-n-k-n-o-w-n';
         acc.RecordTypeId = rt_id1;
         update acc;
    }
    
    //test method for updateAPRecordTypeAndAPStatus trigger
    public static testMethod void test_updateAPRecordTypeAndAPStatus()
    {
        Country__c ctry = new Country__c(Name = 'Afghanistan', Region__c = 'Middle East');
        insert ctry;
         
        ID rt_id = null, rt_id1 = null; 
        List<RecordType> rt = [SELECT Id,Name FROM RecordType WHERE Name = 'Agent' and SobjectType='Account'];
         if(rt.size() != null)
             rt_id = rt[0].Id;
        
        Account acc = new Account(Name = 'Test Account', RecordTypeId = rt_id, Status__c = 'Active', Mailing_Country__c = ctry.id);
        insert acc;
        
        contact cont = new Contact(Salutation = 'Dr.', FirstName = 'Test', LastName = 'Test', Gender__c = 'male', Role__c = 'Assistant', Contact_Status__c = 'Active', AccountId = acc.Id);
        insert cont;
        
        date dueDate = date.newInstance(2014, 1, 30);
        ICEF_Event__c icefEvent = new ICEF_Event__c(Name='Test IcefEvent', Event_City__c = 'City', technical_event_name__c = 'TestingEvent', Event_Manager_Educators__c = UserInfo.getUserId(), Event_Manager_Agents__c = UserInfo.getUserId(), Start_date__c=dueDate);
        insert icefEvent;   
        
        Account_Participation__c ap = new Account_Participation__c(Account__c = acc.Id, ICEF_Event__c = icefEvent.Id, Organisational_Contact__c = cont.Id, Participation_Status__c = 'accepted', Catalogue_Country__c = ctry.Id,Country_Section__c = ctry.Id);  
        insert ap;
        
        Contact_Participation__c cp = new Contact_Participation__c(Contact__c = cont.Id, ICEF_Event__c = icefEvent.Id, Participation_Status__c = 'registered', Catalogue_Position__c = 2,Account_Participation__c =ap.Id);
        insert cp;
        
        ap.Participation_Status__c = 'more info';
        update ap;
    }
    
    //test method for setCountryLookupOnCase trigger
    public static testMethod void test_setCountryLookupOnCase()
    {
        Country__c ctry = new Country__c(Name = 'Germany', Region__c = 'Europe (EU)');
        insert ctry;
        
        Case c = new Case(Country_of_Business_picklist__c = 'Germany', Status = 'New', Origin = 'Web');
        insert c;
    }
}