/**
 * This class is the Handler of the trigger 'LeadTrigger'. The 
 * class creates Recommendations for the Leads which are updated
 * as Converted Leads.
 *
 * @author          Jasmine J 
 * @created date    31/08/2015
 */

public class LeadTriggerHandler {
    
    public static Boolean hasExecuted = false;  // variable to avoid recursive update
    public static Map<String, Id> mapAccRecordTypeIds = new Map<String, Id>();
    
    /**
     * Creates a Recommendation record for the each Converted Lead.
     *
     * @param       listConvertedLeads    List of Converted Leads
     */
    public static void createRecommendation(List<Lead> listConvertedLeads) {
        
        hasExecuted = true; // Indicates if the trigger has been executed already in a single execution   
        List<Recommendation__c> listRecommendationsToInsert = new List<Recommendation__c>();
        Recommendation__c newRecommendation; 
        for(Lead updatedLead: listConvertedLeads) {
            System.debug('updatedLead ::' +updatedLead);
            newRecommendation = new Recommendation__c();
            
            // Verifies whether the values assigned from the Lead to the Recommendation record 
            // has a value and assigns it only if it has a value in the field.
            
            if(String.isNotBlank(updatedLead.Additional_Comments_by_Client__c))    
                newRecommendation.Additional_comments_by_client__c = updatedLead.Additional_Comments_by_Client__c;
            
            if(updatedLead.ICEF_Event__c != null)
                newRecommendation.ICEF_Event__c = updatedLead.ICEF_Event__c;
            
            if(updatedLead.Web_form_fill_out_Date__c != null)
                newRecommendation.Recommendation_Date__c = updatedLead.Web_form_fill_out_Date__c;
            
            if(updatedLead.Referring_Account__c != null)
                newRecommendation.Referring_Account__c = updatedLead.Referring_Account__c;
            
            if(updatedLead.Referring_Contact__c != null)
                newRecommendation.Referring_Contact__c = updatedLead.Referring_Contact__c;
            
            if(updatedLead.ConvertedContactId != null)
                newRecommendation.Recommended_Contact__c = updatedLead.ConvertedContactId;
            
            if(updatedLead.ConvertedAccountId != null) {                
                System.debug('updatedLead.ConvertedAccountId ::'+updatedLead.ConvertedAccountId);                
                newRecommendation.Recommended_Account__c = updatedLead.ConvertedAccountId;
            }
            
            // Add the new Recommendation record to list to insert in bulk
            listRecommendationsToInsert.add(newRecommendation); 
            System.debug('Recommendations to insert :: ' +listRecommendationsToInsert);
        }
        
        // Insert only if the list has records to update
        // Roll-back if any error occurs, hence try-catch not added
        if(!listRecommendationsToInsert.isEmpty())             
            insert listRecommendationsToInsert;        
    }
   
    
    /**
     * Updates the Accounts RecordType to 'Agent', if the Lead
     * recordType is 'Refer a Partner - Agent'.
     *
     * @param       listConvertedLeads    List of Converted Leads
     */
    public static void updateAccounts(List<Lead> listConvertedLeads, List<Lead> listLeadsForAccOwnerUpdate) {
        List<Id> listAccountId = new List<Id>(); 
        Set<Id> setAccountIdsForOwnerUpdate = new Set<Id>(); // to store the Accounts 
        													// whose Owner has to be updated
  		Map<Id, Id> mapLeadAccId = new Map<Id, Id>();
        Map<Id, Id> mapLeadCountryId = new Map<Id, Id>();
        Map<Id, Lead> mapLeads = new Map<Id, Lead>(); 
        
        mapAccRecordTypeIds = getMapOfAccRecordTypeIds();
        System.debug('mapAccRecordTypeIds :: '+mapAccRecordTypeIds);    
        
        Id idRecordType =  getRecordTypeId('Refer a Partner - Agent', 'Lead');
                    
        // Iterate through all the inserted Leads to update the Account
        for(Lead updatedLead: listConvertedLeads) { 
            
            // If RecordType of Lead is 'Refer a Partner - Agent', 
            // add the associated AccountId to the list listAccountId
            if(updatedLead.ConvertedAccountId != null) {
                
                // If RecordType is not null, only then process further
                if(idRecordType != null) {
                    System.debug('idRecordType ::' +idRecordType);
                    if(updatedLead.RecordTypeId == idRecordType) {                                    
                        System.debug('AccountId ::'+updatedLead.ConvertedAccountId);  
                        listAccountId.add(updatedLead.ConvertedAccountId);                                               
                    }
                }
            }                
        }
        System.debug('listLeadsForAccOwnerUpdate ::' +listLeadsForAccOwnerUpdate);
        for(Lead leadForAccOwnerUpdate: listLeadsForAccOwnerUpdate){
        	setAccountIdsForOwnerUpdate.add(leadForAccOwnerUpdate.ConvertedAccountId);
            mapLeadAccId.put(leadForAccOwnerUpdate.ConvertedAccountId, leadForAccOwnerUpdate.Id);  
            //if(leadForAccOwnerUpdate.Country_Lookup__c != null)
            	mapLeadCountryId.put(leadForAccOwnerUpdate.Id, leadForAccOwnerUpdate.Country_Lookup__c); 
            //else if (leadForAccOwnerUpdate.Country__c != null)
                //mapLeadCountryId.put(leadForAccOwnerUpdate.Id, leadForAccOwnerUpdate.Country__c); 
            mapLeads.put(leadForAccOwnerUpdate.Id, leadForAccOwnerUpdate);    
        }
            
        System.debug('mapLeadCountryId.values() ::' +mapLeadCountryId.values());
        Map<Id, Country__c> mapCountry = new Map<Id, Country__c>([SELECT Id, OwnerId, 
                                                                  Agent_Relationship_Manager__c, 
                                                                  Sales_Territory_Manager__c
                                                                  FROM Country__c
                                                                  WHERE Id IN :mapLeadCountryId.values()]);     
        System.debug('mapCountry ::' +mapCountry);            
        
        System.debug('setAccountIdsForOwnerUpdate ::' +setAccountIdsForOwnerUpdate);
        System.debug('mapLeadAccId ::' +mapLeadAccId);
        System.debug('mapLeadCountryId ::' +mapLeadCountryId);
        
        // List to hold the account records to update
        Set<Account> setAccountToUpdate = new Set<Account>(); 
        Id recordTypeId;
        // Get recordType Id of Agent Account
        if(mapAccRecordTypeIds != null)
        	recordTypeId = mapAccRecordTypeIds.get('Agent');
        System.debug('recordTypeId ::'+recordTypeId);
        
        // To find existing account,
        // check if the created datetime is more than 2 minutes.
        // If yes, the Account is considered existing Account.
        DateTime currentDateTime = DateTime.now();
        DateTime newDateTime = currentDateTime.addMinutes(-2);
        System.debug('newDateTime ::' +newDateTime);
        
        if(!setAccountIdsForOwnerUpdate.isEmpty()) {
            Set<Id> setAccIds = new Set<Id>();
            // If RecordType is not null, only then process further
            if(recordTypeId != null){
                
                if(!listAccountId.isEmpty()){
                    // Update Account only if the Account is a new account 
                    // and not an existing Account.                              
                    for(Account acc: [SELECT Id, Name, RecordTypeId, CreatedDate
                                      FROM Account 
                                      WHERE Id IN :listAccountId AND CreatedDate > :newDateTime]) {                        
						acc.RecordTypeId = recordTypeId;
						Id leadId, countryId;
						if(!mapLeadAccId.isEmpty() && mapLeadAccId.containsKey(acc.Id)) {
						    leadId = mapLeadAccId.get(acc.Id);
							if(leadId != null) {
							    if(!mapLeadCountryId.isEmpty() && mapLeadCountryId.containsKey(leadId)) {
								    countryId = mapLeadCountryId.get(leadId);   
									if(countryId != null) {
									    if(!mapCountry.isEmpty()  && mapCountry.containsKey(countryId)) {
											acc.OwnerId = mapCountry.get(countryId).Agent_Relationship_Manager__c;
										}
									}
								}
							}
						}
						System.debug('Account ::'+acc);
						setAccountToUpdate.add(acc);
						setAccIds.add(acc.Id);
					}
                    System.debug('setAccountToUpdate ::'+setAccountToUpdate);                        
                }
            }
                
            Set<Account> setForAccOwnerUpdate = updateAccountOwner(setAccountIdsForOwnerUpdate, 
                                                                   newDateTime, mapLeadAccId, 
                                                                   mapLeadCountryId, mapCountry, 
                                                                   mapLeads);
            
            if(!setForAccOwnerUpdate.isEmpty()) {
            	for(Account acc: setForAccOwnerUpdate){
                    if(!setAccIds.contains(acc.Id))
                        setAccountToUpdate.add(acc);   
                }    
            }                
            System.debug('setAccIds ::' +setAccIds);
            System.debug('setAccountToUpdate ::'+setAccountToUpdate);
            
            List<Account> listAccountUpdates = new List<Account>();
            listAccountUpdates.addAll(setAccountToUpdate);
            System.debug('listAccountUpdates ::' +listAccountUpdates);
            
            // Roll-back if any error occurs, hence try-catch not added
            if(!listAccountUpdates.isEmpty()) 
                update listAccountUpdates;
        }        
    }
    
    
    /**
     * Gets the RecordType Id for Object
     *
     * @param       strRecordTypeName    RecordType Name
     * @param       strObjectName        Object Name whose record type id is to be found
     */
    public static Id getRecordTypeId(String strRecordTypeName, String strObjectName) {
        Id recordTypeId;
        RecordType recType = new RecordType();
        // Gets the Recordtype Id, given the Recordtype Name and Object name       
        try {
            recType = [SELECT Id, Name 
                                  FROM RecordType 
                                  WHERE (Name = :strRecordTypeName
                                    AND SObjectType = :strObjectName)];
            System.debug('recType Id ::' +recType.Id);   
            recordTypeId = recType.Id;
        }
        catch(Exception e){
            System.debug('Exception while fetching recordtype ::' +e);      
        }
        return recordTypeId;
    }
    
    /**
     * Gets the map of Account RecordType Ids.
     *
     * @return       mapAccRecordTypeId    Map of required Account RecordTypes
     */
    public static Map<String, Id> getMapOfAccRecordTypeIds() {
        Map<String, String> mapAccRecordTypes = new Map<String, String>();
        Map<String, Id> mapAccRecordTypeId = new Map<String, Id>();
        try {
            mapAccRecordTypes = createMapOfReqAccountRecordTypes();
            
            if(!mapAccRecordTypes.isEmpty()) {
            	for(RecordType recType : [SELECT Id, Name 
                                      FROM RecordType 
                                      WHERE (Name = :mapAccRecordTypes.keySet()
                                        AND SObjectType = 'Account')]) {
                	mapAccRecordTypeId.put(recType.Name, recType.Id);		    
            	}
            	System.debug('mapAccRecordTypeId :: '+mapAccRecordTypeId);    
            }
        }
        catch(Exception e){
            System.debug('Exception while fetching recordtype ::' +e);      
        }
        return mapAccRecordTypeId;
    }
    
    /**
     * Updates the Account Owner of the Newly Created Lead Converted Accounts
     *
     * @param       setAllConvertedAccountIds	RecordType Name
     * @param       newDateTime        			Date Time to recognize the new Accounts
     * @param       mapLeadAccId        		Map of Lead and the corresponding ConvertedAccount Id
     * @param       mapLeadCountryId        	Map of Lead and the corresponding Country Id
     * @param       mapCountry        			Map of countries whose value is to be used to 
     * 											populate the Owner values
     * @param       mapLeads        			Map of leads inserted
     * 
     * @return      setAccountToUpdate        	Set of Accounts to update
     */
    public static Set<Account> updateAccountOwner(Set<Id> setAllConvertedAccountIds, DateTime newDateTime,
                                                  Map<Id, Id> mapLeadAccId, Map<Id, Id> mapLeadCountryId, 
                                                  Map<Id, Country__c> mapCountry, Map<Id, Lead> mapLeads) {
        Set<Account> setAccountToUpdate = new Set<Account>();
        try {                                             
            // Get recordType Id of Agent Account
            Id recordTypeId = mapAccRecordTypeIds.get('Agent');
            System.debug('recordTypeId ::' +recordTypeId);
                                                          
            // Get recordType Id of Educator Account
            Id recordTypeIdEducator = mapAccRecordTypeIds.get('Educator');
            System.debug('recordTypeId ::' +recordTypeIdEducator);
            
            // Get recordType Id of Service Provider Account
            Id recordTypeIdServProvidr = mapAccRecordTypeIds.get('Service Provider'); 
            System.debug('recordTypeId ::' +recordTypeIdServProvidr); 
        
        	// Update Account Owner
            for(Account acc: [SELECT Id, Name, RecordTypeId, CreatedDate
                              FROM Account 
                              WHERE Id IN :setAllConvertedAccountIds AND CreatedDate > :newDateTime]) { 
                System.debug('acc before modification::'+acc);                  
                Id leadId, countryId;
                if(acc.RecordTypeId == recordTypeId) {
                    // Account RecordType is 'Agent'
                    if(!mapLeadAccId.isEmpty() && mapLeadAccId.containsKey(acc.Id)) {
                		leadId = mapLeadAccId.get(acc.Id);
                        if(leadId != null) {
                            System.debug('leadId ::'+leadId);
                            if(!mapLeadCountryId.isEmpty() && mapLeadCountryId.containsKey(leadId)) {                                
                                countryId = mapLeadCountryId.get(leadId);     
                                if(countryId != null) {
                                    if(!mapCountry.isEmpty()  && mapCountry.containsKey(countryId)) {
                                        acc.OwnerId = mapCountry.get(countryId).Agent_Relationship_Manager__c; 
                                    }
                                }
                            }
                        }
                    }
                }	
                else if(acc.RecordTypeId == recordTypeIdEducator && recordTypeIdEducator != null) {
                    // Account RecordType is 'Educator'
                    if(!mapLeadAccId.isEmpty() && mapLeadAccId.containsKey(acc.Id)) {
                		leadId = mapLeadAccId.get(acc.Id);
                        if(leadId != null) {
                        	System.debug('leadId ::'+leadId);
                            if(!mapLeadCountryId.isEmpty() && mapLeadCountryId.containsKey(leadId)) {              
                                countryId = mapLeadCountryId.get(leadId);
                                if(countryId != null) {
                                    if(!mapCountry.isEmpty()  && mapCountry.containsKey(countryId)) {
                                		acc.OwnerId = mapCountry.get(countryId).Sales_Territory_Manager__c; 
                                    }
                                }
                            }
                        }
                    }
                }
                else if(acc.RecordTypeId == recordTypeIdServProvidr && recordTypeIdServProvidr != null) {
                    // Account RecordType is 'Service Provider'
                	if(!mapLeadAccId.isEmpty() && mapLeadAccId.containsKey(acc.Id)) {
                		leadId = mapLeadAccId.get(acc.Id);
                        if(leadId != null) {
                        	System.debug('leadId ::'+leadId);
                            if(!mapLeadCountryId.isEmpty() && mapLeadCountryId.containsKey(leadId)) {              
                                countryId = mapLeadCountryId.get(leadId);
                                if(countryId != null) {
                                    if(!mapCountry.isEmpty()  && mapCountry.containsKey(countryId)) {
                                		acc.OwnerId = mapCountry.get(countryId).Sales_Territory_Manager__c; 
                                    }
                                }
                            }
                        }
                    } 
                }
                else {
                    if(!mapLeadAccId.isEmpty() && mapLeadAccId.containsKey(acc.Id)) {
                        // Account RecordType is not 'Educator' / 'Agent' / 'Service Provider'
                        leadId = mapLeadAccId.get(acc.Id);
                        if(leadId != null) {
                            System.debug('leadId ::'+leadId);
                            if(!mapLeads.isEmpty() && mapLeads.containsKey(leadId)) {
                                acc.OwnerId	= mapLeads.get(leadId).OwnerId;
                            }
                        }
                    }
                }
                System.debug('Account ::'+acc);
                setAccountToUpdate.add(acc);
            }
            System.debug('setAccountToUpdate ::'+setAccountToUpdate);    
        }
        catch(Exception e){
            System.debug('Exception ::' +e);      
        }
        return setAccountToUpdate;
    }
    
    /**
     * Updates the Account Owner of the Newly Created Lead Converted Accounts
     * 
     * @return      mapAccRecordTypes	Map of the Account recordtypes whose
     * 									id is required
     */
    private static Map<String, String> createMapOfReqAccountRecordTypes() {
        
        Map<String, String> mapAccRecordTypes = new Map<String, String>();
        
		mapAccRecordTypes.put('Educator', 'Account'); 
        mapAccRecordTypes.put('Service Provider', 'Account');        
        mapAccRecordTypes.put('Agent', 'Account');
                                                      
        System.debug('mapAccRecordTypes ::' +mapAccRecordTypes); 
        return mapAccRecordTypes;
    }
}