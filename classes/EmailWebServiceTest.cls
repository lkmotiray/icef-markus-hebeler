@isTest(seeAllData=true)
private class EmailWebServiceTest {
    
    testmethod static void testSendEmailService() {
        
        TEST.startTest();
        List<String>  invoicesId = new List<String>();
       /* for(c2g__codaInvoice__c invoice : [ SELECT Id 
                                            FROM c2g__codaInvoice__c 
                                            LIMIT 10 ]) {
            invoicesId.add(invoice.id);    
        }*/
        
        for(c2g__codaInvoice__c invoice : [ SELECT Id, c2g__InvoiceTotal__c, 
                                                   c2g__OutstandingValue__c
                                            FROM c2g__codaInvoice__c LIMIT 10]) {
            if( invoice.c2g__InvoiceTotal__c == invoice.c2g__OutstandingValue__c )                                                
                invoicesId.add(invoice.id);    
        }
        
        if(!invoicesId.isEmpty()){
            EmailWebService.sendEmail(invoicesId);
        }
        TEST.stopTest();
    }
    
}