/*  Class
    @purpose-This class is developed to parse a XML document retireve the related value and 
             accordingly update the exchange rates in the org.
    @created_date-09-08-13
    @last_modified-[14-08-13]    
*/

global with sharing class generateExchangeRatesRBA implements Database.AllowsCallouts
{
    public static List<c2g__codaExchangeRate__c> exchangeRates;             //to store the current exchange rates       
    public static List<c2g__codaAccountingCurrency__c> allAcctCurrencies;   //to store the current accounting currencies
    public static String UnitTestXml;                                       //to store the requested XML doc
    
    
    
    /*  Function
    @purpose-This function is used to retrieve and insert the updated exchange rates to org    
    @last_modified-[14-08-13]
    */
    
    @future(callout=true)                                                   //this allows the http callouts within method
    public static void insertExchageRates()                                 
    {        
        HttpRequest req = new HttpRequest();                                //used to send as a part of http callout
        Http http = new Http();
        req.setMethod('GET');                                               //type of method 'GET'
        String exchangeRatesDoc;                                            //to store the body of the response
                
        String url = 'https://www.rba.gov.au/rss/rss-cb-exchange-rates.xml'; //URL for fetching XML doc   
        req.setEndpoint(url);             

        
        if(UnitTestXml == null)                                             //if requested within function
        {
            HTTPResponse response = http.send(req);
            System.debug('response:::'+response);
            exchangeRatesDoc = response.getBody();
        }
        else
            exchangeRatesDoc = UnitTestXml;                                 ////if requested from outside function
        System.debug('exchangeRatesDoc:::'+exchangeRatesDoc);
        
        //XML parsing
        DOM.Document doc = new DOM.Document();
        doc.load(exchangeRatesDoc);   
        DOM.XMLNode root = doc.getRootElement();
        
        List<Dom.XmlNode> lstEntries = DOM_Utils.getElementsByName(root, 'item');
        System.debug('size of list'+lstEntries.size());                     //to check the size of list
        
        exchangeRates = new List<c2g__codaExchangeRate__c>();
        allAcctCurrencies = new List<c2g__codaAccountingCurrency__c>();
        
        Date exchangeStartDate;                                             //to store the start date for exchange rate
        String str_exchangeStartDate;                                       //to store the stop date for exchange rate
        
        ID cmpnyId = null;                                                  //ID to store company ID
        List<c2g__codaCompany__c> cmpny = [Select Id, Name from c2g__codaCompany__c where Name = 'ICEF Asia Pacific'];
        System.debug('company size:::'+cmpny.size());   
        
        if(cmpny.size() != 0)
        {
            //initialize cmpnyId to 1st and only company with name "ICEF Asia Pacific"
            cmpnyId = cmpny[0].Id;                      
        }
        
        
        //fetch the relevant records by verifying each node fetched from XML        
        for(Dom.XmlNode entry: lstEntries)
        {   
            //to retireve the start date
            List<Dom.XmlNode> lstDateEntries = DOM_Utils.getElementsByName(entry, 'period');        
            str_exchangeStartDate=lstDateEntries[0].getText();
            exchangeStartDate=Date.valueOf(str_exchangeStartDate);              //Convert string to date
            System.debug('Start Dtae is::::'+str_exchangeStartDate);
            
            //to retrieve the target currency and value
            List<Dom.XmlNode> lstTargetCurrencyEntries = DOM_Utils.getElementsByName(entry, 'targetCurrency'); 
            List<Dom.XmlNode> lstChildEntries = DOM_Utils.getElementsByName(entry, 'value');  
            System.debug('Number of targetCurrencies::::'+lstTargetCurrencyEntries.size());
            System.debug('Number of values::::'+lstChildEntries.size());
            
            //Check of the target currency is either USD or EUR
            for(Dom.XmlNode target: lstTargetCurrencyEntries)
            {
                String target_str= target.getText();
                if((target_str.equals('EUR'))||((target_str.equals('USD'))))
                {
                        System.debug('Inside');
                    
                        for(Dom.XmlNode node:lstChildEntries)
                        {
                                
                            string value_str=node.getText();                //retrieves the value from XML
                            Decimal value = Decimal.valueof(value_str);     //converting the value to decimal
                            System.debug('rate:::'+value);                  
                            
                            
                            
                            //fetch the accounting currencies with the selected company ID
                            
                            List<c2g__codaAccountingCurrency__c> acctCurrency = [Select ID, Name, c2g__OwnerCompany__c from c2g__codaAccountingCurrency__c where c2g__Home__c = False and c2g__OwnerCompany__c =: cmpnyId];
                            System.debug('Size of AccountingCurrency'+acctCurrency.size());
                            if(acctCurrency.size() != 0)
                            {
                                
                                for(integer i = 0; i < acctCurrency.size(); i++)
                                {
                                    
                                    //add each fetched account currency to allAccCurrencies List
                                    allAcctCurrencies.add(acctCurrency[i]);
                                    
                                    //fetch all the exchange rates with selected acctCurrency and exchangeStartDate
                                    List<c2g__codaExchangeRate__c> allExchangeRates = [Select c2g__ExchangeRateCurrency__c, c2g__StartDate__c from c2g__codaExchangeRate__c where c2g__ExchangeRateCurrency__c =: acctCurrency[i].Id AND c2g__StartDate__c =: exchangeStartDate];
                                    System.debug('AccountCurrencyID:::'+acctCurrency[i].Id);
                                    System.debug('size of all Exchange rates::'+allExchangeRates.size());
                                    
                                    if(target_str.equals(acctCurrency[i].Name))
                                    {
                                        if(allExchangeRates.size() == 0)
                                        {                                        
                                            //if no exhange rates are fetched then create new rate
                                            c2g__codaExchangeRate__c eRate = new c2g__codaExchangeRate__c(c2g__ExchangeRateCurrency__c = acctCurrency[i].Id, c2g__Rate__c = value, c2g__StartDate__c = exchangeStartDate);                                        
                                            exchangeRates.add(eRate);   
                                            System.debug('Final Inside');
                                        }                                        
                                    
                                    }                                                                                                   
                                }                                                                        
                            }                
                        }               
                }              
            }
        }
        System.debug('Size of records to be inserted'+exchangeRates.size());                
              
        
        try
        {         
            if(exchangeRates.size() != 0)
            {
                    //accounting currencies for ID
                    List<c2g__codaAccountingCurrency__c> acctCurrency = [Select ID, Name, c2g__OwnerCompany__c from c2g__codaAccountingCurrency__c where c2g__Home__c = False and c2g__OwnerCompany__c =: cmpnyId];
                    ID newID1,newID2;                                           //temporory IDs
                
                    for(c2g__codaAccountingCurrency__c acct:acctCurrency)
                    {
                        if(acct.Name=='USD')
                            newID1=acct.ID;
                        if(acct.Name=='EUR')
                            newID2=acct.ID;
                    }
                
                	System.debug('exchangeRates lst:::'+ json.serialize(exchangeRates));   
                
                    //Insert exchange rates
                    insert exchangeRates;
                
                    //fetch the inserted records for count verification
                    List<c2g__codaExchangeRate__c> usdLst=[Select name from c2g__codaExchangeRate__c where c2g__StartDate__c =: System.today() and c2g__ExchangeRateCurrency__c=: newID1];
                    List<c2g__codaExchangeRate__c> eurLst=[Select name from c2g__codaExchangeRate__c where c2g__StartDate__c =: System.today() and c2g__ExchangeRateCurrency__c=: newID2];                       
                             
                    System.debug('Size of USD EUR lst:::'+usdLst.size()+' '+eurLst.size());    
                        
                    
                    //Send email after successful insertion of exchange rates with recent five values of exchange rates
                    User u = [Select Name, Email, Id from User where Id =: UserInfo.getUserId()];
                    
                    String subject = 'Exchange Rates are inserted successfully';
                    String body = 'Hello '+u.Name + ',';
                    body += '<br></br><br></br>';
                    body += 'Exchange Rates are inserted successfully. Following are the recent five exchage rates for particular Exchange Rate Currency. <br></br><br></br>';
                    body += 'Exchange Rate Currency | Rate | Start Date<br></br><br></br>------------------------------------------------------------<br></br><br></br>';
                    for(integer i = 0; i < allAcctCurrencies.size(); i++)
                    {
                        List<c2g__codaExchangeRate__c> allExchRates = [Select c2g__ExchangeRateCurrency__c, c2g__Rate__c, c2g__StartDate__c from c2g__codaExchangeRate__c where c2g__ExchangeRateCurrency__c =: allAcctCurrencies[i].Id ORDER BY c2g__StartDate__c DESC LIMIT 5];
                        for(integer j = 0; j < allExchRates.size(); j++)
                        {
                            body += allAcctCurrencies[i].Name + ' | ' + allExchRates[j].c2g__Rate__c + '  | ' + allExchRates[j].c2g__StartDate__c;
                            body += '<br></br>';
                        }  
                        body += '------------------------------------------------------------<br></br><br></br>';              
                    }
                    
                    body += 'Thanks,';
                    
                    string[] toemailaddr = new string[]{};
                    
                    //toemailaddr.add(u.Email);
                    toemailaddr.add('jsarfraz@icef.com');
                    
                    sendEmail(subject, body, toemailaddr);
                
                    //throw the exception in case of duplicate records
                    if((usdLst.size()>1)||(eurLst.size()>1))  
                    {                    
                        Exception e;
                        throw(e);
                    }                
                
            }            
        }catch(Exception e)
        {
            //Send email after unsuccessful insertion of exchange rates with error
            User u = [Select Name, Email, Id from User where Id =: UserInfo.getUserId()];            
            
            String subject = 'Exchange Rates are not inserted successfully';
            String body = 'Hello '+u.Name + ',';
            body += '<br></br><br></br>';
            body += 'Exchange Rates are not inserted successfully, because of following error: <br><br/>'+ e + '.' + '<br></br><br></br>';
            
            String error = e.getMessage();
            
            if(error.contains('multi-company mode'))
            {
                body += 'Please select only one company as the current company. Because data entry is disabled until the user returns to single-company mode.<br></br><br></br>'; 
            }
            
            body += 'Please do the following steps to schedule <b>Update Exchange Rates</b> job again:<br></br>';
            body += '1. Go to Setup -> Administrative Setup -> Monitoring -> Scheduled Jobs <br></br>'; 
            body += '2. Delete <b>Update Exchage Rate Schedule</b> scheduled job<br></br>'; 
            body += '3. Click Accounting Currencies tab, then Click on Go<br></br>';
            body += '4. Click on <b>Update Exchange Rates AUD</b> button<br></br><br></br>';
            
            body += 'Thanks,';
            
            string[] toemailaddr = new string[]{};
            
            //toemailaddr.add(u.Email); 
            toemailaddr.add('jsarfraz@icef.com');
            toemailaddr.add('jlove@icef.com');       
            toemailaddr.add('mhebeler@icef.com');            
            sendEmail(subject, body, toemailaddr);            
        }
    }      
    
    
    /*  Function
    @purpose-This function is used to send the mail    
    @last_modified-[09-08-13]
    */
    
    public static void sendEmail(string subject, string body, string[] toemailaddr)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();   
        
        email.setSubject(subject);            
        
        email.setHtmlBody(body);            
        
        email.setToAddresses(toemailaddr);
        
        email.setUseSignature(true);       
                
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });    
    }
    
    /*  Function
    @purpose-This function is used to schedule the insertExchageRates() function for regular interval
    @last_modified-[09-08-13]
    */
    webService Static void scheduleUpdateExchangeRatesRBA()
    {
        insertExchageRates();
        string cronId = '';       
        
        scheduleGenerateExchangeRatesRBA m = new scheduleGenerateExchangeRatesRBA();        //object of sheduler class
        if(!Test.isRunningTest()){
            cronId = system.schedule('Update AUD Exchage Rate Schedule', '0 0 17 * * ?', m);     
        }
    }
        
}