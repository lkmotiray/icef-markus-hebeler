@isTest
class SetDateActedUponTest
{
    static testmethod void Test_Mesthod()
    {
        //insert country
        Country__c mailingCountry = new Country__c(Name = 'Test');
        insert mailingCountry;
        
        //insert account
        Account testAccount = new Account(Name = 'Test Account',
                                          Mailing_Country__c = mailingCountry.Id);
        insert testAccount;
        
        //insert contact                                 
        Contact testContact = new Contact(lastName='Test Contact',
                                          Gender__c = 'male',
                                          AccountId = testAccount.Id,
                                          Salutation = 'Mr');
        insert testContact;
        
        List<Id> contactIds = new List<Id>();
        contactIds.add(testContact.Id);
        
        String result = SetDateActedUpon.setDateActedUpon(contactIds);
        
        System.assertEquals(result,'Please reload list view to finalise removal.');
    }
}