/*  Test Class
    @purpose-This test class is developed to test the scheduleGenerateExchangeRatesRBA class
    @created_date-09-08-13
    @last_modified-[19-08-13]    
*/

@isTest
public class testScheduleGenerateExchangeRatesRBA{
    
    /*  Function
    @purpose-This function is used to test the scheduled job        
    @last_modified-[19-08-13]
	*/
	public static testMethod void testSchedule()
    {
    	Test.StartTest();										//Start Test
              
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new mockHttpExchangeRatesRBA());
        
        
        // Call method to test.
        // This causes a dummy response to be sent from the class that implements HttpCalloutMock.
        //call the getInfoFromExternalService() method from another test class testGenerateExchangeRatesRBA
        
        HttpResponse res=testGenerateExchangeRatesRBA.getInfoFromExternalService();
        String str=res.getBody();
        System.debug('response...:'+str);
             
        
        //execute the methods of generateExchangeRatesRBA
        generateExchangeRatesRBA.UnitTestXml=str;
        
        
        
        	//object of class for argument to the schedule method
        scheduleGenerateExchangeRatesRBA sgerRBA = new scheduleGenerateExchangeRatesRBA();	
        String sch = '0 0 23 * * ?';							//time and date string
        string jobId = system.schedule('Test update exchange rate schedule', sch, sgerRBA);
        
        	//object of Cron Trigger to verify the CronExpression and TimesTriggered 
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals('0 0 23 * * ?',ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        
        Test.stopTest();										//Stop Test
    }
}