/**********
 * purpose: update account on changes of Contact
 * 			- Number_of_PMD_Contacts__c: Number of Contacts with value "Primary Decision Maker" in field Role__c
 *          - X20_year_mag__c (Boolean): true if there is any Contact with x20_year_mag__c checkbox checked (=> Insights_Magazine_text__c = "yes"), false if none
 * 			- Has_Contact_with_emailaddress__c (Boolean): True if any of the contacts has an email address, false if none
 * created Date: 22.11.2018
 * developer: Henriette Schönleber 
 * ********/

public class ContactHandler {
	
    //Id set of Accounts that hage to be checked
    Set<Id> accountIds = new Set<Id>();
    //set boolean to avoid multiple updates
    Boolean isFirstTime = true;
    
    public void checkAccountFields (List<Contact> contactList){
        //get Account Ids of Contacts, add to account set
        for(Contact con : contactList){
            accountIds.add(con.AccountId);
        }
        accountIds.remove(null);
        if(accountIds.size() > 0) {
	        //get existing values of account for field to check 
    	    Map<Id,Account> accountsToCheck = new Map<Id,Account>([SELECT X20_year_mag__c, Number_of_PMD_Contacts__c, Has_Contact_with_emailaddress__c
        	                                FROM Account 
            	                            WHERE Id IN :accountIds]);
 			//calculate new values
            AggregateResult[] allContacts = [SELECT AccountId, count(Insights_Magazine_text__c), count(Contact_Email__c), Count(is_PDM__c) 
                                             FROM Contact
                                             WHERE AccountId IN :accountIds
                                             AND Contact_Status__c = 'Active'
                                             GROUP BY AccountId
                                          	]; 
            System.debug('allContacts: '+allContacts);
            //Mapping of new Values by Account Id for comparison
            Map<Id, Map<String, Integer>> mapAccountIdNewValues = new Map<Id, Map<String, Integer>>();
            System.debug('aggregateResultSize: ' + allContacts.size());
            if (allContacts.size() > 0){
      	      for (AggregateResult ar : allContacts){
        	        Id acctId = (Id)ar.get('AccountId');
            	    Map<String, Integer> valueMap = new Map<String, Integer>();
                	valueMap.put('insights', (Integer)ar.get('expr0'));
                	valueMap.put('hasEmail', (Integer)ar.get('expr1'));
                	valueMap.put('PDMs', (Integer)ar.get('expr2'));
                
               	 	mapAccountIdNewValues.put(acctId, valueMap);
            	}
            }
            System.debug('mapAccountIdNewValues: ' + mapAccountIdNewValues);
            
            List<Id> accountsToUpdate = new List<Id>();
            //compare values and update account if necessary

                
            Boolean insightsBool = false;
            Boolean emailBool = false;
            Integer pdms = 0;
            
            for(Id acctId : accountIds){
                
                if(mapAccountIdNewValues.containsKey(acctId)){
                	insightsBool = getBooleanfromInteger(mapAccountIdNewValues.get(acctId).get('insights'));
                	emailBool = getBooleanfromInteger(mapAccountIdNewValues.get(acctId).get('hasEmail'));
                	pdms = mapAccountIdNewValues.get(acctId).get('PDMs');
                } else {
                    //no active contact left, null all values in account
                    insightsBool = false;
                    emailBool = false;
                    pdms = 0;
                }
                
                if ((Integer)accountsToCheck.get(acctId).Number_of_PMD_Contacts__c != pdms){
                    accountsToUpdate.add(acctId);
                } else if (accountsToCheck.get(acctId).X20_year_mag__c != insightsBool){
                    accountsToUpdate.add(acctId);
                } else if (accountsToCheck.get(acctId).Has_Contact_with_emailaddress__c != emailBool){
                    accountsToUpdate.add(acctId);
                }
            }        
            if (accountsToUpdate.size() > 0){
                System.debug('accounts to update: ' + accountsToUpdate);
                List<Account> acctListToUpdate = new List<Account>();
                for (Id acctId : accountsToUpdate){
                    Account acct = accountsToCheck.get(acctId);
                    acct.X20_year_mag__c = insightsBool;
                    acct.Has_Contact_with_emailaddress__c = emailBool;
                    acct.Number_of_PMD_Contacts__c = pdms;
                    acctListToUpdate.add(acct);
                }
                Database.update(acctListToUpdate);
            } else {
                System.debug('AccountRelatedContactTrigger: no account to update');
            }
        }
    }
    
    public void checkUpdate(List<Contact> newconList){
        if (isFirstTime){
            isFirstTime = false;
            checkAccountFields(newConList);
        }else{
            System.debug('AccountRelatedContactTrigger: Once is enough!');
        }
    }
    
    public Boolean getBooleanFromInteger(Integer num) {
        Boolean bool = false;
        if (num > 0){
            bool = true;
        }
        return bool;
    }
    
}