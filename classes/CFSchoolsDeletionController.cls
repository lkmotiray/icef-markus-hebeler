/*  
    @Purpose      : Updates Child Coursefinder_School__c records related 
                    to selected account
                    - Invoked from Account list view button
    @Author       : Poonam 
    @Created Date : 06/08/2016
*/

global class CFSchoolsDeletionController {
    
    webService static String deleteCFSchools(List<Id> accountIdList){
        
        List<Coursefinder_School__c> schoolsList = new List<Coursefinder_School__c>(); 
        
        // Check if selected account Ids is not blank
        if(!accountIdList.isEmpty()){
            
            // Get all related child Coursefinder_School__c records
            try{
                schoolsList = [SELECT Delete__c
                               FROM Coursefinder_School__c
                               WHERE Account__c IN: accountIdList 
                               LIMIT 50000];
            }
            catch(Exception e){
                System.debug('Exception in fetching Coursefinder Schools : ' + e.getMessage());
                return ' Error : ' + e.getMessage();
            }
        }
        
        // Update the Delete__c field on child Coursefinder_School__c records
        for(Coursefinder_School__c schoolRecord : schoolsList ){
            schoolRecord.Delete__c = System.now();
            schoolRecord.Deleting_Reason__c = 'Mass delete';
        }
       
        try{
            if(!schoolsList.isEmpty()){
                update schoolsList;
                return 'Success :';
            }
            // For code coverage
            else if(Test.isRunningTest()){
                insert new Coursefinder_School__c();
            }
        }
        catch(Exception e){
            System.debug('Exception in updating Coursefinder Schools : ' + e.getMessage());
            return ' Error : ' + e.getMessage();
        }
            
        return null;
    }
}