// send error handling emails
public class EmailUtil {
    
    public static void sendErrorEmail(String subject, String body){
    	
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {'hschoenleber@icef.com'};
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('My Apex error message');
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    }
        
}