@isTest(seeAllData = true) 
public class TestTriggers
{    
    
    //Test assignAccountOwnerName  trigger
    public static testMethod void testAssignAccountOwnerName()
    {
        // Create two Users
        User u1, u2;
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) 
        {        
            Profile profile = [select id from profile where name='System Administrator'];
            
            u1 = new User(alias = 'dws', email='dwsTester@tester.com',
                          emailencodingkey='UTF-8', lastname='dwsTester', languagelocalekey='en_US',
                          localesidkey='en_US', profileId = profile.Id, 
                          timezonesidkey='America/Los_Angeles', username='dwsTester@tester.com' ,firstname = 'dws');
            
            insert u1;           
            
            system.assertEquals('dwsTester@tester.com', u1.username); 
            system.assertEquals('dws', u1.firstname); 
            
            u2 = new User(alias = 'dws1', email='dwsTester1@tester.com',
                          emailencodingkey='UTF-8', lastname='dwsTester1', languagelocalekey='en_US',
                          localesidkey='en_US', profileId = profile.Id, 
                          timezonesidkey='America/Los_Angeles', username='dwsTester1@tester.com', firstname = 'dws1');
            
            insert u2; 
        }
        
        Country__c ctry = new Country__c(Name = 'Afghanistan', Region__c = 'Middle East');
        insert ctry;
        
        Account acc = new Account(ARM__c = u1.id, Name = 'Test Account', Status__c = 'Active', Mailing_Country__c = ctry.id, OwnerId = u1.Id);
        insert acc;
        
        // Test: Account_Owner_Name__c should be set to user 1 name
        Account a = [select id, OwnerId, Account_Owner_Name__c from Account where Id = :acc.Id];
        
        System.assertEquals(u1.Id, a.OwnerId);
        //System.assertEquals(u1.firstname + ' ' + u1.lastname, a.Account_Owner_Name__c);
        
        // Modify Owner
        a.OwnerId = u2.Id;
        update a;
        
        // Test: Account_Owner_Name__c should be set to user 2 name
        Account a1 = [select id, OwnerId, Account_Owner_Name__c from Account where Id = :a.Id];
    }
    
    
    //Test populateTaxInQuote trigger
    public static testMethod void testPopulateTaxInQuoteTrigger()
    {
        Decimal totalTaxAmount;
        
        //Insert required data
        Country__c ctry = new Country__c(Name = 'Afghanistan', Region__c = 'Middle East');
        insert ctry;
        
        Account acc = new Account(ARM__c = Userinfo.getUserId(),Name = 'Test Account', Status__c = 'Active', Mailing_Country__c = ctry.id);
        insert acc;
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        
        Opportunity opp = new Opportunity(Name='test opp', Amount=100, TotalOpportunityQuantity=10, Pricebook2Id=standardPB.Id, StageName='Prospecting', CloseDate=System.today(), AccountId=acc.Id);
        insert opp;
        
        Product2 p = new Product2(Name='test product 1', IsActive=true);             
        insert(p);                            
        
        PricebookEntry currPbe = new PricebookEntry(Pricebook2Id=standardPB.Id, Product2Id=p.Id, IsActive=true, UnitPrice=10, UseStandardPrice=FALSE);
        insert currPbe;
        
        Quote quote1 = new Quote(Name='test quote1', OpportunityId=opp.Id, Pricebook2Id=opp.Pricebook2Id);
        insert quote1;
        
        date dueDate = date.newInstance(2014, 1, 30);
        ICEF_Event__c icefEvent = new ICEF_Event__c(Name='Test IcefEvent', Event_City__c = 'City', technical_event_name__c = 'TestingEvent', Event_Manager_Educators__c = UserInfo.getUserId(), Event_Manager_Agents__c = UserInfo.getUserId(), Start_date__c=dueDate);
        insert icefEvent;  
        
        //Insert quote line item
        QuoteLineItem qli = new QuoteLineItem(QuoteId=quote1.Id, UnitPrice=2000, Quantity=10, Discount=5, ServiceDate=System.today(), PricebookEntryId=currPbe.Id, Tax__c = '10%',  ICEF_Event__c = icefEvent.Id);  
        insert qli;
        
        totalTaxAmount = calculateTotalTaxAmount(qli.Id);
        
        Quote quote2 = [select id, Tax from Quote where id =: quote1.Id]; 
        
        System.assertEquals(quote2.Tax, totalTaxAmount);
        
        //Update Quote line Item
        qli.Tax__c = '23%';
        update qli;
        
        totalTaxAmount = calculateTotalTaxAmount(qli.Id);
        
        Quote quote3 = [select id, Tax from Quote where id =: quote1.Id]; 
        
        System.assertEquals(quote3.Tax, totalTaxAmount);
    }
    
    static Decimal calculateTotalTaxAmount(Id qliId) {
        
        Decimal totalTaxAmount = 0;
        Integer taxAmt;
        QuoteLineItem qli1 = [select Id, Tax__c, QuoteId, TotalPrice from QuoteLineItem where Id =: qliId];
        String taxAmount = qli1.Tax__c;
        if(taxAmount != null)
        {
            taxAmount = taxAmount.Replace('%', '');
            taxAmt = Integer.valueOf(taxAmount);                
            totalTaxAmount += ((qli1.TotalPrice * taxAmt) / 100); 
        }
        return totalTaxAmount;
    }  
    
    //Test Dim4FromIcefEvent trigger
    public static testMethod void testDim4FromIcefEventTrigger()
    {
        date dueDate = date.newInstance(2014, 1, 30);
        ICEF_Event__c icefEvent = new ICEF_Event__c(Name='Test IcefEvent', Event_City__c = 'City', technical_event_name__c = 'TestingEvent', Event_Manager_Educators__c = UserInfo.getUserId(), Event_Manager_Agents__c = UserInfo.getUserId(), Start_date__c=dueDate);
        insert icefEvent;   
        
        ICEF_Event__c icefEvent1 = [Select Id, Name from ICEF_Event__c where Id =: icefEvent.Id];
        icefEvent1.Name = 'Test IcefEvent1';
        
        // Start the test, this changes governor limit context to that of trigger rather than test.
        test.startTest();
        
        update icefEvent1;
        
        // Stop the test, this changes limit context back to test from trigger.
        test.stopTest();
        
        ICEF_Event__c icefEvent2 = [Select Id, Name from ICEF_Event__c where Id =: icefEvent1.Id];
        c2g__codaDimension1__c cd = [Select Id, ICEF_Event__c, Name, c2g__ReportingCode__c from c2g__codaDimension1__c where ICEF_Event__c =: icefEvent1.Id];
        
        System.AssertEquals(cd.c2g__ReportingCode__c, icefEvent2.Name);
        System.AssertEquals(cd.Name, icefEvent2.Name);
    }  
    
    //Test updateCaseOwner trigger
    public static testMethod void testupdateCaseOwnerTrigger()
    {
        //Insert required data
        Country__c ctry = new Country__c(Name = 'Afghanistan', Region__c = 'Middle East');
        insert ctry;
        
        Account acc = new Account(ARM__c = Userinfo.getUserId(),Name = 'Test Account', Status__c = 'Active', Mailing_Country__c = ctry.id);
        insert acc;
        
        Case c = new Case(Country_of_Business_picklist__c = 'Germany', Status = 'New', Origin = 'Web', AccountID = acc.Id);
        insert c;
    }
    
    //Test updateContactOwner trigger
    public static testMethod void testupdateContactOwnerTrigger()
    {
        // Create two Users
        User u1, u2;
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser )
        {        
            Profile profile = [select id from profile where name='System Administrator'];
            
            u1 = new User(alias = 'dws', email='dwsTester@tester.com',
                          emailencodingkey='UTF-8', lastname='dwsTester', languagelocalekey='en_US',
                          localesidkey='en_US', profileId = profile.Id,
                          timezonesidkey='America/Los_Angeles', username='dwsTester@tester.com' ,firstname = 'dws');
            
            insert u1;           
            
            system.assertEquals('dwsTester@tester.com', u1.username);
            system.assertEquals('dws', u1.firstname);
            
            u2 = new User(alias = 'dws1', email='dwsTester1@tester.com',
                          emailencodingkey='UTF-8', lastname='dwsTester1', languagelocalekey='en_US',
                          localesidkey='en_US', profileId = profile.Id,
                          timezonesidkey='America/Los_Angeles', username='dwsTester1@tester.com', firstname = 'dws1');
            
            insert u2;
        }
        
        //Insert required data
        Country__c ctry = new Country__c(Name = 'Afghanistan', Region__c = 'Middle East');
        insert ctry;
        
        Account acc = new Account(ARM__c = Userinfo.getUserId(),Name = 'Testing Account11', Status__c = 'Active', Mailing_Country__c = ctry.id,  OwnerId = u1.Id);
        insert acc;
        
        contact cont = new Contact(Salutation = 'Dr.', FirstName = 'Test', LastName = 'Test', Gender__c = 'male', Role__c = 'Assistant', Contact_Status__c = 'Active', AccountId = acc.Id);
        insert cont;
        
        Account a = [select id, OwnerId from Account where Id = :acc.Id];
        
        // Modify Owner
        a.OwnerId = u2.Id;
        update a;
        
        Contact c = [select id, OwnerId from Contact where Id = :cont.Id];
        
        Account a1 = [select id, OwnerId from Account where Id = :a.Id];
        //System.assertEquals(c.OwnerId, a1.OwnerId);
    }
    
    
    
    
    //Test updateCaseOwner trigger and helper class 
    public static testMethod void testCaseOwnerHelper()
    {
        //Insert required data
        Country__c ctry = new Country__c(Name = 'Afghanistan', Region__c = 'Middle East');
        insert ctry;
        
        Account accountTest = new Account(ARM__c = Userinfo.getUserId(),Name = 'Test Account', Status__c = 'Active', Mailing_Country__c = ctry.id);
        insert accountTest;
        
        //create case record 
        Case testCase = new Case(Country_of_Business_picklist__c = 'Germany', Status = 'New', Origin = 'Web', AccountID = accountTest.Id);
        insert testCase;
        
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User testUser = new User(alias = 'dws', email='dwsTester@tester.com',
                            emailencodingkey='UTF-8', lastname='dwsTester', languagelocalekey='en_US',
                            localesidkey='en_US', profileId = adminProfile.Id,
                            timezonesidkey='America/Los_Angeles', username='dwsTester@tester.com' ,firstname = 'dws');
        
        insert testUser;
        Test.startTest();
        System.runAs ( testUser ) {  
            // update case record owner 
            testCase.OwnerId = testUser.Id;
            CaseOwnerHelper.isExecuted = false;
            update testCase;
        }
        Case updatedCase = [SELECT Id, OwnerId FROM Case WHERE Id = :testCase.Id ];
        System.assertEquals(updatedCase.OwnerId, testUser.Id);
        Test.StopTest();
    }    
}