/*
    Purpose : Controller To sendAccommodationConfirmation Page
*/

public class sendAccommodationConfirmationController{

    public string subject {get; set;}
    public string body {get; set;}
    public boolean isNoError {get; set;}
    public string recipientEmailId {get; set;}
    public string workshopcc {get; set;}
    public string contactParticipationName {get; set;}
    public boolean attachEmailBodyAsPDF {get;set;}
    
    public List<String> cpMergeFieldList = new List<String>();
    public List<String> cpFieldList = new List<String>();
    public List<String> accommodationFieldList = new List<String>();
    public List<String> accommodationMergeFieldList = new List<String>();
    public string[] recipientEmailIdList = new string[] {};
    
    public List<String> mergeFieldsOtherSObjectFields = new List<String> {};
    public List<String> otherSObjectFields = new List<String> {};
    Accommodation__c accommodationRecord;
    Contact_Participation__c contactParticipationRecord; //changed Accommodation__c
    String accommodationId,contactPartId,emailTemplateId,orgWideMailId;
    Boolean isHtmlbody;
    EmailTemplate emailTemp;
    
    /*
        Purpose   : This Controller will be executed on page load
        Parameter : Page Standard Controller
    */
    public sendAccommodationConfirmationController(ApexPages.StandardController controller) {
        
        // set id values
        accommodationId = System.currentPageReference().getParameters().get('id');
        contactPartId = System.currentPageReference().getParameters().get('cpid');
        emailTemplateId =  System.currentPageReference().getParameters().get('etid');
        orgWideMailId =  System.currentPageReference().getParameters().get('mail');
        
        if(validateIds()){
            // execute initially required methods
            executeMethodsOnPageLoad();
        } 
                
    }
    
    /*
        Purpose   : This Controller will be from another class to access functionality
        Parameter : Page Standard Controller
    */
    public sendAccommodationConfirmationController(String accommodationRecordId, 
                                                   String contactPartRecordId, 
                                                   String emailTemplateRecordId){
        
        // set id values
        accommodationId = accommodationRecordId;
        contactPartId = contactPartRecordId;
        emailTemplateId = emailTemplateRecordId;
        
        if(validateIds()){
            // execute initially required methods
            executeMethodsOnPageLoad();
        }
    }
    
    /*
        Purpose   : Validate input ids 
        Return    : Boolean values
    */
    public Boolean validateIds(){
        
        if(getObjectName(accommodationId ) != 'Accommodation__c'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                     'Url is currupted, Please check Accommodation record id'));
            return false;
        }
        if(getObjectName(contactPartId) != 'Contact_Participation__c'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                             'Url is currupted, Please check Contact Participation record id'));
            return false;
        }
        
        if(emailTemplateId != null && emailTemplateId.trim() != '')
        if(getObjectName(emailTemplateId) != 'EmailTemplate'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                             'Url is currupted, Please check Email Template id'));
            return false;
        }
        return true;
        
    }

    /*
        Purpose   : Validate input ids 
        Return    : Boolean values
    */  
    public string getObjectName(String idString){
        try {
            Id sObjectId = idString;
            return sObjectId.getSObjectType().
                             getDescribe().
                             getName();
        }
        catch(Exception e){
            isNoError = false;
            system.debug(e.getMessage());
            return null;
        }
        
    }
    
    /*
        Purpose   : Execute innitially required Methods On Page Load
    */
    public void executeMethodsOnPageLoad(){
        
        attachEmailBodyAsPDF = false;
        isHtmlbody = false; 
        isNoError = true;
        
        if(orgWideMailId != '')
                selectedOption = orgWideMailId;
                
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        
        // Populate Accommodation Record
        populateAccommodationRecord(globalDescription);        
        
        // Populate Contact Participation Record
        populateContactPartRecord(globalDescription);
                
        if( emailTemplateId != null && 
            emailTemplateId != 'null' && 
            emailTemplateId  != '') {
            
                // Load email body on page
                showEmailBodyOnPageLoad(emailTemplateId);
            } 
    }
    
    /*
        Purpose   : Populate Contact Participations Record
        Parameter : org globalDescription
    */
    public void populateContactPartRecord( Map<String,Schema.SObjectType> globalDescription ){
        String  query;
        String  allfields = '';
        if( contactPartId == null || 
            contactPartId == 'null' || 
            contactPartId == '')  {
            
            showError();
        }
        else
        {   
            //Access all fields of Contact Participation
            schema.SObjectType contactParticipationType2 = globalDescription.get('Contact_Participation__c'); //changed Accommodation__c
            Schema.DescribeSObjectResult Obj2 = contactParticipationType2.getDescribe();
            Map<String, Schema.SObjectField> objFields2 = Obj2.fields.getMap();
            boolean isFirst = true;         
            for(String fieldName : objFields2.keySet())
            {
                if(isFirst)
                {
                    allfields = fieldName.trim();
                    isFirst = false;
                }
                else
                    allfields = allfields + ', ' + fieldName;
            }
            query = 'Select ' + allfields + ' from Contact_Participation__c where Id = \'' + contactPartId + '\' ';          
            contactParticipationRecord = Database.query(query); 
            contactParticipationName = contactParticipationRecord.Name; 
            
            if(contactParticipationRecord.Workshop_email__c != null)
            {
                recipientEmailId = contactParticipationRecord.Workshop_email__c;
                
                if(contactParticipationRecord.Workshop_CC_Mailers__c != null){
                    workshopcc = contactParticipationRecord.Workshop_CC_Mailers__c;
                }
                
                if(workshopcc != null)
                    recipientEmailIdList.add(workshopcc);
                recipientEmailIdList.add(recipientEmailId);
            }
            else
            {
                Contact c = [Select Id, Email from Contact where id =: contactParticipationRecord.Contact__c];
                recipientEmailId = c.Email;
                recipientEmailIdList.add(recipientEmailId);
            }
        }
    }
    
    /*
        Purpose   : Populate Accommodation Record
        Parameter : org globalDescription
    */
    public void populateAccommodationRecord( Map<String,Schema.SObjectType> globalDescription ){
        string accommodationAllfields = '';
        string query;
        if( accommodationId == null || 
            accommodationId == 'null' || 
            accommodationId == '')
        {
            showError();
        }
        else
        {   
            //Access all fields Accommodation__c
            schema.SObjectType accommodationType2 = globalDescription.get('Accommodation__c'); 
            Schema.DescribeSObjectResult accommodationObj = accommodationType2.getDescribe();
            Map<String, Schema.SObjectField> accommodationFields = accommodationObj.fields.getMap(); 
            boolean isFirst = true; 
            for(String fieldName : accommodationFields.keySet())
            {
                if(isFirst)
                {
                    accommodationAllfields = fieldName.trim();
                    isFirst = false;
                }
                else
                    accommodationAllfields = accommodationAllfields + ', ' + fieldName;
            }
            query = 'Select ' + accommodationAllfields + ' from Accommodation__c where Id = \'' + accommodationId + '\' ';        
            accommodationRecord = Database.query(query);        
        }
    }
    
    /*
        Purpose   : get list of select option of org wide email addresses 
    */
    public List<SelectOption> getItems()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        User u = [Select Name, Email, Id from User where Id =: UserInfo.getUSerId()];
        String loggedInUser = '"'+ u.Name + '" <'+ u.Email + '>';
        options.add(new SelectOption(loggedInUser, loggedInUser)); 
        
        List<OrgWideEmailAddress> owa = [select Address, DisplayName from OrgWideEmailAddress];
        
        for(integer i = 0; i < owa.size(); i++)
        {            
            string option = '"'+ owa[i].DisplayName + '" <'+ owa[i].Address + '>';
            options.add(new SelectOption(option, option));
        }   
             
        return options;
    }    
    
    /*
        Purpose   : Method to redirect on mailTemplateForAccommodationConfirmation Page.
    */
    public Pagereference selectTemplateRedirect()
    {    
          PageReference selectEmailTemplate = new PageReference('/apex/mailTemplateForAccommodationConfirmation?id=' +
                                                                accommodationId +
                                                                '&cpid=' +
                                                                contactPartId +
                                                                '&mail=' +
                                                                selectedOption);
          selectEmailTemplate.setRedirect(true);     
          return selectEmailTemplate;
    }
    
    /*
        Purpose   : Used to show email body on page load.
        Parameter : Email template id
    */
    public void showEmailBodyOnPageLoad(string emailTemplateId)
    {
        string  startString = '{!', endString = '}';
        
        emailTemp = [SELECT Subject, Body, Markup, HtmlValue, TemplateType 
                     FROM EmailTemplate 
                     WHERE Id =: emailTemplateId]; 
                     
        //system.debug('emailTemp '+emailTemp);//debug
        if(emailTemp != null)
        {
            subject = emailTemp.Subject;
            
            if(emailTemp.TemplateType != 'Text')
                body = emailTemp.HtmlValue; //Body; 
            else
                body = emailTemp.Body;
                
            List<String> totalFieldList = getTotalFieldList(body);
            for(String field : totalFieldList){
                if(field.contains('{!Accommodation__c')){
                    accommodationFieldList.add(field);
                    accommodationMergeFieldList.add(field.substring(field.indexof('.')+1,
                                                    field.indexof(endString))); 
                }
                if(field.contains('{!Contact_Participation__c')){
                    cpFieldList.add(field);
                    cpMergeFieldList.add(field.substring(field.indexof('.')+1,
                                                      field.indexof(endString))); 
                }
            }
             
            body = replaceAccommodationMergeFields(body) ;
            body = replaceCPMergeFields(body);
            //system.debug('after replacement body '+body);//debug
        }
    }
    
    /*
        Purpose   : Get total field list.
        Parameter : string body
    */
    public List<string> getTotalFieldList(String body){
        string  startString = '{!', endString = '}';
        integer i=0,j=0,k;
        String accommodationField,accommodationMergeField ;
        List<String> fieldList = new List<String>();
        while(i < body.length())
        {  
            j = body.indexof(startString,i);
            if(j == -1)
                break;
            k = body.indexof(endString,j);
            fieldList.add(body.substring(j,k+1));  
            i = k;                
        } 
        return fieldList;
    }
    
    /*
        Purpose   : Used to replace Accommodation merge fields.
        Parameter : string body
    */
    public String replaceAccommodationMergeFields( String strBody )
    {
        isHtmlbody = true;            
        integer i = 0, j = 0, k;
        String accommodationField;
        String accommodationMergeField;
        
        mergeFieldsOtherSObjectFields = new List<String>();
        otherSObjectFields = new List<String>();
              
        //Accessing all Accommodation fields    
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        schema.SObjectType accommodationType = globalDescription.get('Accommodation__c'); 
        
        Schema.DescribeSObjectResult accommodationObj = accommodationType.getDescribe();
        Map<String, Schema.SObjectField> accommodationObjFields = accommodationObj.fields.getMap(); 
              
        for(integer a = 0; a < accommodationMergeFieldList.size(); a++)
        {
            for(String fieldName : accommodationObjFields.keySet())
            {
                if(accommodationMergeFieldList[a] == fieldName)
                {                                           
                    schema.SobjectField accommodationFieldSchema = accommodationObjFields.get(fieldname);           
                    Schema.DescribeFieldResult accommodationSchemaResult = accommodationFieldSchema.getDescribe(); 
                    
                    if(String.valueof(accommodationRecord.get(fieldName)) != null)
                    {
                        if(String.valueOF(accommodationSchemaResult.getType()) == 'Boolean')
                        {
                            String tempBool;
                            if(Boolean.valueof(accommodationRecord.get(fieldName)) == true)
                                tempBool = '1';
                            else
                                tempBool = '0';
                            strBody = strBody.replace(accommodationFieldList[a], tempBool);                             
                            
                        }
                        else if(String.valueOF(accommodationSchemaResult.getType()) == 'DateTime' || String.valueOF(accommodationSchemaResult.getType()) == 'Date')
                        {
                            strBody = strBody.replace(accommodationFieldList[a], String.valueOf(Date.valueof(accommodationRecord.get(fieldName))));                             
                            
                        } 
                        else if(accommodationSchemaResult.isIdLookup() && String.valueOF(accommodationSchemaResult.getType()) == 'ID')
                        {
                            string tempId = String.ValueOf(accommodationRecord.get(fieldName));
                            tempId = tempId.substring(0, tempId.length()-3);
                            strBody = strBody.replace(accommodationFieldList[a], tempId);       
                        } 
                        else if(String.valueOF(accommodationSchemaResult.getType()) == 'Reference')
                        {
                            List<Schema.sObjectType> P = accommodationSchemaResult.getReferenceTo();
                            
                            if(String.valueOf(P[0]) != 'User' && String.valueOf(P[0]) != 'RecordType')
                            {
                                String query1 = 'Select Id, Name from '+String.valueOf(P[0])+ ' where Id = \'' + accommodationRecord.get(fieldName) + '\' ';    
                                List<sObject> so = Database.query(query1);   
                                
                                strBody = strBody.replace(accommodationFieldList[a], String.valueof(so[0].get('Name'))); 
                                mergeFieldsOtherSObjectFields.add(fieldName);
                                otherSObjectFields.add(accommodationFieldList[a]);                        
                            }    
                            else
                            {
                                string tempId2 = String.ValueOf(accommodationRecord.get(fieldName));
                                tempId2 = tempId2.substring(0, tempId2.length()-3);
                                strBody = strBody.replace(accommodationFieldList[a], tempId2);   
                            }
                        }                           
                        else
                        {
                            string tempStr = String.valueof(accommodationRecord.get(fieldName));
                            tempStr = tempStr.replaceAll('\n','<br/>');
                            strBody = strBody.replace(accommodationFieldList[a], tempStr);    
                        }
                    }
                    else
                    {
                        strBody = strBody.replace(accommodationFieldList[a], '');
                    }                            
                    
                    break;
                }
                else if(accommodationMergeFieldList[a] == 'CreatedBy')
                {
                    String query =  'Select Id, Name from User where Id = \'' + 
                                     accommodationRecord.get('CreatedByID') + 
                                     '\' ';
                    List<User> u = Database.Query(query);
                    System.debug('u::'+u[0].Name);
                    System.debug('accommodationFieldList[a]::'+accommodationFieldList[a]);
                    //System.debug('strBody::'+strBody);
                    
                    strBody = strBody.replace(accommodationFieldList[a], u[0].Name);
                    break;
                }
                else if(accommodationMergeFieldList[a] == 'LastModifiedBy')
                {
                    String query =  'Select Id, Name from User where Id = \'' + 
                                    accommodationRecord.get('LastModifiedByID') + 
                                    '\' ';
                    List<User> u = Database.Query(query);
                    strBody = strBody.replace(accommodationFieldList[a], u[0].Name);
                    
                    break;
                }
                else if(accommodationMergeFieldList[a] == 'RecordType')
                {
                    String query =  'Select Id, Name from RecordType where Id = \'' + 
                                     accommodationRecord.get('RecordTypeID') + 
                                     '\' ';
                    List<RecordType> rt = Database.Query(query);
                    strBody = strBody.replace(accommodationFieldList[a], rt[0].Name);
                    break;
                }                 
                
            }   
            
          }                       
                      
        for(integer x = 0; x < mergeFieldsOtherSObjectFields.size(); x++)
        {
            for(integer y = 0; y < accommodationMergeFieldList.size(); y++)
            {
                string[] temp = mergeFieldsOtherSObjectFields[x].split('__');
                string[] temp1 = otherSObjectFields[x].split('__');
                
                if(temp.size() == 2 && temp1.size() == 3)
                {
                    string referIdField = temp[0] + 'Id__' + temp[1];
                    string cpreferIdField = temp1[0] + '__' + temp1[1] + 'Id__' + temp1[2];
                    
                    if(y < cpMergeFieldList.size()){
                        System.debug(' CP Merge Field :: ' + cpMergeFieldList.size());
                        System.debug(' Merge Field Other :: '+ mergeFieldsOtherSObjectFields.size());
                        System.debug('JSON ' + JSON.serialize(accommodationMergeFieldList));
                        System.debug(' accommodationMergeFieldList :: '+ accommodationMergeFieldList.size());
                        system.debug('List Error ::'+cpMergeFieldList);
                        
                        if(cpMergeFieldList[y] == referIdField)
                        {
                            string tempId1 = String.ValueOf(accommodationRecord.get(mergeFieldsOtherSObjectFields[x]));
                            tempId1 = tempId1.substring(0, tempId1.length()-3);
                            strBody = strBody.replace(cpreferIdField, tempId1);
                            break;
                        }
                    }
                }
            }
        }
        
        for(integer m = 0; m < accommodationFieldList.size(); m++)
        {
            strBody = strBody.replace(accommodationFieldList[m], '');
        }
        return strBody;            
    }
    
    /*
        Purpose   : Used to replace Contact Participation merge fields.
        Parameter : string body
    */
    public String replaceCPMergeFields( String strBody)
    {
        mergeFieldsOtherSObjectFields = new List<String>();
        otherSObjectFields = new List<String>();
        isHtmlbody = true;    
               
        //Accessing all contact participation fields    
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        schema.SObjectType contactParticipationType = globalDescription.get('Contact_Participation__c'); //changed Accommodation__c
        
        Schema.DescribeSObjectResult Obj = contactParticipationType.getDescribe();
        Map<String, Schema.SObjectField> objFields = Obj.fields.getMap(); 
       
        for(integer a = 0; a < cpMergeFieldList.size(); a++)
        {
            for(String fieldName : objFields.keySet())
            {
                
                if(cpMergeFieldList[a] == fieldName)
                {                                           
                    schema.SobjectField s = objFields.get(fieldname);           
                    Schema.DescribeFieldResult R = s.getDescribe(); 
                    
                    if(String.valueof(contactParticipationRecord.get(fieldName)) != null)
                    {
                        if(String.valueOF(R.getType()) == 'Boolean')
                        {
                            String tempBool;
                            if(Boolean.valueof(contactParticipationRecord.get(fieldName)) == true)
                                tempBool = '1';
                            else
                                tempBool = '0';
                            strBody = strBody.replace(cpFieldList[a], tempBool);     
                        }
                        else if(String.valueOF(R.getType()) == 'DateTime' || 
                                String.valueOF(R.getType()) == 'Date')
                        {
                            strBody = strBody.replace(cpFieldList[a], String.valueOf(Date.valueof(contactParticipationRecord.get(fieldName))));     
                        } 
                        else if(R.isIdLookup() && 
                                String.valueOF(R.getType()) == 'ID')
                        {
                            string tempId = String.ValueOf(contactParticipationRecord.get(fieldName));
                            tempId = tempId.substring(0, tempId.length()-3);
                            strBody = strBody.replace(cpFieldList[a], tempId);    
                        } 
                        else if(String.valueOF(R.getType()) == 'Reference')
                        {
                            List<Schema.sObjectType> P = R.getReferenceTo();
                            
                            if(String.valueOf(P[0]) != 'User' && 
                               String.valueOf(P[0]) != 'RecordType')
                            {
                                String query1 = 'Select Id, Name from '+String.valueOf(P[0])+ ' where Id = \'' + contactParticipationRecord.get(fieldName) + '\' ';    
                                List<sObject> so = Database.query(query1);   
                                
                                strBody = strBody.replace(cpFieldList[a], String.valueof(so[0].get('Name')));      
                                mergeFieldsOtherSObjectFields.add(fieldName);
                                otherSObjectFields.add(cpFieldList[a]);                        
                            }    
                            else
                            {
                                string tempId2 = String.ValueOf(contactParticipationRecord.get(fieldName));
                                tempId2 = tempId2.substring(0, tempId2.length()-3);
                                strBody = strBody.replace(cpFieldList[a], tempId2);   
                            }
                        }                           
                        else
                        {
                            string tempStr = String.valueof(contactParticipationRecord.get(fieldName));
                            tempStr = tempStr.replaceAll('\n','<br/>');
                            strBody = strBody.replace(cpFieldList[a], tempStr);      
                        }
                    }
                    else
                    {
                        strBody = strBody.replace(cpFieldList[a], '');
                    }                            
                    
                    break;
                }
                else if(cpMergeFieldList[a] == 'CreatedBy')
                {
                    String query =  'SELECT Id, Name FROM User WHERE Id = \'' + 
                                     contactParticipationRecord.get('CreatedByID') + 
                                     '\' ';
                    List<User> u = Database.Query(query);
                    strBody = strBody.replace(cpFieldList[a], u[0].Name);
                    
                    break;
                }
                else if(cpMergeFieldList[a] == 'LastModifiedBy')
                {
                    String query =  'SELECT Id, Name FROM User WHERE Id = \'' + 
                                     contactParticipationRecord.get('LastModifiedByID') + 
                                     '\' ';
                    List<User> u = Database.Query(query);
                    strBody = strBody.replace(cpFieldList[a], u[0].Name);
                    
                    break;
                }
                else if(cpMergeFieldList[a] == 'RecordType')
                {
                    String query =  'SELECT Id, Name FROM RecordType WHERE Id = \'' + 
                                    contactParticipationRecord.get('RecordTypeID') + 
                                    '\' ';
                    List<RecordType> rt = Database.Query(query);
                    strBody = strBody.replace(cpFieldList[a], rt[0].Name);
                    break;
                }                 
                
            }  
          }                       
                      
        for(integer x = 0; x < mergeFieldsOtherSObjectFields.size(); x++)
        {
            for(integer y = 0; y < cpMergeFieldList.size(); y++)
            {
                string[] temp = mergeFieldsOtherSObjectFields[x].split('__');
                string[] temp1 = otherSObjectFields[x].split('__');
                if(temp.size() == 2 && temp1.size() == 3)
                {
                    string referIdField = temp[0] + 'Id__' + temp[1];
                    string cpreferIdField = temp1[0] + '__' + temp1[1] + 'Id__' + temp1[2];
                    if(cpMergeFieldList[y] == referIdField)
                    {
                        string tempId1 = String.ValueOf(contactParticipationRecord.get(mergeFieldsOtherSObjectFields[x]));
                        tempId1 = tempId1.substring(0, tempId1.length()-3);
                        strBody = strBody.replace(cpreferIdField, tempId1);
                        break;
                    }
                }
                
            }
        }
        
        for(integer m = 0; m < cpFieldList.size(); m++)
        {
            strBody = strBody.replace(cpFieldList[m], '');
        }
        
        return strBody;
    } 

    /*
        Purpose : set value of selectOption of orgwide email address.
    */
    public String selectedOption
    {
       get
        {
           if(selectedOption==null)
           {
               User u = [Select Email, Id from User where Id =: UserInfo.getUSerId()];
               selectedOption = u.Email;    
               System.debug('selectedOption  1::'+selectedOption);           
           }
           return selectedOption;
        }
    set;
    } 
    
    /*
        Purpose : Used to send mail.
    */  
    public pageReference sendEmail()
    {
        
        if( recipientEmailId != null && 
            recipientEmailId.trim() != '') {
            
            if( subject != null && 
                subject.trim() != '' && 
                body != null && 
                body.trim() != '') {
                
                try
                {
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();   
                    email.setSubject(subject);
                    
                    
                    /*if(isHtmlbody)
                        email.setHtmlBody(body);//setPlainTextBody(body);
                    else    
                        email.setPlainTextBody(body);*/
                        email.setHtmlBody(body);
                    
                    if(recipientEmailIdList != null && !recipientEmailIdList.isEmpty())    
                    {
                      // for testing  
                       // List<String> testAddress = new List<String>{'sagar@dreamwares.com'};
                       // email.setToAddresses(testAddress);
                       email.setToAddresses(recipientEmailIdList);
                    }
                   
                    email.setTargetObjectId(contactParticipationRecord.Contact__c);
                    email.saveAsActivity = true;
                    email.setUseSignature(true); 
                    
                    //set organisation wide email address        
                    
                    selectedOption = selectedOption.subString(selectedOption.indexOf('<')+1, selectedOption.indexOf('>'));
                    
                    List<OrgWideEmailAddress> owa = [SELECT id, Address 
                                                     FROM OrgWideEmailAddress 
                                                     WHERE Address =: selectedOption];
                    if(owa.size() != 0)
                        email.setOrgWideEmailAddressId(owa[0].id);
                    
                    //Set email file attachments
                    List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                    System.debug('emailTemplateId::'+emailTemplateId);
                    
                    if(emailTemplateId != '')
                    {
                        for (Attachment a : [SELECT Id, Name, Body, BodyLength 
                                             FROM Attachment 
                                             WHERE ParentId = :emailTemplateId])
                        {
                            // Add to attachment file list
                            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                            efa.setFileName(a.Name);
                            efa.setBody(a.Body);
                            //System.debug('Attachment body ::'+a.Body);
                            fileAttachments.add(efa);
                        }
                        
                    }
                    
                    //System.debug('attachEmailBodyAsPDF::'+attachEmailBodyAsPDF);
                    //check for pdf attachment
                    if(attachEmailBodyAsPDF)
                    {   //System.debug('emailTemp::'+emailTemp);
                        Blob pdfBlob;
                        if(emailTemp != null)
                        {
                            PageReference pageRef = new PageReference('/apex/EmailAttachmentPDF');
                            pageRef.getParameters().put('id', accommodationId);
                            pageRef.getParameters().put('etid', emailTemplateId);
                            pageRef.getParameters().put('cpid', contactPartId);
                            pdfBlob = pageRef.getContent();
                            //System.debug('Page Contains ::'+ pageRef.getContent());
                        }
                        else{
                            //System.debug(' Body ::'+body);
                            pdfBlob = Blob.toPDF(body);
                        }
                        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                        efa.setFileName(subject+'.pdf');
                        efa.setBody(pdfBlob);
                        //System.debug('pdfBlob::'+pdfBlob);
                        efa.setContentType('application/pdf');
                        fileAttachments.add(efa);
                        system.debug('Attaching pdf');
                    }
                    
                    //set file attachments
                    if(!fileAttachments.isEmpty())
                        //System.debug('fileAttachments::'+fileAttachments);
                        email.setFileAttachments(fileAttachments);
                    
                    // Send the email
                    system.debug('Activity target id'+ email.getTargetObjectId());
                    system.debug('Activity'+ email.getSaveAsActivity());
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email }); 
                    
                     
                    string taskSubject = '',comment;
                    taskSubject = 'Email: '+ subject;
                    comment = 'Subject: '+subject+' \n Body: \n'+body;
                    
                    //Create task
                    Task t = new Task(
                             ownerId = UserInfo.getUserId(),
                             Subject = taskSubject,
                             whatId = accommodationId,
                             status = 'Completed',
                             whoId = contactParticipationRecord.Contact__c,
                             Type = 'Email',
                             ActivityDate = Date.TODAY(),
                             Description = comment);
                    insert t;          
                    
                      
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm, 
                                                               'Confirmation Email Sent Successfully  <a href="/'+contactPartId+'" > Go to Contact Participation   </a><a href="/'+accommodationId+'" > Go to Accommodation</a>'));
                    return null;
                    /*Pagereference accommodationRecordDetailPage = new PageReference('/'+accommodationId);
                    accommodationRecordDetailPage.setRedirect(true);
                    return accommodationRecordDetailPage;*/
                
                }
                catch(Exception e)
                {
                    system.debug('Error while email sending ::'+e.getMessage()+
                              'At line number :'+e.getLineNumber()+'ERROR ' +
                               e.getStackTraceString());
                               
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                                               'Sorry, Failed to send Accommodation confirmation mail '));
                    return null;
                }
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                                           'Please add subject and body before sending mail'));
                return null;
            }
        }
        else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                                       'Sorry unable to send mail. No workshop and contact email address is available'));
            return null;
        }
        
    }
    
    /*
        Purpose : Used to show error.
    */
    Public PageReference showError()
    {
        isNoError = false;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 
                                                   'You may have changed url.. Id parameter is required in Url'));
        return null;
    } 
    
}