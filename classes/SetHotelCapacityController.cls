/**
  * @Author      : Prajakta
  * @Description : SetHotelCapacity VF page Controller
  * @Created Date: 08 Sept 2016
 */
public with sharing class SetHotelCapacityController {
    
    private Id batchToUpdateAccountParticipationId;
    public Integer updatedCount{get;set;}
    private String selectedHotelCapacity;
    public Boolean isPollerActive {get;set;}
    public String batchExecutionStatus{get;set;}
    private final integer BATCHSIZE = 10;
    //--Constructor
    public SetHotelCapacityController() {
        isPollerActive = false;
        if(String.isEmpty(getParametersFromURL('ids'))) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Id\'s not found in URL!'));
            return;
        }
    }
    
    /**
      * @purpose : Get Hotel_Capacity__c records whose ICEF event is matches with the provided 
                   Account participation records
      * @param   : accountPartIds_p to proceess
      * @return  : Hotel_Capacity__c list
     */
    @RemoteAction
    public static List<Hotel_Capacity__c> getHotelCapacityRecords(String accountPartIds_p ) {
        List<Hotel_Capacity__c> hotelCapacityList = new List<Hotel_Capacity__c>();
        if(String.isNotEmpty(accountPartIds_p)) {
            List<String> accountPartIds = accountPartIds_p.split(',');
            
            if( accountPartIds != null && !accountPartIds.isEmpty() ) {
                try {
                   
                    Set<Id> icefEventsIds = new Set<Id>();
                    for( Account_Participation__c acountParticipation  : [ SELECT Id,ICEF_Event__c,Hotel_Capacity__c
                                                                             FROM Account_Participation__c
                                                                             WHERE Id IN : accountPartIds ] ) {
                        icefEventsIds.add(acountParticipation.ICEF_Event__c);
                    }
                    
                    if(icefEventsIds != null && !icefEventsIds.isEmpty()) {
                        hotelCapacityList = [ SELECT Id,Name,Hotel_City__c,First_event_day_in_this_Hotel__c,RecordType.Name
                                              FROM Hotel_Capacity__c
                                              WHERE ICEF_Event__c IN : icefEventsIds ];
                    }
                } catch(QueryException qryException) {
                    System.debug('Error occured while retrieving record : ' + qryException.getMessage());
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, qryException.getMessage()));
                    return hotelCapacityList;
                }
            } 
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Id\'s not found in URL!'));
            return hotelCapacityList;
        }  
        return hotelCapacityList;   
    }
    
    /**
      * @purpose : Invoked batch class to update records
     */
    public void updateAccountParticipants() {
        String accountParticipationIds = getParametersFromURL('ids');
        selectedHotelCapacity  = getParametersFromURL('hotelCapacityId');
        System.debug('accountParticipationIds : ' + accountParticipationIds);
        System.debug('selectedHotelCapacity : ' + selectedHotelCapacity);
        if( String.isNotEmpty(accountParticipationIds) && String.isNotEmpty(selectedHotelCapacity)) {
            batchToUpdateAccountParticipationId = Database.executeBatch(new BatchToUpdateAccountParticipation(accountParticipationIds,selectedHotelCapacity), BATCHSIZE);
            isPollerActive = true;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Id\'s OR selected hotelCapacityId not found!'));
            return;
        }  
    }
    
    /**
      * @purpose : Check status of batch throgh action poller
     */
    public void checkAccountPartUpdateStatus() {
        AsyncApexJob job;
        try {
            job = [ SELECT Id, Status, JobItemsProcessed, TotalJobItems
                    FROM AsyncApexJob 
                    WHERE Id =: batchToUpdateAccountParticipationId ];
            if( job != null && String.isNotEmpty(job.Status) ) {
                batchExecutionStatus = job.Status;
            }
        } catch(QueryException qryException) {
            System.debug('Error occured while retrieving AsyncApexJob : ' + qryException.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, qryException.getMessage()));
            return;
        }   
        if( String.isNotEmpty(batchExecutionStatus) ) {
            String accountParticipationIds = getParametersFromURL('ids');
            if( batchExecutionStatus.equals('Completed') ) {
                
                if( String.isNotEmpty(accountParticipationIds) ) {
                    updatedCount   = [ SELECT COUNT()
                                       FROM Account_Participation__c
                                       WHERE Id IN : accountParticipationIds.split(',') AND Hotel_Capacity__c = : selectedHotelCapacity ];
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully updated ' + updatedCount  + ' records'));
                } else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Id\'s not found in URL!'));
                    return;
                }
                isPollerActive = false;
            } else if(batchExecutionStatus.equals('Processing')) {
                isPollerActive = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, job.JobItemsProcessed + ' of ' + job.TotalJobItems + ' batches processed. Please wait...'));
            } else if( batchExecutionStatus.equals('Aborted') || batchExecutionStatus.equals('Failed') ) {
                isPollerActive = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Batch execution is ' + batchExecutionStatus));
            } else {
                isPollerActive = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Batch execution is ' + batchExecutionStatus + '. Please wait...'));
            }   
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unexpected error has occured'));
        }      
    }
    
    /**
      * @purpose : Get value of URL parameter
      * @param   : parameter name
      * @return  : return value of requested URL parameter
     */
    private static String getParametersFromURL(String paramName) {
        return ApexPages.currentPage().getParameters().get(paramName);
    }
    
}