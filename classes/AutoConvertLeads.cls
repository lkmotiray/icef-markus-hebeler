Public class AutoConvertLeads
{
    @InvocableMethod
    public static void LeadAssign(List<Id> LeadIds)
    {
        LeadStatus CLeadStatus= [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true Limit 1];
        Map<Id,Lead> leadMap = new Map<Id, Lead>([SELECT Id, Convert_to_Account_Id__c, Convert_to_Contact_Id__c FROM Lead WHERE Id IN :LeadIds]);
        List<Database.LeadConvert> MassLeadconvert = new List<Database.LeadConvert>();
        for(id currentlead: LeadIds){
                Database.LeadConvert Leadconvert = new Database.LeadConvert();
                Leadconvert.setLeadId(currentlead);                
                Leadconvert.setConvertedStatus(CLeadStatus.MasterLabel);
                Leadconvert.setDoNotCreateOpportunity(TRUE); //Remove this line if you want to create an opportunity from Lead Conversion 
                Leadconvert.setAccountId(leadMap.get(currentLead).Convert_to_Account_Id__c);
            	Leadconvert.setContactId(leadMap.get(currentLead).Convert_to_Contact_Id__c);
            	MassLeadconvert.add(Leadconvert);
        }
        
        if (!MassLeadconvert.isEmpty()) {
            List<Database.LeadConvertResult> lcr = Database.convertLead(MassLeadconvert);
        }
    }
}