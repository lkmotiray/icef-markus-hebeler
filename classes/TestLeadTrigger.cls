/**
 * This is the Test class for the trigger 'LeadTrigger'. The 
 * class tests the creation of Recommendation records 
 * for the Leads which are updated as Converted Leads.
 *
 * @author      	Jasmine J 
 * @created date	31/08/2015
 */
@isTest
public class TestLeadTrigger {
    
    public static List<Database.LeadConvertResult> results = new List<Database.LeadConvertResult>();
	public static Map<String, Id> mapAccRecordTypeIds = new Map<String, Id>();
    
    /**
     * Creates Lead and converts it to trigger the LeadTrigger.
     */
    private static testMethod void testLeadTrigger() {
        
        // Retrieve a record with RecordType to use in Account and Account Participation record insertion. //  
        mapAccRecordTypeIds = LeadTriggerHandler.getMapOfAccRecordTypeIds();
        System.debug('mapAccRecordTypeIds :: '+mapAccRecordTypeIds);  
        
        Id recTypeAgent = mapAccRecordTypeIds.get('Agent');
        System.debug('recTypeAgent ::' +recTypeAgent);
        
        // For Account Owner Update
        User testUser = new User();
        testUser.Username= 'user1@company.in';
        testUser.Email = 'user1@company.in';
        testUser.Lastname = 'user1';
        testUser.Firstname = 'test1';
        testUser.Alias = 'test1';
        testUser.CommunityNickname = '12346';
        testUser.UserRole = [ select id from userrole LIMIT 1 ];
        SObject prof = [ select id from profile LIMIT 1 ];
        testUser.ProfileId = (ID) prof.get('ID');
        testUser.TimeZoneSidKey = 'GMT';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.UserPermissionsMobileUser = false; 
        insert testUser;
        
        // Inserting Country record to use in Account and Account Participation record insertion. //
        Country__c testCountry = new Country__c(Name = 'Brazil', Region__c = 'USA');
        Country__c countryEgy = new Country__c(Name = 'Egypt', 
                                               Agent_Relationship_Manager__c = testUser.Id,
                                               Sales_Territory_Manager__c = testUser.Id);
        insert new List<SObject>{ testCountry, countryEgy };  
        
        // Inserting Account record using Country and RecordType Ids and use in Contact record. //
        Account testAccount = new Account(Name = 'Test Account', 
                                          RecordTypeId = recTypeAgent, 
                                          Status__c = 'Active', 
                                          Mailing_Country__c = testCountry.id);
        
        Account testAccount1 = new Account(Name = 'Test Account1', 
                                          RecordTypeId = recTypeAgent, 
                                          Status__c = 'Active', 
                                          Mailing_Country__c = testCountry.id);
        
        insert new List<SObject>{ testAccount, testAccount1 };
        
        List<Account> listTestAccounts = [SELECT Id, RecordType.Name FROM Account LIMIT 2];
        System.debug('listTestAccounts  ::' +listTestAccounts);
        
        // Inserting Contact record to use in Account Participation record insertion. //
        Contact testContact = new Contact(Salutation = 'Dr.', FirstName = 'Test', 
                                          LastName = 'Test', Gender__c = 'Male', 
                                          Role__c = 'Assistant', 
                                          Contact_Status__c = 'Active', 
                                          AccountId = listTestAccounts.get(0).Id);
        
        // Create a date instance to use in ICEF Event record insertion. //
        Date testDate = Date.newInstance(2015, 04, 09);
        // Inserting the ICEF Event record using the date instance. //
        ICEF_Event__c testICEFEvent = new ICEF_Event__c(Name='Test IcefEvent', 
                                                        Event_City__c = 'City', 
                                                        technical_event_name__c = 'TestEvent', 
                                                        Event_Manager_Educators__c = UserInfo.getUserId(), 
                                                        Event_Manager_Agents__c = UserInfo.getUserId(), 
                                                        Start_date__c = testDate);    
        
        insert new List<SObject>{ testContact, testICEFEvent };
        
        // Get valid RecordType for Account creation //
        Id idLeadRecordType = LeadTriggerHandler.getRecordTypeId('Refer a Partner - Agent', 'Lead');        
        // Get a valid lead conversion status.
        LeadStatus convertStatus = [SELECT Id, MasterLabel
            						FROM LeadStatus 
                                    WHERE IsConverted = true 
                                    LIMIT 1];
        
        countryEgy = [SELECT Id, Name, Agent_Relationship_Manager__c, Sales_Territory_Manager__c
                      FROM Country__c 
                      WHERE Name='Egypt'];
        System.debug('countryEgy ::'+countryEgy);
        
        // create a Lead with LeadSource = 'Refer a partner functionality'
        Lead testLead = new Lead(LastName = 'LastName', FirstName = 'FirstName', 
                                 Company = 'Test', Status = 'Open', Salutation = 'Ms',
                                 Gender__c = 'Female', RecordTypeId = idLeadRecordType,
                                 LeadSource = 'Refer a partner functionality',
                                 Additional_Comments_by_Client__c = 'Testing',
                                 ICEF_Event__c = testICEFEvent.Id, 
                                 Web_form_fill_out_Date__c = Date.today(),
                                 Referring_Account__c = listTestAccounts.get(0).Id,
                                 Referring_Contact__c = testContact.Id, 
                                 //Country__c = countryEgy.Id, 
                                 Country_Lookup__c = countryEgy.Id,
                                 Contact_Email__c = 'test123@gmail.com',
                                 Email = 'test123@gmail.com',
                                 NeverBounce_Email_Check__c = 'valid');        
       
        insert new List<SObject>{ testLead };
        Test.setMock(HttpCalloutMock.class, new mockHttpExchangeRatesRBA());
            Test.startTest();

		testLead = [SELECT Id, isconverted FROM Lead WHERE LastName = 'LastName' AND isConverted = false];
        System.debug('testLead ::' +testLead);
        
        // Create a list of lead conversions, then create a leadConvert object
        // and add it to the list of lead conversions to be executed.
		List<Database.LeadConvert> leadConversions = new List<Database.LeadConvert>();

        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(testLead.id);
        lc.setDoNotCreateOpportunity(true);
        lc.setConvertedStatus(convertStatus.MasterLabel);
        leadConversions.add(lc);        
        
        // Execute the conversions and check for success.
        results = Database.convertLead( leadConversions, false);
        //System.assert(results[0].isSuccess());          
        
        System.debug('results ::'+results);
        User userAgentRelMangr = [SELECT Id FROM User WHERE Email = 'user1@company.in'];
        
        // To test the update of Account the Lead is converted to 'Agent' recordtype 
        // if the Lead recordtype is 'Refer a Partner - Agent'
        Recommendation__c testRecommendation = [SELECT Id, Additional_Comments_by_Client__c, 
                                                ICEF_Event__c, Recommendation_Date__c, 
                                                Referring_Account__r.Name, Referring_Contact__r.Name,
                                                Recommended_Contact__r.Name, Recommended_Account__r.Name
                                                FROM Recommendation__c 
                                                WHERE Recommended_Contact__r.Name = 'FirstName LastName'];
        
		System.assertEquals(testRecommendation.Additional_comments_by_client__c,'Testing');
        
        // Verify the creation of Recommendation record only for 
        // LeadSource = 'Refer a partner functionality'
        List<Recommendation__c> listRecommendation = [SELECT Id FROM Recommendation__c];
        System.assertEquals(1, listRecommendation.size());     
        
        listTestAccounts = [SELECT Id, RecordType.Name FROM Account];
        
        // Verify the new Account created has its RecordTypethe 
        // 'Educator' updated to 'Agent' RecordType 
        Account testLeadAccount = [SELECT Id, Name, RecordType.Name, OwnerId 
                                   FROM Account 
                                   WHERE Id = :results[0].getAccountId()];        
        System.assertEquals('Agent', testLeadAccount.RecordType.Name);
        System.debug('testLeadAccount ::'+testLeadAccount);  
        
          Lead testLead2 = new Lead(LastName = 'LastName2', FirstName = 'FirstName2', 
                                 Company = 'Test', Status = 'Open', Salutation = 'Ms',
                                 Gender__c = 'Female', RecordTypeId = idLeadRecordType,
                                 LeadSource = 'Refer a partner functionality',
                                 Additional_Comments_by_Client__c = 'Testing',
                                 ICEF_Event__c = testICEFEvent.Id, 
                                 Web_form_fill_out_Date__c = Date.today(),
                                 Referring_Account__c = listTestAccounts.get(0).Id,
                                 Referring_Contact__c = testContact.Id, 
                                 //Country__c = countryEgy.Id, 
                                 Events_interested_in__c = 'Beijing',
                                 Country_Lookup__c = countryEgy.Id,
                                 Contact_Email__c = 'test123@gmail.com',
                                 Email = 'test123@gmail.com',
                                 NeverBounce_Email_Check__c = 'valid');     
       
        insert new List<SObject>{ testLead2 };
        testLead2 = [SELECT Id, isconverted FROM Lead WHERE LastName = 'LastName2' AND isConverted = false];
        
         Database.LeadConvert lc2 = new database.LeadConvert();
        lc2.setLeadId(testLead2.id);
        lc2.setDoNotCreateOpportunity(true);
        lc2.setConvertedStatus(convertStatus.MasterLabel);
        leadConversions.add(lc2);        
        
        // Execute the conversions and check for success.
        results = Database.convertLead( leadConversions, false);
        Test.stopTest();
    }
    
    /**
     * Creates Lead and converts it to trigger the LeadTrigger.
     */
    private static testMethod void testLeadTrigger1() {
        
        // Retrieve a record with RecordType to use in Account and Account Participation record insertion. //  
        mapAccRecordTypeIds = LeadTriggerHandler.getMapOfAccRecordTypeIds();
        System.debug('mapAccRecordTypeIds :: '+mapAccRecordTypeIds);  
        
        Id recTypeAgent = mapAccRecordTypeIds.get('Agent');
        System.debug('recTypeAgent ::' +recTypeAgent);
        
        // For Account Owner Update
        User testUser = new User();
        testUser.Username= 'user1@company.in';
        testUser.Email = 'user1@company.in';
        testUser.Lastname = 'user1';
        testUser.Firstname = 'test1';
        testUser.Alias = 'test1';
        testUser.CommunityNickname = '12346';
        testUser.UserRole = [ select id from userrole LIMIT 1 ];
        SObject prof = [ select id from profile LIMIT 1 ];
        testUser.ProfileId = (ID) prof.get('ID');
        testUser.TimeZoneSidKey = 'GMT';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.UserPermissionsMobileUser = false; 
        insert testUser;
        
        // Inserting Country record to use in Account and Account Participation record insertion. //
        Country__c testCountry = new Country__c(Name = 'Brazil', Region__c = 'USA');
        Country__c countryEgy = new Country__c(Name = 'Egypt', 
                                               Agent_Relationship_Manager__c = testUser.Id,
                                               Sales_Territory_Manager__c = testUser.Id);
        insert new List<SObject>{ testCountry, countryEgy };  
        
        // Inserting Account record using Country and RecordType Ids and use in Contact record. //
        Account testAccount = new Account(Name = 'Test Account', 
                                          RecordTypeId = recTypeAgent, 
                                          Status__c = 'Active', 
                                          Mailing_Country__c = testCountry.id);
        
        Account testAccount1 = new Account(Name = 'Test Account1', 
                                          RecordTypeId = recTypeAgent, 
                                          Status__c = 'Active', 
                                          Mailing_Country__c = testCountry.id);
        
        insert new List<SObject>{ testAccount, testAccount1 };
        
        List<Account> listTestAccounts = [SELECT Id, RecordType.Name FROM Account LIMIT 2];
        System.debug('listTestAccounts  ::' +listTestAccounts);
        
        // Inserting Contact record to use in Account Participation record insertion. //
        Contact testContact = new Contact(Salutation = 'Dr.', FirstName = 'Test', 
                                          LastName = 'Test', Gender__c = 'Male', 
                                          Role__c = 'Assistant', 
                                          Contact_Status__c = 'Active', 
                                          AccountId = listTestAccounts.get(0).Id);
        
        // Create a date instance to use in ICEF Event record insertion. //
        Date testDate = Date.newInstance(2015, 04, 09);
        // Inserting the ICEF Event record using the date instance. //
        ICEF_Event__c testICEFEvent = new ICEF_Event__c(Name='Test IcefEvent', 
                                                        Event_City__c = 'City', 
                                                        technical_event_name__c = 'TestEvent', 
                                                        Event_Manager_Educators__c = UserInfo.getUserId(), 
                                                        Event_Manager_Agents__c = UserInfo.getUserId(), 
                                                        Start_date__c = testDate);    
        
        insert new List<SObject>{ testContact, testICEFEvent };
        
        // Get valid RecordType for Account creation //
        Id idLeadRecordType = LeadTriggerHandler.getRecordTypeId('Refer a Partner - Agent', 'Lead');        
        // Get a valid lead conversion status.
        LeadStatus convertStatus = [SELECT Id, MasterLabel
            						FROM LeadStatus 
                                    WHERE IsConverted = true 
                                    LIMIT 1];
        
        countryEgy = [SELECT Id, Name, Agent_Relationship_Manager__c, Sales_Territory_Manager__c
                      FROM Country__c 
                      WHERE Name='Egypt'];
        System.debug('countryEgy ::'+countryEgy);
        
        Test.setMock(HttpCalloutMock.class, new mockHttpExchangeRatesRBA());
        Test.startTest();
        
        Lead testLead2 = new Lead(LastName = 'LastName2', FirstName = 'FirstName2', 
                                  Company = 'Test', Status = 'Open', Salutation = 'Ms',
                                  Gender__c = 'Female', RecordTypeId = idLeadRecordType,
                                  LeadSource = 'Refer a partner functionality',
                                  Additional_Comments_by_Client__c = 'Testing',
                                  ICEF_Event__c = testICEFEvent.Id, 
                                  Web_form_fill_out_Date__c = Date.today(),
                                  Referring_Account__c = listTestAccounts.get(0).Id,
                                  Referring_Contact__c = testContact.Id, 
                                  //Country__c = countryEgy.Id, 
                                  Events_interested_in__c = 'Beijing',
                                  Country_Lookup__c = countryEgy.Id,
                                  Contact_Email__c = 'test123@gmail.com',
                                  Email = 'test123@gmail.com',
                                  NeverBounce_Email_Check__c = 'valid');     
        
        insert new List<SObject>{ testLead2 };
            testLead2 = [SELECT Id, isconverted FROM Lead WHERE LastName = 'LastName2' AND isConverted = false];
        
		List<Database.LeadConvert> leadConversions = new List<Database.LeadConvert>();        
        Database.LeadConvert lc2 = new database.LeadConvert();
        lc2.setLeadId(testLead2.id);
        lc2.setDoNotCreateOpportunity(true);
        lc2.setConvertedStatus(convertStatus.MasterLabel);
        leadConversions.add(lc2);        
        
        // Execute the conversions and check for success.
        results = Database.convertLead( leadConversions, false);
        Test.stopTest();
    }
}

/* Setup method and a single test method is used here to manage  
 * the SOQL query limit exception on the inserting required associated 
 * records for the lead conversion process faced during development 
 * of this test class.
 */