/**
 *  @Purpose Tests the functionality of the setNumberOfCPRegistered
 *  @Author Dreamwares
 *  @Date 07 sept 2017
 */
@isTest
public class setNumberOfCPRegisteredTriggerTest{
    /**
        @ Purpose : Inserts all the records required for testing the functionality of the Trigger
    */ 
    @TestSetup static void insertTestData(){
        
        // insert all required data
        Id agentRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agent').getRecordTypeId();
        
        Country__c country1 = new Country__c(Name = 'Test Country 1', 
                                             Region__c = 'Middle East', 
                                             Sales_Territory_Manager__c = UserInfo.getUserId());
        insert country1;
        
        Account account1 = new Account(Name = 'Test Account', RecordTypeId = agentRecordTypeId, 
                                       Status__c = 'Active', Mailing_Country__c = country1.id,
                                       c2g__CODAAccountTradingCurrency__c = 'EUR',
                                       Mailing_City__c = 'test',
                                       ARM__c = UserInfo.getUserId());
        insert account1;
        update account1;
        
        contact contact1 = new Contact(Salutation = 'Dr.', FirstName = 'Test', LastName = 'Test', 
                                   Gender__c = 'male', Role__c = 'Assistant', Contact_Status__c = 'Active', 
                                   AccountId = account1.Id);
        insert contact1;        
        
        ICEF_Event__c icefEvent = new ICEF_Event__c(Name='Test IcefEvent', Event_City__c = 'test',
                                                    Event_Country__c = country1.ID,
                                                    technical_event_name__c = 'TestingEvent', 
                                                    Event_Manager_Educators__c = UserInfo.getUserId(), 
                                                    Event_Manager_Agents__c = UserInfo.getUserId(), 
                                                    Start_date__c=Date.today().addDays(-30));
        insert icefEvent;
        update icefEvent;
        
        Id EducatorRecordTypeIdForAP = Schema.SObjectType.Account_Participation__c.getRecordTypeInfosByName().get('Educator').getRecordTypeId();
        Account_Participation__c accountParticipationRec = new Account_Participation__c(Account__c = account1.Id, ICEF_Event__c = icefEvent.Id,
                                                                                        Organisational_Contact__c = contact1.Id, 
                                                                                        Participation_Status__c = 'accepted', 
                                                                                        Catalogue_Country__c = country1.Id,
                                                                                        Country_Section__c = country1.Id,
                                                                                        RecordTypeId = EducatorRecordTypeIdForAP);  
        accountParticipationRec.Newcomer__c = false;
        insert accountParticipationRec;
        
        Contact_Participation__c contactParticipationRec = new Contact_Participation__c(Contact__c = contact1.Id, ICEF_Event__c = icefEvent.Id, 
                                                                   Participation_Status__c = 'registered', 
                                                                   Catalogue_Position__c = 2, 
                                                                   Account_Participation__c =accountParticipationRec.Id);
        insert contactParticipationRec;       
        
    }
    /**
     *  @ Purpose To test Functionality  
    */  
    private static testMethod void updateContactParticipationRecordTest(){
        Account_Participation__c accountParticipationRec = [SELECT ID, Participation_Status__c, RecordTypeId 
                                                            FROM Account_Participation__c Limit 1];
        
        Contact_Participation__c contactParticipationRec = [SELECT ID, Account_Participation_Record_Type_Text__c,
                                                            Account_Participation_Status_Text__c
                                                            FROM Contact_Participation__c 
                                                            WHERE Account_Participation__c =: accountParticipationRec.Id Limit 1];
        
        contactParticipationRec.espstats_number_of_appointment__c = 12;
        Update contactParticipationRec;
        accountParticipationRec = [SELECT ID, Number_of_Appointments__c 
                                   FROM Account_Participation__c Limit 1];
        System.assertEquals(12,accountParticipationRec.Number_of_Appointments__c);
        delete contactParticipationRec;
    }
}