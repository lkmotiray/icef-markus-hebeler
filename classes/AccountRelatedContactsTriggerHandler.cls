/*
 *  @purpose Handler to tick X20_year_mag__c of Account if any related contact's X20_year_mag__c is ticked; Also Count number of account related contacts whose role is Primary Decision Maker
 *  @createdby Mamta Kukreja, Amol Kulthe
 *  @createddate 16/6/15
 */
public class AccountRelatedContactsTriggerHandler{

    //Creating a set of account ids
    public Set<ID> setAccountIds = new Set<ID>();
    
    //Creating a set of pmd account ids
    public Set<ID> setPMDAccountIds = new Set<ID>();
    
    //Creating a set of both the above sets for querying purpose
    public Set<ID> setMergedAccountIds = new Set<ID>();
    
    //in case of update, check if checkbox value is changed from ticked to unticked and vice versa, and add it to the set
    Set<ID> setCheckAccountIds = new Set<ID>();
    
    //Is used to avoid repeated updations
    public static Boolean IsRunFirstTime = true;
    
    /*
      Method Name :- clearSets
      purpose     :- Clearing all sets
      created by  :- Mamta Kukreja
   */
    public void clearSets(){
        setAccountIds.clear();
        setPMDAccountIds.clear();
        setMergedAccountIds.clear();
        setCheckAccountIds.clear();
    }
    
    
    /*
      Method Name :- mergeSets
      purpose     :- Merging all sets
      created by  :- Mamta Kukreja
   */
    public void mergeSets(){
        //Merging both sets
        if(setAccountIds.size() > 0)
            setMergedAccountIds.addAll(setAccountIds);
            
        if(setPMDAccountIds.size() > 0)
            setMergedAccountIds.addAll(setPMDAccountIds);
            
        if(setCheckAccountIds.size() > 0)
            setMergedAccountIds.addAll(setCheckAccountIds);
        
        //If set is not empty then do updations
        if(setMergedAccountIds.size() > 0) {
            //updateAccount();
            AccountRelatedContactsTriggerHandler.updateAccount(setAccountIds, setPMDAccountIds, setCheckAccountIds);
        }            
    }
     
    @Future
    public static void updateAccount(Set<Id> setAccountIds, Set<Id> setPMDAccountIds, Set<Id> setCheckAccountIds) {
        //Stores the count of PMD contacts for a particular account
        Integer countPMDContacts;
        Set<Id> setMergedAccountIds = new Set<Id>();
        setMergedAccountIds.addAll(setAccountIds);
        setMergedAccountIds.addAll(setPMDAccountIds);
        setMergedAccountIds.addAll(setCheckAccountIds);
        
        if(setMergedAccountIds.isEmpty()) { return; }
        
        //Creates a map of account id along with its records so as to avoid duplicacies
        Map <Id, Account> mapUpdateAccounts = new Map <Id, Account>();
        Account accRec;
        
        for(Account accountRecord: [SELECT Id, Number_of_PMD_Contacts__c, X20_year_mag__c, 
                                    (SELECT Contact.Id, Contact.Role__c, Contact.X20_year_mag__c  
                                     FROM Account.Contacts) 
                                    FROM Account
                                    WHERE Id IN :setMergedAccountIds
                                    LIMIT 50000]){
                                    
            if(setPMDAccountIds.contains(accountRecord.Id)) {
                //Count pmd contacts
                
                countPMDContacts = 0;
                //Get the list of all contacts related to current account object
                for(Contact contactRecord : accountRecord.Contacts)
                {
                    if(contactRecord.Role__c == 'Primary Decision Maker')
                        countPMDContacts++;
                }
                System.debug('countPMDContacts : '+countPMDContacts);
                accountRecord.Number_of_PMD_Contacts__c = countPMDContacts;
                mapUpdateAccounts.put(accountRecord.Id, accountRecord); 
            }
            
            if(setAccountIds.contains(accountRecord.Id)){
                System.debug('set 20 yr mag to true');
                //Set X20_year_mag__c to true
                if(mapUpdateAccounts.containsKey(accountRecord.Id)){
                    accRec = mapUpdateAccounts.get(accountRecord.Id);
                    accRec.X20_year_mag__c = true;
                }
                else {
                    accountRecord.X20_year_mag__c = true;
                    mapUpdateAccounts.put(accountRecord.Id, accountRecord); 
                } 
            }
            
            if(setCheckAccountIds.contains(accountRecord.Id)){

                Boolean flag = false;
                for(Contact contactRecord : accountRecord.Contacts){       
                    //if any 1 is ticked then don't do anything
                    if(contactRecord.X20_year_mag__c == true){
                        flag = true;
                        break;
                    }
                }
                if(flag != true){
                    //if no ticked rec found then make that acc checkbox unticked
                    System.debug('set 20 yr mag to false');
                    if(mapUpdateAccounts.containsKey(accountRecord.Id)){
                        accRec = mapUpdateAccounts.get(accountRecord.Id);
                        accRec.X20_year_mag__c = false;
                        System.debug('20 yr if X20_year_mag__c:: '+accRec.X20_year_mag__c);
                    }
                    else {
                        accountRecord.X20_year_mag__c = false;
                        mapUpdateAccounts.put(accountRecord.Id, accountRecord);  
                    } 
                }
            }   
        }
        
        System.debug('mapUpdateAccounts : '+mapUpdateAccounts);
        if(mapUpdateAccounts.size() > 0) {
            try{
                
                update mapUpdateAccounts.values();
                
                System.debug('Updated Account::'+[Select Id, X20_year_mag__c from Account where Id IN: mapUpdateAccounts.keySet()]);
            }
            catch(Exception e) {                
                System.debug('Message : '+e.getMessage());    
            }  
        }
    }
    
    /*
      Method Name :- updateAccount
      purpose     :- Updating account records whose id is present in setMergedAccountIds
      created by  :- Mamta Kukreja
   */
    /*public void updateAccount(){
        //Stores the count of PMD contacts for a particular account
        Integer countPMDContacts;
        
        //Creates a map of account id along with its records so as to avoid duplicacies
        Map <Id, Account> mapUpdateAccounts = new Map <Id, Account>();
        Account accRec;
        
        for(Account accountRecord: [SELECT Id, Number_of_PMD_Contacts__c, X20_year_mag__c, 
                                    (SELECT Contact.Id, Contact.Role__c, Contact.X20_year_mag__c  
                                     FROM Account.Contacts) 
                                    FROM Account
                                    WHERE Id IN :setMergedAccountIds
                                    LIMIT 50000]){
                                    
            if(setPMDAccountIds.contains(accountRecord.Id)) {
                //Count pmd contacts
                
                countPMDContacts = 0;
                //Get the list of all contacts related to current account object
                for(Contact contactRecord : accountRecord.Contacts)
                {
                    if(contactRecord.Role__c == 'Primary Decision Maker')
                        countPMDContacts++;
                }
                System.debug('countPMDContacts : '+countPMDContacts);
                accountRecord.Number_of_PMD_Contacts__c = countPMDContacts;
                mapUpdateAccounts.put(accountRecord.Id, accountRecord); 
            }
            
            if(setAccountIds.contains(accountRecord.Id)){
                System.debug('set 20 yr mag to true');
                //Set X20_year_mag__c to true
                if(mapUpdateAccounts.containsKey(accountRecord.Id)){
                    accRec = mapUpdateAccounts.get(accountRecord.Id);
                    accRec.X20_year_mag__c = true;
                }
                else {
                    accountRecord.X20_year_mag__c = true;
                    mapUpdateAccounts.put(accountRecord.Id, accountRecord); 
                } 
            }
            
            if(setCheckAccountIds.contains(accountRecord.Id)){

                Boolean flag = false;
                for(Contact contactRecord : accountRecord.Contacts){       
                    //if any 1 is ticked then don't do anything
                    if(contactRecord.X20_year_mag__c == true){
                        flag = true;
                        break;
                    }
                }
                if(flag != true){
                    //if no ticked rec found then make that acc checkbox unticked
                    System.debug('set 20 yr mag to false');
                    if(mapUpdateAccounts.containsKey(accountRecord.Id)){
                        accRec = mapUpdateAccounts.get(accountRecord.Id);
                        accRec.X20_year_mag__c = false;
                        System.debug('20 yr if X20_year_mag__c:: '+accRec.X20_year_mag__c);
                    }
                    else {
                        accountRecord.X20_year_mag__c = false;
                        mapUpdateAccounts.put(accountRecord.Id, accountRecord);  
                    } 
                }
            }   
        }
        
        System.debug('mapUpdateAccounts : '+mapUpdateAccounts);
        if(mapUpdateAccounts.size() > 0) {
            try{
                
                update mapUpdateAccounts.values();
                
                System.debug('Up[dated Account::'+[Select Id, X20_year_mag__c from Account where Id IN: mapUpdateAccounts.keySet()]);
            }
            catch(Exception e) {                
                System.debug('Message : '+e.getMessage());    
            }  
        }
    }*/
    
    /*
      Method Name :- checkInsertAndUndelete
      purpose     :- Check Field X20_year_mag__c and Number_of_PMD_Contacts__c on Account object after insert and undelete of contact
      created by  :- Mamta Kukreja, Amol Kulthe
   */
    public void checkInsertAndUndelete(List<Contact> triggerContactList){
    
        //in case of insert and undelete, if X20_year_mag__c is ticked then tick respective account record's X20 year mag
        //If role of new contact is Primary Decision Maker then update respective account's number of pmd contacts
		Set<ID> accountIds = new Set<ID>();
        clearSets();
        
        for(Contact contactRecord : triggerContactList) {
			//get set of IDs to check for existing contact related with these account IDs
			accountIds.add(contactRecord.AccountId);
		
            //Adding account ids to the set if the contact's X20_year_mag__c is checked
            if(contactRecord.X20_year_mag__c == true) {
                setAccountIds.add(contactRecord.AccountId);
            }
            //Adding account ids to the set if the contact's role is Primary Decision Maker
            if(contactRecord.Role__c == 'Primary Decision Maker') {
                setPMDAccountIds.add(contactRecord.AccountId);
            } 
        }
        
        checkForExistingContacts(new List<Id>(accountIds), true, new List<Id>());
		
        mergeSets();
    }
    
    /*
      Method Name :- checkUpdate
      purpose     :- Check Field X20_year_mag__c, Number_of_PMD_Contacts__c on Account object after update of contact
      created by  :- Mamta Kukreja, Amol Kulthe
   */
    public void checkUpdate(List<Contact> triggerContactList, Map<Id, Contact> triggerOldContactMap){
		
		Set<ID> accountIds = new Set<ID>();
		Set<ID> contactIds = new Set<ID>();
		
        clearSets();
    
        for(Contact contactRecord : triggerContactList) {
			//get set of IDs to check for existing contact related with these account IDs
			accountIds.add(contactRecord.AccountId);
			
			//set of contact records to avoid in all contacts of above account IDs
			contactIds.add(contactRecord.Id);
		
            // Access the "old" record by its ID in Trigger.oldMap
            Contact oldContact = triggerOldContactMap.get(contactRecord.Id);
            
            // Check that the field was changed to another value
            if (!oldContact.X20_year_mag__c && contactRecord.X20_year_mag__c) { // earlier unticked, now ticked
                //tick account's field
                setAccountIds.add(contactRecord.AccountId);
            }
            else if(oldContact.X20_year_mag__c && !contactRecord.X20_year_mag__c) { //earlier ticked, now unticked
                //add account ids to the set which have to be checked
                setCheckAccountIds.add(contactRecord.AccountId);
            }
            System.debug('UpdatedContactRecord::' + contactRecord);
            System.debug('contactRecord.Role__c:: ' +contactRecord.Role__c
                         + 'contactRecord.Contact_Status__c' + contactRecord.Contact_Status__c);
            if(((contactRecord.Role__c == 'Primary Decision Maker' && oldContact.Role__c != 'Primary Decision Maker')||
                (contactRecord.Role__c != 'Primary Decision Maker' && oldContact.Role__c == 'Primary Decision Maker'))  
                /*&& (contactRecord.Contact_Status__c == 'Active')*/) {
                    
                    setPMDAccountIds.add(contactRecord.AccountId);
            }
        }
        
		checkForExistingContacts(new List<Id>(accountIds), false, new List<Id>(contactIds));
		
        mergeSets();
    }
    
    /*
      Method Name :- checkDelete
      purpose     :- Check Field X20_year_mag__c, Number_of_PMD_Contacts__c on Account object after delete of contact
      created by  :- Mamta Kukreja, Amol Kulthe
   */
    public void checkDelete(List<Contact> triggerContactList){
        Set<ID> accountIds = new Set<ID>();
		Set<ID> contactIds = new Set<ID>();
		
        //in case of delete, if its ticked then check if any other contact is ticked. If not then untick respective account record's field
        
        clearSets();
        
        //Adding account ids to the set if the contact's X20_year_mag__c is checked
        for(Contact contactRecord : triggerContactList) {
            //get set of IDs to check for existing contact related with these account IDs
			accountIds.add(contactRecord.AccountId);
			
			//set of contact records to avoid in all contacts of above account IDs
			contactIds.add(contactRecord.Id);
			
			if(contactRecord.Role__c == 'Primary Decision Maker') {
                setPMDAccountIds.add(contactRecord.AccountId);
            } 
            
            if(contactRecord.X20_year_mag__c == true) {
                setCheckAccountIds.add(contactRecord.AccountId);
            }
        }
        
		checkForExistingContacts(new List<Id>(accountIds), false, new List<Id>(contactIds));
		
        mergeSets();       
    } 
	
	
	/*
      Method Name :- checkForExistingContacts
      purpose     :- Check Field X20_year_mag__c, Number_of_PMD_Contacts__c for existing Contacts on Account object on each operation
      created by  :- Urmila
   */
    public void checkForExistingContacts(List<Id> accountIdList, Boolean isInsert, List<Id> contactIdList){
		List<Account> accountList = new List<Account>();
		try {
			if(isInsert) {
				accountList = [SELECT Id, Number_of_PMD_Contacts__c, X20_year_mag__c, 
									 (SELECT Contact.Id, Contact.Role__c, Contact.X20_year_mag__c, AccountId 
									  FROM Account.Contacts) 
							   FROM Account
							   WHERE Id IN :accountIdList
                               LIMIT 50000];
			}
			else {
				accountList = [SELECT Id, Number_of_PMD_Contacts__c, X20_year_mag__c, 
									 (SELECT Contact.Id, Contact.Role__c, Contact.X20_year_mag__c, AccountId 
									  FROM Account.Contacts
									  WHERE Id NOT IN : contactIdList) 
							   FROM Account
							   WHERE Id IN :accountIdList
                               LIMIT 50000];
			}
		}
		catch(Exception e) {
			System.debug('Exception caught:::'+e.getMessage());
		}
		for(Account accountRecord: accountList) {                                    
                //Adding account ids to the set if the contact's X20_year_mag__c is checked
			for(Contact contactRecord : accountRecord.Contacts) {
				if(contactRecord.Role__c == 'Primary Decision Maker') {
					setPMDAccountIds.add(contactRecord.AccountId);
				} 
				
				if(contactRecord.X20_year_mag__c == true) {
					setAccountIds.add(contactRecord.AccountId);
				}
			}
		}		
	}
}