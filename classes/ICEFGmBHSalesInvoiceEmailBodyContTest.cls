@isTest(SeeAllData = true)
public class ICEFGmBHSalesInvoiceEmailBodyContTest {
	
    private static void setupTestData() {
        
        List<Account> accountList  = [SELECT ID FROM ACCOUNT LIMIT 1];
        List<c2g__codaAccountingCurrency__c> accountCurrencyList = [SELECT ID FROM c2g__codaAccountingCurrency__c LIMIT 1];
        
        c2g__codaInvoice__c invoiceToInsert = new c2g__codaInvoice__c(); 
        invoiceToInsert.c2g__InvoiceDate__c = Date.newInstance(2017,2,17);
        invoiceToInsert.c2g__DueDate__c = Date.newInstance(2017,3,17);
        invoiceToInsert.c2g__Account__c = accountList[0].Id;
        invoiceToInsert.c2g__InvoiceCurrency__c = accountCurrencyList[0].id;
        
        INSERT invoiceToInsert;
        
        c2g__codaInvoiceInstallmentLineItem__c installMentLineItem = new c2g__codaInvoiceInstallmentLineItem__c();
        installMentLineItem.c2g__Amount__c = 0;
        installMentLineItem.c2g__DueDate__c = system.today();
        installMentLineItem.c2g__Invoice__c = invoiceToInsert.Id;
    
        INSERT installMentLineItem;
        
        
        /*c2g__codaInvoice__c invoiceToInsert2 = new c2g__codaInvoice__c(); 
        invoiceToInsert2.c2g__InvoiceDate__c = Date.newInstance(2017,2,17);
        invoiceToInsert2.c2g__DueDate__c = Date.newInstance(2017,3,17);
        invoiceToInsert2.c2g__Account__c = accountList[0].Id;
        invoiceToInsert2.c2g__InvoiceCurrency__c = accountCurrencyList[0].id;
		invoiceToInsert2.c2g__NetTotal__c  = 1;
        invoiceToInsert2.c2g__TaxTotal__c = 0.5;
        INSERT invoiceToInsert2;
        
        c2g__codaInvoiceInstallmentLineItem__c installMentLineItem2 = new c2g__codaInvoiceInstallmentLineItem__c();
        installMentLineItem2.c2g__Amount__c = 1;
        installMentLineItem2.c2g__DueDate__c = system.today();
        installMentLineItem2.c2g__Invoice__c = invoiceToInsert2.Id;
    
        INSERT installMentLineItem2;*/
    } 
    
    static testMethod void testTestPSComponentController() {  
        ICEFGmBHSalesInvoiceEmailBodyContTest.setupTestData();
        /*c2g__codaInvoice__c salesInvoice = [ SELECT Id 
                                             FROM c2g__codaInvoice__c 
                                             WHERE Id IN (SELECT c2g__Invoice__c 
                                                 FROM c2g__codaInvoiceInstallmentLineItem__c)
                                             ORDER BY c2g__Amount__c ASC 
                                             LIMIT 10 ];*/
        Test.startTest();
        	List<c2g__codaInvoice__c>  listInvoice = [SELECT Id 
                                             		 FROM c2g__codaInvoice__c 
                                                     WHERE Id IN (SELECT c2g__Invoice__c 
                                                         FROM c2g__codaInvoiceInstallmentLineItem__c)
                                                     Order by c2g__Account__c ASC
                                                     LIMIT 20];
        	
        	Set<Id> setId = new Set<Id>();	
        	for(c2g__codaInvoice__c salesInvoice : listInvoice){   
                setId.add(salesInvoice.id);
            }
        	
        	InvoiceEmailSend.mapInvoiceLines = InvoiceEmailSend.createMapInvoceineItems(setId);
            InvoiceEmailSend.mapInvoices = InvoiceEmailSend.createMapInvoces(setId);
        
        	/*for(c2g__codaInvoice__c salesInvoice: [  SELECT Id 
                                             		 FROM c2g__codaInvoice__c 
                                                     WHERE Id IN (SELECT c2g__Invoice__c 
                                                         FROM c2g__codaInvoiceInstallmentLineItem__c)
                                                     Order by c2g__Account__c ASC
                                                     LIMIT 20]){*/
                                                       
                                                    
        	for(c2g__codaInvoice__c salesInvoice : listInvoice){   
        		ICEFGmBHSalesInvoiceEmailBodyController controller = new ICEFGmBHSalesInvoiceEmailBodyController();
            	controller.salesInvoiceInstance = salesInvoice;
            	controller.getSizeOfPaymentSchedules();                                        
            }
            
        Test.stopTest();
    }  
    
}