public class coursefinderSchoolTriggerHelper {
    
    /******************************
     * function to update Names of Agencies in Coursefinder School (CP_Agency_Names__c)
     * parameter: Map of Coursefinder Schools
	*******************************/
    public static void updateNamesOfAgencies (Map<Id, Coursefinder_School__c> CfSchoolMap){
        //get CP Agencies for Cf Schools
        Map<Id,CoursePricer_Agency__c> cpAgencyMap = new Map<Id, CoursePricer_Agency__c>([SELECT Agency_Name_text__c,CP_link__c, Coursefinder_School__c 
                                                                                          FROM CoursePricer_Agency__c 
                                                   										  WHERE Coursefinder_School__c IN :CfSchoolMap.keySet()
                                                                                          AND currently_active__c = true
                                                                                          ORDER BY LastModifiedDate DESC]);
        //create a Mapping for Coursefinder School Id and relative CP Agencies
        Map<Id, List<CoursePricer_Agency__c>> cfSchoolIdCpAgenciesMapping = new Map<Id, List<CoursePricer_Agency__c>>();
        for(Coursepricer_Agency__c cpAgency : cpAgencyMap.values()){
            if(cfSchoolIdCpAgenciesMapping.containsKey(cpAgency.Coursefinder_School__c)){
                //if key for CF Agency is already existing, add CP Agency to Agency list for this CF School
                cfSchoolIdCpAgenciesMapping.get(cpAgency.Coursefinder_School__c).add(cpAgency);
                //otherwise add new mapping for CF school
            } else {
                List<CoursePricer_Agency__c> agenciesForThisSchool = new List<Coursepricer_Agency__c>();
                agenciesForThisSchool.add(cpAgency);
                cfSchoolIdCpAgenciesMapping.put(cpAgency.Coursefinder_School__c, agenciesForThisSchool);
            }
        }
        // update field
        for(Coursefinder_School__c cfSchool : CfSchoolMap.values()){
            List<CoursePricer_Agency__c> cpAgencies = new List<CoursePricer_Agency__c>();
            Set<Id> cfSchoolsWithAgencies = cfSchoolIdCpAgenciesMapping.keySet();
            if (cfSchoolsWithAgencies.contains(cfSchool.Id)){
                cpAgencies = cfSchoolIdCpAgenciesMapping.get(cfSchool.Id);
            }            
            String[] cpAgencyNames = new String[]{};
            String[] cpAgencyLinks = new String[]{};
            if (cpAgencies.size() > 0){
                for (CoursePricer_Agency__c cpAgency : cpAgencies){
    	       		cpAgencyNames.add(cpAgency.Agency_Name_text__c);
               		cpAgencyLinks.add(cpAgency.CP_link__c);    
            	}
                
        	}                    

	        //String from list
    	    String cpNamesString = '';
            String cpLinksString = '';
        	if (cpAgencyNames.size()> 0){
        		cpNamesString = String.join(cpAgencyNames, '\n');
                cpLinksString = String.join(cpAgencyLinks, '\n');
            } else {
                cpNamesString = '';
                cpLinksString = '';
            }
//        	System.debug('cpNamesString: '+ cpNamesString);
//        	System.debug('cpLinksString: ' + cpLinksString);
        	cfSchool.CP_Agency_Names__c = cpNamesString;
            cfSchool.CP_Agency_links__c = cpLinksString;
            if(cpAgencyNames.size() > 0){
                cfSchool.CP_Agency_last_added__c = cpAgencyNames[0];
            } else {
                cfSchool.CP_Agency_last_added__c = '';
            }
            
        }
    }
}