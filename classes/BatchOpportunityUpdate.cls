/*
@ Name          : batchOpportunityUpdate
@ Description   : Update Opportunity list with Number_of_Sales_Invoices__c
@ Created By    : 
@ Date          : 22-jan-2016
@ Calling From  : 
*/
global class BatchOpportunityUpdate implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
         String query;
        if(Test.isRunningTest()){
            query = 'SELECT Id, Number_of_Sales_Invoices__c,(SELECT Id FROM c2g__Invoices__r Where c2g__InvoiceStatus__c = \'Complete\') FROM Opportunity where Invoicing_contact__c != null and StageName != \'Closed Won\' LIMIT 1 ';
        }else{
            query = 'SELECT Id, Number_of_Sales_Invoices__c,(SELECT Id FROM c2g__Invoices__r Where c2g__InvoiceStatus__c = \'Complete\') FROM Opportunity ';
        }
        
        return Database.getQueryLocator(query);
    }   
    
    global void execute(Database.BatchableContext BC, List<Opportunity> opportunityList) {
         List<Opportunity> updatingOpportunityList = new List<Opportunity>();
         for(Opportunity oppRec : opportunityList){
            System.debug('Total Sales Invoices::'+oppRec.c2g__Invoices__r.size());
            oppRec.Number_of_Sales_Invoices__c = oppRec.c2g__Invoices__r.size();
            updatingOpportunityList.add(oppRec);  
         }
         if( updatingOpportunityList.size() > 0){
             update updatingOpportunityList; 
         }
    }   
    
    global void finish(Database.BatchableContext BC) {
        
    }
}