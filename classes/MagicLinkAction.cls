/* Invocable Method to update Magic Link in Contact
 */
public with sharing class MagicLinkAction {
    
    @InvocableMethod(label='generate magic link' description='generate magic link in Contact')
    public static void generateMagicLink(List<Id> contactIds){
        MagicLinkQueueable newJob = new MagicLinkQueueable(contactIds);
        ID jobId = System.enqueueJob(newJob);
    }
      
    
}