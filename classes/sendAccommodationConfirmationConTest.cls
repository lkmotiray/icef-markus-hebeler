/*
Purpose : test class for sendAccommodationConfirmationCon
*/
@isTest
public class sendAccommodationConfirmationConTest {
    
    @testSetup
    private static void setTestData(){
        
        // create country
        String countryId = createCountry();
        
        // create ICEF Event
        String icefEventId = createICEFEvent(countryId);
        
        // Create Account
        String accountId = createAccount(countryId);
        
        // create contact
        String contactId = CreateContact(accountId);
        
        // create Account participation
        String accountParticipationId = createAccountParticipation(accountId,icefEventId,countryId,contactId);
        
        // create Contact participation
        String contactPartId = createContactParticipation(contactId,icefEventId,accountParticipationId);
        
        // create hotel
        String hotelId = createHotel(countryId);
        
        // create Hotel Capacity
        String hotelCapacityId = createHotelCapacity(icefEventId,hotelId);
        
        // create hotel rooms 
        String hotelRoomId = createHotelRoom(hotelCapacityId);
        
        // create Accomodation
        createAccommodation(hotelRoomId,contactPartId,accountParticipationId);
        
    }
    
    public static testmethod void validateFunctionality(){
        
        // Accommodation CP ET
        Accommodation__c accommodation = [SELECT Id,Contact_Participation__c  FROM Accommodation__c LIMIT 1];
        EmailTemplate emailTemplateRecord = [SELECT Id,Body FROM EmailTemplate LIMIT 1];
        
        PageReference sendAccommodationConfirmationPage = Page.sendAccommodationConfirmation;
        sendAccommodationConfirmationPage.getParameters().put('id',accommodation.Id); 
        sendAccommodationConfirmationPage.getParameters().put('cpid',accommodation.Contact_Participation__c  ); 
        sendAccommodationConfirmationPage.getParameters().put('etid',emailTemplateRecord.Id); 
        system.Test.setCurrentPage(sendAccommodationConfirmationPage);
        
        //system.debug('Test EmailTemplate '+emailTemplateRecord);
        sendAccommodationConfirmationCon sendAccommodationConfirmationObj = new sendAccommodationConfirmationCon();
        sendAccommodationConfirmationObj.Subject = 'Test23';
        sendAccommodationConfirmationObj.body ='{!Accommodation__c.Name}:{!Contact_Participation__c.Name}'; 
        
        sendAccommodationConfirmationObj.accommodationFieldList.addAll(new List<string>{'Accommodation__c.Name',
            'Accommodation__c.Names_different__c',
            'Accommodation__c.CreatedBy',
            'Accommodation__c.CreatedDate',
            'Accommodation__c.Account_Part__c',
            'Accommodation__c.LastModifiedBy'
            });
        
        sendAccommodationConfirmationObj.accommodationMergeFieldList.addAll(new List<string>{'Name',
            'Names_different__c',
            'CreatedBy',
            'CreatedDate',
            'Account_Part__c',
            'LastModifiedBy'
            });
        
        sendAccommodationConfirmationObj.cpFieldList.addAll(new List<string>{'Contact_Participation__c.Name',
            'Contact_Participation__c.ANZ_Lunch__c',
            'Contact_Participation__c.CreatedBy',
            'Contact_Participation__c.CreatedDate',
            'Contact_Participation__c.Account_Part__c',
            'Contact_Participation__c.LastModifiedBy',
            'Contact_Participation__c.RecordType'
            });
        sendAccommodationConfirmationObj.cpMergeFieldList.addAll(new List<string>{'Name',
            'ANZ_Lunch__c',
            'CreatedBy',
            'CreatedDate',
            'Account_Part__c',
            'LastModifiedBy',
            'RecordType'
            });
        
        String selectOption = sendAccommodationConfirmationObj.selectedOption;
        
        sendAccommodationConfirmationObj.getItems();
        
        sendAccommodationConfirmationObj.recipientEmailId  = 'test@test.com';
        sendAccommodationConfirmationObj.selectedOption = '<test@test.com>';
        sendAccommodationConfirmationObj.attachEmailBodyAsPDF  = true;
        sendAccommodationConfirmationObj.recipientEmailIdList.add('test@test.com');
        sendAccommodationConfirmationObj.sendEmail();
        sendAccommodationConfirmationObj.showError();
        sendAccommodationConfirmationObj.validateIds();
        sendAccommodationConfirmationObj.getObjectName(accommodation.Id);
        
        Pagereference pageRef = sendAccommodationConfirmationObj.cancel();
        pageRef = sendAccommodationConfirmationObj.showError();
        
    }  
    
    public static testmethod void testExecutePopulate(){
        
        // Accommodation CP ET
        Accommodation__c accommodation = [SELECT Id,Contact_Participation__c  FROM Accommodation__c LIMIT 1];
        EmailTemplate emailTemplateRecord = [SELECT Id,Body FROM EmailTemplate LIMIT 1];
        Contact_Participation__c contactPart = [SELECT ID FROM Contact_Participation__c LIMIT 1];
        
        
        PageReference sendAccommodationConfirmationPage = Page.sendAccommodationConfirmation;
        sendAccommodationConfirmationPage.getParameters().put('id',accommodation.Id); 
        sendAccommodationConfirmationPage.getParameters().put('cpid',accommodation.Contact_Participation__c  ); 
        sendAccommodationConfirmationPage.getParameters().put('etid',emailTemplateRecord.Id); 
        system.Test.setCurrentPage(sendAccommodationConfirmationPage);
        
        //system.debug('Test EmailTemplate '+emailTemplateRecord);
        sendAccommodationConfirmationCon sendAccommodationConfirmationObj = new sendAccommodationConfirmationCon();
        sendAccommodationConfirmationObj.Subject = 'Test23';
        sendAccommodationConfirmationObj.body ='{!Accommodation__c.Name}:{!Contact_Participation__c.Name}'; 
        
        sendAccommodationConfirmationObj.accommodationFieldList.addAll(new List<string>{'Accommodation__c.Name',
            'Accommodation__c.Names_different__c',
            'Accommodation__c.CreatedBy',
            'Accommodation__c.CreatedDate',
            'Accommodation__c.Account_Part__c',
            'Accommodation__c.LastModifiedBy'
            });
        
        sendAccommodationConfirmationObj.accommodationMergeFieldList.addAll(new List<string>{'Name',
            'Names_different__c',
            'CreatedBy',
            'CreatedDate',
            'Account_Part__c',
            'LastModifiedBy'
            });
        
        sendAccommodationConfirmationObj.cpFieldList.addAll(new List<string>{'Contact_Participation__c.Name',
            'Contact_Participation__c.ANZ_Lunch__c',
            'Contact_Participation__c.CreatedBy',
            'Contact_Participation__c.CreatedDate',
            'Contact_Participation__c.Account_Part__c',
            'Contact_Participation__c.LastModifiedBy',
            'Contact_Participation__c.RecordType'
            });
        sendAccommodationConfirmationObj.cpMergeFieldList.addAll(new List<string>{'Name',
            'ANZ_Lunch__c',
            'CreatedBy',
            'CreatedDate',
            'Account_Part__c',
            'LastModifiedBy',
            'RecordType'
            });
        
        sendAccommodationConfirmationObj.executeMethodsOnPageLoad();
    }  
    
    public static testmethod void validateFunctionalityNegatively(){
        Accommodation__c accommodation = [SELECT Id, Contact_Participation__c, Contact_Participation__r.Preferred_WS_Email__c,
                                          Contact_Participation__r.Workshop_CC_Mailers__c
                                          FROM Accommodation__c LIMIT 1];
        
        EmailTemplate emailTemplateRecord = [SELECT Id,Body FROM EmailTemplate LIMIT 1];
        
        accommodation.Contact_Participation__r.Preferred_WS_Email__c = null;
        accommodation.Contact_Participation__r.Workshop_CC_Mailers__c = null;
        
        update accommodation.Contact_Participation__r;
        sendAccommodationConfirmationCon sendAccommodationConfirmationObj = new sendAccommodationConfirmationCon();
        sendAccommodationConfirmationObj.executeMethodsOnPageLoad();
        String testVariable = sendAccommodationConfirmationObj.testVariable; 
    }
    
    public static string createAccommodation(String hotelRoomId, String contactPartId, String accountPartId){
        
        Accommodation__c accommodation = new Accommodation__c();
        accommodation.Hotel_Room__c = hotelRoomId;
        accommodation.Contact_Participation__c = contactPartId;
        accommodation.Account_Part__c = accountPartId;
        insert accommodation;
        
        return accommodation.Id;
    }
    
    public static string createHotelRoom(String hotelCapacityId){
        
        Hotel_Room__c hotelRoom = new Hotel_Room__c();
        hotelRoom.Hotel_Capacity__c = hotelCapacityId;
        Insert hotelRoom;
        return hotelRoom.Id;
    }
    
    public static string createEmailTemplate(){
        
        BrandTemplate brandTemplateRecord = [SELECT Id From BrandTemplate LIMIT 1];
        
        Folder folderRecord = [SELECT Id FROM Folder LIMIT 1];
        
        
        EmailTemplate emailTemplateBody = new EmailTemplate();
        emailTemplateBody.body = '{!Accommodation__c.Name}:{!Contact_Participation__c.Name}';
        emailTemplateBody.Subject = 'Test23';
        emailTemplateBody.Name = 'Test23';
        emailTemplateBody.DeveloperName = 'Test23';
        emailTemplateBody.TemplateType = 'html';
        emailTemplateBody.FolderId = folderRecord.Id;
        emailTemplateBody.BrandTemplateId = brandTemplateRecord.Id;
        emailTemplateBody.TemplateStyle = 'formalLetter';
        Insert emailTemplateBody;
        return emailTemplateBody.Id; 
    }  
    
    public static string createHotelCapacity(String icefEventId,String hotelId){
        
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Agent Contingent'
                                       AND SobjectType = 'Hotel_Capacity__c' ];
        User userRecord = [SELECT Id,Email FROM User WHERE Profile.Name = 'System Administrator' LIMIT 1];
        Hotel_Capacity__c hotelCapacityRecord = new Hotel_Capacity__c( RecordType = recordTypeRecord,
                                                                      Name = 'TestCapacity',
                                                                      ICEF_Event__c = icefEventId,
                                                                      Hotel__c = hotelId,
                                                                      Event_Manager_Agents__c = userRecord.Id,
                                                                      Event_Manager_Agents_Email__c = userRecord.Email,
                                                                      Event_Manager_Providers__c = userRecord.Id,
                                                                      Event_Manager_Provider_Emails__c = userRecord.Email,
                                                                      Security_Deposit__c = 'Credit Card Details only'
                                                                     ); 
        insert hotelCapacityRecord;
        return hotelCapacityRecord.Id;
        
    }
    
    public static String createHotel(String countryId){
        
        // User userRecord = [SELECT Id FROM User LIMIT 1];
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Hotel'
                                       AND SobjectType = 'Account' ];
        
        Account accountRecord = new Account( RecordType = recordTypeRecord,
                                            Name = 'Test',
                                            Status__c = 'Active',
                                            Mailing_Country__c = countryId
                                           );
        
        insert accountRecord;
        return accountRecord.Id;
    }
    
    public static string createContactParticipation( String contactId, String icefEventId, String accountParticipationId){ 
        
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Workshop Participant'
                                       AND SobjectType = 'Contact_Participation__c' ];
        
        Contact_Participation__c contactParticipationRecord = new Contact_Participation__c( RecordType = recordTypeRecord,
                                                                                           Contact__c = contactId,
                                                                                           ICEF_Event__c = icefEventId,
                                                                                           Workshop_CC_Mailers__c = 'tsetcc@gmail.com',
                                                                                           Catalogue_Position__c = 1,
                                                                                           Participation_Status__c = 'registered',
                                                                                           Account_Participation__c = accountParticipationId,
                                                                                           Preferred_WS_Email__c ='test@gmail.com'
                                                                                          );
        insert contactParticipationRecord;
        return contactParticipationRecord.Id;
        
    }
    
    Public static String CreateContact(String accountId){
        
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Agent'
                                       AND SobjectType = 'Contact' ];
        
        Contact contactRecord = new Contact( RecordType = recordTypeRecord,
                                            Salutation = 'Mr',
                                            FirstName = 'FirstName',
                                            LastName = 'LastName',
                                            Gender__c = 'male',
                                            AccountId = accountId,
                                            Role__c = 'Primery Decision Maker',
                                            Contact_Status__c = 'Active',
                                            Email ='test@gmail.com'
                                           );
        insert contactRecord;
        return contactRecord.Id;
    }
    
    Public static String createAccountParticipation(String accountId,String icefEventId,String countryId,String contactId ){
        
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Agent'
                                       AND SobjectType = 'Account_Participation__c' ];
        
        List<Account_Participation__c> accPartRecords = new List<Account_Participation__c>();                                  
        accPartRecords.add( new Account_Participation__c( RecordType = recordTypeRecord,
                                                         Account__c = accountId,
                                                         ICEF_Event__c = icefEventId,
                                                         Country_Section__c = countryId,
                                                         Participation_Status__c = 'accepted',
                                                         Catalogue_Country__c = countryId,
                                                         Primary_Recruitment_Country__c = countryId,
                                                         Organisational_Contact__c = contactId
                                                        ));
        
        accPartRecords.add( new Account_Participation__c( RecordType = recordTypeRecord,
                                                         Account__c = accountId,
                                                         ICEF_Event__c = icefEventId,
                                                         Country_Section__c = countryId,
                                                         Participation_Status__c = 'accepted',
                                                         Catalogue_Country__c = countryId,
                                                         Primary_Recruitment_Country__c = countryId,
                                                         Organisational_Contact__c = contactId
                                                        ));
        
        insert accPartRecords;
        return accPartRecords[0].Id;
    }
    public static String createAccount(String countryId){
        User userRecord = [SELECT ID 
                           FROM User
                           WHERE Name = 'Liz Adams'];
        
        
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Agent'
                                       AND SobjectType = 'Account' ];
        
        Account accountRecord = new Account( RecordType = recordTypeRecord,
                                            Name = 'Test',
                                            CurrencyIsoCode = 'EUR',
                                            Educational_Sector__c = '5 MULTI-SECTORAL',
                                            Status__c = 'Active',
                                            Building_Country__c = countryId,
                                            Mailing_Country__c = countryId
                                            //ARM__c = userRecord.Id
                                            //c2g__CODAAccountsReceivableControl__c ='test',
                                           // c2g__CODAAccountTradingCurrency__c ='test'
                                           );
        
        insert accountRecord;
        return accountRecord.Id;
    }
    
    public static String createCountry(){
        User userRecord = [SELECT ID 
                           FROM User
                           WHERE Name = 'Liz Adams'];
        
        Country__c countryRecord = new Country__c ( Name='TestC' ,
                                                   Region__c = 'South & Central America',
                                                   Sales_Territory__c = 'Americas',
                                                   Agent_Relationship_Manager__c = userRecord.Id,
                                                   countries_from_to_in__c = True,
                                                   Sales_Territory_Manager__c = userRecord.Id
                                                  );
        insert countryRecord;
        return countryRecord.Id;
    }
    
    public static String createICEFEvent(String countryId){
        
        User userRecord = [SELECT ID 
                           FROM User
                           WHERE Name = 'Liz Adams'];
        
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Global Workshop'
                                       AND SobjectType = 'ICEF_Event__c' ];
        
        ICEF_Event__c recordOfICEFEvent = new ICEF_Event__c( RecordType = recordTypeRecord,
                                                            Name = 'TestEvent',
                                                            Event_City__c = 'TestCity',
                                                            Event_Country__c = countryId,
                                                            technical_event_name__c = 'Technical',
                                                            catalogue_format__c = 'A4',
                                                            catalogue_spelling__c = 'British English',
                                                            Event_Manager_Educators__c = userRecord.Id,
                                                            Event_Manager_Agents__c = userRecord.Id
                                                           );
        
        insert recordOfICEFEvent;                                                   
        return recordOfICEFEvent.Id;
    }
}