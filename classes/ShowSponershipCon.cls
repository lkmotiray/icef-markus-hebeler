/*
    @Purpose : Get details of "ICEF_Event_Sponsorship" record.
    @Created Date : 19/07/2017
*/
public class ShowSponershipCon {
    
    public Id ICEF_EventId {get; set;}
    public Integer goldSize {get; set;}
    public Integer platinumSize {get; set;}
    public Integer silverSize {get; set;}
    public Boolean showTopBorder {get; set;}
    
    // Get all records of ICEF_Event_Sponsorship__c related to ICEF_Event_c
    public Map<String,List<ICEF_Event_Sponsorship__c>> ICEF_EventSponsorshipMap {   
        get{
            List<ICEF_Event_Sponsorship__c> ICEF_Event_SponsorshipList= new List<ICEF_Event_Sponsorship__c>();
            
            if(String.isNotBlank(ICEF_EventId)){
                try{
                    ICEF_Event_SponsorshipList= [SELECT Id,Name,Logo__c,Position__c,Sponsorship_Package__c,Logo_URL__c,Width__c,Website_URL__c 
                                                 FROM ICEF_Event_Sponsorship__c 
                                                 WHERE ICEF_Event__c =: ICEF_EventId AND Ready_for_use__c = True
                                                 ORDER BY Sponsorship_Package__c, Position__c, Name];
                }catch(Exception ex){
                    System.debug('Exception in fetching ICEF_Event_Sponsorship__c records: ' + ex.getMessage());
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Exception in fetching ICEF_Event_Sponsorship__c records: ' + ex.getMessage()));
                }
            }
            
            ICEF_EventSponsorshipMap = new Map<String,List<ICEF_Event_Sponsorship__c>>();
            
            ICEF_EventSponsorshipMap.put('Platinum', new List<ICEF_Event_Sponsorship__c>());
            ICEF_EventSponsorshipMap.put('Gold', new List<ICEF_Event_Sponsorship__c>());
            ICEF_EventSponsorshipMap.put('Silver', new List<ICEF_Event_Sponsorship__c>());
            
            for(ICEF_Event_Sponsorship__c ICEF_EventSponsorshipRec : ICEF_Event_SponsorshipList){
                if(ICEF_EventSponsorshipMap.containsKey(ICEF_EventSponsorshipRec.Sponsorship_Package__c)){
                    ICEF_EventSponsorshipMap.get(ICEF_EventSponsorshipRec.Sponsorship_Package__c).add(ICEF_EventSponsorshipRec);
                }
            }
            
            goldSize = ICEF_EventSponsorshipMap.get('Gold').size();
            platinumSize = ICEF_EventSponsorshipMap.get('Platinum').size();
            silverSize = ICEF_EventSponsorshipMap.get('Silver').size();
            
            //System.debug('goldSize' + goldSize);
            //System.debug('platinumSize' + platinumSize);
            //System.debug('silverSize' + silverSize);
            
            return ICEF_EventSponsorshipMap;
        }
        set;
    }
    
     // Get all records of ICEF_Event_Sponsorship__c related to ICEF_Event_c
    public String topBorder {   
        get{
            if(showTopBorder)
                 return topBorder = 'border-top:2px solid  #00519e;';
              else
                return topBorder = 'border-top: 0 none;';
        }
        set;
    }
}