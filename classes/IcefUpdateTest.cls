@isTest
private class IcefUpdateTest
{
    static testMethod void testAccountInvoiceInfo()
    {
        Country__c con = new Country__c(Name='USA',Agent_Relationship_Manager__c = Userinfo.getUserId());
        insert con;
        Account a = new Account(ARM__c = Userinfo.getUserId(),Name='Testing acc', Status__c='Active', Mailing_State_Province__c='CA', Mailing_Country__c=con.Id, account_owner_email__c='aaa.bbb@gmail.com', Email__c='aaa.bbb@gmail.com');
        insert a;
        
        Contact c = new Contact();
        c.AccountId = a.Id;
        c.Gender__c = 'male';
        c.FirstName = 'First';
        c.LastName = 'Last';
        c.Salutation = 'Mr';
        
        insert c;
        
        Date d = date.newInstance(1978, 4, 8);      
        Opportunity op = new Opportunity(name='Test Apex Class', AccountId=a.Id, CloseDate=d, Invoicing_contact__c=c.id, StageName='Closed Won');
        
        try
        {
            Test.startTest();
            insert op;
            Test.stopTest();
        }
        catch(DMLException e)
        {
            system.assert(false, 'An Error occurred inserting the test record:');
        }
    }
    
    static testMethod void testAgencyCountryTarget()
    {
        Country__c con = new Country__c(Name='USA',Agent_Relationship_Manager__c = Userinfo.getUserId());
        insert con;
        Account a = new Account(ARM__c = Userinfo.getUserId(),Name='Testing acc', Status__c='Active', Mailing_State_Province__c='CA', Mailing_Country__c=con.Id, account_owner_email__c='aaa.bbb@gmail.com', Email__c='aaa.bbb@gmail.com');
        insert a;
        
        Contact c = new Contact();
        c.AccountId = a.Id;
        c.Gender__c = 'male';
        c.FirstName = 'First';
        c.LastName = 'Last';
        c.Salutation = 'Mr';
        insert c;
        
        Date d = date.newInstance(1978, 4, 8);
        
        Opportunity op = new Opportunity(name='Test Apex Class', AccountId=a.Id, CloseDate=d, Invoicing_contact__c=c.id, StageName='Closed Won');
        
        try
        {
            Test.startTest();
            insert op;
            Test.stopTest();
        }
        catch(DMLException e)
        {
            system.assert(false, 'An Error occurred inserting the test record:');
        }
    }
    
    
    // @todo update actions
    // @todo bulk actions
}