/*
    @Purpose : Get details of "Accommodation" and "ICEF_Event_Sponsorship" record.
    @Created Date : 24/01/2018
*/
public class SponershipDetailsWithUserSignCon {
    
    public Id accommodationId{get; set; }
    public Id ICEF_EventId {get; set; }
    public String hotelAddress{get; set;}
    
    public Accommodation__c accommodationRec{   
        get{
            System.debug('Get accommodation record ');
            accommodationRec= new Accommodation__c();
            
            if(String.isNotBlank(accommodationId)){
                try{
                    
                    accommodationRec= [SELECT Id,Hotel_Room__r.Hotel_Capacity__r.ICEF_Event__r.Id,Salutatory_Address_standard__c,Room_Type_HR__c,
                                              Hotel_Room__r.Hotel_Capacity__r.ICEF_Event__r.Skyline_Header__c,Number_of_Nights_HR__c,
                                              Persons_attending_HR__c,Hotel_Extras_HR__c,Test__c, Hotel_Room__r.Hotel_Capacity__r.ICEF_Event__r.Skyline_Header_URL__c,
                                              Hotel_Room__r.Hotel_Capacity__r.ICEF_Event__r.Date_Location_Header__c,Early_Departure_Fee__c,Booking_Deadline_HC__c,
                                              Hotel_Room__r.Hotel_Capacity__r.ICEF_Event__r.Event_Full_Name__c,Textmodule_Night_Nights__c,Arrival_Date_Room_HR_text__c, Arrival_Date_Room_HR__c,
                                              Departure_Date_Room_HR__c, Departure_Date_Room_HR_text__c,Hotel_Email__c,WS_Venue_Transport_Text_HC__c,Hotel_Website__c,Hotel_Address__c,Hotel_Name__c,
                                              Check_in_Time_HC__c,Check_out_Time_HC__c,Textmodule_breakfast_included__c,Textmodule_Extra_Costs__c,Textmodule_Security_Deposit__c,Booking_Form_Text_Complete_HC__c,
                                              Visa_Support_Text_HC__c,Cancellation_Fee_HC__c,ICEF_Event_Full_Name_HC__c,Event_Manager_Agents_Phone_HC__c,Event_Manager_Agents_Email_HC__c,
                                              Event_Manager_Agents_Signature__c,Event_Manager_Agents_HC_Name__c,Textmodule_LAW_no_accommodation__c,Textmodule_ICEF_sponsors_accommodation__c
                                       FROM Accommodation__c 
                                       WHERE Id=: accommodationId];
                }catch(Exception ex){
                    System.debug('Exception in fetching Accommodation: ' + ex.getMessage());
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Exception in fetching Accommodation: ' + ex.getMessage()));
                }
            }
            if(accommodationRec != NULL 
               && accommodationRec.Hotel_Room__r != null 
               && accommodationRec.Hotel_Room__r.Hotel_Capacity__r != null 
               && accommodationRec.Hotel_Room__r.Hotel_Capacity__r.ICEF_Event__r != null){
                if(accommodationRec.Hotel_Room__r.Hotel_Capacity__r.ICEF_Event__r.Id != null){
                    ICEF_EventId = accommodationRec.Hotel_Room__r.Hotel_Capacity__r.ICEF_Event__r.Id;
                }  
                hotelAddress = accommodationRec.Hotel_Address__c.stripHtmlTags();            
            }
            return accommodationRec;
        }
        set;
    }  
    
    public SponershipDetailsWithUserSignCon() { 
        
        accommodationRec = new Accommodation__c();
    }
   
}