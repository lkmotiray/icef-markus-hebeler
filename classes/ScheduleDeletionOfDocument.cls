/**
  * @ Author      : Prajakta
  * @ Description : Scheduler to Delete Documents in CustomEmailTempAttachment folder
  * @ Created date: 11 May 2016
 */
global class ScheduleDeletionOfDocument implements Schedulable 
{ 
    global void execute(SchedulableContext sc) 
    {
        Folder folder = [ SELECT Id FROM Folder WHERE Name =: 'CustomEmailTempAttachment'];
        if( folder != null && !String.isEmpty(folder.id) )
        {
            List<Document> lstDocumentToDelete  = [ SELECT Id
                                                    FROM Document 
                                                    WHERE folderId = :folder.Id ];
                                     
            if( lstDocumentToDelete != null && !(lstDocumentToDelete.isEmpty())) 
            {
                Database.delete(lstDocumentToDelete);     
            }                         
        }      
    }
}