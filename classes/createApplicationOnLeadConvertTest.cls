@isTest
public class createApplicationOnLeadConvertTest {
    
    Static testMethod void validateLZU(){
        
        lead newLead = new lead(firstName = 'Cole', lastName = 'Swain', company = 'BlueWave', 
                                status = 'contacted');
        newLead.FirstName = 'Cole';
        newLead.LastName = 'Swain';
        newLead.Company = 'BlueWave';
        newLead.Status = 'contacted';
        newLead.Events_interested_in__c = 'Dubai';
        insert newLead;
        
        test.startTest();
        database.leadConvert lc = new database.leadConvert();
        lc.setLeadId(newLead.id);
        
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        test.stopTest();
    }
}