/******************************************************************************************
Developer   = Dreamwares (India)
Description = check if the value of TP Owner field is updated properly on insert of Training
              participation.
Date        = 16-01-2015 
******************************************************************************************/

@isTest
public class TrainingParticipationTriggerHandlerTest
{
    public static testmethod void TPOwnerTest()
    {
        createTestData();
        
        List<Training_Participation__c> testLstTrainingPart = new List<Training_Participation__c>();
        testLstTrainingPart = [SELECT TP_Owner__c FROM Training_Participation__c];
        
        for(Training_Participation__c testTP :testLstTrainingPart)
        {
            System.assertEquals(testTP.TP_Owner__c, UserInfo.getUserId());
        }
    }
    
    public static void createTestData()
    {
        //create Account record
        System.debug('User Id = ' + UserInfo.getUserId());
        Country__c con = new Country__c(Name='USA',Agent_Relationship_Manager__c = Userinfo.getUserId());
        insert con;
        Account a = new Account(ARM__c = Userinfo.getUserId(),Name='Testing acc', Status__c='Active', Mailing_State_Province__c='CA', Mailing_Country__c=con.Id, account_owner_email__c='aaa.bbb@gmail.com', Email__c='aaa.bbb@gmail.com');
        insert a;
        
        Contact c = new Contact();
        c.AccountId = a.Id;
        c.Gender__c = 'male';
        c.FirstName = 'First';
        c.LastName = 'Last';
        c.Salutation = 'Mr';
        
        insert c;
        
        Training_Type__c testTrainingType = new Training_Type__c(Certificate_Name__c = 'Testing');
        insert testTrainingType;
        
        Integer intLoopVar = 0;
        
        List<Training_Participation__c> lstTestRecords = new List<Training_Participation__c>();
        
        for(intLoopVar = 0; intLoopVar < 200; intLoopVar++)
        {
            Training_Participation__c testTrainingPart = new Training_Participation__c(Training_Type__c = testTrainingType.Id, 
                                                                                       Account__c = a.Id,
                                                                                       Contact__c = c.Id);
            lstTestRecords.add(testTrainingPart);
        }
        
        insert lstTestRecords;
    }
}