/* schedulable Class to update Magic link in Contact on a regular basis. 
 * calls BatchClass MagicLinkBatch to be able to update many records.
 */
global class MagicLinkSchedule implements Schedulable {
	
    global void execute(SchedulableContext ctx){
			
     	MagicLinkBatch myBatchObject = new MagicLinkBatch();
        Id batchId = Database.executeBatch(myBatchObject, 25);
    }
    
}