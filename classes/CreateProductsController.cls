/*
 *	@Purpose      : Controller for 'CreateProducts' page.
 *	@Created Date : 12-04-2018
 */
global class CreateProductsController {
    global static string ICFE_EventId{get;set;}
    global static string accountId{get;set;}
      
    public CreateProductsController(ApexPages.StandardController stdController){
        System.debug('in constructor');
        ICEF_Event__c ICEF_eventObj;
        
        if(!Test.isRunningTest()){
            stdController.addFields(new string[]{'Venue_Hotel__c'});
            ICEF_eventObj = (ICEF_Event__c)stdController.getRecord();
        }
        
        if(Test.isRunningTest()){
            ICEF_eventObj = [SELECT Id, Venue_Hotel__c FROM ICEF_Event__c LIMIT 1];
        }
        
        ICFE_EventId = ICEF_eventObj.Id;
        accountId = ICEF_eventObj.Venue_Hotel__c;
        system.debug('In constructor :::'+'Event Id'+ICFE_EventId+'AccountId'+accountId);
    }
    
   /*
    *	@Purpose: Get all Product records having following ProductFamily
	*			    a.) Workshop Catalogue/Guide
    *               b.) Workshop Receptions
    *               c.) Workshop Merchandising
    *               d.) Workshop Bag Inserts/Tags
    *               e.) Workshop Lounges/Hub
    *               f.) Workshop Sponsorship 
    *	@Return : ProductList [List]
    */
    @RemoteAction
    public static List<Product2> getProducts() {
        
        List<Product2> productList = new List<Product2>();
        
        List<String> productFamilyList = new List<String>{'Workshop Catalogue/Guide',
                                                          'Workshop Receptions',
                                                          'Workshop Merchandising',
                                                          'Workshop Bag Inserts/Tags',
                                                          'Workshop Lounges/Hub',
                                                          'Workshop Sponsorship',
                                                          'Workshop Presentations',
                                                          'Workshop Display Advertising'};
            
            try{
                productList = [SELECT Id, Name, Family 
                               FROM Product2
                               WHERE Family IN : productFamilyList AND IsActive = True];
            }catch(Exception ex){
                System.debug('Error occured while fetching Product records' + ex.getMessage());
            }
        
        return productList;
    }
    
    /*
     *  @Purpose    : Create 'Product Participations' records. 
     *  @Parameters : productList [List] , String ICFE_EventId, String accountId
     */
    @RemoteAction
    public static Boolean createPPRecords(String productRecords, String ICFE_EventId, String accountId) { 
        system.debug('STRING>>>'+productRecords);
        String response = 'Error occured while creating records, Please contact System Admin';  
        Boolean isError = false;
        Boolean isAccountValid = false;
            
        if( String.isNotBlank(productRecords) && String.isNotBlank(ICFE_EventId) ){
            List<Product_Participation__c> productParticipationList = new List<Product_Participation__c>();
            System.debug('productRecords' + productRecords);
            
            // Parse "productRecords" string by using "ProductWrapper" class. 
            List<ProductWrapper> productList = (List<ProductWrapper>) System.JSON.deserialize(productRecords, List<ProductWrapper>.class);
            System.debug('productList' + productList);    
            
            // Check if the given accountId is valid or not
            if( String.isNotBlank(accountId) ){
                isAccountValid = validateAccount(accountId);
            }
            
            // Create "ProductParticipation" records
            for(ProductWrapper product : productList){            
                for( Integer index = 1; index <= product.count ; index++){
                    if( isAccountValid ){
                        productParticipationList.add(new Product_Participation__c(ICEF_Event__c= ICFE_EventId,
                                                                                  Product_status__c = 'available',
                                                                                  /*Account__c = accountId, 
                                                                                   //Commented Date : 6/7/2018,
                                                                                    Commented By = Kanchan P*/
                                                                                  Product__c = product.Id ));
                    }else{
                        productParticipationList.add(new Product_Participation__c(ICEF_Event__c= ICFE_EventId,
                                                                                  Product_status__c = 'available',                                                                             
                                                                                  Product__c = product.Id ));
                    }                    
                }                               
            }
            
            if( !productParticipationList.isEmpty() ){
                try{
                    INSERT productParticipationList;
                    response = 'Records created successfully';
                    isError = true;
                    sendSuccessMail(productList,ICFE_EventId);
                }catch(Exception ex){
                    System.debug('Error occured while inserting productParticipation records' + ex.getMessage());
                }
            }       
         }
        
        return isError;
    }     
    
    /*
     *  @Purpose    : Check whether Account(Id) is valid.
     *  @Parameters : AccountId
     */
    @RemoteAction
    public static Boolean validateAccount( String accountId ) {
        if(String.isNotBlank(accountId)){
            try{
                List<Account> accounts = [SELECT Id FROM Account WHERE Id =: accountId];
                if(accounts.size() > 0){
                    return true;
                }
            }catch(DMLException ex){
                System.debug('Following Error has been occurred while fetching account'+ ex);
                return false;
            }
        }
        return false;
    }
    
    /*
     *  @Purpose    : 
     *  @Parameters : AccountId
     */
    @RemoteAction
    public static String getEventId() {
        Map<String, Id> eventIdMap = new Map<String, Id>();
        eventIdMap.put('ICFE_EventId', ICFE_EventId);
        eventIdMap.put('accountId', accountId);
        String json = JSON.serialize(eventIdMap);
        system.debug('++++'+json);
        return json;
    }
    
    /*
     *  @Purpose : Send Email on Successfull creation of ProductParticipation  
     *  @Param   : a)productList
                   b)ICFE_EventId
     */
    private Static void sendSuccessMail(List<ProductWrapper> productList, String ICFE_EventId){
        
        if(productList != null){
            ICEF_Event__c icefEvent = [SELECT Name FROM ICEF_Event__c WHERE id =: ICFE_EventId];   
            
            List<Email_Recipient__c> emailRecipientList = Email_Recipient__c.getall().values();
            
            List<String> emailIdList = new List<String>();
            for(Email_Recipient__c emailRecipient :emailRecipientList){
                emailIdList.add(emailRecipient.email__c);    
            }
            
            if(emailIdList != null){
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                
                email.setSubject('Product Participations Creation Notification for ICEF Event: '+icefEvent.Name);
                email.setToAddresses(emailIdList);
                email.setHtmlBody(getTableEmailBody(productList,icefEvent.Name));
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});     
            } 
        }
    }
    
    /*
     *  @Purpose : Create Table Email Body  
     *  @Param   : a)productList
                   b)eventName
     */
    public static string getTableEmailBody(List<ProductWrapper> productList, String eventName){
        
        String htmlBody = '<p>Product Participations for '+eventName+' have been created in bulk. The following products have been created:</p>';
        
        if(productList != null){
            //open table
            htmlBody += '<table border="1" style="border-collapse: collapse"><tr><th><b>Product Name</b></th><th><b>Count</b></th></tr>';
            
            //iterate over list and output data into table rows
            for(ProductWrapper product :productList){
                
                htmlBody += '<tr><td>' + product.Name + '</td><td align="right">' + product.count + '</td></tr>';
                
            }
            
            //close table
            htmlBody += '</table>';    
        }        
        return htmlBody;        
    }
}