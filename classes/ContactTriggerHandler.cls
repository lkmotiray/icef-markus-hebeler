/**************
 * purpose: update Contact on change in Contact 
 * - set Password__c if null
 * - set API token if null
 * - set magic link (every time) 
 * **************/

public class ContactTriggerHandler{
    
    private static Long timeStamp;
    private static String randomValue, secret = 'ejesheitohn8eeV7yu7j', openid, combinedString, sha1Coded;
    //List<String> values;
    
    public static void isBeforeInsert(List<Contact> contactList ){
        updateContact(contactList);
//        System.enqueueJob(new MagicLinkUpdateQ(contactList));
    }
    public static void isBeforeUpdate(List<Contact> contactList ){
        updateContact(contactList);
//        System.enqueueJob(new MagicLinkUpdateQ(contactList));
    }
    
    public static void updateContact(List<Contact> contactList){
        for(contact contact : contactList ){
            updateContactPassword(contact);
            updateContactApiToken(contact);
            setMagicLinkValue(contact);
        } 
    }
    
    //updateContactPassword
    public static void updateContactPassword(contact c ){
        if (c.Password__c==null){
            Datetime saltDate = Datetime.now();
            String apiString = String.valueOfGmt(saltDate) + String.valueOf(c.id);
            Blob apiBlob = Crypto.generateDigest('SHA-256', Blob.valueOf(apiString));
            
            c.Password__c = EncodingUtil.convertToHex(apiBlob).substring(0, 8);
            c.password_update__c = saltDate;
        }
    }
    //updateContactApiToken
    public static void updateContactApiToken(contact c){
        if (c.Api_Token__c==null)
        {
            Datetime saltDate = Datetime.now();
            String apiString = String.valueOfGmt(saltDate) + String.valueOf(c.id);
            Blob apiBlob = Crypto.generateDigest('SHA-256', Blob.valueOf(apiString));
            
            c.Api_Token__c = EncodingUtil.convertToHex(apiBlob);  
        }
    }
    
    
    /*****************************************************************
    
    Purpose of trigger:
    
    Each time the contact is created or updated the field 'magic_link' in Contacts. The value of this field should be as follows:
     
    1 = contact.email
    2 = unix time stamp
    3 = random value with 10 letters
    4 = secret is a given string
    5 = SHA1 hash out of [1, 2, 4, 3].join('')
    
    ******************************************************************/
    //setMagicLinkValue
    public static void setMagicLinkValue(Contact cont){
        //values = new List<String>();
      
        timeStamp = System.Now().getTime()/1000;
        
        
        randomValue = EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(0,10);
        openid = cont.email;
        /*values.add(openid);
        values.add(String.valueof(timeStamp));
        values.add(secret);
        values.add(randomValue);*/
        
        combinedString = openid + timeStamp + secret + randomValue;
        
        sha1Coded = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', Blob.valueOf(combinedString)));
        
        cont.magic_link__c = '/login/magic?openid=' + cont.email +'&time_stamp='+ String.ValueOf(timeStamp) +'&salt='+ randomValue +'&hash='+ sha1Coded; 
        cont.magic_link_updated__c = DateTime.now();
        }
    

}