/*
 * @Purpose: Test class for StockOverviewLinkVFController
 * @Date: 26-6-2018
 * @Author: (Dreamwares)
 */
@isTest
public class StockOverviewLinkControllerTest {
   
   /*
    * @Purpose: Create data required for testing
    */
    @testSetup
    public static void testData(){
         // create country
        Country__c countryRecord = new Country__c ( Name='TestCountry');
        insert countryRecord;
                  
        // Create User
        User testUser = new User(Alias = 'standt', 
                                 Email='testuser@testorg.com', 
                                 EmailEncodingKey='UTF-8', 
                                 LastName='Testing', LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US', 
                                 ProfileId = '00e200000019GqS', 
                                 TimeZoneSidKey='America/Los_Angeles', 
                                 UserName='testuser@fgh.com');
        System.runAs(testUser) {} 
                                  
        // create ICEF Event               
        ICEF_Event__c recordOfICEFEvent = new ICEF_Event__c(Name = 'TestEvent',
                                                            Event_City__c = 'TestCity',
                                                            Event_Country__c = countryRecord.Id,
                                                            technical_event_name__c = 'Technical',
                                                            catalogue_format__c = 'A4',
                                                            catalogue_spelling__c = 'British English',
                                                            Event_Manager_Educators__c = testUser.Id,
                                                            Event_Manager_Agents__c = testUser.Id);
                                                           
        insert recordOfICEFEvent;  
    }
    
   /*
    * @Purpose :  Test method to test functionality
    */
    @isTest
    public static void functionalityTest(){
        
        ICEF_Event__c event = [SELECT id FROM ICEF_Event__c];
        
        PageReference pageRef = Page.StockOverview;
        pageRef.getparameters().put('id', event.id);  
        Test.setCurrentPage(pageRef);
        
        StockOverviewLinkVFController stockOverviewLinkVFController = new StockOverviewLinkVFController();
        PageReference pageReference = stockOverviewLinkVFController.redirect();
        
        System.assertNotEquals(null, pageReference);
    }
}