/*  Scheduler Class
@purpose-This scheduler class is developed to schedule an update for the exchange rates on regular interval 
@created_date-09-08-13
@last_modified-[12-08-13]    
*/

global class scheduleSetCurrentCompany implements Schedulable {
    //implemented this execute() in order to implement the schedulable interface 
    global void execute(SchedulableContext sc)
    {
        try {
            Current_Company_For_Organization__c currentCompany = Current_Company_For_Organization__c.getInstance(UserInfo.getProfileId());
            if(currentCompany!= null && !String.isBlank(currentCompany.Company_Name__c)){
                Boolean isSame = confirmCurrentCompany(currentCompany.Company_Name__c);
                if(!isSame){
                    deleteExistingGroupMembers();
                    changeCurrentCompany(currentCompany.Company_Name__c);
                }            
            }
            else {
                System.debug('Please Enter value in Custom Setting : Current Company For Organization.');
            }
        }
        catch(Exception e){
            System.debug('Exception caught::'+e.getMessage());
        }
    }
    
    /*  Function
    @purpose-This function is used to confirm whether current company is set to as expected        
    @last_modified-[27-04-15]
    */
    public boolean confirmCurrentCompany(String companyName) {
        List<GroupMember> existingGroupMembers = new List<GroupMember>();
        
        existingGroupMembers = [SELECT id, UserOrGroupId, GroupId 
                                FROM GroupMember 
                                WHERE Group.DeveloperName LIKE : '%'+companyName+'%'
                                AND UserOrGroupId =: UserInfo.getUserId()];
        if(existingGroupMembers.size()>0){
            return true;
        }
        
        return false;
    }
    
    /*  Function
    @purpose-This function is used to change the current company as required        
    @last_modified-[27-04-15]
    */
    public void changeCurrentCompany(String companyName) {
        //Check group for Current Company
        System.debug('Checking Group for Current Company');
        Group currentGroup;
        List<Group> existingGroups = new List<Group>();
        existingGroups = [SELECT id, name, DeveloperName, type 
                          FROM Group 
                          WHERE Name Like : '%'+companyName+'%'];
        if(existingGroups.size()>0){
            currentGroup = existingGroups[0];		
        }
        else {
            currentGroup = new Group(DeveloperName = 'FF'+companyName.replaceAll( '\\s+', ''), Name = companyName, Type = 'Queue');
            insert currentGroup;
        }
        
        System.debug('Creating new group member for current company');
        GroupMember newGroupMmber = new GroupMember(GroupId = currentGroup.Id, UserOrGroupId = UserInfo.getUserId());
        insert newGroupMmber;              
    }
    
    /*  Function
    @purpose-This function is used to delete all the other group members       
    @last_modified-[27-04-15]
    */
    public void deleteExistingGroupMembers() {
        //deleting group member of other groups/companies
        List<GroupMember> otherGroupMembers = new List<GroupMember>();
        otherGroupMembers = [SELECT id, UserOrGroupId, GroupId 
                             FROM GroupMember 
                             WHERE Group.DeveloperName Like '%ICEF%' 
                             AND UserOrGroupId =: UserInfo.getUserId()];
        if(otherGroupMembers.size() > 0) {
            delete otherGroupMembers;
        }
    }
}