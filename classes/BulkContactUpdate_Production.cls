global class BulkContactUpdate_Production implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id, AccountId, Role__c, X20_year_mag__c, Contact_Status__c FROM Contact');
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        System.debug('scope:::'+scope);
        //Creating a set of account ids
        Set<ID> setAccountIds = new Set<ID>();
        
        //Creating a set of pmd account ids
        Set<ID> setPMDAccountIds = new Set<ID>();
        
        //Creating a set of both the above sets for querying purpose
        Set<ID> setMergedAccountIds = new Set<ID>();
        
        //in case of update, check if checkbox value is changed from ticked to unticked and vice versa, and add it to the set
        Set<ID> setCheckAccountIds = new Set<ID>();
        
        for(sObject sObjectRec : scope) {
            Contact contactRecord = (Contact)sObjectRec;
            
            // Access the "old" record by its ID in Trigger.oldMap
            //Contact oldContact = triggerOldContactMap.get(contactRecord.Id);
            
            // Check that the field was changed to another value
            if (contactRecord.X20_year_mag__c) { // ticked
                //tick account's field
                setAccountIds.add(contactRecord.AccountId);
            }
            else if(!contactRecord.X20_year_mag__c) { // unticked
                //add account ids to the set which have to be checked
                setCheckAccountIds.add(contactRecord.AccountId);
            }
            
            if(contactRecord.Role__c == 'Primary Decision Maker') {
                setPMDAccountIds.add(contactRecord.AccountId);
            }
        }
        System.debug('setAccountIds:::'+setAccountIds.size());
        System.debug('setPMDAccountIds:::'+setPMDAccountIds.size());
        System.debug('setCheckAccountIds:::'+setCheckAccountIds.size());
        
        if(setAccountIds.size() > 0)
            setMergedAccountIds.addAll(setAccountIds);
        
        if(setPMDAccountIds.size() > 0)
            setMergedAccountIds.addAll(setPMDAccountIds);
        
        if(setCheckAccountIds.size() > 0)
            setMergedAccountIds.addAll(setCheckAccountIds);
        
        //If set is not empty then do updations
        if(setMergedAccountIds.size() > 0) {
            //Stores the count of PMD contacts for a particular account
            Integer countPMDContacts;
            
            //Creates a map of account id along with its records so as to avoid duplicacies
            Map <Id, Account> mapUpdateAccounts = new Map <Id, Account>();
            Account accRec;
            
            for(Account accountRecord: [SELECT Id, Number_of_PMD_Contacts__c, X20_year_mag__c, 
                                        (SELECT Contact.Id, Contact.Role__c, Contact.X20_year_mag__c  
                                         FROM Account.Contacts) 
                                        FROM Account
                                        WHERE Id IN :setMergedAccountIds LIMIT 50000]){
                                            
                                            if(setPMDAccountIds.contains(accountRecord.Id)) {
                                                //Count pmd contacts
                                                
                                                countPMDContacts = 0;
                                                //Get the list of all contacts related to current account object
                                                for(Contact contactRecord : accountRecord.Contacts)
                                                {
                                                    if(contactRecord.Role__c == 'Primary Decision Maker')
                                                        countPMDContacts++;
                                                }
                                                System.debug('countPMDContacts : '+countPMDContacts);
                                                accountRecord.Number_of_PMD_Contacts__c = countPMDContacts;
                                                mapUpdateAccounts.put(accountRecord.Id, accountRecord); 
                                            }
                                            
                                            if(setAccountIds.contains(accountRecord.Id)){
                                                System.debug('set 20 yr mag to true');
                                                //Set X20_year_mag__c to true
                                                if(mapUpdateAccounts.containsKey(accountRecord.Id)){
                                                    accRec = mapUpdateAccounts.get(accountRecord.Id);
                                                    accRec.X20_year_mag__c = true;
                                                }
                                                else {
                                                    accountRecord.X20_year_mag__c = true;
                                                    mapUpdateAccounts.put(accountRecord.Id, accountRecord); 
                                                } 
                                            }
                                            
                                            if(setCheckAccountIds.contains(accountRecord.Id)){
                                                
                                                Boolean flag = false;
                                                for(Contact contactRecord : accountRecord.Contacts){       
                                                    //if any 1 is ticked then don't do anything
                                                    if(contactRecord.X20_year_mag__c == true){
                                                        flag = true;
                                                        break;
                                                    }
                                                }
                                                if(flag != true){
                                                    //if no ticked rec found then make that acc checkbox unticked
                                                    System.debug('set 20 yr mag to false');
                                                    if(mapUpdateAccounts.containsKey(accountRecord.Id)){
                                                        accRec = mapUpdateAccounts.get(accountRecord.Id);
                                                        accRec.X20_year_mag__c = false;
                                                    }
                                                    else {
                                                        accountRecord.X20_year_mag__c = false;
                                                        mapUpdateAccounts.put(accountRecord.Id, accountRecord);  
                                                    } 
                                                }
                                            }   
                                        }
            
            System.debug('mapUpdateAccounts : '+mapUpdateAccounts);
            if(mapUpdateAccounts.size() > 0) {
                try{
                    update mapUpdateAccounts.values();
                }
                catch(Exception e) {                
                    System.debug('Message : '+e.getMessage());    
                }  
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}