@isTest
public class getApiTokenTest {
    
    static testmethod void testApiTokenSet(){
        Account acct = DataFactory.createTestAgents(1)[0];
        insert acct;
        
        List<Contact> cons = DataFactory.createTestContacts(acct, 20);
        
    	insert cons;
        
        List<Contact> consBeforeMethod = new List<Contact>();
        List<Id> conIds = new List<Id>();
        for (Contact con : cons){
            con.API_Token__c = '1';
            consBeforeMethod.add(con);
            conIds.add(con.Id);
        }
        update consBeforeMethod;
        Test.startTest();
        System.debug('API Token before method: ' + consBeforeMethod[0].API_Token__c);
        
        List<Id> updatedContactIds = getAPITokenAction.getApiToken(conIds);
        Test.stopTest();
        System.assertEquals(0, [SELECT Id FROM Contact WHERE API_Token__c = '1'].size());
    }
}