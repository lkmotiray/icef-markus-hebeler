//update my Icef User if Contact has relevant changes except email address change
public class MyIcefUserEmailUpdate {
	@invocableMethod(label='update MyIcef User Email' description = 'calls future Method to update MyIcefUser when email has changed')
    public static void updateMyIcefUserEmail(List<Id> contactIds){
        for(Id cId : contactIds){
        	AuthUser.updateUserEmail(cId);   
        }
    }
}