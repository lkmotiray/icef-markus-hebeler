/**
    @ Developer      :- DWS
    @ Purpose        :- test class for AccPartTriggerHndlrForHRAccommodation.
    @ Created By     :- DWS
    @ LastModifiy By :- DWS
*/

@isTest

public class AccPartTriggerHndlrHRAccommodationTest {

    @testSetup
    public static void testSetup(){
    
        
        // create country
        String countryId = createCountry();
        
        // create ICEF Event
        String icefEventId = createICEFEvent(countryId);
       
        // Create Account
        String accountId = createAccount(countryId);
       
        // create contact
        String contactId = CreateContact(accountId);
         
        // create Account participation
        String accountParticipationId = createAccountParticipation(accountId,icefEventId,countryId,contactId);
         
        // create Contact participation
        createContactParticipation(contactId,icefEventId,accountParticipationId);
        
        // create hotel
        String hotelId = createHotel(countryId);
        
        // create Hotel Capacity
        createHotelCapacity(icefEventId,hotelId);
    }
    
    public static testmethod void validateTriggerHandler(){
    
        //Country__c countryRecord = [SELECT Id, Name FROM Country__c LIMIT 1 ];
        List<Account_Participation__c> accountParticipations = [SELECT Id,Hotel_Capacity__c FROM Account_Participation__c];
        Hotel_Capacity__c  hotelCapacity = [SELECT Id FROM Hotel_Capacity__c LIMIT 1];
        accountParticipations[0].Hotel_Capacity__c = hotelCapacity.Id;
        accountParticipations[1].Hotel_Capacity__c = hotelCapacity.Id;
        Update accountParticipations;
        
        List<Accommodation__c> accommodations = [SELECT Id FROM Accommodation__c];
        
        system.assert(accommodations.size() > 0);
        
    }
    
    public static void createHotelCapacity(String icefEventId,String hotelId){
        
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Agent Contingent'
                                           AND SobjectType = 'Hotel_Capacity__c' ];
        
        Hotel_Capacity__c hotelCapacityRecord = new Hotel_Capacity__c( RecordType = recordTypeRecord,
                                                                       Name = 'TestCapacity',
                                                                      ICEF_Event__c = icefEventId,
                                                                      Hotel__c = hotelId); 
        insert hotelCapacityRecord;
        
    }
    
    public static void createContactParticipation( String contactId, 
                                                   String icefEventId,
                                                   String accountParticipationId 
                                                  ){
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Workshop Participant'
                                           AND SobjectType = 'Contact_Participation__c' ];
                                           
        Contact_Participation__c contactParticipationRecord = new Contact_Participation__c( RecordType = recordTypeRecord,
                                                                                            Contact__c = contactId,
                                                                                            ICEF_Event__c = icefEventId,
                                                                                            Catalogue_Position__c = 1,
                                                                                            Participation_Status__c = 'registered',
                                                                                            Account_Participation__c = accountParticipationId 
                                                                                          );
        insert contactParticipationRecord;
        
    }
    
    Public static String CreateContact(String accountId){
    
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Agent'
                                           AND SobjectType = 'Contact' ];
                                           
        Contact contactRecord = new Contact( RecordType = recordTypeRecord,
                                             Salutation = 'Mr',
                                             FirstName = 'FirstName',
                                             LastName = 'LastName',
                                             Gender__c = 'male',
                                             AccountId = accountId,
                                             Role__c = 'Primery Decision Maker',
                                             Contact_Status__c = 'Active'
                                            );
        insert contactRecord;
        return contactRecord.Id;
    }
    
    Public static String createAccountParticipation(String accountId,String icefEventId,String countryId,String contactId ){
    
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Agent'
                                           AND SobjectType = 'Account_Participation__c' ];
                                           
         List<Account_Participation__c> accPartRecords = new List<Account_Participation__c>();                                  
         accPartRecords.add( new Account_Participation__c( RecordType = recordTypeRecord,
                                                                               Account__c = accountId,
                                                                               ICEF_Event__c = icefEventId,
                                                                               Country_Section__c = countryId,
                                                                               Participation_Status__c = 'accepted',
                                                                               Catalogue_Country__c = countryId,
                                                                               Primary_Recruitment_Country__c = countryId,
                                                                               Organisational_Contact__c = contactId
                                                                               ));
        
        accPartRecords.add( new Account_Participation__c( RecordType = recordTypeRecord,
                                                                               Account__c = accountId,
                                                                               ICEF_Event__c = icefEventId,
                                                                               Country_Section__c = countryId,
                                                                               Participation_Status__c = 'accepted',
                                                                               Catalogue_Country__c = countryId,
                                                                               Primary_Recruitment_Country__c = countryId,
                                                                               Organisational_Contact__c = contactId
                                                                               ));
                                                                               
        insert accPartRecords;
        return accPartRecords[0].Id;
    }
    public static String createAccount(String countryId){
    
        User userRecord = [SELECT Id FROM User LIMIT 1];
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Educator'
                                           AND SobjectType = 'Account' ];
                                           
        Account accountRecord = new Account( RecordType = recordTypeRecord,
                                             Name = 'Test',
                                             CurrencyIsoCode = 'EUR',
                                             Educational_Sector__c = '5 MULTI-SECTORAL',
                                             Status__c = 'Active',
                                             Mailing_Country__c = countryId,
                                             ARM__c = userRecord.Id,
                                             Mailing_State_Province__c = 'AL'
                                            );
        
        insert accountRecord;
        return accountRecord.Id;
    }
    public static String createHotel(String countryId){
    
       // User userRecord = [SELECT Id FROM User LIMIT 1];
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Hotel'
                                           AND SobjectType = 'Account' ];
                                           
        Account accountRecord = new Account( RecordType = recordTypeRecord,
                                             Name = 'Test',
                                             //CurrencyIsoCode = 'EUR',
                                             //Educational_Sector__c = '5 MULTI-SECTORAL',
                                             Status__c = 'Active',
                                             Mailing_Country__c = countryId
                                             //ARM__c = userRecord.Id,
                                             //Mailing_State_Province__c = 'AL'
                                            );
        
        insert accountRecord;
        return accountRecord.Id;
    }
    public static String createCountry(){
        User userRecord = [SELECT ID FROM User LIMIT 1];
        Country__c countryRecord = new Country__c ( Name='TestC' ,
        Region__c = 'South & Central America',
        Sales_Territory__c = 'Americas',
        Agent_Relationship_Manager__c = userRecord.Id,
        countries_from_to_in__c = True,
        Sales_Territory_Manager__c = userRecord.Id,
        City_Post_Code_State_Order__c = 'City Post Code',
        Sales_Priority__c = '1',
        Sales_Priority_Agents__c = '1',
        Phone_Country_Code__c = '001',
        Languages_spoken__c = 'Dutch, English, Papiamento',
        ISO_Number__c = 999,
        ISO2__c = 'SX',
        ISO3__c = 'SXM'
        );
        insert countryRecord;
        return countryRecord.Id;
    }
    
    public static String createICEFEvent(String countryId){
        
        User userRecord = [SELECT ID FROM User LIMIT 1];
        
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Global Workshop'
                                           AND SobjectType = 'ICEF_Event__c' ];
                                           
        ICEF_Event__c recordOfICEFEvent = new ICEF_Event__c( RecordType = recordTypeRecord,
                                                             Name = 'TestEvent',
                                                             Event_City__c = 'TestCity',
                                                             Event_Country__c = countryId,
                                                             technical_event_name__c = 'Technical',
                                                             catalogue_format__c = 'A4',
                                                             catalogue_spelling__c = 'British English',
                                                             Event_Manager_Educators__c = userRecord.Id,
                                                             Event_Manager_Agents__c = userRecord.Id
                                                           );
                                                           
        insert recordOfICEFEvent;                                                   
        return recordOfICEFEvent.Id;
    }
}