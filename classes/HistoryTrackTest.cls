@isTest
public class HistoryTrackTest {
	
    @isTest static void TestCampaignHistoryOnAddMember(){
        String testCampaignId = DataFactory.createTestCampaign();
        Lead testLead = DataFactory.createTestLead('Educator');
        testLead.Email = 'testlead@icef.com';
        insert testLead;
        
		CampaignMember testCM = DataFactory.createTestCampaignMember(testCampaignId, testLead.Id);
        Test.startTest();
        insert testCM;
        Test.stopTest();
        
        System.assertEquals(testLead.Id, [SELECT Lead_Id__c FROM CampaignHistory__c WHERE CampaignId__c = :testCampaignId][0].Lead_Id__c);        
        
    }
    
    @isTest static void TestCMhistoryOnChangeStatus(){
        String testCampaignId = DataFactory.createTestCampaign();
        Lead testLead = DataFactory.createTestLead('Educator');
        insert testLead;
        
        CampaignMember testCM = DataFactory.createTestCampaignMember(testCampaignId, testLead.Id);
        insert testCM;
        
        testCM.Status = 'Responded';
        
        Test.startTest();
        update testCM;
        Test.stopTest();
        
        System.assertEquals(testLead.Id, [SELECT Lead_Id__c 
                                          FROM CampaignHistory__c 
                                          WHERE CampaignId__c = :testCampaignId 
                                          AND Change__c = 'Campaign Member Status changed'][0].Lead_Id__c);
    }
    
    @isTest static void TestCMhistoryOnDelete(){
        String testCampaignId = DataFactory.createTestCampaign();
        Lead testLead = DataFactory.createTestLead('Educator');
        insert testLead;
        
        CampaignMember testCM = DataFactory.createTestCampaignMember(testCampaignId, testLead.Id);
        insert testCM;
        
        Test.startTest();
        delete testCM;
        Test.stopTest();
        
        System.assertEquals(testLead.Id, [SELECT Lead_Id__c 
                                          FROM CampaignHistory__c 
                                          WHERE CampaignId__c = :testCampaignId 
                                          AND Change__c = 'Campaign Member removed'][0].Lead_Id__c);
    }
    
}