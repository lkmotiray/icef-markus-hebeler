@isTest
public class TestBulkContactUpdate_Production {
	
    @testsetup 
    public static void setupData(){
        RecordType accountRecType = [SELECT Id, Name FROM RecordType WHERE SobjectType='Account' LIMIT 1];
        
        Account actRec = new Account(RecordTypeId = accountRecType.Id, Name='Test Account');
        insert actRec;
        
        List<Contact> cntList = new List<Contact>();
        Boolean testVar = true;
        for(Integer i = 0; i<100; i++){
            Contact cntRec = new Contact(FirstName='TestFirst'+i, 
                                    LastName='TestLast'+i, 
                                    Salutation='Mr', 
                                    Gender__c='male', 
                                    AccountId=actRec.Id,
                                    Role__c = 'Primary Decision Maker');
            if(testVar){
                cntRec.X20_year_mag__c = true;
                testVar = false;
            }
            else {
                cntRec.X20_year_mag__c = false;                
                testVar = true;
            }
            cntList.add(cntRec);
        }
        
        try{
            insert cntList;
        }
        catch(Exception e) {                
            System.debug('Message : '+e.getMessage());    
        }  
    }
    
    /*
      Method Name :- testBatchClass
      purpose     :- test the bacth class to update the account related to given contacts.
    */
    
    static testMethod void testBatchClass() {
        BulkContactUpdate_Production myBatchObject = new BulkContactUpdate_Production();
        Database.executeBatch(myBatchObject);
    }
}