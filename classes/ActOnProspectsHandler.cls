/**********************************************************************************
Developer : Amol B
Date : 11 Mar 2016
Discription : Creates new tasks for Contacts selected on List view
**********************************************************************************/
global class ActOnProspectsHandler 
{ 
    webService static String addNewTasks(List<Id> lstSelectedContactIds)
    { 
        try
        {
            //get contact and related account information
            List<Contact> lstSelectedContacts = [SELECT Id, AccountId, Account.OwnerId,Date_acted_upon__c
                                                 FROM Contact
                                                 WHERE Id IN :lstSelectedContactIds];
        
            //create new tasks                                     
            if(!lstSelectedContacts.isEmpty())
            {
                List<Task> lstNewTasks = new List<Task>();
                
                for(Contact contact : lstSelectedContacts)
                {
                    if(contact.AccountId != null)
                    {
                        lstNewTasks.add(new Task(OwnerId = contact.Account.OwnerId,
                                                 Priority = 'Normal',
                                                 Status = 'Not Started',
                                                 Subject = 'Act on prospect',
                                                 WhatId = contact.AccountId,
                                                 WhoId = contact.Id,
                                                 ActivityDate = System.today() + 7));
                                                 
                        contact.Date_acted_upon__c = System.Today();
                    }
                }
                
                if(!lstNewTasks.isEmpty())
                {
                    insert lstNewTasks;
                    update lstSelectedContacts;
                    return 'Successfully Added New Tasks';
                }
            }
        }
        catch(Exception e)
        {
            system.debug('Error : ' + e.getMessage());
            return 'Unable to create Tasks';
        }
        return 'Error';
    }
}