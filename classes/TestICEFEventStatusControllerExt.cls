@isTest
public class TestICEFEventStatusControllerExt
{
  //test method
    public static testMethod void test1()
    {
        //System.Debug('test Method started');

        Account acc = new Account(Name='Test Account');
        insert acc;   
        
        system.assertEquals('Test Account', acc.Name);     
        
        Contact con = new Contact(LastName='Tester', AccountId = acc.Id, Salutation = 'Dr.', Gender__c = 'male', Role__c = 'Other', Contact_Status__c = 'Active');
        insert con;
        
        system.assertEquals('Tester', con.LastName);
                        
        User currUser;
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile profile = [select id from profile where name='System Administrator'];
            UserRole userRole = [Select id from userrole where name='ARM'];
        
            currUser = new User(UserRoleId = userRole.Id, alias = 'dws', email='dwsTester@tester.com',
                   emailencodingkey='UTF-8', lastname='dwsTester', languagelocalekey='en_US',
                   localesidkey='en_US', profileId = profile.Id, 
           timezonesidkey='America/Los_Angeles', username='dwsTester@tester.com');
           
            insert currUser; 
            
            system.assertEquals('dwsTester@tester.com', currUser.username);   
        }
        
        Country__c country;
        
        System.runAs(currUser) {
         country = new Country__c(Name='Test Country', Agent_Relationship_Manager__c=currUser.Id, Region__c = 'Asia');
        insert country;
        system.assertEquals('Test Country', country.Name); 
        
       
        
        }
       
        //----1
    
        ICEF_Event__c icefEvent = new ICEF_Event__c(Name='Test IcefEvent', Event_City__c = 'City', technical_event_name__c = 'TestingEvent', Event_Manager_Educators__c = currUser.Id, Event_Manager_Agents__c = currUser.Id);
        insert icefEvent;        
        system.assertEquals('Test IcefEvent', icefEvent.Name);        
        
        Agent_Country_Target__c act;
        act = new Agent_Country_Target__c(Country__c=country.Id, ICEF_Event__c=icefEvent.Id, Agent_Target__c = 30);
        insert act;        
        act = new Agent_Country_Target__c(Country__c=country.Id, ICEF_Event__c=icefEvent.Id, Agent_Target__c = 20);
        insert act;
        
         List<RecordType> ListRT = [SELECT Id, SobjectType FROM RecordType where SobjectType='Account_Participation__c' AND Name='Agent'];
        
        Account_Participation__c ap;
        ap = new Account_Participation__c(Account__c=acc.Id,ICEF_Event__c=icefEvent.Id, Organisational_Contact__c=con.Id,Participation_Status__c='accepted', Catalogue_Country__c=country.Id, RecordTypeId=ListRT[0].Id, Country_Section__c = country.Id);
        insert ap;
        ap = new Account_Participation__c(Account__c=acc.Id,ICEF_Event__c=icefEvent.Id, Organisational_Contact__c=con.Id,Participation_Status__c='expression of interest', Catalogue_Country__c=country.Id, Country_Section__c = country.Id);
        insert ap;       
        ap = new Account_Participation__c(Account__c=acc.Id,ICEF_Event__c=icefEvent.Id, Organisational_Contact__c=con.Id,Participation_Status__c='moreinfo', Catalogue_Country__c=country.Id, Country_Section__c = country.Id);
        insert ap;
                
        ApexPages.StandardController stdCtlr = new ApexPages.Standardcontroller(icefEvent);
        ICEFEventStatusControllerExt testcon = new ICEFEventStatusControllerExt(stdCtlr);  
        
         System.Debug('icefEvent.Id:'+ icefEvent.Id); 
         
        testcon.eventId =  icefEvent.Id;
        testcon.ViewData(); 
        
        // -----------------------
        
        
        List<String> groupArr = new List<String>{'AccountOwner', 'Region'};
        List<String> fieldArr = new List<String>{'AccountOwners', 'Regions', 'Targets', 'Accepted', 'MoreInfo', 'ExpressionOfInterest', 'WaitListed', 'PipelineTotal', 'ReachedPer', 'PipelinePer', 'GrandTotal', 'TotalPer'};
        List<String> direcArr = new List<String>{'asc', 'desc'};
        
        for(String currGroup: groupArr)
        {
            testcon.groupFieldName = currGroup;
            System.Debug('currGroup:'+ currGroup); 
            testcon.regroup();
            for(String field: fieldArr)
            {
                testcon.sortExpression = field; 
                 System.Debug('field:'+ field);                  
                 
                for( String direc: direcArr)
                {
                    System.Debug('direc:'+ direc); 
                    testcon.setSortDirection(direc);
                     System.Debug('from test getSortDirection:'+ testcon.getSortDirection());    
                    testcon.ViewSortedData(); 
                   // List<testcon.EventDetails> testList = testcon.sortList(testcon.eventDetailRecords, field);       
                    
                } 
            }
        }
       
       
       System.Debug('test Method finished'); 
        
    }  
}