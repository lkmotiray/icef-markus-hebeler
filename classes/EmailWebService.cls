/**
  * @Description : Web service to send an bulk email
  * @Created Date: 24 October 2016
 */
global with sharing class EmailWebService {

    @TestVisible static String ORGWIDEEMAILNAME = 'Accounts, ICEF GmbH';
    @TestVisible static String templateName = 'codaSalesInvoice';
    
    /**
      * @purpose : send email
      * @Return  : list of invoices ids
     */
    webService static String sendEmail(List<String> ids) {       
        if(ids != null && !ids.isEmpty()) {
            //try {
                
                List<c2g__codaInvoice__c> salesInvoices = [ SELECT Id, Name, c2g__Opportunity__r.Invoicing_contact__r.Name, 
                                                                    c2g__Opportunity__r.Invoicing_contact__r.Id, 
                                                                    c2g__Opportunity__r.Invoicing_contact__r.Email,
                                                                    c2g__Opportunity__c, 
                                                                    c2g__Opportunity__r.CC_Invoice_to__r.Email,
                                                                    c2g__Opportunity__r.Invoicing_contact__r.Contact_Email__c,
                                                                    //c2g__Account__r.c2g__CODAInvoiceEmail__c,
                                                                    c2g__Account__r.Owner.Email,
                                                                    c2g__Opportunity__r.Owner.Email,
                                                                    c2g__Opportunity__r.Invoicing_contact__r.FirstName,
                                                                    c2g__InvoiceTotal__c, c2g__OutstandingValue__c,
                                                           			c2g__Account__r.Email_for_all_invoices__c
                                                            FROM c2g__codaInvoice__c
                                                            WHERE Id IN : ids ];
                
                InvoiceEmailSend.mapInvoices = New Map<Id, c2g__codaInvoice__c>(salesInvoices );
                
                if( salesInvoices != null && !salesInvoices.isEmpty() ) {                                           
                    List<OrgWideEmailAddress> orgWideEmails = [ SELECT Id, Address, DisplayName 
                                                                 FROM OrgWideEmailAddress
                                                                 WHERE DisplayName = : ORGWIDEEMAILNAME
                                                                 LIMIT 1 ];
                    
                    if( orgWideEmails == null || orgWideEmails.isEmpty() ) {
                        return 'false>>'+ 'Error : ' + ORGWIDEEMAILNAME + ' org wide email address not found!!';
                    }
                    
                    List<EmailTemplate> templates =  [ SELECT Id, DeveloperName 
                                                       FROM EmailTemplate 
                                                       WHERE DeveloperName = : templateName
                                                       LIMIT 1 ];
                    
                    if( templates == null || templates.isEmpty() ) {
                        return 'false>>'+ 'Error : ' + templateName + ' template not found!!';
                    }
                    
                    Set<Id> setId = new Set<Id>();
                    for( c2g__codaInvoice__c salesInvoice : salesInvoices) {
                        setId.add(salesInvoice.id);                        
                    }
                    
                    InvoiceEmailSend.mapInvoiceLines = InvoiceEmailSend.createMapInvoceineItems(setId);
                    
                    
                    List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
                    Messaging.SingleEmailMessage mailToSend;
                                        
                    List<c2g__codaInvoice__c> erroneousSalesInvoices = new List<c2g__codaInvoice__c >();
                    List<String> invoicesWithEmail = new List<String>();
                    integer i = 0;
                    for( c2g__codaInvoice__c salesInvoice : salesInvoices) {
                        mailToSend = new Messaging.SingleEmailMessage();
                        if( salesInvoice.c2g__Opportunity__r.Invoicing_contact__r != null 
                            && salesInvoice.c2g__Opportunity__r.Invoicing_contact__r.Email != null ) {
                            mailToSend = new Messaging.SingleEmailMessage();
                            mailToSend.setWhatId(salesInvoice.id);
                            mailToSend.setTargetObjectId(salesInvoice.c2g__Opportunity__r.Invoicing_contact__r.Id);
                            mailToSend.setOrgWideEmailAddressId(orgWideEmails[0].id);
                            mailToSend.setTemplateId(templates[0].id);
                            mailToSend.setReplyTo(orgWideEmails[0].Address);
                            
                            List<String> emailAddressList = new List<String>();
                                
                            List<String> listEmailToAddressList = new List<String>();
                                
                            if(salesInvoice.c2g__Opportunity__c != NULL){
                                if(salesInvoice.c2g__Opportunity__r.Invoicing_contact__r.Email != null && !emailAddressList.contains(salesInvoice.c2g__Opportunity__r.Invoicing_contact__r.Email)){
                                    listEmailToAddressList.add(salesInvoice.c2g__Opportunity__r.Invoicing_contact__r.Email);
                                    emailAddressList.add(salesInvoice.c2g__Opportunity__r.Invoicing_contact__r.Email);
                                }
                            }
                                
                            if(salesInvoice.c2g__Account__c != NULL && salesInvoice.c2g__Account__r.Email_for_all_invoices__c != null 
                               && !emailAddressList.contains(salesInvoice.c2g__Account__r.Email_for_all_invoices__c)) { 
                                 listEmailToAddressList.add((String)salesInvoice.getSObject('c2g__Account__r').get('Email_for_all_invoices__c')); 
                                 emailAddressList.add(salesInvoice.c2g__Account__r.Email_for_all_invoices__c);
                            }
                                
                            mailToSend.setToAddresses(listEmailToAddressList);
                            
                            List<String> ccAddressList = new List<String>();
                            
                            // set CC for mail
                            /*if( String.isNotBlank(salesInvoice.c2g__Account__r.c2g__CODAInvoiceEmail__c ) && !emailAddressList.contains(salesInvoice.c2g__Account__r.c2g__CODAInvoiceEmail__c) ) {
                                ccAddressList.add(salesInvoice.c2g__Account__r.c2g__CODAInvoiceEmail__c);       
                                emailAddressList.add(salesInvoice.c2g__Account__r.c2g__CODAInvoiceEmail__c);                
                            }*/
                            /*mailToSend.setCcAddresses(new List<String>{ salesInvoice.c2g__Account__r.c2g__CODAInvoiceEmail__c, orgWideEmails[0].Address, salesInvoice.c2g__Account__r.Owner.Email, salesInvoice.c2g__Opportunity__r.CC_Invoice_to__r.Email });*/
                            
                            if( salesInvoice.c2g__Account__r.Owner.Email != NULL && !emailAddressList.contains(salesInvoice.c2g__Account__r.Owner.Email)){
                                ccAddressList.add(salesInvoice.c2g__Account__r.Owner.Email);
                                emailAddressList.add(salesInvoice.c2g__Account__r.Owner.Email);
                            }
                            
                            if( salesInvoice.c2g__Opportunity__r.Owner.Email != NULL && !emailAddressList.contains(salesInvoice.c2g__Opportunity__r.Owner.Email)){
                                ccAddressList.add(salesInvoice.c2g__Opportunity__r.Owner.Email);
                                emailAddressList.add(salesInvoice.c2g__Opportunity__r.Owner.Email);
                            }
                            
                            if( salesInvoice.c2g__Opportunity__r.CC_Invoice_to__r.Email != NULL && !emailAddressList.contains(salesInvoice.c2g__Opportunity__r.CC_Invoice_to__r.Email)){
                                ccAddressList.add(salesInvoice.c2g__Opportunity__r.CC_Invoice_to__r.Email);
                                emailAddressList.add(salesInvoice.c2g__Opportunity__r.CC_Invoice_to__r.Email);
                            }
                            
                            if( orgWideEmails[0].Address != NULL ){
                                ccAddressList.add(orgWideEmails[0].Address);
                            }
                            
                            if( !ccAddressList.isEmpty()){
                                mailToSend.setCcAddresses(ccAddressList);
                            }
                            
                            System.debug('cc address::' + mailToSend.getCcAddresses());
                            
                            
                            //System.debug('email---' + salesInvoice.c2g__Opportunity__r.CC_Invoice_to__r.Email);
                            
                            System.debug('mailToSend---' + mailToSend);
                                
                            //mailToSend.setSaveAsActivity(false);
                            mailToSend.setSaveAsActivity(true);
                            
                            lstMsgsToSend.add(mailToSend);
                            invoicesWithEmail.add(salesInvoice.Name); 
                        } else {
                            erroneousSalesInvoices.add(salesInvoice);
                        }
                       
                    }        
                    Messaging.SendEmailResult[] resultSendEmail;    
                    try{
                        //System.debug('mails to send : ' + lstMsgsToSend);
                        resultSendEmail = Messaging.sendEmail(lstMsgsToSend);
                    } catch( Exception genericException ) {
                        exceptionHandling(genericException);
                        return 'false>>'+ 'Error : ' + genericException.getMessage();
                    }
                    String emailSendingResult = getEmailSendingResult( lstMsgsToSend,
                                                                       resultSendEmail, 
                                                                       invoicesWithEmail, 
                                                                       erroneousSalesInvoices);
                    sendNotificationEmail(emailSendingResult, 'Invoice Email Sent', UserInfo.getUserEmail());
                    
                    return 'true>>Email sent successfully';
                } else {
                    return 'false>>'+ 'Error : ' + 'Invoices not found!!';
                }
           /*} catch( Exception genericException ) {
                exceptionHandling(genericException);
                return 'false>>'+ 'Error : ' + genericException.getMessage();
            }*/
        } else {
            return 'false>>'+ 'Error : ' + 'Invoices not found!!';
        }
    }
    
    /**
      * @purpose : send email
      * @param   : 1) result of Sent Emails
                   2) invoices With Email
                   3) sales invoices with invalid email address
      * @return  : string of error and success msgs
     */
    private static String getEmailSendingResult( List<Messaging.SingleEmailMessage> lstMsgsToSend,
                                                 Messaging.SendEmailResult[] resultSentEmail, 
                                                 List<String> invoicesWithEmail, 
                                                 List<c2g__codaInvoice__c> erroneousSalesInvoices ) {
        System.debug('Email result : ' + resultSentEmail);
        List<c2g__codaInvoice__c> invocesToUpdate = new List<c2g__codaInvoice__c>();
        List<Id> invoicesIdsToCreateActivities = new List<Id>();
        String emailSendingResult = '';
        Integer count = 0 ;
        Integer sizeInvoicesWithEmail = invoicesWithEmail.size();
        String errorMsgs = '<br/><b>Error occurred while sending following invoices</b><table border="1"><th>Sales Invoice</th><th>Error</th>';
        String successfulInvoices = '<b>The email for the following invoices have been sent:</b><br/>';
        Boolean isFail = false;
        Boolean isSuccess = false;
        
        for( Integer i = 0; i < sizeInvoicesWithEmail; i++ ) { 
            if( resultSentEmail.get(i).isSuccess()) {
                isSuccess = true;
                successfulInvoices += (i+1) + ') ' + invoicesWithEmail[i] + '<br/>';
                c2g__codaInvoice__c invoice = new c2g__codaInvoice__c( Id = lstMsgsToSend[i].getWhatId(),
                                                                       Sent__c = true
                                                                     );
                invocesToUpdate.add(invoice);
                invoicesIdsToCreateActivities.add(lstMsgsToSend[i].getWhatId());
            } else {
                isFail = true;
                errorMsgs += '<tr><td>' + invoicesWithEmail[i] + '</td>' +
                             '<td>' + resultSentEmail.get(i).getErrors()[0].getMessage() + '</td></tr>';        
            }
        }
        
        emailSendingResult += isSuccess?successfulInvoices:'';
        
        if( erroneousSalesInvoices != null
            && ! erroneousSalesInvoices.isEmpty() ) {
            emailSendingResult += '<br/><b>The following emails were not sent as no invoice email exists:</b><br/>';
            count = 1;
        }
                       
        for( c2g__codaInvoice__c salesInvoice : erroneousSalesInvoices ) {
            emailSendingResult  += count + ') ' + salesInvoice.name + '<br/>';
            count++;        
        }
        
        emailSendingResult += isFail?errorMsgs + '</table>':'';
        
        Database.SaveResult[] results = Database.update(invocesToUpdate,false);
        Boolean isFailUpdation = false;
        String updationResult = '<br/><b>Error occurred while updating sent checkbox on invoices</b><table border="1"><th>Sales Invoice</th><th>Error</th>';
        if (results != null){
            for (Database.SaveResult result : results) {
                if (!result.isSuccess()) {
                    isFailUpdation = true;
                    Database.Error[] errs = result.getErrors();
                    String updateErrorMsg = '';
                    for(Database.Error err : errs) {
                        updateErrorMsg += err.getStatusCode() + ' - ' + err.getMessage() + ';';
                    }
                    updationResult += '<tr><td>' + result.getId() + '</td>' +
                                 '<td>' + updateErrorMsg + '</td></tr>';      
                }
            }
        }
        
        emailSendingResult += isFailUpdation?updationResult + '</table>':'';
        
        ActivityManager.createActivityHistory(invoicesIdsToCreateActivities);
        
        return emailSendingResult;               
    }
    
    /**
      * @purpose : send email
      * @Return  : 1) html body
                   2) subject
                   3) To address
     */
    private static void sendNotificationEmail( String htmlBody,
                                               String subject,
                                               String Toaddress ) {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(new String[] { Toaddress });
        if(Test.isRunningTest()) {
            email.setToAddresses(new String[] { Toaddress });    
        }
        email.setSubject(subject);
        email.sethtmlBody(htmlBody);
        try {
            Messaging.sendEmail(new Messaging.Email[] { email });
        } catch(Exception genericException ) {
            exceptionHandling(genericException);
        }
    }
    
    /**
        @ Purpose : To Print the Exception  
    */
    global static void exceptionHandling(Exception e){
        
         System.debug('Error ::' + e.getMessage() + ' \n Stack Trace:' + e.getStackTraceString());
    }
}