@isTest
//verschiedene methoden für verschiedene Fälle. Membership update, delete, verschiedene membership types, account record types
//der hier funzt wenigstens schonmal...
public class MembershipsHandlerTest {
    
    //test method UpdateMembershipsInAccount for Member Agent with Memberships: AM: 2, IQ: 0, IR: 0 
    static testMethod void testUpdateMembershipsInAccountAgentAM(){
		List<Account> agents = DataFactory.createTestMembers(1, 'Agent');
        Account agent = agents.get(0);
//        System.debug('agent Memberships: '+agent.Memberships__c);
//        System.debug('agent Name: '+agent.Name);
        List<Account> associations = DataFactory.createTestAssociations(2, 'Association Membership');
        insert associations;
//        System.debug('Test Associations: '+ associations);
        List<Membership__c> memList = new List<Membership__c>();
        for(Account assoc : associations){
            Membership__c mem = DataFactory.createTestMembership(agent, assoc);
            memList.add(mem);
        }
        insert memList;
//        System.debug('memList: '+ memList);
//        List<Membership__c> memListToTest = [SELECT Id, Name_short__c, Status__c, Member__c from Membership__c where Name_short__c = 'short AM 0'];
        Map<Id, Membership__c> memMapToTest = new Map<id,Membership__c>([SELECT Id, Name_short__c, Status__c, Member__c from Membership__c where Name_short__c = 'short AM 0']);
//        System.debug('memListToTest: '+memListToTest);        
        MembershipsHandler.updateMembershipsInAccount(memMapToTest);
        Account a = [SELECT Name, Memberships__c, Industry_Qualifications__c, Industry_Recognition__c FROM Account WHERE Name = :agent.Name];
//        System.debug('a.Name: ' + a.Name);
//        System.debug('a.Memberships__c: '+a.Memberships__c);
        System.assertEquals('short AM 0;short AM 1', a.Memberships__c);
        System.assertEquals(null, a.Industry_Qualifications__c);
        System.assertEquals(null, a.Industry_Recognition__c);
    }
    
    //test method UpdateMembershipsInAccount for Member Agent with Memberships: AM: 0, IQ: 2, IR: 0
    static testMethod void testUpdateMembershipsInAccountAgentIQ(){
        List<Account> agents = DataFactory.createTestMembers(1, 'Agent');
        Account agent = agents.get(0);
        List<Account> associations = DataFactory.createTestAssociations(2, 'Agent Industry Qualification');
        insert associations;
        List<Membership__c> memList = new List<Membership__c>();
        for(Account assoc : associations){
            Membership__c mem = DataFactory.createTestMembership(agent, assoc);
            memList.add(mem);
        }
        insert memList;
        Map<id,Membership__c> memMapToTest = new Map<Id,Membership__c>([SELECT Id, Name_short__c, Status__c, Member__c from Membership__c where Name_short__c = 'short IQ 0']);
//        System.debug('memListToTest: '+memListToTest);             
        MembershipsHandler.updateMembershipsInAccount(memMapToTest);
        Account a = [SELECT Name, Memberships__c, Industry_Qualifications__c, Industry_Recognition__c FROM Account WHERE Name = :agent.Name];
        System.assertEquals(null, a.Memberships__c);
        System.assertEquals('short IQ 0;short IQ 1', a.Industry_Qualifications__c);
        System.assertEquals(null, a.Industry_Recognition__c);
    }
    
    //test method UpdateMembershipsInAccount for Member Agent with Memberships: AM: 1, IQ: 1, IR: 1
    static testMethod void testUpdateMembershipsInAccountAgentAmIqIr(){
        List<Account> agents = Datafactory.createTestMembers(1, 'agent');
        Account agent = agents.get(0);
        List<Account> associations = new List<Account>();
        List<Account> ams = DataFactory.createTestAssociations(1, 'Association Membership');
        List<Account> iqs = DataFactory.createTestAssociations(1, 'Agent Industry Qualification');
        List<Account> irs = DataFactory.createTestAssociations(1, 'Industry Recognition');
        associations.addAll(ams);
        associations.addAll(iqs);
        associations.addAll(irs);
        insert associations;
        List<Membership__c> memList = new List<Membership__c>();
        for(Account assoc : associations){
            Membership__c mem = DataFactory.createTestMembership(agent, assoc);
            memList.add(mem);
        }
        insert memList;
        Map<id,Membership__c> memMapToTest = new Map<id,Membership__c>([SELECT Id, Name_short__c, Status__c, Member__c from Membership__c where Name_short__c = 'short AM 0']);
        //System.debug('memListToTest: '+memMapToTest);               
        MembershipsHandler.updateMembershipsInAccount(memMapToTest);        
        Account a = [SELECT Name, Memberships__c, Industry_Qualifications__c, Industry_Recognition__c FROM Account WHERE Name = :agent.Name];
        System.assertEquals('short AM 0', a.Memberships__c);
        System.assertEquals('short IQ 0', a.Industry_Qualifications__c);
        System.assertEquals('short IR 0', a.Industry_Recognition__c);        
    }
    
    //test method UpdateMembershipsInAccount for Member Educator with Memberships: AM: 1, IQ: 1, IR: 1
    static testMethod void testUpdateMembershipsInAccountEduAmIqIr(){
        List<Account> edus = Datafactory.createTestMembers(1, 'educator');
        Account edu = edus.get(0);
        List<Account> associations = new List<Account>();
        List<Account> ams = DataFactory.createTestAssociations(1, 'Association Membership');
        List<Account> iqs = DataFactory.createTestAssociations(1, 'Agent Industry Qualification');
        List<Account> irs = DataFactory.createTestAssociations(1, 'Industry Recognition');
        associations.addAll(ams);
        associations.addAll(iqs);
        associations.addAll(irs);
        insert associations;
        List<Membership__c> memList = new List<Membership__c>();
        for(Account assoc : associations){
            Membership__c mem = DataFactory.createTestMembership(edu, assoc);
            memList.add(mem);
        }
        insert memList;
        Map<id,Membership__c> memMapToTest = new Map<id,Membership__c>([SELECT Id, Name_short__c, Status__c, Member__c from Membership__c where Name_short__c = 'short AM 0']);
        //System.debug('memListToTest: '+memMapToTest);              
        MembershipsHandler.updateMembershipsInAccount(memMapToTest);
        Account a = [SELECT Name, Memberships__c, Industry_Qualifications__c, Industry_Recognition__c FROM Account WHERE Name = :edu.Name];
        System.assertEquals('short AM 0;short IQ 0;short IR 0', a.Memberships__c);
        System.assertEquals(null, a.Industry_Qualifications__c);
        System.assertEquals(null, a.Industry_Recognition__c);             
    }
    
    //test method UpdateMembershipsInAccount for Member Agent with only OTHER memberships
    static testMethod void testUpdateMembershipsInAccountOtherMemberships(){
        List<Account> agents = DataFactory.createTestMembers(1, 'agent');
        Account agent = agents.get(0);
        List<Account> associations = DataFactory.createTestAssociations(1, 'Other');
        insert associations;
        List<Membership__c> memList = new List<Membership__c>();
        for(Account assoc : associations){
            Membership__c memAgt = DataFactory.createTestMembership(agent, assoc);
            memList.add(memAgt);
        }
        insert memList;
        Map<Id,Membership__c> memMapToTest = new Map<Id,Membership__c>([SELECT Id, Name_short__c, Status__c, Member__c from Membership__c where Name_short__c = 'short 0']);
        //System.debug('memListToTest: '+memMapToTest);        
        MembershipsHandler.updateMembershipsInAccount(memMapToTest);
        Account aAgent = [SELECT Name, Memberships__c, Industry_Qualifications__c, Industry_Recognition__c FROM Account WHERE Name = :agent.Name];
        System.assertEquals('short  0', aAgent.Memberships__c);
        System.assertEquals(null, aAgent.Industry_Qualifications__c);
        System.assertEquals(null, aAgent.Industry_Recognition__c);
    }
    //test method UpdateMembershipsInAccount for Member Educator with only inactive Membership
    static testMethod void testUpdateMembershipsInAccountEduInactiveMembership(){
        List<Account> edus = DataFactory.createTestMembers(1, 'educator');
        Account edu = edus.get(0);
        List<Account> associations = DataFactory.createTestAssociations(1, 'Association Membership');
        insert associations;
        List<Membership__c> memList = new List<Membership__c>();
        for(Account assoc : associations){
            Membership__c mem = DataFactory.createTestMembership(edu, assoc);
            mem.Status__c = 'Inactive';
            memList.add(mem);
        }
        insert memList;
        Map<Id,Membership__c> memMapToTest = new Map<Id,Membership__c>([SELECT Id, Name_short__c, Status__c, Member__c from Membership__c where Name_short__c = 'short AM 0']);
        //System.debug('memListToTest: '+memMapToTest);
        MembershipsHandler.updateMembershipsInAccount(memMapToTest);
        Account a = [SELECT Name, Memberships__c, Industry_Qualifications__c, Industry_Recognition__c FROM Account WHERE Name = :edu.Name];
        System.assertEquals(null, a.Memberships__c);
        System.assertEquals(null, a.Industry_Qualifications__c);
        System.assertEquals(null, a.Industry_Recognition__c);        
    }
}