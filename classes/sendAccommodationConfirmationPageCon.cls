public class sendAccommodationConfirmationPageCon {
    
    Public String Accommodation_Id { get; set;}
    public String contactPartId { get; set; }
    
    public sendAccommodationConfirmationPageCon() {
        Accommodation_Id = System.currentPageReference().getParameters().get('id');
        contactPartId = System.currentPageReference().getParameters().get('cpid');
    }    
}