@isTest
public class CreateProductsControllerTest {

    @testSetup
    private static void createTestData() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User usr = new User(Alias = 'standt', Email='standarduserabcd@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduserefgh@testorg.com', CommunityNickname='tester');
        INSERT usr;
        Country__c country = new Country__c(Name = 'India', Region__c = 'Asia', Agent_Relationship_Manager__c = usr.Id);
        INSERT country;
        
        c2g__codaGeneralLedgerAccount__c codaAccount = new c2g__codaGeneralLedgerAccount__c();
        codaAccount.CurrencyIsoCode = 'USD';
        codaAccount.Name = 'Test Coda';
        codaAccount.c2g__ReportingCode__c = 'A1B2';
        codaAccount.c2g__Type__c = 'Balance Sheet';
        INSERT codaAccount;
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Hotel').getRecordTypeId();
        Account hotel = new Account();
        hotel.Name = 'Test Hotel';
        hotel.OwnerId = usr.Id;
        hotel.RecordTypeId = RecordTypeIdAccount;
        hotel.Status__c = 'Active';
        hotel.Mailing_Country__c = country.Id;
        hotel.c2g__CODAAccountsReceivableControl__c = codaAccount.Id;
        hotel.c2g__CODAAccountTradingCurrency__c = 'USD';
        INSERT hotel;
        
        List<Product2> productList = new List<Product2>();
        Product2 product01 = new Product2();
        product01.Name = 'Test Product 1';
        product01.ProductCode = 'A123';
        product01.IsActive = True;
        product01.Family = 'Workshop Catalogue/Guide';
        product01.CurrencyIsoCode = 'USD';
        productList.add(product01);
        Product2 product02 = new Product2();
        product02.Name = 'Test Product 2';
        product02.ProductCode = 'B123';
        product02.IsActive = True;
        product02.Family = 'Workshop Receptions';
        product02.CurrencyIsoCode = 'USD';
        productList.add(product02);
        INSERT productList;
        
        ICEF_Event__c ICEF_Event = new ICEF_Event__c();
        ICEF_Event.Name = 'Test Event';
        ICEF_Event.Event_City__c = 'Test City';
        ICEF_Event.Event_Country__c = country.Id;
        ICEF_Event.technical_event_name__c = 'Test Technical';
        ICEF_Event.Event_Manager_Agents__c = usr.Id;
        ICEF_Event.Event_Manager_Educators__c = usr.Id;
        ICEF_Event.Event_Manager_Exhibitors__c = usr.Id;
        ICEF_Event.catalogue_format__c = 'A4';
        ICEF_Event.catalogue_spelling__c = 'American English';
        ICEF_Event.form_edu_regular_rate_startdate__c = system.today();
        ICEF_Event.form_edu_available_currency__c = 'USD';
        ICEF_Event.Venue_Hotel__c = hotel.Id;
        INSERT ICEF_Event;
    }
    
    static testmethod void createPPRecordsTest () {
        Test.startTest();
        ICEF_Event__c ICEF_Event = [SELECT Id, Name FROM ICEF_Event__c LIMIT 1];
        Account hotel = [SELECT Id, Name FROM Account LIMIT 1];
        Boolean isValid = CreateProductsController.validateAccount(hotel.Id);
        if(isValid){
            system.assertEquals(true, isValid);
            CreateProductsController contr = new CreateProductsController(new ApexPages.StandardController(ICEF_Event));
            List<Product2> productList = getProductsTest();
            if(productList.size()>0){
                system.assertNotEquals(0, productList.size());
                String productListstr = createJSONString(productList);
                String eventId = CreateProductsController.getEventId();
                //system.debug(':::'+eventId);
                Boolean isSuccess = CreateProductsController.createPPRecords(productListstr, ICEF_Event.Id, hotel.Id);
                system.assertEquals(true, isSuccess);
                //system.debug('is Successfully created????'+isSuccess);
            }
        }
        Test.stopTest();
    }
    
    static testmethod List<Product2> getProductsTest () {
        List<Product2> productList = CreateProductsController.getProducts();
        //system.debug('list::::'+productList);
        return productList;
    }
    
    public static string createJSONString(List<Product2> productList){
    	string json ='['
                        +'{"Id":"'+productList[0].Id+'","Name":"'+productList[0].Name+'","Family":"'+productList[0].Family+'","count":1},'
                        +'{"Id":"'+productList[1].Id+'","Name":"'+productList[1].Name+'","Family":"'+productList[1].Family+'","count":1}'
                    +']';
        //system.debug('json string === '+ json);
        return json;
    }
}