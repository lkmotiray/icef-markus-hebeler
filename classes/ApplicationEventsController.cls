/*
Controller for ApplicationWiseEvents and createAccountPart VF Page
Unit test in _______ Apex Class
*/

public with sharing class ApplicationEventsController 
{        
    public String ApplicationId;    
    public RecordType rtypeacc{get; set;}
    Application__c application {get; set;}
    public Account acc{get; set;}
    List<String> eventNames;
    List<String> eventsLocation {get; set;}
    public List<ICEF_Event__c> eventDetails {get; set;}
    Map<String, String> mapEventNameId {get; set;}            // Event Name => Event Id
    Map<String, String> mapRecordTypeAP{get; set;}            // Record Type Name => Record Type Id of AP
    Map<String, List<String>> dependentPicklistMap {get; set;}    //Stores the controlling Picklist value with related List of dependent values

    public List<Account_Participation__c> accPartList{get; set;}
    public List<SelectOption> rTypes{get; set;}
    public List<RecordType> rt{get; set;}
    
    public sObjectWrapper sObjWrap {get; set;}
    public List<sObjectWrapper> sObjWrapList {get; set;}
        
    public ApplicationEventsController()
    {
           ApplicationId = ApexPages.CurrentPage().getParameters().get('id');
           application = [select id, Events_interested_in__c, Account__c, Contact__c, Looking_for_Partners_in__c, Application_Date_Account__c from Application__c where id =: ApplicationId];
           eventsLocation = (application.Events_interested_in__c).split(';');    
           
           acc = [select id, RecordTypeId, Phone, Website, Website_2__c, Email__c, Fax, Mailing_City__c, Mailing_State_Province__c, Mailing_Country__c,Name, Department__c, Mailing_street__c, Mailing_Post_Code__c from Account where id =: application.Account__c];
                 
           eventNames = fetchEventNames(eventsLocation);
                    
           rtypeacc = [select id, name from RecordType where SobjectType = 'Account' and id =:acc.RecordTypeId];
           system.debug('Account record type: '+rtypeacc.name);
           
           rt = new List<RecordType>();
           rt = [select id,name from RecordType where SobjectType = 'Account_Participation__c'];
           rTypes = new List<SelectOption>();
           mapRecordTypeAP = new Map<String, String>();
           for(RecordType r: rt)
           {
               rTypes.add(new SelectOption(String.valueOf(r.name), String.valueOf(r.name)));
               mapRecordTypeAP.put(String.valueOf(r.name), String.valueOf(r.id));
           }
           
           String rtypename;
           if(rtypeacc.name == 'Educator' || rtypeacc.name == 'Agent' || rtypeacc.name == 'Work and Travel')
               rtypename = rtypeacc.name;
           else
               rtypename = 'Exhibitor';
                      
           sObjWrapList = new List<sObjectWrapper>();
           for(String e : eventNames)
           {
               sObjWrap = new sObjectWrapper();
               sObjWrap.EventName = e;                     
               sObjWrap.RecordType = rtypename ;                       
               sObjWrapList.add(sObjWrap);
           }
           System.debug('sObjWrapList: '+sObjWrapList);
           if(sObjWrapList.size() == 0)
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No Upcoming Events Found.')); 
           system.debug('ApplicationId: '+ApplicationId);                      
    }
    
     
    List<String> fetchEventNames(List<String> eventsLocation)
    {
            //This map will be used to store the Event Name with Start Date
        Map<String,Date> eventDetailsMap=new Map<String,Date>();
            //This list will store the list of Start Dates of Event
        List<Date> eventDates = new List<Date>();
        
        Date todaysDate = Date.today();                
        List<String> eventNames = new List<String>();
        eventDetails = new List<ICEF_Event__c>();
        mapEventNameId = new Map<String, String>();
        eventDetails = [select id, Name, Event_Full_Name__c, Event_Country__r.Name, Start_date__c from ICEF_Event__c where (Start_date__c >=:todaysDate) ORDER BY Start_date__c ASC];
        System.debug('Event List:::'+eventDetails);               
            for( String loc : eventsLocation)
            {
                if(loc == 'North America (Canada)' )
                {
                    for(ICEF_Event__c e : eventDetails)        
                    {
                        if(e.Event_Country__r.Name == 'Canada')
                        {
                            //eventNames.add(e.Name);
                            eventDetailsMap.put(e.Name,e.Start_date__c);
                            mapEventNameId.put(e.Name, e.id);
                        }
                    }
                }
                else if(loc == 'North America (US)' )
                {
                    for(ICEF_Event__c e : eventDetails)        
                    {
                        if(e.Event_Country__r.Name == 'USA')
                        {
                            //eventNames.add(e.Name);
                            eventDetailsMap.put(e.Name,e.Start_date__c);
                            mapEventNameId.put(e.Name, e.id);
                        }
                    }
                }
                else if(loc == 'Japan - Korea Agent Roadshow' )
                {
                    for(ICEF_Event__c e : eventDetails)        
                    {
                        if(e.Name.contains('Japan - Korea'))
                        {
                            //eventNames.add(e.Name);
                            eventDetailsMap.put(e.Name,e.Start_date__c);
                            mapEventNameId.put(e.Name, e.id);
                        }
                    }
                }
                else if(loc != 'WTZ')
                {
                    for(ICEF_Event__c e : eventDetails)        
                    {
                        if(e.Name.contains(loc))
                        {
                            //eventNames.add(e.Name);
                            eventDetailsMap.put(e.Name,e.Start_date__c);
                            mapEventNameId.put(e.Name, e.id);                            
                        }
                    }
                }
            }    
                //sorting events by Event Start Date
            eventDates.addAll(eventDetailsMap.values());
            eventDates.sort();
            System.debug('Event Dates after sorting:::'+eventDates);
            for(Date eventStartDate: eventDates) 
            {
                for(String eventName: eventDetailsMap.keySet()) 
                {
                    if(eventDetailsMap.get(eventName)==eventStartDate)
                        eventNames.add(eventName);
                }
            }
            
        System.debug('Event Names:::'+eventNames);                   
        return eventNames;
    }
    
    
    /* Function
        @purpose-This function parse the jsonResponse received from Javascript
        @calledFrom - action function on VF Page
    */
    public void parseJson() {
        dependentPicklistMap = new Map<String, List<String>>();
            //get parameter
        String statusDetails = ApexPages.CurrentPage().getParameters().get('statusDetailsMap');
        System.debug('Status Details::'+statusDetails);
            //JSON Parser
        JSONParser parser = JSON.createParser(statusDetails);     
           
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                String ControllingText = parser.getText();                             
                parser.nextToken();                                
                List<String> dependentList = new List<String>();             
                if(parser.getCurrentToken()==JSONToken.START_ARRAY)  {                   
                    while(parser.nextToken() != JSONToken.END_ARRAY) {
                        dependentList.add(parser.getText());
                    }
                }
                dependentPicklistMap.put(ControllingText, dependentList);
            }
        }
        System.debug('Dependent PickList:::'+dependentPicklistMap);        
        loadDependent();
    }
    
    
    /*Function 
        @purpose-this function will set the dependent picklist value on load
        @calledFrom - 1) from parseJson method 
                      2) from vf page on change of controlling picklist
    */
    public void loadDependent(){
        for(sObjectWrapper s : sObjWrapList){
            List<SelectOption> options = new List<SelectOption>();        
            List<String> dependentValues = new List<String>();           
            dependentValues = dependentPicklistMap.get(s.PartStatus);
                //check if the controlling picklist value has dependent picklist or not
            if(dependentValues.size()>0) {                 
                for(String str:  dependentValues )
                    options.add(new SelectOption(str, str));
                s.RelatedStatusDetails = options;
                s.disableStatusDetails = false;
            }
            else {                   
                options.add(new SelectOption('--None--', '--None--'));
                s.RelatedStatusDetails = options;
                s.disableStatusDetails = true;
            }
        
        }
    }
    
    
    /*
        //this function is not used as List<SelectOptions> are available now
    public List<SelectOption> getAccPartStatus()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Account_Participation__c.Participation_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry p : ple)
            options.add(new SelectOption(p.getValue(), p.getValue())); 
        return options;
    }       */
    
    public PageReference nextPage()
    {      
        for(sObjectWrapper s : sObjWrapList)
        {
            if(s.RecordType == 'Agent')
                s.PartStatus = 'expression of interest';
            else
                s.PartStatus = 'pending payment';
                
                //for Preparing list of part status according to Record Type
            if(s.Selected)
            {
                List<SelectOption> options = new List<SelectOption>();                
                String rTypeString = s.RecordType.trim().replace(' ','_');               
                System.debug('Selected recordtype:::'+rTypeString );
                //try
                //{
                    List<String> seloptions = PicklistDescriber.describe('Account_Participation__c',rTypeString , 'Participation_Status__c');
                    for(String sOption : seloptions)
                        options.add(new SelectOption(sOption, sOption));
                    s.RelatedPartStatus = options;
                    System.debug('RType and Select Option:::'+rTypeString+' '+s.RelatedPartStatus);
                //}
                //catch(Exception e)
                //{}
            }
        }
        PageReference page = new PageReference('/apex/createAccountPart?id='+ApplicationId);
        return page;
    }
     
    public PageReference previousPage()
    {
       PageReference page = new PageReference('/apex/ApplicationWiseEvents?id='+ApplicationId);
        return page;
    }
         
    public PageReference cancel() 
    {
        system.debug('ApplicationId: '+ApplicationId);
        PageReference page = new PageReference('/'+ApplicationId);
        return page;
    }      
    
    
    /*Function 
        @purpose-this function will add the message in page message area on VisualForce Page
        @calledFrom - 1) from javascript on Page                     
    */
    public void showMessage() {
        String errorLevel = ApexPages.CurrentPage().getParameters().get('errorLevel');
        String messageName = ApexPages.CurrentPage().getParameters().get('messageName');
        if(errorLevel == 'ERROR') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, messageName));
        }
    }
    
    
    /*Function 
        @purpose-this function toggles the required attribute of text area(i.e.Status Comment)
        @calledFrom - 1) from vf page on change of Status Details
    */
    public PageReference toggleStatusComment() {        
        for(sObjectWrapper s : sObjWrapList) {            
            if(s.StatusDetails == 'Other Reason') {
                s.requiredStatusComment = true;
            }
            else
                s.requiredStatusComment = false;
        }
        return null;
    }
    
    
    /*Function 
        @purpose-this function will validate:    
                      1) Status Comment Length(not greater than 255).
                      2) Status Comment is not empty is Status Detail is Other Reason.
        @calledFrom - 1) from vf page on click of Create button
    */                
    public PageReference validateAndSave() {
        
        boolean isValid = true;
        for(sObjectWrapper s : sObjWrapList) {
            if(s.Selected) {
                System.debug('status comment:::'+s.StatusComment); 
                if(s.StatusComment.length()>255){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please check the comment size(Maximum 255 characters allowed).'));            
                    return null;                    
                }   
                                        
                if(s.requiredStatusComment && s.StatusComment == '') {
                    isValid = false;
                    break;
                }                
            }                    
        }
        if(isValid)
            return saveAP();
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please Enter the comment for other reason'));            
            return null;
        }
    }    
        
    public PageReference saveAP() 
    {     
        Date todaysDate = Date.today(); 
        accPartList = new List<Account_Participation__c>();       
        for(sObjectWrapper s : sObjWrapList)
        {            
            if(s.Selected == true)
            {
                Account_Participation__c accpart = new Account_Participation__c();
                accpart.ICEF_Event__c = mapEventNameId.get(s.EventName);
                accpart.RecordTypeId = mapRecordTypeAP.get(s.RecordType);
                accpart.Participation_Status__c = s.PartStatus;
                accpart.Status_details__c = s.StatusDetails;                    //insert newly added stat
                accpart.Status_comments__c = s.StatusComment;                   //insert status comment
                accpart.Account__c = application.Account__c;
                accpart.Organisational_Contact__c = application.Contact__c;
                accpart.Priority_Countries__c = application.Looking_for_Partners_in__c;
                accpart.Application_Date__c = application.Application_Date_Account__c;
                accpart.Status_Date__c = todaysDate;
                accpart.Primary_Recruitment_Country__c = acc.Mailing_Country__c;        //Primary Recruitment Country updated
                System.debug('Status date:'+accpart.Status_Date__c);
                ICEF_Event__c ev = [select id, Name, Predecessor_event__c from ICEF_Event__c where id =:mapEventNameId.get(s.EventName)];
                System.debug('ev'+ev);               
                if(ev.Predecessor_event__c != null)
                {           
                    List<Account_Participation__c> accpartobj = [select id, ICEF_event_Full_name__c, Catalogue_Website2__c, Catalogue_Website__c, Catalogue_Email2__c, Catalogue_Email1__c, Catalogue_Fax__c, Catalogue_Tel__c, Catalogue_State_Province__c, Catalogue_City__c, Catalogue_Post_Code__c, Catalogue_Street__c, Catalogue_entry__c, Catalogue_Name__c, Catalogue_Department__c, Catalogue_Country__c, Country_Section__c from Account_Participation__c where Account__c =: acc.id and ICEF_Event__c =: ev.Predecessor_event__c];
                    System.debug('accpartobj::'+accpartobj); 
                    if(accpartobj.size() > 0)
                    {  
                        accpart.Catalogue_Country__c = accpartobj[0].Catalogue_Country__c;
                        accpart.Country_Section__c = accpartobj[0].Country_Section__c;  
                        accpart.Catalogue_entry__c = accpartobj[0].Catalogue_entry__c;
                        accpart.Catalogue_Name__c = accpartobj[0].Catalogue_Name__c;
                        accpart.Catalogue_Department__c = accpartobj[0].Catalogue_Department__c;
                        accpart.Catalogue_Street__c = accpartobj[0].Catalogue_Street__c;
                        accpart.Catalogue_Post_Code__c = accpartobj[0].Catalogue_Post_Code__c;
                        accpart.Catalogue_City__c = accpartobj[0].Catalogue_City__c;
                        accpart.Catalogue_State_Province__c = accpartobj[0].Catalogue_State_Province__c;
                        accpart.Catalogue_Tel__c = accpartobj[0].Catalogue_Tel__c;
                        accpart.Catalogue_Fax__c = accpartobj[0].Catalogue_Fax__c;
                        accpart.Catalogue_Email1__c = accpartobj[0].Catalogue_Email1__c;
                        accpart.Catalogue_Email2__c = accpartobj[0].Catalogue_Email2__c;
                        accpart.Catalogue_Website__c = accpartobj[0].Catalogue_Website__c;
                        accpart.Catalogue_Website2__c = accpartobj[0].Catalogue_Website2__c;
                    }   
                    else
                    {
                        accpart.Country_Section__c = acc.Mailing_Country__c;
                        accpart.Catalogue_Name__c = acc.Name;
                        accpart.Catalogue_Department__c = acc.Department__c;
                        accpart.Catalogue_Street__c = acc.Mailing_street__c;
                        accpart.Catalogue_Post_Code__c = acc.Mailing_Post_Code__c;
                        accpart.Catalogue_City__c = acc.Mailing_City__c;
                        accpart.Catalogue_State_Province__c = acc.Mailing_State_Province__c;
                        accpart.Catalogue_Country__c = acc.Mailing_Country__c;
                        accpart.Catalogue_Tel__c = acc.Phone;
                        accpart.Catalogue_Fax__c = acc.Fax;
                        accpart.Catalogue_Email1__c = acc.Email__c;
                        accpart.Catalogue_Website__c = acc.Website;
                        accpart.Catalogue_Website2__c = acc.Website_2__c;      
                    }                
                }
                else
                {
                    accpart.Country_Section__c = acc.Mailing_Country__c;
                    accpart.Catalogue_Name__c = acc.Name;
                    accpart.Catalogue_Department__c = acc.Department__c;
                    accpart.Catalogue_Street__c = acc.Mailing_street__c;
                    accpart.Catalogue_Post_Code__c = acc.Mailing_Post_Code__c;
                    accpart.Catalogue_City__c = acc.Mailing_City__c;
                    accpart.Catalogue_State_Province__c = acc.Mailing_State_Province__c;
                    accpart.Catalogue_Country__c = acc.Mailing_Country__c;
                    accpart.Catalogue_Tel__c = acc.Phone;
                    accpart.Catalogue_Fax__c = acc.Fax;
                    accpart.Catalogue_Email1__c = acc.Email__c;
                    accpart.Catalogue_Website__c = acc.Website;
                    accpart.Catalogue_Website2__c = acc.Website_2__c;                    
                }
                accPartList.add(accpart);
            }
        }
        System.debug('AP:: '+accPartList);
        insert accPartList;
        //PageReference page = new PageReference('/'+ApplicationId);
        PageReference page = new PageReference('/'+acc.id);
        return page;
    }
    
    public class sObjectWrapper
    {
        public Boolean Selected{get; set;}
        public String EventName{get; set;}
        public String eventId{get; set;}
        public String RecordType{get; set;}    
        public String PartStatus{get; set;}
        public String StatusDetails {get; set;}                     //store the selected status detail value
        public boolean disableStatusDetails {get; set;}             //decides to enable or disable the statusDetail field
        public String StatusComment {get; set;}                     //store the status Comment
        public boolean requiredStatusComment {get; set;}             //decides to enable or disable the statusComment field
        public List<SelectOption> RelatedPartStatus {get;set;}      //for status related to record type
        public List<SelectOption> RelatedStatusDetails {get;set;}   //for status related to part status
        public sObjectWrapper()
        {
            Selected = true;
            disableStatusDetails = true;        //initially disabled
            requiredStatusComment = false;
        }
    }
}