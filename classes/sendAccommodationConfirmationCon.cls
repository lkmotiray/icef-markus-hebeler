/*
    Purpose : Controller To sendAccommodationConfirmation Page
*/

public class sendAccommodationConfirmationCon {

    public string subject {get; set;}
    public string body {get; set;}
    public boolean isNoError {get; set;}
    public string recipientEmailId {get; set;}
    public string workshopcc {get; set;}
    public string contactParticipationName {get; set;}
    public boolean attachEmailBodyAsPDF {get;set;}
    
    public List<String> cpMergeFieldList = new List<String>();
    public List<String> cpFieldList = new List<String>();
    public List<String> accommodationFieldList = new List<String>();
    public List<String> accommodationMergeFieldList = new List<String>();
    public string[] recipientEmailIdList = new string[] {};
    
    public List<String> mergeFieldsOtherSObjectFields = new List<String> {};
    public List<String> otherSObjectFields = new List<String> {};
    Accommodation__c accommodationRecord;
    Contact_Participation__c contactParticipationRecord; //changed Accommodation__c
    //String accommodationId,contactPartId,emailTemplateId,orgWideMailId;
    public String accommodationId {get; set;}
    public String contactPartId {get; set;}
    public String emailTemplateId {get; set;}
    public String orgWideMailId {get; set;}
    public EmailTemplate template { get; set; }
    Boolean isHtmlbody;
    EmailTemplate emailTemp;
    //This variable is used for just call executeMethodsOnPageLoad() method (Like pageLoad method)
    public String testVariable {
        get {  
            if(validateIds()){
                // execute initially required methods
                executeMethodsOnPageLoad();
            } 
            return testVariable = 'test';
        }
        set;
    }
    
    /*
        Purpose   : This Controller will be executed on page load
        Parameter : Page Standard Controller
    */
    public sendAccommodationConfirmationCon() {
        
        // set id values
        accommodationId = System.currentPageReference().getParameters().get('id');
        contactPartId = System.currentPageReference().getParameters().get('cpid');              
               
        // Set Subject of email
        Accommodation__c accommodation = new Accommodation__c();
        
        if( String.isNotBlank(accommodationId) ){
            try{
                accommodation = [SELECT Id, ICEF_Event_Full_Name_HC__c,Hotel_Name__c 
                                 FROM Accommodation__c
                                 WHERE Id =: accommodationId
                                 LIMIT 1];                
            }catch(Exception ex){
                System.debug('An error has occurred while fetching Accommodation__c record: ' + ex.getMessage());
            }
        }        
        
        if(accommodation != NULL && accommodation.ICEF_Event_Full_Name_HC__c != NULL){
            subject = accommodation.ICEF_Event_Full_Name_HC__c + ': HotelConfirmation' ;
            
            if( accommodation.Hotel_Name__c != NULL ){
                subject += accommodation.Hotel_Name__c;
            }
        }                
    }
    
    /*
        Purpose   : Validate input ids 
        Return    : Boolean values
    */
    public Boolean validateIds(){
        
        if(getObjectName(accommodationId) != 'Accommodation__c'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                                       'Url is currupted, Please check Accommodation record id'));
            return false;
        }
        if(getObjectName(contactPartId) != 'Contact_Participation__c'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                                       'Url is currupted, Please check Contact Participation record id'));
            return false;
        }
        
        // Check Email address of contactPartition
        Contact_Participation__c contactPartition = new Contact_Participation__c();
        
        try{
            contactPartition = [SELECT Id, Workshop_email__c
                                FROM Contact_Participation__c 
                                WHERE Id =: contactPartId ];
        }catch(Exception ex){
            System.debug('An error has occurred while fetching ContactPartition record: ' + ex.getMessage());
        }  
        
        if( contactPartition != NULL && String.isBlank(contactPartition.Workshop_email__c)  ){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                                       'Workshop Email Address is Null, Please check Contact Participation Workshop Email Address'));
            return false;
        }
        
        return true;
    }

    /*
        Purpose   : Validate input ids 
        Return    : Boolean values
    */  
    public string getObjectName(String idString){
        try {
            Id sObjectId = idString;
            return sObjectId.getSObjectType().
                getDescribe().
                getName();
        }
        catch(Exception e){
            isNoError = false;
            system.debug(e.getMessage());
            return null;
        }        
    }
    
    /*
        Purpose   : Execute innitially required Methods On Page Load
    */
    public void executeMethodsOnPageLoad(){
        
        attachEmailBodyAsPDF = false;
        isHtmlbody = false; 
        isNoError = true;
        
        if(orgWideMailId != '')
                selectedOption = orgWideMailId;
                
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        
        // Populate Accommodation Record
        populateAccommodationRecord(globalDescription);        
        
        // Populate Contact Participation Record
        populateContactPartRecord(globalDescription);
                
       }
    
    /*
        Purpose   : Redirect to Accommodation record
        Parameter : 
    */
    public PageReference cancel(){
        PageReference page = new PageReference('/' + accommodationId);
        page.setRedirect(true);
        return page;
    }
    
    /*
        Purpose   : Populate Contact Participations Record
        Parameter : org globalDescription
    */
    public void populateContactPartRecord( Map<String,Schema.SObjectType> globalDescription ){
        String  query;
        String  allfields = '';
        if( contactPartId == null || 
            contactPartId == 'null' || 
            contactPartId == '')  {
            
            showError();
        }
        else
        {   
            //Access all fields of Contact Participation
            schema.SObjectType contactParticipationType2 = globalDescription.get('Contact_Participation__c'); //changed Accommodation__c
            Schema.DescribeSObjectResult Obj2 = contactParticipationType2.getDescribe();
            Map<String, Schema.SObjectField> objFields2 = Obj2.fields.getMap();
            boolean isFirst = true;         
            for(String fieldName : objFields2.keySet())
            {
                if(isFirst)
                {
                    allfields = fieldName.trim();
                    isFirst = false;
                }
                else
                    allfields = allfields + ', ' + fieldName;
            }
            
            query = 'Select ' + allfields + ' from Contact_Participation__c where Id = \'' + contactPartId + '\' ';  
            System.debug('query::-' + query);
            
            try{
                contactParticipationRecord = Database.query(query);
            }catch(Exception ex){
                System.debug('Error occured while fetching Contact Participation record' + ex.getMessage() );
            }
            
            if( contactParticipationRecord  != NULL ){
             
                contactParticipationName = contactParticipationRecord.Name; 
                
                if(contactParticipationRecord.Workshop_email__c != null)
                {
                    recipientEmailId = contactParticipationRecord.Workshop_email__c;
                    
                    if(contactParticipationRecord.Workshop_CC_Mailers__c != null){
                        workshopcc = contactParticipationRecord.Workshop_CC_Mailers__c;  
                    }
                    
                    if(workshopcc != null){
                        recipientEmailIdList.add(workshopcc);
                    	recipientEmailIdList.add(recipientEmailId);                        
                    }
                }
                else
                {
                    Contact contact = [Select Id, Email from Contact where id =: contactParticipationRecord.Contact__c];
                    recipientEmailId = contact.Email;
                    recipientEmailIdList.add(recipientEmailId);
                }
            }
        }
    }
    
    /*
        Purpose   : Populate Accommodation Record
        Parameter : org globalDescription
    */
    public void populateAccommodationRecord( Map<String,Schema.SObjectType> globalDescription ){
        string accommodationAllfields = '';
        string query;
        
        if( accommodationId == null || 
            accommodationId == 'null' || 
            accommodationId == '')
        {
            showError();
        }
        else
        {   
            //Access all fields Accommodation__c
            schema.SObjectType accommodationType2 = globalDescription.get('Accommodation__c'); 
            Schema.DescribeSObjectResult accommodationObj = accommodationType2.getDescribe();
            Map<String, Schema.SObjectField> accommodationFields = accommodationObj.fields.getMap(); 
            boolean isFirst = true; 
            
            for(String fieldName : accommodationFields.keySet())
            {
                if(isFirst)
                {
                    accommodationAllfields = fieldName.trim();
                    isFirst = false;
                }
                else
                    accommodationAllfields = accommodationAllfields + ', ' + fieldName;
            }
            
            query = 'Select ' + accommodationAllfields + ' from Accommodation__c where Id = \'' + accommodationId + '\' ';        
            accommodationRecord = Database.query(query);        
        }
    }
    
    /*
        Purpose   : get list of select option of org wide email addresses 
    */
    public List<SelectOption> getItems()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        User u = [Select Name, Email, Id from User where Id =: UserInfo.getUSerId()];
        String loggedInUser = '"'+ u.Name + '" <'+ u.Email + '>';
        options.add(new SelectOption(loggedInUser, loggedInUser)); 
        
        List<OrgWideEmailAddress> owa = [select Address, DisplayName from OrgWideEmailAddress];
        
        for(integer i = 0; i < owa.size(); i++)
        {            
            string option = '"'+ owa[i].DisplayName + '" <'+ owa[i].Address + '>';
            options.add(new SelectOption(option, option));
        }   
             
        return options;
    }
  
    /*
        Purpose : set value of selectOption of orgwide email address.
    */
    public String selectedOption
    {
       get
        {
           if(selectedOption==null)
           {
               User u = [Select Email, Id from User where Id =: UserInfo.getUSerId()];
               selectedOption = u.Email;    
               System.debug('selectedOption  1::'+selectedOption);           
           }
           return selectedOption;
        }
    set;
    } 
    
    /*
    *   @Purpose : Send an email to Contact Partition's Workshop Email field.
    *   @Parameters :
    */ 
    public pageReference sendEmail(){
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        Contact contactRec;
        EmailTemplate template;
        Accommodation__c accommodation = new Accommodation__c();
        Contact_Participation__c contactPartition = new Contact_Participation__c();
        
        // Get Contact Partition record
        try{
            contactPartition = [SELECT Id, Workshop_email__c, Workshop_CC_Mailers__c, Contact__c
                                FROM Contact_Participation__c 
                                WHERE Id =: contactPartId ];
        }catch(Exception ex){
            System.debug('An error has occurred while fetching ContactPartition record: ' + ex.getMessage());
        }  
        
        String targetEmailAddress= '';
        
        if( contactPartition != NULL && String.isNotBlank(contactPartition.Workshop_email__c)  ){
            targetEmailAddress = contactPartition.Workshop_email__c;
        }
		
		String ccEmailAddress ;
		
        if( contactPartition != NULL && String.isNotBlank(contactPartition.Workshop_CC_Mailers__c)  ){
            ccEmailAddress = contactPartition.Workshop_CC_Mailers__c;
        }
        
        if( String.isNotBlank(targetEmailAddress) ){     
            if(accommodationId != Null){
                try{
                    // Get single Contact record to set as a TargetObjectId for add Email Template.
                    contactRec = [SELECT Id, Email 
                                  FROM Contact
                                  LIMIT 1];
                } catch(DmlException e) {
                    System.debug('An error has occurred while fetching Contact record: ' + e.getMessage());
                }
                
                System.debug('contactRec::' + contactRec);
                
                try{
                    //Get single accommodation record to set as a whatId
                    accommodation = [SELECT Id
                                     FROM Accommodation__c
                                     WHERE ID =: accommodationId];
                } catch(DmlException e) {
                    System.debug('An error has occurred while fetching accommodation record: ' + e.getMessage());
                }
                
                System.debug('accommodation::' + accommodation);
                
                try{
                    //Get template that is used while sending email
                    template = [SELECT Id,Subject FROM EmailTemplate 
                                WHERE DeveloperName = 'HotelConfirmationWithSignature'
                                LIMIT 1];
                } catch(DmlException e) {
                    System.debug('An error has occurred while fetching Template record: ' + e.getMessage());
                }            
                
                
                System.debug('template::' + template);
                
                if(String.isNotBlank(template.Id) && String.isNotBlank(accommodation.Id) && String.isNotBlank(contactRec.Id))  {
                    
                    List<String> ccEmailAddressList = new List<String>();
                    
                    if( String.isNotBlank(ccEmailAddress) ){
                        ccEmailAddressList.add(ccEmailAddress);
                    }
                    
                    // Call createEmail() method to create emails and add into emailList
                    emailList.add(createEmail(template.Id, accommodation.Id, contactPartition.Contact__c, 
                                              new List<String>{ targetEmailAddress }, ccEmailAddressList ));
                    
                    System.debug('emailList::' + emailList);
                    
                    if(!emailList.isEmpty()){
                        try{
                            // Send all emails in the master list
                            system.debug('Activity target id'+ emailList[0].getTargetObjectId());
                            system.debug('Activity'+ emailList[0].getSaveAsActivity());
                            Messaging.sendEmail(emailList);
                            System.debug('After sendEmail() method');
                        } catch(Exception e) {
                            System.debug('An error has occurred while Sending email: ' + e.getMessage());
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                                                       'Sorry, Failed to send Accommodation confirmation mail '));
                            return null;
                        }
                        
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm, 
                                                                   'Confirmation Email Sent Successfully  <a href="/'+contactPartId+'" > Go to Contact Participation   </a><a href="/'+accommodationId+'" > Go to Accommodation</a>'));
                        return null;
                    }
                }
            }
        }
        else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                                       'Sorry unable to send mail. No workshop and contact email address is available'));
            return null;           
        }
        
        return null;
    }    
     
   /*
    *   @Purpose : To create Email with provided data.
    *   @Parameters : templateId [Id], accommodationId [Id], contactId[Id], targetEmail [String]
    *   @Return Type : Messaging.SingleEmailMessage
    */ 
    Public Messaging.SingleEmailMessage createEmail(Id templateId, Id accommodationId, Id targetId, List<String> targetEmails, List<String> ccAddressList){
        
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail =new Messaging.SingleEmailMessage();
        
        if(!targetEmails.isEmpty()){
            // Step 2: Set target mail
            mail.setToAddresses(targetEmails); 
            
            // Step 3: Set who the email is sent from
            mail.setReplyTo(System.UserInfo.getUserEmail());
            
            // Step 4: Set template
            mail.setTemplateId(templateId);
            
            // Step 5: Set whatId
            mail.setWhatId(accommodationId);
            
            // Step 6: Set setTargetObjectId
            mail.setTargetObjectId(targetId);
          
            // Step 7: Restict sending email to Contact
            mail.setTreatTargetObjectAsRecipient(false);
			
			// setCcAddresses
			if( !ccAddressList.isEmpty() ){
				mail.setCcAddresses( ccAddressList );
			}
            
            mail.saveAsActivity = true;
            mail.setUseSignature(true); 
            
            //set organisation wide email address        
            
            selectedOption = selectedOption.subString(selectedOption.indexOf('<')+1, selectedOption.indexOf('>'));
            
            List<OrgWideEmailAddress> owa = [SELECT id, Address 
                                             FROM OrgWideEmailAddress 
                                             WHERE Address =: selectedOption];
            if(owa.size() != 0)
                mail.setOrgWideEmailAddressId(owa[0].id);
            
            if(attachEmailBodyAsPDF){
                Messaging.EmailFileAttachment attach = generatePDFAttachment(accommodationId);      
                if( attach != NULL ){
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });
                }               
            }           
        }
       return mail ;
    }
    
     /*
        Purpose : Get Pdf Content.
     */
    public Messaging.EmailFileAttachment generatePDFAttachment(Id id){  
        // Reference the attachment page and pass in the Id
        PageReference pdf = Page.AccommodationDetails;
        pdf.getParameters().put('id',(String)id); 
        pdf.setRedirect(true);
        
        if(!test.isRunningTest()){
            // Take the PDF content
            Blob pdfContent = pdf.getContent();
            //setting name of attachment
            String workshopFullName = subject + ' '+'- Confirmation'; 
            
            // Create the email attachment  
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();        
            efa.setFileName(workshopFullName + '.pdf');
            efa.setBody(pdfContent);
            
            return efa;
        }
        
        return null;
    }
    
    /*
        Purpose : Used to show error.
    */
    Public PageReference showError()
    {
        isNoError = false;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 
                                                   'You may have changed url.. Id parameter is required in Url'));
        return null;
    } 
    
}