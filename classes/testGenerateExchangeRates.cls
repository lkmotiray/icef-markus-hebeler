/*  Test Class
    @purpose-This test class is developed to test the generateExchangeRates class
    @created_date-23-04-13
    @last_modified-[23-04-13]    
*/


@isTest(seeAllData = true)
public class testGenerateExchangeRates{
   
    
    /*  Function
    @purpose-This function is used to test methods of generateExchangeRates class       
    @last_modified-[23-04-13]  
    */
    static testmethod void test_generateExchangeRates()
    {
        string htmlDoc = '<li><div><span class="xml">&nbsp;</span>XML file available for parsing: http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml</div></li>';
        
        generateExchangeRates.unitTestHtml = htmlDoc;
        
        string xml = '<?xml version="1.0" encoding="UTF-8"?>';
        xml += '<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01" xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">';
        xml += '<gesmes:subject>Reference rates</gesmes:subject>';
        xml += '<gesmes:Sender>';
        xml += '<gesmes:name>European Central Bank</gesmes:name>';
        xml += '</gesmes:Sender>';
        xml += '<Cube>';
        xml += '<Cube time=\'';
        xml += System.Today();
        xml += '\'>';
        xml += '<Cube currency=\'USD\' rate=\'1.3042\'/>';            
        xml += '<Cube currency=\'GBP\' rate=\'0.82810\'/>';         
        xml += '<Cube currency=\'AUD\' rate=\'1.2192\'/>';
        xml += '<Cube currency=\'BRL\' rate=\'2.2549\'/>';
        xml += '<Cube currency=\'CAD\' rate=\'1.3019\'/>';
        xml += '</Cube>';
        xml += '</Cube>';
        xml += '</gesmes:Envelope>';  
        
        generateExchangeRates.UnitTestXml = xml;    
            
                
        generateExchangeRates.insertExchageRates();
        generateExchangeRates.scheduleUpdateExchangeRates();
    }
    
}