@isTest
public class mergeAccountTriggerTest {
    @isTest static void mergeAccountsWithMembershipInLoser(){
        Test.startTest();

        List<Account> accts = DataFactory.createTestAgents(2);
        List<Account> assocs = DataFactory.createTestAssociations(1, 'Association Membership');
        
        insert accts;
        insert assocs;
                
        Account master = [SELECT Id, Name FROM Account WHERE Name = 'Test Agent 1'];
        Account loser = [SELECT Id, Name FROM Account WHERE Name = 'Test Agent 0'];
        
        Membership__c membership = DataFactory.createTestMembership(loser, assocs[0]);
        insert membership;
        
        merge master loser;

        Test.stopTest();
        
        List<Membership__c> mems = [SELECT Id, Nudge__c, Member__c FROM Membership__c WHERE Member__c = :master.Id];
              
        System.assertEquals(1, mems.size());
        System.assertNotEquals(null, mems[0].Nudge__c);
    }
    
    @isTest static void mergeAccountsWithMembershipInMaster(){
        Test.startTest();

        List<Account> accts = DataFactory.createTestAgents(2);
        List<Account> assocs = DataFactory.createTestAssociations(1, 'Association Membership');
        
        insert accts;
        insert assocs;
                
        Account master = [SELECT Id, Name FROM Account WHERE Name = 'Test Agent 1'];
        Account loser = [SELECT Id, Name FROM Account WHERE Name = 'Test Agent 0'];
        
        Membership__c membership = DataFactory.createTestMembership(master, assocs[0]);
        insert membership;
        
        merge master loser;

        Test.stopTest();
        
        List<Membership__c> mems = [SELECT Id, Nudge__c, Member__c FROM Membership__c WHERE Member__c = :master.Id];
              
        System.assertEquals(1, mems.size());
        System.assertNotEquals(null, mems[0].Nudge__c);
    }
    
    @isTest static void mergeAccountsWithoutMemberships(){
        //make sure Accounts without memberships still can be merged without throwing an error: 
        // https://developer.salesforce.com/forums/?id=906F000000092gOIAQ
        List<Account> accts = DataFactory.createTestAgents(2);
       	
        Test.startTest();
        
        insert accts;
        Account master = [SELECT Id, Name FROM Account WHERE Name = 'Test Agent 1'];
        Account loser = [SELECT Id, Name FROM Account WHERE Name = 'Test Agent 0'];

		Database.MergeResult result = Database.merge(master, loser);      
        
        Test.stopTest();
        
		System.assertEquals(true, result.isSuccess());       
    }
}