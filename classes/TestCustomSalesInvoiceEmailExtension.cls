/**
 * Test class for CustomSalesInvoiceEmailExtension apex class
 * @author        Jasmine J
 * @createdDate   10/13/2015
 * 
 */

@isTest(SeeAllData = true)
public class TestCustomSalesInvoiceEmailExtension {
    
    private static void setupTestData() {
        
        List<Account> accountList  = [SELECT ID FROM ACCOUNT LIMIT 1];
        
        List<c2g__codaUserCompany__c> listComapnies = [SELECT Id, Name, c2g__Company__c, c2g__User__c 
                                                       FROM c2g__codaUserCompany__c
                                                       WHERE c2g__User__c =: UserInfo.getUserId()];
        
        List<c2g__codaAccountingCurrency__c> accountCurrencyList = [SELECT ID FROM c2g__codaAccountingCurrency__c
                                                                    WHERE c2g__OwnerCompany__c =: listComapnies[0].c2g__Company__c LIMIT 1];
        
        c2g__codaInvoice__c invoiceToInsert = new c2g__codaInvoice__c(); 
        invoiceToInsert.c2g__InvoiceDate__c = Date.newInstance(2017,2,17);
        invoiceToInsert.c2g__DueDate__c = Date.newInstance(2017,3,17);
        invoiceToInsert.c2g__Account__c = accountList[0].Id;
        invoiceToInsert.c2g__InvoiceCurrency__c = accountCurrencyList[0].id;
        
        INSERT invoiceToInsert;
        
        c2g__codaInvoiceInstallmentLineItem__c installMentLineItem = new c2g__codaInvoiceInstallmentLineItem__c();
        installMentLineItem.c2g__Amount__c = 0;
        installMentLineItem.c2g__DueDate__c = system.today();
        installMentLineItem.c2g__Invoice__c = invoiceToInsert.Id;
    
        INSERT installMentLineItem;
    } 
     
    /**
     * Function to test the send Email functionality of the Page and Controller
     *    
     */    
     static testMethod void testMethod1() {        
        // Assign email ids to send test email
        String strTo = 'to@test.com';
        String strFrom = 'from@test.com';
        String strAdditionalTo = 'additionalTo@test.com';
        String strCC = 'cc@test.com';
        String strBCC = 'bcc@test.com';
        String subject = 'Email: Testing CustomSalesInvoiceEmail Page Email';
        
        Test.startTest();
        
        // Refer the page to test 
        PageReference pageRef = Page.CustomSalesInvoiceEmail;            
        Test.setCurrentPage(pageRef);  
        
         // Get the Sales Invoice record to test
        c2g__codaInvoice__c testSalesInvoice = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        System.currentPageReference().getParameters().put('Id', testSalesInvoice.Id);
        
        // Set the email template id to pass as parameter to the url
        EmailTemplate testEmailTemplate = [SELECT Id FROM EmailTemplate LIMIT 1];
        System.currentPageReference().getParameters().put('emailTemplateId', '00X20000001hJym');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        CustomSalesInvoiceEmailExtension controllerInstance = new CustomSalesInvoiceEmailExtension(sc);    
        
        controllerInstance.prepareSendEMail();
        //controllerInstance.getEmailAttachments();
        controllerInstance.getSelectedToUsers();
        controllerInstance.getItems();
        controllerInstance.getFromList();   
        controllerInstance.strEmailFromUser = UserInfo.getUserId();
        controllerInstance.strAdditionalTo = strAdditionalTo;
        controllerInstance.strCC = strCC;
        controllerInstance.strBCC = strBCC;
        controllerInstance.sendEmail(); 
        
        controllerInstance.prepareSendEMail();
        //controllerInstance.getEmailAttachments();
        controllerInstance.getFromList();   
        controllerInstance.strEmailFromUser = UserInfo.getUserId();
        controllerInstance.strAdditionalTo = strAdditionalTo;
        controllerInstance.strCC = strCC;
        controllerInstance.subject = 
        controllerInstance.strBCC = strBCC;
        controllerInstance.deleteAttachment();
        controllerInstance.sendEmail();      
        
        List<Task> insertedTask = [SELECT Id, WhatId, Subject
                                   FROM Task 
                                   WHERE WhatId = :testSalesInvoice.Id];
        //System.assertNotEquals(0, insertedTask.size());
        //System.assert(insertedTask.get(0).Subject != null);             
        Test.stopTest();
    }
    
    static testMethod void testMethod2(){
                
        Test.startTest();
        
        // Refer the page to test 
        PageReference pageRef = Page.CustomSalesInvoiceEmail;            
        Test.setCurrentPage(pageRef);  
        
         // Get the Sales Invoice record to test
        c2g__codaInvoice__c testSalesInvoice = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        System.currentPageReference().getParameters().put('Id', testSalesInvoice.Id);
        
        // Set the email template id to pass as parameter to the url
        EmailTemplate testEmailTemplate = [SELECT Id FROM EmailTemplate LIMIT 1];
        System.currentPageReference().getParameters().put('emailTemplateId', '00X20000001hJym');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        CustomSalesInvoiceEmailExtension controllerInstance = new CustomSalesInvoiceEmailExtension(sc);    
        
        controllerInstance.popupType = 'txtCC';
        controllerInstance.getRecordsInSelectList();
        controllerInstance.getItems();
        controllerInstance.txtSelectedCriteria = 'Users';
        
        controllerInstance.popupType = 'txtBCC';
        controllerInstance.getRecordsInSelectList();
        System.assertEquals(controllerInstance.txtSelectedCriteria, 'Users');
        
        Test.stopTest();
    }
    
     static testMethod void testActivityManager() { 
         List<c2g__codaInvoice__c> listOfcodaInvoice = [SELECT Id, Name FROM c2g__codaInvoice__c 
                                      Where c2g__Transaction__r.owner.Name IN ('FF ICEF GmbH','FF ICEF Asia Pacific') 
                                         limit 2 ];
          List<String> listOfcodaInvoiceIds = new List<String>();                             
         for(c2g__codaInvoice__c saleInv : listOfcodaInvoice ){
             listOfcodaInvoiceIds.add(saleInv.id);
         }                                
        ActivityManager.createActivityHistory(listOfcodaInvoiceIds);
        List<Task> tasklist = [ Select Id,WhatId 
                                From Task 
                                Where WhatId IN :listOfcodaInvoiceIds];
        system.debug('tasklist::'+tasklist);                        
        System.assert(tasklist.size()>1, 'Acivity Created');    
     }
     
    static testmethod void testAttachFile() {
        //--Set current page
        Test.setCurrentPageReference(new PageReference('Page.CustomSalesInvoiceEmail'));
        // Get the Sales Invoice record to test
        c2g__codaInvoice__c testSalesInvoice = [SELECT Id FROM c2g__codaInvoice__c LIMIT 1];
        System.currentPageReference().getParameters().put('Id', testSalesInvoice.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(testSalesInvoice);
        CustomSalesInvoiceEmailExtension controllerInstance = new CustomSalesInvoiceEmailExtension(sc);            
        //--Verify action function methods
        String strJson = '';
        Apexpages.currentPage().getParameters().put('fileInstance',strJson);
        controllerInstance.getSelectedEmailAttachment();
        
        CustomSalesInvoiceEmailExtension.EmailAttachmentWrapper emaiAttaWrap = new CustomSalesInvoiceEmailExtension.EmailAttachmentWrapper();
        emaiAttaWrap.getfileSize();
        Document documentRec = [ SELECT Id, Name, bodyLength FROM Document LIMIT 1 ];
        
        String strJson1 = '[{"Id":"'+
                            documentRec.Id+
                            '","Name":"'+
                            documentRec.Name+
                            '","size":"'+
                            documentRec.bodyLength+
                            '","Type":"Document"}]';
        
        Apexpages.currentPage().getParameters().put('fileInstance',strJson1);
        controllerInstance.getSelectedEmailAttachment();
        System.assertEquals(controllerInstance.mapEmailAttachments.size(), 1); 
    }
    
    static testmethod void testcodaInvoiceExtension() {
        
        TestCustomSalesInvoiceEmailExtension.setupTestData();
        Test.setCurrentPageReference(new PageReference('Page.CustomSalesInvoiceEmail'));
        Apexpages.currentPage().getParameters().put('name','test file');
        
        List<c2g__codaInvoice__c> invoiceList = [SELECT Id, c2g__InvoiceTotal__c, 
                                                       c2g__OutstandingValue__c,
                                                       ( SELECT Id, c2g__Amount__c 
                                                          FROM c2g__InvoiceInstallmentLineItems__r)
                                                FROM c2g__codaInvoice__c 
                                                WHERE c2g__InvoiceTotal__c != NULL 
                                                    AND c2g__OutstandingValue__c != NULL
                                                    AND Id IN 
                                                        ( SELECT c2g__Invoice__c 
                                                          FROM c2g__codaInvoiceInstallmentLineItem__c)
                                                ORDER BY c2g__Account__c ASC
                                                LIMIT 10];
        
        for( c2g__codaInvoice__c invoice : invoiceList ){
            Decimal difference = invoice.c2g__InvoiceTotal__c - invoice.c2g__OutstandingValue__c;
            
            for( c2g__codaInvoiceInstallmentLineItem__c item : invoice.c2g__InvoiceInstallmentLineItems__r){
                if( item.c2g__Amount__c <= difference ){
                    ApexPages.StandardController sc1 = new ApexPages.StandardController( invoice );
                    codaInvoiceExtension extension1 = new codaInvoiceExtension(sc1);
                }else if( item.c2g__Amount__c >= difference && difference != 0 ){
                    ApexPages.StandardController sc1 = new ApexPages.StandardController( invoice );
                    codaInvoiceExtension extension1 = new codaInvoiceExtension(sc1);
                }else{
                    ApexPages.StandardController sc1 = new ApexPages.StandardController( invoice );
                    codaInvoiceExtension extension1 = new codaInvoiceExtension(sc1);
                }
            }
        }
    }
    
    static testmethod void testcodaCreditNoteExtension() {
        Test.setCurrentPageReference(new PageReference('Page.CreditNotePDF'));
        Apexpages.currentPage().getParameters().put('name','test file');
        c2g__codaCreditNote__c testCreditNote = [SELECT Id FROM c2g__codaCreditNote__c LIMIT 1];
        ApexPages.StandardController sc = new ApexPages.StandardController( testCreditNote );
        codaCreditNoteExtension extension = new codaCreditNoteExtension(sc);  
    }
}