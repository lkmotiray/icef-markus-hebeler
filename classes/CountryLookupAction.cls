public with sharing class CountryLookupAction {
    @InvocableMethod(label='Lead Country Lookup' description='updates Country Lookup field in Lead according to Country given in picklist.')
    public static List<Id> countryLookup(List<ID> leadIds){
        List<Lead> leadsToUpdate = new List<Lead>();
        List<Id> leadsToReturn = new List<Id>();
 		List<Lead> leadList = [SELECT Id, Country__c, Country_Lookup__c FROM Lead WHERE Id IN :leadIds];  
        System.debug('CountryLookupAction, initial Lead List: ' + leadList);
        for (Lead led : leadList){
            if(led.Country__c != '' && getIdFromString(led.Country__c) != null){
           		led.Country_Lookup__c = getIdFromString(led.Country__c);
        		leadsToUpdate.add(led);
            }
            leadsToReturn.add(led.Id);
        }
        Database.update(leadsToUpdate);
        return leadsToReturn;
    }
    
    private static ID getIdFromString(String countryName){
        List<Country__c> countryIds = [SELECT Id FROM Country__c WHERE Name = :countryName LIMIT 1];
        ID countryId = null;
        if(countryIds.size() == 1){
        	countryId = countryIds[0].Id;
        }
        return countryId;
    }

}