/*  Class
    @purpose-This class is developed to implement the HttpCalloutMock interface to enable mockcallout
    @created_date-09-08-13
    @last_modified-[19-08-13]    
*/
global class mockHttpExchangeRatesRBA implements HttpCalloutMock{
    
    /*  Function
    @purpose-This function is used to set and send the dummy Http response
    @return-HttpResponse object
    @last_modified-[19-08-13]
	*/
	global HTTPResponse respond(HTTPRequest req) {       
       
        // Create a dummy response
      	String xml='<?xml version="1.0" encoding="UTF-8"?>';
        xml+='<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cb="http://www.cbwiki.net/wiki/index.php/Specification_1.2/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rba="http://www.rba.gov.au/statistics/frequency/exchange-rates.html" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://purl.org/rss/1.0/" xsi:schemaLocation="http://www.w3.org/1999/02/22-rdf-syntax-ns# rdf.xsd">';
        xml+='<item rdf:about="http://www.rba.gov.au/statistics/frequency/exchange-rates.html#USD">';
        xml+='<cb:value>0.9216</cb:value>';
        xml+='<cb:period>2013-09-18</cb:period>';
        xml+='<cb:targetCurrency>USD</cb:targetCurrency>';
        xml+='</item>';        
        xml+='<item rdf:about="http://www.rba.gov.au/statistics/frequency/exchange-rates.html#CNY">';
        xml+='<cb:value>5.6418</cb:value>';
        xml+='<cb:period>2013-09-18</cb:period>';
        xml+='<cb:targetCurrency>CNY</cb:targetCurrency>';
        xml+='</item>';
        xml+='<item rdf:about="http://www.rba.gov.au/statistics/frequency/exchange-rates.html#EUR">';
        xml+='<cb:value>0.6915</cb:value>';
        xml+='<cb:period>2013-09-18</cb:period>';
        xml+='<cb:targetCurrency>EUR</cb:targetCurrency>';
        xml+='</item>';
        xml+='<item rdf:about="http://www.rba.gov.au/statistics/frequency/exchange-rates.html#USD">';
        xml+='<cb:value>0.9216</cb:value>';
        xml+='<cb:period>2013-09-18</cb:period>';
        xml+='<cb:targetCurrency>USD</cb:targetCurrency>';
        xml+='</item>';
        xml+='</rdf:RDF>';
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(xml);
        res.setStatusCode(200);
        return res;
    }
}