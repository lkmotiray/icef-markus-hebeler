@isTest
public class setCountryLookupOnLeadTest {
	
    public static testMethod void testInsertLeadValidCountry(){
        
    	String countryId = DataFactory.createTestCountry();
        Lead testLead = DataFactory.createTestLead('Educator'); 
        Country__c country = [SELECT Id, Name FROM Country__c WHERE Id = :countryId LIMIT 1];
        testLead.Company = 'Test Valid Country Company';
        testLead.Country__c = country.Name;
        
        Test.startTest();
        insert testLead;
        Test.stopTest();
        
        Lead checkLead = [SELECT Id, Country_Lookup__c FROM Lead WHERE Company = 'Test Valid Country Company' LIMIT 1];
        
        System.assertEquals(country.Id, checkLead.Country_Lookup__c);
    }
    
    public static testMethod void testInsertLeadEmptyCountry(){
        
        Lead testLead = DataFactory.createTestLead('Educator'); 
        testLead.Company = 'Test Empty Country Company';
        
        Test.startTest();
        insert testLead;
        Test.stopTest();
        
        Lead checkLead = [SELECT Id, Country_Lookup__c FROM Lead WHERE Company = 'Test Empty Country Company' LIMIT 1];
        
        System.assertEquals(null, checkLead.Country_Lookup__c);
    }
    
    public static testMethod void testUpdateLeadValidCountry(){
        
    	String countryId = DataFactory.createTestCountry();
        Lead testLead = DataFactory.createTestLead('Educator'); 
        Country__c country = [SELECT Id, Name FROM Country__c WHERE Id = :countryId LIMIT 1];
        testLead.Company = 'Test Valid Country Company';
        
        Test.startTest();
        insert testLead;
        Lead checkLead = [SELECT Id, Country_Lookup__c FROM Lead WHERE Company = 'Test Valid Country Company' LIMIT 1];
        checkLead.Country__c = country.Name;
        update checkLead;
        Test.stopTest();
        
        Lead realCheckLead = [SELECT Id, Country_Lookup__c FROM Lead WHERE Company = 'Test Valid Country Company' LIMIT 1];
        
        System.assertEquals(country.Id, realCheckLead.Country_Lookup__c);
    }

    public static testMethod void testUpdateLeadUnsinnCountry(){
        
    	Lead testLead = DataFactory.createTestLead('Educator'); 
        testLead.Company = 'Test Unsinn Country Company';
        
        Test.startTest();
        insert testLead;
        Lead checkLead = [SELECT Id, Country__c, Country_Lookup__c FROM Lead WHERE Company = 'Test Unsinn Country Company' LIMIT 1];
        checkLead.Country__c = 'Unsinn';
        update checkLead;
        Test.stopTest();
        
        Lead realCheckLead = [SELECT Id, Country_Lookup__c FROM Lead WHERE Company = 'Test Unsinn Country Company' LIMIT 1];
        
        System.assertEquals(null, realCheckLead.Country_Lookup__c);
    }

    
}