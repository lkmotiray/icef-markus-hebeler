@isTest
public class UpdateFieldARMTriggerTest
{
    public static testMethod void testTrigger()
    {
        Country__c con = new Country__c(Name='USA',Agent_Relationship_Manager__c = Userinfo.getUserId());
        insert con;
    
        RecordType RT = [select Id,name from RecordType where Name ='Agent' and SobjectType = 'Account' limit 1];
        
        Account acc = new Account(ARM__c = Userinfo.getUserId(),Name='Testing acc', Status__c='Active', Mailing_State_Province__c='CA', Mailing_Country__c=con.Id, account_owner_email__c='aaa.bbb@gmail.com', Email__c='aaa.bbb@gmail.com');
        insert acc;
        
        system.debug('Account.... '+acc.ARM__c);
        acc.ARM__c = null;
        update acc;
    }
}