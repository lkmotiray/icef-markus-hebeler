/* 
 * @Purpose : to convert opportunity amount
 */
public class OpportunityTriggerHandler {
    
    public void updateCurrenency(List<Opportunity> listNewOpportunies) {
        
        if(listNewOpportunies != null && !listNewOpportunies.isEmpty()){
            map<String, Decimal> mapIsoCodeWithRate =mapIsoCodeWithRate();
            
            for(Opportunity opportunityToConvertCurrency : listNewOpportunies) {
                // convert opportunity amount to EUR                
                
                opportunityToConvertCurrency.Converted_Amount__c = currencyConverter(opportunityToConvertCurrency.CurrencyIsoCode, 
                                                                                     'EUR', opportunityToConvertCurrency.Amount,
                                                                                     mapIsoCodeWithRate);  
                opportunityToConvertCurrency.Added_CF_credit_converted__c = currencyConverter(opportunityToConvertCurrency.CurrencyIsoCode, 
                                                                                       'EUR', opportunityToConvertCurrency.Added_Credit__c,
                                                                                        mapIsoCodeWithRate);      
                                                                            
            }
        }
    }    
    
    // @Purpse : to create currency conversion map 
    private map<String, Decimal>mapIsoCodeWithRate(){
        map<String, Decimal> mapIsoCodeWithRate = new map<String, Decimal>();
        
        // get Company name to find CUreency conversion rates 
        Current_Company_For_Organization__c currentCompany = Current_Company_For_Organization__c.getInstance(UserInfo.getProfileId());
        
        if(currentCompany!= null && String.isNotBlank(currentCompany.Company_Name__c)){
            List<c2g__codaAccountingCurrency__c>listAccountCurrencies = new  List<c2g__codaAccountingCurrency__c>();
            listAccountCurrencies = fetchAccountCurrenciesWithRate(currentCompany.Company_Name__c);
            
            for (c2g__codaAccountingCurrency__c accountCurrency: listAccountCurrencies ) {           
                
                if(accountCurrency.c2g__ExchangeRates__r.size() >0 ){
                    
                    if(accountCurrency.c2g__ExchangeRates__r[0].c2g__Rate__c != null) {
                        // put currency rate                   
                        mapIsoCodeWithRate.put(accountCurrency.Name, accountCurrency.c2g__ExchangeRates__r[0].c2g__Rate__c);
                        continue; 
                    }
                    
                }   
                
                // if currency rate not found put 1              
                mapIsoCodeWithRate.put(accountCurrency.Name, 1);            
            }
            
        }
        else {
            System.debug('Please Enter value in Custom Setting : Current Company For Organization.');
        }                
        return  mapIsoCodeWithRate ;
                
    }
    
    private List<c2g__codaAccountingCurrency__c> fetchAccountCurrenciesWithRate(String currencyCompany) {
        
        List<c2g__codaAccountingCurrency__c>listAccountCurrencies = new  List<c2g__codaAccountingCurrency__c>();
        if(String.isNotBlank(currencyCompany)) {
            listAccountCurrencies = [SELECT Id, Name, 
                                         (SELECT Id, c2g__Rate__c
                                          FROM c2g__ExchangeRates__r
                                          ORDER BY CreatedDate DESC
                                          LIMIT 1)                                 
                                     FROM c2g__codaAccountingCurrency__c
                                     WHERE c2g__OwnerCompany__r.Name = :currencyCompany];
            
            
        }
        return listAccountCurrencies; 
    }
    // @purpose : Convert Source Currnecy to Target Currency ISO code  
    private Decimal currencyConverter(String strSourceIsoCode, String strTargetIsoCode, Decimal decSourceAmount,
                                      map<String, Decimal>mapIsoCodeWithRate )
    {
        Decimal  convertedAmount = 0;
        if(mapIsoCodeWithRate.containsKey(strSourceIsoCode) && mapIsoCodeWithRate.containsKey(strTargetIsoCode)) {                    
            // convert currency
            try {
                convertedAmount = ((decSourceAmount/mapIsoCodeWithRate.get(strSourceIsoCode)) * mapIsoCodeWithRate.get(strTargetIsoCode)).setScale(2);              
            }
            catch(Exception ex){
                convertedAmount = 0;
            }
        }        
        return convertedAmount; 
    }
}