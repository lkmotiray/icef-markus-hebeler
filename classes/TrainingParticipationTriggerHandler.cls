/******************************************************************************************
Developer   = Dreamwares (India)
Description = Updates the value of TP Owner field
Date        = 16-01-2015 
******************************************************************************************/

public class TrainingParticipationTriggerHandler
{
    /*
    Method Name - afterInsert
    Parameters  - Map of Id and Training Partition record
    Return      - none
    */
    public void afterInsert(Map<Id,Training_Participation__c> mapNewTrainingParticipation)
    {
        //get ARM field value of Account for all new Training Participations
        List<Training_Participation__c> lstNewTrainingParticipation = new List<Training_Participation__c>([SELECT Id,Name,Account__r.ARM__c,TP_Owner__c 
                                                                                                        FROM Training_Participation__c 
                                                                                                        WHERE Id IN :mapNewTrainingParticipation.keySet()]);
                  
        List<Training_Participation__c> updateList = new List<Training_Participation__c>();
        
        //assign value of Accounts ARM field to Training Participations TP Owner field
        for(Training_Participation__c TrainingPart :lstNewTrainingParticipation)
        {
            if(TrainingPart.Account__r.ARM__c != null && TrainingPart.TP_Owner__c == null)
            {
                TrainingPart.TP_Owner__c = TrainingPart.Account__r.ARM__c;
                updateList.add(TrainingPart);
            }
        }
        
        //update all modified training participations
        try
        {
            update(updateList);
        }
        Catch(Exception e)
        {
            System.debug('ERROR : ' + e.getMessage());
        }
    }
}