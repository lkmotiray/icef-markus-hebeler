/*  Scheduler Class
    @purpose-This scheduler class is developed to schedule an update for the exchange rates on regular interval 
    @created_date-09-08-13
    @last_modified-[12-08-13]    
*/

global class scheduleGenerateExchangeRatesRBA implements Schedulable{
    //implemented this execute() in order to implement the schedulable interface 
    global void execute(SchedulableContext sc)
    {
        confirmCurrentCompany();
        generateExchangeRatesRBA.insertExchageRates();          //static method insertExchangeRates is called
    }
    
    public static void confirmCurrentCompany() {
        try {
            //Check if current company is "FFICEFAsiaPacific"
            List<GroupMember> existingGroupMembers = new List<GroupMember>();
            existingGroupMembers = [SELECT id, UserOrGroupId, GroupId 
                                    FROM GroupMember 
                                    WHERE Group.developername = 'FFICEFAsiaPacific' AND UserOrGroupId =: UserInfo.getUserId()];
            
            //If current company is not "FFICEFAsiaPacific" 
            if(!(existingGroupMembers.size() > 0)) {
                //Check group for "FFICEFAsiaPacific"
                System.debug('Checking Group for "Asia Pacific"');
                Group AsiaPacificGroup;
                List<Group> existingGroups = new List<Group>();
                existingGroups = [SELECT id, name, developername, type 
                                  FROM Group 
                                  WHERE developername = 'FFICEFAsiaPacific'];
                if(existingGroups.size()>0){
                    AsiaPacificGroup = existingGroups[0];       
                }
                else {
                    AsiaPacificGroup = new Group(developername = 'FFICEFAsiaPacific', Name = 'FF ICEF Asia Pacific', Type = 'Queue');
                    insert AsiaPacificGroup;
                }
                
                System.debug('Creating new group member for "Asia Pacific"');
                GroupMember newGroupMmber = new GroupMember(GroupId = AsiaPacificGroup.Id, UserOrGroupId = UserInfo.getUserId());
                insert newGroupMmber;
                
                //deleting group member of "FFICEFGmbH"
                List<GroupMember> otherGroupMembers = new List<GroupMember>();
                otherGroupMembers = [SELECT id, UserOrGroupId, GroupId 
                                     FROM GroupMember 
                                     WHERE Group.DeveloperName = 'FFICEFGmbH' AND UserOrGroupId =: UserInfo.getUserId()];
                if(otherGroupMembers.size() > 0) {
                    delete otherGroupMembers;
                }
            }
            else{
                System.debug('Selected Company is "Asia Pacific"');
            }
        }
        catch(Exception e){
            System.debug('Exception caught:::'+e.getMessage());
        }
    }
    
}