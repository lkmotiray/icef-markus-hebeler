/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
@isTest
private class testSetSalutationByRuleTrigger
{
    static testMethod void testInsertWithUnspecifiedContact()
    {
        Country__c con = new Country__c(Name='USA',Agent_Relationship_Manager__c = Userinfo.getUserId());
        insert con;
        Account a = new Account(ARM__c = Userinfo.getUserId(),Name='Testing acc', Status__c='Active', Mailing_State_Province__c='CA', Mailing_Country__c=con.Id, account_owner_email__c='aaa.bbb@gmail.com', Email__c='aaa.bbb@gmail.com');
        insert a;
        
        Contact c = new Contact();
        c.AccountId = a.Id;
        c.Gender__c = 'unspecified';
        c.FirstName = 'First';
        c.LastName = 'Last';
        c.Salutation = 'Sir/Madam';
        
        insert c;
        try
        {
            Test.startTest();
            insert c;
            Test.stopTest();
        }
        catch(DMLException e)
        {
            //system.assert(false, 'An Error occurred inserting the test record:');
        }
        
        Contact c2 = [select Salutatory_Address_standard__c, Salutatory_Address_personal__c from Contact where Id = :c.Id limit 1];
       
    }
    
    static testMethod void testInsertWithSpecifiedContact()
    {
        Country__c con = new Country__c(Name='USA',Agent_Relationship_Manager__c = Userinfo.getUserId());
        insert con;
        Account a = new Account(ARM__c = Userinfo.getUserId(),Name='Testing acc', Status__c='Active', Mailing_State_Province__c='CA', Mailing_Country__c=con.Id, account_owner_email__c='aaa.bbb@gmail.com', Email__c='aaa.bbb@gmail.com');
        insert a;
        
        Contact c = new Contact();
        c.AccountId = a.Id;
        c.Gender__c = 'male';
        c.FirstName = 'First';
        c.LastName = 'Last';
        c.Salutation = 'Mr';
        
        insert c;
        try
        {
            Test.startTest();
            insert c;
            Test.stopTest();
        }
        catch(DMLException e)
        {
            //system.assert(false, 'An Error occurred inserting the test record:');
        }
        
        Contact c2 = [select Salutatory_Address_standard__c, Salutatory_Address_personal__c from Contact where Id = :c.Id limit 1];
        
    }

// no more missing saltutation by validation #todo delete
    static testMethod void testInsertWithSpecifiedContactAndMissingSalutation()
    {
        Country__c con = new Country__c(Name='USA',Agent_Relationship_Manager__c = Userinfo.getUserId());
        insert con;
        Account a = new Account(ARM__c = Userinfo.getUserId(),Name='Testing acc', Status__c='Active', Mailing_State_Province__c='CA', Mailing_Country__c=con.Id, account_owner_email__c='aaa.bbb@gmail.com', Email__c='aaa.bbb@gmail.com');
        insert a;
        
        Contact c = new Contact();
        c.AccountId = a.Id;
        c.Gender__c = 'male';
        c.FirstName = 'First';
        c.LastName = 'Last';
        c.Salutation = 'Mr';
        
        insert c;
        try
        {
            Test.startTest();
            insert c;
            Test.stopTest();
        }
        catch(DMLException e)
        {
            //system.assert(false, 'An Error occurred inserting the test record:');
        }
        
        Contact c2 = [select Salutatory_Address_standard__c, Salutatory_Address_personal__c from Contact where Id = :c.Id limit 1];
        
    }

    
    // @todo update actions
    // @todo bulk actions
}