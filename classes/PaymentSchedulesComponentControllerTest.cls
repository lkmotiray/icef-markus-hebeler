@isTest(SeeAllData = true)
public class PaymentSchedulesComponentControllerTest {

    static testMethod void testTestPSComponentController() {  
        c2g__codaInvoice__c salesInvoice = [ SELECT Id FROM c2g__codaInvoice__c LIMIT 1 ];
        Test.startTest();
            PaymentSchedulesComponentController controller = new PaymentSchedulesComponentController();
            controller.salesInvoiceInstance = salesInvoice;
            controller.getSizeOfPaymentSchedules();
        Test.stopTest();
    }  
    
}