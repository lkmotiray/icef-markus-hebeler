/*---------------------------------------
 @Developer: 		Henriette Schönleber
 @Purpose:  		On change of Membership: update fields "Memberships__c", "Industry_Qualifications__c", "Industry_Recognition__c" in associated member Account ("Member__c")
			 		in order to see all short names of Memberships in the respective fields (depending on Association Type of associated association (Association__c))
 @Created Date:		2018/02/07
 @Modified Date:	2018/02/09
----------------------------------------- */

public class MembershipsHandler {
    
    /*
    @Purpose:	update fields "Memberships__c", "Industry_Qualifications__c", "Industry_Recognition__c" in Member account
	@Parameter: List of Membership__c records. Trigger.newMap or Trigger.oldMap
     */
    public static void updateMembershipsInAccount(Map<Id,Membership__c> memMap){

        //set of Ids of all Member accounts of updated Memberships 
        //List<Membership__c> memList = memMap.values();       
		Set<ID> acctIds = new Set<id>();
        for (Membership__c mem : memMap.values()){
            acctIds.add(mem.Member__c);
        }
        
        //find all active Memberships per account
        if (acctIds.size() > 0){
            Map<Account, List<Membership__c>> acctsWithMemberships = membershipsPerAccount(acctIds);
            List<Account> accountsToUpdate = new List<Account>();
            //for each account compare Memberships with values in Membership fields (per type), update ONLY if necessary
            for (Account a : acctsWithMemberships.keySet()){
//                System.debug('Account - Memberships__c: '+ a.Memberships__c);
                Boolean updateAccount = false;
                List<Membership__c> accountsMemberships = acctsWithMemberships.get(a);

                if (a.RecordTypeId == '012200000005D7gAAE'){
                    List<Membership__c> associationMemberships = new List<Membership__c>();
                    List<Membership__c> industryQualifications = new List<Membership__c>();
                    List<Membership__c> industryRecognitions = new List<Membership__c>();

                    for(Membership__c membership : accountsMemberships){
                        if (membership.Association_Type__c == 'Agent Industry Qualification'){
                            industryQualifications.add(membership);
                        }
                        else if (membership.Association_Type__c == 'Industry Recognition'){
                            industryRecognitions.add(membership);                          
                        }
                        else {
                            associationMemberships.add(membership);
                        }
                    } 
                    if (!getMembershipString(industryQualifications).equalsIgnoreCase(a.Industry_Qualifications__c)){
                        if(industryQualifications.size() > 0){
                            a.Industry_Qualifications__c = getMembershipString(industryQualifications);
                        } else {
                            a.Industry_Qualifications__c = '';
                        }
                        updateAccount = true;
                    }
                    if (!getMembershipString(industryRecognitions).equalsIgnoreCase(a.Industry_Recognition__c)){
                        if(industryRecognitions.size() > 0){
                            a.Industry_Recognition__c = getMembershipString(industryRecognitions);
                        } else {
                            a.Industry_Recognition__c = null;
                        }
                        updateAccount = true;
                    }
                    if (!getMembershipString(associationMemberships).equalsIgnoreCase(a.Memberships__c)){
                        if(associationMemberships.size() > 0){
                            a.Memberships__c = getMembershipString(associationMemberships);
                        } else {
                            a.Memberships__c = null;
                        }
                        updateAccount = true;
                    }
                    
                } else {
                    if(!getMembershipString(accountsMemberships).equalsIgnoreCase(a.Memberships__c)){
                        if(accountsMemberships.size() > 0){
                            a.Memberships__c = getMembershipString(accountsMemberships);
                        } else {
                            a.Memberships__c = null;
                        }
                        updateAccount = true;
                    }
                }
                if(updateAccount == true){accountsToUpdate.add(a);}
            }
            System.debug('accountsToUpdate: ' + accountsToUpdate);
            update accountsToUpdate;
        }        
    }
    
    /*
    @Purpose:	function to get a String of existing memberships Short names
	@Parameter:	List of Memberships
     */
    public static String getMembershipString(List<Membership__c> memberships){
        String[] membershipStringList = new String[]{};
        for (Membership__c membership : memberships){
            membershipStringList.add(membership.Name_short__c);
        }
        //make list unique
        List<String> uniqueMemberList = new List<String>();
        Set<String> uniquize = new Set<String>();
        uniquize.addAll(membershipStringList);
        uniqueMemberList.addAll(uniquize);
        //String from list
        String membershipString = '';
        if (uniqueMemberList.size()> 0){
        	membershipString = String.join(uniqueMemberList, ';');
        }
//        System.debug('membershipString: '+ membershipString);
        return membershipString;
    }
    
    /*
    @Purpose: 	function to get a Map of Account and its active Memberships
  	@Parameter:	Set of Account Ids
     */
    public static Map<Account, List<Membership__c>> membershipsPerAccount(Set<Id> acctids){
		Map<Account,List<Membership__c>> acctsAndMemberships = new Map<Account,List<Membership__c>>();
        //find all memberships per account
        for (Id id : acctids){
            Account a = [SELECT Id, Memberships__c,Industry_Qualifications__c,Industry_Recognition__c,RecordTypeId FROM Account WHERE Id = :id];
            List<Membership__c> memList = new List<Membership__c>([SELECT Name_short__c, Association_Type__c
                                                                   FROM Membership__c
                                                                   WHERE Member__c = :id 
                                                                   AND Status__c = 'Active'
                                                                   AND Name_short__c != null
                                                                  ]);
            // add Account -> Membershipstring to acctsAndMemberships
            acctsAndMemberships.put(a,memList);
        }
//        System.debug(acctsAndMemberships);
		return acctsAndMemberships;
    } 
    
}