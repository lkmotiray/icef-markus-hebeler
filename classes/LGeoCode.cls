public with sharing class LGeoCode extends GoogleAPIUtility {
    
    public String Address; 
    public String con;
    public Boolean Problem=false; 
     
    public Lead l; 
    
    public List<Lead> leadsToPlot;
    
    public String getGKey(){ 
        try{
        findNearby__c settings = findNearby__c.getInstance();
        return settings.GKey__c;
        }
        catch(Exception e){return ' ';}
        
    }
    
    
    public Lead getL(){return l;}
    
    //Currently setting the continue flag with a Boolean - though the value is a String
    //This is probably not a good idea.
    public void setContinue(Boolean flag){ 
        if(flag){ con ='T';}
        else{ con ='F';}
    }
    public String getContinue(){return con;}
     
    public pageReference init(){
        Boolean doIDoThis=false;
        try{    
            FindNearby__c FNA = FindNearby__c.getOrgDefaults();
            doIDoThis = FNA.Leads__c;
        }catch(Exception e){
            doIDoThis = false;
        }
        if(!doIDoThis){
            return  Page.AGeoCode;
        }
            
        getAddress();
        return null;
    }
    
    //refactored as an exmple on 2011-08-01
    //instead of querying for one at a time (but still allowing for it)
    //query into a collection, then cycle through those.
    public String getAddress(){ 
        //do we have any existing leads to plot?
        if (leadsToPlot == null || leadsToPlot.size() == 0) {
            //are we trying to plot a specific lead?
            String id = ApexPages.currentPage().getParameters().get('id');
            if (id != null) {
                Lead l = MapUtility.getSingleLeadToPlot(id);
                leadsToPlot = new List<Lead>();
                leadsToPlot.add(l);
            } else {
                //go get some more
                leadsToPlot = MapUtility.getLeadsToPlot();
            }
            //did we get any or are we done?
            if (leadsToPlot == null || leadsToPlot.size() == 0) {
                //nothing here -- return placeholder
                return '-';
            }
        } 
        //ok we have some leads in the list to plot.
        //take the last lead out.
        Lead lastLead = leadsToPlot.get(leadsToPlot.size()-1);
        leadsToPlot.remove(leadsToPlot.size() -1);
        
        
        //Reset the values
        //-----------------
        setContinue(false);
        Address = '-';
        l = null;
        //-----------------
        
        //handled above. String id = ApexPages.currentPage().getParameters().get('id'); 
        //assign lastLead to L, the variable that's managing all this for us.
        l = lastLead;
        problem = false; 
        if(l != null ){
            setContinue(true);
            MapItem a = new MapItem(l);
            Address = a.rAddress;   
        }       

        return Address;
    }
    
    
    public PageReference result() {
        //Get the Status and the Accuracy of the result
        String code = Apexpages.currentPage().getParameters().get('Stat');
         if(code == 'OK')
            code = 'Located';
        else if(code == 'ZERO_RESULTS')
            code = 'Problem with Address';
        else if(code == 'OVER_QUERY_LIMIT')
            code = 'Google Exhausted';
        else if(code == 'REQUEST_DENIED')    
            code = 'Google Exhausted';
        else if(code == 'INVALID_REQUEST')
            code = 'Problem with Address';
                
        
        //If there is a Lead to map...
        if(l !=null)
        {
            //Clean up the message
           // l.Mapping_Status__c = MapUtility.DetermineStatus(code,Accuracy);        
             l.Mapping_Status__c = code;  
            //Determin what to do. 
            if(l.Mapping_Status__c == 'Located')
            {
                try{
                    l.Lat__c = Double.valueOf(Apexpages.currentPage().getParameters().get('Lat'));
                    l.Lon__c = Double.valueOf(Apexpages.currentPage().getParameters().get('Lon'));
                }
                catch(Exception e){
                    l.Mapping_Status__c = 'Problem with Address';
                    System.debug('LGeoCode:' + e + ' Lat:'+Apexpages.currentPage().getParameters().get('Lat')+' Lon:'+Apexpages.currentPage().getParameters().get('Lon'));
                }
            }
            if(l.Mapping_Status__c == 'Bad Google Maps Key')
            {
                return Page.MapError_Google_Key;
            }
            if(l.Mapping_Status__c == 'Google Exhausted')
            {
                return Page.MapError_TooMany;
            }
            
            
            //If there was a problem with the Address
            if(l.Mapping_Status__c =='Problem with Address')
            {
                problem = true; 
            }
            
            update l; 
        
            if(ApexPages.currentPage().getParameters().get('id') != null){
                    return done();
             }
                return null;
        }
        
        return done();
        
    }
        public String getLName(){
        String name = '-';
        try{
            name = String.escapeSingleQuotes(l.Name);
        }
        catch(Exception e){}
        return name;
    }
    public PageReference done(){ 
        String id = ApexPages.currentPage().getParameters().get('id'); 
        
        if(id != null)
        {
            //If there was a problem with the Address
            if(problem)
            {
                return Page.MapError_ProblemAddress;
            }
            
            //If Not
            PageReference p = new PageReference('/apex/FindNearbyMap?lid='+id+'&aid=null');
            return p;
        }
        else{    
            return Page.AGeoCode;
        }
    }
    //*********************************************************************************
    private static testMethod void TestAccountGeoCodeController() {
        LGeoCode trol = new LGeoCode();
        
        //Test setContinue
        trol.setContinue(true);
        System.assert(trol.con=='T');       
        trol.setContinue(false);
        System.assert(trol.con=='F');       
        
        
        //Test GetContinue
        System.assert(trol.con == trol.GetContinue());
        
        //Test GetLName
        System.assert(trol.getLName() != null);
        System.assert(trol.done() != null);
        try{
        Lead tL = new Lead();
        tL.FirstName = 'Iman';
        tL.LastName = 'Iman';
        tL.Street = 'a';
        tL.City = 'a';
        tL.State = 'Ca';
        tL.PostalCode = '94105';
        tL.Country = 'USA';
        tL.Company='Company';
        trol.getGkey();
        trol.init();
        insert tL; 
        trol.result();
        trol.getAddress();
        trol.l = tL; 
        
        ApexPages.currentPage().getParameters().put('Stat', '200');
        //ApexPages.currentPage().getParameters().put('Acc', '7');
        ApexPages.currentPage().getParameters().put('Lat', '7');
        ApexPages.currentPage().getParameters().put('Lon', '7');
        trol.result();
        
        ApexPages.currentPage().getParameters().put('Stat', '200');
       // ApexPages.currentPage().getParameters().put('Acc', '4');
        trol.result();
        ApexPages.currentPage().getParameters().put('Stat', '610');
        ApexPages.currentPage().getParameters().put('Acc', '7');   
        trol.result();
        ApexPages.currentPage().getParameters().put('Stat', '620');
       // ApexPages.currentPage().getParameters().put('Acc', '7');
        trol.result();
        ApexPages.currentPage().getParameters().put('Stat', '777');
      //  ApexPages.currentPage().getParameters().put('Acc', '7');     
        trol.result();
            
        ApexPages.currentPage().getParameters().put('id', tL.Id);        
       	ApexPages.currentPage().getParameters().put('Stat', 'OK');
        trol.getAddress();
        trol.result();         
        ApexPages.currentPage().getParameters().put('Lat', 'l');
        ApexPages.currentPage().getParameters().put('Lon', 'g');
        trol.result();
        ApexPages.currentPage().getParameters().put('Stat', 'ZERO_RESULTS');                 
        trol.result();            
        ApexPages.currentPage().getParameters().put('Stat', 'OVER_QUERY_LIMIT');
        trol.result();            
        ApexPages.currentPage().getParameters().put('Stat', 'REQUEST_DENIED');
        trol.result();                     
        ApexPages.currentPage().getParameters().put('Stat', 'INVALID_REQUEST');
        trol.result();
        ApexPages.currentPage().getParameters().put('id', null);
        trol.result();
       
        trol.getL();
        }
        catch(Exception e){}
     }
    
}