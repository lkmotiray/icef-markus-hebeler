/**
  * Purpose : PaymentSchedulesComponent Controller
 */
public with sharing class PaymentSchedulesComponentController {
    public Integer paymentSchedulesSize {get;set;}
    public c2g__codaInvoice__c salesInvoiceInstance {get; set;}
    
    //--Set size of payment schedules
    public void getSizeOfPaymentSchedules() {
    
        try {
            paymentSchedulesSize = [ SELECT COUNT() 
                                     FROM c2g__codaInvoiceInstallmentLineItem__c
                                     WHERE c2g__Invoice__c = :salesInvoiceInstance.id ];
            salesInvoiceInstance = [ SELECT Id, c2g__Opportunity__r.Invoicing_contact__r.Name,
                                            c2g__Opportunity__r.Invoicing_contact__r.FirstName
                                     FROM c2g__codaInvoice__c
                                     WHERE Id = :salesInvoiceInstance.id LIMIT 1];
        } catch(Exception genericException) {
            System.debug('Exception occurred : ' + genericException.getMessage());
        }
    }
}