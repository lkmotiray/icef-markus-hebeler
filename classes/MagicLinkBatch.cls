global class MagicLinkBatch implements Database.Batchable<sObject>{

    global Integer recordsProcessed = 0;
    global Database.QueryLocator start(Database.BatchableContext bc){
        DateTime checkDate = DateTime.now()-7;
        DateTime modifiedSince = DateTime.now()-3;
        return Database.getQueryLocator(
            [SELECT Id, magic_link__c, magic_link_updated__c
            FROM Contact
            WHERE (magic_link_updated__c < :checkDate OR magic_link_updated__c = null)
            AND Contact_Status__c = 'Active' 
            AND LastModifiedDate > :modifiedSince
            AND Email != null]
        );
    }
    
    global void execute(Database.BatchableContext bc, List<Contact> scope){
        Map<Id,Contact> contactMap = new Map<Id,Contact>(scope);
        List<Id> updateContacts = new List<Id>();
        updateContacts.addAll(contactMap.keySet());
        // for(Contact con : scope){
        //     updateContacts.add(con.Id);
        //     recordsProcessed = recordsProcessed +1;
        // }
        recordsProcessed = recordsProcessed + updateContacts.size();
        MagicLinkAction.generateMagicLink(updateContacts);
    }
    
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' contact magic links updated.');
        // AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email 
        //                     FROM AsyncApexJob 
        //                     WHERE Id = :bc.getJobId()];
//        EmailUtils.sendMessage(job, recordsProcessed);
    }
}