public with sharing class sendemailtocontactparticipation
{
    /********** Declaration **********************/

    public string subject {get; set;}
    public string body {get; set;}
    public string workshopemail {get; set;}
    public string workshopcc {get; set;}
    public string contactId {get; set;}
    public string cpname {get; set;}
    public string cpid, etid, emailtemplatebody = '', emailtemplatebody_copy = '';

    public string[] cpemailids = new string[] {};
    Contact_Participation__c cp;
    public string comment = '';
    public string testCpId;
    public string testemailtempId;
    List<String> mergefieldArr = new List<String>();
    List<String> cpfieldArr = new List<String>();
    integer i = 0, j = 0, k, m = 0;
    string mergefield, cpfield, startString = '{!', endString = '}';
    List<EmailTemplate> emailTemp = new List<EmailTemplate>();
    public string testBody = '';
    public string[] testcpemailids = new string[] {};
    public List<String> mergeFieldsOtherSObjectFields = new List<String> {};
    public List<String> cpFieldsOtherSObjectFields = new List<String> {};
    //public String selectedOption = null;
    public string fromEmail;
    public boolean flag{get; set;}
    public boolean emailAttachment{get;set;}

    //Constructor for test method
    public sendemailtocontactparticipation(string emailtempid, string contpartid)
    {
        flag = true;
        cpid = contpartid;
        integer s = 0;
        string allfields = '', cpquery = '';
        emailAttachment = false;

        Map<String , Schema.SObjectType> globalDescription1 = Schema.getGlobalDescribe();
        schema.SObjectType contactParticipationType1 = globalDescription1.get('Contact_Participation__c');

        Schema.DescribeSObjectResult Obj1 = contactParticipationType1.getDescribe();

        Map<String, Schema.SObjectField> objFields1 = Obj1.fields.getMap();
        for(String fieldName : objFields1.keySet())
        {
            if(s == 0)
            {
                allfields = fieldName.trim();
                s = 1;
            }
            else
                allfields = allfields + ', ' + fieldName;
        }
        cpquery = 'Select ' + allfields + ' from Contact_Participation__c where Id = \'' + cpid + '\' ';
        cp = Database.query(cpquery);

        //cp = [Select follow_up_link__c, survey_country_3__c, table_booth_type__c, icef_staff_online_form_date__c, createdbyid, visa_comments_event_specific__c, flight_departure_time__c, start_date_of_event__c, workshop_email_equal_to_contact_email__c, isdeleted, late_check_out_required__c, country_section__c, recordtypeid, systemmodstamp, pre_event_survey_filled_out__c, ap_hotel_date_arrival__c, no_eschedule__c, hotel_date_arrival__c, createddate, preferred_ws_email__c, special_diet__c, icef_event__c, account_name__c, use_formal_salutation__c, contact_remarks__c, survey_country_2__c, account_id__c, flight_number_arrival__c, lastmodifiedbyid, survey_country_1__c, country_survey_date__c, account_record_type__c, hotel_confirmation__c, no_show_follow_up_sent__c, iatc__c, hotel_departure_time__c, visa_status__c, picture_url__c, ap_contacts__c, event_managers_providers_email__c, old_icef_id__c, account_participation__c, icef_staff_comments__c, iatc_enquiry_at_this_ws__c, no_shows_at_ws__c, status_sent_date__c, iatc_certification_number__c, info_4_sent__c, info_5_sent__c, other_diet__c, online_evaluation_filled_out__c, flight_arrival_time__c, picture__c, catalogue_format__c, catalogue_position__c, eatc_enquiry_at_this_ws__c, hotel_arrival_time__c, account_participation_record_type__c, info_pack_sent__c, workshop_email__c, link_to_pre_event_survey__c, iatc_status_date__c, follow_up_sent__c, passport_number__c, arm_emailaddress__c, contact_phone__c, lastactivitydate, ap_hotel_date_departure__c, esp_login_sent__c, info_2_sent__c, personal_link_to_evaluation_survey__c, marcom_cax_list__c, electricity_at_table__c, visa_list_anza__c, iatc_status__c, visa_fee_paid__c, flight_number_departure__c, guest_participation_details__c, ap_room_type__c, account_participation_status__c, participation_status__c, ap_no_of_nights__c, survey_country_5__c, esp_login_transfer__c, visa_comments__c, catalogue_country__c, contact_title__c, table_booth_number2__c, info_6_sent__c, event_managers_agents_email__c, contact_mobile__c, workshop_cc__c, currencyisocode, flight_arrival_date__c, fam_tour_participation__c, flight_departure_date__c, survey_country_4__c, lastmodifieddate, id, confirmation_sent__c, account_participation_status_text__c, link_to_pre_event_survey_agencies__c, name, event_full_name__c, visa_status_china__c, salutatory_address__c, contact__c, wifi_required__c, table_booth_number__c, ap_hotel_extras__c, iatc_comments__c, early_check_in_required__c, account_participation_record_type_text__c, visa_date__c, picture_upload_date__c, picture_in_catalogue__c, account_onsite_registration__c, info_3_sent__c, birthdate__c, department__c from Contact_Participation__c where id =: cpid];
        cpname = cp.Name;

        if(cp.Workshop_email__c != null)
        {
            workshopemail = cp.Workshop_email__c;
            //workshopcc = cp.Workshop_CC__c;
            workshopcc = cp.Workshop_CC_Mailers__c;
            if(workshopcc != null)
                cpemailids.add(workshopcc);
            cpemailids.add(workshopemail);
        }
        else
        {
            Contact c = [Select Id, Email from Contact where id =: cp.Contact__c];
            workshopemail = c.Email;
            cpemailids.add(workshopemail);
        }

        User u = [Select Email from USer where Id =: UserInfo.getUserId()];

        fromEmail = ApexPages.currentPage().getParameters().get('from');
        if(fromEmail != '')
            selectedOption = fromEmail;

        etid = ApexPages.currentPage().getParameters().get('etid');
        if(etid == null)
            etid = emailtempid;
        if(etid != '')
        {
            showEmailBodyOnPageLoad(etid);
        }
    }

    //Stadard Controller constructor
    public sendemailtocontactparticipation(ApexPages.StandardController controller)
    {
        flag = true;
        emailAttachment = false;

        System.debug('controller::'+controller.getRecord());
        string allfields = '', cpquery = '';
        integer s = 0;

        cpid = controller.getRecord().Id;

        if(cpid == null || cpid == 'null' || cpid == '')
        {
            showError();
        }
        else
        {
            //Access all fields of Contact Participation
            Map<String , Schema.SObjectType> globalDescription2 = Schema.getGlobalDescribe();
            schema.SObjectType contactParticipationType2 = globalDescription2.get('Contact_Participation__c');

            Schema.DescribeSObjectResult Obj2 = contactParticipationType2.getDescribe();

            Map<String, Schema.SObjectField> objFields2 = Obj2.fields.getMap();
            for(String fieldName : objFields2.keySet())
            {
                if(s == 0)
                {
                    allfields = fieldName.trim();
                    s = 1;
                }
                else
                    allfields = allfields + ', ' + fieldName;
            }
            cpquery = 'Select ' + allfields + ' from Contact_Participation__c where Id = \'' + cpid + '\' ';
            System.debug('cpquery::'+cpquery);
            cp = Database.query(cpquery);

            //cp = [Select follow_up_link__c, survey_country_3__c, table_booth_type__c, icef_staff_online_form_date__c, createdbyid, visa_comments_event_specific__c, flight_departure_time__c, start_date_of_event__c, workshop_email_equal_to_contact_email__c, isdeleted, late_check_out_required__c, country_section__c, recordtypeid, systemmodstamp, pre_event_survey_filled_out__c, ap_hotel_date_arrival__c, no_eschedule__c, hotel_date_arrival__c, createddate, preferred_ws_email__c, special_diet__c, icef_event__c, account_name__c, use_formal_salutation__c, contact_remarks__c, survey_country_2__c, account_id__c, flight_number_arrival__c, lastmodifiedbyid, survey_country_1__c, country_survey_date__c, account_record_type__c, hotel_confirmation__c, no_show_follow_up_sent__c, iatc__c, hotel_departure_time__c, visa_status__c, picture_url__c, ap_contacts__c, event_managers_providers_email__c, old_icef_id__c, account_participation__c, icef_staff_comments__c, iatc_enquiry_at_this_ws__c, no_shows_at_ws__c, status_sent_date__c, iatc_certification_number__c, info_4_sent__c, info_5_sent__c, other_diet__c, online_evaluation_filled_out__c, flight_arrival_time__c, picture__c, catalogue_format__c, catalogue_position__c, eatc_enquiry_at_this_ws__c, hotel_arrival_time__c, account_participation_record_type__c, info_pack_sent__c, workshop_email__c, link_to_pre_event_survey__c, iatc_status_date__c, follow_up_sent__c, passport_number__c, arm_emailaddress__c, contact_phone__c, lastactivitydate, ap_hotel_date_departure__c, esp_login_sent__c, info_2_sent__c, personal_link_to_evaluation_survey__c, marcom_cax_list__c, electricity_at_table__c, visa_list_anza__c, iatc_status__c, visa_fee_paid__c, flight_number_departure__c, guest_participation_details__c, ap_room_type__c, account_participation_status__c, participation_status__c, ap_no_of_nights__c, survey_country_5__c, esp_login_transfer__c, visa_comments__c, catalogue_country__c, contact_title__c, table_booth_number2__c, info_6_sent__c, event_managers_agents_email__c, contact_mobile__c, workshop_cc__c, currencyisocode, flight_arrival_date__c, fam_tour_participation__c, flight_departure_date__c, survey_country_4__c, lastmodifieddate, id, confirmation_sent__c, account_participation_status_text__c, link_to_pre_event_survey_agencies__c, name, event_full_name__c, visa_status_china__c, salutatory_address__c, contact__c, wifi_required__c, table_booth_number__c, ap_hotel_extras__c, iatc_comments__c, early_check_in_required__c, account_participation_record_type_text__c, visa_date__c, picture_upload_date__c, picture_in_catalogue__c, account_onsite_registration__c, info_3_sent__c, birthdate__c, department__c from Contact_Participation__c where id =: cpid];
            cpname = cp.Name;

            if(cp.Workshop_email__c != null)
            {
                workshopemail = cp.Workshop_email__c;
                //workshopcc = cp.Workshop_CC__c;
                workshopcc = cp.Workshop_CC_Mailers__c;
                if(workshopcc != null)
                    cpemailids.add(workshopcc);
                cpemailids.add(workshopemail);
            }
            else
            {
                Contact c = [Select Id, Email from Contact where id =: cp.Contact__c];
                workshopemail = c.Email;
                cpemailids.add(workshopemail);
            }

            User u = [Select Email from USer where Id =: UserInfo.getUserId()];

            fromEmail = ApexPages.currentPage().getParameters().get('from');
            if(fromEmail != '')
                selectedOption = fromEmail;

            etid = ApexPages.currentPage().getParameters().get('etid');
            if(etid != '')
            {
                showEmailBodyOnPageLoad(etid);
            }
        }
    }

    Public PageReference showError()
    {
        flag = false;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You may have changed url.. Id parameter is required in Url'));
        return null;
    }

    //Function to replace merge field values
    public void showEmailBodyOnPageLoad(string etid)
    {
        emailTemp = [Select Subject, Body, Markup, HtmlValue, TemplateType from EmailTemplate where Id =: etid];

        if(emailTemp.size() != 0)
        {
            subject = emailTemp[0].Subject;

            if(emailTemp[0].TemplateType != 'Text')
                body = emailTemp[0].HtmlValue; //Body;
            else
                body = emailTemp[0].Body;

            if(body != null){
                String seperator = '/qwertyuiopasd123654789655455fghjklzxcvbnm/';
                
                body = subject + seperator + body;
                
                body = replaceMergeFields(body);
                
                List<String> listContents = body.split(seperator);
                if(!listContents.isEmpty()){
                    subject = listContents[0];
                    body = listContents[1];
                }else{
                    body = body.replace(seperator, '');
                }
            }
        }

        emailtemplatebody_copy = body;
    }

    public string replaceMergeFields(String body)
    {
        emailtemplatebody = body;

        while(i < body.length())
        {
            j = body.indexof(startString, i);
            if(j == -1)
                break;

            k = body.indexof(endString, j);

            cpfieldArr.add(body.substring(j,k+1));
            cpfield = body.substring(j,k+1);
            string mfield = cpfield.substring(cpfield.indexof('.')+1,cpfield.indexof(endString));
            mergefieldArr.add(mfield);

            i = k;
        }


        //Accessing all contact participation fields
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        schema.SObjectType contactParticipationType = globalDescription.get('Contact_Participation__c');

        Schema.DescribeSObjectResult Obj = contactParticipationType.getDescribe();

        Map<String, Schema.SObjectField> objFields = Obj.fields.getMap();
        //System.debug('objfields count::'+objFields.size());
        for(integer a = 0; a < mergefieldArr.size(); a++)
        {
            for(String fieldName : objFields.keySet())
            {

                if(mergefieldArr[a] == fieldName)
                {
                    schema.SobjectField s = objFields.get(fieldname);
                    Schema.DescribeFieldResult R = s.getDescribe();

                    if(String.valueof(cp.get(fieldName)) != null)
                    {
                        if(String.valueOF(R.getType()) == 'Boolean')
                        {
                            String tempBool;
                            if(Boolean.valueof(cp.get(fieldName)) == true)
                                tempBool = '1';
                            else
                                tempBool = '0';
                            body = body.replace(cpfieldArr[a], tempBool);
                            emailtemplatebody = emailtemplatebody.replace(cpfieldArr[a], tempBool);
                        }
                        else if(String.valueOF(R.getType()) == 'DateTime' || String.valueOF(R.getType()) == 'Date')
                        {
                            body = body.replace(cpfieldArr[a], String.valueOf(Date.valueof(cp.get(fieldName))));
                            emailtemplatebody = emailtemplatebody.replace(cpfieldArr[a], String.valueOf(Date.valueof(cp.get(fieldName))));
                        }
                        else if(R.isIdLookup() && String.valueOF(R.getType()) == 'ID')
                        {
                            string tempId = String.ValueOf(cp.get(fieldName));
                            tempId = tempId.substring(0, tempId.length()-3);
                            body = body.replace(cpfieldArr[a], tempId);
                            emailtemplatebody = emailtemplatebody.replace(cpfieldArr[a], tempId);
                        }
                        else if(String.valueOF(R.getType()) == 'Reference')
                        {
                            List<Schema.sObjectType> P = R.getReferenceTo();

                            if(String.valueOf(P[0]) != 'User' && String.valueOf(P[0]) != 'RecordType')
                            {
                                String query1 = 'Select Id, Name from '+String.valueOf(P[0])+ ' where Id = \'' + cp.get(fieldName) + '\' ';
                                List<sObject> so = Database.query(query1);

                                body = body.replace(cpfieldArr[a], String.valueof(so[0].get('Name')));
                                emailtemplatebody = emailtemplatebody.replace(cpfieldArr[a], String.valueof(so[0].get('Name')));
                                mergeFieldsOtherSObjectFields.add(fieldName);
                                cpFieldsOtherSObjectFields.add(cpfieldArr[a]);
                            }
                            else
                            {
                                string tempId2 = String.ValueOf(cp.get(fieldName));
                                tempId2 = tempId2.substring(0, tempId2.length()-3);

                                body = body.replace(cpfieldArr[a], tempId2);

                                emailtemplatebody = emailtemplatebody.replace(cpfieldArr[a], tempId2);

                            }
                        }
                        else
                        {
                            string tempStr = String.valueof(cp.get(fieldName));
                            tempStr = tempStr.replaceAll('\n','<br/>');
                            body = body.replace(cpfieldArr[a], tempStr);
                            emailtemplatebody = emailtemplatebody.replace(cpfieldArr[a], tempStr);
                        }
                    }
                    else
                    {
                        body = body.replace(cpfieldArr[a], '');
                        emailtemplatebody = emailtemplatebody.replace(cpfieldArr[a], '');
                    }

                    break;
                }
                else if(mergefieldArr[a] == 'CreatedBy')
                {
                    String query =  'Select Id, Name from User where Id = \'' + cp.get('CreatedByID') + '\' ';
                    List<User> u = Database.Query(query);

                    body = body.replace(cpfieldArr[a], u[0].Name);

                    emailtemplatebody = emailtemplatebody.replace(cpfieldArr[a], u[0].Name);

                    break;
                }
                else if(mergefieldArr[a] == 'LastModifiedBy')
                {
                    String query =  'Select Id, Name from User where Id = \'' + cp.get('LastModifiedByID') + '\' ';
                    List<User> u = Database.Query(query);
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'cpfieldArr[a]'+cpfieldArr[a]));
                    body = body.replace(cpfieldArr[a], u[0].Name);

                    emailtemplatebody = emailtemplatebody.replace(cpfieldArr[a], u[0].Name);

                    break;
                }
                else if(mergefieldArr[a] == 'RecordType')
                {
                    String query =  'Select Id, Name from RecordType where Id = \'' + cp.get('RecordTypeID') + '\' ';
                    List<RecordType> rt = Database.Query(query);

                    body = body.replace(cpfieldArr[a], rt[0].Name);

                    emailtemplatebody = emailtemplatebody.replace(cpfieldArr[a], rt[0].Name);

                    break;
                }

            }

          }

        for(integer x = 0; x < mergeFieldsOtherSObjectFields.size(); x++)
        {
            for(integer y = 0; y < mergefieldArr.size(); y++)
            {
                string[] temp = mergeFieldsOtherSObjectFields[x].split('__');
                string[] temp1 = cpFieldsOtherSObjectFields[x].split('__');

                if(temp.size() == 2 && temp1.size() == 3)
                {
                    string referIdField = temp[0] + 'Id__' + temp[1];
                    string cpreferIdField = temp1[0] + '__' + temp1[1] + 'Id__' + temp1[2];

                    if(mergefieldArr[y] == referIdField)
                    {
                        string tempId1 = String.ValueOf(cp.get(mergeFieldsOtherSObjectFields[x]));
                        tempId1 = tempId1.substring(0, tempId1.length()-3);

                        body = body.replace(cpreferIdField, tempId1);

                        emailtemplatebody = emailtemplatebody.replace(cpreferIdField, tempId1);

                        break;
                    }
                }

            }
        }

        for(integer m = 0; m < cpfieldArr.size(); m++)
        {
            //System.debug('cpfieldArr[m]::'+cpfieldArr[m]);
            body = body.replace(cpfieldArr[m], '');
            emailtemplatebody = emailtemplatebody.replace(cpfieldArr[m], '');
        }

        return body;
    }
    public String getBody() {
        return null;
    }
    public void setBody(string emailbody)
    {
        this.body = emailbody;
    }

    public String selectedOption
    {
       get
        {
           //System.debug('selectedOption in property::'+selectedOption);
           if(selectedOption==null)
           {
               User u = [Select Email, Id from User where Id =: UserInfo.getUSerId()];
               selectedOption = u.Email;
               System.debug('selectedOption  1::'+selectedOption);
           }
           return selectedOption;
        }
    set;
    }

    public List<SelectOption> getItems()
    {
        List<SelectOption> options = new List<SelectOption>();

        User u = [Select Name, Email, Id from User where Id =: UserInfo.getUSerId()];
        String loggedInUser = '"'+ u.Name + '" <'+ u.Email + '>';
        options.add(new SelectOption(loggedInUser, loggedInUser));

        List<OrgWideEmailAddress> owa = [select Address, DisplayName from OrgWideEmailAddress];

        for(integer i = 0; i < owa.size(); i++)
        {
            string option = '"'+ owa[i].DisplayName + '" <'+ owa[i].Address + '>';
            options.add(new SelectOption(option, option));
        }

        return options;
    }

    //Send email function
    public pageReference sendEmail()
    {
        try
        {

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

            email.setSubject(subject);

            if(testbody == '')
            {
                if(emailtemplatebody_copy == null){
                    System.debug('HTML');
                    System.debug('body=='+(String)body);
                    email.setHtmlBody(body);
                }else{
                     System.debug('plain');
                    email.setHtmlBody(emailtemplatebody_copy);//setPlainTextBody(body);
                }
            }else{
                email.setPlainTextBody(testbody);
            }

            if(cpemailids != null)
            {
                //System.debug('cpemailids1::'+cpemailids);
                email.setToAddresses(cpemailids);
            }
            else
            {
                //System.debug('testcpemailids1::'+testcpemailids);
                email.setToAddresses(testcpemailids);
            }

            email.saveAsActivity = true;

            email.setUseSignature(true);

            //set organisation wide email address

            selectedOption = selectedOption.subString(selectedOption.indexOf('<')+1, selectedOption.indexOf('>'));

            List<OrgWideEmailAddress> owa = [select id, Address from OrgWideEmailAddress where Address =: selectedOption];
            if(owa.size() != 0)
                email.setOrgWideEmailAddressId(owa[0].id);

            //Set email file attachments
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();

            if(etid != '')
            {
                for (Attachment a : [select Id, Name, Body, BodyLength from Attachment where ParentId = :etid])
                {
                    // Add to attachment file list
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName(a.Name);
                    efa.setBody(a.Body);
                    fileAttachments.add(efa);
                }

            }

            //check for pdf attachment
            if(emailAttachment)
            {
                if(!emailTemp.isEmpty())
                {
                    PageReference pageRef = new PageReference('/apex/EmailAttachmentPDF');
                    pageRef.getParameters().put('etid', etid);
                    pageRef.getParameters().put('cpid', cpid);
                    Blob pdfBlob = pageRef.getContent();

                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName(subject + '.pdf');
                    efa.setBody(pdfBlob);
                    efa.setContentType('application/pdf');
                    fileAttachments.add(efa);
                }
            }

            //set file attachments
            if(!fileAttachments.isEmpty())
                email.setFileAttachments(fileAttachments);

            // Send the email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });

            string taskSubject = '';

            taskSubject = 'Email: '+ subject;

            if(emailtemplatebody != '')
                comment = 'Subject: '+subject+' \n Body: \n'+emailtemplatebody;
            else
                comment = 'Subject: '+subject+' \n Body: \n'+body;
            //Make sure not to exceed 32000 char limit for Comment in subject
            if(comment.length() > 32000){
                comment = comment.left(32000);
            }
            //Create task
            Task t = new Task(
                     ownerId = UserInfo.getUserId(),
                     Subject = taskSubject,
                     whatId = cpid,
                     status = 'Completed',
                     whoId = cp.Contact__c,
                     Type = 'Email',
                     ActivityDate = Date.TODAY(),
                     Description = comment);
            insert t;

            Pagereference p = new PageReference('/'+cpid);
            p.setRedirect(true);
            return p;

         }catch(Exception e)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, '\''+selectedOption+'\' Email Address is not allowed for you profile. Please select another email address.'));
            return null;
        }

    }

    public Pagereference selectTemplateRedirect()
    {
          PageReference p = new PageReference('/apex/cpselecttemplate?cpid='+cpid+'&from='+selectedOption);
          p.setRedirect(true);
          return p;
    }
    public Pagereference cancel()
    {
          Pagereference p1 = new PageReference('/'+cpid);
          p1.setRedirect(true);
          return p1;
    }

    //test method
    public static testMethod void testsendemailtocontactparticipation()
    {
        List<folder> f = [Select Name, Id from Folder where Type = 'Email'];
        //insert f;
        Id fid = null, etid = null;
        if(f.size() != 0)
        {
            fid = f[0].id;
            List<EmailTemplate> et = [Select Name, Subject, Body from EmailTemplate where FolderId =: fid];
            if(et.size() != 0)
            {
                etid = et[0].id;
            }
        }

        User u = [Select Id, Name from User where Id =: UserInfo.getUserId()];
        Country__c ctry = new Country__c(Name = 'Afghanistan', Region__c = 'Middle East', Agent_Relationship_Manager__c = u.Id, Sales_Territory_Manager__c = u.Id);
        insert ctry;
        System.RunAs(u)
         {
            Test.startTest();
            mytestFunc2();
            Test.stopTest();
         }

        Account acc = new Account(Name = 'Test Account', Status__c = 'Active', Mailing_Country__c = ctry.id);
        insert acc;

        Contact cont = new Contact(Salutation = 'Dr.', FirstName = 'Test', LastName = 'Test', Gender__c = 'male', Role__c = 'Assistant',
                                   Contact_Status__c = 'Active', AccountId = acc.Id, Email = 'rupali.pophaliya@dreamwarestech.com');
        insert cont;

        ICEF_Event__c ie = new ICEF_Event__c(Name = 'Test Event1', Event_City__c = 'Test City1', technical_event_name__c = 'Test Event1',
                                            Event_Manager_Agents__c = u.Id, Event_Manager_Educators__c = u.Id, Event_Country__c = ctry.id);
        insert ie;

        Account_Participation__c ap = new Account_Participation__c(Account__c = acc.Id, ICEF_Event__c = ie.Id, Organisational_Contact__c = cont.Id, Contacts__c = cont.Id,
                                                                    Room_Type__c = 'Delux', Participation_Status__c = 'accepted', Catalogue_Country__c = ctry.Id,
                                                                    Hotel_Extras__c = 'test', Hotel_Date_Arrival__c = System.TODAY(),
                                                                    Hotel_Date_Departure__c = System.TODAY(), Country_Section__c = ctry.Id);
        insert ap;

        Contact_Participation__c cp = new Contact_Participation__c(Contact__c = cont.Id, ICEF_Event__c = ie.id, Participation_Status__c = 'registered',
                                                                   Catalogue_Position__c = 2, Preferred_WS_Email__c = 'rupali.pophaliya@dreamwarestech.com',
                                                                   Account_Participation__c = ap.id);
        insert cp;

        List<EmailTemplate> et = [Select Id from EmailTemplate where Name = 'Test Template'];
        if(et.size() != 0)
        {
            etid = et[0].id;
        }
        sendemailtocontactparticipation setocp = new sendemailtocontactparticipation(etid, cp.id);
        setocp.testBody = 'test body';
        setocp.testcpemailids.add('rupali.pophaliya@dreamwarestech.com');
        //setocp.showEmailBodyOnPageLoad(etid);
        setocp.getItems();
        setocp.getbody();
        setocp.setbody(setocp.testBody);
        setocp.selectedOption = null;
        setocp.selectedOption = '"Markus Hebeler, ICEF GmbH" <mhebeler@icef.com>';
        setocp.sendEmail();
        setocp.selectTemplateRedirect();
        setocp.cancel();

        Test.setCurrentPageReference(new PageReference('Page.EmailAttachmentPDF'));
        System.currentPageReference().getParameters().put('etid', etid);
        System.currentPageReference().getParameters().put('cpid', cp.id);
        EmailAttachmentPDFController contr = new EmailAttachmentPDFController();

        sendemailtocontactparticipation setocp1 = new sendemailtocontactparticipation(new ApexPages.StandardController(cp));
        setocp1.sendEmail();
    }

    @future
    private static void mytestFunc2()
    {

        List<folder> f = [Select Name, Id from Folder where Type = 'Email'];
        //insert f;
        EmailTemplate et = new EmailTemplate();
        Id fid = null, etid = null;
        if(f.size() != 0)
        {
            fid = f[0].id;
            et = new EmailTemplate(TemplateType = 'Text', Name = 'Test Template', Subject = 'Test Subject', Body = 'Test Body {!Contact_Participation__c.AP_Hotel_Extras__c}, {!Contact_Participation__c.AP_Contacts__c}, {!Contact_Participation__c.AP_Hotel_Date_Departure__c}, {!Contact_Participation__c.AP_No_of_Nights__c}, {!Contact_Participation__c.AP_Room_Type__c}, {!Contact_Participation__c.Salutatory_Address__c} {!Contact_Participation__c.CreatedBy}, {!Contact_Participation__c.LastModifiedBy} {!Contact_Participation__c.RecordType}', DeveloperName = 'Test_Template', FolderId = fid);
            insert et;
            Blob abody = Blob.valueOf('testing');
            Attachment att = new Attachment(Name = 'testing', body = abody, ParentId = et.id);
            insert att;
        }
    }
}