/***********************************************************************************************************************************

@purpose        : a) 20 year mag Checkbox Functionality : This checkbox indicates whether the account has at least one contact 
                     with the checkbox ticked.
                  b) Rollup for Contacts having Role 'Primary Decision Maker' : In each account, the number of related contacts 
                     that have Role ‘Primary Decision Maker’ have been displayed
             
@created Date   : 20-July-2016
***********************************************************************************************************************************/
global class AccountPMDBatch implements Database.Batchable<SObject>, Database.Stateful {

    String failureCSV;
    Integer Batchcount = 0;
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        failureCSV = 'Id,reason\n';
        return Database.getQueryLocator([SELECT Id, Number_of_PMD_Contacts__c, X20_year_mag__c 
                                        FROM Account ]); 
    }
    
    global void execute(Database.BatchableContext BC, List<Account> accounts) {
        
        List<Account> accountToUpdates = new List<Account>();
        
        Map<id, List<Contact>> accountIdToContactMap = buildAccountIdToContactMap(accounts);
        
        Integer countPMDContacts;
        Boolean isX20_year_mag_Checked;
        
        for (Account a : accounts) {
            
             
            countPMDContacts = 0;
            isX20_year_mag_Checked = false;
            if (accountIdToContactMap.containsKey(a.id) ) { 
                
                for (Contact contactRec : accountIdToContactMap.get(a.id)) { 
                
                    if(contactRec.Role__c == 'Primary Decision Maker')
                        countPMDContacts++;
                        
                    if(contactRec.X20_year_mag__c && !isX20_year_mag_Checked)
                        isX20_year_mag_Checked = true;
                }
            }
            
            accountToUpdates.add(new Account( id = a.id, 
                                              X20_year_mag__c = isX20_year_mag_Checked, 
                                              Number_of_PMD_Contacts__c = countPMDContacts));
        }
        
        Database.SaveResult[] srList = Database.update(accountToUpdates, false);
        Integer cnt = 0;
        for (Database.SaveResult sr : srList) {
            
            if (!sr.isSuccess()) {
                
                String errorMsg='';
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    
                    errorMsg = errorMsg + err.getMessage();
                }

                if(errorMsg.length() > 65)
                    errorMsg = errorMsg.substring(0,65);
                
                failureCSV = failureCSV + accountToUpdates[cnt]+','+errorMsg+'\n';
            }
            cnt = cnt + 1;
        }
        
        Batchcount = Batchcount + 1;

    }
    
    
    private static Map<id, List<Contact>> buildAccountIdToContactMap(List<Account> accounts){
        
        Map<id, List<Contact>> accountIdToContactMap = new Map<id, List<Contact>>();
        for(Contact conRec: [SELECT Id, Role__c, X20_year_mag__c, AccountID 
                             FROM Contact
                             WHERE AccountID IN: accounts]){
                                 
            if(accountIdToContactMap.containsKey(conRec.AccountID))
                accountIdToContactMap.get(conRec.AccountID).add(conRec);
            else
                accountIdToContactMap.put(conRec.AccountID, new List<Contact>{conRec});
            
        }
        
        return accountIdToContactMap;
    }
    
    global void finish(Database.BatchableContext info) {
        
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { 'hardik@dreamwares.com' } );
        mail.setSubject( 'AccountPMDBatch result' );
        mail.setHtmlBody( 'Batchcount=='+Batchcount );

        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setFileName( 'AccountPMDBatchResult.csv' );
        attachment.setBody( Blob.valueOf( failureCSV ) );
        mail.setFileAttachments( new Messaging.EmailFileAttachment[]{ attachment } );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }
}