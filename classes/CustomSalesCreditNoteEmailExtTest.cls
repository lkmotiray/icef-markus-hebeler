/**
 *  @Purpose Tests the functionality of the CustomSalesCreditNoteEmailExtension
 *  @Author Dreamwares
 *  @Date 16 march 2018
 */
@isTest(SeeAllData = true)
public class CustomSalesCreditNoteEmailExtTest{
            
     static testMethod void testCustomSalesCreditNoteEmail1() {        
        // Assign email ids to send test email
        String strTo = 'to@test.com';
        String strFrom = 'from@test.com';
        String strAdditionalTo = 'additionalTo@test.com';
        String strCC = 'cc@test.com';
        String strBCC = 'bcc@test.com';
        String subject = 'Email: Testing CustomSalesInvoiceEmail Page Email';
        
        Test.startTest();
        
        // Refer the page to test 
        PageReference pageRef = Page.CustomSalesCreditNoteEmail;            
        Test.setCurrentPage(pageRef);  
        
         // Get the Sales Invoice record to test
        c2g__codaCreditNote__c  testCreditNote = [SELECT Id FROM c2g__codaCreditNote__c  LIMIT 1];
        System.currentPageReference().getParameters().put('Id', testCreditNote.Id);
        
        // Set the email template id to pass as parameter to the url
        EmailTemplate testEmailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'codaSalesCreditNoteStandard' LIMIT 1];
        System.currentPageReference().getParameters().put('emailTemplateId', testEmailTemplate.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testCreditNote);
        CustomSalesCreditNoteEmailExtension controllerInstance = new CustomSalesCreditNoteEmailExtension(sc);    
        
        controllerInstance.prepareSendEMail();
        //controllerInstance.getEmailAttachments();
        controllerInstance.getSelectedToUsers();
        controllerInstance.getItems();
        controllerInstance.getFromList();   
        controllerInstance.strEmailFromUser = UserInfo.getUserId();
        controllerInstance.strAdditionalTo = strAdditionalTo;
        controllerInstance.strCC = strCC;
        controllerInstance.strBCC = strBCC;
        controllerInstance.sendEmail(); 
        
        controllerInstance.prepareSendEMail();
        //controllerInstance.getEmailAttachments();
        controllerInstance.getFromList();   
        controllerInstance.strEmailFromUser = UserInfo.getUserId();
        controllerInstance.strAdditionalTo = strAdditionalTo;
        controllerInstance.strCC = strCC;
        controllerInstance.subject = 
        controllerInstance.strBCC = strBCC;
        controllerInstance.deleteAttachment();
        controllerInstance.sendEmail();      
        
        List<Task> insertedTask = [SELECT Id, WhatId, Subject
                                   FROM Task 
                                   WHERE WhatId = :testCreditNote.Id];
        //System.assertNotEquals(0, insertedTask.size());
        //System.assert(insertedTask.get(0).Subject != null);             
        Test.stopTest();
    }
    
    static testMethod void testCustomSalesCreditNoteEmail2(){
                
        Test.startTest();
        
        // Refer the page to test 
        PageReference pageRef = Page.CustomSalesCreditNoteEmail;            
        Test.setCurrentPage(pageRef);  
        
         // Get the Sales Invoice record to test
        c2g__codaCreditNote__c  testCreditNote = [SELECT Id FROM c2g__codaCreditNote__c  LIMIT 1];
        System.currentPageReference().getParameters().put('Id', testCreditNote.Id);
        
        // Set the email template id to pass as parameter to the url
        EmailTemplate testEmailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'codaSalesCreditNoteStandard' LIMIT 1];
        System.currentPageReference().getParameters().put('emailTemplateId', testEmailTemplate.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testCreditNote);
        CustomSalesCreditNoteEmailExtension controllerInstance = new CustomSalesCreditNoteEmailExtension(sc);    
        
        controllerInstance.popupType = 'txtCC';
        controllerInstance.getRecordsInSelectList();
        controllerInstance.getItems();
        controllerInstance.txtSelectedCriteria = 'Users';
        
        controllerInstance.popupType = 'txtBCC';
        controllerInstance.getRecordsInSelectList();
        System.assertEquals(controllerInstance.txtSelectedCriteria, 'Users');
        
        Test.stopTest();
    }
    
     
     
    static testmethod void testAttachFile() {
        
        // Assign email ids to send test email
        String strTo = 'to@test.com';
        String strFrom = 'from@test.com';
        String strAdditionalTo = 'additionalTo@test.com';
        String strCC = 'cc@test.com';
        String strBCC = 'bcc@test.com';
        String subject = 'Email: Testing CustomSalesInvoiceEmail Page Email';
        
        //--Set current page
        PageReference pageRef = Page.CustomSalesCreditNoteEmail;         
        Test.setCurrentPage(pageRef);
        // Get the Sales Invoice record to test
        c2g__codaCreditNote__c  testCreditNote = [SELECT Id FROM c2g__codaCreditNote__c 
                                                    WHERE c2g__Opportunity__r.Invoicing_contact__c != null  LIMIT 1];
        System.currentPageReference().getParameters().put('Id', testCreditNote.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(testCreditNote);
        CustomSalesCreditNoteEmailExtension controllerInstance = new CustomSalesCreditNoteEmailExtension(sc);            
        //--Verify action function methods
        String strJson = '';
        Apexpages.currentPage().getParameters().put('fileInstance',strJson);
        controllerInstance.getSelectedEmailAttachment();
        
        controllerInstance.strAdditionalTo = strAdditionalTo;
        controllerInstance.strCC = strCC;
        controllerInstance.strBCC = strBCC;        
        controllerInstance.subject = subject;
        controllerInstance.emailPlainBody = 'Test body';
        
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setBody(Blob.valueof('TEST BODY'));
        attachment.setFileName('Cost Plus Merchants.csv');
        
        controllerInstance.createSalesCreditNoteEmailTask(new List<Messaging.EmailFileAttachment>{attachment});
        
        CustomSalesCreditNoteEmailExtension.EmailAttachmentWrapper emaiAttaWrap = new CustomSalesCreditNoteEmailExtension.EmailAttachmentWrapper();
        emaiAttaWrap.getfileSize();
        Document documentRec = [ SELECT Id, Name, bodyLength FROM Document LIMIT 1 ];
        
        String strJson1 = '[{"Id":"'+
                            documentRec.Id+
                            '","Name":"'+
                            documentRec.Name+
                            '","size":"'+
                            documentRec.bodyLength+
                            '","Type":"Document"}]';
        
        Apexpages.currentPage().getParameters().put('fileInstance',strJson1);
        controllerInstance.getSelectedEmailAttachment();
        System.assertEquals(controllerInstance.mapEmailAttachments.size(), 1); 
    }    
}