global class scheduledGenerateExchangeRates implements Schedulable
{   
   global void execute(SchedulableContext SC) 
   {
       confirmCurrentCompany();
      generateExchangeRates.insertExchageRates();          
   }
   
   public static void confirmCurrentCompany() {
        try {
            //Check if current company is "FFICEFGmbH"
            List<GroupMember> existingGroupMembers = new List<GroupMember>();
            existingGroupMembers = [SELECT id, UserOrGroupId, GroupId 
                                    FROM GroupMember 
                                    WHERE Group.developername = 'FFICEFGmbH' AND UserOrGroupId =: UserInfo.getUserId()];
            
            //If current company is not "FFICEFGmbH" 
            if(!(existingGroupMembers.size() > 0)) {
                //Check group for "FFICEFGmbH"
                System.debug('Checking Group for "ICEF GmbH"');
                Group ICEFGroup;
                List<Group> existingGroups = new List<Group>();
                existingGroups = [SELECT id, name, developername, type 
                                  FROM Group 
                                  WHERE developername = 'FFICEFGmbH'];
                if(existingGroups.size()>0){
                    ICEFGroup = existingGroups[0];      
                }
                else {
                    ICEFGroup = new Group(developername = 'FFICEFGmbH', Name = 'FF ICEF GmbH', Type = 'Queue');
                    insert ICEFGroup;
                }
                
                System.debug('Creating new group member for "ICEF GmbH"');
                GroupMember newGroupMmber = new GroupMember(GroupId = ICEFGroup.Id, UserOrGroupId = UserInfo.getUserId());
                insert newGroupMmber;
                
                //deleting group member of "FFICEFAsiaPacific"
                List<GroupMember> otherGroupMembers = new List<GroupMember>();
                otherGroupMembers = [SELECT id, UserOrGroupId, GroupId 
                                     FROM GroupMember 
                                     WHERE Group.DeveloperName = 'FFICEFAsiaPacific' AND UserOrGroupId =: UserInfo.getUserId()];
                if(otherGroupMembers.size() > 0) {
                    delete otherGroupMembers;
                }
            }
            else{
                System.debug('Selected Company is "ICEF GmbH"');
            }
        }
        catch(Exception e) {
            System.debug('Excpetion caught:::'+e.getMessage());
        }
    }
    
}