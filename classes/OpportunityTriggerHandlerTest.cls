@isTest(SeeAllData = true)
public class OpportunityTriggerHandlerTest {
   
    static testMethod void testConversion() {
        
        Id testAcctId = [SELECT ID FROM Account Limit 1].Id;
        
        Opportunity opportunityToConvertRate = new Opportunity();
        opportunityToConvertRate.Name ='New mAWS Deal';
        opportunityToConvertRate.AccountID = testAcctId ;
        opportunityToConvertRate.StageName = 'Generate Opportunity';
        opportunityToConvertRate.Amount = 3000;
        opportunityToConvertRate.CloseDate = System.today();
        insert opportunityToConvertRate;
        
        opportunityToConvertRate.Amount = 300;
        update opportunityToConvertRate;
        opportunityToConvertRate = [SELECT Id, Converted_Amount__c FROM Opportunity WHERE Id = :opportunityToConvertRate.Id LIMIT 1];
        System.assertNotEquals(0, opportunityToConvertRate.Converted_Amount__c);
        opportunityToConvertRate.Amount = 0;
        update opportunityToConvertRate;                
        
    }
    static testMethod void testBulkConversion() {
        
        Id testAcctId = [SELECT ID FROM Account Limit 1].Id;
        List<Opportunity>listNewopportunity = new List<Opportunity>();
        Opportunity opportunityToConvertRate;
        for(Integer Count =0 ; Count <=202 ; Count++) {
            opportunityToConvertRate = new Opportunity();
            opportunityToConvertRate.Name ='New mAWS Deal';
            opportunityToConvertRate.AccountID = testAcctId;
            opportunityToConvertRate.StageName = 'Generate Opportunity';
            opportunityToConvertRate.Amount = 3000;
            opportunityToConvertRate.CloseDate = System.today();
            listNewopportunity.add(opportunityToConvertRate);
        }
        insert listNewopportunity ;
        
        for(Opportunity opportunityToConvertAmount :listNewopportunity ){
            opportunityToConvertAmount.Amount = 300;
            
        } 
        update listNewopportunity;
        opportunityToConvertRate = [SELECT Id, Converted_Amount__c FROM Opportunity WHERE ID IN :listNewopportunity LIMIT 1];
        System.assertNotEquals(0, opportunityToConvertRate.Converted_Amount__c);                  
    }   
    
    
    static testMethod void testCreditConversion() {
    
        // Set up some local variables
        String standardPriceBookId = '';
    
        PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        standardPriceBookId = pb2Standard.Id;
    
        Id testAcctId = [SELECT ID FROM Account Limit 1].Id;

        Opportunity opportunityToConvertRate = new Opportunity();
        opportunityToConvertRate.Name ='New mAWS Deal';
        opportunityToConvertRate.AccountID = testAcctId;
        opportunityToConvertRate.StageName = 'Generate Opportunity';
        opportunityToConvertRate.Amount = 3000;
        opportunityToConvertRate.CloseDate = System.today();
        insert opportunityToConvertRate;
        
       // set up opp and Verify that the results are as expected.
       
        Opportunity opportunityRecord = [SELECT Name FROM Opportunity WHERE Id = :opportunityToConvertRate.Id];
    
        // set up product2 and Verify that the results are as expected.
        Product2 productTest = new Product2(Name='Test Product',isActive=true, Amount_of_added_CF_Credit__c = 22);
        insert productTest;
        Product2 productRecordTest = [SELECT Name FROM Product2 WHERE Id = :productTest.Id];
        System.assertEquals('Test Product', productRecordTest.Name);
    
        // set up PricebookEntry and Verify that the results are as expected.
        PricebookEntry pricebookEntry = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=productTest.Id, UnitPrice=99, isActive=true);
        insert pricebookEntry;
        PricebookEntry pricebookEntryRecord = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pricebookEntry.Id];
        System.assertEquals(standardPriceBookId, pricebookEntryRecord.Pricebook2Id);       
    }     
   
}