@isTest
private class CoursefinderSchoolTriggerHelperTest {
    
    static testMethod void createBasicTestData(){
        
        List<Account> accountsToBeInserted = new List<Account>();
        
        //School Account
    	List<Account> eduAccounts = new List<Account>();
        eduAccounts = DataFactory.createTestEducators(1);
		accountsToBeInserted.addAll(eduAccounts);        
        
        //4 Agency Accounts
        List<Account> agentAccounts = new List<Account>();
        agentAccounts = DataFactory.createTestAgents(3);
        accountsToBeInserted.addAll(agentAccounts);

        insert accountsToBeInserted;
        
        //CF school
        Account eduAccount = [SELECT Id 
                              FROM Account
                              WHERE Name = 'Test Educator 0'];
        Coursefinder_School__c cfSchool = DataFactory.createTestCoursefinderSchool(eduAccount.Id, 'Test CF School');
        
        insert cfSchool;
        
        //3 active Coursepricer Agencies
        List<Account> agentAccts = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Agent' LIMIT 3];
        List<CoursePricer_Agency__c> cpAgencies = DataFactory.createTestCoursePricerAgencies(agentAccts, cfSchool);
        
        insert cpAgencies;
    }
	
    //test if number of links is counted correctly and last added agency (trigger used... add without trigger?)
    //success 30.07.2018
    static testMethod void testupdateNamesOfAgencies(){
        
        createBasicTestData();
        List<Account> agencyAccounts = DataFactory.createTestAgents(5);
        insert agencyAccounts;
        Account agency = [SELECT Id, Name FROM Account WHERE Name = 'Test Agent 4'];
        
        Coursefinder_School__c cfSchool = [SELECT Id 
                                          FROM Coursefinder_School__c
                                          WHERE Name = 'Test CF School'];
        CoursePricer_Agency__c cpAgency = DataFactory.createTestCoursePricerAgency(CfSchool, agency);
        insert cpAgency;
        
        Coursefinder_School__c cfSchoolAfterInsert = [SELECT Id, CP_Agency_last_added__c, CP_links_on_external_sites__c, CP_Agency_Names__c
                                          			FROM Coursefinder_School__c
                                          			WHERE Name = 'Test CF School'];
        System.assertEquals('Test Agent 4', cfSChoolAfterInsert.CP_Agency_last_added__c);
        System.assertEquals(4, cfSchoolAfterInsert.CP_links_on_external_sites__c);
        System.assertEquals('Test Agent 4\nTest Agent 0\nTest Agent 1\nTest Agent 2', cfSchoolAfterInsert.CP_Agency_Names__c);
    }
    
    //test for case when all CF agencies are inactive. 
    //expected: - CP Agency Names is empty
    //			- CP Agency links is empty
    //			- CP Agency last added is empty
    static testMethod void testNoAgenciesLeft(){
		List<Account> eduAccounts = DataFactory.createTestEducators(1);
        insert eduAccounts;
        Account eduAccount = eduAccounts[0];
        System.debug('eduAccount: '+eduAccount.Id);
        
        Coursefinder_School__c cfSchool = DataFactory.createTestCoursefinderSchool(eduAccount.Id, 'schoolName');
        insert cfSchool;
        System.debug('cfSchool: '+ cfSchool);
        Coursefinder_School__c testSchool = [SELECT Id, CP_Agency_Names__c, CP_Agency_links__c, CP_Agency_last_added__c FROM Coursefinder_School__c WHERE Id = :cfSchool.Id];
        
        List<Account> agencies = DataFactory.createTestAgents(1);
        insert agencies;
        Account agency = agencies[0];
        
        CoursePricer_Agency__c cpAgency = DataFactory.createTestCoursePricerAgency(testSchool, agency);
        insert cpAgency;
        cpAgency.currently_active__c = false;
        update cpAgency;
        
        Map<Id, Coursefinder_School__c> cfSchoolMap = new Map<Id, Coursefinder_School__c>();
        cfSchoolMap.put(testSchool.Id, testSchool);
        System.debug('cfSchoolMap: ' + cfSchoolMap);
        
        // test method updateNamesOfAgencies for CF school without agencies
        coursefinderSchoolTriggerHelper.updateNamesOfAgencies(cfSchoolMap);
        Coursefinder_School__c cfSchoolEnd = [SELECT Id, CP_Agency_Names__c, CP_Agency_links__c, CP_Agency_last_added__c FROM Coursefinder_School__c WHERE Id = :cfSchool.Id];
        System.debug('cfSchoolEnd: '+cfSchoolEnd);
        System.debug('CP_Agency_Names__c: '+cfSchoolEnd.CP_Agency_Names__c);
        System.debug('links: '+ cfSchoolEnd.CP_Agency_links__c);
        System.debug('last Agency: '+ cfSchoolEnd.CP_Agency_last_added__c);
//        System.assertEquals('', '');
        System.assertEquals(null, cfSchoolEnd.CP_Agency_Names__c);
        System.assertEquals(null, cfSchoolEnd.CP_Agency_links__c);
        System.assertEquals(null, cfSchoolEnd.CP_Agency_last_added__c);
    }
}