/**
  * @Author      : Prajakta
  * @Description : Batch To Update Account Participation records
  * @Created Date: 08 Sept 2016
 */
public class BatchToUpdateAccountParticipation implements Database.Batchable<sObject> {
    
    List<String> accountParticipationIds = new List<String>();
    String hotelCapacityId;
    //String failedRecordDetalis = '';

    //-- Parameterized constructor
    public BatchToUpdateAccountParticipation(String accountPartcipationIds, String hotelCapacityId) {
        this.accountParticipationIds = accountPartcipationIds.split(',');
        this.hotelCapacityId = hotelCapacityId;
    }
    
    //-- start method to get account participation records
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator( [ SELECT Id, Hotel_Capacity__c 
                                           FROM Account_Participation__c 
                                           WHERE Id IN : accountParticipationIds] );
    }
    
    //-- Update Hotel_Capacity__c of retrieved account participation records with provided hotelCapacityId
    public void execute(Database.BatchableContext BC, List<Account_Participation__c> accountParticipations) {
       
        for(Account_Participation__c accountParticipation : accountParticipations){
            accountParticipation.Hotel_Capacity__c = hotelCapacityId;
        }
        Database.SaveResult[] results = Database.update(accountParticipations);
        
        /*if (results != null){
            for (Database.SaveResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] errs = result.getErrors();
                    failedRecordDetalis  += result.getId();
                    for(Database.Error err : errs) {
                        System.debug(err.getStatusCode() + ' - ' + err.getMessage());
                        failedRecordDetalis += ' ' + err.getMessage();
                    }
                }
            }
        }*/
    }
    
    //-- Finish method to send an error updates to admin
    public void finish(Database.BatchableContext BC){
        /*Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();  
        message.subject = 'BatchToUpdateAccountParticipation' + ' - Result';
        message.plainTextBody = failedRecordDetalis;
        message.toAddresses = new String[] {'prajakta@dreamwares.com'};
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);  */
    }
}