@isTest
public class ICEFCourseProfilesTreeViewControllerTest {

    @isTest
    public static void testCourseProfileDate(){
        List<ICEF_Event__c> ICEFEventList = new List<ICEF_Event__c>();
        
        Country__c country = new Country__c( Name = 'New Zealand',
                                             Agent_Relationship_Manager__c = UserInfo.getUserId());
        insert country; 

        Id accountAgentRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agent').getRecordTypeId();
        
        Account account = new Account( RecordTypeId = accountAgentRecordTypeId,
                                       Name = 'test Account',
                                       CurrencyIsoCode = 'EUR',
                                       Status__c = 'Active',
                                       Mailing_Country__c = country.Id,
                                       c2g__CODAAccountTradingCurrency__c = 'EUR');
        insert account; 
        
        Contact contact = new Contact( Salutation = 'Mr',
                                       LastName = 'test contact',
                                       Gender__c = 'male',
                                       AccountId = account.Id,
                                       Role__c = 'Other',
                                       Contact_Status__c = 'Active' );
        insert contact;                            
        
        ICEF_Event__c event = new ICEF_Event__c( Name = 'test',
                                                  Event_Manager_Educators__c = UserInfo.getUserId(),
                                                  Event_Manager_Exhibitors__c = UserInfo.getUserId(),
                                                  Event_Manager_Agents__c = UserInfo.getUserId(),
                                                  Event_City__c = 'City',
                                                  Event_City_2__c = 'Citi 2',
                                                  Event_Country__c = country.Id,
                                                  Event_Country_2__c = country.Id,
                                                  technical_event_name__c = 'test event ',
                                                  catalogue_spelling__c = 'British English',
                                                  catalogue_format__c = 'A4',
                                                  form_edu_available_currency__c = 'EUR');
        
        insert event;                           
        
        Id agentRecordTypeId = Schema.SObjectType.Account_Participation__c.getRecordTypeInfosByName().get('Agent').getRecordTypeId();
        
        Account_Participation__c accountParticipation = new Account_Participation__c( RecordTypeId = agentRecordTypeId,
                                                                                      Account__c = account.Id,
                                                                                      ICEF_Event__c = event.Id,
                                                                                      Country_Section__c = country.Id,
                                                                                      Participation_Status__c = 'accepted',
                                                                                      Organisational_Contact__c = contact.Id,
                                                                                      Catalogue_Country__c = country.Id,
                                                                                      Catalogue_City__c = 'Wellington',
                                                                                      Primary_Recruitment_Country__c = country.Id );
                                                                                      
        insert  accountParticipation;       

        List<Course_Profile__c> coursProfileList = new List<Course_Profile__c>();
        
        Id workRecordTypeId = Schema.SObjectType.Course_Profile__c.getRecordTypeInfosByName().get('Workshop Profile').getRecordTypeId();
        
        for( Integer i = 1; i <= 200; i++ ){
            coursProfileList.add( new Course_Profile__c( RecordTypeId = workRecordTypeId,
                                                         Account__c = account.Id,
                                                         Account_Participation__c = accountParticipation.Id,
                                                         Language_Arabic_in__c = 'Antigua and Barbuda;Australia;Bahamas;Brazil;Canada;Croatia',
                                                         Language_Japanese_in__c = 'Austria',
                                                         Language_Chinese_in__c = 'Antigua and Barbuda;Australia;Bahamas;Brazil;Canada;Croatia',
                                                         Language_English_in__c = 'Austria',
                                                         Language_French_in__c = 'Antigua and Barbuda;Australia;Bahamas;Brazil;Canada;Croatia',
                                                         Language_German_in__c = 'Austria',
                                                         Language_Italian_in__c = 'Antigua and Barbuda;Australia;Bahamas;Brazil;Canada;Croatia',
                                                         Language_Korean_in__c = 'Austria',
                                                         Language_Portuguese_in__c = 'Austria',
                                                         Language_Russian_in__c = 'Austria',
                                                         Language_Spanish_in__c = 'Antigua and Barbuda;Australia;Bahamas;Brazil;Canada;Croatia',
                                                         Type_of_Language_Course__c = 'General') );
        }
        insert coursProfileList;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(event);
        ICEFCourseProfilesTreeViewController controller = new ICEFCourseProfilesTreeViewController(sc);
        
        PageReference pageRef = Page.ICEFAgentCourseProfiles;
        pageRef.getParameters().put('id', String.valueOf(event.Id));
        Test.setCurrentPage(pageRef);
        
        String t = ICEFCourseProfilesTreeViewController.showCourseProfileData(event.Id);
        System.debug('t..'+t);
    }
}