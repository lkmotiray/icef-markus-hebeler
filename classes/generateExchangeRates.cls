global with sharing class generateExchangeRates implements Database.AllowsCallouts
{
    public static List<c2g__codaExchangeRate__c> exchangeRates;
    public static List<c2g__codaAccountingCurrency__c> allAcctCurrencies;   
    public static String UnitTestXml;
    public static String unitTestHtml;
        
    @future(callout=true)
    public static void insertExchageRates() 
    {        
        /*HttpRequest req = new HttpRequest();
        Http http = new Http();
        req.setMethod('GET');
        String ecbDoc, exchangeRatesDoc;
        
            //req.setTimeout(60000); 
            //String url = 'http://www.ecb.int/stats/exchange/eurofxref/html/index.en.html';    
            //--Changed url becoz above url is moved permanently to below new url
        String url = 'http://www.ecb.europa.eu/stats/exchange/eurofxref/html/index.en.html'; 
        req.setEndpoint(url);
        if (unitTestHtml == null) 
        {
            HTTPResponse resp = http.send(req);        
            ecbDoc = resp.getBody();  
        }
        else
        {
            ecbDoc = unitTestHtml;
        }      
        System.debug('ecbDoc::'+ecbDoc);
        
        integer startpos = ecbDoc.indexOf('XML file available for parsing:');
        integer endpos = ecbDoc.indexof('</div>', startpos+32);
        
        string ecbXmlUrl = ecbDoc.substring(startpos+32, endpos);
        System.debug('ecbXmlUrl:::'+ecbXmlUrl);*/
        
        String ecbXmlUrl = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml'; //URL for fetching XML doc  
        String exchangeRatesDoc; 
        
        HttpRequest request = new HttpRequest();   
        Http http = new Http();     
        request.setMethod('GET');
        request.setEndpoint(ecbXmlUrl);        
        
        if(UnitTestXml == null)
        {
            HTTPResponse response = http.send(request);
            exchangeRatesDoc = response.getBody();
        }
        else
            exchangeRatesDoc = UnitTestXml;
        System.debug('exchangeRatesDoc:::'+exchangeRatesDoc);
        
        DOM.Document doc = new DOM.Document();
        doc.load(exchangeRatesDoc);   
        DOM.XMLNode root = doc.getRootElement();
        List<Dom.XmlNode> lstEntries = DOM_Utils.getElementsByName(root, 'Cube');
        System.debug('lstEntries::'+lstEntries);
        
        exchangeRates = new List<c2g__codaExchangeRate__c>();
        allAcctCurrencies = new List<c2g__codaAccountingCurrency__c>();
        
        Date exchangeStartDate;
        String str_exchangeStartDate;
            
        for(Dom.XmlNode entry :lstEntries)
        {
            if (entry.getAttributeCount() == 1) 
            {
                str_exchangeStartDate = entry.getAttributeValue(entry.getAttributeKeyAt(0), entry.getAttributeKeyNsAt(0));
                exchangeStartDate = Date.valueOf(str_exchangeStartDate);
            }
        }           
            
        for(Dom.XmlNode entry :lstEntries)
        {
            if (entry.getAttributeCount() == 2) 
            {
                string currency1 = entry.getAttributeValue(entry.getAttributeKeyAt(0), entry.getAttributeKeyNsAt(0));                    
                string rate_str = entry.getAttributeValue(entry.getAttributeKeyAt(1), entry.getAttributeKeyNsAt(1));
                System.debug('result:: '+currency1+' Rate result::'+rate_str); 
                Decimal rate = Decimal.valueof(rate_str);
                System.debug('rate:::'+rate); 
                ID cmpnyId = null;
                List<c2g__codaCompany__c> cmpny = [Select Id, Name from c2g__codaCompany__c where Name = 'ICEF GmbH'];
                if(cmpny.size() != 0)
                {
                    cmpnyId = cmpny[0].Id;
                }
                List<c2g__codaAccountingCurrency__c> acctCurrency = [Select ID, Name, c2g__OwnerCompany__c from c2g__codaAccountingCurrency__c where Name =: currency1 and c2g__Home__c = False and c2g__OwnerCompany__c =: cmpnyId];
                if(acctCurrency.size() != 0)
                {
                    
                    for(integer i = 0; i < acctCurrency.size(); i++)
                    {
                        allAcctCurrencies.add(acctCurrency[i]);
                        List<c2g__codaExchangeRate__c> allExchangeRates = [Select c2g__ExchangeRateCurrency__c, c2g__StartDate__c from c2g__codaExchangeRate__c where c2g__ExchangeRateCurrency__c =: acctCurrency[i].Id AND c2g__StartDate__c =: exchangeStartDate];
                        if(allExchangeRates.size() == 0)
                        {
                            c2g__codaExchangeRate__c eRate = new c2g__codaExchangeRate__c(c2g__ExchangeRateCurrency__c = acctCurrency[i].Id, c2g__Rate__c = rate, c2g__StartDate__c = exchangeStartDate);
                            exchangeRates.add(eRate);
                        }
                    }
                }            
            }            
        }   
        try
        {         
            if(exchangeRates.size() != 0)
            {
                insert exchangeRates;           
                            
                //Send email after successful insertion of exchange rates with recent five values of exchange rates
                User u = [Select Name, Email, Id from User where Id =: UserInfo.getUserId()];
                
                String subject = 'Exchange Rates are inserted successfully';
                String body = 'Hello '+u.Name + ',';
                body += '<br></br><br></br>';
                body += 'Exchange Rates are inserted successfully. Following are the recent five exchage rates for particular Exchange Rate Currency. <br></br><br></br>';
                body += 'Exchange Rate Currency | Rate | Start Date<br></br><br></br>------------------------------------------------------------<br></br><br></br>';
                for(integer i = 0; i < allAcctCurrencies.size(); i++)
                {
                    List<c2g__codaExchangeRate__c> allExchRates = [Select c2g__ExchangeRateCurrency__c, c2g__Rate__c, c2g__StartDate__c from c2g__codaExchangeRate__c where c2g__ExchangeRateCurrency__c =: allAcctCurrencies[i].Id ORDER BY c2g__StartDate__c DESC LIMIT 5];
                    for(integer j = 0; j < allExchRates.size(); j++)
                    {
                        body += allAcctCurrencies[i].Name + ' | ' + allExchRates[j].c2g__Rate__c + '  | ' + allExchRates[j].c2g__StartDate__c;
                        body += '<br></br>';
                    }  
                    body += '------------------------------------------------------------<br></br><br></br>';              
                }
                
                body += 'Thanks,';
                
                string[] toemailaddr = new string[]{};
                
                //toemailaddr.add(u.Email);
                toemailaddr.add('jsarfraz@icef.com');
                
                sendEmail(subject, body, toemailaddr);
            }            
        }catch(Exception e)
        {
            //Send email after unsuccessful insertion of exchange rates with error
            User u = [Select Name, Email, Id from User where Id =: UserInfo.getUserId()];            
            
            String subject = 'Exchange Rates are not inserted successfully';
            String body = 'Hello '+u.Name + ',';
            body += '<br></br><br></br>';
            body += 'Exchange Rates are not inserted successfully, because of following error: <br><br/>'+ e + '.' + '<br></br><br></br>';
            
            String error = e.getMessage();
            
            if(error.contains('multi-company mode'))
            {
                body += 'Please select only one company as the current company. Because data entry is disabled until the user returns to single-company mode.<br></br><br></br>'; 
            }
            
            body += 'Please do the following steps to schedule <b>Update Exchange Rates</b> job again:<br></br>';
            body += '1. Go to Setup -> Administrative Setup -> Monitoring -> Scheduled Jobs <br></br>'; 
            body += '2. Delete <b>Update Exchage Rate Schedule</b> scheduled job<br></br>'; 
            body += '3. Click Accounting Currencies tab, then Click on Go<br></br>';
            body += '4. Click on <b>Update Exchange Rates</b> button<br></br><br></br>';
            
            body += 'Thanks,';
            
            string[] toemailaddr = new string[]{};
            
            //toemailaddr.add(u.Email); 
            toemailaddr.add('jsarfraz@icef.com');
            toemailaddr.add('jlove@icef.com');     
            toemailaddr.add('mhebeler@icef.com');       
            sendEmail(subject, body, toemailaddr);            
        }
    }  
    
    
    public static void sendEmail(string subject, string body, string[] toemailaddr)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();   
        
        email.setSubject(subject);            
        
        email.setHtmlBody(body);            
        
        email.setToAddresses(toemailaddr);
        
        email.setUseSignature(true);       
                
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });    
    }
    
    webService Static void scheduleUpdateExchangeRates()
    {
        insertExchageRates();
        string cronId = '';       
        
        scheduledGenerateExchangeRates m = new scheduledGenerateExchangeRates(); 
        cronId = system.schedule('Update Exchage Rate Schedule', '0 0 16 * * ?', m);     
    }
    
}