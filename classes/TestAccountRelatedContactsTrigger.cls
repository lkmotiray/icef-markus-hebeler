/*
 *  @purpose Test class for trigger to tick X20_year_mag__c  of Account if any related contact's X20_year_mag__c is ticked; Also Count number of account related contacts whose role is Primary Decision Maker
 *  @createdby Mamta Kukreja, Amol Kulthe
 *  @createddate 16/6/15
 */
 
@isTest
public class TestAccountRelatedContactsTrigger {

    /*
      Method Name :- testContactTriggerWithBulkData
      purpose     :- check whether the Account fields X20_year_mag__c and Number_of_PMD_Contacts__c are updating or not 
      createdby   :- Mamta Kukreja, Amol K
    */
    /*
    static testMethod void testContactTriggerWithBulkData() {
        // select record type
        RecordType recType = [SELECT Id, Name 
                              FROM RecordType 
                              WHERE Name = 'Agent' 
                               AND SobjectType='Account'];
        
        Country__c testCountry = new Country__c(Name = 'test', Region__c = 'testRegion');
        testCountry.Agent_Relationship_Manager__c = UserInfo.getUserId();
        insert testCountry;
        Test.startTest();
        
        AccountRelatedContactsTriggerHandler handler = new AccountRelatedContactsTriggerHandler();

        // Insert Account 
        Account testAccount = new Account(Name = 'Test Account', RecordTypeId = recType.Id, Status__c = 'Active', Mailing_Country__c = testCountry.id);
        testAccount.Mailing_State_Province__c = 'NY';
        testAccount.CurrencyIsoCode = 'USD';
        testAccount.Educational_Sector__c = '11 UNIVERSITIES';
        testAccount.ARM_emailaddress__c = 'test@mail.com';
        insert testAccount;
        
        //Check whether account's checkbox is initially unticked
        Account checkAccountRecord = [SELECT X20_year_mag__c FROM Account WHERE name = 'Test Account' LIMIT 1];
        System.assertEquals(false, checkAccountRecord.X20_year_mag__c);
    
        // Insert contact
        List<Contact>listTestContacts = new List<Contact>();
        Contact testContact;
        
        for(Integer COUNT = 0; COUNT < 150 ; COUNT++) {
            if(COUNT != 18) {
                testContact = new Contact(Salutation = 'Dr.', FirstName = 'first_name'+COUNT, LastName = 'Test'+COUNT, Gender__c = 'Male', Role__c = 'Primary Decision Maker', Contact_Status__c = 'Active', AccountId = testAccount.Id);
            }
            else{
                testContact = new Contact(Salutation = 'Dr.', FirstName = 'first_name' +COUNT, LastName = 'Test'+COUNT, Gender__c = 'Male', Role__c = 'Primary Decision Maker', Contact_Status__c = 'Active', AccountId = testAccount.Id, X20_year_mag__c = true);
            }
            listTestContacts.add(testContact);
        }
        insert listTestContacts;
        
        //Check if X20_year_mag__c field of account object is ticked 
        checkAccountRecord = [SELECT X20_year_mag__c FROM Account WHERE name = 'Test Account' LIMIT 1];
        //System.assertEquals(true, checkAccountRecord.X20_year_mag__c);
        
        // Check field Number_of_PMD_Contacts__c updated or not         
        testAccount = [SELECT Id, Name, Number_of_PMD_Contacts__c FROM Account LIMIT 1];        
        //System.AssertEquals(testAccount.Number_of_PMD_Contacts__c,150);
        //handler.IsRunFirstTime = true;  
        AccountRelatedContactsTriggerHandler.IsRunFirstTime = true;  
        
        //untick one contact's checkbox and check whether account's checkbox is unticked too
        Contact con = [SELECT X20_year_mag__c FROM Contact WHERE FirstName = 'first_name18' AND AccountId = :testAccount.id LIMIT 1];
        con.X20_year_mag__c = false;
        update con;
        
        checkAccountRecord = [SELECT X20_year_mag__c FROM Account WHERE name = 'Test Account' LIMIT 1];
        System.assertEquals(false, checkAccountRecord.X20_year_mag__c);
        
        //Tick it again
        con.X20_year_mag__c = true;
        update con;
        
        listTestContacts = [SELECT Id, Role__c FROM Contact LIMIT 150]; 
        delete listTestContacts;
        
        // Check field Number_of_PMD_Contacts__c updated or not         
        testAccount = [SELECT Id, Name, Number_of_PMD_Contacts__c FROM Account LIMIT 1];
        System.AssertEquals(testAccount.Number_of_PMD_Contacts__c,0);
        
        checkAccountRecord = [SELECT X20_year_mag__c FROM Account WHERE name = 'Test Account' LIMIT 1];
        System.assertEquals(false, checkAccountRecord.X20_year_mag__c);
        
        //handler.IsRunFirstTime = true;        
        AccountRelatedContactsTriggerHandler.IsRunFirstTime = true;  
        undelete listTestContacts;
        
        // Check field Number_of_PMD_Contacts__c updated or not 
        testAccount = [SELECT Id, Name, Number_of_PMD_Contacts__c FROM Account LIMIT 1];        
        //System.AssertEquals(testAccount.Number_of_PMD_Contacts__c,150);      
        //handler.IsRunFirstTime = true;
        AccountRelatedContactsTriggerHandler.IsRunFirstTime = true;  
        listTestContacts = [SELECT Id, Role__c FROM Contact LIMIT 150]; 
        
        for(Contact updateContact : listTestContacts){
            updateContact.Role__c = 'Other';            
        }
        update listTestContacts;       
        
        // Check field Number_of_PMD_Contacts__c updated or not         
        //testAccount = [SELECT Id, Name, Number_of_PMD_Contacts__c FROM Account LIMIT 1];
        //System.AssertEquals(testAccount.Number_of_PMD_Contacts__c,0);

        Test.stopTest();
    }*/
    
    static testMethod void testContactTriggerWithBulkData1() {
        // select record type
        RecordType recType = [SELECT Id, Name 
                              FROM RecordType 
                              WHERE Name = 'Agent' 
                               AND SobjectType='Account'];
        
        Country__c testCountry = new Country__c(Name = 'test', Region__c = 'testRegion');
        testCountry.Agent_Relationship_Manager__c = UserInfo.getUserId();
        insert testCountry;
        
        // Insert Account 
        Account testAccount = new Account(Name = 'Test Account', RecordTypeId = recType.Id, Status__c = 'Active', Mailing_Country__c = testCountry.id);
        testAccount.Mailing_State_Province__c = 'NY';
        testAccount.CurrencyIsoCode = 'USD';
        testAccount.Educational_Sector__c = '11 UNIVERSITIES';
        testAccount.ARM_emailaddress__c = 'test@mail.com';
        insert testAccount;
                
        Test.startTest();
        
        AccountRelatedContactsTriggerHandler handler = new AccountRelatedContactsTriggerHandler();

        //Check whether account's checkbox is initially unticked
        Account checkAccountRecord = [SELECT X20_year_mag__c FROM Account WHERE name = 'Test Account' LIMIT 1];
        System.assertEquals(false, checkAccountRecord.X20_year_mag__c);
    
        // Insert contact
        List<Contact>listTestContacts = new List<Contact>();
        Contact testContact;
        
        for(Integer COUNT = 0; COUNT < 150 ; COUNT++) {
            if(COUNT != 18) {
                testContact = new Contact(Salutation = 'Dr.', FirstName = 'first_name'+COUNT, LastName = 'Test'+COUNT, Gender__c = 'Male', Role__c = 'Primary Decision Maker', Contact_Status__c = 'Active', AccountId = testAccount.Id);
            }
            else{
                testContact = new Contact(Salutation = 'Dr.', FirstName = 'first_name' +COUNT, LastName = 'Test'+COUNT, Gender__c = 'Male', Role__c = 'Primary Decision Maker', Contact_Status__c = 'Active', AccountId = testAccount.Id, X20_year_mag__c = true);
            }
            listTestContacts.add(testContact);
        }
        insert listTestContacts;
        
        Test.stopTest();
        //Check if X20_year_mag__c field of account object is ticked 
        checkAccountRecord = [SELECT X20_year_mag__c FROM Account WHERE name = 'Test Account' LIMIT 1];
        //System.assertEquals(true, checkAccountRecord.X20_year_mag__c);
    }
    
    static testMethod void testContactTriggerWithBulkData2() {
        // select record type
        RecordType recType = [SELECT Id, Name 
                              FROM RecordType 
                              WHERE Name = 'Agent' 
                               AND SobjectType='Account'];
        
        Country__c testCountry = new Country__c(Name = 'test', Region__c = 'testRegion');
        testCountry.Agent_Relationship_Manager__c = UserInfo.getUserId();
        insert testCountry;
        
        // Insert Account 
        Account testAccount = new Account(Name = 'Test Account', RecordTypeId = recType.Id, Status__c = 'Active', Mailing_Country__c = testCountry.id);
        testAccount.Mailing_State_Province__c = 'NY';
        testAccount.CurrencyIsoCode = 'USD';
        testAccount.Educational_Sector__c = '11 UNIVERSITIES';
        testAccount.ARM_emailaddress__c = 'test@mail.com';
        insert testAccount;
                               
        AccountRelatedContactsTriggerHandler handler = new AccountRelatedContactsTriggerHandler();

        //Check whether account's checkbox is initially unticked
        Account checkAccountRecord = [SELECT X20_year_mag__c FROM Account WHERE name = 'Test Account' LIMIT 1];
        System.assertEquals(false, checkAccountRecord.X20_year_mag__c);
    
        // Insert contact
        List<Contact>listTestContacts = new List<Contact>();
        Contact testContact;
        
        for(Integer COUNT = 0; COUNT < 150 ; COUNT++) {
            if(COUNT != 18) {
                testContact = new Contact(Salutation = 'Dr.', FirstName = 'first_name'+COUNT, LastName = 'Test'+COUNT, Gender__c = 'Male', Role__c = 'Primary Decision Maker', Contact_Status__c = 'Active', AccountId = testAccount.Id);
            }
            else{
                testContact = new Contact(Salutation = 'Dr.', FirstName = 'first_name' +COUNT, LastName = 'Test'+COUNT, Gender__c = 'Male', Role__c = 'Primary Decision Maker', Contact_Status__c = 'Active', AccountId = testAccount.Id, X20_year_mag__c = true);
            }
            listTestContacts.add(testContact);
        }
        insert listTestContacts;
        
        AccountRelatedContactsTriggerHandler.IsRunFirstTime = true;  
        Test.startTest();
        //untick one contact's checkbox and check whether account's checkbox is unticked too
        Contact con = [SELECT X20_year_mag__c FROM Contact WHERE FirstName = 'first_name18' AND AccountId = :testAccount.id LIMIT 1];
        con.X20_year_mag__c = false;
        update con;
        Test.stopTest();       
        
        checkAccountRecord = [SELECT X20_year_mag__c FROM Account WHERE name = 'Test Account' LIMIT 1];
        System.assertEquals(false, checkAccountRecord.X20_year_mag__c);
    }  
    
    static testMethod void testContactTriggerWithBulkData3() {
        // select record type
        RecordType recType = [SELECT Id, Name 
                              FROM RecordType 
                              WHERE Name = 'Agent' 
                               AND SobjectType='Account'];
        
        Country__c testCountry = new Country__c(Name = 'test', Region__c = 'testRegion');
        testCountry.Agent_Relationship_Manager__c = UserInfo.getUserId();
        insert testCountry;
        
        // Insert Account 
        Account testAccount = new Account(Name = 'Test Account', RecordTypeId = recType.Id, Status__c = 'Active', Mailing_Country__c = testCountry.id);
        testAccount.Mailing_State_Province__c = 'NY';
        testAccount.CurrencyIsoCode = 'USD';
        testAccount.Educational_Sector__c = '11 UNIVERSITIES';
        testAccount.ARM_emailaddress__c = 'test@mail.com';
        insert testAccount;
                               
        AccountRelatedContactsTriggerHandler handler = new AccountRelatedContactsTriggerHandler();

        //Check whether account's checkbox is initially unticked
        Account checkAccountRecord = [SELECT X20_year_mag__c FROM Account WHERE name = 'Test Account' LIMIT 1];
        System.assertEquals(false, checkAccountRecord.X20_year_mag__c);
    
        // Insert contact
        List<Contact>listTestContacts = new List<Contact>();
        Contact testContact;
        
        for(Integer COUNT = 0; COUNT < 150 ; COUNT++) {
            if(COUNT != 18) {
                testContact = new Contact(Salutation = 'Dr.', FirstName = 'first_name'+COUNT, LastName = 'Test'+COUNT, Gender__c = 'Male', Role__c = 'Primary Decision Maker', Contact_Status__c = 'Active', AccountId = testAccount.Id);
            }
            else{
                testContact = new Contact(Salutation = 'Dr.', FirstName = 'first_name' +COUNT, LastName = 'Test'+COUNT, Gender__c = 'Male', Role__c = 'Primary Decision Maker', Contact_Status__c = 'Active', AccountId = testAccount.Id, X20_year_mag__c = true);
            }
            listTestContacts.add(testContact);
        }
        insert listTestContacts;
        
        AccountRelatedContactsTriggerHandler.IsRunFirstTime = true;  
        Test.startTest();
        AccountRelatedContactsTriggerHandler.IsRunFirstTime = true;  
        listTestContacts = [SELECT Id, Role__c FROM Contact LIMIT 150]; 
        
        for(Contact updateContact : listTestContacts){
            updateContact.Role__c = 'Other';            
        }
        update listTestContacts;
        
        Database.executeBatch(new AccountPMDBatch(), 2000);         
        Test.stopTest();
        checkAccountRecord = [SELECT X20_year_mag__c FROM Account WHERE name = 'Test Account' LIMIT 1];
        System.assertEquals(true, checkAccountRecord.X20_year_mag__c);
        
        
        
    }  
}