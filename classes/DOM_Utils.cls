public class DOM_Utils {

    // ------------------------------------------------------------------------------------------
    // For the given Node and all its children (recursively), get all Nodes
    // with the given name.
    public static List<DOm.XmlNode> getElementsByName(Dom.XmlNode element, String name) {
        List<Dom.XmlNode> lstElements = new List<Dom.XmlNode>();
        if (name == element.getName()) lstElements.add(element);

        for (Dom.XmlNode childElement: element.getChildElements()) {
            lstElements.addAll(getElementsByName(childElement, name));
        }
        
        return lstElements;
    }
    
    // ------------------------------------------------------------------------------------------
    // For the given element, return the value of the given namespace/attribute, if any.
    public static String getChildElementValue(Dom.XmlNode element, String name){
        return getChildElementValue(element, name, null);
    }
    public static String getChildElementValue(Dom.XmlNode element, String name, String namespace){
        Dom.XmlNode childElement = element.getChildElement(name, namespace);
        
        if (childElement == null){
            return null;
        } else {
            return childElement.getText();
        }
    }    
    
    ////////////////test Methods
    
    static testMethod void test1()
    {
    string xml = '<?xml version="1.0" encoding="UTF-8"?>';
        xml += '<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01" xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">';
        xml += '<gesmes:subject>Reference rates</gesmes:subject>';
        xml += '<gesmes:Sender>';
        xml += '<gesmes:name>European Central Bank</gesmes:name>';
        xml += '</gesmes:Sender>';
        xml += '<Cube>';
        xml += '<Cube time=\'';
        xml += System.Today();
        xml += '\'>';
        xml += '<Cube currency=\'USD\' rate=\'1.3042\'/>';            
        xml += '<Cube currency=\'GBP\' rate=\'0.82810\'/>';         
        xml += '<Cube currency=\'AUD\' rate=\'1.2192\'/>';
        xml += '<Cube currency=\'BRL\' rate=\'2.2549\'/>';
        xml += '<Cube currency=\'CAD\' rate=\'1.3019\'/>';
        xml += '</Cube>';
        xml += '</Cube>';
        xml += '</gesmes:Envelope>';  
        
     String ATOM_NAMESPACE = 'http://www.w3.org/2005/Atom';
            Dom.Document resDoc;
            resDoc = new Dom.Document();
            resDoc.load(xml);
            Dom.XmlNode root = resDoc.getRootElement();         
            List<Dom.XmlNode> lstEntries = DOM_Utils.getElementsByName(root, 'Cube');
            for (Dom.XmlNode entry :lstEntries) 
            {
                string title = DOM_Utils.getChildElementValue(entry, 'Cube');                
            }                
      }
}