@isTest
private class setOwnerAsAccountTriggerTest{
    @testSetup
    public static void testData(){
    
        integer numAccts = 5;
        List<Account> accts = new List<Account>();
        
        for(Integer i=0;i< numAccts;i++) {
            Account a = new Account(Name='TestAccount' + i);
            accts.add(a);
        }
        insert accts;
        
        integer numConts = 5;
        List<Contact> Contacts = new List<Contact>();
        
        for(Integer i=0;i< numConts;i++) {
            Contact c = new Contact(LastName='TestContact' + i, Accountid=accts[i].Id , Salutation='Captain' );
            Contacts.add(c);
        }
        insert Contacts;
    }
    
    public TestMethod static void init(){
    
        List<Contact> ContactList = new List<Contact>();
        ContactList = [SELECT Id , OwnerId , account.OwnerId 
                       FROM Contact];
                       
        
            system.assertEquals(contactList[0].account.OwnerId, contactList[0].ownerId );    
        
    }
    
    
}