/**************************************

This class is used in OppLineSyncTrigger, OppSyncTrigger, QuoteLineSyncTrigger, QuoteSyncTrigger triggers

***************************************/


public class TriggerStopper 
{
    public static boolean stopOpp = false;
    public static boolean stopQuote = false;
    public static boolean stopOppLine = false;
    public static boolean stopQuoteLine = false;
    
    public static boolean newQuote = false;
}