public with sharing class contactPasswordAction {
    
    private static Datetime saltDate = Datetime.now();
    
    @InvocableMethod(label = 'generate Password' description = 'generates password for Contact')
    public static List<Id> generatePassword(List<Id> contactIds){
        List<Id> returnList = new List<Id>();
        List<Contact> updateList = new List<Contact>();
        List<Contact> inputContacts = [SELECT Id, Password__c FROM Contact WHERE Id IN :contactIds];
        for (Contact c : inputContacts){
            c.Password__c = getContactPassword(c);
            updateList.add(c);
            returnList.add(c.Id);
        }
        update updateList;
        return returnList;
    }
    
    public static String getContactPassword(contact c ){       
        String apiString = String.valueOfGmt(saltDate) + String.valueOf(c.id);
        Blob apiBlob = Crypto.generateDigest('SHA-256', Blob.valueOf(apiString));
        String psd = EncodingUtil.convertToHex(apiBlob).substring(0, 8);
        return psd;
    }

}