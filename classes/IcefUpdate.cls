global class IcefUpdate
{
    public static void accountInvoiceInfo(List<Opportunity> listOfOpportunity) {    
        List<sObject> listOfAccount ;   // List Of Account
        List<Opportunity> listOfNewOpp; // List Of Opportunity 
        sObject sObj;   
        try{
            listOfNewOpp = new List<Opportunity>();
            System.debug('listOfOpportunity::'+listOfOpportunity);
            if(!listOfOpportunity.isEmpty()){
                listOfNewOpp = [Select Id,AccountId,Invoicing_contact__c,Invoicing_contact__r.Email 
                                From Opportunity 
                                Where Id IN :listOfOpportunity AND
                                      StageName = 'Closed Won' AND 
                                      Invoicing_contact__c != null ];
                                      
                 System.debug('listOfNewOpp::'+listOfNewOpp);
                 
                if(!listOfNewOpp.isEmpty()){
                    listOfAccount = new List<sObject>();
                    for(Opportunity opp : listOfNewOpp){                        
                        sObj = Schema.getGlobalDescribe().get('Account').newSObject();
                        sObj.put('ID',opp.AccountId);
                        sObj.put('c2g__CODAFinanceContact__c',opp.Invoicing_contact__c);
                        sObj.put('c2g__CODAInvoiceEmail__c',opp.Invoicing_contact__r.Email);
                        listOfAccount.add(sObj);
                       
                    }
                    try{
                        if(!listOfAccount.isEmpty()){
                            System.debug(listOfAccount);
                            Update listOfAccount;
                        }                       
                    }catch(Exception e){
                        System.debug('The following exception has occurred  ' + e.getMessage() +
                        'At line number :' + e.getLineNumber() + ' Error ' + e.getStackTraceString());
                    }
                }   
            }
        }catch(Exception e){
            System.debug('The following exception has occurred  ' + e.getMessage() +
                        'At line number :' + e.getLineNumber() + ' Error ' + e.getStackTraceString());
        }
    }   
}