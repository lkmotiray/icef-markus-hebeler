@isTest
public class DataFactory {
    
    // get Test Record Type
    // find in DB 
    // return Id
    public static Id getRecordTypeId(String rtName, String rtObject){ // ('Agent','Account')
        RecordType recType = [SELECT Id, Name FROM RecordType WHERE Name = :rtName and SobjectType = :rtObject];
        return recType.Id;
    }
    
    // create Test Country
    // insert
    // return Id
    public static String createTestCountry(){
        Country__c country = new Country__c();
        country.Name = 'Test Country';
        country.Agent_Relationship_Manager__c = UserInfo.getUserId();
        country.Sales_Territory_Manager__c = UserInfo.getUserId();
        
        insert country;
        return country.Id;
    } 
    
    // create Test Icef Event
    // don't insert
    // return ICEF_Event__c
    public static ICEF_Event__c createTestEvent() {
        Icef_Event__c testEvent = new ICEF_Event__c(Name = 'Test ICEF Event 2018',
                                                    Event_Manager_Educators__c = UserInfo.getUserId(),
                                                    Event_Manager_Agents__c = UserInfo.getUserId(),
                                                    Event_Country__c = createTestCountry(),
                                                    Event_City__c = 'Event City 1',
                                                    technical_event_name__c = 'city1_2018_city1',
                                                    RecordTypeId = getRecordTypeId('Regional Workshop', 'ICEF_Event__c'));            
        return testEvent;
    }
    
    
    // create numC Contacts for an account
    // don't insert
    // return list of contacts
    public static List<Contact> createTestContacts(Account acct, Integer numC){
        List<Contact> conList = new List<Contact>();
        RecordType rtName = [SELECT Name FROM RecordType WHERE Id = :acct.RecordTypeId][0];
        Id recType = getRecordTypeId(rtName.Name, 'Contact');
        Integer con = 0;
        for(con=0 ; con < numC ; con++){
            Contact c = new Contact(FirstName = 'FirstName',
                                    LastName = 'LastName '+con,
                                    Salutation = 'Ms',
                                    Gender__c = 'female',
                                    Role__c = 'Other Sales Contact',
                                    Contact_Status__c = 'Active',
                                    Contact_Email__c = acct.Name.deleteWhitespace()+con+'@icef.com',
                                    AccountId = acct.Id,
                                    RecordTypeId = recType                                    
                                    );
            conList.add(c);
        }
        return conlist;
    }
    
    //create Test Lead
    //don't insert
    public static Lead createTestLead(String recType){
        Lead newLead = new Lead();
        newLead.RecordTypeId = getRecordTypeId(recType, 'Lead');
        newLead.Company = 'Test Company';
        newLead.LastName = 'LastName';
        newLead.Status = 'Open - Not Contacted';
        newLead.Country_Lookup__c = createTestCountry();
        return newLead;
    }
    
    public static String createTestCampaign(){
        Campaign camp = new Campaign(Name = 'Test Campaign');
        insert camp;
        return camp.Id;
    }
    
    public static CampaignMember createTestCampaignMember(String CampaignId, String LeadId){
        CampaignMember cm = new CampaignMember();
        cm.CampaignId = campaignId;
        cm.LeadId = LeadId;
        return cm;
    } 
    
    
    //create a number (numCFS) of Coursefinder Schools for an account
    //don't insert
    //parameters: numCFS, Account
    //return List of Coursefinder Schools
    public static List<Coursefinder_School__c> createTestCfSchools(Integer numCFS, Account eduAcct){
        List<Coursefinder_School__c> cfSchoolList = new List<Coursefinder_School__c>();
        for (Integer i=0 ; i < numCFS ; i++){
            Coursefinder_School__c cfSchool = createTestCoursefinderSchool(eduAcct.Id, 'CFS '+eduAcct.Name);
        }
        return cfSchoolList;
    }
    
    //create a Coursefinder School for an account
    //don't insert
    //parameters: Account Id, School Name (will be set to 'Test CF School' if is '')
    //return Record Id
    public static Coursefinder_School__c createTestCoursefinderSchool(Id accId, String schoolName){
        If (schoolName == ''){schoolName = 'Test CF School';}
        CourseFinder_School__c cfSchool = new Coursefinder_School__c(Account__c = accId,
                                                                    Name = schoolName,
                                                                    City__c = 'Test City',
                                                                    Country__c = 'Test Country');
        return cfSchool;
    }
    
    //create Coursepricer Agencies for a list of agencies and one Coursefinder School
    //don't insert
    //return List of CP Agencies
    public static List<CoursePricer_Agency__c> createTestCoursePricerAgencies(List<Account> agencyAccts, Coursefinder_School__c CfSchool){
        List<CoursePricer_Agency__c> cpaList = new List<CoursePricer_Agency__c>();
        for(Account agency : agencyAccts){
            CoursePricer_Agency__c cpAgency = createTestCoursePricerAgency(CfSchool, agency);
            cpaList.add(cpAgency);
        }
        return cpaList;
    }
    
    //create a CoursePricer Agency for a CourseFinder School
    //don't insert
    //return Record Id
    public static CoursePricer_Agency__c createTestCoursePricerAgency(Coursefinder_School__c CfSchool, Account agency){
        String cpLink = agency.Name.replace(' ' , '-');
        CoursePricer_Agency__c cpAgency = new CoursePricer_Agency__c(Coursefinder_School__c = CfSchool.Id,
                                                                    Agency__c = agency.Id,
                                                                    CP_link__c = 'www.'+cpLink+'.com');
        return cpAgency;
    }
    
    // create Memberships of all Type for all accounts
    // don't insert
    // return memberships
    public static List<Membership__c> createMembershipsAllTypesAllAccounts(Integer numAgt, Integer numEdu, Integer numAssocPerTypePerAccount){
        List<Account> agents = createTestMembers(numAgt, 'Agent');
        List<Account> edus = createTestMembers(numEdu, 'Educator');
        List<Account> members = agents;
        members.addAll(edus);
        List<Account> associations = new List<Account>();
        List<Membership__c> memberships = new List<Membership__c>();
        String[] assocTypes = associationTypes();
        for(String assocType : assocTypes){
            List<Account> assocs = createTestAssociations(numAssocPerTypePerAccount,assocType);
            associations.addAll(assocs);
        } 
        for(Account member : members){
            for(Account assoc : associations){
                Membership__c ms = createTestMembership(member, assoc);
				memberships.add(ms);
            }            
        }
        return memberships;
    }

    // create a number (numAssoc) of Association Accounts with of Association Type assocType. 
    // don't insert
    // REQUIRED FIELDS:
    // Name: 'Test AM (IQ,....) Association 0' (1, 2, ...) -> 'Test AM Association 0'
    // Name short: 'short (shortAssocType) 0 (1,2,...)' -> 'short AM 0'
    // Association Type: assocType
    // Mailing Country: Test Country
    public static List<Account> createTestAssociations(Integer numAssoc, String assocType){
        List<Account> associations = new List<Account>();
        for(Integer i=0;i<numAssoc;i++){
            Account a = new Account(Name='Test '+shortAssocType(assocType)+' Association '+i,
                                   RecordTypeId = getRecordTypeId('Association', 'Account'),
                                   Name_short__c='short '+shortAssocType(assocType)+' '+i,
                                   Association_Type__c= assocType,            
                                   Mailing_Country__c=createTestCountry());
            associations.add(a);
        }
        return associations;
    }
    // create a number (numAcct) of Agent Accounts. 
    // Don't insert.
    // REQUIRED FIELDS:
    // Name: 'Test Agent 0(1,2,...)'
    // Mailing_Country__c: Test Country
    public static List<Account> createTestAgents(Integer numAcct){
        List<Account> accts = new List<Account>();
        for (Integer i=0;i<numAcct;i++){
            Account a = new Account(Name = 'Test Agent '+i,
                                   RecordTypeId = getRecordTypeId('Agent', 'Account'),
                                   Mailing_Country__c = createTestCountry());
            accts.add(a);
        }
        System.debug('DataFactory Test Agents: ' + accts);
        return accts;
    }
    
    // create a number (numAcct) of Educator Accounts. 
    // Don't insert.
    // REQUIRED FIELDS:
    // Name: 'Test Educator 0(1,2,...)'
    // Educational Sector: '11 UNIVERSITIES'
    // Mailing_Country__c: Test Country
    public static List<Account> createTestEducators(Integer numAcct){
        List<Account> accts = new List<Account>();
        for (Integer i=0;i<numAcct;i++){
            Account a = new Account(Name = 'Test Educator '+i,
                                   RecordTypeId = getRecordTypeId('Educator', 'Account'),
                                   Educational_Sector__c = '11 UNIVERSITIES',
                                   Mailing_Country__c = createTestCountry());
            accts.add(a);
        }
        return accts;
    }
         
    // create a number (numAcct) of Accounts with Record Type 'Agent' or 'Educator' (recType) Account Name: 'Agent Member 0', 'Educator Member 0' (1,2,3,...)
    public static List<Account> createTestMembers(Integer numAcct, String recType){
        List<Account> members = new List<Account>();
        String recTypeId = '';
        if (recType == 'Agent'){
        	recTypeId = getRecordTypeId('Agent', 'Account');    
        } else {
            recTypeId = getRecordTypeId('Educator', 'Account');
        }
        for (Integer i=0;i<numAcct;i++){
            Account a = new Account(Name='Test '+RecType+' Member '+i,
                                   RecordTypeId = recTypeId,
                                   Mailing_Country__c = createTestCountry());
            members.add(a);
        }
        insert members;
        return members;
    }

    // create Membership for Member (member) and Association (assoc)
    public static Membership__c createTestMembership(Account member, Account assoc){
        Membership__c membership = new Membership__c(Member__c=member.Id,Association__c=assoc.Id);
        membership.Nudge__c = null;
        membership.Status__c = 'Active';
        return membership;
    }
  
    public static String shortAssocType (String assocType){
        String returnString = '';
        if (assocType == 'Association Membership'){
            returnString = 'AM';
        } else if (assocType == 'Agent Industry Qualification'){
            returnString = 'IQ';
        } else if (assocType == 'Industry Recognition'){
            returnString = 'IR';
        } else if (assocType == 'Other industry Organizations'){
            returnString = 'OTHER';        
        } else {
            returnString = '';
        }     
		return returnString;        
    }
    public static List<String> associationTypes(){
        List<String> associationTypes = new List<String>();
        associationTypes.add('Association Membership');
        associationTypes.add('Agent Industry Qualification');
        associationTypes.add('Industry Recognition');
        associationTypes.add('Other industry Organizations');     
        return associationTypes;
    }
}