/**
 *  @Purpose: Test class for Coursefinder_School_TriggerHandler.
 */
@isTest
public class Coursefinder_School_TriggerTest {
    
    /**
 	*  @Purpose: Create setup data.
 	*/
    
    @testSetup
    static void testData() {
        c2g__codaGeneralLedgerAccount__c recordLedgerAccount = new c2g__codaGeneralLedgerAccount__c( Name = 'Tes');
        
        //Create country
        String countryId = createCountry();
        
        //Create account
        String accountId = createAccount(countryId);
        
        //Create Coursefinder_School__c
        Coursefinder_School__c recordCFSchool= new Coursefinder_School__c(Account__c = accountId);
        insert recordCFSchool;
        
        List<Contact> listContact = new List<Contact>();
        
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Educator').getRecordTypeId();
        for(integer countContact = 0;countContact < 4;countContact++){
            
            Contact recordContact = new Contact();
            recordContact.LastName = 'test contact'+countContact;
            recordContact.AccountId = accountId;
            recordContact.Email = 'test'+countContact+'@test.com';
            recordContact.Salutation = 'Dr.';
            recordContact.Role__c='Primary Decision Maker';
            recordContact.RecordTypeId = RecordTypeIdContact;
            recordContact.Contact_Email__c = 'test'+countContact+'@test.com';
            listContact.add(recordContact);
            
        }
        insert listContact;
        System.assertEquals(4,[SELECT Id FROM Contact].size());
        
        //Create records of CoursePricer_Agency__c and CF_Admin_Contact__c.
        List<CoursePricer_Agency__c> listCoursePricerAgency = new List<CoursePricer_Agency__c>();
        List<CF_Admin_Contact__c> listCFAdminContact = new List<CF_Admin_Contact__c>();
        for(integer count = 0; count < 4; count++){           
            //CoursePricer_Agency__c
            CoursePricer_Agency__c coursePricerAgency = new CoursePricer_Agency__c();
            coursePricerAgency.Agency__c = accountId;
            coursePricerAgency.Coursefinder_School__c = recordCFSchool.Id;
            coursePricerAgency.CP_link__c = 'www.test.com';
            listCoursePricerAgency.add(coursePricerAgency);
            
            //CF_Admin_Contact__c
            CF_Admin_Contact__c  recordCFAdminContact = new CF_Admin_Contact__c();
            recordCFAdminContact.CF_Admin_Account__c = accountId;
            recordCFAdminContact.Contact__c = listContact[count].Id;
            recordCFAdminContact.Status__c='Active';
            listCFAdminContact.add(recordCFAdminContact);
        }
        insert listCoursePricerAgency;
        insert listCFAdminContact;
        System.assertEquals(4,[SELECT Id FROM CF_Admin_Contact__c].size());
        System.assertEquals(4,[SELECT Id FROM CoursePricer_Agency__c].size());
        
    }
    
    /**
 	*  @Purpose: Test Method for Coursefinder_School_TriggerHandler.
 	*/
    public static TestMethod void handleUpdatedCoursefinder_SchoolsTest() 
    {
        Test.startTest();
        
        Integer invocations = Limits.getLimitEmailInvocations();
        Integer invocations2 = Limits.getEmailInvocations();
        system.debug('@invocations'+invocations);
        system.debug('@invocations2'+invocations2);
        Test.stopTest();
        
        system.assertEquals(10, invocations);
    }
 
    /**
 	*  @Purpose: Create Country__c record.
 	*/
    public static String createCountry(){
        User userRecord = [SELECT ID 
                           FROM User
                           WHERE Name = 'Liz Adams'];
        
        Country__c countryRecord = new Country__c ( Name='TestC' ,
                                                   Region__c = 'South & Central America',
                                                   Sales_Territory__c = 'Americas',
                                                   Agent_Relationship_Manager__c = userRecord.Id,
                                                   countries_from_to_in__c = True,
                                                   Sales_Territory_Manager__c = userRecord.Id
                                                  );
        insert countryRecord;
        return countryRecord.Id;
    } 
    
     /**
 	*  @Purpose: Create Account record.
 	*/
    public static String createAccount(String countryId){
        User userRecord = [SELECT ID 
                           FROM User
                           WHERE Name = 'Liz Adams'];
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Educator').getRecordTypeId();
        
        
        Account accountRecord = new Account( RecordTypeId = RecordTypeIdAccount,
                                            Name = 'Test',
                                            CurrencyIsoCode = 'EUR',
                                            Educational_Sector__c = '5 MULTI-SECTORAL',
                                            Status__c = 'Active',
                                            Building_Country__c = countryId,
                                            Mailing_Country__c = countryId,
                                            disable_CoursePricer_Alerts__c=false
                                           );
        
        insert accountRecord;
        System.assertEquals(1,[SELECT Id FROM Account].size());
        return accountRecord.Id;
    }
}