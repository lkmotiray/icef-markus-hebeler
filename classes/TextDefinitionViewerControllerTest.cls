@IsTest(SeeAllData = true)
private class TextDefinitionViewerControllerTest {
    testmethod static void testTextDefinitionViewerController() {
    	c2g__codaInvoice__c salesInvoice = [ SELECT Id, c2g__OwnerCompany__r.name 
                                              FROM c2g__codaInvoice__c 
                                              LIMIT 1 ];
        Test.startTest();
        TextDefinitionViewerController cntroler = new TextDefinitionViewerController();
        cntroler.salesInvoiceRecord = salesInvoice;
        List<c2g__codaTextDefinition__c> textDefinitions = cntroler.getTextDefinitionRecords();
        Test.stopTest();
    }
}