public with sharing class CustomChildTableController {

    public Object[] listOfCountries;
    public List<WrapperClassForTableData> wrapperForDataTable {get; set;}
    
    public void setlistOfCountries (Object[] aList) {
        wrapperForDataTable = new List<WrapperClassForTableData>();
        listOfCountries = aList;
        for(Object o : listOfCountries){
            List<string> temp = String.valueOf(o).split('#');
            WrapperClassForTableData cv = new WrapperClassForTableData(temp[0], temp[1]);
            wrapperForDataTable.add(cv);
        }
    }
    
    public Object getlistOfCountries() {
        return listOfCountries;
    }
    
    public CustomChildTableController() {
        wrapperForDataTable = new List<WrapperClassForTableData>();
    }
    
    
    public class WrapperClassForTableData {
        public String key {get; set;}
        public String value {get; set;}
        
        public WrapperClassForTableData(String itemName, String val) {
            key = itemName;
            value = val;
        }
    }
    
    @isTest
    public static void CustomChildTableControllerTest(){
        Test.startTest();
        CustomChildTableController tc = new CustomChildTableController();
        String testString = 'Argentina#5';
        List<String> testList = new List<String>();
        testList.add(testString);
        tc.setlistOfCountries(testList);
        tc.getlistOfCountries();
        Test.stopTest();
    }
}