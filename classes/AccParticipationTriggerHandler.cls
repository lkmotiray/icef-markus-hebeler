/********************************************************************************************
Developer   - Dreamwares (Amol)
Description - Populate Catalogoue Entry word count
			- update Number of required accommodations
Date        - 08 Apr. 2015
Modified	- 26.4.2018 by Henriette
********************************************************************************************/

public class AccParticipationTriggerHandler
{
    /************************************************************************************
    Method Name - updateCatlogEntryWordCount
    Parameters  - List of Account Participation records whose word count is to be updated
    Return      - none
    Called From - AccountParticipationTrigger
    Description - Populate the value of Catalogue_entry_words__c field on Account Participation
    *************************************************************************************/
    
    public void updateCatlogEntryWordCount(List<Account_Participation__c> lstNewAccPart)
    {
        for(Account_Participation__c newAccPart :lstNewAccPart)
        {
            //update Catalogue_entry_words__c value of each record
            if(newAccPart.Catalogue_entry__c != null)
            {
                newAccPart.Catalogue_entry_words__c = countWords(newAccPart.Catalogue_entry__c);
            }
            else
            {
                newAccPart.Catalogue_entry_words__c = 0;
            }
        }
    }
    
    /************************************************************************************
    Method Name - countWords
    Parameters  - String in which the words should be calculated
    Return      - number of words in input string
    Called From - updateCatlogEntryWordCount
    Description - count the number of words present in input string
    *************************************************************************************/
    private Integer countWords(String countString)
    {
        List<String> lstSplitLines = new List<String>();
        List<String> lstSplitSpace;
        List<String> lstWords = new List<String>();
        String countStringNew;
        
        //String = String.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_');
        countStringNew = countString.replaceAll('[^a-zA-Z0-9\\-]', ' ');
//        System.debug('countStringNew ::' +countString.replaceAll('[^a-zA-Z0-9\\-]', ' '));
        
        //get all lines present in input string
        lstSplitLines = countStringNew.split('\n');
//        System.debug('lstSplitLines ::'+lstSplitLines);
        
        if(!lstSplitLines.isEmpty())
        {
            //for all lines
            for(String line :lstSplitLines)
            {
                //get all words in single line
                lstSplitSpace = new List<String>();
                
                lstSplitSpace = line.split(' ');
                
                //add line's words to final word list
                if(!lstSplitSpace.isEmpty()) {
                    for(String word : lstSplitSpace){
                        if(word !=''){
                   			 lstWords.add(word);         
                        }
                    }                      
                }
            }
        }
        
        //return final wordcount
        if(!lstWords.isEmpty())
            return lstWords.size();
        else
            return 0;
    }
    
    /*******************
     * Method to calculate Number of required accommodations based on related CPs 
     * condition for CPs: Status (registered,...) + NOT No hotel Room needed for this contact
     * Developer: Henriette
     * Date: 26.4.2018
     *******************/
    public void setNumberOfRequiredAccommodations(Map<Id, Account_Participation__c> apList){
        if(apList.keySet().size() > 0){
        	List<Account_Participation__c> hotelRoomNeeded = new List<Account_Participation__c>();
        	for (Account_Participation__c ap : apList.values()){
            	if(ap.No_Hotel_Room_needed__c){
                	ap.Number_of_required_Accommodations__c = 0;
            	} else{
					hotelRoomNeeded.add(ap);                
            	}
        	}
        	if (hotelRoomNeeded.size() > 0 ){
            	//get a set of all AP ids
            	Map<Id, Account_Participation__c> apMap = new Map<Id, Account_Participation__c>(hotelRoomNeeded);
            	//get CPs registered for all APs
            	Map<Id, Integer> ApCpMapping = new Map<Id, Integer>();
            	set<String> setOfStatus = new set<String>{'registered','Guest','participating','Speaker'};

	            AggregateResult[] cpsGroupedByAP = 
    	            [SELECT Account_Participation__c, COUNT(ID)cps
        	         From Contact_Participation__c 
            	     WHERE Account_Participation__c IN :apMap.keySet()
                	 AND No_Hotel_Room_Needed_for_this_Contact__c != true
                	 AND Participation_Status__c IN :setOfStatus
                 	GROUP BY Account_Participation__c
                	];
            	System.debug('cpsGroupedByAP: '+ cpsGroupedByAP);
            
           		for(AggregateResult ar : cpsGroupedByAP){
                	System.debug('AP: '+ ar.get('Account_Participation__c'));
                	System.debug('CPS: ' + ar.get('cps'));
                	ApCpMapping.put((ID)ar.get('Account_Participation__c'), (Integer)ar.get('cps'));
            	}
            
	            system.debug('apCpMapping: ' + ApCpMapping);
    	        system.debug('size of ApCpMapping: ' + ApCpMapping.size());
        	    System.debug('size of apMap: ' + apMap.size());
      		
        	    for(Id apId : apMap.keySet()){
            	    Account_Participation__c ap = apList.get(apId);
    				// if there is at least one valid CP
                	if(ApCpMapping.keySet().contains(apId)){
                    	ap.Number_of_required_Accommodations__c = ApCpMapping.get(apId);
                    //no valid CPs
                	} else {
                    	ap.Number_of_required_Accommodations__c = 0;
                	}
            	}
        	}   
        }
    }
}