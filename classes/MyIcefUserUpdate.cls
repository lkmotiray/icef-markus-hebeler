//update my Icef User if Contact has relevant changes except email address change
//email address most not be null!!
public class MyIcefUserUpdate {
	@invocableMethod(label='create or update MyIcef User (not Email)' description = 'calls future Method to update MyIcefUser')
    public static void updateMyIcefUsers(List<Id> contactIds){
    //public static void updateMyIcefUsers(List<Contact> contacts){    
//        for(Id cId : contactIds){
//        	AuthUser.createOrUpdateUser(cId);   
//        }
        String methodName = 'updateUser';

    	QueueableMyIcefUserUpdate updateJob = new QueueableMyIcefUserUpdate(methodName, contactIds);

        EmailUtil.sendErrorEmail('MyIcefUserUpdate invoked', 'Job Id: ' + updateJob +', for Contact Ids: ' + contactIds);
    }
}