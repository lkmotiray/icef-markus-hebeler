/**
@ Created By : Yogesh Mahajan.
@ Date       : 11-06-2014.   
*/
Public class AssignSummaryFieldHandler{
    // Used to store all multipicklist fields data
    Public Set<String> allValue = new Set<String>();
    Integer count;
    String multipicklistVal;
    String addAllVal;
    
    /**
    @ Name : updateSummaryField
    @ Description : used to add all multpicklist values to reapective field. 
    @ Parameter : 
    */
    Public void updateSummaryField(Map<Id, Course_Profile__c> mapNewCourseProfile){
        
        try{
        System.debug('mapNewCourseProfile.values() ::: '+mapNewCourseProfile.values());
            for( Course_Profile__c courseProfile : mapNewCourseProfile.values() ){ 
                // All career multipicklist
                getMultpicklist(courseProfile, 'Career_Vocational_Certificate_Diploma__c');
                getMultpicklist(courseProfile, 'Career_Voc_Cert_Diploma_Blended__c');
                getMultpicklist(courseProfile, 'Career_Voc_Cert_Diploma_Online__c');
                
                assignValue(courseProfile, 'Career_Voc_Cert_Diploma_ALL__c', 'false');
                
                // All Undergraduate multipicklist
                allValue.clear();
                getMultpicklist(courseProfile, 'Undergraduate_Deg_Bachelor_Blended__c');
                getMultpicklist(courseProfile, 'Undergraduate_Deg_Bachelor_Online__c');
                getMultpicklist(courseProfile, 'Undergraduate_Degree_Bachelor__c');
                
                assignValue(courseProfile, 'Undergraduate_Deg_Bachelor_ALL__c', 'false');
                
                // All Graduate Diploma multipicklist
                allValue.clear();
                getMultpicklist(courseProfile, 'Grad_Postgrad_Cert_Diploma_Blended__c');
                getMultpicklist(courseProfile, 'Grad_Postgrad_Cert_Diploma_Online__c');
                getMultpicklist(courseProfile, 'Graduate_PostgraduateCertificate_Diploma__c');
                
                assignValue(courseProfile, 'Grad_Postgrad_Cert_Diploma_ALL__c', 'false');
                
                // All Graduate Masters multipicklist
                allValue.clear();
                getMultpicklist(courseProfile, 'Graduate_Postgrad_Masters_Blended__c');
                getMultpicklist(courseProfile, 'Graduate_Postgrad_Masters_Online__c');
                getMultpicklist(courseProfile, 'Graduate_Postgraduate_Masters__c');
                
                assignValue(courseProfile, 'Graduate_Postgrad_Masters_ALL__c', 'false');
                
                // All Graduate Doctorate multipicklist
                allValue.clear();
                getMultpicklist(courseProfile, 'Graduate_Postgrad_Doctorate_Blended__c');
                getMultpicklist(courseProfile, 'Graduate_Postgrad_Doctorate_Online__c');
                getMultpicklist(courseProfile, 'Graduate_Postgraduate_Doctorate__c');
                
                assignValue(courseProfile, 'Graduate_Postgrad_Doctorate_ALL__c', 'false');
                
                // All Other Career Diploma 
                allValue.clear();
                getMultpicklist(courseProfile, 'Other_Career_Voc_Cert_Dipl_Blended__c');
                getMultpicklist(courseProfile, 'Other_Career_Voc_Cert_Dipl_Online__c');
                getMultpicklist(courseProfile, 'Other_Career_VocationalCertificate_Dipl__c');
                
                assignValue(courseProfile, 'Other_Career_Voc_Cert_Dipl_ALL__c', 'true');
                
                // All Other Doctorate 
                allValue.clear();
                getMultpicklist(courseProfile, 'Other_Grad_Postgrad_Doc_Blended__c');
                getMultpicklist(courseProfile, 'Other_Grad_Postgrad_Doc_Online__c');
                getMultpicklist(courseProfile, 'Other_Graduate_Postgraduate_Doctorate__c');
                
                assignValue(courseProfile, 'Other_Grad_Postgrad_Doc_ALL__c', 'true');
                
                // All Other Master 
                allValue.clear();
                getMultpicklist(courseProfile, 'Other_Grad_Postgrad_Masters_Blended__c');
                getMultpicklist(courseProfile, 'Other_Grad_Postgrad_Masters_Online__c');
                getMultpicklist(courseProfile, 'Other_Graduate_Postgraduate_Masters__c');
                
                assignValue(courseProfile, 'Other_Grad_Postgrad_Masters_ALL__c', 'true');
                
                // All Other Grad / Postgrad  Diploma 
                allValue.clear();
                getMultpicklist(courseProfile, 'Other_Grad_PostgradCert_Dipl_Blended__c');
                getMultpicklist(courseProfile, 'Other_Grad_PostgradCert_Dipl_Online__c');
                getMultpicklist(courseProfile, 'Other_Graduate_PostgradCertificate_Dipl__c');
                
                assignValue(courseProfile, 'Other_Grad_PostgradCert_Dipl_ALL__c' , 'true');
                
                // All Other Undergrad Deg
                allValue.clear();
                getMultpicklist(courseProfile, 'Other_Undergrad_Deg_Bach_Blended__c');
                getMultpicklist(courseProfile, 'Other_Undergrad_Deg_Bach_Online__c');
                getMultpicklist(courseProfile, 'Other_Undergraduate_Degree_Bachelor__c');
                
                assignValue(courseProfile, 'Other_Undergrad_Deg_Bach_ALL__c', 'true');
            } 
        }
        catch( DmlException e ){
            system.debug( 'Exception IS===='+e.getMessage() );
        }
    } 
    /**
    @ Description : used to get multipicklist value.
    */
    Private void getMultpicklist( Course_Profile__c course,String multipicklistName ){
        multipicklistVal = (String)course.get(multipicklistName);
        if( multipicklistVal != null ){
            sortMultipicklist(multipicklistVal);
            multipicklistVal = '';
        }
    }
    /**
    @ Description : used to assgin multipicklist values.
    */
    Private void assignValue( Course_Profile__c course, String all, String isText ){
        count = 1;
        for ( String val : allValue ){                
            if ( count == 1 ) {
                addAllVal = val;
                if( allValue.size() > 1 )
                {    addAllVal += ';';    } 
                count++;    
            }else if( count < allValue.size() ){
                addAllVal = addAllVal + val+';';             
                count++;
            }
            else if( count == allValue.size() ){
                addAllVal = addAllVal + val;             
                count++;
            }            
        }
        //System.debug('Add Vall====='+addAllVal.length());
        if (isText == 'true' && addAllVal.length() > 255 ){
            addAllVal = addAllVal.substring(0, 254);
            course.put(all, addAllVal);
        }
        else
            course.put(all, addAllVal); //= addAllVal;
        //System.debug('Add Vall====='+addAllVal);
        //System.debug('Add Vall====='+addAllVal.length());
        addAllVal = '';
    }
    /**
    @ Description : Used to split the picklist value by ';'.
    */
    Private void sortMultipicklist(String pickValue){
        List<String> parts = pickValue.split(';'); 
        System.debug('parts ====='+parts );
        allValue.addAll(parts);  
        
    }
}