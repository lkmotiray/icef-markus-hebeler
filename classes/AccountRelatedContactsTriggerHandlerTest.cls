/**
 *  @Purpose Tests the functionality of the setNumberOfCPRegistered
 *  @Author Dreamwares
 *  @Date 07 sept 2017
 */
@isTest
public class AccountRelatedContactsTriggerHandlerTest{
    public static integer noOfRec = 50;
    /**
     *  @ Purpose To test Functionality  
    */  
    private static testMethod void contactUpdateTest(){
        
        Test.startTest();
        Id agentRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agent').getRecordTypeId();
        
        Country__c country1 = new Country__c(Name = 'Test Country 1', 
                                             Region__c = 'Middle East', 
                                             Sales_Territory_Manager__c = UserInfo.getUserId());
        insert country1;
        
        List<Account> listOfAccount = new List<Account>();
        List<Contact> listOfContact = new List<Contact>();
        
        for(Integer idx = 0; idx < noOfRec; idx++){
            
            listOfAccount.add(new Account(Name = 'Test Account'+idx, RecordTypeId = agentRecordTypeId, 
                                       Status__c = 'Active', Mailing_Country__c = country1.id,
                                       c2g__CODAAccountTradingCurrency__c = 'EUR',
                                       Mailing_City__c = 'test',
                                       ARM__c = UserInfo.getUserId()));           
            
        }        
        insert listOfAccount;
        Update listOfAccount;
        
        for(Integer idx = 0; idx < noOfRec; idx++){
            
            listOfContact.add(new Contact(Salutation = 'Dr.', FirstName = 'Test', LastName = 'Test'+idx, 
                                   Gender__c = 'male', Role__c = 'Primary Decision Maker', Contact_Status__c = 'Active', 
                                   AccountId = listOfAccount[idx].Id));           
            
        }        
        insert listOfContact;
        
        Test.stopTest();  
                
        listOfAccount = [SELECT ID, Number_of_PMD_Contacts__c FROM Account Where Id In:listOfAccount];
        System.debug(json.serialize(listOfAccount));
        for(Account account :listOfAccount){
            
            System.assertEquals(1, account.Number_of_PMD_Contacts__c);          
        }
        for(Contact contact:listOfContact){
            
            contact.Role__c = 'Other';
            contact.X20_year_mag__c = true;
        }
        update listOfContact;        
        delete listOfContact; 
        
    }
}