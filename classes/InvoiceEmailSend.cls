global class InvoiceEmailSend {

	global static Map<Id, List<c2g__codaInvoiceInstallmentLineItem__c>> mapInvoiceLines;
    global static Map<Id, c2g__codaInvoice__c> mapInvoices;
	
    global static Map<Id, List<c2g__codaInvoiceInstallmentLineItem__c>> createMapInvoceineItems(Set<id> setId){
        List<c2g__codaInvoice__c> invoices = [SELECT ID, c2g__InvoiceTotal__c,(SELECT Id, c2g__Amount__c, c2g__LineNumber__c ,
                                                                            c2g__DueDate__c
                                                                     FROM c2g__InvoiceInstallmentLineItems__r
																	 ORDER BY c2g__DueDate__c)
                                          FROM c2g__codaInvoice__c	
                                          WHERE Id IN: setId];	
        
        Map<Id, List<c2g__codaInvoiceInstallmentLineItem__c>> mapInvoiceLines = new Map<Id, List<c2g__codaInvoiceInstallmentLineItem__c>>();
		
        for(c2g__codaInvoice__c	invoice : invoices){
            	
            mapInvoiceLines.put(invoice.id, new List<c2g__codaInvoiceInstallmentLineItem__c>(invoice.c2g__InvoiceInstallmentLineItems__r));
        }
        
		return mapInvoiceLines;
    }
    
    global static Map<Id, c2g__codaInvoice__c> createMapInvoces(Set<id> setId){
        try{
            List<c2g__codaInvoice__c> salesInvoices = [ SELECT Id, Name, c2g__Opportunity__r.Invoicing_contact__r.Name, 
                                                                        c2g__Opportunity__r.Invoicing_contact__r.Id, 
                                                                        c2g__Opportunity__r.Invoicing_contact__r.Email,
                                                                        c2g__Opportunity__c,
                                                                        c2g__Opportunity__r.Invoicing_contact__r.Contact_Email__c,
                                                                        c2g__Account__r.c2g__CODAInvoiceEmail__c,
                                                                        c2g__Opportunity__r.Invoicing_contact__r.FirstName,
                                                                        c2g__InvoiceTotal__c, c2g__OutstandingValue__c,
																		c2g__Account__c, c2g__Account__r.Owner.Email,
																		c2g__Opportunity__r.CC_Invoice_to__r.Email
                                                                FROM c2g__codaInvoice__c
                                                                WHERE Id IN : setId ];
                    
            Map<Id, c2g__codaInvoice__c> mapInvoices = New Map<Id, c2g__codaInvoice__c>(salesInvoices ); 
            return mapInvoices;
        }catch(Exception ex){
            System.debug('Exception : '+ ex.getMessage());            
        } 
        return new Map<Id, c2g__codaInvoice__c>();
    }
}