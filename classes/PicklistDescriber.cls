/**
    Class which client code needs to interact with. Call any of the overloaded describe method,
    make sure you are passing all the params in the method signature  
*/
public class PicklistDescriber {
    static final Pattern OPTION_PATTERN = Pattern.compile('<option.+?>(.+?)</option>'); 

    /**
        Describe a picklist field for a SobjectType, its given record type developer name and the picklist field
        example usage : 
        List<String> options = PicklistDescriber.describe('Account', 'Record_Type_1', 'Industry'));
    */
    public static List<String> describe(String sobjectType, String recordTypeName, String pickListFieldAPIName) {
        return parseOptions(
                            new Map<String, String> {
                                                     'sobjectType' => sobjectType,
                                                     'recordTypeName' => recordTypeName,
                                                     'pickListFieldName'=> pickListFieldAPIName
                                                    }
                            );
    }    
    
    /*
        Internal method to parse the OPTIONS
    */
    static List<String> parseOptions(Map<String, String> params) {
        Pagereference pr = Page.PicklistDesc;
        // to handle development mode, if ON
        pr.getParameters().put('core.apexpages.devmode.url', '1');

        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));   
        }
        
        String xmlContents;
        if(Test.isRunningTest()) {
            xmlContents = '<select  id="j_id0:j_id1:j_id2" name="j_id0:j_id1:j_id2"><option value="">--None--</option><option value="accepted">accepted</option>';
            xmlContents += '<option value="canceled">canceled</option>';
            xmlContents += '<option value="cax on list">cax on list</option>';
            xmlContents += '<option value="declined">declined</option>';
            xmlContents += '<option value="expression of interest">expression of interest</option>';
            xmlContents += '<option value="more info">more info</option>';
            xmlContents += '<option value="n/a">n/a</option>';
            xmlContents += '<option value="no selling">no selling</option>';
            xmlContents += '<option value="no show">no show</option>';
            xmlContents += '<option value="not accepted">not accepted</option>';
            xmlContents += '<option value="pending payment">pending payment</option>';
            xmlContents += '<option value="preliminary ok">preliminary ok</option>';
            xmlContents += '<option value="transferred">transferred</option>';
            xmlContents += '<option value="wait listed">wait listed</option>';
            xmlContents += '</select>';                       
        } 
        else {
            //try {
                xmlContents = pr.getContent().toString(); 
            //}
            //catch(Exception e){return null;}
        }            
                       
            System.debug('XML Contents are:::'+xmlContents);
            Matcher mchr = OPTION_PATTERN.matcher(xmlContents);
            List<String> options = new List<String>();
            while(mchr.find()) {
                options.add(mchr.group(1));
            } 
            // remove the --None-- element
            if (!options.isEmpty()) options.remove(0);
                return options;
        }
        
    }