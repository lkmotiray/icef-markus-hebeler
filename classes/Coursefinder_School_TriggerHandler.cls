/**
 *  @ Purpose: Handler for Coursefinder_School_Trigger
 *  @ Author : Dreamwares
 *  @ Note : To Send Email Notifications - 
 * 				1) CP_links_on_external_sites_2__c history tracking must be enabled 
 *              2) Populate CP_links_on_external_sites 2 must be active
 * 				3) Limit_of_CP_Agencies_Error_Notification Custom setting must be present
 * 				4) Email teplate 'New CoursePricer Agency added but limit reached' is needed
 *  @ Last Modfiied Date : 28-09-2018
 */
public class Coursefinder_School_TriggerHandler {
    
    public void handleUpdatedCoursefinder_Schools(List<Coursefinder_School__c> listNewCoursefinderSchool, Map<Id,Coursefinder_School__c> mapOldCoursefinderSchool){
     
        Map<Id, Id> mapSchoolIdToAccountIdUponLimit = new Map<Id, Id>(); // Map Crossed limit records
        Map<Id, Id> mapSchoolIdToAccountIdUnderLimit = new Map<Id, Id>(); // Map records under limit 
        Integer MAX_AGENCIES = 3;
        
 		 Map<Id, Account> mapAccounts = getMapAccounts(listNewCoursefinderSchool);
        
        for(Coursefinder_School__c Coursefinder_School : listNewCoursefinderSchool){
            
            Account schoolAccount = mapAccounts.get(Coursefinder_School.Account__c);
            
            if(Coursefinder_School.CP_links_on_external_sites__c > mapOldCoursefinderSchool.get(Coursefinder_School.Id).CP_links_on_external_sites__c
               && Coursefinder_School.Account__c!= null
               && schoolAccount.disable_CoursePricer_Alerts__c != true
               && Coursefinder_School.CP_Agency_Number_Upgrade__c == null ){
                  
                  if(Coursefinder_School.CP_links_on_external_sites__c > MAX_AGENCIES ){
                      
                      mapSchoolIdToAccountIdUponLimit.put(Coursefinder_School.Id, Coursefinder_School.Account__c);
                  }else if (Coursefinder_School.CP_links_on_external_sites__c <= MAX_AGENCIES){
                      
                      mapSchoolIdToAccountIdUnderLimit.put(Coursefinder_School.Id, Coursefinder_School.Account__c);
                  }
            }
        }
    
        Map<Id, Map<Id, String>> mapSchoolIdToReceipantsUponLimit = getMapSchoolIdToReceipants(mapSchoolIdToAccountIdUponLimit);
        
        Map<Id, Map<Id, String>> mapSchoolIdToReceipantsUnderLimit = getMapSchoolIdToReceipants(mapSchoolIdToAccountIdUnderLimit);
                
        sendNotificationForCoursefinderSchool(mapSchoolIdToReceipantsUponLimit, mapSchoolIdToReceipantsUnderLimit);
    }
    
    private  Map<Id, Account> getMapAccounts(List<Coursefinder_School__c> listNewCoursefinderSchool){
        
        Set<Id> setAccountId = new Set<Id>();
        for(Coursefinder_School__c coursefinder_School: listNewCoursefinderSchool){
            setAccountId.add(coursefinder_School.Account__c);
        }
        
        Map<Id, Account> mapAccounts = new Map<Id, Account>([SELECT Id, disable_CoursePricer_Alerts__c
                                                             FROM Account WHERE Id In : setAccountId]);
        return mapAccounts;        
    }
    
    
    private Map<Id, Map<Id, String>> getMapSchoolIdToReceipants(Map<Id, Id> mapSchoolIdToAccountId){
      
        Map<Id, Map<Id, String>> mapSchoolIdToReceipants = new Map<Id, Map<Id, String>>();
       
        List<CF_Admin_Contact__c> listCFAdmins = [SELECT Id, Contact__c, Contact__r.Email, CF_Admin_Account__c
                                                  FROM CF_Admin_Contact__c
                                                  WHERE CF_Admin_Account__c IN :mapSchoolIdToAccountId.values() 
                                                  AND Status__c = 'Active' 
                                                  AND Contact__r.Email != null];
        
        List<Contact> listContact = [SELECT Id, Email, AccountId
                                     FROM Contact
                                     WHERE AccountId IN :mapSchoolIdToAccountId.values()
                                     AND Email != null
                                     AND Contact_Status__c = 'Active'
                                     AND Role__c = 'Primary Decision Maker'];
        
        List<Contact> listCpContacts = [SELECT Id, Email, AccountId
                                       FROM Contact
                                       WHERE AccountId IN :mapSchoolIdToAccountId.values()
                                       AND Contact_Status__c = 'Active'
                                       AND Email != null
                                       AND CP_Contact__c = true];
        
        for(Id schoolId :mapSchoolIdToAccountId.keySet()){
            Id accountId = mapSchoolIdToAccountId.get(schoolId);
            Boolean hasCpContact = false;
            
            for(Contact cpContact : listCpContacts){
                if(cpContact.AccountId == accountId){
                    hasCpContact = true;
                    if(!mapSChoolIdToReceipants.containsKey(schoolId)){
                        mapSchoolIdToReceipants.put(schoolId, new Map<Id, String>());
                    }
                    mapSchoolIdToReceipants.get(schoolId).put(cpContact.Id, cpContact.Email);
                }
            }
            
            if(!hasCpContact){

                for(CF_Admin_Contact__c contactAdmin :listCFAdmins){
                    if(contactAdmin.CF_Admin_Account__c == accountId){                   
                        //add contact id and email to mapSchoolIdToReceipants 
                        if(!mapSchoolIdToReceipants.containsKey(schoolId)){
                            mapSchoolIdToReceipants.put(schoolId, new Map<Id, String>());
                        }
                        mapSchoolIdToReceipants.get(schoolId).put(contactAdmin.Contact__c , contactAdmin.Contact__r.Email);
                    }
                }
                for(Contact contact :listContact){
                    if(contact.AccountId == accountId){
                        if(!mapSchoolIdToReceipants.containsKey(schoolId)){
                            mapSchoolIdToReceipants.put(schoolId, new Map<Id, String>());
                        }
                        mapSchoolIdToReceipants.get(schoolId).put(contact.Id , contact.Email);
                    }
                }
            }
        }       
        return mapSchoolIdToReceipants;
    }
    
    private void sendNotificationForCoursefinderSchool(Map<Id, Map<Id, String>> mapSchoolIdToReceipantsUponLimit,
                                                      Map<Id, Map<Id, String>> mapSchoolIdToReceipantsUnderLimit){
        
        Map<Id, Messaging.SingleEmailMessage> mapSchoolIdToEmails = new Map<Id, Messaging.SingleEmailMessage>();
        
        OrgWideEmailAddress[] listOwea = [SELECT Id, Address FROM OrgWideEmailAddress WHERE Address ='rholmes@icef.com'];

        List<string> listTemplateNames = new List<String>{'New_CoursePricer_Agency_added_but_limit_reached',
            'New_CoursePricer_Agency_added'};
        List<EmailTemplate> ListTemplates = [SELECT Id, DeveloperName
                                             FROM EmailTemplate
                                             WHERE DeveloperName In :listTemplateNames];
        Id limitReachedTemplate, underLimitTemplate;
                                                          
        
         for(EmailTemplate emailTemplate :ListTemplates){
                
                if(emailTemplate.DeveloperName.equalsIgnoreCase('New_CoursePricer_Agency_added_but_limit_reached')){
                    limitReachedTemplate = emailTemplate.Id;
                }
                if(emailTemplate.DeveloperName.equalsIgnoreCase('New_CoursePricer_Agency_added')){
                    underLimitTemplate = emailTemplate.Id;
                }
         } 
		 if(limitReachedTemplate == null){
               System.assert(false, 'Template not Found - New_CoursePricer_Agency_added_but_limit_reached');
         }
         if(underLimitTemplate == null) {
               System.assert(false, 'Template not Found - New_CoursePricer_Agency_added');
         }   
        
        // Build Email for Receipants Upon Limit
        for(Id coursefinderSchoolId: mapSchoolIdToReceipantsUponLimit.keySet()){
            
            Map<Id, String> mapReceipants = mapSchoolIdToReceipantsUponLimit.get(coursefinderSchoolId);
            
            if(!mapReceipants.isEmpty()){
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                // The email template ID used for the email
                System.debug('limitReachedTemplate Id = ' + limitReachedTemplate);
                mail.setTemplateId(limitReachedTemplate);
                List<Id> listContactId = new List<Id> (mapReceipants.keySet());
                System.debug('setTargetObjectId Contact Id = ' + listContactId[0] + ' and coursefinderSchoolId = '  +coursefinderSchoolId);
                mail.setTargetObjectId(listContactId[0]);
                mail.setWhatId(coursefinderSchoolId);  
                mail.setToAddresses(mapReceipants.values());               
                mail.setSaveAsActivity(false);                
                if(!listOwea.isEmpty()){
                    mail.setOrgWideEmailAddressId(listOwea.get(0).Id);
                }
                mapSchoolIdToEmails.put(coursefinderSchoolId, mail);
            }
        }
        // Build Email for Receipants under Limit
        for(Id coursefinderSchoolId: mapSchoolIdToReceipantsUnderLimit.keySet()){
            
            Map<Id, String> mapReceipants = mapSchoolIdToReceipantsUnderLimit.get(coursefinderSchoolId);
            
            if(!mapReceipants.isEmpty()){
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                // The email template ID used for the email
                mail.setTemplateId(underLimitTemplate);
                System.debug('underLimitTemplate Id = ' + underLimitTemplate);
                List<Id> listContactId = new List<Id> (mapReceipants.keySet());
                System.debug('setTargetObjectId Contact Id = ' + listContactId[0] + ' and coursefinderSchoolId = '  +coursefinderSchoolId);
                mail.setTargetObjectId(listContactId[0]);
                mail.setWhatId(coursefinderSchoolId);   
                mail.setToAddresses(mapReceipants.values());               
                mail.setSaveAsActivity(false);                
                if(!listOwea.isEmpty()){
                    mail.setOrgWideEmailAddressId(listOwea.get(0).Id);
                }
                mapSchoolIdToEmails.put(coursefinderSchoolId, mail);
            }
        }
                                                          
        List<Messaging.SendEmailResult> results = Messaging.sendEmail(mapSchoolIdToEmails.values(), false);
        sendCopyOfEmail(mapSchoolIdToEmails, results);
        System.debug('listEmails = ' + json.serialize(mapSchoolIdToEmails));
        handleSendEmailResult(results);
    }
    
    private void sendCopyOfEmail(Map<Id, Messaging.SingleEmailMessage> mapSchoolIdToEmails, 
                                 List<Messaging.SendEmailResult> sendEmailResult){
                                     
        List<Messaging.SingleEmailMessage> listEmails = new List<Messaging.SingleEmailMessage>();
        
        Map<Id,Coursefinder_School__c> mapCoursefinderSchool = new Map<Id,Coursefinder_School__c>([SELECT Id, Account__r.OwnerId, 
                                                                                                   Account__r.Owner.email
                                                                                                   FROM Coursefinder_School__c
                                                                                                   WHERE Id In : mapSchoolIdToEmails.keySet() ]);
        
        for(Integer idx=0; idx<mapSchoolIdToEmails.values().size(); idx++){

             Messaging.SingleEmailMessage sentEmail = mapSchoolIdToEmails.values()[idx];
            
            if(sendEmailResult[idx].IsSuccess()){
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                Id schoolId = sentEmail.getWhatId();
                Coursefinder_School__c coursefinder_School = mapCoursefinderSchool.get(schoolId);
                
                List<String> listReceipants = new List<String>();
                listReceipants.add('rholmes@icef.com');
                if(coursefinder_School.Account__r.OwnerId != null && String.isNotBlank(coursefinder_School.Account__r.Owner.email)){
                    listReceipants.add(coursefinder_School.Account__r.Owner.email);
                }
                System.debug('sendCopyOfEmail listReceipants= ' + json.serialize(listReceipants));
                mail.setSubject(sentEmail.getSubject());
                //mail.setPlainTextBody(sentEmail.getPlainTextBody());
                mail.setHtmlBody(sentEmail.getHtmlBody());
                mail.setToAddresses(listReceipants);                 
                mail.setSaveAsActivity(false);
                listEmails.add(mail);
            }
            
        }
        List<Messaging.SendEmailResult> results = Messaging.sendEmail(listEmails);
        System.debug('sendCopyOfEmail results= ' + results);
    }
    
    private void handleSendEmailResult(List<Messaging.SendEmailResult> sendEmailResult){
        
        Integer successCount = 0;
        Integer failedCount = 0;
        String resultFileContent = '';
        String errorMsg;
        String[] fillers;
        
        if(sendEmailResult.isEmpty()) { return; }
        
        for(Messaging.SendEmailResult result : sendEmailResult) {
            if(result.IsSuccess()) {
                successCount++;
            }else{
                errorMsg = result.getErrors()[0].getMessage();
                fillers = new String[] {String.valueOf(result.getErrors()[0].getTargetObjectId()), 
                    String.valueOf(result.IsSuccess()),
                    errorMsg};
               resultFileContent += String.format('\n{0},{1},{2}', fillers);
            }            
        }        
        failedCount = sendEmailResult.size() - successCount;
        if(Test.isRunningTest()){
           failedCount = 2;
        }
        if(failedCount < 1) { return; }
        
        String emailBodyTemplate = 'CP Agencies - Send Email Notification Result.<br/>'
            + '<br/> Total Records for Processing: {0}'
            + '<br/> {1} emails sent successfully. '
            + '<br/> {2} emails failed in operation';
        fillers = new String[] {String.valueOf(sendEmailResult.size()), 
            String.valueOf(successCount), 
            String.valueOf(failedCount)};

        String emailBody = String.format(emailBodyTemplate, fillers);
        resultFileContent = '"Coursefinder School ID","Success","Message"' + resultFileContent;
        
        sendNotificationEmail(emailBody, resultFileContent);        
    }

    private void sendNotificationEmail(String htmlBody, String csvFileContent) {
        List<String> listEmailReceipants = new List<String>();
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();  
        message.setSubject('Failed Email Notification Result - Limit of CP Agencies reached');
        message.setHtmlBody(htmlBody);
        
        // fetch error notification Receipants from custom setting
        List<Limit_of_CP_Agencies_Error_Notification__c> listErrorReceipants = Limit_of_CP_Agencies_Error_Notification__c.getall().values();

        for(Limit_of_CP_Agencies_Error_Notification__c errorReceipants : listErrorReceipants){
            listEmailReceipants.add(errorReceipants.Email__c);
        }
        System.debug('\n listEmailReceipants :' + listEmailReceipants);       
        message.setToAddresses(listEmailReceipants);  
         
        if(String.isNotBlank(csvFileContent)) {
            String todayDate = System.today().format();
            Messaging.EmailFileAttachment csvFileAttach = new Messaging.EmailFileAttachment();
            csvFileAttach.setFileName('Limit of CP Agencies reached  [' + todayDate + '] - Result.csv');
            csvFileAttach.setContentType('application/vnd.ms-excel');
            csvFileAttach.setBody(Blob.valueOf(csvFileContent));         

            message.FileAttachments = new Messaging.EmailFileAttachment[] {csvFileAttach};
        }
        
        if(!Test.isRunningTest() && !listEmailReceipants.isEmpty()) {
            List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>{message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);        
            if (!results[0].success) {
                System.debug('\n Email  Deilvery failed:' + results[0].errors[0].message);
            }        
        }
    }
}