public with sharing class getAPItokenAction {
	
    @InvocableMethod(label='get API token' description='updates API token in Contact')
    public static List<Id> getApiToken(List<Id> ContactIds){
        List<Id> listToReturn = new List<Id>();
        List<Contact> contactList = [SELECT Id FROM Contact WHERE Id IN :contactIds];
        List<Contact> listToUpdate = new List<Contact>();
        DateTime saltDate = DateTime.now();
        
        for(Contact con : contactList){
        	String apiString = String.valueOfGmt(saltDate)+String.valueOf(con.Id);
            Blob apiBlob = Crypto.generateDigest('SHA-256', Blob.valueOf(apiString));
            con.Api_Token__c = EncodingUtil.convertToHex(apiBlob);
            listToUpdate.add(con);
            listToReturn.add(con.Id);
        }
        update listToUpdate;
        return listToReturn;
    }
    
}