public with sharing class AGeoCode extends GoogleAPIUtility {
    public String Address; 
    public String con;
    public Account l; 
    public Boolean problem=false; 
    public Account getl(){return l;}
    public String getGKey(){ 
        try{
        findNearby__c settings = findNearby__c.getInstance();
        return settings.GKey__c;
        }
        catch(Exception e){return ' ';}
        
    }
    //Currently setting the continue flag with a Boolean - though the value is a String
    //This is probably not a good idea.
    public void setContinue(Boolean flag){ 
        if(flag){ con ='T';}
        else{ con ='F';} 
    }
    public String getContinue(){return con;}
     
     
     
    /*
    This is the first method to be called.
    Determines if Accounts are turned off at the Global Level.
    If they are ON continue to "GetAddress()" 
    If they are OFF continue to the CGeoCodePrep page
    */ 
    public pageReference init(){ 
        Boolean doIDoThis = false; 
        try{
            
            FindNearby__c FNA = FindNearby__c.getOrgDefaults();
            doIDoThis = FNA.Accounts__c;
        }catch(Exception e){doIDoThis = false;}
        
        if(!doIDoThis){
            return  Page.CGeoCodePrep;
        }
        
        getAddress();
        return null;    
    }
    
    
    /*
        Gets the address of the next item to map. 
    */
    public String getAddress(){ 
        
        //Reset the values
        //-----------------
        setContinue(false);
        String Address = '-';
        l = new Account();
        //-----------------
         
        String id = ApexPages.currentPage().getParameters().get('id'); 
        
        l = MapUtility.getSingleAccountToPlot(id);
        
        problem = false; // Just got a new address, no problems yet. 
        
        
        if(l != null ){
            setContinue(true);
            MapItem a = new MapItem(l);
            Address = a.rAddress;   
        }       
        
        return Address;
    }
    
    
    public PageReference result() {
        //Get the Status and the Accuracy of the result
        String code = Apexpages.currentPage().getParameters().get('Stat');
        system.debug('code:: '+code);
        
        if(code == 'OK')
            code = 'Located';
        else if(code == 'ZERO_RESULTS')
            code = 'Problem with Address';
        else if(code == 'OVER_QUERY_LIMIT')
            code = 'Google Exhausted';
        else if(code == 'REQUEST_DENIED')    
            code = 'Google Exhausted';
        else if(code == 'INVALID_REQUEST')
            code = 'Problem with Address';
                
        //String Accuracy = Apexpages.currentPage().getParameters().get('Acc');
        
        //If there is a Lead to map...
        if(l !=null)
        {
            Account a = new Account(id=l.Id);
            //Clean up the message
        //    a.Mapping_Status__c = MapUtility.DetermineStatus(code,Accuracy);        
            a.Mapping_Status__c = code;               
            //Determin what to do.  
            if(a.Mapping_Status__c == 'Located')
             
            {   try{
                    a.Lat__c = Double.valueOf(Apexpages.currentPage().getParameters().get('Lat'));
                    a.Lon__c = Double.valueOf(Apexpages.currentPage().getParameters().get('Lon'));
                  
                   
                    System.debug('a.Lat__c::'+a.Lat__c+' a.Lon__c::'+a.Lon__c);
                }
                catch(Exception e){
                    a.Mapping_Status__c = 'Problem with Address';
                    System.debug('AGeoCode:' + e + ' Lat:'+Apexpages.currentPage().getParameters().get('Lat')+' Lon:'+Apexpages.currentPage().getParameters().get('Lon'));
                }
            }
            if(a.Mapping_Status__c == 'Bad Google Maps Key')
            {
                return Page.MapError_Google_Key;
            }
            if(a.Mapping_Status__c == 'Google Exhausted')
            {
                return Page.MapError_TooMany;
            }
            
            //If there was a problem with the Address
            if(a.Mapping_Status__c =='Problem with Address')
            {
                problem = true; 
            }
            
            update a; 
            System.debug('a::'+a+' a.Lat__c::'+a.Lat__c+' a.Lon__c::'+a.Lon__c);
            if(ApexPages.currentPage().getParameters().get('id') != null){
                    System.debug('in if::');
                    return done();
            }
            return null;
        }
        
        return done();
        
    }
    public String getLName(){
        String name = '-';
        try{
            name = String.escapeSingleQuotes(l.Name);
        }
        catch(Exception e){}
        return name;
    }
    public PageReference done(){ 
        String id = ApexPages.currentPage().getParameters().get('id'); 
        system.debug('problem:: '+problem);
        if(id != null)
        {
            //If there was a problem with the Address
            if(problem)
            {
                return Page.MapError_ProblemAddress;
            }
            
            PageReference p = new PageReference('/apex/FindNearbyMap?lid=null&aid='+id);
            return p;
        }
        else{
            return Page.CGeoCodePrep;
        }
    }
    
//*********************************************************************************
    private static testMethod void TestAccountGeoCodeController() {
        AGeoCode trol = new AGeoCode ();
        
        //Test setContinue
        trol.setContinue(true);
        System.assert(trol.con=='T');       
        trol.setContinue(false);
        System.assert(trol.con=='F');       
        
        
        //Test GetContinue
        System.assert(trol.con == trol.GetContinue());
        
        //Test GetLName
        System.assert(trol.getLName() != null);
        System.assert(trol.done() != null);

        
        Country__c usa = new Country__c(Name = 'USA');
        usa.Agent_Relationship_Manager__c = UserInfo.getUserId();
        insert usa;
        
        Account tL = new Account();
        tL.Name = 'Iman';
        tL.Which_Address__c = 'Shipping';
        tL.ShippingStreet = 'a';
        tL.ShippingCity = 'a';
        tL.ShippingState = 'Ca';
        tL.ShippingPostalCode = '94105';
        tL.ShippingCountry = 'USA';
        tL.Mailing_Country__c = usa.Id;
        tL.Mailing_State_Province__c = 'CA';
        insert tL; 
        
        trol.result();
        trol.l = tL; 
        trol.getl();
        trol.getAddress();
        ApexPages.currentPage().getParameters().put('Stat', '200');
        //ApexPages.currentPage().getParameters().put('Acc', '7');
        ApexPages.currentPage().getParameters().put('Lat', '7');
        ApexPages.currentPage().getParameters().put('Lon', '7');
        trol.result();

        ApexPages.currentPage().getParameters().put('Stat', '200');
       // ApexPages.currentPage().getParameters().put('Acc', '4');
        trol.result();
        ApexPages.currentPage().getParameters().put('Stat', '610');
       // ApexPages.currentPage().getParameters().put('Acc', '7');   
        trol.result();
        ApexPages.currentPage().getParameters().put('Stat', '620');
      //  ApexPages.currentPage().getParameters().put('Acc', '7');
        trol.result();
        ApexPages.currentPage().getParameters().put('Stat', '777');
       // ApexPages.currentPage().getParameters().put('Acc', '7');     
        trol.result();
            
       	ApexPages.currentPage().getParameters().put('Stat', 'OK');     
        trol.result();
        ApexPages.currentPage().getParameters().put('Lat', 'l');
        ApexPages.currentPage().getParameters().put('Lon', 'g');
        trol.result();
        ApexPages.currentPage().getParameters().put('Stat', 'ZERO_RESULTS');
        trol.result();            
        ApexPages.currentPage().getParameters().put('Stat', 'OVER_QUERY_LIMIT'); 
        trol.result();            
        ApexPages.currentPage().getParameters().put('Stat', 'REQUEST_DENIED');     
        trol.result();        
        ApexPages.currentPage().getParameters().put('id', tL.id); 
        ApexPages.currentPage().getParameters().put('Stat', 'INVALID_REQUEST');   
        trol.result();                  	              
        ApexPages.currentPage().getParameters().put('id', tL.id); 
        trol.problem = false;
       	trol.done();
            
        trol.getGKey();
      
        trol.init();
     }
    
}