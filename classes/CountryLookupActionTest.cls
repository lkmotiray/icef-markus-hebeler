@isTest
public class CountryLookupActionTest {
	
    private static void createTestData(){
    }
    
    static testMethod void testCountryLookupValid(){
        //createtestData
        Id countryId = DataFactory.createTestCountry();
        Id recTypeId = DataFactory.getRecordTypeId('Agent', 'Lead');
        Country__c country = [SELECT Id, Name FROM Country__c WHERE Id = :countryId LIMIT 1];
        List<Id> leadList = new List<Id>();
        
        Lead led = DataFactory.createTestLead('Agent');
        led.Company = 'Test Valid Country';
        led.Country__c = country.Name;
        
        Test.startTest();
        insert led;
        Lead testLead = [SELECT Id, Country_lookup__c FROM Lead WHERE Company = 'Test Valid Country' LIMIT 1];
        leadList.add(testLead.Id);
               
        List<Id> checkLeads = CountryLookupAction.countryLookup(leadList);
        test.stopTest();
        
        Lead checkLead = [SELECT Id, Country_Lookup__c FROM Lead WHERE Id = :checkLeads[0] LIMIT 1];
        System.assertEquals(country.Id, checkLead.Country_Lookup__c);
    }
    
    static testMethod void testCountryLookupUnsinn() {
        Id recTypeId = DataFactory.getRecordTypeId('Agent', 'Lead');
        List<Id> LeadList = new List<Id>();
        
        Lead led = DataFactory.createTestLead('Agent');
        led.Company = 'Test Unsinn Country';
        led.Country__c = 'Unsinn';
                
        Test.startTest();
        insert led;
        Lead testLead = [SELECT Id, Country_lookup__c FROM Lead WHERE Company = 'Test Unsinn Country' LIMIT 1];
        leadList.add(testLead.Id);
        List<Id> checkLeads = CountryLookupAction.countryLookup(leadList);
        Test.stopTest();
        
        Lead checkLead = [SELECT Id, Country_Lookup__c FROM Lead WHERE Id = :checkLeads[0] LIMIT 1];
        System.assertEquals(null, checkLead.Country_Lookup__c);
    }
    
    static testMethod void testCountryLookupEmpty() {
        Id recTypeId = DataFactory.getRecordTypeId('Agent', 'Lead');
        List<Id> LeadList = new List<Id>();
        
        Lead led = DataFactory.createTestLead('Agent');
        led.Company = 'Test Empty Country';
        
        Test.startTest();
        insert led;
        Lead testLead = [SELECT Id, Country_lookup__c FROM Lead WHERE Company = 'Test Empty Country' LIMIT 1];
        leadList.add(testLead.Id);
        List<Id> checkLeads = CountryLookupAction.countryLookup(leadList);
        Test.stopTest();
        
        Lead checkLead = [SELECT Id, Country_Lookup__c FROM Lead WHERE Id = :checkLeads[0] LIMIT 1];
        System.assertEquals(null, checkLead.Country_Lookup__c);
    }
 
}