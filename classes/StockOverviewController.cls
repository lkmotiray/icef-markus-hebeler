/*
 * @Purpose : Controller for StockOverview
 * @Created Date : 11-6-2018
 * @Auther : (Dreamwares)
 */
public class StockOverviewController {
    
    Id eventRecordId;
    Integer index = 0;
    String previousName = '';
    public List<Summary> summaryList{get;set;}
    public List<String> productFamilyList = new List<String>();
    public String eventName {get;set;}
          
    // Constuctor for the class
    public StockOverviewController(ApexPages.StandardController sc){
        
        eventRecordId = apexpages.currentpage().getparameters().get('id');
      
        if(String.isBlank(eventRecordId)){
            System.debug('Invalid Record Id');
        }else{
            AggregateResult[] aggregateResultList;
            try{
                ICEF_Event__c event = [SELECT Id, Name 
                                       FROM ICEF_Event__c
                                       WHERE id =: eventRecordId]; 
                eventName = event.Name; 
                                       
                aggregateResultList = [SELECT count(Id),Product_status__c, Product__r.Name, Product__r.Family
                                       FROM Product_Participation__c
                                       WHERE ICEF_Event__c =: eventRecordId
                                       Group By Product_status__c, Product__r.Name, Product__r.Family 
                                       ORDER By Product__r.Family];  
            }catch(Exception ex){
                System.debug('Exception::::'+ex.getMessage());    
            }
                  
            if(aggregateResultList != null && !aggregateResultList.isEmpty()){
               
                summaryList = new List<Summary>();
                
                // Create the grouping of ProductFamily, Product Name and Product Status
                for (AggregateResult aggregateResult : aggregateResultList) {
                    
                    if(previousName != (String) aggregateResult.get('Name')){
                    
                          summaryList.add(new summary(aggregateResult));
                          index++;
                          previousName = (String) aggregateResult.get('Name'); 
                          if(!productFamilyList.contains((String)aggregateResult.get('Family'))){
                              productFamilyList.add((String)aggregateResult.get('Family'));  
                          }
                    }else{
                    
                        if((String) aggregateResult.get('Product_status__c') == 'available'){
                        summaryList[index-1].available = (Integer) aggregateResult.get('expr0'); 
                        
                        }else if((String) aggregateResult.get('Product_status__c') == 'pending payment'){
                        summaryList[index-1].pendingpayment = (Integer) aggregateResult.get('expr0');
                        
                        }else if((String) aggregateResult.get('Product_status__c') == 'registered'){
                        summaryList[index-1].registered = (Integer) aggregateResult.get('expr0');
                        }
                    }
                }
                // get count of Product in each stage of Opportunity and count Saleable product
                calculateSaleableCount();
            }
        }
    }
    
   /*
    * @Purpose : wrapper class to hold report data
    */
    public class Summary {
        
        public String productFamily { get; private set; }
        public String productName {get; private set; }
        public Integer pendingpayment { get; private set; }                  
        public Integer registered {get; private set; }
        public Integer available { get; private set; }
        public Integer closedWon {get; private set;}
        public Integer saleable{get; private set;}
        public Integer open{get; private set;}  
        
        public Summary(AggregateResult aggregateResult) {
            
            pendingpayment = 0;
            registered = 0;
            available = 0;
            saleable = 0;
            closedWon = 0;
            open = 0;
            
            if(aggregateResult != null){
              
                
                if((String) aggregateResult.get('Product_status__c') == 'available'){
                     available = (Integer) aggregateResult.get('expr0'); 
                        
                }else if((String) aggregateResult.get('Product_status__c') == 'pending payment'){
                     pendingpayment = (Integer) aggregateResult.get('expr0');
                     
                }else if((String) aggregateResult.get('Product_status__c') == 'registered'){
                     registered = (Integer) aggregateResult.get('expr0');
                }
                
                productName = (String) aggregateResult.get('Name'); 
                productFamily = (String) aggregateResult.get('Family');                             
            }
        }
    }
    
   /*
    * @Purpose : get count of Product in each stage of Opportunity and count Saleable product
    */
    public void calculateSaleableCount(){
        
        AggregateResult[] aggregateResultList;
        try{
            aggregateResultList = [SELECT SUM(Quantity), Opportunity.StageName, Product2.Name, Product2.Family 
                                   FROM OpportunityLineItem 
                                   WHERE Product2.Family IN : productFamilyList
                                   AND ICEF_Event__c =: eventRecordId
                                   AND Opportunity.StageName != 'Closed Lost'
                                   Group By Opportunity.StageName, Product2.Name, Product2.Family];                     
        }catch(Exception ex){
            System.debug('Exception::::'+ex.getMessage());
        }
          
        if(aggregateResultList != null && !aggregateResultList.isEmpty()){
              
            String oppStageName;                               
            for(AggregateResult aggregateResult :aggregateResultList){
              
                oppStageName = (String) aggregateResult.get('StageName');
                for(Summary summary :summaryList){
                
                    IF(summary.productFamily == (String) aggregateResult.get('Family') &&
                       summary.productName == (String) aggregateResult.get('Name')){
                        
                        if(oppStageName == 'Prospecting' || oppStageName == 'Qualification' || oppStageName == 'Needs Analysis' ||
                           oppStageName == 'Proposal/Price Quote' || oppStageName == 'Negotiation/Review' ||
                           oppStageName == 'Salesorder sent' || oppStageName == 'Salesorder received'){
                            
                            summary.open = summary.open + integer.valueOf(aggregateResult.get('expr0')); 
                             
                        }else if(oppStageName == 'Closed Won'){
                           
                            summary.closedWon = integer.valueOf(aggregateResult.get('expr0')); 
                             
                        }
                    }    
                }
            } 
        } 
        
        for(Summary summary :summaryList){
            
            summary.saleable = summary.available - summary.open;
            if(summary.saleable < 0){
                summary.saleable = 0;
            } 
        }
    }
}