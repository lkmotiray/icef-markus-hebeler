@isTest
public class AutoConvertLeadsTest {
	
    static testMethod void testAutoConvert(){
        
        Account testAccount = DataFactory.createTestEducators(1)[0];
        insert testAccount;
        
        Contact testContact = DataFactory.createTestContacts(testAccount, 1)[0];
        insert testContact;
        
        List<Id> leadIds = new List<Id>();
        
        List<Lead> testLeadList = DataFactory.createTestLeads(20, 'Educator');
        for (Lead testLead : testLeadList){
            testLead.LeadSource = 'Website Registration Form';
            testLead.Convert_to_Account_Id__c = testAccount.Id;
            testLead.Convert_to_Contact_Id__c = testContact.Id;
        }
        insert testLeadList;
        
        Test.startTest();
        Map<Id, Lead> leadMap = new Map<Id, Lead>([SELECT Id FROM Lead WHERE Convert_to_Account_Id__c = :testAccount.Id]);
        leadIds.addAll(leadMap.keySet());
        AutoConvertLeads.LeadAssign(leadIds);
        Test.stopTest();
        
        Integer successResults = 0;
        
        List<Lead> checkLeads = [SELECT IsConverted FROM Lead WHERE Id IN :leadIds];
        
        for (Lead checkLead : checkLeads){
            if (checkLead.isConverted){
                successResults ++;
            }
        }
        
        System.assertEquals(20, successResults);
    }
    
}