/*****************
 * Henriette
 * 26.11.2018
 *****************/

@isTest
public class ContactTriggerTest {
    
    //create Test data
    static testMethod void createTestData(){
        System.debug('createTestData() start');
	    String testCountryId = DataFactory.createTestCountry();
    	List<Account> testAccounts = DataFactory.createTestEducators(3);
        insert testAccounts;
        System.debug('createTestData() end');
    }
    
    static testMethod void updateRelevantInfoInContact(){
        System.debug('updateRelevantInfoInContact() start');
        createTestData();
        List<Account> accts = [SELECT Id, RecordTypeId, Name FROM Account WHERE Name LIKE 'Test%'];
        List<Contact> cons1 = DataFactory.createTestContacts(accts[0], 2);
        List<Contact> cons2 = DataFactory.createTestContacts(accts[1], 2);
        System.debug('insert cons before Test: start');
        DateTime checkDateTime = DateTime.now();
        insert cons1;
        insert cons2;
        System.debug('insert cons before Test: end');
        Test.startTest();
        List<Contact> consToUpdate = new List<Contact>();
        Set<Id> setAcctIds = new Set<Id>();
        List<Contact> cons = [SELECT Id, X20_year_mag__c, Contact_Email__c, Role__c, AccountId FROM Contact WHERE LastName LIKE 'LastName%'];
        for(Contact con : cons){
            con.X20_year_mag__c = true;
            con.Role__c = 'Primary Decision Maker';
            consToUpdate.add(con);
            setAcctIds.add(con.AccountId);
        }
        System.debug('update contact to fire trigger - start');
        update consToUpdate;
        System.debug('update contact to fire trigger - end');
        List<Account> accs = [SELECT Id, Has_Contact_with_emailaddress__c, Number_of_PMD_Contacts__c, X20_year_mag__c FROM Account WHERE Id IN :setAcctIds];
        Test.stopTest();
        for (Account acc : accs){
            System.assertEquals(true, acc.Has_Contact_with_emailaddress__c);
            System.assertEquals(2, acc.Number_of_PMD_Contacts__c);
            System.assertEquals(true, acc.X20_year_mag__c);
        }
		System.debug('updateRelevantInfoInContact() end');        
    }
    
    static testMethod void updateNotRelevantInfoInContact(){
        System.debug('updateNOtRelevantInfoInContact() start');
        createTestData();
        List<Account> accts = [SELECT Id, RecordTypeId, Name FROM Account WHERE Name LIKE 'Test%'];
        List<Contact> cons = DataFactory.createTestContacts(accts[0], 2);
        System.debug('insert cons before Test: start');
        DateTime checkDateTime = DateTime.now();
        insert cons;
        System.debug('insert cons before Test: end');
        Test.startTest();
        Contact con = [SELECT Id, Contact_Remarks__c, AccountId FROM Contact WHERE LastName LIKE 'LastName%' LIMIT 1];
        con.Contact_Remarks__c = 'Test Comment';
        System.debug('update contact to fire trigger - start');
        update con;
        System.debug('update contact to fire trigger - end');
        Account acc = [SELECT Id, LastModifiedDate, CreatedDate FROM Account WHERE Id = :con.AccountId];
        Test.stopTest();
        System.debug('Created: ' + acc.CreatedDate);
        System.debug('Modified: '+ acc.LastModifiedDate);
        System.debug('checkDateTime: ' + checkDateTime);
//        System.assertEquals(acc.LastModifiedDate, acc.CreatedDate);
		System.debug('updateNotRelevantInfoInContact() end');
    }
	
    static testMethod void insertDeleteContacts(){
        createTestData();
        List<Account> accts = [SELECT Id, RecordTypeId, Name FROM Account WHERE Name LIKE 'Test%'];
        List<Contact> conList = DataFactory.createTestContacts(accts[0], 4);
        Contact con = conList[0];
        con.Role__c = 'Primary Decision Maker';
        con.X20_year_mag__c = true;
        con.Contact_Email__c = 'test@icef.com';
        
        //testinsertion of contacts
        Test.startTest();
        insert conList;
        
        Account testAccount = [SELECT Id, X20_year_mag__c, Number_of_PMD_Contacts__c, Has_Contact_with_emailaddress__c
                              FROM Account
                              WHERE Name = 'Test Educator 0'
                              LIMIT 1];
        System.debug('testAccount: ' + testAccount);
        System.debug('conList: ' + conList);
        System.assertEquals(true, testAccount.X20_year_mag__c);
        System.assertEquals(true, testAccount.Has_Contact_with_emailaddress__c);
        System.assertEquals(1, testAccount.Number_of_PMD_Contacts__c);
        
        //test deletion of one contact
        delete con;

        Account testAccount2 = [SELECT Id, X20_year_mag__c, Number_of_PMD_Contacts__c, Has_Contact_with_emailaddress__c 
                              FROM Account
                              WHERE Name = 'Test Educator 0'
                              LIMIT 1];

        Test.stopTest();
        System.assertEquals(false, testAccount2.X20_year_mag__c);
        System.assertEquals(true, testAccount2.Has_Contact_with_emailaddress__c);
        System.assertEquals(0, testAccount2.Number_of_PMD_Contacts__c);
        
        //test deletion of all contacts
        List<Contact> allContacts = [SELECT Id FROM Contact WHERE AccountId = :testAccount.Id];
        delete allContacts;

        Account testAccount3 = [SELECT Id, X20_year_mag__c, Number_of_PMD_Contacts__c, Has_Contact_with_emailaddress__c
                              FROM Account
                              WHERE Name = 'Test Educator 0'
                              LIMIT 1];        
        
        System.assertEquals(false, testAccount3.X20_year_mag__c);
        System.assertEquals(false, testAccount3.Has_Contact_with_emailaddress__c);
        System.assertEquals(0, testAccount3.Number_of_PMD_Contacts__c);                
    }
    
    static testMethod void updateX20year(){
        createTestData();
        Test.StartTest();
        Account acct = [SELECT Id, Name, RecordTypeId FROM Account WHERE Name LIKE 'Test%' LIMIT 1];
        List<Contact> cons = DataFactory.createTestContacts(acct, 4);
        insert cons;
        List<Contact> consToUpdate = new List<Contact>();
        List<Contact> consToChange = [SELECT X20_year_mag__c FROM Contact WHERE AccountId = :acct.Id LIMIT 3];
        for (Contact con : consToChange){
            con.X20_year_mag__c = true;
            consToUpdate.add(con);
        }
        update consToUpdate;
               
        Account acctToCheck = [SELECT X20_year_mag__c FROM Account WHERE Id = :acct.Id];
        Test.stopTest();
        System.assertEquals(true, acctToCheck.X20_year_mag__c);
    }
}