/**
  * @Author      : Prajakta
  * @Description : Test class for AttachmentPickerControllerEmailAddressLookupController)
  * @Created Date: 13 May 2016
 */
@isTest
public class AttachmentPickerTest
{
    /**
      * @Description : create Test Data
     */
    @TestSetup
    static void createTestData() {
        List<Folder> listFolder = [SELECT Id FROM Folder WHERE Type='Document' AND Name != 'CustomEmailTempAttachment'];
        
        
        List<Document> listDocuments = new List<Document>();
        for(Integer i =0; i < 3; i++) {
            listDocuments.add(new Document( Name = 'Doc '+i,
                                            Body = Blob.valueOf('Test body'),
                                            FolderId = listFolder[i].id,
                                            ContentType = 'text'
                                          ));
        }
        
        insert listDocuments;
    }
    
    /**
      * @Description : verify AttachmentPickerController functionality
     */
    static testmethod void testEmailAttachmentPicker() {
        Test.startTest();
        
        AttachmentPickerController attachmentPickerController = new AttachmentPickerController();
        //--Verify upload method
        attachmentPickerController.document.Name = 'Local system file to upload';
        attachmentPickerController.document.body = Blob.valueOf('Local system file to upload');
        attachmentPickerController.upload();
        
        Document documentRec = [ SELECT Id FROM Document WHERE Name = :'Local system file to upload'];
        System.assert(documentRec.id != null);
        
        
        List<Document> listDocuments  = attachmentPickerController.getDocumentFiles();
        System.assertEquals(3, listDocuments.size());
        
        List<SelectOption> listFileLocation = attachmentPickerController.getLocations();
        
        Test.setCurrentPageReference(new PageReference('Page.AttachmentPicker')); 
        
        attachmentPickerController.selctedLocation = 'Document';
        List<Folder> listFolders = attachmentPickerController.getDocumentFolders();
        
        if(!listFolders.isEmpty())
        {
            Apexpages.currentPage().getParameters().put('selectedfolder', listFolders[0].id);
            attachmentPickerController.getFilesFromDocFolder();   
        }
        
        attachmentPickerController.selctedLocation = UserInfo.getUserId();
        attachmentPickerController.getFilesFromDocFolder();
        
        AttachmentPickerController attachmentPickerController1 = new AttachmentPickerController();
        attachmentPickerController1.document.body = Blob.valueOf('Local system file to upload');
  
        attachmentPickerController1.upload();
        
        Test.stopTest();  
    }  
}