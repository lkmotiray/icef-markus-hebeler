public with sharing class cpselecttemplate 
{
    //public String selectedOption = null;
    public String sendEmailUrl;
    List<Folder> f = [select Name from Folder where Type =: 'Email' Order by Name];
    Public List<EmailTemplate> et = new List<EmailTemplate>();
    public List<eturl> allurl {get; private set;} 
    List<eturl> selectedeturlId = new List<eturl>();
    List<eturl> all = new List<eturl>();
    List<EmailTemplate> allemailtemplates = new List<EmailTemplate>();
    public string cpId = ApexPages.CurrentPage().getParameters().get('cpid');
    public string fromEmail = ApexPages.CurrentPage().getParameters().get('from');
    public string testCpId {get; set;}
    public string selectedTemplateFolder;
        
    public cpselecttemplate()
    {
        allurl = new List<eturl>();
    }
    public List<SelectOption> getItems()
    {
        this.allurl = new List<eturl>();
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('Unfiled Public Email Templates','Unfiled Public Email Templates'));
        //options.add(new SelectOption('My Personal Email Templates', 'My Personal Email Templates'));
        options.add(new SelectOption('--None--', '--None--'));
        for(integer i =0; i<f.size(); i++)
        {
            options.add(new SelectOption(f[i].Name,f[i].Name));
        }
        return options;
    }
    
    public PageReference showSelectedFoldertemplates()
    {
        //added for test method
        if(cpId == null){
            cpId = testCpId;
        }
        System.debug('selectedOption:::'+selectedOption);
        //storeRecentlyUsedEmailTemplate.assignRecentlyUsedEmailTemplate(selectedOption);   
        
        EmailTemplateSettings__c s = EmailTemplateSettings__c.getInstance();
        s.Recently_used_email_template__c = selectedOption;
        Database.UpsertResult result = Database.upsert(s);  
        
        System.debug('s.Recently_used_email_template__c::'+s.Recently_used_email_template__c);
        List<Folder> fldr = [Select id from Folder where Name =: selectedOption and Type = 'Email'];
        System.debug('fldr:'+fldr);
        et.clear();
        allemailtemplates.clear();
        all.clear();
        if(fldr.size() != 0)
            et = [Select Name, Description, TemplateType, IsActive from EmailTemplate where FolderId =: fldr[0].Id Order by Name];
        else
            et.clear();
        allemailtemplates.addAll(et);
        if(allemailtemplates != null)
        {
            for(EmailTemplate emailtemp : allemailtemplates)
            {
                all.add(new eturl(emailtemp));
            }
        }
        allurl.clear();
        for(Object et1 : all)
        {
            allurl.add((eturl)et1); 
        }
        return null;
    }
    
    public void settestCpId(String id){
        this.testCpId = id;
    }
    
    public String gettestCpId(){
        return testCpId;
    }    
    
    public String selectedOption
    {
       get
        {
           if(selectedOption==null)
           {
               string etemp = EmailTemplateSettings__c.getInstance().Recently_used_email_template__c;               
               selectedOption = etemp; //selectedTemplateFolder; 
                      
           }
           return selectedOption;
        }
    set;
    }
    
    Public List<EmailTemplate> getet()
    {
        return et;
    }
    
    public class eturl //wrapper class
    {
        public String emailtempUrl {get; set;}
        public string emailTemplateName {get;set;}
        public String emailTemplateDesc {get; set;}
        public string emailTemplateType {get;set;}
        public boolean emailTemplateActive {get;set;}
        public string cpId {get;set;}
        public string fromEmail {get;set;}
        //public string testCpId = '';       
               
        public EmailTemplate obj
        {
            get;        
            set;           
        }   
                
        public eturl (EmailTemplate obj)
        {
            this.obj = obj;
            cpId = ApexPages.CurrentPage().getParameters().get('cpid');           
            fromEmail = ApexPages.CurrentPage().getParameters().get('from');   
            //added for test method
            if(cpId == null){
                //cpId = testCpId;
            }
            
            emailtempUrl = '/apex/sendemailtocontactparticipation?etid='+this.obj.Id+'&id='+cpId+'&from='+fromEmail;
            emailTemplateName = this.obj.Name;
            emailTemplateDesc = this.obj.Description;
            emailTemplateType = this.obj.TemplateType;    
            emailTemplateActive = this.obj.IsActive; 
        }
    }
    
    public Pagereference cancel()
    {    
          Pagereference p = new PageReference('/apex/sendemailtocontactparticipation?id='+cpId+'&from='+fromEmail);
          p.setRedirect(true);     
          return p;
    }
    
   /* @IsTest
    public static void cpselecttemplateTest(){
    
    Test.startTest();
    Account acc = new Account(Name = 'Test Account', Status__c = 'Active');
    insert acc;
    
    Contact contact = new Contact(Salutation = 'Mr', Gender__C='male', FirstName='newContactFirstName', LastName='newContactLastName', AccountId=acc.Id);
    insert contact;
    
    date dueDate = date.newInstance(2014, 1, 30);
    ICEF_Event__c icefEvent = new ICEF_Event__c(Name='Test IcefEvent', Event_City__c = 'City', technical_event_name__c = 'TestingEvent', Event_Manager_Educators__c = UserInfo.getUserId(), Event_Manager_Agents__c = UserInfo.getUserId(), Start_date__c=dueDate);
    insert icefEvent;
    
    Country__c country = new Country__c(Name='Test Country', Region__c = 'Asia');
    insert country;
            
    Account_Participation__c ap = new Account_Participation__c(Account__c=acc.Id,ICEF_Event__c=icefEvent.Id, Organisational_Contact__c=contact.Id,Participation_Status__c='accepted', Catalogue_Country__c=country.Id, Country_Section__c=country.Id);
    insert ap;
        
    Contact_Participation__c cp = new Contact_Participation__c(Contact__c = contact.Id, ICEF_Event__c = icefEvent.Id, Account_Participation__c = ap.Id, Participation_Status__c = 'Accepted', Catalogue_Position__c = 2);
    insert cp;
       
    cpselecttemplate testObject = new cpselecttemplate();
    
    /**
    Start fooling around
    */
    
/*    EmailTemplate et;
    List<Folder> f = [select Name from Folder where Type =: 'Email'];
    
    List<Folder> fldr = [Select id from Folder where Name =: f[0].Name];
    if(fldr.size() != 0)
        et = [Select Name, Description, TemplateType, IsActive from EmailTemplate where FolderId =: fldr[0].Id LIMIT 1];
    else
        et.clear();
    
    if(et != null){
        eturl wrapObj = new eturl(et);
    }
    
  */  
    
    /**
    End fooling around
    */

    /*testObject.testCpId = cp.Id;
    testObject.getItems();
    testObject.cancel();
    testObject.showSelectedFoldertemplates();
    //testObject.getSelectedOption();
    //testObject.setSelectedOption('Test');
    testObject.getet();
    
    Test.stopTest();   
        
    }*/
    
}