/*
 *  @purpose Handler for QuoteLineSyncTrigger
 *  @recreatedby Mamta Kukreja
 *  @recreateddate 30/6/15
 */
public class QuoteLineSyncTriggerHandler{

    //update tax of following quotes
    List<Quote> listQuotesToUpdate;

    /*
     *  @purpose : to populate required fields before insert
     *  @param : trigger.new list
     *  @returntype : void
     */
    public void beforeInsert(List<QuoteLineItem> newTriggerList) {
        if (QuoteSyncUtil.isRunningTest) {
            for (QuoteLineItem qli : newTriggerList) {
                QuoteSyncUtil.populateRequiredFields(qli); // checks if any required field is null and populates it
            }
        }       
    }
    
    /*
     *  @purpose : syncing OpportunityLineItem to QuoteLineItem and calculating tax after insert
     *  @param : trigger.new list
     *  @returntype : void
     */
    public void afterInsert(List<QuoteLineItem> newTriggerList) {
        
        Map<Id, Quote> quotes = createMapOfQuotes(newTriggerList,true,false,false);
        Set<Id> oppIds = createSetOfOppIds(quotes,true,false);
        system.debug('oppIds : '+oppIds);
        
        if(oppIds.size() > 0) {
            System.debug('within if');
            syncOliToQli(oppIds, quotes, false, null, true, newTriggerList);
        }
        else { // handle validation on ICEF_Event__c
            System.debug('within else');
            for(QuoteLineItem q : newTriggerList) {  
                System.debug('q.ICEF_Event__c: '+q.ICEF_Event__c);
                if(q.ICEF_Event__c == null) {
                    q.adderror('Please enter the relevant workshop or choose "Non-Workshop Product"');
                }               
            }  
        } 
        //update tax 
        updateTax();
    }
    
    /*
     *  @purpose : syncing OpportunityLineItem to QuoteLineItem and calculating tax after update
     *  @param : trigger.new list, trigger.old list and trigger.oldMap
     *  @returntype : void
     */
    public void afterUpdate(List<QuoteLineItem> newTriggerList, List<QuoteLineItem> oldTriggerList, Map<Id,QuoteLineItem> triggerOldMap) {
        
        Map<Id, Quote> quotes = createMapOfQuotes(newTriggerList,false,true,false);
        Set<Id> oppIds = createSetOfOppIds(quotes,false,true);
        system.debug('oppIds : '+oppIds);
        
        if(oppIds.size() > 0) {
            System.debug('within if');
            syncOliToQli(oppIds, quotes, true, triggerOldMap, false, newTriggerList);
        }
        else { // handle validation on ICEF_Event__c
            System.debug('within else');
            for(QuoteLineItem q : newTriggerList) {                              
                System.debug('q.ICEF_Event__c: '+q.ICEF_Event__c); 
                if(q.ICEF_Event__c == null) {                   
                    q.adderror('Please enter the relevant workshop or choose "Non-Workshop Product"');
                }               
            }  
        }
        //update tax 
        updateTax();
    }
    
    /*
     *  @purpose : calculating tax after delete
     *  @param : trigger.old list
     *  @returntype : void
     */
    public void afterDelete(List<QuoteLineItem> oldTriggerList) {
        createMapOfQuotes(oldTriggerList,false,false,true);  
        System.debug('After delete map of quotes, list is '+listQuotesToUpdate);
        updateTax();
    }
    
    /*
     *  @purpose : updating tax
     *  @param : none
     *  @returntype : none
     */
    public void updateTax(){
        if(listQuotesToUpdate.size() > 0){
            try{
                System.debug('Updating quotes tax val : '+listQuotesToUpdate);
                Database.update(listQuotesToUpdate);     
            }
            catch(Exception e){
                System.debug(e.getMessage());
            }
        }
    }
    
    
    /*
     *  @purpose : creating map of quotes
     *  @param : trigger.new list
     *  @returntype : map of quote id to quote record
     */
    public Map<Id, Quote> createMapOfQuotes(List<QuoteLineItem> newTriggerList, Boolean isInsert, Boolean isUpdate, Boolean isDelete){
    
        String qliFields = QuoteSyncUtil.getQuoteLineFieldsString();

        Set<Id> qIds = new Set<Id>();
        Set<Id> qliIds = new Set<Id>();
        for (QuoteLineItem qli : newTriggerList) {
            qIds.add(qli.QuoteId); 
            qliIds.add(qli.Id);  
        }
        System.debug(qIds);
        System.debug('qliIds : '+qliIds);
        
        String quoteRelationQueryString = '';
        
        quoteRelationQueryString = 'SELECT id, Tax, OpportunityId, Opportunity.SyncedQuoteId, isSyncing, ( SELECT Id, QuoteId, Tax__c, TotalPrice, PricebookEntryId, UnitPrice, Quantity, Discount, ServiceDate, SortOrder' + qliFields + ' FROM QuoteLineItems)  FROM Quote WHERE Id IN :qIds';
        Map<Id, Quote> quotes = new Map<Id, Quote>();
        
        Decimal totalTaxAmount;
        String taxAmount;
        Integer taxAmt;
        listQuotesToUpdate = new List<Quote>();
        
        List<Quote> listAllQuotes = Database.query(quoteRelationQueryString);
        
        for (Quote quoteRec : listAllQuotes ) {
                totalTaxAmount = 0; 
                taxAmt = 0;
                for(QuoteLineItem qli : quoteRec.quotelineitems)
                {
                    taxAmount = qli.Tax__c;
                    if(taxAmount != null)
                    {
                        taxAmount = taxAmount.Replace('%', '');
                        taxAmt = Integer.valueOf(taxAmount);                
                        totalTaxAmount += ((qli.TotalPrice * taxAmt) / 100); 
                    }
                }
                if(!isDelete)
                    quotes.put(quoteRec.id, quoteRec);
                System.debug('quoteRec : '+quoteRec+ ' quoteRec.quotelineitems : '+quoteRec.quotelineitems);
            quoteRec.Tax = totalTaxAmount;
            listQuotesToUpdate.add(quoteRec);
        }
        System.debug('quotes : '+quotes);
        if(isDelete){
            return null;
        }    
        return quotes;
    }
    
    /*
     *  @purpose : creating set of opportunity ids
     *  @param : map of quotes, boolean value denoting if trigger was fired on insert or update
     *  @returntype : set of opp ids
     */
    public Set<Id> createSetOfOppIds(Map<Id, Quote> quotes,Boolean isInsert,Boolean isUpdate){
        Set<Id> oppIds = new Set<Id>();
        
        if(TriggerStopper.newQuote)  {              // to check if this trigger was fired when new quote is created
            for (Quote quote : quotes.values()) {
                if ((isInsert || (isUpdate && quote.isSyncing))) {
                   oppIds.add(quote.OpportunityId);      
                }
            }
        }
        else {               // Directly a new QuoteLineItem is inserted 
            for (Quote quote : quotes.values()) {
                System.debug( ' quote.Opportunity.SyncedQuoteId::::'+quote.Opportunity.SyncedQuoteId );
                if ((isInsert || (isUpdate && quote.isSyncing)) && (quote.Opportunity.SyncedQuoteId != null) ) {
                   oppIds.add(quote.OpportunityId);         
                }
            }      
        }  
        return oppIds;
    } 

    /*
     *  @purpose : creating map of opportunities
     *  @param : set of opportunity ids
     *  @returntype : Map of opportunity id to list of opportunities
     */
    public Map<Id, List<OpportunityLineItem>> createMapOfOpportunities(Set<Id> oppIds){
        String oliFields = QuoteSyncUtil.getOppLineFieldsString();
            
        String oliQuery = 'SELECT Id, OpportunityId, PricebookEntryId, UnitPrice, Quantity, Discount, ServiceDate, SortOrder' + oliFields + ' FROM OpportunityLineItem WHERE OpportunityId IN :oppIds';   
        
        Map<Id, List<OpportunityLineItem>> oppToOliMap = new Map<Id, List<OpportunityLineItem>>();
            
        for (OpportunityLineItem oli : Database.query(oliQuery)) {
            List<OpportunityLineItem> oliList = oppToOliMap.get(oli.OpportunityId);
            if (oliList == null) {
                oliList = new List<OpportunityLineItem>();
            } 
            oliList.add(oli);  
            oppToOliMap.put(oli.OpportunityId, oliList);       
        } 
        return oppToOliMap;
    }
    
    /*
     *  @purpose : syncing opportunityLineItem to QuoteLineItem and vice versa
     *  @param : set of opportunity ids, map of quotes, boolean indicating if trigger is update, trigger.oldMap, boolean indicating if trigger is insert, trigger.new list
     *  @returntype : void
     */
    public void syncOliToQli(Set<Id> oppIds, Map<Id, Quote> quotes, Boolean isUpdate, Map<Id, QuoteLineItem> triggerOldMap, Boolean isInsert, List<QuoteLineItem> newTriggerList){
        system.debug('triggerOldMap : '+triggerOldMap);
        Map<Id, List<OpportunityLineItem>> oppToOliMap = createMapOfOpportunities(oppIds);
        Set<String> quoteLineFields = QuoteSyncUtil.getQuoteLineFields();
        Set<OpportunityLineItem> updateOlis = new Set<OpportunityLineItem>();
        Set<QuoteLineItem> updateQlis = new Set<QuoteLineItem>();
              
        for (Quote quote : quotes.values()) {
            System.debug('syncOliToQli quote Id::'+quote.Id);
            List<OpportunityLineItem> opplines = oppToOliMap.get(quote.OpportunityId);
            
            // for quote line insert, there will not be corresponding opp line
            if (opplines == null) continue;        

            Set<OpportunityLineItem> matchedOlis = new Set<OpportunityLineItem>();
        
            for (QuoteLineItem qli : quote.quoteLineItems) {
                boolean updateOli = false;
                QuoteLineItem oldQli = null;
                System.debug('syncOliToQli quote Id from QuoteLine Items::'+quote.Id);    
                if (isUpdate) {
                    
                    oldQli = triggerOldMap.get(qli.Id);
                    if(oldQli == null)
                    continue;
                    System.debug('qli.Id : '+qli.Id+' oldQli : '+oldQli+ ' qli : '+qli);
                
                    if (qli.UnitPrice == oldQli.UnitPrice
                        && qli.Quantity == oldQli.Quantity
                        && qli.Discount == oldQli.Discount
                        && qli.ServiceDate == oldQli.ServiceDate
                        && qli.SortOrder == oldQli.SortOrder 
                       )
                        updateOli = true;                       
                }

                boolean hasChange = false;
                boolean match = false;
                  
                for (OpportunityLineItem oli : opplines) {          
                    if (oli.pricebookentryid == qli.pricebookentryId  
                        && oli.UnitPrice == qli.UnitPrice
                        && oli.Quantity == qli.Quantity
                        && oli.Discount == qli.Discount
                        && oli.ServiceDate == qli.ServiceDate
                        && oli.SortOrder == qli.SortOrder
                       ){
                            
                        if (updateOlis.contains(oli) || matchedOlis.contains(oli)) continue;  
                            
                        matchedOlis.add(oli);                       
 
                        for (String qliField : quoteLineFields) {
                            String oliField = QuoteSyncUtil.getQuoteLineFieldMapTo(qliField);
                            Object oliValue = oli.get(oliField);
                            Object qliValue = qli.get(qliField);
                            if (oliValue != qliValue){                                                                                                 
                                if (isInsert && qliValue == null) {
                                    qli.put(qliField, oliValue);
                                    hasChange = true;    
                                } 
                                else if (isUpdate && !updateOli ) { /*&& oldQli != null*/
                                    //Object oldQliValue = oldQli.get(qliField);
                                    //if (qliValue == oldQliValue) {
                                    if (oliValue == null) 
                                        qli.put(qliField, null);
                                    else 
                                        qli.put(qliField, oliValue);
                                    hasChange = true;
                                    //}     
                                     
                                } 
                                else if (isUpdate && updateOli) {
                                    if (qliValue == null) 
                                        oli.put(oliField, null);
                                    else 
                                        oli.put(oliField, qliValue);
                                    hasChange = true;  
                                }
                            }    
                        }
                        if (hasChange) {
                            if (isInsert || (isUpdate && !updateOli)) { 
                                updateQlis.add(qli);
                            } 
                            else if (isUpdate && updateOli) {                               
                                updateOlis.add(oli);
                            }                    
                        } 
                        updateQlis.add(qli);
                        match = true;      
                        break;                          
                    } 
                }
                // NOTE: this cause error when there is workflow field update that fired during record create
                //if (trigger.isUpdate && updateOli) System.assert(match, 'No matching oppline');     
            }
        }
         
        TriggerStopper.stopOpp = true;
        TriggerStopper.stopQuote = true;             
        TriggerStopper.stopOppLine = true;
        TriggerStopper.stopQuoteLine = true;    
                        
        if (!updateOlis.isEmpty()) { 
            List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
            oliList.addAll(updateOlis); 
            System.debug('update olilist : '+olilist);          
            try{
                Database.update(olilist);     
            }
            catch(Exception e){
                System.debug(e.getMessage());
            }            
        }
            
        if (!updateQlis.isEmpty()) {
            List<QuoteLineItem> qliList = new List<QuoteLineItem>();   
            qliList.addAll(updateQlis);
            /*   To handle validation on ICEF_Event__c */
            for(QuoteLineItem q : qliList ){                              
                if(q.ICEF_Event__c == null){                    
                    for(Integer i = 0; i< newTriggerList.size();i++){
                        if(newTriggerList[i].id == q.id){
                            system.debug('inside if');                             
                            newTriggerList[i].adderror('Please enter the relevant workshop or choose "Non-Workshop Product"');
                        }
                    }
                }               
            }      
            System.debug('update qliList : '+qliList);   
            try{
                Database.update(qliList);     
            }
            catch(Exception e){
                System.debug(e.getMessage());
            }
        }                               
        TriggerStopper.stopOpp = false;
        TriggerStopper.stopQuote = false;                
        TriggerStopper.stopOppLine = false;          
        TriggerStopper.stopQuoteLine = false;

        
    }  
}