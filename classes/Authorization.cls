public class Authorization {
  
    public static String className (){
        return Authorization.class.getName();
    }
    
    public static void testError (){
        String subject =  className();
        String body = 'this is the body of the email.';
        EmailUtil.sendErrorEmail(subject, body);
    }
    
    //find or create API Container with name "MyIcef Container"
    //if not found: authorize and create new
    //return API container
    private static API_Container__c apiContainer(){
        API_Container__c apiContainer = new API_Container__c();
        List<API_Container__c> apiContainers = [SELECT Id, access_token__c, refresh_token__c, LastModifiedDate FROM API_Container__c WHERE Name = 'MyIcef Container'];
        if (apiContainers.size() == 0){
            //authorize and create Container
            String[] tokens = authorize();
            apiContainer.Name = 'MyIcef Container';
            apiContainer.access_token__c = tokens[0];
            apiContainer.refresh_token__c = tokens[1];
            insert apiContainer;
            EmailUtil.sendErrorEmail('Authorization.apxc: apiContainer()','new API container created with name: '+ apiContainer.name +' WHY?');
        } else if (apiContainers.size() > 1){
            String subject = 'error in Authorization.apxc - apiContainer()';
            String body = 'Number of MyIcef API containers: ' + apiContainers.size() + '! This should not have happened.';
            EmailUtil.sendErrorEmail(subject, body);
            //System.debug('Number of MyIcef API containers: ' + apiContainers.size() + '!!! Should not have happened, check code!');
        } else {
            apiContainer = apiContainers[0];
        }
        return apiContainer;
    }
    
    
    //update apiContainer with new access and refresh token
    //@future -- geht nicht.
    private static void updateContainer(String[] tokens){
        System.debug('updating Container');
        API_Container__c container = [SELECT Id, access_token__c, refresh_token__c, LastModifiedDate FROM API_Container__c WHERE Name = 'MyIcef Container'];
        container.access_token__c = tokens[0];
        container.refresh_token__c = tokens[1];
        update container;
    }
    
    //get access_token from API Container or re-authorize and update container if expired
    //return access_token
    public static String getAccessToken(){

        API_Container__c container = apiContainer();
        String accessToken = '';
        DateTime timestamp = DateTime.now(); 
        DateTime accessExpireDate = timestamp.addMinutes(5);
        DateTime refreshExpireDate = timestamp.addMinutes(30);
        if(container.LastModifiedDate != null){
        	accessExpireDate = container.LastModifiedDate.addMinutes(5);
        	refreshExpireDate = container.LastModifiedDate.addMinutes(30);          
        }
        System.debug('timestamp: '+ timestamp);
        System.debug('accessExpireDate: '+ accessExpireDate);
        System.debug('refreshExpireDate: ' + refreshExpireDate);
        //if container younger than 5 minutes: access token from container
        if(timestamp < accessExpireDate){
            System.debug('took access token from container: ' + container.Id);
            accessToken = container.access_token__c;
        }
        //else if container younger than 30 minutes: create new access token using refresh token from container
        else if (timestamp < refreshExpireDate && timestamp >= accessExpireDate){
            System.debug('accessToken expired. Create new using refresh token.');
 			System.debug('timestamp: '+timestamp+' accessExpireDate: ' +accessExpireDate);
            accessToken = refreshAccess(container.refresh_token__c);
        }
        //else authorize, update container(@future) and return new access token 
        else {
            System.debug('refresh token expired. Authorizing. ');
            System.debug('timestamp: '+timestamp + ' refreshExpireDate: ' + refreshExpireDate);
            List<String> tokens = authorize();
            accessToken = tokens[0];
            updateContainer(tokens);
        }
        System.debug('Container: '+container.Id + ' last Modified: ' + container.lastModifiedDate);
        return accessToken;
    }
    
    //get refresh_token from API Container
    //return refresh_token
    
    //get new access_token from API with refresh token and update API Container
    //parameters: refresh_token
    //return: access_token
    private static String refreshAccess(String refreshToken){
        System.debug('Creating new access token using refresh token.');
        String accessToken = '';
        String refresh = '';
        List<String> tokens = new List<String>();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String endpoint = 'https://login.icef.com:8443/auth/realms/ICEF/protocol/openid-connect/token';
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String bodyString = 'grant_type=refresh_token&refresh_token='+refreshToken+'&client_id=salesforce-cli&client_secret=8d84c456-baac-49a9-9edc-8f6ac5384d0e';
        request.setBody(bodyString);
        
        HttpResponse response = http.send(request);
        if(response.getStatusCode() == 200){
            Map<String,Object> results = (Map<String,Object>) JSON.deserializeUntyped(response.getBody());
            accessToken = results.get('access_token').toString();
            tokens.add(accessToken);
            tokens.add(refresh);
            //update container with new token values
            updateContainer(tokens);
        }
        //return access token
        return accessToken;
    }
    
    //create new access and refresh token from API. 
    //return List [access,refresh]
    private static String[] authorize(){
        String[] tokens = new List<String>();
        String access = '';
        String refresh = '';
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        //String endpoint = 'callout:MyIcefUserSync';
        String endpoint = 'https://login.icef.com:8443/auth/realms/ICEF/protocol/openid-connect/token';
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String bodyString = 'grant_type=password&client_id=salesforce-cli&client_secret=8d84c456-baac-49a9-9edc-8f6ac5384d0e&username=icefadmin&password=k4rn3v41';
        //System.debug('bodyString: '+bodyString);
        request.setBody(bodyString);
        
        HttpResponse response = http.send(request);
        System.debug('response: '+ response.getBody());
        System.debug('Status Code: '+ response.getStatusCode()+' '+response.getStatus());
        If(response.getStatusCode() == 200){
            Map<String,Object> results = (Map<String,Object>) JSON.deserializeUntyped(response.getBody());
            access = results.get('access_token').toSTring();
            refresh = results.get('refresh_token').toString();
            tokens.add(access);
            tokens.add(refresh);
            System.debug('access in authorize(): ' +access);
            System.debug('refresh in authorize(): ' + refresh);
        }
        return tokens;
    }
     
}