/*
@ Name : ICEFEventChangedTest (Test class for 'ICEFEventChanged' class handler)
@ Description : 1) When ICEF Event of AP is changed and their exist PP related to AP 
@                  through Account and having same ICEF Event as old ICEF Event of AP. 
@                  Then send email alert to this ICEF Event manager.
@               2) If ICEF Event of AP is changed, then update the ICEF Event of its related CP.
@ Notation : AP - Account Particiaption
@            CP - Contact Particiaption
@            PP - Product Particiaption
@ Developer : Shaikh Saquib
@ Date : 09-Dec-2014
*/

@isTest
public class ICEFEventChangedTest {

    static testMethod void test_Method() {
        
        Test.startTest();
        
        List<ICEF_Event__c> listICEFEvents = new List<ICEF_Event__c>();
        
        for(integer count =0; count < 6; count++ ) {
            
            listICEFEvents.add(new ICEF_Event__c(Name = 'Test ICEF Event ' + (count + 1),
                                                Event_City__c = 'Test Event City ' + (count + 1),
                                                Event_Manager_Educators__c = UserInfo.getUserId(),
                                                Event_Manager_Agents__c = UserInfo.getUserId(),
                                                catalogue_format__c = 'A4',
                                                catalogue_spelling__c = 'British English',
                                                technical_event_name__c = 'Test Technical Event ' + (count + 1)));
        }
        
        insert listICEFEvents;
        
        Country__c recordCountry = new Country__c();
        recordCountry.Name = 'India';
        insert recordCountry;
        
        Account recordAccount = new Account();
        recordAccount.Name = 'Test Account';
        recordAccount.Educational_Sector__c = '0 u-n-k-n-o-w-n';
        recordAccount.Status__c = 'Active';
        recordAccount.Mailing_Country__c = recordCountry.Id;
        recordAccount.ARM__c = Userinfo.getUserId();
        insert recordAccount;   
        
        List<Product_Participation__c> listProductParticipation = new List<Product_Participation__c>();
        for(integer count = 0; count < 3; count++ ) {
            
            listProductParticipation.add(new Product_Participation__c(ICEF_Event__c = listICEFEvents[count].Id,
                                                                      Account__c = recordAccount.Id));
        }
        
        insert listProductParticipation;

        List<Account_Participation__c> listAccountParticipation = new List<Account_Participation__c>();
        for(integer count = 0; count < 3; count++ ) {
        
            listAccountParticipation.add(new Account_Participation__c( Account__c = recordAccount.Id,
                                                                       ICEF_Event__c = listICEFEvents[count].Id,
                                                                       Participation_Status__c = 'pending payment',
                                                                       Country_Section__c = recordCountry.Id,
                                                                       Catalogue_Country__c = recordCountry.Id));
                                                                      
        }
        
        insert listAccountParticipation; 
        
        Contact recordContact = new Contact(Salutation = 'Mr.',
                                            LastName = 'Test Contact',
                                            Gender__c = 'Male',
                                            AccountId = recordAccount.Id,
                                            Role__c = 'Assistant',
                                            Contact_Status__c = 'Active');        
        insert recordContact;
        
        List<Contact_Participation__c> listContactParticipation = new List<Contact_Participation__c>();
        for(integer count =0; count < 3; count++) {
            
            listContactParticipation.add(new Contact_Participation__c(Contact__c = recordContact.Id,
                                                                      ICEF_Event__c = listICEFEvents[0].Id,
                                                                      Participation_Status__c = 'registered',
                                                                      Catalogue_Position__c = 1,
                                                                      Account_Participation__c = listAccountParticipation[0].Id));
        }
        
        for(integer count =0; count < 2; count++) {
            
            listAccountParticipation[count].ICEF_Event__c = listICEFEvents[count + 3].Id;
        }
        
        for(integer count =65; count < 3; count++) {
            
            listAccountParticipation[count].Participation_Status__c = 'transferred';
        }
        
        System.debug('CheckRecursive.run ::: ' + CheckRecursive.run);
        if(CheckRecursive.run == false)
        {
            CheckRecursive.run = true;
        }
        
        update listAccountParticipation;
        
        Test.stopTest();           
    }
}