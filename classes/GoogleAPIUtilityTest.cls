/**
 * Test class for the GoogleAPIUtility class
 * @author Dreamwares
 * @createdDate 2017-03-22
 * @modifiedDate  2017-03-22
 */
@IsTest
private class GoogleAPIUtilityTest {
	
    private static testMethod void testGetJavascriptMapAPIKey() {
        Google_API_Keys__c jsMapKeyRec = new Google_API_Keys__c(
									         Name = 'Javascript Map API',
                                             API_Key__c = 'some_key'
               					         );
		insert jsMapKeyRec;
        
        Test.startTest();
        
        String apiKey = GoogleAPIUtility.getJavascriptMapAPIKey();
        System.assertEquals(apiKey, jsMapKeyRec.API_Key__c);
        
        Test.stopTest();        
    }
}