/*  Test Class
@purpose-This test class is developed to test the scheduleSetCurrentCompany class
@created_date-27-04-15
@last_modified-[27-04-15]    
*/

@isTest
public class testScheduleSetCurrentCompany {
    
    @testSetup static void methodName() {
 		try {            
            Current_Company_For_Organization__c testRecord = new Current_Company_For_Organization__c();
            testRecord.Name = 'TestRecord';
            testRecord.Company_Name__c = 'TestCompany';
            insert testRecord;
        }catch(Exception e) {
            System.debug('Exception caught:::'+e.getMessage());
        }
	}
    
    /*  Function
    @purpose-This function is used to test the scheduled job with all the required values        
    @last_modified-[27-04-15]
    */    
    public static testMethod void positiveTest(){
       
       
        Test.StartTest();
        try{
            scheduleSetCurrentCompany shcSetCompany = new scheduleSetCurrentCompany();
            String sch = '0 0 23 * * ?';
            string jobId = system.schedule('Test set company schedule', sch, shcSetCompany);
            CronTrigger ct = [SELECT id, CronExpression, TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals('0 0 23 * * ?',ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            
        }catch(Exception e) {
            System.debug('Exception caught:::'+e.getMessage());
        }
        
        Test.stopTest();
    }
}