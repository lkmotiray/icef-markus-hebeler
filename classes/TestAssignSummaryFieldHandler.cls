/*
@ Created By : Yogesh Mahajan.
@ Date : 11-06-2014.
*/

@isTest
Private class TestAssignSummaryFieldHandler{
    static testMethod void runSummaryFieldTestCases() {
        // Create new Country 
        Country__c newCountry = new Country__c();
        newCountry.Name = 'Test-country';
        insert newCountry;
        
        // Create new Account
        Account newAcc = new Account();
        newAcc.Name = 'Test-account';
        //newAcc.CurrencyIsoCode = 'EUR-Euro';
        newAcc.Educational_Sector__c = '1 LANGUAGE COURSES';
        newAcc.Status__c = 'Active';
        newAcc.Mailing_Country__c = newCountry.id;
        insert newAcc;
        
        // Create new course
        List<Course_Profile__c> listNewCourse = new List<Course_Profile__c>();
        Course_Profile__c newCourse = new Course_Profile__c();
        newCourse.Account__c = newAcc.id;
        newCourse.Main_sector__c = '1 LANGUAGE COURSES';  
        // All career multipicklist
        newCourse.Career_Vocational_Certificate_Diploma__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Career_Voc_Cert_Diploma_Blended__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Career_Voc_Cert_Diploma_Online__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        // All Undergraduate multipicklist
        newCourse.Undergraduate_Deg_Bachelor_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Undergraduate_Deg_Bachelor_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Undergraduate_Degree_Bachelor__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        // All Graduate Diploma multipicklist
        newCourse.Grad_Postgrad_Cert_Diploma_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Grad_Postgrad_Cert_Diploma_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_PostgraduateCertificate_Diploma__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        // All Graduate Masters multipicklist
        newCourse.Graduate_Postgrad_Masters_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Graduate_Postgrad_Masters_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_Postgraduate_Masters__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        // All Graduate Doctorate multipicklist
        newCourse.Graduate_Postgrad_Doctorate_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Graduate_Postgrad_Doctorate_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_Postgraduate_Doctorate__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        // All Other Career Diploma
        newCourse.Other_Career_Voc_Cert_Dipl_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Career_Voc_Cert_Dipl_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Career_VocationalCertificate_Dipl__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        // All Other Doctorate
        newCourse.Other_Grad_Postgrad_Doc_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_Postgrad_Doc_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_Postgraduate_Doctorate__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        // All Other Master
        newCourse.Other_Grad_Postgrad_Masters_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_Postgrad_Masters_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_Postgraduate_Masters__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        // All Other Grad / Postgrad  Diploma
        newCourse.Other_Grad_PostgradCert_Dipl_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_PostgradCert_Dipl_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_PostgradCertificate_Dipl__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        // All Other Undergrad Deg
        newCourse.Other_Undergrad_Deg_Bach_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Undergrad_Deg_Bach_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Undergraduate_Degree_Bachelor__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        listNewCourse.add(newCourse);
        
        // Creare new course
        newCourse = new Course_Profile__c();
        newCourse.Account__c = newAcc.id;
        newCourse.Main_sector__c = '1 LANGUAGE COURSES';   
        newCourse.Career_Vocational_Certificate_Diploma__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Career_Voc_Cert_Diploma_Blended__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Career_Voc_Cert_Diploma_Online__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Undergraduate_Deg_Bachelor_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Undergraduate_Deg_Bachelor_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Undergraduate_Degree_Bachelor__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Grad_Postgrad_Cert_Diploma_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Grad_Postgrad_Cert_Diploma_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_PostgraduateCertificate_Diploma__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Graduate_Postgrad_Masters_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Graduate_Postgrad_Masters_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_Postgraduate_Masters__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Graduate_Postgrad_Doctorate_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Graduate_Postgrad_Doctorate_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_Postgraduate_Doctorate__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Career_Voc_Cert_Dipl_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Career_Voc_Cert_Dipl_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Career_VocationalCertificate_Dipl__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Grad_Postgrad_Doc_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_Postgrad_Doc_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_Postgraduate_Doctorate__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Grad_Postgrad_Masters_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_Postgrad_Masters_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_Postgraduate_Masters__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Grad_PostgradCert_Dipl_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_PostgradCert_Dipl_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_PostgradCertificate_Dipl__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Undergrad_Deg_Bach_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Undergrad_Deg_Bach_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Undergraduate_Degree_Bachelor__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        listNewCourse.add(newCourse);
        
        // Creare new course
        newCourse = new Course_Profile__c();
        newCourse.Account__c = newAcc.id;
        newCourse.Main_sector__c = '1 LANGUAGE COURSES';   
        newCourse.Career_Vocational_Certificate_Diploma__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Career_Voc_Cert_Diploma_Blended__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Career_Voc_Cert_Diploma_Online__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Undergraduate_Deg_Bachelor_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Undergraduate_Deg_Bachelor_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Undergraduate_Degree_Bachelor__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Grad_Postgrad_Cert_Diploma_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Grad_Postgrad_Cert_Diploma_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_PostgraduateCertificate_Diploma__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Graduate_Postgrad_Masters_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Graduate_Postgrad_Masters_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_Postgraduate_Masters__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Graduate_Postgrad_Doctorate_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Graduate_Postgrad_Doctorate_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_Postgraduate_Doctorate__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Career_Voc_Cert_Dipl_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Career_Voc_Cert_Dipl_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Career_VocationalCertificate_Dipl__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Grad_Postgrad_Doc_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_Postgrad_Doc_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_Postgraduate_Doctorate__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Grad_Postgrad_Masters_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_Postgrad_Masters_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_Postgraduate_Masters__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Grad_PostgradCert_Dipl_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_PostgradCert_Dipl_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_PostgradCertificate_Dipl__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Undergrad_Deg_Bach_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Undergrad_Deg_Bach_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Undergraduate_Degree_Bachelor__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        listNewCourse.add(newCourse);
        
        // Creare new course
        newCourse = new Course_Profile__c();
        newCourse.Account__c = newAcc.id;
        newCourse.Main_sector__c = '1 LANGUAGE COURSES';   
        newCourse.Career_Vocational_Certificate_Diploma__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Career_Voc_Cert_Diploma_Blended__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Career_Voc_Cert_Diploma_Online__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Undergraduate_Deg_Bachelor_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Undergraduate_Deg_Bachelor_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Undergraduate_Degree_Bachelor__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Grad_Postgrad_Cert_Diploma_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Grad_Postgrad_Cert_Diploma_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_PostgraduateCertificate_Diploma__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Graduate_Postgrad_Masters_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Graduate_Postgrad_Masters_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_Postgraduate_Masters__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Graduate_Postgrad_Doctorate_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Graduate_Postgrad_Doctorate_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Graduate_Postgraduate_Doctorate__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Career_Voc_Cert_Dipl_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Career_Voc_Cert_Dipl_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Career_VocationalCertificate_Dipl__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Grad_Postgrad_Doc_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_Postgrad_Doc_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_Postgraduate_Doctorate__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Grad_Postgrad_Masters_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_Postgrad_Masters_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_Postgraduate_Masters__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Grad_PostgradCert_Dipl_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Grad_PostgradCert_Dipl_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Graduate_PostgradCertificate_Dipl__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        
        newCourse.Other_Undergrad_Deg_Bach_Blended__c = 'Accounting/Banking/Economics/Finance';
        newCourse.Other_Undergrad_Deg_Bach_Online__c = 'Agricult./Aquacult./Forestry/Horticult.';
        newCourse.Other_Undergraduate_Degree_Bachelor__c = 'Arts/Creative/Fine/Perform/Music/Visual';
        listNewCourse.add(newCourse);
        
        insert listNewCourse;
        List<ID> listId = new List<ID>();
        for ( Course_Profile__c course : listNewCourse ){
            listId.add(course.id);
        }
        List<Course_Profile__c> listOfUpdatedCourse = [SELECT Career_Voc_Cert_Diploma_ALL__c, Undergraduate_Deg_Bachelor_ALL__c, Other_Undergrad_Deg_Bach_ALL__c  
                                                       From Course_Profile__c
                                                       Where id IN : listId];
        for( Course_Profile__c course : listOfUpdatedCourse){
            System.assertEquals('Accounting/Banking/Economics/Finance;Agricult./Aquacult./Forestry/Horticult.;Arts/Creative/Fine/Perform/Music/Visual',course.Career_Voc_Cert_Diploma_ALL__c );            
            System.assertEquals('Accounting/Banking/Economics/Finance;Agricult./Aquacult./Forestry/Horticult.;Arts/Creative/Fine/Perform/Music/Visual',course.Undergraduate_Deg_Bachelor_ALL__c );   
            //System.assertEquals('Agricult./Aquacult./Forestry/Horticult.;Arts/Creative/Fine/Perform/Music/Visual;Accounting/Banking/Economics/Finance',course.Other_Undergrad_Deg_Bach_ALL__c );                     
        }
    }
}