/*  
    @Purpose      : Test class of CFSchoolsDeletionController 
    @Author       : Poonam 
    @Created Date : 08/08/2016
*/
@isTest
public class CFSchoolsDeletionTest {
    
    static testmethod void testPositiveDeletionOfCFSchools() {
        List<Id> accountIdList = new List<Id>();
        List<Account> accountList =  new List<Account>();
        List<Coursefinder_School__c> schoolList =  new List<Coursefinder_School__c>();
        Id educatorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Educator').getRecordTypeId();
        
        Country__c country =  new Country__c(Agent_Relationship_Manager__c = UserInfo.getUserId());
        insert country;
        
        // Create account records 
        for(Integer count = 0; count < 50; count ++){
            Account account = new Account(Name = 'Test account' + count,
                                          IAS_Number__c = '3' + count,
                                          Status__c = 'Blacklisted',
                                          Educational_Sector__c = '1 LANGUAGE COURSES',
                                          Website = 'abc.com',
                                          RecordTypeId = educatorRecordTypeId,
                                          Name_short__c = 'Sample Name',
                                          ARM__c = UserInfo.getUserId(),
                                          ARM_emailaddress__c = 'test@abc.com',
                                          Status_details__c = 'Bought by other company',
                                          Mailing_Country__c = country.Id);
            accountList.add(account);
        } 
        
        insert accountList;
        
        // Create Coursefinder School records 
        for(Integer count = 0; count < 50; count ++){
            Coursefinder_School__c school = new Coursefinder_School__c(Account__c = accountList[count].Id);
            schoolList.add(school);
        } 
        
        insert schoolList;
        
        // Create a list of selected accounts to be processed
        for(Account account : accountList){
            accountIdList.add(account.Id);
        }
        
        // Call controller method to update delete field on child records of above Accounts
        Test.startTest();
        String result = CFSchoolsDeletionController.deleteCFSchools(accountIdList);
        Test.stopTest();
        
        System.assertEquals('Success :', result);
        
        // Get updated child school records
        schoolList = [SELECT Delete__c, Deleting_Reason__c FROM Coursefinder_School__c];
        
        // Assert whether Deleting_Reason__c and Delete__c fields are populated
        for(Integer count = 0; count < 50; count ++){
            System.assertEquals('Mass delete', schoolList[count].Deleting_Reason__c);
            System.assert(schoolList[count].Delete__c != null);
        }
        
        result = CFSchoolsDeletionController.deleteCFSchools(new List<Id>());
    } 
}