/**
 * Contains utility functions related to the "Google API".
 * @author Dreamwares
 * @createdDate 2017-03-22
 * @modifiedDate  2017-03-22
 */
public virtual class GoogleAPIUtility {
	
    public static String getJavascriptMapAPIKey() {        
        return Google_API_Keys__c.getValues('Javascript Map API').API_Key__c;
    }
}