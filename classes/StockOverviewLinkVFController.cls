/*
 * @Purpose : Controller for StockOverviewLinkVF
 * @Created Date : 26-6-2018
 * @Auther : (Dreamwares)
 */
public class StockOverviewLinkVFController{

    /*
     * @Purpose : redirect to another VF Page
     * @Parem : -
     * @Return : PageReference
     */
    public PageReference redirect(){
    
        Id eventId = ApexPages.currentPage().getParameters().get('id');
        if(String.isBlank(eventId)){
            System.debug('Please provide valid Id'); 
            return null;   
        }else{
            PageReference pageReference = new PageReference('/apex/StockOverview?id='+eventId);
            pageReference.setRedirect(true);
            return pageReference;
        }
    }
}