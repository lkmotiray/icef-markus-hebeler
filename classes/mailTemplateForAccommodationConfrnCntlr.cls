/*
    Purpose : Controller to mailTemplateForAccommodationConfirmation page
              (Directly cloned and modified from exiting component "cpselecttemplate")
*/

public with sharing class mailTemplateForAccommodationConfrnCntlr
{
    //public String selectedOption = null; 
    public String sendEmailUrl;
    List<Folder> f = [select Name from Folder where Type =: 'Email' Order by Name];
    Public List<EmailTemplate> et = new List<EmailTemplate>();
    public List<eturl> allurl {get; private set;} 
    List<eturl> selectedeturlId = new List<eturl>();
    List<eturl> all = new List<eturl>();
    List<EmailTemplate> allemailtemplates = new List<EmailTemplate>();
    public string cpId = ApexPages.CurrentPage().getParameters().get('cpid');
    public string fromEmail = ApexPages.CurrentPage().getParameters().get('mail');
    public string testCpId {get; set;}
    public string selectedTemplateFolder;
      
    public mailTemplateForAccommodationConfrnCntlr()
    {
        allurl = new List<eturl>();
    }
    public List<SelectOption> getItems()
    {
        this.allurl = new List<eturl>();
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('Unfiled Public Email Templates','Unfiled Public Email Templates'));
        //options.add(new SelectOption('My Personal Email Templates', 'My Personal Email Templates'));
        options.add(new SelectOption('--None--', '--None--'));
        for(integer i =0; i<f.size(); i++)
        {
            options.add(new SelectOption(f[i].Name,f[i].Name));
        }
        return options;
    }
    
    public PageReference showSelectedFoldertemplates()
    {
        //added for test method
        if(cpId == null){
            cpId = testCpId;
        }
        System.debug('selectedOption:::'+selectedOption);
        //storeRecentlyUsedEmailTemplate.assignRecentlyUsedEmailTemplate(selectedOption);   
        
        EmailTemplateSettings__c s = EmailTemplateSettings__c.getInstance();
        s.Recently_used_email_template__c = selectedOption;
        Database.UpsertResult result = Database.upsert(s);  
        
        System.debug('s.Recently_used_email_template__c::'+s.Recently_used_email_template__c);
        List<Folder> fldr = [Select id from Folder where Name =: selectedOption AND Type =: 'Email'];
        System.debug('fldr:'+fldr);
        et.clear();
        allemailtemplates.clear();
        all.clear();
        if(fldr.size() != 0)
            et = [Select Name, Description, TemplateType, IsActive from EmailTemplate where FolderId =: fldr[0].Id Order by Name];
        else
            et.clear();
        allemailtemplates.addAll(et);
        if(allemailtemplates != null)
        {
            for(EmailTemplate emailtemp : allemailtemplates)
            {
                all.add(new eturl(emailtemp));
            }
        }
        allurl.clear();
        for(Object et1 : all)
        {
            allurl.add((eturl)et1); 
        }
        return null;
    }
    
    public void settestCpId(String id){
        this.testCpId = id;
    }
    
    public String gettestCpId(){
        return testCpId;
    }    
    
    public String selectedOption
    {
       get
        {
           if(selectedOption==null)
           {
               string etemp = EmailTemplateSettings__c.getInstance().Recently_used_email_template__c;               
               selectedOption = etemp; //selectedTemplateFolder; 
                      
           }
           return selectedOption;
        }
    set;
    }
    
    Public List<EmailTemplate> getet()
    {
        return et;
    }
    
    public class eturl //wrapper class
    {
        public String emailtempUrl {get; set;}
        public string emailTemplateName {get;set;}
        public String emailTemplateDesc {get; set;}
        public string emailTemplateType {get;set;}
        public boolean emailTemplateActive {get;set;}
        public string cpId {get;set;}
        public string fromEmail {get;set;}
        //public string testCpId = '';       
               
        public EmailTemplate obj
        {
            get;        
            set;           
        }   
                
        public eturl (EmailTemplate obj)
        {
            this.obj = obj;
            string accommodationId = ApexPages.CurrentPage().getParameters().get('id');  
            cpId = ApexPages.CurrentPage().getParameters().get('cpid');           
            fromEmail = ApexPages.CurrentPage().getParameters().get('mail');   
            
            //added for test method
            if(cpId == null){
                //cpId = testCpId;
            }
            
            emailtempUrl = '/apex/sendAccommodationConfirmation?id='+accommodationId+'&etid='+this.obj.Id+'&cpid='+cpId+'&mail='+fromEmail;
            emailTemplateName = this.obj.Name;
            emailTemplateDesc = this.obj.Description;
            emailTemplateType = this.obj.TemplateType;    
            emailTemplateActive = this.obj.IsActive; 
        }
    }
    
    public Pagereference cancel()
    {    
          string accommodationId = ApexPages.CurrentPage().getParameters().get('id');  
          cpId = ApexPages.CurrentPage().getParameters().get('cpid');           
          fromEmail = ApexPages.CurrentPage().getParameters().get('mail');  
            
          Pagereference p = new PageReference('/apex/sendAccommodationConfirmation?id='+accommodationId+'&cpid='+cpId+'&mail='+fromEmail);
          p.setRedirect(true);     
          return p;
    }
    
    
    
}