/*
*	@Purpose : This class is used to store "Product" record details.
*	@Date    : 13-04-2018
*/
public class ProductWrapper {
	
	public String Id;
    public String Name; 
    public String Family;
    public Integer count;
}