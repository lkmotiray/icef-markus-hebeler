public class MagicLinkUpdateQ implements Queueable {
    
    private List<Contact> contacts;
    private Long timeStamp;
    private String randomValue, secret = 'ejesheitohn8eeV7yu7j', openid, combinedString, sha1Coded;
    
    public magicLinkUpdateQ(List<Contact> cons){
        this.contacts = cons;
        this.timeStamp = System.Now().getTime()/1000;
        this.randomValue = EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(0,10);
    }
    
    public void execute(QueueableContext context){
        System.debug('queued for contacts: ' + contacts);
        
        for (Contact cont : contacts){
 
            openid = cont.email;
            /*values.add(openid);
            values.add(String.valueof(timeStamp));
            values.add(secret);
            values.add(randomValue);*/
            
            combinedString = openid + timeStamp + secret + randomValue;
            
            sha1Coded = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', Blob.valueOf(combinedString)));
            
            cont.magic_link__c = '/login/magic?openid=' + cont.email +'&time_stamp='+ String.ValueOf(timeStamp) +'&salt='+ randomValue +'&hash='+ sha1Coded; 
            cont.Email_Maintenance_Comments__c = 'email address for magic link: ' + cont.email + ' timestamp: '+ timestamp;
                
        }
    }
}