/**
  * Purpose : ICEFGmBHSalesInvoiceEmailBody Controller
 */
public with sharing class ICEFGmBHSalesInvoiceEmailBodyController {
    public Integer paymentSchedulesSize {get;set;}
    public c2g__codaInvoice__c salesInvoiceInstance {get; set;}
    
    public installmentWrapper installmentInfo { get; set; }
    
    // constructor
    public ICEFGmBHSalesInvoiceEmailBodyController(){
        installmentInfo = new installmentWrapper ();
        installmentInfo.isOverDue = false;
        installmentInfo.isMulOverDue = false;
        installmentInfo.installmentAmt = 0;
        if(salesInvoiceInstance == Null){
            salesInvoiceInstance = new c2g__codaInvoice__c();
        }
    }
    
    //--Set size of payment schedules
    public void getSizeOfPaymentSchedules() {
        try {
            /*paymentSchedulesSize = [ SELECT COUNT() 
                                     FROM c2g__codaInvoiceInstallmentLineItem__c
                                     WHERE c2g__Invoice__c = :salesInvoiceInstance.id ];
            salesInvoiceInstance = [ SELECT Id, c2g__Opportunity__r.Invoicing_contact__r.Name,
                                            c2g__Opportunity__r.Invoicing_contact__r.FirstName,
                                            c2g__InvoiceTotal__c, c2g__OutstandingValue__c
                                     FROM c2g__codaInvoice__c
                                     WHERE Id = :salesInvoiceInstance.id LIMIT 1];*/
            paymentSchedulesSize = InvoiceEmailSend.mapInvoiceLines.get(salesInvoiceInstance.id).size();
            
            if( InvoiceEmailSend.mapInvoices != NULL && !InvoiceEmailSend.mapInvoices.isEmpty()){
                if(InvoiceEmailSend.mapInvoiceLines.containsKey(salesInvoiceInstance.Id)){
                	salesInvoiceInstance = InvoiceEmailSend.mapInvoices.get(salesInvoiceInstance.id);
                }
            }
            
            
        } catch(Exception genericException) {
            System.debug('Exception occurred : ' + genericException.getMessage());
        }
        computeAmountDue();
    }
    
    // method to compute 
    public void computeAmountDue(){
        Map<String, Decimal> lineItemToAmountDeuMap = new Map<String, Decimal>();
        Map<String, Decimal> lineItemToTillDateAmountMap = new Map<String, Decimal>();
        Map<String, Date> lineItemToDateMap = new Map<String, Date>();
        
        List<c2g__codaInvoiceInstallmentLineItem__c> lineItemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        
        Decimal amountDue = 0;
        Decimal tillDatePaid = 0;
        Decimal invoicePaid2Date = salesInvoiceInstance.c2g__InvoiceTotal__c - salesInvoiceInstance.c2g__OutstandingValue__c;
        Integer index = 0;
        
        try{
            lineItemList = [ SELECT Id, c2g__Amount__c, c2g__DueDate__c
                             FROM c2g__codaInvoiceInstallmentLineItem__c
                             WHERE c2g__Invoice__c = :salesInvoiceInstance.Id 
                             ORDER BY c2g__DueDate__c ];
        }catch(Exception e){
            System.debug('Exception Occured while fetching Installment Line Item'+ e);
        }
        
        for( c2g__codaInvoiceInstallmentLineItem__c item : lineItemList ){  
            
            if(item.c2g__Amount__c <= invoicePaid2Date) {
            
                tillDatePaid = item.c2g__Amount__c;
                invoicePaid2Date = invoicePaid2Date - tillDatePaid;                                
                amountDue = item.c2g__Amount__c - tillDatePaid;
                            
                lineItemToAmountDeuMap.put( item.Id, amountDue);  
                lineItemToDateMap.put(item.Id, item.c2g__DueDate__c);   
                lineItemToTillDateAmountMap.put( item.Id, tillDatePaid);
                
            }else if(item.c2g__Amount__c >= invoicePaid2Date && invoicePaid2Date != 0){
                
                tillDatePaid =  item.c2g__Amount__c - invoicePaid2Date;
                invoicePaid2Date = 0;                
                amountDue = item.c2g__Amount__c - tillDatePaid;
                            
                lineItemToAmountDeuMap.put( item.Id, tillDatePaid);    
                lineItemToTillDateAmountMap.put( item.Id, amountDue);
                lineItemToDateMap.put(item.Id, item.c2g__DueDate__c);
                
            }else{
                tillDatePaid = 0;
                invoicePaid2Date = 0;
                amountDue = item.c2g__Amount__c - tillDatePaid;
                lineItemToDateMap.put(item.Id, item.c2g__DueDate__c);            
                lineItemToAmountDeuMap.put( item.Id, amountDue);    
                lineItemToTillDateAmountMap.put( item.Id, tillDatePaid);
            }                      
        }
        
        Integer nxtIndex = 0;       
        for( String item : lineItemToAmountDeuMap.keySet()){
            index ++;
            
            if( nxtIndex > 0){
                
                if(index == lineItemToAmountDeuMap.size()){
                    installmentInfo.index = 'final';
                }else{
                    installmentInfo.index = NumberToWords.convert(String.valueOf(index));
                }
                
                installmentInfo.installmentAmt = lineItemToAmountDeuMap.get(item);
                installmentInfo.installmentDate = lineItemToDateMap.get(item);
                break;  
            }
            
            if( lineItemToAmountDeuMap.get(item) > 0 && lineItemToDateMap.get(item) < date.today() ){
                                
                installmentInfo.isOverDue = true;
                installmentInfo.isMulOverDue = false;
                installmentInfo.overDueAmt = lineItemToAmountDeuMap.get(item);
                installmentInfo.overDueDate = lineItemToDateMap.get(item);
                                
                nxtIndex ++;
                
            } else if( lineItemToAmountDeuMap.get(item) > 0 ){                              
                
                installmentInfo.isOverDue = false;
                installmentInfo.isMulOverDue = false;
                
                if(index == lineItemToAmountDeuMap.size()){
                    installmentInfo.index = 'final';
                }else{
                    installmentInfo.index = NumberToWords.convert(String.valueOf(index));
                }
                
                installmentInfo.installmentAmt = lineItemToAmountDeuMap.get(item);
                installmentInfo.installmentDate = lineItemToDateMap.get(item);
                break;  
            }                   
        }   
        
        Integer overDueCount = 0;
        Decimal totalOverDueAmt = 0;
        for( String item : lineItemToAmountDeuMap.keySet() ){
            
            if( lineItemToAmountDeuMap.get(item) > 0 && lineItemToDateMap.get(item) < date.today() ){
                overDueCount ++ ;
                totalOverDueAmt = totalOverDueAmt + lineItemToAmountDeuMap.get(item);
            }
        }
        
        if( overDueCount > 1 ){
            installmentInfo.isMulOverDue = true;
            installmentInfo.isOverDue = false;
            installmentInfo.mulOverDueAmt = totalOverDueAmt;
            installmentInfo.overDueCtr = overDueCount;
        }
    }   
    
    public Class installmentWrapper{ 
        
        public Boolean isOverDue { get; set; }
        public Boolean isMulOverDue { get; set; }
        public Decimal overDueAmt { get; set; }
        public Decimal mulOverDueAmt { get; set; }
        public Integer overDueCtr { get; set; }
        public Date overDueDate { get; set; }
        public Decimal installmentAmt { get; set; }
        public String index { get; set; }
        public Date installmentDate { get; set; }
    }
}