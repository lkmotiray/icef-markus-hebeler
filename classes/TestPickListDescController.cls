/*  Test Class
    @purpose-This test class is developed to test the PickListDescController class
    @created_date-27-02-14
    @last_modified-[27-02-14]    
*/

@isTest
public class TestPickListDescController{

    /*  Test Method
        @purpose-This test method tests the functionality of PickListDescController class
        @created_date-27-02-14
        @last_modified-[27-02-14]    
    */
    public static testMethod void test() {  
        PageReference pageRef = Page.PicklistDesc;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getparameters().put('sobjectType', 'Account_Participation__c');  
        ApexPages.CurrentPage().getparameters().put('recordTypeName', 'Agent');  
        ApexPages.CurrentPage().getparameters().put('picklistFieldName', 'Participation_Status__c');  
        PickListDescController controller = new PickListDescController();
    }
    
    public static testMethod void testwithId() {  
        Account testAccount = new Account(Name='TestAccount');
        insert testAccount;
        ICEF_Event__c testEvent = new ICEF_Event__c(Name='Japan - Korea',
                                               Event_City__c = 'testEventCity3',
                                               technical_event_name__c = 'testEventTechinal3', 
                                               Event_Full_Name__c='TestFullEvent3',                                                
                                               Start_date__c=System.today());
        insert testEvent;
        
        SObject testSobject = new Account_Participation__c(Account__c=testAccount.Id, ICEF_Event__c= testEvent.Id);
        insert testSobject;
        
        PageReference pageRef = Page.PicklistDesc;
        Test.setCurrentPage(pageRef);
        
        ApexPages.CurrentPage().getparameters().put('id', testSobject.id);  
        //ApexPages.CurrentPage().getparameters().put('sobjectType', 'Account_Participation__c');          
        ApexPages.CurrentPage().getparameters().put('picklistFieldName', 'Participation_Status__c'); 
        PickListDescController controller = new PickListDescController();
    }
    
}