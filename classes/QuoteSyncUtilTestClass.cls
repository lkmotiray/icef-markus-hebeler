@isTest(seeAllData = true) 
private class QuoteSyncUtilTestClass
{
    public static testmethod void testQuoteSyncUtil()
    {
        date dueDate = date.newInstance(2014, 1, 30);
        ICEF_Event__c icefEvent1 = new ICEF_Event__c(Name='Test IcefEvent1', Event_City__c = 'City', technical_event_name__c = 'TestingEvent1', Event_Manager_Educators__c = UserInfo.getUserId(), Event_Manager_Agents__c = UserInfo.getUserId(), Start_date__c=dueDate);
        insert icefEvent1;
        
        ICEF_Event__c icefEvent2 = new ICEF_Event__c(Name='Test IcefEvent2', Event_City__c = 'City', technical_event_name__c = 'TestingEvent2', Event_Manager_Educators__c = UserInfo.getUserId(), Event_Manager_Agents__c = UserInfo.getUserId(), Start_date__c=dueDate);
        insert icefEvent2; 
        
        Pricebook2 pb;
        boolean setNull = false;
        
        Pricebook2 standardPB = [select id, isActive from Pricebook2 where isStandard=true];
        standardPB.isActive = true;
        update standardPB;
        
        pb = new PriceBook2(Name = 'test pricebook', isActive = true);
        insert pb;
        
        QuoteSyncField__c quoteSyncRec = new QuoteSyncField__c(Name = 'Payment_Terms__c', OppSyncField__c = 'Payment_Term__c');
        insert quoteSyncRec;

        //QuoteSyncUtil qsu = new QuoteSyncUtil();
        //QuoteSyncUtil.getQuoteLineFields();
        //QuoteSyncUtil.getQuoteFields();
        //QuoteSyncUtil.getOppLineFields();
        //QuoteSyncUtil.getOppFields();
        QuoteSyncUtil.getQuoteFieldMapTo('ABC');
        //QuoteSyncUtil.addQuoteField('ABC','ABC');
        QuoteSyncUtil.getQuoteFieldsString();
        QuoteSyncUtil.getOppFieldsString();
        QuoteSyncUtil.getQuoteLineFieldsString();
        QuoteSyncUtil.getOppLineFieldsString();
        
        Id accrecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Educator').getRecordTypeId();
        Country__c india = new Country__c(Name = 'India', Agent_Relationship_Manager__c = UserInfo.getUserId());
        insert india;
        Account acc = new Account(name='test acc', recordTypeId = accrecordTypeId, Status__c = 'Active', Mailing_Country__c = india.id, ARM__c= UserInfo.getUserId(),
                                  ARM_emailaddress__c = 'abc@xyz.com', Mailing_State_Province__c = 'IN', CurrencyIsoCode = 'EUR');
        insert(acc);   
        
        Opportunity opp = new Opportunity(Name='test opp', Amount=100, TotalOpportunityQuantity=10, Pricebook2Id=standardPB.Id, StageName='Prospecting', CloseDate=System.today(),
                                          AccountId= acc.id);
        insert opp;
        
        for (String field : QuoteSyncUtil.getOppFields()) 
        {
            System.debug('setNull : '+setNull);
            if (setNull) opp.put(field, null);
            else 
            {
                 System.debug('in else ');
                 Schema.DescribeFieldResult result = QuoteSyncUtil.getField('Opportunity', field);
                 QuoteSyncUtil.createValue('Opportunity', field, result);
            }
        }
        
        Quote quoteRec = new Quote(Name='test quote', OpportunityId = opp.id);
        insert quoteRec;
        
        for (String field : QuoteSyncUtil.getQuoteFields()) 
        {
            if (setNull) quoteRec.put(field, null);
            else 
            {
                 Schema.DescribeFieldResult result = QuoteSyncUtil.getField('Quote', field);
                 QuoteSyncUtil.createValue('Quote', field, result);
            }
        }
                
        
        Product2 p = new Product2(Name='test product 1', IsActive=true);             
        insert(p);                            
        
        PricebookEntry currPbe = new PricebookEntry(Pricebook2Id=standardPB.Id, Product2Id=p.Id, IsActive=true, UnitPrice=10, UseStandardPrice=FALSE);
        insert currPbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id, UnitPrice=10*10, Quantity=10, Discount=5, ServiceDate=System.today(), PricebookEntryId=currPbe.Id, ICEF_Event__c = icefEvent1.Id);         
        insert oli;
        
        for (String field : QuoteSyncUtil.getOppLineFields()) 
        {
            if (setNull) oli.put(field, null);
            else 
            {
                 Schema.DescribeFieldResult result = QuoteSyncUtil.getField('OpportunityLineItem', field);
                 QuoteSyncUtil.createValue('OpportunityLineItem', field, result);
            }
        }
        QuoteSyncUtil.populateRequiredFields(oli);        
        QuoteSyncUtil.populateRequiredFields(opp);
        
        for (String quoteField : QuoteSyncUtil.getQuoteFields()) 
        {
           String oppField = QuoteSyncUtil.getQuoteFieldMapTo(quoteField);
        }
        
        for (String qliField : QuoteSyncUtil.getQuoteLineFields()) 
        {
            String oliField = QuoteSyncUtil.getQuoteLineFieldMapTo(qliField);
        }
        
        String field = QuoteSyncUtil.removeQuoteLineField('ICEF_Event__c');
        
        if (field != null)
           QuoteSyncUtil.addQuoteLineField('ICEF_Event__c', field); 
        
        String field1 = QuoteSyncUtil.removeQuoteField('Payment_Terms__c');
        
        if (field1 != null)
           QuoteSyncUtil.addQuoteField('Payment_Terms__c', field1);            
        
        
        QuoteSyncUtil.isRunningTest = true;
        Quote quote = new Quote(Name='test quote', OpportunityId=opp.Id, Pricebook2Id=opp.Pricebook2Id);
        insert quote;
        
        QuoteLineItem qli = new QuoteLineItem(QuoteId=quote.Id, Discount = 5, UnitPrice=10*10, Quantity=10, ServiceDate=System.today(), PricebookEntryId=currPbe.Id, ICEF_Event__c = icefEvent1.Id, Tax__c = '10%');  
        insert qli; 
		
		Decimal taxAmt = [SELECT tax FROM quote WHERE id = :quote.id LIMIT 1].tax;
		System.assertEquals(taxAmt, 95.00);
        
        opp.SyncedQuoteId = quote.Id;
        update opp;
        
        qli.ICEF_Event__c = icefEvent2.Id;
        update qli;
        
        for (String quoteField : QuoteSyncUtil.getQuoteLineFields()) 
        {
            if (setNull) oli.put(quoteField, null);
            else 
            {
                 Schema.DescribeFieldResult result = QuoteSyncUtil.getField('QuoteLineItem', quoteField);
                 QuoteSyncUtil.createValue('QuoteLineItem', quoteField, result);
            }
        }
		
        QuoteLineItem qli1 = [SELECT Id FROM QuoteLineItem WHERE id = :qli.id LIMIT 1];
        delete qli1;
        
        taxAmt = [SELECT tax FROM quote WHERE id = :quote.id LIMIT 1].tax;
		System.assertEquals(taxAmt, 0);
    }  
}