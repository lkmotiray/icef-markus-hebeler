public with sharing class CustomSubPageBlockController {

    public Object[] listOfCountries;
    public List<WrapperClassForTestSubPageBlock> wrapperObjectList {get; set;}
    
    public void setlistOfCountries (Object[] aList) {
        wrapperObjectList = new List<WrapperClassForTestSubPageBlock>();
        listOfCountries = aList;
        for(Object o : listOfCountries){
            string str =  String.valueOf(o);
            List<string> temp = String.valueOf(o).split('#');
            WrapperClassForTestSubPageBlock cv = new WrapperClassForTestSubPageBlock(temp[0], temp[1]);
            wrapperObjectList.add(cv);
        }
    }
    
    public Object getlistOfCountries() {
        return listOfCountries;
    }
    
    public CustomSubPageBlockController() {
        wrapperObjectList = new List<WrapperClassForTestSubPageBlock>();
    }
    
    
    public class WrapperClassForTestSubPageBlock {
        public String countryName {get; set;}
        public String value {get; set;}
        
        public WrapperClassForTestSubPageBlock(String country, String count) {
            countryName = country;
            value = count;
        }
    }
    
    @isTest
    public static void testController1(){
    
    Test.startTest();
    CustomSubPageBlockController tc = new CustomSubPageBlockController();
    String testString = 'Argentina#5';
    List<String> testList = new List<String>();
    testList.add(testString);
    tc.setlistOfCountries(testList);
    tc.getlistOfCountries();
    Test.stopTest();
    }
}