/*
    Purpose : Controller to ReallocateHotelRooms Page.
*/
Public class reallocateHotelRoomsController {
    
    @RemoteAction
    public static List<List<String>> createTable(String sObjectType,String recordId){
        
         if(validate(recordId) && validate(sObjectType)){
             if(sObjectType == 'Account_Participation__c'){
                 return createTableOfHotelRooms(recordId);
             }
             else if(sObjectType == 'Hotel_Room__c'){
                 return createTableOfAccountParticipation(recordId);
             }
         }
         return null;
    }
    
    @RemoteAction
    public static string reallocateHRtoAP(String recordId1,String recordId2){
    
        String accountParticipationId;
        String hotelRoomId;
        
        if('Account_Participation__c' == getObjectTypeOfId(recordId1) && 
           'Hotel_Room__c' == getObjectTypeOfId(recordId2)){
            accountParticipationId = recordId1;
            hotelRoomId = recordId2;
        }else if('Account_Participation__c' == getObjectTypeOfId(recordId2) && 
                 'Hotel_Room__c' == getObjectTypeOfId(recordId1)) {
            accountParticipationId = recordId2;
            hotelRoomId = recordId1;
        }
        else{
            return 'Sorry, Invalid Ids provided : Please contact to Administrator ';
        }
        
        // Ap Id For number of cp, to each cp one accommodation
        // List<Contact_Participation__c> contactParticipationList = getContactParticipation(accountParticipationId);
        // filter CP
        
        List<Account_Participation__c> accountParticipationList = new List<Account_Participation__c>();
        accountParticipationList.add(new Account_Participation__c(Id = accountParticipationId)); 
        Map<Id,Contact_Participation__c> contactParticipationMap = getUnAccommodatedCPMap(accountParticipationList);
            
        // create accommodations
        string accommodationCreationResult = createAccommodations(accountParticipationId,hotelRoomId,contactParticipationMap.values());
        if(accommodationCreationResult == 'success'){
           if('success' == updateHotelRoom(hotelRoomId))
           return 'success';
           return 'fail';
        }
        return accommodationCreationResult;
    }
    
    public static string updateHotelRoom(String hotelRoomRecordId){
        try{
            Hotel_Room__c hotelRoomRecord = new Hotel_Room__c( Id = hotelRoomRecordId ,
                                                               Room_has_been_re_allocated__c = system.Now() ); 
            update hotelRoomRecord;
            return 'success';
        }
        catch(Exception e){
            system.debug(e.getMessage());
            return 'Please check Accommodations. System failed to update re-allocation date of Hotel Room record';
        }
        
    }
    
    public static string createAccommodations(String accountParticipationId,
                                              String hotelRoomId,
                                              List<Contact_Participation__c> contactParticipationList){
                                              
        if(validate(accountParticipationId) && validate(hotelRoomId) && validate(contactParticipationList)) { 
            
            
                                            
            List<Accommodation__c> accommodationList = new List<Accommodation__c>(); 
            for(Contact_Participation__c  contactParticipationRecord : contactParticipationList) {  
                accommodationList.add( new Accommodation__c( Contact_Participation__c = contactParticipationRecord.Id, 
                                                             Account_Part__c = accountParticipationId,
                                                             Hotel_Room__c = hotelRoomId ));
            }
            try {
                insert accommodationList;
                return 'success';
            }
            catch(Exception e){
                system.debug(e.getMessage());
                return 'Sorry, Failed to create Accommodations';
            }
        }
        return 'Sorry, Unable to create Accommodations';
    }
    
    /*public static List<Contact_Participation__c> getContactParticipation(String accountPartId){
        if(validate(accountPartId)){
            try {
                return [SELECT Id
                        FROM Contact_Participation__c 
                        WHERE Account_Participation__c = :accountPartId
                            AND Participation_Status__c IN ('registered','participating','Guest','Speaker')];    
            }
            catch(Exception e){
                system.debug(e.getMessage());
            }
        }
        return null;
    }*/
    
   
    @RemoteAction
    public static string getObjectTypeOfId(Id sObjectId){
        return sObjectId.getSObjectType().getDescribe().getName();
    }
    public static List<List<String>> createTableOfAccountParticipation(String hotelRoomRecordId){
        // Hotel Room.capacity.ICEF
        if(validate(hotelRoomRecordId)){  
            Hotel_Room__c  hotelRoomRecord;
            try {
                hotelRoomRecord = [SELECT Hotel_Capacity__r.ICEF_Event__c  
                                   FROM Hotel_Room__c  
                                   WHERE Id = :hotelRoomRecordId];
            } 
            catch(Exception e){
                system.debug(e.getMessage());
            }
            system.debug('hotelRoomRecord ' + hotelRoomRecord);
            
        //getAPs 
        Map<Id,Account_Participation__c> accountParticipationMap = getAccountParticipationMap(hotelRoomRecord.Hotel_Capacity__r.ICEF_Event__c);         
        
        Map<Id,Contact_Participation__c> unAccommodatedCPMap = getUnAccommodatedCPMap(accountParticipationMap.values());
     
        
        List<Account_Participation__c> eligibleAccountParticipation = new List<Account_Participation__c>();
        //Map<String,Integer> apRequiredAccommodationCountMap = new Map<String,Integer>();
        set<Id> addedAPIdSet = new set<Id>();
        Id apRecordId;
        for(Contact_Participation__c contactParticipationRecord : unAccommodatedCPMap.values()){
            apRecordId = contactParticipationRecord.Account_Participation__c;
            /*
            if(!apRequiredAccommodationCountMap.containsKey(apRecordId)){
                apRequiredAccommodationCountMap.put(apRecordId,1); 
            }
            else{
                apRequiredAccommodationCountMap.put(apRecordId,
                                                   (apRequiredAccommodationCountMap.get(apRecordId)+1));
            } 
            */
            if(!addedAPIdSet.contains(apRecordId)){
                eligibleAccountParticipation.add(accountParticipationMap.get(apRecordId));
                addedAPIdSet.add(apRecordId);
            }
        }
        system.debug(' eligibleAccountParticipation '+eligibleAccountParticipation);
        
        // create dataset
        return createDatasetForAccountPartTable(eligibleAccountParticipation);
        }
        return null;
        
    }
    
    
    public static Map<Id,Contact_Participation__c> getUnAccommodatedCPMap(List<Account_Participation__c> accountParticipationList){
        // get accommodation booked with AP
        List<Accommodation__c> accommodationList = getAccommodationsRelatedToAP(accountParticipationList);
        // 
        Map<Id,Contact_Participation__c> contactPartMap = getContactPartRelatedToAP(accountParticipationList);
        // eliminate contact Participants
        return eliminateContactParticipants(accommodationList,contactPartMap);
        
       
    }
    
    public static Map<Id,Contact_Participation__c> eliminateContactParticipants(List<Accommodation__c> accommodationList,
                                                                                Map<Id,Contact_Participation__c> contactPartMap){
    
        for(Accommodation__c accommodationRecord : accommodationList){
            if(contactPartMap.containsKey(accommodationRecord.Contact_Participation__c))
                contactPartMap.remove(accommodationRecord.Contact_Participation__c);
        }
        return contactPartMap;
    }
    
    public static List<Accommodation__c> getAccommodationsRelatedToAP(List<Account_Participation__c> accountParticipationList){
        
        try {
            return  [SELECT Account_Part__c,Contact_Participation__c
                     FROM Accommodation__c
                     WHERE Account_Part__c IN :accountParticipationList
                            AND Status__c = 'booked'];
                                                           
        }
        catch(Exception e){
            system.debug(e.getMessage());
        }
                                                               
        return null;
    }
    public static Map<Id,Contact_Participation__c> getContactPartRelatedToAP(List<Account_Participation__c> accountParticipationList){
        
        try {
            return new Map<Id,Contact_Participation__c>([SELECT Id,Account_Participation__c 
                                                         FROM Contact_Participation__c
                                                         WHERE Account_Participation__c IN :accountParticipationList
                                                             AND No_Hotel_Room_needed_for_this_Contact__c = FALSE
                                                             AND Participation_Status__c IN ('registered','participating','Guest','Speaker')]);
                                                           
        }
        catch(Exception e){
            system.debug(e.getMessage());
        }                                        
        return null;
    } 
    public static Map<Id,Account_Participation__c> getAccountParticipationMap(String icefEventId){
        
        if(validate(icefEventId)){
            try {
                return new Map<Id,Account_Participation__c>([SELECT Id,Name,Account__r.Name,
                                                                RecordType.DeveloperName,Number_of_required_Accommodations__c,
                                                                Number_Hotel_Rooms_for_Accommodations__c
                                                             FROM Account_Participation__c
                                                             WHERE ICEF_Event__c = :icefEventId
                                                                 AND Participation_Status__c IN ('accepted','pending payment','registered')]);
            }
            catch(Exception e){
                system.debug(e.getMessage());
            }
            
        }
        return null;
    }
    
    public static List<List<String>> createTableOfHotelRooms(String accountParticipationRecordId){
    
        Account_Participation__c accountParticipationRecord;
        try {
            accountParticipationRecord = [SELECT ICEF_Event__c 
                                          FROM Account_Participation__c 
                                          WHERE Id = :accountParticipationRecordId];
        }
        catch(Exception e){
            system.debug(e.getMessage());
            return null;
        }
        system.debug('accountParticipationRecord '+accountParticipationRecord);
        
        // get related Hotel capacities
        Map<Id,Hotel_Capacity__c> hotelCapacityMap = getHotelCapacities(accountParticipationRecord.ICEF_Event__c);
        system.debug('hotelCapacityMap '+hotelCapacityMap);
        
        // get hotel rooms
        List<Hotel_Room__c> hotelRoomList = getHotelRooms(hotelCapacityMap.values());
        system.debug('hotelRoomList '+hotelRoomList);
        
        // get persons attending details 
        //Map<String,String> hotelRoomToPersonAttendingMap = getHotelRoomPersonAttendings(hotelRoomList);
        
        // create dataSet
        return createDatasetForHotelRoomTable(hotelRoomList,hotelCapacityMap);
        
    }
    /*
    public static Map<String,String> getHotelRoomPersonAttendings(List<Hotel_Room__c> hotelRoomList){
        if(validate(hotelRoomList)){
            List<Hotel_Room__History> hotelRoomHistoryList = new List<Hotel_Room__History>();
            try{
                hotelRoomHistoryList = [SELECT OldValue, NewValue, ParentId, Id 
                                        FROM Hotel_Room__History 
                                        WHERE ParentId IN :hotelRoomList 
                                            AND Field = 'Persons_attending__c' LIMIT 50000];
            }
            catch(Exception e){
                system.debug(e.getMessage());
            }
            if(!hotelRoomHistoryList.isEmpty()){
                Map<String,String> hotelRoomToPersonAttending = new Map<String,String>();
                for(Hotel_Room__History hotelRoomRecord : hotelRoomHistoryList){
                if(!hotelRoomToPersonAttending.containsKey(hotelRoomRecord.ParentId))
                    if(validate((String)hotelRoomRecord.NewValue))
                        hotelRoomToPersonAttending.put(hotelRoomRecord.ParentId,(String)hotelRoomRecord.NewValue); 
                    else if(validate((String)hotelRoomRecord.OldValue))
                        hotelRoomToPersonAttending.put(hotelRoomRecord.ParentId,(String)hotelRoomRecord.OldValue); 
                }
                return hotelRoomToPersonAttending;
            }
        }
        return null;
    }*/
    
    public static  Map<Id,Hotel_Capacity__c> getHotelCapacities(String icefEventId){
        //
        if(validate(icefEventId)){
            try{
                return  new Map<Id,Hotel_Capacity__c>([SELECT Id,RecordType.Name,Name 
                                                        FROM Hotel_Capacity__c
                                                        WHERE ICEF_Event__c = :icefEventId]);
            }
            catch(Exception e){
                system.debug(e.getMessage());
            }
        }
        return null;
    }
    
    //*** Number_of_Accommodations__c = 0 
    public static  List<Hotel_Room__c> getHotelRooms(List<Hotel_Capacity__c> hotelCapacitiesList){
        if(validate(hotelCapacitiesList)){
            try{
                return [SELECT Id,Name,Hotel_Capacity__c,Prior_Persons_attending__c 
                        FROM Hotel_Room__c
                        WHERE Hotel_Capacity__c = :hotelCapacitiesList
                            AND Number_of_Accommodations__c= 0];
            }
            catch(Exception e){
                system.debug(e.getMessage());
            }
        }
        return null;
    }
    
    
    // get Hotel rooms
    // create data set
    public static  List<List<string>> createDatasetForHotelRoomTable(List<Hotel_Room__c> hotelRoomsList,
                                                                     Map<Id,Hotel_Capacity__c> hotelCapacityMap
                                                                     ){
    
        if(validate(hotelRoomsList)) {
            List<String> datarow;
            List<List<String>> dataSet2 = new List<List<String>>();
            for(Hotel_Room__c hotelRoomRecord : hotelRoomsList){
                datarow = new List<String>();
                datarow.add('<input type="radio" name="rdo_hc" value="'+hotelRoomRecord.Id+'" Id="rdo_'+hotelRoomRecord.Id+'" >');// hotel number
                datarow.add('<a target="_blank"  style="color:#3333EC;" href="../'+hotelRoomRecord.Id+'">'+hotelRoomRecord.Name+'</a>');// hotel Number
                
                if(hotelCapacityMap.containsKey(hotelRoomRecord.Hotel_Capacity__c)){
                    datarow.add(hotelCapacityMap.get(hotelRoomRecord.Hotel_Capacity__c).Name);// hotel capacityName
                    if(hotelCapacityMap.get(hotelRoomRecord.Hotel_Capacity__c).RecordType != null)
                        if(validate(hotelCapacityMap.get(hotelRoomRecord.Hotel_Capacity__c).RecordType.Name))
                        datarow.add(hotelCapacityMap.get(hotelRoomRecord.Hotel_Capacity__c).RecordType.Name);
                        else
                        datarow.add('- none -');
                }
                else{
                    datarow.add('- none -');
                    datarow.add('- none -');
                }
                if(validate(hotelRoomRecord.Prior_Persons_attending__c))
                    datarow.add(hotelRoomRecord.Prior_Persons_attending__c);
                else
                    datarow.add('- none -');
                
                dataSet2.add(datarow);
            }
            return dataSet2;
        }
        return null;
    }
    public static  List<List<string>> createDatasetForAccountPartTable(List<Account_Participation__c> accountParticipationList){
    
        if(validate(accountParticipationList)) {
            List<String> datarow;
            List<List<String>> dataSet2 = new List<List<String>>();
            for(Account_Participation__c accountParticipationRecord : accountParticipationList){
                datarow = new List<String>();
                datarow.add('<input type="radio" name="rdo_hc" value="'+accountParticipationRecord.Id+'" Id="rdo_'+accountParticipationRecord.Id+'" >');// hotel number
                
                datarow.add('<a style="color:#3333EC;" href="../'+accountParticipationRecord.Id+'">'+accountParticipationRecord.Name+'</a>');// Accont Part name
                
                datarow.add(accountParticipationRecord.Account__r.Name);// account name
                if( accountParticipationRecord.Number_of_required_Accommodations__c == null){
                  datarow.add(' ');// required accommodation number
                }else{
                    datarow.add(''+accountParticipationRecord.Number_of_required_Accommodations__c);// required accommodation number
                }
                datarow.add(accountParticipationRecord.RecordType.DeveloperName);// RecordType Name
                
                if( accountParticipationRecord.Number_Hotel_Rooms_for_Accommodations__c == null){
                  datarow.add(' ');// Number Of Hotel Rooms
                }else{
                    datarow.add(''+accountParticipationRecord.Number_Hotel_Rooms_for_Accommodations__c);// Number Of Hotel Rooms
                }
                
                dataSet2.add(datarow);
            }
            system.debug('dataSet2 '+dataSet2);
            return dataSet2;
        }
        return null;
    }
    public static  boolean validate(String stringValue){
        if(stringValue != null && stringValue.trim() != '')
            return true;
        return false;
    } 
    public static  boolean validate(List<sObject> sObjectList){
        if(sObjectList != null && !sObjectList.isEmpty())
            return true;
        return false;
    } 
    
    /*
        function createDataSet(records){
        var dataSet = new Array();
        if(records != null){
            for (var i = 0; i < records.length; i++) {
                var dataRow = new Array();
                
                dataRow =[
                         "<input type='radio' name='rdo_hc' value='"+records[i].Id+"' Id='rdo_"+records[i].Id+"' >",
                         (records[i].Name != undefined?"<a target='_blank' class='trid' id='"+records[i].Id+"'  href='../../"+records[i].Id+"'>"+records[i].Name+"</a>" :'- none -'),
                         records[i].RecordType != undefined?records[i].RecordType.Name:'- none -',
                         records[i].Hotel_City__c || '- none -',
                         records[i].First_event_day_in_this_Hotel__c || '- none -'
                    ];
              
                dataSet.push(dataRow);
            }
        return dataSet;
        }
        else{
            return null;
        }
    }
    */
}