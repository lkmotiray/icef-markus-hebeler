/**
* @ Description : Class to accept the numeric value and return the ranking(word)
* @ Created Date : 5 Feb 2018
*/

public class NumberToWords {
    private static final List<String> specialNames = new List<String>{'',' thousand', ' million', ' billion',
                                                                      ' trillion', ' quadrillion', ' quintillion'};
            
    private static final List<String> tensNames = new List<String>{ '', ' ten', ' twenty', ' thirty', ' forty',
                                                                    ' fifty', ' sixty', ' seventy', ' eighty',
                                                                    ' ninety'};
                    
    private static final List<String> numNames = new List<String>{ '', ' first', ' second', ' third', ' fourth', ' fifth', ' sixth',
                                                                   ' seventh', ' eighth', ' ninth', ' tenth', ' eleventh', ' twelfth',
                                                                   ' thirteenth', ' fourteenth', ' fifteen', ' sixteenth', 
                                                                   ' seventeenth', ' eighteenth', ' nineteenth', 'twentieth'};

    private static String convertLessThanOneThousand(Integer num){
        String current;
        
        if (Math.mod(num,100) < 20){
            current = numNames[Math.mod(num,100)];
            num /= 100;
        }
        else {
            current = numNames[Math.mod(num,10)];
            num /= 10;
            
            current = tensNames[Math.mod(num,10)] + current;
            num /= 10;
        }
        if (num == 0) return current;
        return numNames[num] + ' hundred' + current;                            
    }
    
    public static String convert(String stringValue) {
        Integer num;
        
        if(stringValue.contains(',')){
            String val='';
            Integer i=0;
            if(stringValue.contains('-')){
                val='-';
                i=1;
            }
            for(; i<stringValue.length(); i++){
                if(stringValue.charAt(i)!= 44){
                    val+=''+(stringValue.charAt(i)-48);
                }
            }
            num = Integer.valueOf(val);
        }else{
            num = Integer.valueOf(stringValue);
        }

        if (num == 0) { return 'zero'; }
        
        String prefix = '';
        
        if (num < 0) {
            num = -num;
            prefix = 'negative';
        }
        
        String current = '';
        Integer place = 0;
        
        do {
            Integer n = Math.mod(num,1000);
            if (n != 0){
                String s = convertLessThanOneThousand(n);
                current = s + specialNames[place] + current;
            }
            place++;
            num /= 1000;
        } while (num > 0);
        
        return (prefix + current).trim();
    }
}