Public class ExtendICEFARMController
{
    public Set<Id> extSetValidAccIds{get;set;}
    
    public Map<String, List<Account_Participation__c>> ProcessAPs(List<ICEF_Event__c> listEvents)
    {
        Map<Id, Account_Participation__c> extMapAPs = new Map<Id, Account_Participation__c>([Select Id, ICEF_Event__c, Catalogue_Country__c, Participation_Status__c,First_Timer1__c, Account__c from Account_Participation__c where ICEF_Event__c = :listEvents]);
        Map<String, List<Account_Participation__c>> extMapAPWithCountry = new Map<String, List<Account_Participation__c>>();
        extSetValidAccIds = new Set<Id>();
                
        for(Id apId: extMapAPs.keySet())
        {
             
             extSetValidAccIds.add(extMapAPs.get(apId).Account__c);    
             //String key = String.valueOf(map_AP.get(apId).Catalogue_Country__c) + String.valueOf(map_AP.get(apId).ICEF_Event__c);
              List<Account_Participation__c> groupedAPs = extMapAPWithCountry.get(String.valueOf(extMapAPs.get(apId).Catalogue_Country__c) + String.valueOf(extMapAPs.get(apId).ICEF_Event__c));
              if (null==groupedAPs)
              {
               groupedAPs = new List<Account_Participation__c>();
               extMapAPWithCountry.put((String.valueOf(extMapAPs.get(apId).Catalogue_Country__c) + String.valueOf(extMapAPs.get(apId).ICEF_Event__c)), groupedAPs);
              }
           
             groupedAPs.add(extMapAPs.get(apId));
                         
        }
    
        return extMapAPWithCountry;
    }
    
   /****** --------------- TEST METHOD --------------- *************/
    
    public static testMethod void testExtendICEFARMController()
    {
        User currUser;
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile profile = [select id from profile where name='System Administrator'];
            UserRole userRole = [Select id from userrole where name='ARM'];
        
            currUser = new User(UserRoleId = userRole.Id, alias = 'dws', email='dwsTester@tester.com',
                   emailencodingkey='UTF-8', lastname='dwsTester', languagelocalekey='en_US',
                   localesidkey='en_US', profileId = profile.Id, 
           timezonesidkey='America/Los_Angeles', username='dwsTester@tester.com');
           
            insert currUser; 
            
            system.assertEquals('dwsTester@tester.com', currUser.username);   
        }
        Account acc = new Account(Name='Test Account',ownerId=currUser.Id);
        insert acc;   
        
        system.assertEquals('Test Account', acc.Name);     
        
        Contact con = new Contact(LastName='Tester', AccountId = acc.Id, Salutation = 'Dr.', Gender__c = 'male', Role__c = 'Other', Contact_Status__c = 'Active');
        insert con;
        
        system.assertEquals('Tester', con.LastName);
        
        Country__c country;
        
        System.runAs(currUser) {
             country = new Country__c(Name='Test Country', Agent_Relationship_Manager__c=currUser.Id, Region__c = 'Asia');
            insert country;
            system.assertEquals('Test Country', country.Name); 
        
        }
        
        date dueDate = date.newInstance(2012, 5, 29);
        ICEF_Event__c icefEvent = new ICEF_Event__c(Name='Test IcefEvent', Event_City__c = 'City', technical_event_name__c = 'TestingEvent', Event_Manager_Educators__c = currUser.Id, Event_Manager_Agents__c = currUser.Id, Start_date__c=dueDate);
        insert icefEvent;
        system.assertEquals('Test IcefEvent', icefEvent.Name);
        
        Account_Participation__c ap;
        ap = new Account_Participation__c(Account__c=acc.Id,ICEF_Event__c=icefEvent.Id, Organisational_Contact__c=con.Id,Participation_Status__c='accepted', Catalogue_Country__c=country.Id, Country_Section__c = country.Id);
        insert ap;
              
        
        List<ICEF_Event__c> testICEFEvents = [Select Id, Name from ICEF_Event__c];
        
        
        ExtendICEFARMController extCont = new ExtendICEFARMController();
        extCont.ProcessAPs(testICEFEvents);
    }

}