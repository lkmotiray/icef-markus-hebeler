/*  
    @Purpose      : Handler of attachment trigger
    @Author       : Poonam 
    @Created Date : 07/08/2017 
*/

public class AttachmentTriggerHandler {
    /*
        @Purpose     :  Populates latest attachment Id on parent Advert record
    */
    public static void populateLatestAttachmentId(List<Attachment> attachmentList){
        Set<Id> advertIdSet = new Set<Id>();
        Id parentId;
        
        for(Attachment attachment : attachmentList){
            parentId = attachment.ParentId;
            
            // Check if attachment parentId is of type Advert
            if(String.isNotBlank(parentId) && parentId.getSObjectType().getDescribe().getName() == 'Advert__c'){
                advertIdSet.add(parentId);
            }
        }    
        
        // If the set of Advert Ids is not blank, fetch the Advert records
        if(!advertIdSet.isEmpty()){
            List<Advert__c> advertList = new List<Advert__c>();
            
            try{
                advertList = [SELECT (SELECT Id FROM Attachments ORDER BY CreatedDate DESC LIMIT 1)
                              FROM Advert__c 
                              WHERE Id IN : advertIdSet];
            }
            catch(Exception e){
                System.debug('Exception in fetching advert records : ' + e.getMessage());
            }
            
            // Populate the latest attachment Id on record, make it blank if no attachment
            for(Advert__c advert : advertList){
                if(!advert.Attachments.isEmpty()){
                    advert.att_id__c = advert.Attachments[0].Id;
                }
                else{
                    advert.att_id__c = null;
                }
            }
            
            // Update the advert records
            try{
                update advertList;
            }
            catch(Exception e){
                System.debug('Exception in updating advert records : ' + e.getMessage());
            }
        }        
    }
}