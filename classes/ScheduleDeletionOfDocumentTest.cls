/**
  * @Author      : Prajakta
  * @Created Date: 13 May 2016
  * @Description : Test the functionality of class 'ScheduleDeletionOfDocument '
 */
@isTest
private class ScheduleDeletionOfDocumentTest 
{

    public static String CRON_EXP = '0 0 13 * * ?';

    static testmethod void testSchedularDeletionOfDocument() {
        
        String folderId = createTestData();
        
        Test.startTest();

        // Schedule the test job
        String jobId = System.schedule('test schedule', CRON_EXP,new ScheduleDeletionOfDocument());

        // Get the information from the CronTrigger API object
        CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered
                           FROM CronTrigger WHERE id = : jobId];

        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();

        List<Document> listDocuments = new List<Document>();
        
        listDocuments = [ SELECT Id FROM Document WHERE folderId = : folderId ];
        
        System.assert(listDocuments.isEmpty());
    }
    
    static String createTestData() 
    {
        List<Document> lstDocument = new List <Document>();
        
        //--Get id of CustomEmailTempAttachment folder
        Folder folder = [ SELECT Id FROM Folder WHERE Name =: 'CustomEmailTempAttachment'];
        
        //--Insert documents
        Document document = new Document();
        for (Integer count = 0; count < 5; count++) {

            document = new Document();
            document.body = Blob.valueOf('Test Doc');
            document.Name = 'Test' + count + 1;
            document.FolderId = folder.id;
            lstDocument.add(document);
        }

        insert lstDocument;
        
        return folder.id;
    }
}