@isTest
public class mergeAccountHandlerTest {
    
    static TestMethod void testTouchMemberships(){
        List<Account> accts = DataFactory.createTestAgents(1);
        List<Account> assocs = DataFactory.createTestAssociations(1, 'Association Membership');
        Test.startTest();
        insert accts;
        insert assocs;
        Membership__c mem = DataFactory.createTestMembership(accts[0],assocs[0]);        
        insert mem;
        Set<ID> idList = new Set<ID>();   
        idList.add(mem.Id);
        mergeAccountHandler.touchMemberships(idList);
        DateTime current = System.now();
        Test.stopTest();
        Membership__c membership = [Select Id, Nudge__c, Member__c, Association__c FROM Membership__c Where Id = :mem.Id];
        System.assertEquals(current, membership.Nudge__c);
    }
}