public class ReallocateHotelRoomsLightningController {

    public String recordId {get; set;}
    public ReallocateHotelRoomsLightningController(ApexPages.StandardController controller){
        recordId = controller.getId();
    }
    
    public PageReference init() {
        PageReference p;
        p = new PageReference('/apex/ReallocateHotelRooms?id='+recordId);
        p.setRedirect(true);
        return p;
    }
}