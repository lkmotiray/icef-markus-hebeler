/*  Test Class
    @purpose-This test class is developed to test the generateExchangeRatesRBA class
    @created_date-09-08-13
    @last_modified-[14-08-13]    
*/


@isTest(seeAllData = true)
public class testGenerateExchangeRatesRBA{
   
    /*  Function
    @purpose-This function is used to test the mockCallout behaviour and generates HttpResponse    
    @return-HttpResponse object
    @last_modified-[09-08-13]
    */

    public static HttpResponse getInfoFromExternalService() {
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://www.rba.gov.au/rss/rss-cb-exchange-rates.xml');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        return res;
    } 
    
    /*  Function
    @purpose-This function is used to test methods of generateExchangeRatesRBA class       
    @last_modified-[14-08-13]
    */
    static testmethod void test_generateExchangeRatesRBA()
    {
        System.debug('Start Test');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new mockHttpExchangeRatesRBA());
        
        
        // Call method to test.
        // This causes a dummy response to be sent from the class that implements HttpCalloutMock.
        
        HttpResponse res=getInfoFromExternalService();
        String str=res.getBody();
        System.debug('response...:'+str);
        
        //Prapare test data
        //prepareTestData();
        
        //execute the methods of generateExchangeRatesRBA
        generateExchangeRatesRBA.UnitTestXml=str;
        generateExchangeRatesRBA.insertExchageRates();
        
        //generateExchangeRatesRBA.scheduleUpdateExchangeRatesRBA();
        
        
        System.debug('Stop Test');
        generateExchangeRatesRBA.scheduleUpdateExchangeRatesRBA();
    }
    
    
}