/*  Test Class
    @purpose-This test class is developed to test the ApplicationEventsController class
    @created_date-27-02-14
    @last_modified-[07-03-14]    
*/


@isTest
public class TestApplicationEventsController{
        //Test Data
    public static Account testAccount;
    public static Contact testContact;
    public static Application__c testApp;
    public static List<ICEF_Event__c> testEventList;
    public static List<ICEF_Event__c> testPredecessorEventList;
    public static ICEF_Event__c testPredecessorEvent;
    public static List<Country__c> testCountryList;
    public static String testDependentJson;
    public static List<SelectOption> testDependent;
    
    
    /*  Test Method
        @purpose-This test method tests the functionality of ApplicationEventsController class
        @created_date-27-02-14
        @last_modified-[11-03-14]    
    */
    public static testMethod void test() {                                  
        
        prepareTestData();              
        PageReference pageRef = Page.ApplicationWiseEvents;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getparameters().put('id', testApp.id); 
        ApexPages.CurrentPage().getparameters().put('statusDetailsMap', testDependentJson); 
        ApexPages.CurrentPage().getparameters().put('errorLevel', 'ERROR');
        ApexPages.CurrentPage().getparameters().put('messageName', 'Comment Length is too large (Maximum comment length should be 255).');
        ApplicationEventsController controller = new ApplicationEventsController(); 
        ApplicationEventsController controller2 = new ApplicationEventsController(); 
        
        for(ApplicationEventsController.sObjectWrapper sobj: controller.sObjWrapList) {
            sobj.Selected = true;
            sobj.RelatedPartStatus = testDependent;
        }        
        for(ApplicationEventsController.sObjectWrapper sobj: controller.sObjWrapList) {            
            sobj.PartStatus = sobj.RelatedPartStatus[0].getValue();
        }
        controller.parseJson();
        Integer count = 0;
        for(ApplicationEventsController.sObjectWrapper sobj: controller.sObjWrapList) {            
            sobj.StatusDetails = sobj.RelatedStatusDetails[0].getValue();
            sobj.StatusComment = 'Reason '+count;
            count++;
        }
        
        System.debug('Test Data:::'+controller.sObjWrapList);
        for(ApplicationEventsController.sObjectWrapper sobj: controller.sObjWrapList) {            
            sobj.PartStatus = sobj.RelatedPartStatus[2].getValue();
        }
        controller.parseJson();        
        controller.showMessage();
        controller.nextPage();        
        controller.toggleStatusComment();
        controller.validateAndSave();  
        controller.previousPage();        
        controller.cancel();  
        
        
        controller.sObjWrapList[1].StatusComment = 'The list methods are all instance methods, that is, they operate on a particular instance of a list. For example, the following removes all elements from myList:myList.clear();Even though the clear method does not include any parameters, the list that calls it is its implicit parameter.';
        controller.validateAndSave();   
        
        controller.sObjWrapList[1].RecordType = 'Agent';
        controller.sObjWrapList[1].StatusDetails = 'Other Reason';
        controller.sObjWrapList[1].StatusComment = '';
        controller.nextPage();   
        controller.toggleStatusComment();
        controller.validateAndSave();  
        
        /*        
        controller2.sObjWrapList[1].Selected = true;
        controller2.sObjWrapList[1].PartStatus = 'canceled';
        controller2.sObjWrapList[1].StatusDetails = 'Other Reason';        
        controller2.sObjWrapList[1].StatusComment = 'General Reason';
        controller2.parseJson();
        controller2.toggleStatusComment();        
        controller2.validateAndSave();         */
    }
    
    /*  Test Method
        @purpose-This test support method prepares the test data for test method.
        @created_date-27-02-14
        @last_modified-[7-03-14]    
    */
    public static void prepareTestData() {   
            
        
        Country__c con = new Country__c(Name='USA',Agent_Relationship_Manager__c = Userinfo.getUserId());
        insert con;
        Account a = new Account(ARM__c = Userinfo.getUserId(),Name='Testing acc', Status__c='Active', Mailing_State_Province__c='CA', Mailing_Country__c=con.Id, account_owner_email__c='aaa.bbb@gmail.com', Email__c='aaa.bbb@gmail.com');
        insert a;
        
        testContact = new Contact();
        testContact.AccountId = a.Id;
        testContact.Gender__c = 'male';
        testContact.FirstName = 'First';
        testContact.LastName = 'Last';
        testContact.Salutation = 'Mr';
        insert testContact;
        
        testApp = new Application__c();
        testApp.Account__c =  a.id;
        testApp.Country__c = 'Afghanistan';
        testApp.Contact__c = testContact.id;
        testApp.Events_interested_in__c = 'ANZA;North America (Canada);North America (US);Japan - Korea Agent Roadshow;Raw';
        testApp.Looking_for_Partners_in__c = 'Australia;India;Afghanistan';
        testApp.Application_Date_Account__c = System.today();
        insert testApp;
        
        testCountryList = new Country__c[4];
        testCountryList.set(0,new Country__c(Name='Canada'));
        testCountryList.set(1,new Country__c(Name='USA'));
        testCountryList.set(2,new Country__c(Name='Japan' ));
        testCountryList.set(3,new Country__c(Name='Japan-Korea' ));
        insert testCountryList;
        
        testEventList = new ICEF_Event__c[3];
        testEventList.set(0, new ICEF_Event__c(Name='TestEvent1',
                                               Event_City__c = 'testEventCity1',
                                               technical_event_name__c = 'testEventTechinal1', 
                                               Event_Full_Name__c='TestFullEvent1', 
                                               Event_Country__c = testCountryList.get(0).id, 
                                               Start_date__c=System.today()));
        testEventList.set(1, new ICEF_Event__c(Name='TestEvent2',
                                               Event_City__c = 'testEventCity2',
                                               technical_event_name__c = 'testEventTechinal2', 
                                               Event_Full_Name__c='TestFullEvent2', 
                                               Event_Country__c = testCountryList.get(1).id, 
                                               Start_date__c=System.today()));
        testEventList.set(2, new ICEF_Event__c(Name='Japan - Korea',
                                               Event_City__c = 'testEventCity3',
                                               technical_event_name__c = 'testEventTechinal3', 
                                               Event_Full_Name__c='TestFullEvent3', 
                                               Event_Country__c = testCountryList.get(2).id, 
                                               Start_date__c=System.today()));
        insert testEventList;        
        
        List<ICEF_Event__c> allInsertedTestsEvents = [Select Id from ICEF_Event__c];
        
        testPredecessorEventList = new ICEF_Event__c[2];
        testPredecessorEventList.set(0, new ICEF_Event__c(Name='Raw',
                                                 Event_City__c = 'testEventCity4',
                                                 technical_event_name__c = 'testEventTechinal4', 
                                                 Event_Full_Name__c='TestFullEvent4', 
                                                 Event_Country__c = testCountryList.get(0).id, 
                                                 Start_date__c=System.today(), 
                                                 Predecessor_event__c = allInsertedTestsEvents[0].id));
        
        testPredecessorEventList.set(1, new ICEF_Event__c(Name='Raw1',
                                                 Event_City__c = 'testEventCity5',
                                                 technical_event_name__c = 'testEventTechinal5', 
                                                 Event_Full_Name__c='TestFullEvent5', 
                                                 Event_Country__c = testCountryList.get(1).id, 
                                                 Start_date__c=System.today(),
                                                 Predecessor_event__c = allInsertedTestsEvents[1].id));        
        insert testPredecessorEventList;   
        
        Account_Participation__c accPart = new Account_Participation__c(Account__c = a.id,
                                                                             ICEF_Event__c = allInsertedTestsEvents[0].id,
                                                                             Participation_Status__c = 'canceled',
                                                                             Country_Section__c = testCountryList.get(0).id,
                                                                             Catalogue_Country__c = testCountryList.get(0).id);
        insert accPart;
        
        prepareDependentJson();
        prepareDependentPickList();
    }
    
     /*  Test Method
        @purpose-This test support method prepares json test data for test method.
        @created_date-07-03-14
        @last_modified-[07-03-14]    
    */
    public static void prepareDependentJson() {
        testDependentJson = '';
        testDependentJson += '{"accepted":[],';
        testDependentJson += '"canceled":["Business needs re-evaluation","Health reasons","Other reason","Travel problems","Visa issues"],';
        testDependentJson += '"cax / on hold":[],';
        testDependentJson += '"cax on list":[],';
        testDependentJson += '"deactivated":[],';
        testDependentJson += '"declined":["Bad timing (high season)","Cost factor","EDU list not appealing","Enough providers from this market","Limited student demand","Other commitments","Prefer other WS / other market focus","Reason not provided","Unreachable","Visa issues","Other"],';
        testDependentJson += '"expression of interest":[],';
        testDependentJson += '"more info":[],';
        testDependentJson += '"n/a":[],';
        testDependentJson += '"no selling":[],';
        testDependentJson += '"no show":[],';
        testDependentJson += '"not accepted":["Blacklisted","Country cap","Incomplete Reg form","No/poor references","No focus on HE","No response","No students sent","Not target country","Other reason","Workshop cap"],';
        testDependentJson += '"pending payment":[],';
        testDependentJson += '"preliminary ok":[],';
        testDependentJson += '"reconfirmed":[],';
        testDependentJson += '"registered":[],';
        testDependentJson += '"transferred":[],';
        testDependentJson += '"wait listed":[]} ';
    }
    
    public static void prepareDependentPickList() {
        testDependent = new List<SelectOption>();
        testDependent.add(new SelectOption('canceled','canceled'));
        testDependent.add(new SelectOption('declined','declined'));
        testDependent.add(new SelectOption('expression of interest', 'expression of interest'));
        testDependent.add(new SelectOption('no show','no show'));
        testDependent.add(new SelectOption('pending payment','pending payment'));
        testDependent.add(new SelectOption('reconfirmed','reconfirmed'));
        testDependent.add(new SelectOption('registered','registered'));
        testDependent.add(new SelectOption('transferred','transferred'));
        testDependent.add(new SelectOption('wait listed','wait listed'));     
    }
}