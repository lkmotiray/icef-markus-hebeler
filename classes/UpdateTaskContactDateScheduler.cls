/**
    @ PURPOSE : TO UPDATE DATE ACTED UPON FIELD OF CONTACTS, ASSIGNED TO THE TASKS.
*/
public class UpdateTaskContactDateScheduler implements Schedulable {

    // HOLDS LAST FIRED TIME.
    public DateTime lastFiredDateTime { get; set; }
    
    /**
        @ PURPOSE : SETS THE DEFAULT VALUES OF THE ATTRIBUTES.
    */
    public UpdateTaskContactDateScheduler(DateTime lastFiredDateTime) {
        this.lastFiredDateTime = lastFiredDateTime;
    }
    
    public List<String> errorEmails(){
        List<String> toAdresses = new List<String>();
        toAdresses.add('mhebeler@icef.com');
        toAdresses.add('hschoenleber@icef.com');
        return toAdresses;
    }
    
    public void execute(SchedulableContext context) {
        
        try {
            // TO FETCH TASKS WHOSE ACTIVITY TYPE IS EMAIL.
            List<Task> listOfTasks = [ SELECT Id, WhoId, TaskSubtype, Activity_type__c, Status 
                                       FROM Task 
                                       WHERE TaskSubType = 'Email' AND WhoId != NULL AND 
                                             LastModifiedDate >= :lastFiredDateTime FOR UPDATE
                                     ];
            System.debug('Tasks ::: ' + JSON.serialize(listOfTasks));
            
            // UPDATE CONTACTS ASSOCIATED TO TASKS.
            if(listOfTasks != NULL && !listOfTasks.isEmpty()) {
                updateDateOfContacts(listOfTasks);
            }
            
            // SCHEDULE SAME JOB AFTER SOME SECONDS.
            System.abortJob(context.getTriggerId());
            DateTime systemTime = System.now().addSeconds(20);
            
            String CRON_EXP = ' ' + systemTime.second() + ' '   + systemTime.minute() + ' ' + 
                                    systemTime.hour()   + ' '   + systemTime.day()    + ' ' + 
                                    systemTime.month()  + ' ? ' + systemTime.year();
            
            System.schedule('Update Contact Associated To Email Task', 
                            CRON_EXP, 
                            new UpdateTaskContactDateScheduler(System.now()));
            
            System.debug('Update Task Contact scheduler has been scheduled for ' + systemTime + ' Time');
            
        } catch(Exception e) {
            System.debug('Exception : ' + e.getMessage() + ', Stack Trace : ' + e.getStackTraceString());
            
            // SEND MAIL IF ANY EXCEPTION OCCURS.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Notification : UpdateTaskContactDateScheduler has been stopped');
            mail.saveAsActivity = false;
            //mail.setTargetObjectId(UserInfo.getUserId());
            //List<String> toAdresses = new List<String>();
            //toAdresses.add('mhebeler@icef.com');
            //toAdresses.add('hschoenleber@icef.com');
            mail.setToAddresses(errorEmails());
            String body = 'Hi, \n The scheduler to update Date of contact associated to Task is stopped ' + 
                          'due to the following exception. \n Exception Message : ' + e.getMessage() + 
                          ' , \n Exception Stack Trace : ' + e.getStackTraceString() + '. \n Please Fix the issue and restart';
            mail.setPlainTextBody(body);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        }
    }
    
    /**
       @ PURPOSE     : FETCHES AND UPDATE DATE OF CONTACTS ASSOCIATED TO TASK.
       @ PARAMETER   : List<Task> [ TASKS ].
    */
    public void updateDateOfContacts(List<Task> listOfTasks) {
        // HOLDS CONTACT ID'S ASSIGNED TO TASKS WHOSE ACTIVITY TYPE IS EMAIL.
        Set<Id> setOfContactId = new Set<ID>();
        
        for(Task task : listOfTasks) {
            if(String.valueOf(task.whoId).startsWith('003')) {
                setOfContactId.add(task.WhoId);
            }
        }
        
        if(!setOfContactId.isEmpty()) {
            // FETCH CONTACTS ASSIGNED TO TASKS.
            List<Contact> listOfContacts = [ SELECT ID, Date_acted_upon__c 
                                             FROM Contact 
                                             WHERE ID IN : setOfContactId 
                                           ];
            System.debug('Contacts Associated To Tasks ::: ' + JSON.serialize(listOfContacts));
            
            // HOLDS CONTACT RECORDS TO BE UPDATED.
            List<Contact> listOfContactsToBeUpdated = new List<Contact>();
            
            // SET TODAY DATE FOR EACH CONTACT AND INTO LIST TO BE UPDATED.
            for(Contact contact : listOfContacts) {
                contact.Date_acted_upon__c = System.today();
                listOfContactsToBeUpdated.add(contact);
            }
            
            // UPDATE CONTACTS.
            if(!listOfContactsToBeUpdated.isEmpty()) {
                UPDATE listOfContactsToBeUpdated;
                System.debug('Updated Contacts ::: ' + JSON.serialize(listOfContactsToBeUpdated));
            }
        }
    }
}