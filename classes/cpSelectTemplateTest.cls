@IsTest
 public class cpSelectTemplateTest{

     static testMethod void cpselecttemplateTest1(){
                  
         Test.startTest();
         Id agentRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agent').getRecordTypeId();
         
         Country__c country = new Country__c(Name = 'Test Country 1', Region__c = 'Middle East');
         insert country;
         
         Account acc = new Account(Name = 'Test Account', RecordTypeId = agentRecordTypeId, 
                                   Status__c = 'Active', Mailing_Country__c = country.id,
                                   c2g__CODAAccountTradingCurrency__c = 'EUR',
                                   Mailing_City__c = 'test',
                                   ARM__c = UserInfo.getUserId());
         insert acc;
         update acc;
         
         Contact contact = new Contact(Salutation = 'Mr', Gender__C='male', 
                                       FirstName='newContactFirstName', 
                                       LastName='newContactLastName', 
                                       AccountId=acc.Id);
         insert contact;
         //Update contact;
         
         date dueDate = date.newInstance(2014, 1, 30);
         ICEF_Event__c icefEvent = new ICEF_Event__c(Name='Test IcefEvent', Event_City__c = 'test',
                                                     Event_Country__c = country.ID,
                                                     technical_event_name__c = 'TestingEvent', 
                                                     Event_Manager_Educators__c = UserInfo.getUserId(), 
                                                     Event_Manager_Agents__c = UserInfo.getUserId(), 
                                                     Start_date__c= dueDate);
         insert icefEvent;
         //update icefEvent;
         
         Id EducatorRecordTypeIdForAP = Schema.SObjectType.Account_Participation__c.getRecordTypeInfosByName().get('Educator').getRecordTypeId();
         Account_Participation__c ap = new Account_Participation__c(Account__c=acc.Id,ICEF_Event__c=icefEvent.Id, 
                                                                    Organisational_Contact__c=contact.Id,
                                                                    Participation_Status__c='accepted', 
                                                                    Catalogue_Country__c=country.Id, 
                                                                    Country_Section__c=country.Id,
                                                                    Newcomer__c = false,
                                                                    RecordTypeId = EducatorRecordTypeIdForAP);
         insert ap;
         //update ap;
         
         Contact_Participation__c cp = new Contact_Participation__c(Contact__c = contact.Id, 
                                                                    ICEF_Event__c = icefEvent.Id, 
                                                                    Account_Participation__c = ap.Id, 
                                                                    Participation_Status__c = 'Accepted', 
                                                                    Catalogue_Position__c = 2);
         insert cp;
         
         cpselecttemplate testObject = new cpselecttemplate();
         //cpselecttemplate.eturl eturl1;
         /**
			Start fooling around
		*/
         
         EmailTemplate et;
         List<Folder> f = [select Name from Folder where Type =: 'Email'];
         
         List<Folder> fldr = [Select id from Folder where Name =: f[0].Name];
         if(fldr.size() != 0)
             et = [Select Name, Description, TemplateType, IsActive from EmailTemplate where FolderId =: fldr[0].Id LIMIT 1];
         else
             et.clear();
         
         if(et != null){
             cpselecttemplate.eturl wrapObj = new cpselecttemplate.eturl(et);
         }
         
         
         
         /**
			End fooling around
		*/
         
         testObject.testCpId = cp.Id;
         testObject.getItems();
         testObject.cancel();
         testObject.showSelectedFoldertemplates();
         //testObject.getSelectedOption();
         //testObject.setSelectedOption('Test');
         testObject.getet();
         
         Test.stopTest();   
         
     }
     
 }