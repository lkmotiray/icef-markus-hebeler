@isTest
public class MembershipTriggerTest {
    public static List<Account> members;
    public static List<Account> associations;
    public static List<Membership__c> memberships;
    
    //create test data 
    //1 Agent 
    //1 Association of Type AM
    static void create1Agent1Assoc(){
        members = DataFactory.createTestAgents(1);
        associations = DataFactory.createTestAssociations(1,'Association Membership');
    }
    //create Test data
    //1 Agent
    //2 Associations of Type IQ
    static void create1Agent2Assocs(){
        members = DataFactory.createTestAgents(1);
        associations = DataFactory.createTestAssociations(2, 'Agent Industry Qualification');
    }
    
    //test trigger when an existing membership is updated to Status active
    
    //test trigger when an existing active membership is deleted
    @isTest static void testDeleteMembership(){
        create1Agent2Assocs();
        Test.startTest();
        memberships = new List<Membership__c>();
        insert members;
        insert associations;
        Account member = members.get(0);
        for (Account assoc : associations){
            Membership__c membership = DataFactory.createTestMembership(member, assoc);
            memberships.add(membership);
        }
        insert memberships;
        memberships.sort();
        Membership__c toDelete = memberships.get(0);
		delete toDelete;
        Account a = [SELECT	Id, Industry_Qualifications__c FROM Account WHERE Name = 'Test Agent 0'];
        System.assertEquals('short IQ 1', a.Industry_Qualifications__c);
        Test.stopTest();
        
    }

    // test trigger when an existing membership is updated to Status inactive
    @isTest static void updateStatusOfMembershipForAgentToInactive(){
        create1Agent2Assocs();
        Test.startTest();
        memberships = new List<Membership__c>();
        insert members;
        insert associations;
        Account member = members.get(0);
        for (Account assoc : associations){
            Membership__c membership = DataFactory.createTestMembership(member, assoc);
            memberships.add(membership);
        }
        insert memberships;
        memberships.sort();
        Membership__c msInactivate = memberships.get(0);
        msInactivate.Status__c = 'Inactive';
        msInactivate.Status_details__c = 'Wrong Entry';
        update msInactivate;
        Account a = [SELECT	Id, Industry_Qualifications__c FROM Account WHERE Name = 'Test Agent 0'];
        System.assertEquals('short IQ 1', a.Industry_Qualifications__c);
        Test.stopTest();
    }
    // test trigger when a new active membership is created
    @isTest static void testNewActiveMembership(){
        //create test data
        create1Agent1Assoc();
        
        //perform test
        Test.startTest();   
        insert members;
        insert associations;
        Account member = members.get(0);
        Account association = associations.get(0);     
        Membership__c membership = DataFactory.createTestMembership(member, association);        
        insert membership;

        
        //verify
        Account a = [SELECT Id, Memberships__c FROM Account WHERE Name = 'Test Agent 0'];
        System.assertEquals('short AM 0', a.Memberships__c);
        
        Test.stopTest();
    }
    // test trigger when a new inactive membership is created
    @isTest static void testNewInactiveMembership(){
        //create test data
        create1Agent1Assoc();
        
        //perform test
        Test.startTest();   
        insert members;
        insert associations;
        Account member = members.get(0);
        Account association = associations.get(0);     
        Membership__c membership = DataFactory.createTestMembership(member, association); 
        membership.Status__c = 'Inactive';
        membership.Status_details__c = 'Wrong Entry';
        insert membership;

        
        //verify
        Account a = [SELECT Id, Memberships__c FROM Account WHERE Name = 'Test Agent 0'];
        System.assertEquals(null, a.Memberships__c);
        
        Test.stopTest();
    }
}