/**
 * @ Purpose : Test class for NumberToWords class
 * @ Created Date : 3 March 2018
*/
@isTest
public class NumberToWordsTest {
	
    @isTest
    public static void testNumberToWordsResult(){
        
        String result = NumberToWords.convert('3');
        System.assertEquals('third', result);
        System.assertEquals('thirty thousand', NumberToWords.convert('30,000'));
    }
}