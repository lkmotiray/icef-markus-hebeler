/******************************************************************************************************************************************* 
  * TEST CLASS      -   TestAccountParticipationTrigger
  * PURPOSE         -   This test class is developed to test the Trigger AccountParticipationTrigger on the Object Account_Participation__c.
  * DEVELOPED BY    -   Dreamwares IT Solutions LLP [ Jasmine Jose ]
  * LAST MODIFIED   -   [04-09-2015]  
  * DESCRIPTION     -   [Tests the functioning - Total number of words present in 'Catalogoue Entry' field in the Object].
*******************************************************************************************************************************************/

@isTest
public class TestAccountParticipationTrigger {
    
    /********************************************************************************************************************************************  
     *  TEST Method     -   testCatalogueEntryWordsCount
     *  Purpose         -   This test method is to test the insert and update events of the Trigger AccountParticipationTrigger.
     *  Modified By     -   Dreamwares IT Solutions LLP [ Jasmine Jose ]
     *  Last Modified   -   [04-09-2015]  
     *  Description     -   It checks the following trigger events of the AccountParticipationTrigger Trigger
                            1)  before insert
                            2)  before update
    *********************************************************************************************************************************************/ 
    
    static testMethod void testCatalogueEntryWordsCount() {
        // Find a user contact to serve as ARM / territory Manager
        Id testUserId = System.UserInfo.getUserId();
        
        // Retrieve a record with RecordType to use in Account and Account Participation record insertion. //
        RecordType recType = [SELECT Id, Name FROM RecordType WHERE Name = 'Agent' and SobjectType='Account'];
        
        // Inserting Country record to use in Account and Account Participation record insertion. //
        Country__c testCountry = new Country__c(Name = 'Brazil', 
                                                Region__c = 'USA',
                                                Agent_Relationship_Manager__c = testUserId,
                                                Sales_Territory_Manager__c = testUserId
                                               );
        insert testCountry;
        
        // Inserting Account record using Country and RecordType Ids and use in Contact record. //
        Account testAccount = new Account(Name = 'Test Account', 
                                          RecordTypeId = recType.Id, 
                                          Status__c = 'Active',
                                          Mailing_City__c = 'Account City',
                                          Mailing_Country__c = testCountry.id);
        insert testAccount;
        
        // Inserting Contact record to use in Account Participation record insertion. //
        Contact testContact = new Contact(Salutation = 'Dr.', 
                                          FirstName = 'Test', 
                                          LastName = 'Test', 
                                          Gender__c = 'Male', 
                                          Role__c = 'Assistant', 
                                          Contact_Status__c = 'Active', 
                                          AccountId = testAccount.Id);
        insert testContact;
        
        // Create a date instance to use in ICEF Event record insertion. //
        Date testDate = Date.newInstance(2015, 04, 09);
        // Inserting the ICEF Event record using the date instance. //
        ICEF_Event__c testICEFEvent = new ICEF_Event__c(Name='Test IcefEvent', 
                                                        Event_City__c = 'City', 
                                                        technical_event_name__c = 'TestEvent', 
                                                        Event_Manager_Educators__c = UserInfo.getUserId(), 
                                                        Event_Manager_Agents__c = UserInfo.getUserId(), 
                                                        Event_Country__c = testCountry.Id,
                                                        Start_date__c = testDate);
        insert testICEFEvent; 
        
        // Insert a bulk of Account Participation records. //
        List<Account_Participation__c> listAccountParticipation = new List<Account_Participation__c>();
        Account_Participation__c insertAccPartRec;
        for(Integer indexOfAPartcpatnRec = 0; indexOfAPartcpatnRec < 30 ; indexOfAPartcpatnRec++){
            insertAccPartRec = new Account_Participation__c();
            insertAccPartRec.Account__c = testAccount.Id;
            insertAccPartRec.ICEF_Event__c = testICEFEvent.Id;
            insertAccPartRec.Organisational_Contact__c = testContact.Id;
            insertAccPartRec.Participation_Status__c = 'registered';
            insertAccPartRec.Catalogue_Country__c = testCountry.Id;
            insertAccPartRec.Country_Section__c = testCountry.Id;
            insertAccPartRec.Primary_Recruitment_Country__c = testCountry.Id;
            insertAccPartRec.Catalogue_entry__c = 'Salesforce Inc. is a global cloud computing company headquartered in San Francisco, California. Though best known for its customer relationship management (CRM) product.';
            listAccountParticipation.add(insertAccPartRec);
        }
        insert listAccountParticipation;
        
        // Verify the value of the Catalogue_entry_words__c field ( after insertion ). //
        List<Account_Participation__c> listAccountParticipationToAssert = new List<Account_Participation__c>();
        listAccountParticipationToAssert = [SELECT Id, Name, Catalogue_entry__c, Catalogue_entry_words__c FROM Account_Participation__c];
        System.assertEquals(23, listAccountParticipationToAssert.get(10).Catalogue_entry_words__c);
        
        // UPDATE TEST CODE - BEGINS HERE //
        
        // Get the list of inserted Account Participation records. //        
        List<Account_Participation__c> listAccParticipationToUpdate = new List<Account_Participation__c>();
        listAccParticipationToUpdate = [SELECT Id, Name, Catalogue_entry__c, Catalogue_entry_words__c FROM Account_Participation__c];
        
        // Update the Catalogue_entry__c field in the list of records retrieved. //
        List<Account_Participation__c> listAcctPartcpnUpdated = new List<Account_Participation__c>();
        for(Account_Participation__c updateAccPartRec : listAccParticipationToUpdate){
            updateAccPartRec.Catalogue_entry__c = 'Salesforce is a global cloud computing company headquartered in San Francisco, California and the best known CRM product.';
            listAcctPartcpnUpdated.add(updateAccPartRec);
        }
        update listAcctPartcpnUpdated;
        
        // Verify the value of the Catalogue_entry_words__c field ( after update ). //
        listAccountParticipationToAssert = [SELECT Id, Name, Catalogue_entry__c, Catalogue_entry_words__c FROM Account_Participation__c];
        System.assertEquals(18, listAccountParticipationToAssert.get(10).Catalogue_entry_words__c);
    }  
    
    /********************************************************************************************************************************************  
     *  TEST Method     -   testCatalogueEntryWordsCountNegative
     *  Purpose         -   This test method is to test the insert event of the Trigger AccountParticipationTrigger.
     *  Modified By     -   Dreamwares IT Solutions LLP [ Jasmine Jose ]
     *  Last Modified   -   [04-09-2015]  
     *  Description     -   It checks the following trigger events of the AccountParticipationTrigger Trigger
                            -  before insert
    *********************************************************************************************************************************************/ 
  
    static testMethod void testCatalogueEntryWordsCountNegative() {
        // Retrieve a record with RecordType to use in Account and Account Participation record insertion. //
        RecordType recType = [SELECT Id, Name FROM RecordType WHERE Name = 'Agent' and SobjectType='Account'];
        
        //logged in User is TestUser to use as ARM and territory Manager in country
        Id testUserId = System.UserInfo.getUserId();
        
        // Inserting Country record to use in Account and Account Participation record insertion. //
        Country__c testCountry = new Country__c(Name = 'Brazil',
                                                Sales_Territory_Manager__c = testUserId,
                                                Agent_Relationship_Manager__c = testUserId,
                                                Region__c = 'USA');
        insert testCountry;
        
        // Inserting Account record using Country and RecordType Ids and use in Contact record. //
        Account testAccount = new Account(Name = 'Test Account', 
                                          RecordTypeId = recType.Id, 
                                          Status__c = 'Active', 
                                          Mailing_City__c = 'Account City',
                                          Mailing_Country__c = testCountry.id);
        insert testAccount;
        
        // Inserting Contact record to use in Account Participation record insertion. //
        Contact testContact = new Contact(Salutation = 'Dr.', FirstName = 'Test', LastName = 'Test', Gender__c = 'Male', Role__c = 'Assistant', Contact_Status__c = 'Active', AccountId = testAccount.Id);
        insert testContact;
        
        // Create a date instance to use in ICEF Event record insertion. //
        Date testDate = Date.newInstance(2015, 04, 09);
        // Inserting the ICEF Event record using the date instance. //
        ICEF_Event__c testICEFEvent = new ICEF_Event__c(Name='Test IcefEvent', 
                                                        Event_City__c = 'City', 
                                                        technical_event_name__c = 'TestEvent', 
                                                        Event_Manager_Educators__c = UserInfo.getUserId(), 
                                                        Event_Manager_Agents__c = UserInfo.getUserId(), 
                                                        Event_Country__c = testCountry.Id,
                                                        Start_date__c = testDate);
        insert testICEFEvent; 
        
        // Insert a bulk of Account Participation records. //
        List<Account_Participation__c> listAccountParticipation = new List<Account_Participation__c>();
        Account_Participation__c insertAccPartRec;
        for(Integer indexOfAPartcpatnRec = 0; indexOfAPartcpatnRec < 30 ; indexOfAPartcpatnRec++){
            insertAccPartRec = new Account_Participation__c();
            insertAccPartRec.Account__c = testAccount.Id;
            insertAccPartRec.ICEF_Event__c = testICEFEvent.Id;
            insertAccPartRec.Organisational_Contact__c = testContact.Id;
            insertAccPartRec.Participation_Status__c = 'registered';
            insertAccPartRec.Catalogue_Country__c = testCountry.Id;
            insertAccPartRec.Country_Section__c = testCountry.Id;
            insertAccPartRec.Catalogue_entry__c = '';
            insertAccPartRec.Primary_Recruitment_Country__c = testCountry.Id;
            listAccountParticipation.add(insertAccPartRec);
        }
        insert listAccountParticipation;
        
        // Verify the value of the Catalogue_entry_words__c field ( after insertion ). //
        List<Account_Participation__c> listAccountParticipationToAssert = new List<Account_Participation__c>();
        listAccountParticipationToAssert = [SELECT Id, Name, Catalogue_entry__c, Catalogue_entry_words__c FROM Account_Participation__c];
        System.assertEquals(0, listAccountParticipationToAssert.get(10).Catalogue_entry_words__c);
    }  
       
    static testMethod void testRequiredAccommsForAPHotelNotNeeded(){
        AccParticipationTriggerHandler handler = new AccParticipationTriggerHandler();
        //Test Account
        //Test Contact
        //Test Event
        //Test AP: No Hotel Room needed: true
        //Test CPs: no Hotel room needed false, registered
        Test.startTest();
        Id testCountry = DataFactory.createTestCountry();
        Icef_Event__c testEvent = DataFactory.createTestEvent();
        
        insert testEvent;
        
        List<Account> testAccounts = DataFactory.createTestAgents(1);
        
        insert testAccounts;
        
        List<Account> acList = [SELECT Id, RecordTypeId FROM Account WHERE Name = 'Test Agent 0'];
        Account testAccount = acList[0];
        testAccount.Mailing_City__c = testEvent.Event_City__c;
        List<Contact> testContacts = DataFactory.createTestContacts(testAccount, 10);        
        
        insert testContacts;
        
        Contact testContact = testContacts[0];
        //System.debug('testAgent: ' + testAccounts);
        List<Account_Participation__c> testAPs = new List<Account_Participation__c>();
		Account_Participation__c testAPNoHotelRoom = new Account_Participation__c(Account__c = testAccount.Id,
                                                                       ICEF_Event__c = testEvent.Id,
                                                                       Organisational_Contact__c = testContact.Id,
                                                                       Participation_Status__c = 'accepted',
                                                                       Catalogue_Country__c = testCountry,
                                                                       Country_Section__c = testCountry,
                                                                       Primary_Recruitment_Country__c = testCountry,
                                                                       No_Hotel_Room_needed__c = true
                                                                      );
        testAPs.add(testAPNoHotelRoom);
        Account_Participation__c testAPHotelRoomNeeded = new Account_Participation__c(Account__c = testAccount.Id,
                                                                       ICEF_Event__c = testEvent.Id,
                                                                       Organisational_Contact__c = testContact.Id,
                                                                       Participation_Status__c = 'accepted',
                                                                       Catalogue_Country__c = testCountry,
                                                                       Country_Section__c = testCountry,
                                                                       Primary_Recruitment_Country__c = testCountry,
                                                                       No_Hotel_Room_needed__c = false
                                                                      );
        testAPs.add(testAPHotelRoomNeeded);
        
        insert testAPs;
        
        List <Contact_Participation__c> cps = new List<Contact_Participation__c>();
        for(Contact c : testContacts){
            Contact_Participation__c cp = new Contact_Participation__c();
            cp.Contact__c = c.Id;
            cp.Account_Participation__c = testAPNoHotelRoom.Id;
            cp.RecordTypeId = DataFactory.getRecordTypeId('Workshop Participant', 'Contact_Participation__c');
            cp.Participation_Status__c = 'registered';
            cp.Catalogue_Position__c = 1;
            cp.ICEF_Event__c = testEvent.Id;
            cps.add(cp);
            Contact_Participation__c cp2 = new Contact_Participation__c();
            cp2.Contact__c = c.Id;
            cp2.Account_Participation__c = testAPHotelRoomNeeded.Id;
            cp2.RecordTypeId = DataFactory.getRecordTypeId('Workshop Participant', 'Contact_Participation__c');
            cp2.Participation_Status__c = 'registered';
            cp2.Catalogue_Position__c = 1;
            cp2.ICEF_Event__c = testEvent.Id;
            cps.add(cp2);            
        }
        
        insert cps;
        
		Map<Id, Account_Participation__c> apMap = new Map<Id, Account_Participation__c>(testAPs);        
        handler.setNumberOfRequiredAccommodations(apMap);
        
        Test.stopTest();
        
        

    }
}