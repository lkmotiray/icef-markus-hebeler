@isTest
public class UpdateTaskContactDateSchedulerTest {

    // HOLDS COUNT OF RECORDS TO BE INSERTED.
    private static final Integer recordCount = 10;
    
    @testSetup
    private static void setupTestData() {
        
        // INSERT COUNTRY.
        Country__c country = new Country__c(Name = 'USA');
        country.Agent_Relationship_Manager__c = UserInfo.getUserId();
        INSERT country;
        
        // INSERT ACCOUNTS.
        List<Account> listOfAccounts = new List<Account>();
        for(Integer index = 0; index < recordCount; index++) {
            listOfAccounts.add(createAccount(index, country.Id));
        }
        INSERT listOfAccounts;
        
        // INSERT CONTACTS.
        List<Contact> listOfContacts = new List<Contact>();
        for(Integer index = 0; index < recordCount; index++) {
            listOfContacts.add(createContact(index, listOfAccounts.get(index).Id));
        }
        INSERT listOfContacts;

        // INSERT TASKS.
		List<Task> listOfTasks = new List<Task>();
        for(Integer index = 0; index < recordCount; index++) {
            listOfTasks.add(createTask(listOfContacts.get(index).Id));
        }
        INSERT listOfTasks;
    }
    
    @isTest
    private static void testUpdateContactScheduler() {
        
        TEST.startTest();
        
        Datetime currentDate = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ currentDate.minute() + ' * ' + currentDate.day() + ' ' + 
            					currentDate.month() + ' ? ' + currentDate.year();
        
        // UPDATE STATUS OF ALL TASKS.
        List<Task> listOfTasks = [ SELECT Id, Status FROM Task ];
        for(Task task : listOfTasks) {
            task.Status = 'In Progress';
        }
        UPDATE listOfTasks;
        
        DateTime currentTime = System.now();
        UpdateTaskContactDateScheduler testSched = new UpdateTaskContactDateScheduler(currentTime);
        
        System.schedule('Test Update Contact Associated To Email Task', CRON_EXP, 
                        testSched);
        List<String> testEmails = testSched.errorEmails();
//new UpdateTaskContactDateScheduler(System.now())        
        TEST.stopTest();
        
        // ASSERT ALL CONTACTS DATE ACTED UPON WITH TODAY DATE.
        List<Contact> listOfContacts = [ SELECT Id, Date_acted_upon__c FROM Contact ];
        for(Contact contact : listOfContacts) {
            System.assertEquals(contact.Date_acted_upon__c, System.today());
        }
        System.assertEquals('mhebeler@icef.com', testEmails[0]);
    }
    
    private static Account createAccount(Integer index, String countryId) {
        Account account = new Account();
        account.Name = 'TEST ACCOUNT ' + index;
        account.Mailing_State_Province__c = 'NY';
        account.CurrencyIsoCode = 'USD';
        account.Educational_Sector__c = '11 UNIVERSITIES';
        account.Status__c = 'Active';
        account.Mailing_Country__c = countryId;
        account.RecordTypeId='012200000005D7RAAU';
        account.ARM_emailaddress__c = 'test@mail.com' + index;
        return account;
    }
    
    private static Contact createContact(Integer index, String accountId) {
        Contact contact = new Contact();
        contact.LastName = 'TEST CONTACT ' + index;
        contact.AccountId = accountId;
        contact.Gender__c = 'male';
        contact.Role__c = 'Assistant';
        contact.Contact_Status__c = 'Active';
        contact.Salutation = 'Mr.';
        contact.Contact_Email__c = 'test.contact@org.com' + index;
        return contact;
    }
    
    private static Task createTask(String contactId) {
        Task task = new Task();
        task.TaskSubType = 'Email';
        task.Subject = 'Call or email';
        task.WhoId = contactId;
        task.Priority = 'Normal';
        task.Status = 'Not Started';
        return task;
    }
}