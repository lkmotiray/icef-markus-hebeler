/**
    @ Developer      :- DWS
    @ Purpose        :- Trigger handler for AccountParticipationTrigger.
    @ Created By     :- DWS
    @ LastModifiy By :- DWS
*/

Public Class AccPartTriggerHndlrForHRAccommodation{
    
   

    public static boolean isExecuted = false;
    /*
        @ Purpose     : Create Hotel Room record And Accommodation record for account participant and contact participant.
    */
    public static void createHotelRoomAndAccommodation(List<Account_Participation__c> newAccParticipationList,
                                                       Map<Id,Account_Participation__c> oldAccParticipationMap){
        system.debug('Before exec if');
        
       // if(!isExecuted){
            //if(runOnce()){
            
            //system.debug('In exec if');
            isExecuted = true;           
                                         
            // Filter List of Account_Participation__c
            List<Account_Participation__c> accountParticipationList = filterListOfAccountParticipation(newAccParticipationList, oldAccParticipationMap);        
            system.debug('accountParticipationList '+accountParticipationList);
            
            if(validateList(accountParticipationList)){ 

                // fetch list of Contact Participation
                List<Contact_Participation__c> contactParticipationList = getListOfContactParticipation(accountParticipationList);       
                system.debug('contactParticipationList '+contactParticipationList );
                
                //create map ap to cp 
                Map<ID,List<ID>> accPartIdToContactPartIdMap = createAccPartIdToContactPartIdMap(contactParticipationList);       
                system.debug('accPartIdToContactPartIdMap '+accPartIdToContactPartIdMap );
                
                Boolean isManualUpdation = false;
                if(newAccParticipationList.size() == 1)
                    isManualUpdation = true;
                    
                // create hotel room 
                List<Hotel_Room__c> hotelRoomList = createHotelRoom(accountParticipationList,accPartIdToContactPartIdMap,isManualUpdation);
                system.debug('after creation hotelRoomList '+hotelRoomList );
                
                if(isManualUpdation  && hotelRoomList != null && hotelRoomList.isEmpty()){
                    newAccParticipationList[0].Hotel_Capacity__c.addError('No contact participations available for this record. Please remove the Hotel Room Capacity');
                }        
                
                // create accommodation  check hotel room type, hotel capacity 
                if(hotelRoomList != null && !hotelRoomList.isEmpty())
                    createAccommodation(accountParticipationList,accPartIdToContactPartIdMap,hotelRoomList);
            }
        //}
    }
    
    /*
        @ Purpose     : Create Accommodation records.
        @ Parameter   : Account Participation List, Hotel Room List, Map of AP id to CP id
                        Map of AP id to CP count
    */
    public static void createAccommodation(List<Account_Participation__c> accountParticipationList,
                                           Map<ID,List<ID>> accPartIdToContactPartIdMap,
                                           List<Hotel_Room__c> hotelRoomList){
         system.debug('createAccommodation ::: ');

        List<Accommodation__c> accommodationList = new List<Accommodation__c>();
        
        String hotelRoomId;
        Map<ID,Hotel_Room__c> hotelRoomMap = new Map<ID,Hotel_Room__c>(hotelRoomList);
        Map<ID,Account_Participation__c> accountParticipationMap = new Map<ID,Account_Participation__c>(accountParticipationList);
        
        for(Account_Participation__c accountParticipation : accountParticipationList) {
            if(accPartIdToContactPartIdMap.containsKey(accountParticipation.Id)) {
                for(Hotel_Room__c hotelRoom : hotelRoomMap.values()){
                    hotelRoomId = null;
                    if(hotelRoom.Hotel_Capacity__c  == accountParticipation.Hotel_Capacity__c ){
                        hotelRoomId = hotelRoom.Id;
                        break;
                    }
                }
                if(hotelRoomId != null){
                    hotelRoomMap.remove(hotelRoomId);
                    for(ID contactPartId : accPartIdToContactPartIdMap.get(accountParticipation.Id)){
                        accommodationList.add(new Accommodation__c( Hotel_Room__c = hotelRoomId,
                                                                    Account_Part__c = accountParticipation.Id,
                                                                    Contact_Participation__c = contactPartId,
                                                                    Status__c = 'booked'
                                                                   ));
                    }
                }
            }
        }
        system.debug('Test Acc ::'+JSON.serialize(accommodationList));
        
        insert accommodationList;
       

    }
    /*
        @ Purpose     : Create Map of AP id to CP id.
        @ Parameter   : Contact Participation List
        @ Return Type : Map of AP id to CP id [Map<ID,List<ID>>]
    */
    public static void removeHotelCapacities(Set<String> ineligibleAccountParticipationsId) {
                                             
        List<Account_Participation__c> accountParticipationList = new List<Account_Participation__c>();                                     
        for(String accountParticipationId : ineligibleAccountParticipationsId){
            accountParticipationList.add(new Account_Participation__c( Id = accountParticipationId,
                                                                       Hotel_Capacity__c = null));
        }
        update accountParticipationList;
    }
    
    /*
        @ Purpose     : Create Map of AP id to CP id.
        @ Parameter   : Contact Participation List
        @ Return Type : Map of AP id to CP id [Map<ID,List<ID>>]
    */
    public static Map<ID,List<ID>> createAccPartIdToContactPartIdMap(List<Contact_Participation__c> contactParticipationList){
       
        Map<ID,List<ID>> accPartIdToContactPartIdMap = new Map<ID,List<ID>>();
        if(validateList(contactParticipationList)){
            
            List<ID> contactPartIdList;
            for(Contact_Participation__c contactParticipation : contactParticipationList){
                if(accPartIdToContactPartIdMap.containsKey(contactParticipation.Account_Participation__c)) {
                    contactPartIdList = accPartIdToContactPartIdMap.get(contactParticipation.Account_Participation__c);
                    contactPartIdList.add(contactParticipation.Id);
                    accPartIdToContactPartIdMap.put(contactParticipation.Account_Participation__c,contactPartIdList);
                }else {
                    contactPartIdList = new List<ID>();
                    contactPartIdList.add(contactParticipation.Id);
                    accPartIdToContactPartIdMap.put(contactParticipation.Account_Participation__c,contactPartIdList);
                }
            }
        }
        return accPartIdToContactPartIdMap;
    }
    
    /*
        @ Purpose     : create Hotel Room.
        @ Parameter   : Account Participation List
        @ Return Type : List of Hotel Room
    */
    public static List<Hotel_Room__c> createHotelRoom(List<Account_Participation__c> accountParticipationList,
                                                      Map<ID,List<ID>> accPartIdToContactPartIdMap,
                                                      Boolean isManualUpdation){
       
        if(validateList(accountParticipationList)) {
            List<Hotel_Room__c> hotelRoomList = new List<Hotel_Room__c>();
            Set<String> ineligibleAccountParticipationsId = new Set<String>();  
            system.debug('accountParticipationList to create '+accountParticipationList);
            system.debug('accPartIdToContactPartIdMap to create '+accPartIdToContactPartIdMap);
            
            
            for(Account_Participation__c accountParticipation : accountParticipationList){
                if(accPartIdToContactPartIdMap.containskey(accountParticipation.Id)) {
                    hotelRoomList.add(new Hotel_Room__c( Hotel_Capacity__c = accountParticipation.Hotel_Capacity__c,
                                                         Booking_Status__c = 'booked' ));
                }
                else{
                    ineligibleAccountParticipationsId.add(accountParticipation.Id);
                    system.debug('The account participation '+accountParticipation.Id+' cannot have hotel room due to zero number of contact participations');
                }
            }
            system.debug('hotelRoomList to create '+hotelRoomList);
            if(!isManualUpdation)
            removeHotelCapacities(ineligibleAccountParticipationsId);
            
            insert hotelRoomList;
            return hotelRoomList;
        }
        return null;
    }
    
    
    
    /*
        @ Purpose     : Get List Of Contact Participation.
        @ Parameter   : List of Account Participation
        @ Return Type : List Of Contact Participation
    */
    public static List<Contact_Participation__c> getListOfContactParticipation(List<Account_Participation__c> accountParticipationList){
       
        if(validateList(accountParticipationList)) {
            List<Contact_Participation__c> contactParticipationList = new List<Contact_Participation__c>();
            Set<String> statusSet = new Set<String>{'registered','Guest','Speaker','participating'};
            try {
                contactParticipationList = [SELECT Id,Participation_Status__c,Account_Participation__c  
                                            FROM Contact_Participation__c 
                                            WHERE Account_Participation__c IN :accountParticipationList
                                                AND Participation_Status__c IN :statusSet 
                                                AND No_Hotel_Room_needed_for_this_Contact__c = FALSE];

                return contactParticipationList;
            }
            catch(Exception e){
                system.debug(e.getMessage());
            }
        }
        return null;
    }
    
    /*
        @ Purpose     : Filter List Of Account Participation.
        @ Parameter   : List of Account Participation,Map of Account Participation
        @ Return Type : List of Account Participation
    */
    public static List<Account_Participation__c> filterListOfAccountParticipation( List<Account_Participation__c> newAccParticipationList,
                                                                                   Map<Id,Account_Participation__c> oldAccParticipationMap){
        //*** accountParticipationRecord.No_Hotel_Room_needed__c == FALSE

        if(validateList(newAccParticipationList) && validateMap(oldAccParticipationMap) ){
            List<Account_Participation__c> filteredAccountParticipation = new List<Account_Participation__c>();
            for(Account_Participation__c accountParticipationRecord : newAccParticipationList){
                if( accountParticipationRecord.Hotel_Capacity__c != null &&
                    accountParticipationRecord.Hotel_Capacity__c != oldAccParticipationMap.get(accountParticipationRecord.Id).Hotel_Capacity__c ) {
                        filteredAccountParticipation.add(accountParticipationRecord);
                    }
            }
            return filteredAccountParticipation;
        }
        return null;
    }
    
    /*
        @ Purpose     : validate sObject list.
        @ Parameter   : sObject list
        @ Return Type : Boolean value
    */
    public static Boolean validateList(List<sObject> sObjectList){
        
        if(sObjectList != null && !sObjectList.isEmpty()){
            return True;
        }
        return False;
    }
    
    /*
        @ Purpose     : validate sObject Map.
        @ Parameter   : sObject Map
        @ Return Type : Boolean value
    */
    public static Boolean validateMap(Map<Id,sObject> sObjectMap){
        
        if(sObjectMap != null && !sObjectMap.isEmpty()){
            return True;
        }
        return False;
    }
}