/**
  * @Description : Extension class for CreditNotePDF VF Page
  * @Created Date: 13 May 2016
 */
public with sharing class codaCreditNoteExtension {

    public c2g__codaCreditNote__c creditNote ;    
    
    public codaCreditNoteExtension (ApexPages.StandardController controller) {
               
        creditNote = (c2g__codaCreditNote__c)controller.getRecord();
                    
        String fileName = ApexPages.currentPage().getParameters().get('name');
        if( String.isNotEmpty(fileName) ) {
            ApexPages.currentPage().getHeaders().put('content-disposition',  'attachment; filename=\"' + fileName + '\"');
        }       
    }      
}