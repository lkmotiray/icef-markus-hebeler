/**
@ Developer          : Dreamwares IT Solutions [Sagar].
@ Purpose            : To Test AccommodationTrigger functionality                                 
@ Created Date       : 08/06/2016
*/
@isTest
public class AccommodationHandlerTest{
    
    @testSetup
    public static void testData(){        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user = new User(Alias = 'standt1', Email='standarduser11@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = p.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='standarduser111@testorg.com');
        
        INSERT user ;
        
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Global Workshop'
                                           AND SobjectType = 'ICEF_Event__c' ];
         
         // Insert Country     
        Country__c country = new Country__c();
        country.name ='Test';
        Country.Agent_Relationship_Manager__c = user.Id;
        country.Sales_Territory_Manager__c = user.Id;
        Insert country;
        
        ICEF_Event__c ICEF_Event = new ICEF_Event__c(RecordType = recordTypeRecord,
                                                     Name='test',Event_City__c='istanbul', 
                                                     technical_event_name__c='turkey_2016_istanbul', 
                                                     Event_Manager_Agents__c=user.id ,
                                                     Event_Manager_Educators__c=user.id,
                                                     Event_City_2__c ='istanbul',Event_Country__c =country.id 
                                                     );
        
        INSERT ICEF_Event;        
       
        
        Id AccREdordTypeId = [ SELECT Id, DeveloperName, SobjectType 
                              FROM RecordType 
                              WHERE SobjectType = 'Account' and DeveloperName = 'Educator' LIMIT 1].Id; 
        
        // Insert Account                      
        Account newAcc = new Account();
        newAcc.RecordTypeId = AccREdordTypeId;
        newAcc.Name = 'Test';
        newAcc.CurrencyIsoCode = 'EUR';
        newAcc.Educational_Sector__c = '5 MULTI-SECTORAL';
        newAcc.Status__c = 'Active';
        newAcc.ARM__c = user.Id;
        //newAcc.Mailing_Country__c = 'Berlin';
        newAcc.Mailing_Country__c = country.Id;
        Insert newAcc; 
        
        recordTypeRecord = [SELECT Id 
                            FROM RecordType 
                            WHERE Name = 'Agent'
                                AND SobjectType = 'Contact' ];
        
        Contact newContact = new Contact();
        newContact.RecordType = recordTypeRecord;
        newContact.firstName = 'TestFirstName';
        newContact.LastName= 'TestLastName';
        newContact.Salutation = 'Dr.';
        newContact.Gender__c= 'Male';
        newContact.AccountId = newAcc.Id;
        newContact.Role__c = 'Assistant';
        newContact.Contact_Status__c = 'Active';
        Insert newContact;
        
        recordTypeRecord = [SELECT Id 
                            FROM RecordType 
                            WHERE Name = 'Agent'
                                AND SobjectType = 'Account_Participation__c' ];
                                           
        Account_Participation__c accPart = new Account_Participation__c();
        accPart.RecordType = recordTypeRecord ;
        accPart.Account__c = newAcc.Id;
        accPart.ICEF_Event__c = ICEF_Event.Id;
        accPart.Country_Section__c = country.Id;
        accPart.Organisational_Contact__c = newContact.Id;
        accPart.Participation_Status__c = 'accepted';
        accPart.Catalogue_Country__c =country.Id;
        accPart.Primary_Recruitment_Country__c =country.Id;
        Insert accPart;
        
        /* Modified code */
        
        recordTypeRecord = [SELECT Id 
                            FROM RecordType 
                            WHERE Name = 'Workshop Participant'
                                AND SobjectType = 'Contact_Participation__c' ];
                                           
        Contact_Participation__c contactParticipationRecord = new Contact_Participation__c( RecordType = recordTypeRecord,
                                                                                            Contact__c = newContact.Id,
                                                                                            ICEF_Event__c = ICEF_Event.id,
                                                                                            Catalogue_Position__c = 1,
                                                                                            Participation_Status__c = 'registered',
                                                                                            Account_Participation__c = accPart.Id
                                                                                          );
        insert contactParticipationRecord;
        
        
        recordTypeRecord = [SELECT Id 
                            FROM RecordType 
                            WHERE Name = 'Hotel'
                                AND SobjectType = 'Account' ];
                               
        Account hotelRecord = new Account( RecordType = recordTypeRecord,
                                           Name = 'Test',
                                           Status__c = 'Active',
                                           Mailing_Country__c = country.Id
                                         );
        Insert hotelRecord;
        
        recordTypeRecord = [SELECT Id 
                           FROM RecordType 
                           WHERE Name = 'Agent Contingent'
                               AND SobjectType = 'Hotel_Capacity__c' ];
        
        List<Hotel_Capacity__c> hotelCapacityList = new List<Hotel_Capacity__c>();
        hotelCapacityList.add(new Hotel_Capacity__c(RecordType = recordTypeRecord,
                                                    name='test' ,
                                                    Check_in_Time__c='03:45', 
                                                    Check_out_Time__c='04:45' ,
                                                    ICEF_Event__c=ICEF_Event.id,
                                                    Hotel__c = hotelRecord.Id,
                                                    //Henriette - start: added new required field 
                                                    Security_deposit__c = 'Credit Card Details only'
                                                    //Henriette - end
                                                    ));
        INSERT hotelCapacityList;
        
        List<Hotel_Room__c> hotelRoomList = new List<Hotel_Room__c>();
        hotelRoomList.add(new Hotel_Room__c(Hotel_Capacity__c = hotelCapacityList[0].id ));
        INSERT hotelRoomList;
        
        List<Accommodation__c> AccommodationList = new List<Accommodation__c>();
        AccommodationList.add(new Accommodation__c( CurrencyIsoCode='AUD', 
                                                   Hotel_Room__c=hotelRoomList[0].id, 
                                                   Contact_Participation__c = contactParticipationRecord.Id, 
                                                   Account_Part__c = accPart.Id,
                                                   Arrival_Date__c=date.today().adddays(-1), 
                                                   Departure_Date__c=date.today().adddays(1), 
                                                   First_Name__c='test', 
                                                   Last_Name__c='test', 
                                                   Salutation__c='Dr.', 
                                                   Status__c='booked'
                                                  ) );
        
        AccommodationList.add(new Accommodation__c( CurrencyIsoCode='AUD', 
                                                   Hotel_Room__c=hotelRoomList[0].id, 
                                                   Contact_Participation__c = contactParticipationRecord.Id, 
                                                   Account_Part__c = accPart.Id,
                                                   Arrival_Date__c=date.today().adddays(-1), 
                                                   Departure_Date__c=date.today().adddays(1), 
                                                   First_Name__c='test', 
                                                   Last_Name__c='test', 
                                                   Salutation__c='Dr.', 
                                                   Status__c='booked') );
        
        AccommodationList.add(new Accommodation__c( CurrencyIsoCode='AUD', 
                                                   Hotel_Room__c=hotelRoomList[0].id, 
                                                   Contact_Participation__c = contactParticipationRecord.Id, 
                                                   Account_Part__c = accPart.Id,
                                                   Arrival_Date__c=date.today().adddays(-1), 
                                                   Departure_Date__c=date.today().adddays(1), 
                                                   First_Name__c='test', 
                                                   Last_Name__c='test', 
                                                   Salutation__c='Dr.', 
                                                   Status__c='booked') );
        
        AccommodationList.add(new Accommodation__c( CurrencyIsoCode='AUD', 
                                                   Hotel_Room__c=hotelRoomList[0].id, 
                                                   Contact_Participation__c = contactParticipationRecord.Id, 
                                                   Account_Part__c = accPart.Id,
                                                   Arrival_Date__c=date.today().adddays(-1), 
                                                   Departure_Date__c=date.today().adddays(1), 
                                                   First_Name__c='test', 
                                                   Last_Name__c='test', 
                                                   Salutation__c='Dr.', 
                                                   Status__c='booked' ) );
        
        AccommodationList.add(new Accommodation__c( CurrencyIsoCode='AUD', 
                                                   Hotel_Room__c=hotelRoomList[0].id, 
                                                   Contact_Participation__c = contactParticipationRecord.Id, 
                                                   Account_Part__c = accPart.Id,
                                                   Arrival_Date__c=date.today().adddays(-1), 
                                                   Departure_Date__c=date.today().adddays(1), 
                                                   First_Name__c='test', 
                                                   Last_Name__c='test', 
                                                   Salutation__c='Dr.', 
                                                   Status__c='booked') );
        
        INSERT AccommodationList;
    }
    
    public static testmethod void testingAccommodation(){
        
        List<Accommodation__c> AccommodationList = [SELECT Id, Name, CurrencyIsoCode, Hotel_Room__c, Contact_Participation__c, Account_Part__c,
                                                    Arrival_Date__c, Departure_Date__c, First_Name__c, Last_Name__c, Salutation__c, 
                                                    Status__c FROM Accommodation__c];

        AccommodationList[0].Salutation__c ='Captain';
        AccommodationList[0].First_Name__c='testing';
        AccommodationList[0].Last_Name__c='testing';
        
        AccommodationList[1].Salutation__c ='Captain';
        AccommodationList[1].First_Name__c='testing';
        AccommodationList[1].Last_Name__c='testing';
        
        AccommodationList[2].Salutation__c ='Captain';
        AccommodationList[2].First_Name__c='testing';
        AccommodationList[2].Last_Name__c='testing';
        
        AccommodationList[3].Salutation__c ='Captain';
        AccommodationList[3].First_Name__c='testing';
        AccommodationList[3].Last_Name__c='testing';

        UPDATE AccommodationList;
        
        DELETE AccommodationList;
    }
    //Henriette's test methods
    @isTest static void testChangeOfHotelRoom(){
        Test.startTest();
        Hotel_Capacity__c hc = [SELECT Id FROM Hotel_Capacity__c LIMIT 1];
        List<Hotel_Room__c> HotelRoomList = [SELECT Id, Persons_attending__c FROM Hotel_Room__c];
        HotelRoomList.add(new Hotel_Room__c(Hotel_Capacity__c = hc.Id));
        
        upsert HotelRoomList;
        
        List<Accommodation__c> AccommodationList0 = [SELECT Id, Name, CurrencyIsoCode, Hotel_Room__c, Contact_Participation__c, Account_Part__c,
                                                    Arrival_Date__c, Departure_Date__c, First_Name__c, Last_Name__c, Salutation__c, 
                                                    Status__c FROM Accommodation__c WHERE Hotel_Room__c = :HotelRoomList[0].id];
        
        List<Accommodation__c> AccommodationList1 = [SELECT Id, Name, CurrencyIsoCode, Hotel_Room__c, Contact_Participation__c, Account_Part__c,
                                                    Arrival_Date__c, Departure_Date__c, First_Name__c, Last_Name__c, Salutation__c, 
                                                    Status__c FROM Accommodation__c WHERE Hotel_Room__c = :HotelRoomList[1].id];
        
        String pa0 = HotelRoomList[0].Persons_attending__c;
        String pa1 = HotelRoomList[1].Persons_attending__c;
        
        AccommodationList0[0].Hotel_Room__c = HotelRoomList[1].id;
        
        update AccommodationList0;
        Test.stopTest();
        
        Hotel_Room__c hr0 = [SELECT Id, Persons_attending__c FROM Hotel_Room__c WHERE Id = :HotelRoomList[0].Id];
        Hotel_Room__c hr1 = [SELECT Id, Persons_attending__c FROM Hotel_Room__c WHERE Id = :HotelRoomList[1].Id];
        
        System.assertNotEquals(hr0.Persons_attending__c, pa0);
        System.assertNotEquals(hr1.Persons_attending__c, pa1);
    }
}