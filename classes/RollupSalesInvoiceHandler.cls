/*
@ Name          : RollupSalesInvoiceHandler
@ Description   : Calculate number of Sales Invoices related to an Opportunity in the Opportunity
@ Created By    : 
@ Date          : 
@ Calling From  : SalesInvoiceTrigger
*/

public class RollupSalesInvoiceHandler
{
    //-- List of Opportunity object's records to be updated    
    List<Opportunity> updatingOpportunityList = new List<Opportunity>();
    
    //-- List of Opportunity Id`s to be updated
    Set<Id> newOpportunityId = new Set<Id>();
    
    /*
    @   Description : Calculate number of Sales Invoices related to an Opportunity in the Opportunity and update it
    */
    public void updateNumberofSalesInvoicesTotal()
    {
        //-- get the actual Opportunity object records to update
        for(Opportunity oppRec : [SELECT Id, Number_of_Sales_Invoices__c,
                                         (SELECT Id FROM c2g__Invoices__r Where c2g__InvoiceStatus__c = 'Complete')
                                          FROM Opportunity WHERE Id IN :newOpportunityId])
        {   
            
            oppRec.Number_of_Sales_Invoices__c = oppRec.c2g__Invoices__r.size();
            updatingOpportunityList.add(oppRec);                 
        } 
        if( updatingOpportunityList.size() > 0)
            update updatingOpportunityList; 
    }
    
   
   /*
    @   Description : inserting the the new invoice record in Opportunity object
    */
    public void insertInvoice(List<c2g__codaInvoice__c> newInvoiceList)
    {
        //get the Id`s of all Opportunity records affected by insert operation on invoice object
        for(c2g__codaInvoice__c invoice : newInvoiceList)
        {
            newOpportunityId.add(invoice.c2g__Opportunity__c);
        }
        updateNumberofSalesInvoicesTotal();
    }     
    

  
    /*
    @   Description : deleting the the invoice record from Opportunity object
    */
  /*  public void deleteInvoice(c2g__codaInvoice__c[] oldInvoiceList)
    {       
        //get the Id`s of all Opportunity records affected by delete operation on invoice object
        for(c2g__codaInvoice__c invoice : oldInvoiceList)
        {
            newOpportunityId.add(invoice.c2g__Opportunity__c);
        }
        updateNumberofSalesInvoicesTotal(); 
    }*/
    
    /*
    @   Description : adding the the invoice record from Opportunity object  
    */
   /* public void undeleteInvoice(c2g__codaInvoice__c[] newInvoiceList)
    {
        //get the Id`s of all Opportunity records affected by Undelete operation on invoice object
        for(c2g__codaInvoice__c invoice : newInvoiceList)
        {
            newOpportunityId.add(invoice.c2g__Opportunity__c);
        }
        updateNumberofSalesInvoicesTotal();            
    } */
}