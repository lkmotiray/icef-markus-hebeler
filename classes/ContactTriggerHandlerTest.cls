/**
 * @auhor Mahesh G.
 * @createdDate 05/31/2016
 */
@IsTest
private class ContactTriggerHandlerTest {
    static testMethod void testUpdateAccountContactHasEmailField() {
        Country__c country = new Country__c(Name = 'USA');
        country.Agent_Relationship_Manager__c = UserInfo.getUserId();
        insert country;
        
        List<Account> listAccounts = new List<Account>();
        for(Integer index = 0; index < 20; index++) {
            Account newAccount = new Account();
            newAccount.Name = 'Test Account-' + index;
            newAccount.Mailing_State_Province__c = 'NY';
            newAccount.CurrencyIsoCode = 'USD';
            newAccount.Educational_Sector__c = '11 UNIVERSITIES';
            newAccount.Status__c = 'Active';
            newAccount.Mailing_Country__c = country.Id;
            newAccount.RecordTypeId='012200000005D7RAAU';
            newAccount.ARM_emailaddress__c = 'test@mail.com' + index;
            listAccounts.add(newAccount);
        }
        insert listAccounts;
        
        
        Test.startTest();
        
        List<Contact> listContacts = new List<Contact>();
        for(Integer index = 0; index < 10; index++) {
            Contact newContactWithEmail = new Contact();
			newContactWithEmail.Salutation = 'Mr.';
            newContactWithEmail.LastName = 'ContactEmail' + index;
            newContactWithEmail.AccountId = listAccounts[index].Id;
            newContactWithEmail.Contact_Email__c = 'test.contact@org.com' + index;
            listContacts.add(newContactWithEmail);
            
            Contact newContactWithoutEmail = new Contact();
            newContactWithoutEmail.Salutation = 'Mr.';
            newContactWithoutEmail.LastName = 'ContactEmail' + (index + 10);
            newContactWithoutEmail.AccountId = listAccounts[index + 10].Id;
            listContacts.add(newContactWithoutEmail);
        }
        
        insert listContacts;
        system.debug('listContacts[0]: ' + listContacts[0]);
        System.debug('listContacts[15]: ' + listContacts[15]);
        Test.stopTest();
        
        Map<Boolean, Integer> mapExpectedResult = new Map<Boolean, Integer> {
            									  	true => 10,
													false => 10                                                        
        										  };
		AggregateResult[] aggregateResults = [SELECT Has_Contact_with_emailaddress__c hasEmail, COUNT(Id) total
                                              FROM Account 
                                              WHERE Id IN :listAccounts
                                              GROUP BY Has_Contact_with_emailaddress__c];
        System.debug('aggregateResults[0]: ' + aggregateResults[0]);
        System.debug('aggregateResults[1]: ' + aggregateResults[1]);
		System.assertEquals(mapExpectedResult.get((Boolean)aggregateResults[0].get('hasEmail')), (Integer)aggregateResults[0].get('total'));
        System.assertEquals(mapExpectedResult.get((Boolean)aggregateResults[1].get('hasEmail')), (Integer)aggregateResults[1].get('total'));       
    }
}