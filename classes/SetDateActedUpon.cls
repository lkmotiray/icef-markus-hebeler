/*Mass updates field 'Date Acted Upon' of all selected contacts with the current date.*/


global class SetDateActedUpon 
{ 
    webService static String setDateActedUpon(List<Id> lstSelectedContactIds)
    { 
        try
        {
            //get contact and related account information
            List<Contact> lstSelectedContacts = [SELECT Id, AccountId, Account.OwnerId,Date_acted_upon__c
                                                 FROM Contact
                                                 WHERE Id IN :lstSelectedContactIds];
        
            //fill field Date Acted Upon                                     
            if(!lstSelectedContacts.isEmpty())
            {
                             
                for(Contact contact : lstSelectedContacts)
                {
                    if(contact.AccountId != null)
                    {
                                
                        contact.Date_acted_upon__c = System.Today();
                    }
                }
                
                update lstSelectedContacts;
                return 'Please reload list view to finalise removal.';
               
            }
        }
        catch(Exception e)
        {
            system.debug('Error : ' + e.getMessage());
            return 'Unable to set Dates';
        }
        return 'Error';
    }
}