/*  Test Class
    @purpose-This test class is developed to test the scheduleGenerateExchangeRates class
    @created_date-27-04-15
    @last_modified-[27-04-15]    
*/

@isTest
public class testScheduleGenerateExchangeRates {
	/*  Function
    @purpose-This function is used to test the scheduled job        
    @last_modified-[27-04-15]
	*/
    
     public static testMethod void testschedule() 
    {
               
        Test.StartTest();
		string htmlDoc = '<li><div><span class="xml">&nbsp;</span>XML file available for parsing: http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml</div></li>';
        
        generateExchangeRates.unitTestHtml = htmlDoc;
        
        string xml = '<?xml version="1.0" encoding="UTF-8"?>';
        xml += '<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01" xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">';
        xml += '<gesmes:subject>Reference rates</gesmes:subject>';
        xml += '<gesmes:Sender>';
        xml += '<gesmes:name>European Central Bank</gesmes:name>';
        xml += '</gesmes:Sender>';
        xml += '<Cube>';
        xml += '<Cube time=\'';
        xml += System.Today();
        xml += '\'>';
        xml += '<Cube currency=\'USD\' rate=\'1.3042\'/>';            
        xml += '<Cube currency=\'GBP\' rate=\'0.82810\'/>';         
        xml += '<Cube currency=\'AUD\' rate=\'1.2192\'/>';
        xml += '<Cube currency=\'BRL\' rate=\'2.2549\'/>';
        xml += '<Cube currency=\'CAD\' rate=\'1.3019\'/>';
        xml += '</Cube>';
        xml += '</Cube>';
        xml += '</gesmes:Envelope>';  
        
        generateExchangeRates.UnitTestXml = xml;   
        
        scheduledGenerateExchangeRates sger = new scheduledGenerateExchangeRates();
        String sch = '0 0 23 * * ?';
        string jobId = system.schedule('Test update exchange rate schedule', sch, sger);
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals('0 0 23 * * ?',ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        
        Test.stopTest();

    }
}