/**
  * @Author      : Prajakta
  * @Description : Extension class for ICEF_Asia_Pac_Sales_Invoice_PDF, ICEF_GmbH_Sales_Invoice_PDF
  * @Purpose     : To auto download the pdf              
  * @Created Date: 13 May 2016 
 */
public with sharing class codaInvoiceExtension {

    public c2g__codaInvoice__c invoice ;
    public Map<String, Decimal> lineItemToAmountDeuMap { get;set; }
    public Map<String, Decimal> lineItemToTillDateAmountMap { get;set; }
    public Map<String, Decimal> lineItemToCumulativeAmountMap { get;set; }
    public List<c2g__codaInvoiceInstallmentLineItem__c> lineItemList { get;set; }	
	public Map<String, Decimal> totalValuesMap { get;set; }    
    
    public codaInvoiceExtension(ApexPages.StandardController controller) {
        
        lineItemToAmountDeuMap = new Map<String, Decimal>();
        lineItemToTillDateAmountMap = new Map<String, Decimal>();
        lineItemToCumulativeAmountMap = new Map<String, Decimal>();
        lineItemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        totalValuesMap = new Map<String, Decimal>();		
        invoice = (c2g__codaInvoice__c)controller.getRecord();                    
        String fileName = ApexPages.currentPage().getParameters().get('name');
        if( String.isNotEmpty(fileName) ) {
            ApexPages.currentPage().getHeaders().put('content-disposition',  'attachment; filename=\"' + fileName + '\"');
        }
        InvoiceEmailSend.mapInvoiceLines = InvoiceEmailSend.createMapInvoceineItems(new Set<Id>{invoice.id});
		InvoiceEmailSend.mapInvoices = InvoiceEmailSend.createMapInvoces(new Set<Id>{invoice.id});
            
        computeAmountDue();
    }   

    /*
    * @ Description : Method to compute amount due and generate respective Map
    */
    private void computeAmountDue(){
        
        //List<c2g__codaInvoiceInstallmentLineItem__c> lineItemList = new List<c2g__codaInvoiceInstallmentLineItem__c>();
        
        
        /*try{
            idToLineItemMap = new Map<Id, c2g__codaInvoiceInstallmentLineItem__c> ([ SELECT Id, c2g__Amount__c
                                                                                     FROM c2g__codaInvoiceInstallmentLineItem__c
                                                                                     WHERE c2g__Invoice__c = :invoice.Id 
                                                                                     ORDER BY c2g__DueDate__c ]);
        }catch(Exception e){
            System.debug('Exception Occured while fetching Installment Line Item'+ e);
        }   */
        
        Decimal amountDue = 0;
        Decimal tillDatePaid = 0;
        Decimal cumulativeAmount = 0;
		Decimal totalAmtDue = 0;
		Decimal totalTillDatePaid = 0;
		Decimal totalAmt = 0;
		Decimal invoicePaid2Date = 0;
		
        if(invoice.c2g__InvoiceTotal__c != null && invoice.c2g__OutstandingValue__c != null){
			invoicePaid2Date = invoice.c2g__InvoiceTotal__c - invoice.c2g__OutstandingValue__c;
		}
        System.debug('invoicePaid2Date '+ invoicePaid2Date);
        
        /*try{
            lineItemList = [ SELECT Id, c2g__Amount__c, c2g__LineNumber__c ,
                                    c2g__DueDate__c
                             FROM c2g__codaInvoiceInstallmentLineItem__c
                             WHERE c2g__Invoice__c = :invoice.Id 
                             ORDER BY c2g__DueDate__c ];
        }catch(Exception e){
            System.debug('Exception Occured while fetching Installment Line Item'+ e);
        }*/
        if( InvoiceEmailSend.mapInvoiceLines != NULL ){
            if(InvoiceEmailSend.mapInvoiceLines.containsKey(invoice.Id)){
                lineItemList = InvoiceEmailSend.mapInvoiceLines.get(invoice.Id);
            }
        }
        
        for( c2g__codaInvoiceInstallmentLineItem__c item : lineItemList ){  
            
            if(item.c2g__Amount__c <= invoicePaid2Date) {
            
                tillDatePaid = item.c2g__Amount__c;
                invoicePaid2Date = invoicePaid2Date - tillDatePaid;                                
                amountDue = item.c2g__Amount__c - tillDatePaid;
                            
                cumulativeAmount = cumulativeAmount + amountDue;
                
                lineItemToCumulativeAmountMap.put( item.Id, cumulativeAmount);      
                lineItemToAmountDeuMap.put( item.Id, amountDue);    
                lineItemToTillDateAmountMap.put( item.Id, tillDatePaid);
				
				totalAmtDue = totalAmtDue + amountDue;
				totalTillDatePaid = totalTillDatePaid + tillDatePaid;
				totalAmt = totalAmt + item.c2g__Amount__c;
                
            }else if(item.c2g__Amount__c >= invoicePaid2Date && invoicePaid2Date != 0){
                
                tillDatePaid =  item.c2g__Amount__c - invoicePaid2Date;
                invoicePaid2Date = 0;                
                amountDue = item.c2g__Amount__c - tillDatePaid;
                            
                cumulativeAmount = cumulativeAmount + tillDatePaid;
                
                lineItemToCumulativeAmountMap.put( item.Id, cumulativeAmount);                                      
                lineItemToAmountDeuMap.put( item.Id, tillDatePaid);    
                lineItemToTillDateAmountMap.put( item.Id, amountDue);
				
				totalAmtDue = totalAmtDue + tillDatePaid;
				totalTillDatePaid = totalTillDatePaid + amountDue;
				totalAmt = totalAmt + item.c2g__Amount__c;
                
            }else{
                tillDatePaid = 0;
                invoicePaid2Date = 0;
                amountDue = item.c2g__Amount__c - tillDatePaid;
                            
                cumulativeAmount = cumulativeAmount + amountDue;
                
                lineItemToCumulativeAmountMap.put( item.Id, cumulativeAmount);                                      
                lineItemToAmountDeuMap.put( item.Id, amountDue);    
                lineItemToTillDateAmountMap.put( item.Id, tillDatePaid);
				
				totalAmtDue = totalAmtDue + amountDue;
				totalTillDatePaid = totalTillDatePaid + tillDatePaid;
				totalAmt = totalAmt + item.c2g__Amount__c;
            }                       
        }
		totalValuesMap.put('totalAmtDue', totalAmtDue);
		totalValuesMap.put('totalTillDatePaid', totalTillDatePaid);
		totalValuesMap.put('totalAmt', totalAmt);
		
		System.debug('totalAmtDue :::'+ totalAmtDue);
        System.debug('totalTillDatePaid::: '+ totalTillDatePaid);
    }
}