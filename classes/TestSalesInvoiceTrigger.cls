/*
@ Name          : TetsSalesInvoiceTrigger
@ Description   : Test class for SalesInvoiceTrigger
@ Created By    : 
@ Date          : 22-jan-2016
@ Calling From  : 
*/

@isTest(SeeAllData = true)
public class TestSalesInvoiceTrigger {
   
    /**
         @MehodName   : testMethod1
         @Purpose     : Test Method 
         @CreatedDate : 
     */
    
    static testMethod void salesInvoice() {  
        //createTestRecords();
        Test.startTest();
        c2g__codaPeriod__c period = [SELECT ID FROM c2g__codaPeriod__c LIMIT 1]; 
        Account newAccount =[SELECT ID FROM Account LIMIT 1]; 
        Opportunity opportunity = [SELECT ID FROM Opportunity where Invoicing_contact__c != null and StageName !='Closed Won' LIMIT 1]; 
        
        c2g__codaAccountingCurrency__c accountCurrency = [SELECT Id
                                                    FROM c2g__codaAccountingCurrency__c 
                                                    Limit 1];
        
        
        c2g__codaInvoice__c salesInvoice = new c2g__codaInvoice__c();
        salesInvoice.c2g__InvoiceDate__c = System.today()-1;
        salesInvoice.c2g__DueDate__c = System.today()+1;
        salesInvoice.c2g__InvoiceStatus__c = 'In progress';
        salesInvoice.c2g__Period__c = period.Id;
        salesInvoice.c2g__Opportunity__c = opportunity.Id;
        salesInvoice.c2g__Account__c = newAccount.Id;
        salesInvoice.c2g__InvoiceCurrency__c = accountCurrency.Id;
        Insert salesInvoice;
        
        Test.stopTest();
    }  
    
    static testMethod void salesInvoiceSecondtest() { 
        Test.startTest();
       
        Id batchId = Database.executeBatch(new BatchOpportunityUpdate(),1);
        System.debug('batchId ::'+batchId);
        //System.abortJob(batchId);
        Test.stopTest();
        
    }
}