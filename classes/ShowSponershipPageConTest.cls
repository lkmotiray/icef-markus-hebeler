/*
Purpose : test class for ShowSponershipPageCon
*/
@isTest
public class ShowSponershipPageConTest {
    
    @testSetup
    private static void setTestData(){
        
        // create country
        String countryId = createCountry();
        
        // create ICEF Event
        String icefEventId = createICEFEvent(countryId);
    }
    
    public static testmethod void validateFunctionality(){
        
        ICEF_Event__c recordOfICEFEvent = [SELECT Id,Event_Full_Name__c
                                           FROM ICEF_Event__c 
                                           LIMIT 1];
        //Create ApexPages.StandardController instance 
        ApexPages.StandardController icefEvent = new ApexPages.StandardController(recordOfICEFEvent); 
        
        ShowSponershipPageCon showSponsershipobj = new ShowSponershipPageCon(icefEvent);
        showSponsershipobj.ICEF_EventId = recordOfICEFEvent.ID;
        ICEF_Event__c ICEF_EventRecord = showSponsershipobj.ICEF_EventRecord;
    }
    
    public static String createCountry(){
        User userRecord = [SELECT ID 
                           FROM User
                           WHERE Name = 'Liz Adams'];
        
        Country__c countryRecord = new Country__c (Name='TestC' ,
                                                   Region__c = 'South & Central America',
                                                   Sales_Territory__c = 'Americas',
                                                   Agent_Relationship_Manager__c = userRecord.Id,
                                                   countries_from_to_in__c = True,
                                                   Sales_Territory_Manager__c = userRecord.Id);
        insert countryRecord;
        return countryRecord.Id;
    }
    public static String createICEFEvent(String countryId){
        
        User userRecord = [SELECT ID 
                           FROM User
                           WHERE Name = 'Liz Adams'];
        
        RecordType recordTypeRecord = [SELECT Id 
                                       FROM RecordType 
                                       WHERE Name = 'Global Workshop'
                                       AND SobjectType = 'ICEF_Event__c' ];
        
        ICEF_Event__c recordOfICEFEvent = new ICEF_Event__c(RecordType = recordTypeRecord,
                                                            Event_Full_Name__c ='test event',
                                                            Name = 'TestEvent',
                                                            Event_City__c = 'TestCity',
                                                            Event_Country__c = countryId,
                                                            technical_event_name__c = 'Technical',
                                                            catalogue_format__c = 'A4',
                                                            catalogue_spelling__c = 'British English',
                                                            Event_Manager_Educators__c = userRecord.Id,
                                                            Event_Manager_Agents__c = userRecord.Id);
                                                         
        
        insert recordOfICEFEvent;                                                   
        return recordOfICEFEvent.Id;
    }      
}