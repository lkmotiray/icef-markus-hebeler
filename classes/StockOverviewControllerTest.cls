/*
 * @Purpose: Test class for StockOverviewController
 * @Date: 15-6-2018
 * @Author: (Dreamwares)
 */
@isTest
public class StockOverviewControllerTest {

   /*
    * @Purpose: Create data required for testing
    */
    @testSetup
    public static void testData(){
    
        // create country
        Country__c countryRecord = new Country__c ( Name='TestCountry');
        insert countryRecord;
                  
        // Create User
        User testUser = new User(Alias = 'standt', 
                                 Email='testuser@testorg.com', 
                                 EmailEncodingKey='UTF-8', 
                                 LastName='Testing', LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US', 
                                 ProfileId = '00e200000019GqS', 
                                 TimeZoneSidKey='America/Los_Angeles', 
                                 UserName='testuser@fgh.com');
        System.runAs(testUser) {} 
                                  
        // create ICEF Event               
        ICEF_Event__c recordOfICEFEvent = new ICEF_Event__c(Name = 'TestEvent',
                                                            Event_City__c = 'TestCity',
                                                            Event_Country__c = countryRecord.Id,
                                                            technical_event_name__c = 'Technical',
                                                            catalogue_format__c = 'A4',
                                                            catalogue_spelling__c = 'British English',
                                                            Event_Manager_Educators__c = testUser.Id,
                                                            Event_Manager_Agents__c = testUser.Id);
                                                           
        insert recordOfICEFEvent;  
        
        // Create Product
        List<Product2> productList = new List<Product2>();
        for(Integer index = 0; index < 3; index++){
            Product2 product;
            if(index == 0){
                product = new Product2(Name = 'test 1', 
                                       Family = 'Workshop Registrations',
                                       IsActive = true);
            }else if(index == 1){
                product = new Product2(Name = 'test 2', 
                                       Family = 'Workshop Catalogue/Guide',
                                       IsActive = true);            
            }else if(index == 2){
                product = new Product2(Name = 'test 3', 
                                       Family = 'Workshop Receptions',
                                       IsActive = true);
            }
            productList.add(product);
        }
        insert productList;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        // Create Pricebook Entry for each product
        List<PricebookEntry> PricebookEntryList = new List<PricebookEntry>();
        for(Integer index = 0; index < 3; index++){
        
            PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, 
                                                              UnitPrice = 10000, 
                                                              IsActive = true);
            if(index == 0){
                standardPrice.Product2Id = productList[0].Id;
            }else if(index == 1){
                standardPrice.Product2Id = productList[1].Id;
            }else if(index == 2){
                standardPrice.Product2Id = productList[2].Id;
            }
            PricebookEntryList.add(standardPrice);
        }
        insert PricebookEntryList;
        
        
        // Create Product Participation
        List<Product_Participation__c> productParticipationList = new List<Product_Participation__c>();
        for(Integer index = 0; index < 9; index++){
            Product_Participation__c productParticipation; 
            if(index == 0){
                 productParticipation = new Product_Participation__c(ICEF_Event__c = recordOfICEFEvent.id,
                                                                     Product__c = productList[0].id ,
                                                                     Product_status__c = 'available');
            }else if(index == 1){
                productParticipation = new Product_Participation__c(ICEF_Event__c = recordOfICEFEvent.id,
                                                                    Product__c = productList[0].id ,
                                                                    Product_status__c = 'pending payment');
            }else if(index == 2){
                 productParticipation = new Product_Participation__c(ICEF_Event__c = recordOfICEFEvent.id,
                                                                     Product__c = productList[0].id ,
                                                                     Product_status__c = 'registered');
            }else if(index == 3){
                 productParticipation = new Product_Participation__c(ICEF_Event__c = recordOfICEFEvent.id,
                                                                     Product__c = productList[1].id ,
                                                                     Product_status__c = 'pending payment');
            }else if(index == 4){
                productParticipation = new Product_Participation__c(ICEF_Event__c = recordOfICEFEvent.id,
                                                                    Product__c = productList[1].id ,
                                                                    Product_status__c = 'registered');            
            }else if(index == 5){
                productParticipation = new Product_Participation__c(ICEF_Event__c = recordOfICEFEvent.id,
                                                                    Product__c = productList[1].id ,
                                                                    Product_status__c = 'available');            
            }else if(index == 6){
                productParticipation = new Product_Participation__c(ICEF_Event__c = recordOfICEFEvent.id,
                                                                    Product__c = productList[2].id ,
                                                                    Product_status__c = 'registered');         
            }else if(index == 7){
                productParticipation = new Product_Participation__c(ICEF_Event__c = recordOfICEFEvent.id,
                                                                    Product__c = productList[2].id ,
                                                                    Product_status__c = 'available');
            }else if(index == 8){
                productParticipation = new Product_Participation__c(ICEF_Event__c = recordOfICEFEvent.id,
                                                                    Product__c = productList[2].id ,
                                                                    Product_status__c = 'pending payment');
            }                                                                       
            productParticipationList.add(productParticipation);
        }
        insert productParticipationList; 
        
        // Create Account
        Account account = new Account( Name = 'Test',
                                       CurrencyIsoCode = 'EUR',
                                       Educational_Sector__c = '5 MULTI-SECTORAL',
                                       Status__c = 'Active',
                                       Mailing_Country__c = countryRecord.id,
                                       ARM__c = testUser.Id,
                                       Mailing_State_Province__c = 'AL');
        insert account;
        
        // Create Contact
        Contact contact = new Contact( Salutation = 'Mr',
                                       FirstName = 'FirstName',
                                       LastName = 'LastName',
                                       Gender__c = 'male',
                                       AccountId = account.Id,
                                       Role__c = 'Primery Decision Maker',
                                       Contact_Status__c = 'Active',
                                       Contact_Email__c = 'test@test.com');
        insert contact;
       
        // Create Opportunity
        List<Opportunity> opportunityList = new List<Opportunity>();
        for(Integer index = 0; index < 9; index++){
            
             Opportunity opportunity = new Opportunity(Name = 'test Opportunity',
                                                       AccountId = account.id,
                                                       CloseDate = System.today(),
                                                       Invoicing_contact__c = contact.id);
                                                  
             if(index == 0){
                 opportunity.StageName = 'Prospecting';    
             }else if(index == 1){
                 opportunity.StageName = 'Qualification';      
             }else if(index == 2){
                 opportunity.StageName = 'Needs Analysis';     
             }else if(index == 3){
                 opportunity.StageName = 'Proposal/Price Quote';       
             }else if(index == 4){
                 opportunity.StageName = 'Negotiation/Review';      
             }else if(index == 5){
                 opportunity.StageName = 'Salesorder sent';      
             }else if(index == 6){
                 opportunity.StageName = 'Salesorder received';      
             }else if(index == 7){
                 opportunity.StageName = 'Closed Won';      
             }else if(index == 8){
                 opportunity.StageName = 'Closed Lost';      
             }
            opportunityList.add(opportunity); 
        }
        insert opportunityList;
        
        // Create Opportunity Line Items
        List<OpportunityLineItem> opportunityLineItemList = new List<OpportunityLineItem>();
        for(Integer index = 0; index < 9; index++){
        
              OpportunityLineItem oppLineItem = new OpportunityLineItem(Product_Name__c = 'test product',
                                                                        PricebookEntryId = PricebookEntryList[0].id,
                                                                        Product2Id = productList[0].id,
                                                                        Quantity = 1,
                                                                        TotalPrice = 1,
                                                                        ICEF_Event__c = recordOfICEFEvent.id,
                                                                        Change_Comments__c = 'testing changes');
        
             if(index == 0){
                 oppLineItem.OpportunityId = opportunityList[0].id;
             }else if(index == 1){
                 oppLineItem.OpportunityId = opportunityList[1].id;      
             }else if(index == 2){
                 oppLineItem.OpportunityId = opportunityList[2].id;     
             }else if(index == 3){
                 oppLineItem.OpportunityId = opportunityList[3].id;      
             }else if(index == 4){
                 oppLineItem.OpportunityId = opportunityList[4].id;
             }else if(index == 5){
                 oppLineItem.OpportunityId = opportunityList[5].id;     
             }else if(index == 6){
                 oppLineItem.OpportunityId = opportunityList[6].id;      
             }else if(index == 7){
                 oppLineItem.OpportunityId = opportunityList[7].id;      
             }else if(index == 8){
                 oppLineItem.OpportunityId = opportunityList[8].id;
             }
            opportunityLineItemList.add(oppLineItem);
        }
        
        insert opportunityLineItemList;                                                      
    }
    
   /*
    * @Purpose :  Test method to test functionality
    */
    @isTest
    public static void testFunctioanlity(){
        
        Test.StartTest();
            ICEF_Event__c event = [SELECT id FROM ICEF_Event__c];
            
            PageReference pageRef = Page.StockOverview;
            pageRef.getparameters().put('id', event.id);  
            Test.setCurrentPage(pageRef);
            
            Apexpages.StandardController sc = new Apexpages.StandardController(event);
            StockOverviewController stockManagmentController = new StockOverviewController(sc);
            
            Integer saleable = 0;
            for(Integer index = 0; index < (stockManagmentController.summaryList).size(); index++){
                if(stockManagmentController.summaryList[index].productName == 'test 2'){
                    saleable = stockManagmentController.summaryList[index].saleable;
                }
            }
        Test.StopTest();
            System.assertEquals(1,saleable);
    }
}