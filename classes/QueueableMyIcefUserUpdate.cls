public class QueueableMyIcefUserUpdate implements Queueable, Database.AllowsCallouts {
    
    //private Contact contact;
    private String whichMethod;
    private List<Id> contactIds;
    
    // lieber: String method name und Contact ids.
    // dann alle contacts auf einmal holen (Map) und die einzelnen Contacts updaten / createn / löschen
    public queueableMyIcefUserUpdate(String methodName, List<Id> recordIds){
        this.contactIds = recordIds;
        this.whichMethod = methodName;
    }
    
    public void execute(QueueableContext context){
        
        Map<Id,Contact> contactMap = new Map<Id,Contact>([SELECT Id, FirstName, LastName, Contact_Email__c, AccountId 
                                                          FROM Contact 
                                                          WHERE Id IN :contactIds]);
        List<Id> newUser = new List<Id>();
        
        for (Id conId : contactIds){
            switch on whichMethod {
                //muss ich vielleicht noch umbenennen in createOrUpdate.
                when 'updateUser' {
                    //create or Update User
                    String token = Authorization.getAccessToken(); 
                    Contact contact = contactMap.get(conId);
                    UserInformation user = AuthUser.getUserByUsername(contact.Contact_Email__c, token);    
                    // If there is no user for contact email: create new.
                    if(user.id == null){
						newUser.add(conId);
                    } else {
                        System.debug('Userinfo changed in Contact. Updating user.');
                        //erstetzen!
                        AuthUser.updateUser(contact.Id, user, token);
                    }
                }
                when 'updateEmailUser'{
                    //
                }
                when 'createUser'{
                    
                }
                when else{
                    //error message to me
                }
            }
        }
        
        if (newUser.size() > 0){
            // queue new job with method create
        }
        
    }

}