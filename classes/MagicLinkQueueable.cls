public class MagicLinkQueueable implements Queueable {
    
    private List<Id> contactIds;
    private Long timeStamp;
    private String randomValue, secret = 'ejesheitohn8eeV7yu7j', openid, combinedString, sha1Coded;
    
    public magicLinkQueueable(List<Id> conIds){
        this.contactIds = conIds;
        this.timeStamp = System.Now().getTime()/1000;
        this.randomValue = EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(0,10);
    }
    
    public void execute(QueueableContext context){
        System.debug('queued for contacts: ' + contactIds);
        List<Contact>contactsToUpdate = new List<Contact>();
        Map<Id,Contact> contactMap = new Map<Id,Contact>([SELECT Id,Email,magic_link_updated__c,magic_link__c 
                                                          FROM Contact 
                                                          WHERE Id IN :contactIds]);
        System.debug('contactMap: '+ contactMap);
        for (Id contId : contactIds){
            Contact cont = contactMap.get(contId);

            System.debug('Contact (Cont): ' + Cont);
            openid = cont.email;
            
            combinedString = openid + timeStamp + secret + randomValue;
            
            sha1Coded = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', Blob.valueOf(combinedString)));
            
            cont.magic_link__c = '/login/magic?openid=' + cont.email +'&time_stamp='+ String.ValueOf(timeStamp) +'&salt='+ randomValue +'&hash='+ sha1Coded; 
            cont.magic_link_updated__c = DateTime.newInstance(timestamp*1000);
            
            contactsToUpdate.add(cont);
  
        }
        Database.update(contactsToUpdate);
    }
}