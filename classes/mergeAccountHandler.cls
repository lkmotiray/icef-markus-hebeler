/*-------------------------------------
    @Developer:     Henriette Schönleber
    @Purpose:       functions to fire triggers after merging accounts
    @Created Date:  2018/02/07
    @Modified Date:	2018/02/21 : only fire if it was a merge. Only fire if there are memberships.
 --------------------------------------*/

public class mergeAccountHandler {
    /*
     * @Purpose:    update Nudge field of a list of Memberships (to fire update triggers)
     * @Parameter:  List of Membership Ids: one id per loser account in merge
     */
    @future
    public static void touchMemberships(Set<Id> ids){
        List<Membership__c> memList = [SELECT Id, Nudge__c FROM Membership__c WHERE Id IN :ids];
        DateTime stamp = System.now();
        for (Membership__c mem : memList){
            mem.Nudge__c = stamp;
        }
        Database.update(memList);
    }
    
    /*
     * @Purpose:    get list of membership ids, one per losing account.
     * @Parameter:  Map of Account: Trigger.OldMap
     */
    public static void fireMembershipTrigger(Map<Id,Account> acctMap){
        Set<Id> accts = new Set<Id>();
        for (Account acct : acctMap.values()){
            if (String.isNotBlank(acct.MasterRecordId)){
                accts.add(acct.MasterRecordId);
            }
        }
        // make sure it was a merge
        if (accts.size() > 0){
        	Map<Id,Membership__c> memList = new Map<Id, Membership__c>([SELECT Id, Member__c FROM Membership__c WHERE Member__c IN :accts]); 
            // make sure to fire when there are Memberships only
        	if (memList.size() > 0){
        		touchMemberships(memList.keySet());           
        	}            
        }
    }
}