@isTest
private class MagicLinkBatchTest {
    @testSetup
    static void setup(){
        Account acct = DataFactory.createTestEducators(1)[0];
        insert acct;
        List<Contact> contacts = DataFactory.createTestContacts(acct, 100);
        insert contacts;
    }

    static testMethod void testBatchMagic(){
        Test.startTest();
        MagicLinkBatch mlb = new MagicLinkBatch();
        Id batchId = Database.executeBatch(mlb);
        Test.stopTest();
        System.assertEquals(0, [SELECT count() FROM Contact WHERE magic_link_updated__c = null]);
    }
}