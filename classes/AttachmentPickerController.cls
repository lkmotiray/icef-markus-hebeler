/** 
  * @Author      : Prajakta
  * @Description : Controller class for AttachmentPicker VF page
  * @Created Date: 13 May 2016
 */
public with sharing class AttachmentPickerController {

    public String documentSearchText                  { get; set; }
    public List<DocumentWrapper> listDocumentWrapper  {get;set;}
    transient public String listDocumentWrapperJsonStr{get;set;}
    public Boolean isShowAllDocs{get;set;}
    
    Folder folder;
    public Document document {
        get {
            if (Document == null)
            Document = new Document ();
            return document;
        }
        set;
    }
    public String selctedLocation { get; set; }
    
    transient public Integer sizeAllDocuments{get;set;}
    //--Constructor
    public AttachmentPickerController() {
        sizeAllDocuments = 0;
        isShowAllDocs = false;
        selctedLocation = 'My Computer';
        listDocumentWrapper = new List<DocumentWrapper>();
        try  
        {
            folder = [ SELECT Id FROM Folder WHERE Name = :'CustomEmailTempAttachment'];    
        } catch(Exception e)
        {
            System.debug('Exception occured while retrieving CustomEmailTempAttachment folder::'+e);
        }
    }
    
    /**
      * @purpose : get All files from documents
      * @Return  : List of Documents
     */
    public List<Document> getDocumentFiles()  {
        selctedLocation = 'My Computer';
        List<Document> listDocuments = new List<Document>();
        try {
            String searchTextForName = '%' + documentSearchText + '%';
                                   
            String excludedFolderID =  folder.Id;  
                          
            String query = 'SELECT Id, Name, LastModifiedDate, Author.Name, BodyLength FROM Document '+
                             'WHERE FolderId != :excludedFolderID AND ContentType != null '+
                             (String.isEmpty(documentSearchText) ? 'AND LastModifiedDate >= LAST_N_DAYS:8' : ' AND Name LIKE :searchTextForName') +
                             ' LIMIT 200';
                          
            listDocuments = Database.query(query);
            
        }catch(Exception e) {
            System.debug('Error occured while retrieving files::'+e);
        }
        documentSearchText = '';
        isShowAllDocs = false;
        return listDocuments;
    }
    
    
    /**
      * @purpose : upload file as document from local system
      * @Return  : PageReference
     */
    public PageReference upload() {
          //isShowAllDocs = false;
          System.debug('file size::'+document.BodyLength);
          
          //--Check file size not exceed than 10MB
          if(document.BodyLength > 10485760)
          {
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'File Limit exceeded!!'));
              return null;       
          }
          
          //--Set author id for document
          document.AuthorId = UserInfo.getUserId();
          
          try {
              //--Insert Document
              if(folder != null) {
                 document.FolderId = folder.Id;
                 insert document;
                 
                 if(!String.isEmpty(document.id)) {
                     System.debug('bodyLength '+document.bodyLength);
                     DocumentWrapper docWrapper = new DocumentWrapper();
                     docWrapper.Id = document.id;
                     docWrapper.fileName = document.Name;
                     docWrapper.fileBodyLength = document.BodyLength;
                     docWrapper.contentType = 'Document';
                     
                     listDocumentWrapper.add(docWrapper);
                     listDocumentWrapperJsonStr = JSON.Serialize(listDocumentWrapper);
                     System.debug('listDocumentWrapper::'+listDocumentWrapper);
                 }
              }
              else {
                  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'CustomEmailTempAttachment is not exist, Please create folder with name \'CustomEmailTempAttachment\' and try again'));    
              }
          } catch (DMLException e) {
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
              return null;
          } finally {
              document = new document(); 
          }
        
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
          return null;
    }

    /**
      * @purpose : get available Locations of attachment[Files]
      * @Return  : List of select option
     */
    public List<SelectOption> getLocations() {
        List<SelectOption> listFileLocation = new List<SelectOption>();
        listFileLocation.add(new SelectOption('My Computer','My Computer'));
        listFileLocation.add(new SelectOption(UserInfo.getUserId(),'My Personal Documents'));
        listFileLocation.add(new SelectOption('Document','Document Folders'));
        return listFileLocation;
    }
    
    /**
      * @purpose : get Document Folders
      * @Return  : List of folders
     */
    public List<Folder> getDocumentFolders() {
        List<Folder> listDocFolders = new List<Folder>();
        if(selctedLocation.equals('Document')) {
            try  {
                listDocFolders = [ SELECT Id, Name, Type 
                                   FROM Folder 
                                   WHERE Type = 'Document'
                                         AND
                                         Id != :folder.Id 
                                 ];
            } Catch(Exception e) {
                System.debug('Exception while retrieving Folders::'+e);
            }
        }
        return listDocFolders;
    }
    
    /**
      * @purpose : get Files From selected folder
      * @Return  : list of documents from selected folder
     */
    public List<Document> getFilesFromDocFolder() {
        List<Document> listDocuments = new List<Document>();
        if(selctedLocation.equals('Document')) {
            try {
                listDocuments = new List<Document>();
                Id selectedFolderId = Apexpages.currentPage().getParameters().get('selectedfolder');
                System.debug('selectedFolderId::'+selectedFolderId);
                listDocuments = [SELECT Id, Name, Author.Name, BodyLength FROM Document
                                 WHERE FolderId =: selectedFolderId AND
                                       ContentType != null ORDER BY createdDate DESC LIMIT 999];
                System.debug('listDocuments1 ::'+listDocuments);
                
            }catch(Exception e) {
                System.debug('Error occured while retrieving files::'+e);
            }
             
        }
        else if(selctedLocation.equals(UserInfo.getUserId())) {
            try {
                listDocuments = new List<Document>();
                Id folderId = UserInfo.getUserId();
                listDocuments = [SELECT Id, Name, Author.Name, BodyLength FROM Document
                                 WHERE FolderId =: folderId AND
                                       ContentType != null ORDER BY createdDate DESC LIMIT 999];
                System.debug('listDocuments2 ::'+listDocuments);
            } catch(Exception e) {
                System.debug('Error occured while retrieving files::'+e);
            }     
        }
        //return null;
        return listDocuments; 
    }
    
   
    /**
      * @Description : Document wrapper class
     */
    class DocumentWrapper {
        String Id;
        public String fileName{get;set;}
        String contentType;
        public Integer fileBodyLength {get;set;}
        
    }
}